<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div id="sidebar-mycinema">
<?php
echo $this->render( 'SocialSnackFrontBundle:Partials:sidebar/cinemaHeader.html.php', array(
    'date'            => $date,
    'cinema'          => $cinema,
    'sessions'        => $sessions,
    'single'          => FALSE,
    'available_dates' => $available_dates,
));
?>

    <div class="mycinema-list" rel="main-list">
<?php if ( !sizeof( $sessions ) ) { ?>
    <?php if ( !sizeof( $closer_cinemas ) ) { ?>
        <div class="sidebar-nosessions">Por el momento esta película no está programada en este cine ni en los cines cercanos.</div>
    <?php } else { ?>
        <div class="sidebar-nosessions">Por el momento esta película no está programada en este cine.</div>
    <?php } ?>
<?php } ?>

<?php foreach ( $sessions as $_movie ) { ?>
        <div class="mycinema-li">
            <span class="mycinema-item-rating" title="Clasificación"><?php echo $_movie['parent']->getData('RATING'); ?></span>

            <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="mycinema-item-title"><?php echo $_movie['parent']->getName(); ?></a>

            <?php
            echo $this->render('SocialSnackFrontBundle:Partials:sidebar/movieSessions.html.php', array(
                'items'    => $_movie['sessions'],
                'platinum' => $cinema->getPlatinum(),
                'checkout_ref' => 'movie_side',
            ));
            ?>
        </div>
<?php } ?>
    </div>

<?php
$blocks = array(
    array(
        'data'   => $closer_cinemas,
        'title'  => sprintf( 'Ver la película <strong>%s</strong> en cines cercanos', $movie->getName() ),
        'states' => FALSE
    )
);

$max = 3;

foreach ( $blocks as $block ) :
?>
    <?php if ( sizeof( $block['data'] ) ) : ?>
    <div class="sidebar-heading-alt">
        <span class="icon icon-marker icon-white pad-left sidebar-title-alt"><?php echo $block['title']; ?></span>
    </div>

    <div class="mycinema-list mycinema-alt-list">
        <?php $i = 0; ?>
        <?php foreach ( $block['data'] as $cc ) { ?>
        <div class="mycinema-li<?php if ( $i >= $max ) echo ' hidden'; ?>">
            <a href="<?php echo $view['fronthelper']->get_cinema_link( $cc['cinema'] ); ?>" class="mycinema-item-title"><?php echo $cc['cinema']->getName(); ?></a>

            <?php
            echo $this->render('SocialSnackFrontBundle:Partials:sidebar/movieSessions.html.php', array(
                'items'    => $cc['movies'],
                'platinum' => $cc['cinema']->getPlatinum(),
                'checkout_ref' => 'movie_side_nearby',
            ));
            ?>
        </div>
            <?php $i++; ?>
        <?php } ?>

        <?php if ( sizeof( $block['data'] ) > $max ) { ?>
        <a href="#" class="mycinema-more">Ver más</a>
        <?php } ?>
    </div>
	<?php endif; ?>
<?php endforeach; ?>

<?php if ( !sizeof( $sessions ) && !sizeof( $closer_cinemas ) ) { ?>
    <a href="#" class="sidebar-heading-alt no-tip" rel="ask_location">
        <span class="icon icon-marker icon-white pad-left sidebar-title-alt">Busca <strong><?php echo $movie->getName(); ?></strong> en otra zona</span>
    </a>
<?php } ?>

    <p id="mycinema-search-movie"><a class="btn btn-default btn-icon icon icon-search" href="<?php
    echo $view['router']->generate('billboard', array(
        'area_id' => $area->getId(),
        'slug' => FrontHelper::sanitize_for_url($area->getName()),
        'arguments' => 'movie-' . $movie->getId()
    )); ?>/">Buscar esta película en otros cines</a></p>
</div><!-- #sidebar-mycinema -->
