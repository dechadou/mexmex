<?php $view->extend('SocialSnackFrontBundle:Landings:classics_layout.html.php'); ?>

<?php $view['slots']->start('landingbody'); ?>

<?php
$limit  = 4;
$offset = 0;

while ($row = array_slice($movies, $offset, $limit)) :
  $offset += $limit;
?>
  <div class="grid-row">
    <div class="grid-centered">
  <?php foreach ($row as $movie) : ?>
      <div class="grid-col-4">
        <a class="movie-card" href="<?php echo $view['fronthelper']->get_movie_link($movie); ?>">
          <div class="movie-poster">
            <img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '182x272'); ?>">
          </div>

          <p class="movie-title"><?php echo $movie->getName(); ?></p>
          <p class="movie-meta"><?php echo $movie->getData('GENERO'); ?></p>
        </a>
      </div>
  <?php endforeach; ?>
    </div>
  </div>
<?php endwhile; ?>

<?php $view['slots']->stop(); ?>
