<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div id="sidebar-mycinema">
  
  <div class="sidebar-heading-alt">
    <span class="icon icon-marker icon-white pad-left sidebar-title-alt">Cines</span>
	</div>
  
	<div class="mycinema-list mycinema-alt-list">
    <?php $i = 0; ?>
		<?php foreach ( $sessions as $cc ) { ?>
		<div class="mycinema-li">
			<a href="<?php echo $view['router']->generate( 'cinema_single', array( 'cinema_id' => $cc['cinema']->getId(), 'slug' => FrontHelper::sanitize_for_url( $cc['cinema']->getName() ) ) ); ?>" class="mycinema-item-title"><?php echo $cc['cinema']->getName(); ?></a>
				<?php /* @var $session \SocialSnack\WsBundle\Entity\Session */ ?>
				<?php foreach ( $cc['movies'] as $item ) { ?>
				<div>
          <?php foreach ( FrontHelper::get_movie_attrs( $item['movie']->getAttr() ) as $attr ) {; ?>
          <span class="movie-info-attr"><?php echo $attr; ?></span>
          <?php } ?>
        </div>
        <div class="mycinema-sessions icon icon-dark icon-clock pad-left">
          <?php foreach ( $item['sessions'] as $session ) { ?>
          <a href="<?php echo $view['router']->generate( 'checkout', array( 'session_id' => $session->getId() ) ); ?>" rel="noindex,nofollow"><?php echo $session->getTime()->format( 'h:i A' ); ?></a>
          <?php } ?>
        </div>
				<?php } ?>
		</div>
		<?php $i++; ?>
		<?php } ?>
	</div>
  
</div><!-- #sidebar-mycinema -->