<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Promo;
use SocialSnack\FrontBundle\Entity\PromoA;
use SocialSnack\FrontBundle\Entity\PromoIE;
use SocialSnack\FrontBundle\Entity\PromoImportant;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PromoHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Rodrigo Catalano
 */
class PromoHandler {

  protected $doctrine;

  protected $front_utils;


  public function __construct(Registry $doctrine, FrontUtils $front_utils) {
    $this->doctrine = $doctrine;
    $this->front_utils = $front_utils;
  }

  public function getPromoA($id=null){
    /* @var $repo \SocialSnack\FrontBundle\Entity\PromoARepository */
    $repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:PromoA' );
    $result = [];
    $promos = $repo->findActive(1,$id);

    foreach($promos as $promo){
      $result[] = $this->serializePromoA($promo);
    }

    if ($id != null) {
      return count($result) ? $result[0] : FALSE;
    } else {
      return ['promoA' => $result];
    }
  }

  public function getPromoImportant($id=null){
    /* @var $repo \SocialSnack\FrontBundle\Entity\PromoImportantRepository */
    $repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:PromoImportant' );
    $promos = $repo->findActive(null,1,$id);
    $result = [];

    foreach($promos as $promo) {
      $result[] = $this->serializePromoImportant($promo);
    }

    if ($id != null) {
      return count($result) ? $result[0] : FALSE;
    } else {
      return ['promoImportant' => $result];
    }
  }

  public function getPromoIE($id=null){
    /* @var $repo \SocialSnack\FrontBundle\Entity\PromoIERepository */
    $repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:PromoIE' );
    $promos = $repo->findActive(1,$id);
    $result = [];

    foreach($promos as $promo){
      $result[] = $this->serializePromoIE($promo);
    }

    if ($id != null) {
      return count($result) ? $result[0] : FALSE;
    } else {
      return ['promoIE' => $result];
    }
  }

  public function getPromo($id=null){
    /* @var $repo \SocialSnack\FrontBundle\Entity\PromoRepository */
    $repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Promo' );
    $promos = $repo->findActive(1,$id);
    $result = [];

    foreach($promos as $promo){
      $result[] = $this->serializePromo($promo);
    }

    if ($id != null) {
      return count($result) ? $result[0] : FALSE;
    } else {
      return ['promo' => $result];
    }
  }

  public function getAll($id=null) {
    $result = array_merge(
        $this->getPromoA($id),
        $this->getPromoIE($id),
        $this->getPromoImportant($id),
        $this->getPromo($id)
    );

    return $result;
  }

  public function getByType($type,$id=null){
    $result = [];
    switch($type){
      case 'promoA':
        $result = $this->getPromoA($id);
        break;
      case 'promoIE':
        $result = $this->getPromoIE($id);
        break;
      case 'promoImportant':
        $result = $this->getPromoImportant($id);
        break;
      case 'promo':
        $result = $this->getPromo($id);
        break;
      default:
        $result = false;
        break;
    }
    return $result;
  }


  protected function serializePromoImportant(PromoImportant $promo) {
    return array(
        'id'    => $promo->getId(),
        'title' => $promo->getTitle(),
        'url'   => $promo->getUrl(),
        'image' => $promo->getImage() ? $this->front_utils->get_cms_url($promo->getImage(), '700x573') : '',
        'thumb' => $promo->getThumb() ? $this->front_utils->get_cms_url($promo->getThumb(), '490x116') : '',
    );
  }

  protected function serializePromoIE(PromoIE $promo) {
    return array(
        'id'         => $promo->getId(),
        'title'      => $promo->getTitle(),
        'thumb'      => $promo->getThumb() ? $this->front_utils->get_cms_url($promo->getThumb(), '300x393') : '',
        'image'      => $promo->getImage() ? $this->front_utils->get_cms_url($promo->getImage()) : '',
        'categories' => $promo->getCategories()
    );
  }

  protected function serializePromoA(PromoA $promo) {
    return array(
        'id'        => $promo->getId(),
        'title'     => $promo->getTitle(),
        'url'       => $promo->getUrl(),
        'image'     => $promo->getImage() ? $this->front_utils->get_cms_url($promo->getImage(), '1020x129') : '',
    );
  }

  protected function serializePromo(Promo $promo) {
    return array(
        'id'        => $promo->getId(),
        'title'     => $promo->getTitle(),
        'url'       => $promo->getUrl(),
        'image'     => $promo->getImage() ? $this->front_utils->get_cms_url('promos/' . $promo->getImage(), '670x320') : '',
        'thumb'     => $promo->getThumb() ? $this->front_utils->get_cms_url('promos/' . $promo->getThumb(), '236x222') : '',
    );
  }

} 