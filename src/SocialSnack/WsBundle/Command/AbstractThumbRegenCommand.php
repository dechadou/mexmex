<?php

namespace SocialSnack\WsBundle\Command;

use Composer\Command\Helper\DialogHelper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractThumbRegenCommand extends ContainerAwareCommand {

  protected $output;

  protected function execute(InputInterface $input, OutputInterface $output) {
    set_time_limit(0);

    $this->output = $output;

    $sizes = $this->defineSizes();
    $files = $this->getFiles();

    $output->writeln('');
    $output->writeln(sprintf('Regenerating size(s): %s...', implode(', ', array_keys($sizes))));
    $output->writeln('');

    $this->process($files, $sizes);

    $output->writeln('');
    $output->writeln('<info>Done.</info>');

    return $this;
  }


  abstract protected function getFiles();


  abstract protected function getAvailableSizes();


  protected function defineSizes() {
    /** @var DialogHelper $dialog */
    $dialog = $this->getHelper('dialog');

    $sizes = $this->getAvailableSizes();

    $all = $dialog->askAndValidate(
        $this->output,
        '<info>Regenerate all sizes?</info> [<comment>yes</comment>] ',
        function($answer) {
          if (!in_array(strtolower($answer), ['y', 'n', 'yes', 'no'])) {
            throw new \Exception('Please answer [Y]es or [N]o');
          }

          return $answer;
        },
        FALSE,
        'Y'
    );

    // If user chose Yes then return all the available sizes.
    if (strtolower(substr($all, 0, 1)) === 'y') {
      return $sizes;
    }

    // If user chose No then ask for desired sizes (from available ones).
    $this->output->writeln('');
    $this->output->writeln(sprintf('<info>Available sizes:</info> <comment>%s</comment>.', implode(', ', array_keys($sizes))));
    $this->output->writeln('');

    $names = [];
    do {
      do {
        $name = $dialog->ask(
            $this->output,
            '<info>Enter size name (press <return> to stop adding sizes)</info>: ',
            '',
            array_keys($sizes)
        );
        if ($name) {
          $names[] = $name;
        }
      } while ($name != '');

      if (!sizeof($names)) {
        $this->output->writeln('<error> You must choose at least one size </error>');
      }
    } while (!sizeof($names));

    $sizes = array_intersect_key($sizes, array_flip($names));

    return $sizes;
  }


  abstract protected function process($files, $sizes);

}