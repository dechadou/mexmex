<?php
$view->extend('SocialSnackFrontBundle:X4d:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Experiencia X4d</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <iframe width="536" height="302" src="https://www.youtube.com/embed/jKCE5Fu62Mk?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>

      <?php
      /*
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-00.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-03.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-04.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-02.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-00.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-01.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-03.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-04.jpg'); ?>"></a>
        </div>
      </div>
      */
      ?>
    </div>

    <div class="grid-col-5">
      <p class="lead">Siente la máxima experiencia del cine al adentrarte en la historia de tu película favorita y experimenta sensaciones tan reales que creerás que estás en medio de la acción.</p>

      <h2>Movimiento</h2>
      <p>Siente todos y cada uno de los movimientos de la película en sincronía con lo que verás en la pantalla; vive las  explosiones, terremotos,  turbulencias de una forma tan real, que sentirás que estás dentro de ella.</p>

      <h2>Agua</h2>
      <p>Prepárate para darte los mejores chapuzones dentro de la película y compartir con tus actores favoritos las experiencias que vivirán ellos al sentir como te salpica la brisa durante las escenas de tormentas, lluvias, huracanes y tornados. ¡Vívelo y empápate de emoción!</p>

      <h2>Aire</h2>
      <p>Despéinate de diversión durante la película y siente como las ráfagas de viento llegan hacia ti mientras tu actor favorito lucha por salir de una gran escena de acción.</p>

      <p class="lead">Vive la Magia del Cine al máximo y siéntete parte de la película en la Sala Cinemex® X4D.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
