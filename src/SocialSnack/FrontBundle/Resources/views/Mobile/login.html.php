<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>
<?php $view['slots']->start('body'); ?>

<div id="login-popup">
  <div id="login-popup-fb">
    <p class="popup-title">Ingresa ahora con tu cuenta de Facebook</p>

    <a href="#" class="btn btn-icon btn-facebook icon icon-facebook btn100" id="fb-login-bt">Conectarme</a>
  </div>

  <div id="login-popup-std">
    <p class="popup-title">Ingresa con tu correo electrónico</p>

      <?php
      echo $view->render(
          'SocialSnackFrontBundle::flashMsgs.html.php',
          array( 'flash_msgs' => $flash_msgs )
      );
      ?>
      
    <form id="login-form" action="<?php echo $view['router']->generate( 'login_check' ); ?>" class="std-form compact-form no-cols" method="post">
      <?php
//              echo $view['actions']->render(
//                  new ControllerReference('SocialSnackFrontBundle:Partials:esiCsrf', array(
//                      'token' => 'authenticate',
//                      'name'  => '_csrf_token'
//                  )),
//                  array('strategy' => 'esi')
//              );
      ?>
      <?php if ( $target_path ) { ?>
      <input type="hidden" name="_target_path" value="<?php echo $target_path; ?>" />
      <?php } ?>
      <div class="form-row">
        <input type="email" name="_username" id="login-user" placeholder="Email" data-validators="not_empty email" data-flyvalidate="1" />
      </div>
      <div class="form-row">
        <input type="password" name="_password" id="login-pass" placeholder="Contraseña" data-validators="not_empty" data-flyvalidate="1" />
      </div>
      <?php /*
      <a href="<?php echo $view['router']->generate('user_forgot_pass'); ?>" class="small forgot-password">¿Olvidaste tu contraseña?</a>
      */ ?>
      <button type="submit" class="btn btn-icon btn-default icon icon-user btn100">Ingresar</button>
    </form><!-- #login-form -->

    <?php /*
    <a href="<?php echo $view['router']->generate( 'user_register' ); ?>" class="popup-bottom icon pad-right icon-rarr icon-dark">
      ¿No tienes una cuenta?<br/>
      Regístrate aquí
    </a>
    */ ?>
  </div>
</div><!-- #login-popup -->
<script>
var redirect_after_login = true;
</script>
<?php $view['slots']->stop(); ?>