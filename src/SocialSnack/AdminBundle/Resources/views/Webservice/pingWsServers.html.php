<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Webservice' );
$view['slots']->start('body');
?>
  <section class="panel">
    <header class="panel-heading">
      Estado de los servidores de WS
    </header>
    <div class="panel-body">
      <form action="" method="post">
        <div class="row">
          <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Consultar</button>
          </div>
        </div>
      </form>
    </div>
  </section>

<?php if (sizeof($result)) { ?>
  <div id="debug">
    <section class="panel">
      <header class="panel-heading">
        Resultado
      </header>

      <table class="table table-striped">
        <thead>
        <tr>
          <th>IP</th>
          <th>Código respuesta</th>
          <th>Mensaje</th>
          <th>Estado</th>
          <th>Salud</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach ($result as $item) { ?>
            <tr>
              <th><?php echo $item['ip']; ?></th>
              <td><?php echo $item['status']; ?></td>
              <td><?php echo $item['msg']; ?></td>
              <td>
                <?php if (200 === $item['status']) { ?>
                  <?php echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_SUCCESS; ?>
                <?php } elseif (NULL === $item['status']) { ?>
                  <?php echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_DANGER; ?>
                <?php } else { ?>
                  <?php echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_WARNING; ?>
                <?php } ?>
              </td>
              <td>
                <?php
                switch ($item['health']) {
                  case 'stable':  echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_SUCCESS; break;
                  case 'danger':  echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_DANGER; break;
                  case 'warning': echo \SocialSnack\AdminBundle\Debug\Result\ResultAbstract::TABLE_LABEL_WARNING; break;
                }
                ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

    </section>
  </div>
<?php } ?>
<?php $view['slots']->stop();