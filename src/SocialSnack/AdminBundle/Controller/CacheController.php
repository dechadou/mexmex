<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\FrontBundle\Service\CachePurger;
use Symfony\Component\HttpFoundation\Request;

class CacheController extends BaseController {


  protected function get_required_role() {
    return 'ROLE_CACHE';
  }


  /**
   * @Route("/cache_purge_url", name="social_snack_cache_url")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function purgeUrlAction(Request $request){
    $this->check_roles();

    $purge = NULL;

    $form = $this->createFormBuilder()
        ->add('env','choice', array(
            'choices'=>array(
                CachePurger::ENV_STATIC => 'static.cinemex.com',
                CachePurger::ENV_MOBILE => 'm.cinemex.com',
                CachePurger::ENV_FRONT  => 'cinemex.com',
            ),
            'required' => true,
            'attr' => ['class' => 'form-control'],
            'label' => 'Dominio',
        ))
        ->add('url', 'text', ['label' => 'URL'])
        ->add('submit', 'submit', ['label' => 'Borrar cache'])
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $form->getData();
      $cache_purger = $this->get('ss.cache_purger');
      $options = array(
          'type' => 'urls',
          'urls' => [[
              'path' => $data['url'],
              'env'  => $data['env'],
          ]],
      );
      $cache_purger->enqueue($options);
      $purge = $cache_purger->purge_cache();

      $this->addToLog('Borrar CACHE en url:'.$data['url']);
    }

    return $this->render('SocialSnackAdminBundle:Cache:purge_url.html.php', array(
        'form'   => $form->createView(),
        'purge'  => $purge,
    ));

  }


  /**
   * @Route("/cache_purge", name="social_snack_cache_purge")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function purgeAction(Request $request) {
    $this->check_roles();

    $purge = NULL;

    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas = $repo->findAll();
    $cinemaarray = array();
    $cinemaarray[0] = "Seleccione el cine";
    $cinemaarray[-1] = "Todos los cines";
    foreach ($cinemas as $cinema) {
      $cinemaarray[$cinema->getId()] = $cinema->getName();
    }


    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:StateArea');
    $zones = $repo->findAll();
    $zonearray = array();
    $zonearray[0] = "Seleccione la zona";
    $zonearray[-1] = "Todas las zonas";
    foreach ($zones as $zone) {
      $zonearray[$zone->getId()] = $zone->getName();
    }

    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:State');
    $zones = $repo->findAll();
    $statearray = array();
    $statearray[0] = "Seleccione un estado";
    $statearray[-1] = "Todos los estados";
    foreach ($zones as $zone) {
      $statearray[$zone->getId()] = $zone->getName();
    }

    $form = $this->createFormBuilder()
        ->add('zone', 'choice', array(
            'choices' => $zonearray,
            'attr' => ['class' => 'form-control']
        ))
        ->add('cinemazone', 'choice', array(
            'choices' => $statearray,
            'attr' => ['class' => 'form-control']
        ))
        ->add('cinema', 'choice', array(
            'choices' => $cinemaarray,
            'attr' => ['class' => 'form-control']
        ))
        ->add('cinemaTickets', 'choice', array(
            'choices' => $cinemaarray,
            'attr' => ['class' => 'form-control']
        ))
        ->add('home', 'checkbox', array(
            'required' => false
        ))
        ->add('social', 'checkbox', array(
            'required' => false
        ))
        ->add('dates', 'checkbox', array(
            'required' => false
        ))
        ->add('conceptMap', 'choice', array(
            'choices' => [
                'Art'      => 'Casa de Arte',
                'Xtreme'   => 'CinemeXtremo',
                'Premium'  => 'Premium',
                'Platinum' => 'Platino',
                'X4D'      => 'X4D',
                'V3D'      => '3D',
            ],
            'empty_value' => 'Seleccione el concepto',
            'required' => false,
            'attr' => ['class' => 'form-control']
        ))
        ->add('save', 'submit')
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $form->getData();

      $cache_purger = $this->get('ss.cache_purger');

      // Purge billboard by area.
      if ($data['zone'] != 0) {
        if ($data['zone'] == -1) {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_ALL_AREAS_BILLBOARDS,
          );
          $this->addToLog('Borrar CACHE de todas las Zonas');
        } else {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_AREA_BILLBOARD,
              'area_id' => $data['zone']
          );
          $this->addToLog('Borrar CACHE de la zona '.$data['zone']);
        }
        $cache_purger->enqueue($options);

      }

      // Purge states maps.
      if ($data['cinemazone'] != 0) {
        if ($data['cinemazone'] == -1) {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_ALL_STATES_MAPS,
          );
          $this->addToLog('Borrar CACHE de todos los States');
        } else {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_STATE_MAP,
              'state_id' => $data['cinemazone']
          );
          $this->addToLog('Borrar CACHE en estado '.$data['cinemazone']);
        }
        $cache_purger->enqueue($options);
      }

      // Purge concept maps.
      if ($data['conceptMap']) {
        $options = array(
            'type' => 'method',
            'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_CONCEPT_MAP,
            'attribute' => $data['conceptMap']
        );

        $cache_purger->enqueue($options);
        $this->addToLog('Borrar CACHE de mapa de concepto ' . $data['conceptMap']);
      }

      // Purge cinemas single pages.
      if ($data['cinema'] != 0) {
        if ($data['cinema'] == -1) {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_ALL_CINEMAS_SINGLES,
          );
          $this->addToLog('Borrar CACHE en todos los cines');
        } else {
          $options = array(
              'type' => 'method',
              'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_CINEMA_SINGLE,
              'cinema_id' => $data['cinema']
          );
          $this->addToLog('Borrar CACHE en cine '.$data['cinema']);
        }
        $cache_purger->enqueue($options);
      }

      // Purge cinemas tickets and sessions.
      if ($data['cinemaTickets'] > 0) {
        $cache_purger->enqueue(array(
            'type' => 'method',
            'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_CINEMA_TICKETS,
            'cinema_id' => $data['cinemaTickets']
        ));
        $cache_purger->enqueue(array(
            'type' => 'method',
            'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_CINEMA_SESSIONS,
            'cinema_id' => $data['cinemaTickets']
        ));
        $this->addToLog('Borrar CACHE de boletos en cine '.$data['cinema']);
      }

      // Purge dates by area cache.
      if ($data['dates']) {
        $options = array(
            'type' => 'method',
            'method' => CachePurger::PURGE_AVAILABLE_DATES,
        );
        $cache_purger->enqueue($options);
        $this->addToLog('Borrar CACHE de fechas');
      }

      // Purge home cache.
      if ($data['home']) {
        $options = array(
            'type' => 'method',
            'method' => CachePurger::PURGE_HOME_BILLBOARD,
        );
        $cache_purger->enqueue($options);
        $this->addToLog('Borrar CACHE en Home');
      }

      // Purge Instagram/Twitter cache.
      if ($data['social']) {
        $options = array(
            'type' => 'urls',
            'urls' => $this->generateUrl('esi_social_footer')
        );
        $cache_purger->enqueue($options);
        $this->addToLog('Borrar CACHE en SocialFooter');
      }

      $purge = $cache_purger->purge_cache();
    } elseif ($_purge = $this->getRequest()->getSession()->getFlashBag()->get('purge')) {
      $purge = $_purge;
    }

    return $this->render('SocialSnackAdminBundle:Cache:index.html.php', array(
        'form'   => $form->createView(),
        'purge'  => $purge,
    ));
  }


  /**
   * @Route("/cache_purge/front/all", name="social_snack_cache_purge_front_all")
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function purgeFrontAllAction() {
    set_time_limit(60 * 5);
    $purger = $this->get('ss.cache_purger');
    $purge = $purger->purge_cache(array(
        'type' => 'method',
        'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_FRONT_ALL,
    ));
    $this->getRequest()->getSession()->getFlashBag()->set('purge', $purge);
    $this->addToLog('Borrar CACHE completo');
    return $this->redirect($this->generateUrl('social_snack_cache_purge'));
  }

  public function addToLog($message){
    $logger = $this->get('logger');
    $logger->notice('LOG ==> '.$message.' ejecutado por el usuario '. $this->getUser()->getUsername().' a las '.date('d/m/Y h:i:s a'));
  }


  /**
   * @Route("/cache_purge/doctrine", name="social_snack_cache_doctrine")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function purgeDoctrine(Request $request) {
    $fb = $this->createFormBuilder();
    $fb
        ->add('id', 'text', ['label' => 'ID'])
        ->add('submit', 'submit', ['label' => 'Borrar'])
    ;

    $form = $fb->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $purger = $this->get('ss.cache_purger');
      $purge = $purger->purge_cache(array(
          'type' => 'doctrine',
          'cache_ids' => [$form->get('id')->getData()]
      ));
      $this->getRequest()->getSession()->getFlashBag()->set('purge', $purge);
      $this->addToLog('Borrar CACHE de Doctrine');
      return $this->redirect($this->generateUrl('social_snack_cache_doctrine'));
    }

    return $this->render('SocialSnackAdminBundle:Cache:doctrine.html.php', [
        'form' => $form->createView(),
        'purge' => $this->getRequest()->getSession()->getFlashBag()->get('purge'),
    ]);
  }

}