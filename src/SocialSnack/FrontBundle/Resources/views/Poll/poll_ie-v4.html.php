<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Encuesta Invitado Especial</title>

    <link href="//cdn.jsdelivr.net/normalize/3.0.3/normalize.min.css" rel="stylesheet">

<style>
@font-face {
  font-family:'din-pro';
  src:url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Light.eot'); ?>?#iefix') format('embedded-opentype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Light.otf'); ?>') format('opentype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Light.woff'); ?>') format('woff'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Light.ttf'); ?>') format('truetype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Light.svg'); ?>#DINPro-Light') format('svg');
  font-weight:200;
  font-style:normal;
}

@font-face {
  font-family:'din-pro';
  src:url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Bold.eot'); ?>?#iefix') format('embedded-opentype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Bold.otf'); ?>') format('opentype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Bold.woff'); ?>') format('woff'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Bold.ttf'); ?>') format('truetype'),
      url('<?php echo $view['assets']->getUrl('assets/fonts/DINPro-Bold.svg'); ?>#DINPro-Light') format('svg');
  font-weight:700;
  font-style:normal;
}

*,
*:before,
*:after {
  box-sizing:inherit;
}

html {
  box-sizing:border-box;
  font-size:62.5%;
  height:100%;
}

body {
  background:#581c23 url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-bg.jpg'); ?>) no-repeat 50% 50%;
  background-size:cover;
  color:#d41a4b;
  font-family:din-pro, Helvetica, sans-serif;
  font-size:2rem;
  font-weight:200;
  height:100%;
  line-height:1.2;
  text-align:center;
  -webkit-font-smoothing:antialiased;
}

h1,
p,
ul {
  margin:0 0 1em;
}

a {
  text-decoration:none;
}

a:focus {
  outline:none;
}

ul {
  list-style:none;
  padding:0;
}

img {
  display:inline-block;
}

.btn {
  background:#d41a4b;
  border-radius:4px;
  color:#fff;
  display:inline-block;
  font-size:2rem;
  padding:2rem 6rem;
  text-decoration:none;
}

.btn:hover {
  background-color:#941234;
}

.btn__img {
  vertical-align:middle;
}

.btn--prev,
.btn--prev:hover,
.btn--next,
.btn--next:hover {
  background:none;
  font-size:1.5rem;
  padding:0;
}

.btn--prev span,
.btn--next span {
  display:none;
}

.btn--prev {
  float:left;
}

.btn--prev .btn__img {
  margin-right:.5rem;
}

.btn--next {
  float:right;
}

.btn--next .btn__img {
  margin-left:.5rem;
}

.nav__pages {
  display:none;
  margin:0;
  height:3rem;
  padding:.7rem;
}

.nav__page_item {
  display:inline-block;
}

.nav__page_link {
  background:#9c0728;
  border:.4rem solid #d41a4b;
  border-radius:50%;
  display:inline-block;
  height:1.6rem;
  text-indent:-9999em;
  width:1.6rem;
}

.nav__page_link--active {
  background:#fff;
  border-color:#fff;
}

.nav__steps {
  color:#570017;
  font-size:1.5rem;
  font-weight:700;
  margin-bottom:0;
  line-height:3.6rem;
}

.nav__steps span {
  color:#fff;
}

.centered {
  margin:0 auto;
  position:relative;
}

.logo {
  margin:0 auto 3rem;
  width:15rem;
}

.item {
  display:table;
  height:100%;
  margin:0 auto;
  min-height:100%;
  padding:3% 0;
  position:relative;
  transform-style:preserve-3d;
  vertical-align:middle;
}

.item__inner {
  border-radius:3px;
  box-shadow:0 3px 10px rgba(0, 0, 0, .7);
  display:table-cell;
  height:88%;
  min-height:88%;
  width:88vw;
}

.item__content {
  background:#d8d8da url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-pattern-gray.jpg'); ?>) no-repeat 50% 50%;
  background-size:cover;
  border-radius:3px 3px 0 0;
  display:table;
  height:86%;
  padding:2.5rem;
  transform-style:preserve-3d;
  vertical-align:middle;
  width:100%;
}

.item__content_inner {
  display:table-cell;
  vertical-align:middle;
}

.item__nav {
  background:#d41a4b;
  border-radius:0 0 3px 3px;
  color:#fff;
  display:table;
  height:14%;
  line-height:3rem;
  padding:1.5rem 2.5rem;
  vertical-align:middle;
  width:100%;
}

.item__nav_inner {
  display:table-cell;
  vertical-align:middle;
}

.item__help {
  background:#d41a4b;
  border:1px solid #fff;
  border-radius:.3rem;
  color:#fff;
  font-size:1.1rem;
  margin:2rem 0 0;
  padding:1rem;
  text-transform:uppercase;
}

.item--home .item__content,
.item--thanks .item__content {
  background:transparent;
  color:#fff;
  height:100%;
  text-align:left;
}

.item--home {
  overflow:hidden;
}

.item--home .item__inner {
  box-shadow:none;
  padding:0 6%;
}

.item__title {
  font-size:4rem;
  font-weight:700;
  margin-bottom:.4em;
  line-height:1.1;
}

.item--home .nav__pages {
  display:block;
  margin-top:3em;
  padding:0;
}

.item--thanks .item__inner {
  box-shadow:none;
  height:100%;
}

.item--thanks .item__content {
  background:transparent;
  height:100%;
}

.answers {
  font-size:2rem;
  margin:0 -1rem;
}

.answers:before,
.answers:after {
  content:" ";
  display:table;
}

.answers:after {
  clear:both;
}

.answers__item {
  float:left;
  padding:.5rem 1rem;
  width:20%;
}

.answers__input {
  opacity:0;
  position:absolute;
  visibility:hidden;
}

.answers__label {
  border:1px solid #5e5e5e;
  border-radius:50%;
  display:block;
  height:4.4rem;
  margin:0 auto;
  line-height:4rem;
  position:relative;
  width:4.4rem;
}

.answers__label:after,
.answers__text:before,
.answers__text:after,
.answers__input {
  left:0;
  position:absolute;
  top:50%;
}

.answers__label--active {
  background:#d41a4b;
  border-color:#fff;
  color:#fff;
  font-weight:700;
}

.answer__tooltip {
  display:none;
}

@media (min-width: 992px) {
  body {
    font-size:2.5rem;
    line-height:1.5;
  }

  h1, p, ul {
    margin-bottom:1.5em;
  }

  .centered {
    transform:translate3d(0, -50%, 0);
    top:50%;
  }

  .logo {
    margin:-3em auto 2.5em;
    width:240px;
  }

  .answers {
    font-size:1.5rem;
    margin:0 auto;
    max-width:50%;
  }

  .answers__item {
    padding:1rem;
    position:relative;
    width:10%;
  }

  .answers__label:before,
  .answers__label:after,
  .answers__text:before {
    display:none;
  }

  .answers__label {
    border:0;
    height:auto;
    line-height:1;
    width:100%;
  }

  .answers__status {
    border:1px solid #5e5e5e;
    border-radius:50%;
    cursor:pointer;
    display:block;
    height:3.3rem;
    margin:0 auto 1.5rem;
    width:3.3rem;
  }

  .answers__text {
    border:none;
    color:#555;
    display:block;
    font-weight:200;
    position:static;
    text-indent:0;
  }

  .answers__label--active {
    background:none;
  }

  .answers__label--active .answers__text {
    background:transparent;
    color:#555;
    font-weight:200;
  }

  .answers__label--active .answers__status {
    background:#d41a4b;
    border-color:#fff;
  }

  .answer__tooltip {
    background:#d41a4b;
    border:1px solid #fff;
    color:#fff;
    display:block;
    font-size:1rem;
    font-weight:700;
    padding:.8rem 1.5rem;
    position:absolute;
    text-transform:uppercase;
    top:.3rem;
    width:11rem;
  }

  .answer__tooltip--left {
    left:-12rem;
  }

  .answer__tooltip--right {
    right:-12rem;
  }

  .point__a,
  .point__b {
    background:#d41a4b;
    border:.1rem solid #fff;
    content:'';
    height:1.5rem;
    position:absolute;
    transform:rotate(45deg);
    top:1.5rem;
    width:1.5rem;
    z-index:-1;
  }

  .point__a  {
    border-color:#d41a4b;
    z-index:1;
  }

  .answer__tooltip--left .point__a,
  .answer__tooltip--left .point__b {
    left:10rem;
  }

  .answer__tooltip--left .point__a {
    left:9.84rem;
  }

  .answer__tooltip--right .point__a,
  .answer__tooltip--right .point__b {
    right:10rem;
  }

  .answer__tooltip--right .point__a {
    right:9.84rem;
  }

  .nav__pages {
    display:block;
  }

  .btn--prev span,
  .btn--next span {
    display:inline;
  }

  .item__help {
    display:none;
  }

  .item__title {
    font-size:5.6rem;
    font-weight:700;
    margin-bottom:.4em;
    line-height:1.1;
  }

  .item__graphs {
    height:100%;
    position:absolute;
    right:-8.10536%;
    top:0;
    width:51.40625%;
  }

  .item__graphs:before {
    background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-graphs.png'); ?>) no-repeat 50% 50%;
    background-size:cover;
    content:'';
    left:0;
    position:absolute;
    padding-top:51.36778%;
    transform:translate(0, -50%);
    top:50%;
    width:100%;
  }

  .item--thanks .item__graphs {
    right:3rem;
    width:60%;
  }

  .nav__steps {
    display:none;
  }

  .item__content {
    height:82%;
  }

  .item__nav {
    height:18%;
  }

  .pt-page-current .answers__label,
  .pt-page-current .answers__status {
    transition:none;
  }
}

@media (max-width: 991px) {
  .answers__label,
  .answers__status {
    transition:all .75s;
  }
}

/*****************
 * ANIMATIONS
 *****************/
@-webkit-keyframes scaleDown {
  from { }
  to { opacity: 0; -webkit-transform: scale(.8); }
}
@keyframes scaleDown {
  from { }
  to { opacity: 0; -webkit-transform: scale(.8); transform: scale(.8); }
}

@-webkit-keyframes scaleUp {
  from { opacity: 0; -webkit-transform: scale(.8); }
}
@keyframes scaleUp {
  from { opacity: 0; -webkit-transform: scale(.8); transform: scale(.8); }
}

@-webkit-keyframes moveFromRight {
  from { -webkit-transform: translateX(100%); }
}
@keyframes moveFromRight {
  from { -webkit-transform: translateX(100%); transform: translateX(100%); }
}

@-webkit-keyframes moveToRight {
  from { }
  to { -webkit-transform: translateX(100%); }
}
@keyframes moveToRight {
  from { }
  to { -webkit-transform: translateX(100%); transform: translateX(100%); }
}

.pt-perspective {
  position: relative;
  width: 100%;
  height: 100%;
  perspective: 1200px;
  transform-style: preserve-3d;
}

.pt-page {
  /*display:none;*/
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  visibility: hidden;
  overflow: auto;
  backface-visibility: hidden;
  transform: translate3d(0, 0, 0);
}

.pt-page-current,
.no-js .pt-page {
  visibility: visible;
}

.no-js body {
  overflow: auto;
}

.pt-page-ontop {
  z-index: 999;
}

.pt-page-scaleDown {
  -webkit-animation:scaleDown .7s ease both;
  animation:scaleDown .7s ease both;
}

.pt-page-moveFromRight {
  -webkit-animation:moveFromRight .6s ease both;
  animation:moveFromRight .6s ease both;
}

.pt-page-moveToRight {
	-webkit-animation: moveToRight .6s ease both;
	animation: moveToRight .6s ease both;
}

.pt-page-scaleUp {
	-webkit-animation: scaleUp .7s ease both;
	animation: scaleUp .7s ease both;
}

.show {
  display:block;
}

.pt-page-1 .btn--prev {
  display:none;
}

.centered__wrapper {
  display:table;
  vertical-align:middle;
}

.centered__item {
  display:table-cell;
}
</style>
  </head>
  <body>
    <div class="pt-perspective">
<?php /* ?>
      <div class="pt-page pt-page-0">
        <div class="item item--home">
          <div class="item__inner">
            <div class="item__content">
              <div class="item__content_inner">
                <img class="logo" src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-logo.png'); ?>" alt="Cinemex">

                <h1 class="item__title">Cuéntanos tu experiencia<br>
                  en Cinemex</h1>

                <p>Ayúdanos a superar tus expectativas con base<br> en tu última visita.</p>

                <p class="nav__pages"><a class="btn" href="#" data-nav="next">Comenzar</a></p>
              </div>
            </div>
          </div>

          <div class="item__graphs"></div>
        </div>
      </div>
<?php //*/ ?>

      <form data-poll-form>
      <?php
      $qlen = count($questions) - 1;

      foreach ($questions as $qid => $question) :
      ?>
      <div class="pt-page pt-page-<?php echo ($qid + 1); ?>">
        <div class="item">
          <div class="item__inner">
            <div class="item__content">
              <div class="item__content_inner">
                <img class="logo" src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-logo-black.png'); ?>" alt="Cinemex" width="166px">

                <p><?php echo $question['text']; ?></p>

                <ul class="answers">
                  <?php for ($i = 1; $i <= 10; ++$i) : ?>
                  <li class="answers__item">
                    <?php if ($i === 1) : ?>
                    <p class="answer__tooltip answer__tooltip--left">
                      <i class="point__a"></i><i class="point__b"></i>
                      <?php if ($qid === 0) : ?>Nada probable<? else : ?>Nada satisfecho<?php endif; ?>
                    </p>
                    <?php endif; ?>

                    <?php if ($i === 10) : ?>
                    <p class="answer__tooltip answer__tooltip--right">
                      <i class="point__a"></i><i class="point__b"></i>
                      <?php if ($qid === 0) : ?>Totalmente probable<? else : ?>Totalmente satisfecho<?php endif; ?>
                    </p>
                    <?php endif; ?>

                    <label class="answers__label">
                      <input class="answers__input" type="radio" name="question_<?php echo ($qid + 1); ?>" value="<?php echo $i; ?>">
                      <span class="answers__status"></span>
                      <span class="answers__text" data-answer="<?php echo $i; ?>"><?php echo $i; ?></span>
                    </label>
                  </li>
                  <?php endfor; ?>
                </ul>

                <?php if ($qid === 0) : ?>
                  <p class="item__help">Califica del <strong>1 (nada probable)</strong> al <strong>10 (altamente probable)</strong></p>
                <?php else : ?>
                  <p class="item__help">Califica del <strong>1 (nada satisfecho)</strong> al <strong>10 (altamente satisfecho)</strong></p>
                <?php endif; ?>
              </div>
            </div>

            <div class="item__nav">
              <div class="item__nav_inner">
                <a class="btn btn--prev" href="#" data-nav="prev">
                  <img class="btn__img" src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-arr-left.png'); ?>"> <span>Anterior</span></a>

                <a class="btn btn--next" href="#" data-nav="next" <?php if ($qid === $qlen) : ?>data-submit<?php endif; ?>>
                  <span>Siguiente</span> <img class="btn__img" src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-arr-right.png'); ?>"></a>

                <ul class="nav__pages">
                  <?php foreach ($questions as $pid => $q) : ?>
                  <li class="nav__page_item"><a class="nav__page_link <?php if ($pid === $qid) : ?>nav__page_link--active<?php endif; ?>" href="#">Pregunta <?php echo ($pid + 1); ?></a></li>
                  <?php endforeach; ?>
                </ul>

                <p class="nav__steps"><span><?php echo ($qid + 1); ?></span> de <?php echo count($questions); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      </form>

      <div class="pt-page pt-page-<?php echo ($qid + 1); ?>">
        <div class="item item--thanks">
          <div class="item__inner">
            <div class="item__content">
              <div class="item__content_inner">
                <img class="logo" src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/v4-logo.png'); ?>" alt="Cinemex">

                <p class="item__title">¡Muchas gracias!</p>
                <p>Tu opinión es lo más importante.</p>
              </div>
            </div>
          </div>

          <div class="item__graphs"></div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <?php
    foreach ($view['assetic']->javascripts(
      array(
        '@SocialSnackFrontBundle/Resources/public/vendor/modernizr/modernizr.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/hammerjs/hammer.min.js',
        '@SocialSnackFrontBundle/Resources/public/js/ie-poll-v4.js'
      ),
      array('?yui_js'),
      array('output' => 'js/compiled/iepollv4.js')
    ) as $url): ?>
      <script src="<?php echo $view->escape($url); ?>"></script>
    <?php endforeach; ?>

    <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  //ga('create', 'UA-24522461-23', 'auto');
  ga('create', 'UA-24522461-23', {
    'cookieDomain': 'none'
  });
    ga('require', 'displayfeatures');
  ga('send', 'pageview');
  </script>


<script type="text/javascript">
  var _sf_async_config = { uid: 52715, domain: 'cinemex.com', useCanonical: true };
  (function() {
    function loadChartbeat() {
      window._sf_endpt = (new Date()).getTime();
      var e = document.createElement('script');
      e.setAttribute('language', 'javascript');
      e.setAttribute('type', 'text/javascript');
      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
      document.body.appendChild(e);
    };
    var oldonload = window.onload;
    window.onload = (typeof window.onload != 'function') ?
        loadChartbeat : function() { oldonload(); loadChartbeat(); };
  })();
</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-JDF5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-JDF5');</script>
<!-- End Google Tag Manager -->
  </body>
</html>
