<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BuyTicketCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:buyticket' )
			->setDescription( 'Buy tickets for a movie' )
			->addOption( 'card',      null, InputOption::VALUE_REQUIRED )
			->addOption( 'expire',    null, InputOption::VALUE_REQUIRED )
			->addOption( 'cvs',       null, InputOption::VALUE_REQUIRED )
			->addOption( 'name',      null, InputOption::VALUE_REQUIRED )
			->addOption( 'session',   null, InputOption::VALUE_REQUIRED )
			->addOption( 'cinema',    null, InputOption::VALUE_REQUIRED );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$req          = $this->getContainer()->get( 'cinemex_ws' );
		
		$output->writeln( sprintf( 'Nombre: %s',    $input->getOption( 'name' ) ) );
		$output->writeln( sprintf( 'CVS: %s',       $input->getOption( 'cvs' ) ) );
		$output->writeln( sprintf( 'Fecha exp: %s', $input->getOption( 'expire' ) ) );
		$output->writeln( sprintf( 'Tarjeta: %s',   $input->getOption( 'card' ) ) );
		$output->writeln( sprintf( 'Sesión: %s',    $input->getOption( 'session' ) ) );
		$output->writeln( sprintf( 'Cine: %s',      $input->getOption( 'cinema' ) ) );
		
//		return;
		$req->init( 'Compra' );
		$res = $req->request( array(
				'CodigoCine'        => $input->getOption( 'cinema' ),
				'Session'           => $input->getOption( 'session' ),
				'CadenaBoletos'     => '|1|0001|1|7100|', // " | N | ticketcode | cantidad | precio | ticketcode | cantidad | precio | …"
				'CadenaPlatino'     => '|1|0000000001|2|1|34|',
				'TDC'               => $input->getOption( 'card' ),
				'Vigencia'          => $input->getOption( 'expire' ),
				'DV'                => $input->getOption( 'cvs' ),
				'NombreCliente'     => $input->getOption( 'name' ),
		) );
		
		$output->writeln( 'Didn\'t fail, yay!' );
		return;
	}
	
}