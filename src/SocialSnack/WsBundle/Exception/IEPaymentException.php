<?php

namespace SocialSnack\WsBundle\Exception;

/**
 * Class IEPaymentException
 * @package SocialSnack\WsBundle\Exception
 * @author Guido Kritz
 */
class IEPaymentException extends RequestMsgException {

} 