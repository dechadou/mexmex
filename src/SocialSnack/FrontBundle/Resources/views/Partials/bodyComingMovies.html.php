<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$formatter = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC');
$formatter->setPattern('d MMM');
?>
<div class="row xs-clear-on-3 sm-clear-on-<?php echo ($movies_cols - 1); ?> md-clear-on-<?php echo ($movies_cols); ?> clearfix movies-grid upcoming-movies">
<?php /* @var $movie \SocialSnack\WsBundle\Entity\Movie */ ?>

<?php
foreach ($movies as $movie) :
  $attributes = array();
  $data       = array();

  if ('DIGITAL 3D' == $movie->getData('FORMATOVIDEO')) {
    $attributes[] = '3d';
  }

  foreach ($attributes as $attr) {
    $data[] = 'data-' . $attr;
  }

  $movie_url = $view['fronthelper']->get_movie_link($movie, 'moviecoming_single');

  $release_date = $movie->getReleaseDate();
  $trailer      = $movie->getData('TRAILER');
?>
  <div class="col col-xs-1-3 col-sm-1-<?php echo ($movies_cols - 1); ?> col-md-1-<?php echo $movies_cols; ?> movies-grid-item" <?php echo implode( ' ', $data ); ?>>
    <a href="<?php echo $movie_url; ?>" class="movies-grid-cover">
      <img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '154x230'); ?>" alt="<?php echo $movie->getName(); ?>" />
    </a>

    <a href="<?php echo $movie_url; ?>"  class="movies-grid-title"><?php echo $movie->getName(); ?></a>

    <?php
    echo $this->render('SocialSnackFrontBundle:Partials:movieRibbons.html.php', array(
      'movie' => $movie
    ));
    ?>

    <?php if (isset($is_friday) AND $is_friday === FALSE AND $release_date instanceof DateTime) : ?>
    <a href="<?php echo $movie_url; ?>" class="movie-poster-bt buy-tickets-bt">Estreno<br> <?php echo $formatter->format($movie->getReleaseDate()); ?></a>
    <?php else : ?>
    <a href="<?php echo $movie_url; ?>" class="movie-poster-bt buy-tickets-bt">Más<br/>datos</a>
    <?php endif; ?>

    <?php if (gettype($trailer) === 'string' AND !empty($trailer)) : ?>
		<a href="#" data-youtube-id="<?php echo FrontHelper::get_youtube_id_from_url($trailer); ?>" class="movie-poster-bt watch-trailer-bt">Ver<br/>tráiler</a>
    <?php endif; ?>
	</div>
<?php endforeach; ?>
</div>
