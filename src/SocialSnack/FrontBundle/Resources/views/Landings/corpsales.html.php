<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>

<?php
$view['slots']->start('body');

$menu = array(
  array(
    'label' => 'Ventas corporativas',
    'route' => array('corpsales_index'),
    'class' => 'icon-cinemex',
  ),
  array(
    'label' => 'Preguntas frecuentes',
    'route' => array('corpsales_faq'),
    'class' => 'icon-file',
  ),
  array(
    'label' => 'Informes y contacto',
    'route' => array('corpsales_form'),
    'class' => 'icon-file',
  )
);
?>

<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('promos/corpsales-banner.png', 'CMS'); ?>">
</div>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <ul class="panel-menu">
      <?php
      foreach ($menu as $item) :
        $current = (in_array($route, (array) $item['route']));
      ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>

  <div class="wide-main-col col">
    <article class="entry-full">
      <?php $view['slots']->output('corpsales_body'); ?>
    </article>
  </div>
</div>
<?php $view['slots']->stop(); ?>
