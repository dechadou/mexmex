<?php

namespace SocialSnack\RestBundle\Tests\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\RestBundle\Handler\BuyHandler;
use SocialSnack\RestBundle\Service\Utils as RestUtils;
use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Entity\Ticket;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use SocialSnack\WsBundle\Tests\Request\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class BuyHandlerTest
 * @package SocialSnack\RestBundle\Tests\Handler
 * @author Guido Kritz
 */
class BuyHandlerTest extends WebTestCase {

  protected function get_ws_method() {
    return 'DetalleSession';
  }


  /**
   * @param ContainerInterface $container
   * @param Registry           $doctrine
   * @param WsRequest          $ws
   * @param \Memcached         $memcached
   * @param RestUtils          $rest_utils
   * @param int                $timeout_time
   * @param bool               $simulated_purchase
   * @return BuyHandlerTesteable
   */
  protected function getHandler(ContainerInterface $container = NULL, Registry $doctrine = NULL, WsRequest $ws = NULL, \Memcached $memcached = NULL, RestUtils $rest_utils = NULL, $timeout_time = 7, $simulated_purchase = FALSE) {
    $reflector = new \ReflectionClass('\SocialSnack\RestBundle\Tests\Handler\BuyHandlerTesteable');
    return $reflector->newInstanceArgs($this->getHandlerArgs($container, $doctrine, $ws, $memcached, $rest_utils, $timeout_time, $simulated_purchase));
  }


  protected function getHandlerMock(array $methods = NULL, $call_constructor = TRUE, $constructor_args = NULL) {
    $handler = $this->getMockBuilder('\SocialSnack\RestBundle\Tests\Handler\BuyHandlerTesteable')
        ->setMethods($methods);

    if (is_null($constructor_args)) {
      $constructor_args = $this->getHandlerArgs();
    }

    if (!$call_constructor) {
      $handler->disableOriginalConstructor();
    } else {
      $handler->setConstructorArgs($constructor_args);
    }

    return $handler->getMock();
  }

  protected function getHandlerArgs(ContainerInterface $container = NULL, Registry $doctrine = NULL, WsRequest $ws = NULL, \Memcached $memcached = NULL, RestUtils $rest_utils = NULL, $timeout_time = 7, $simulated_purchase = FALSE) {
    return [
        $container  ?: $this->getContainer(),
        $doctrine   ?: $this->getContainer()->get('doctrine'),
        $ws         ?: $this->getContainer()->get('cinemex_ws'),
        $memcached  ?: $this->getContainer()->get('memcached'),
        $rest_utils ?: $this->getContainer()->get('ss.rest.utils'),
        $timeout_time,
        $simulated_purchase
    ];
  }


  public function testSelectTickets() {
//    $req = new Request();
//    $req->request->set('session_id', 123);
//    $req->request->set('tickets', ['foobar']);
//
//    $session = $this->getMock('\Symfony\Component\HttpFoundation\Session\Session');
//    $req->setSession($session);
//
//    $handler = $this->getHandlerMock([
//        'cleanupUserTransactions',
//        'validateAvailableSeatsForSession',
//        'getSeatsLayout',
//        'allocateSeats',
//        'freezeSeats',
//        'buildTmpTransactionId',
//    ]);
//
//    $handler->expects($this->once())
//        ->method('cleanupUserTransactions');
//
//    // @todo Insert Session somehow
//
//    $handler->expects($this->once())
//        ->method('validateAvailableSeatsForSession');
//
//    $handler->expects($this->once())
//        ->method('getSeatsLayout');
//
//    // @todo Mock allocateSeats response
//    $handler->expects($this->once())
//        ->method('allocateSeats')
//        ->willReturn([]);
//
//    $handler->expects($this->once())
//        ->method('freezeSeats')
//        ->willReturn([]);
//
//    $handler->expects($this->once())
//        ->method('buildTmpTransactionId');
//
//    // Expect Memcached::add
//
//    $output = $handler->select_tickets($req);
//    $this->assertInternalType('array', $output);
//    $this->assertObjectHasAttribute('transaction_id', $output);
//    $this->assertObjectHasAttribute('timeout',        $output);
//    $this->assertObjectHasAttribute('timeout_time',   $output);
//    $this->assertObjectHasAttribute('layout',         $output);
//    $this->assertObjectHasAttribute('seats',          $output);
//    $this->assertObjectHasAttribute('timestamp',      $output);
  }


  public function testSelectTicketsMissingSessionField() {
    $req = new Request();
    $req->request->set('tickets', '');
    $req->setSession($this->getContainer()->get('session'));

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\MissingFieldsException');

    $handler = $this->getHandler();
    $handler->select_tickets($req);
  }


  public function testSelectTicketsMissingTicketsField() {
    $req = new Request();
    $req->request->set('session_id', '');
    $req->setSession($this->getContainer()->get('session'));

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\MissingFieldsException');

    $handler = $this->getHandler();
    $handler->select_tickets($req);
  }


  public function testSelectTicketsNotAllowed() {
    $session = new Session();
    $session->setInfo(json_encode(['SEATALLOCATIONON' => 'N']));

    $req = new Request();
    $req->request->set('tickets', '');
    $req->request->set('session_id', '');
    $req->setSession($this->getContainer()->get('session'));

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\RestException');

    $handler = $this->getHandler();
    $handler->select_tickets($req);
  }


  public function testSelectSeats() {

  }


  public function testCompleteTransaction() {

  }


  public function testPerformPurchaseReal() {
    $handler = $this->getHandlerMock(
        ['simulatedPurchase', 'actualPurchase']
    );
    $handler->expects($this->never())
        ->method('simulatedPurchase');

    $handler->expects($this->once())
        ->method('actualPurchase')
        ->willReturn('foobar');

    $output = $handler->perform_purchase(new Session(), [], []);
    $this->assertEquals('foobar', $output);
  }


  public function testPerformPurchaseSimulated() {
    $handler = $this->getHandlerMock(
        ['simulatedPurchase', 'actualPurchase'],
        TRUE,
        $this->getHandlerArgs(NULL, NULL, NULL, NULL, NULL, NULL, TRUE)
    );
    $handler->expects($this->once())
        ->method('simulatedPurchase')
        ->willReturn('foobar');

    $handler->expects($this->never())
        ->method('actualPurchase');

    $output = $handler->perform_purchase(new Session(), [], []);
    $this->assertEquals('foobar', $output);
  }


  public function testPerformPurchaseException() {
    $handler = $this->getHandlerMock(
        ['simulatedPurchase', 'actualPurchase']
    );
    $handler->expects($this->never())
        ->method('simulatedPurchase');

    $handler->expects($this->once())
        ->method('actualPurchase')
        ->will($this->throwException(new \SocialSnack\WsBundle\Exception\RequestMsgException()));

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\RestException');
    $handler->perform_purchase(new Session(), [], []);
  }


  public function testPerformPurchaseIEPaymentException() {
    $handler = $this->getHandlerMock(
        ['simulatedPurchase', 'actualPurchase', 'retryWithoutIE']
    );
    $handler->expects($this->never())
        ->method('simulatedPurchase');

    $handler->expects($this->once())
        ->method('actualPurchase')
        ->will($this->throwException(new \SocialSnack\WsBundle\Exception\IEPaymentException()));

    $handler->expects($this->once())
        ->method('retryWithoutIE')
        ->willReturn('foobar');

    $output = $handler->perform_purchase(new Session(), [], []);
    $this->assertEquals('foobar', $output);
  }


  public function testRetryWithoutIEException() {
    $handler = $this->getHandler();

    $this->setExpectedException('\Exception');
    $handler->retryWithoutIE(new Session(), ['IE' => ''], []);
  }


  public function testRetryWithoutIETwiceException() {
    $handler = $this->getHandlerMock(
        ['actualPurchase']
    );

    $handler->expects($this->once())
        ->method('actualPurchase')
        ->will($this->throwException(new \SocialSnack\WsBundle\Exception\IEPaymentException()));

    $this->setExpectedException('\Exception');
    $handler->retryWithoutIE(new Session(), ['IE' => '1234567890123456'], []);
  }


  public function testRetryWithoutIE() {
    $handler = $this->getHandlerMock(
        ['perform_purchase']
    );

    $handler->expects($this->once())
        ->method('perform_purchase')
        ->willReturn('foobar');

    $output = $handler->retryWithoutIE(new Session(), ['IE' => '1234567890123456'], []);
    $this->assertEquals('foobar', $output);
  }


  public function testSimulatedPurchase() {
    // Not so much to test about this...
  }


  protected function _testActualPurchase($expected_method, Session $session, $transaction) {
    $ws = $this->getMockBuilder('\SocialSnack\WsBundle\Request\Request')
        ->disableOriginalConstructor()
        ->getMock();

    $ws->expects($this->once())
        ->method('init')
        ->with($expected_method);

    $ws->expects($this->once())
        ->method('request')
        ->willReturn('foobar');

    $handler = $this->getHandler(NULL, NULL, $ws);
    $output = $handler->actualPurchase($session, [], $transaction);
    $this->assertEquals('foobar', $output);
  }


  public function testActualPurchaseNoAllocation() {
    $session = new Session();
    $this->_testActualPurchase('Compra', $session, []);
  }


  public function testActualPurchaseWithAllocation() {
    $session = new Session();
    $session->setInfo(json_encode(['SEATALLOCATIONON' => 'Y']));
    $this->_testActualPurchase('TerminaCompra', $session, ['ws_trans_id' => 'foobar']);
  }


  public function testFreezeSeats() {
    $ws = $this->getMockBuilder('\SocialSnack\WsBundle\Request\Request')
        ->disableOriginalConstructor()
        ->getMock();

    $ws->expects($this->once())
        ->method('init')
        ->with('ApartaAsientos');

    $ws->expects($this->once())
        ->method('request')
        ->willReturn('foobar');

    // @todo Assert 'Opcion' passed to request()

    $cinema = new Cinema();
    $session = new Session();
    $session->setCinema($cinema);

    $handler = $this->getHandler(NULL, NULL, $ws);
    $output = $handler->freezeSeats($session, '', '');
    $this->assertInternalType('array', $output);
  }


  public function testReleaseSeats() {
    $ws = $this->getMockBuilder('\SocialSnack\WsBundle\Request\Request')
        ->disableOriginalConstructor()
        ->getMock();

    $ws->expects($this->once())
        ->method('init')
        ->with('ApartaAsientos');

    $ws->expects($this->once())
        ->method('request');

    // @todo Assert 'Opcion' passed to request()

    $cinema = new Cinema();
    $session = new Session();
    $session->setCinema($cinema);

    $handler = $this->getHandler(NULL, NULL, $ws);
    $output = $handler->releaseSeats($session, '', '', '');
  }


  public function testCleanupUserTransactions() {
    // Ensure the transaction is invalidated.
    $handler = $this->getHandlerMock(['invalidateTransaction']);
    $handler->expects($this->once())
        ->method('invalidateTransaction')
        ->with($this->equalTo('abc'));

    $session = $this->getMock('\Symfony\Component\HttpFoundation\Session\Session');
    $session->expects($this->once())
        ->method('get')
        ->with($this->equalTo('transaction_id'))
        ->willReturn('abc');

    // Ensure the transaction is removed from session.
    $session->expects($this->once())
        ->method('remove')
        ->with($this->equalTo('transaction_id'));

    $handler->cleanupUserTransactions($session);
  }


  public function testGetSeatsLayoutSuccess() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;CADENAASIENTOS&gt;&lt;LOCALIDAD&gt;CodCategoria:0000000001|NumCategoria:2|RowNumber:10|RowName:B|01,02:0|02,02:0|03,02:0|04,02:0|05,02:0|06,02:0|07,02:0|08,02:0|09,02:0|10,02:0|11,02:0|12,02:0|13,02:0|14,02:E|15,02:E|16,02:E|17,02:E|18,02:E|19,02:E|RowNumber:9|RowName:C|01,03:0|02,03:0|03,03:0|04,03:0|05,03:0|06,03:0|07,03:0|08,03:0|09,03:0|10,03:0|11,03:0|12,03:0|13,03:0|14,03:E|15,03:E|16,03:E|17,03:E|18,03:E|19,03:E|&lt;/LOCALIDAD&gt;&lt;/CADENAASIENTOS&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));

    $handler = $this->getHandler(NULL, NULL, self::$ws);
    $res = $handler->getSeatsLayout(LoadSessionData::$sessions[0]);
    $this->assertInternalType('array', $res);
    $this->assertArrayHasKey('layout',   $res);
    $this->assertArrayHasKey('cat_code', $res);
    $this->assertArrayHasKey('cat_num',  $res);
  }


  public function testGetSeatsLayoutWsFail() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/"></string>');

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));

    $handler = $this->getHandler(NULL, NULL, self::$ws);
    $res = $handler->getSeatsLayout(LoadSessionData::$sessions[0], array(), 'abc');
    $this->assertFalse($res);
  }


  public function testGetAvailableSeatsSuccess() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;DISPONIBLIDAD&gt;&lt;MENSAJE&gt;Asientos disponibles por este medio: 97&lt;/MENSAJE&gt;&lt;/DISPONIBLIDAD&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));

    $handler = $this->getHandler(NULL, NULL, self::$ws);
    $res = $handler->getAvailableSeats(LoadSessionData::$sessions[0]);
    $this->assertEquals(97, $res);
  }


  public function testGetAvailableSeatsWsFail() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="UTF-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;XMLFORMAT&gt;CMXERROR&lt;/XMLFORMAT&gt;&lt;FECHAHORA&gt;20131016163150&lt;/FECHAHORA&gt;&lt;ERRORDESC&gt;Ocurrio un error al obtener el detalle de la sesion&lt;/ERRORDESC&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));

    $handler = $this->getHandler(NULL, NULL, self::$ws);
    $res = $handler->getAvailableSeats(LoadSessionData::$sessions[0]);
    $this->assertFalse($res);
  }


  public function testParseSeatsLabelsSingle() {
    $res = new \stdClass();
    $res->CADENAASIENTOS = '19652|0|0||0000000002|1|0001|8|8|A|4|';
    $expected = ['A4'];

    $handler = $this->getHandler();

    $output = $handler->parse_seats_labels($res);

    $this->assertEquals($expected, $output);
  }


  public function testParseSeatsLabelsMultiple() {
    $res = new \stdClass();
    $res->CADENAASIENTOS = '74869|0|0||0000000001|2|0006|4|9|K|7|9|74870|0|0||0000000001|2|0006|4|10|K|8|9';
    $expected = ['K7', 'K8'];

    $handler = $this->getHandler();

    $output = $handler->parse_seats_labels($res);

    $this->assertEquals($expected, $output);
  }


  public function testGetTrackingCodes() {
    $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->getMock();
    $trackingHandler = $this->getMockBuilder('SocialSnack\RestBundle\Handler\TrackingCodeHandler')->disableOriginalConstructor()->getMock();
    $twig = $this->getMockBuilder('SocialSnack\FrontBundle\Service\TwigString')->disableOriginalConstructor()->getMock();

    // Mock the dependencies. @todo Inject dependencies directly instead of using the container.
    $container->method('get')
        ->will($this->returnValueMap([
            ['rest.trackingcode.handler', 1, $trackingHandler],
            ['twigstring', 1, $twig]
        ]));

    $session = $this->getMock('SocialSnack\WsBundle\Entity\Session');
    $session->method('getGroupId')->willReturn(666);
    $history = $this->getMock('SocialSnack\FrontBundle\Entity\Transaction');
    $history->method('getSession')->willReturn($session);

    $code1 = $this->getMock('SocialSnack\RestBundle\Entity\TrackingCode');
    $code1->method('getCode')->willReturn('foo');
    $code2 = $this->getMock('SocialSnack\RestBundle\Entity\TrackingCode');
    $code2->method('getCode')->willReturn('bar');

    $trackingHandler->expects($this->once())
        ->method('getGroupCodes')
        ->with(666)
        ->willReturn([$code1, $code2]);

    $twig->expects($this->exactly(2))
        ->method('render')
        ->with($this->anything(), $this->identicalTo(['transaction' => $history]))
        ->will($this->returnCallback(function($a, $b) {
          return strtoupper($a . $b['transaction']->getSession()->getGroupId());
        }));

    $handler = $this->getHandlerMock(
        NULL,
        TRUE,
        $this->getHandlerArgs($container)
    );

    $output = $handler->get_tracking_codes($history);

    $this->assertSame('FOO666BAR666', $output);
  }

  public function testBuildThankyouPage() {
    // @todo
//    $handler = $this->getMockBuilder('\SocialSnack\RestBundle\Handler\BuyHandler')
//        ->setMethods(['get_tracking_codes'])
//        ->setConstructorArgs($this->getHandlerArgs())
//        ->getMock();
//    $handler->expects($this->once())
//        ->method('get_tracking_codes')
//        ->willReturn('');
//
//    $transaction = $this->getMock('\SocialSnack\FrontBundle\Entity\Transaction');
//    $transaction->method('getId')->willReturn(1);
//    $transaction->method('getHash')->willReturn('abc');
//    $output = $handler->build_thankyou_page($transaction);
//    $this->assertInternalType('string', $output);
  }


  public function testInvalidateTransaction() {
    $trans = [
        'ws_trans_id' => 'bar',
        'session_id'  => 1,
        'seats_str'   => 'asdf',
        'tickets_str' => 'asdf',
    ];

    // Mock Memcached
    $mem = $this->getMockBuilder('\Memcached')
        ->setMethods(['get', 'getResultCode', 'delete'])
        ->disableOriginalConstructor()
        ->getMock();

    // Mock methods expected response.
    $mem->expects($this->once())
        ->method('getResultCode')
        ->willReturn(\Memcached::RES_SUCCESS);
    $mem->expects($this->once())
        ->method('get')
        ->willReturn($trans);

    // Ensure the transaction is deleted.
    $mem->expects($this->once())
        ->method('delete');

    // Mock Repository
    $repo = $this->getMockBuilder('\SocialSnack\WsBundle\Entity\SessionRepository')
        ->setMethods(['findWithCinema'])
        ->disableOriginalConstructor()
        ->getMock();
    $repo->expects($this->once())
        ->method('findWithCinema')
        ->willReturn(new Session());

    $handler = $this->getHandlerMock(
        ['releaseSeats', 'getRepository'],
        TRUE,
        $this->getHandlerArgs(NULL, NULL, NULL, $mem)
    );

    $handler->expects($this->once())
        ->method('getRepository')
        ->willReturn($repo);

    // Ensure seats are released.
    $handler->expects($this->once())
        ->method('releaseSeats');

    $handler->invalidateTransaction('foo');
  }


  protected function _testValidateAvailableSeatsForSession($available_seats) {
    $handler = $this->getHandlerMock(['getAvailableSeats'], FALSE);

    // Mock getAvailableSeats return value
    $handler->expects($this->once())
        ->method('getAvailableSeats')
        ->willReturn($available_seats);

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\RestException');

    $handler->validateAvailableSeatsForSession(new Session(), 5);
  }


  public function testValidateAvailableSeatsForSessionError() {
    $this->_testValidateAvailableSeatsForSession(FALSE);
  }


  public function testValidateAvailableSeatsForSessionSoldOut() {
    $this->_testValidateAvailableSeatsForSession(0);
  }


  public function testValidateAvailableSeatsForSessionNotEnough() {
    $this->_testValidateAvailableSeatsForSession(3);
  }


  public function testBuildTmpTransactionIdNew() {
    $mem = $this->getMockBuilder('\Memcached')
        ->disableOriginalConstructor()
        ->getMock();
    $mem->method('getResultCode')
        ->willReturn(\Memcached::RES_NOTFOUND);
    $mem->method('add')
        ->willReturn(TRUE);

    $handler = $this->getHandler(NULL, NULL, NULL, $mem);
    $trans_id = $handler->buildTmpTransactionId(new Session());

    $this->assertInternalType('string', $trans_id);
    $this->assertGreaterThan(0, strlen($trans_id));

    // @todo Test with Memcached CAS.
  }


  public function testParseReqTickets() {
    $session = new Session();
    $ticket1 = new Ticket();
    $ticket1
        ->setDescription('Ticket 1')
        ->setPrice(1000)
        ->setInfo(json_encode(['TICKETTYPECODE' => '0001']))
    ;
    $ticket2 = new Ticket();
    $ticket2
        ->setDescription('Ticket 2')
        ->setPrice(2000)
        ->setInfo(json_encode(['TICKETTYPECODE' => '0002']))
    ;
    $session->setTickets([$ticket1, $ticket2]);

    $handler = $this->getHandler();

    $req = [
        [
            'type' => '0001',
            'qty'  => 3
        ],
        [
            'type' => '0002',
            'qty'  => 4
        ],
    ];

    $expected = [
        [
            'name'  => 'Ticket 1',
            'type'  => '0001',
            'price' => 1000,
            'qty'   => 3
        ],
        [
            'name'  => 'Ticket 2',
            'type'  => '0002',
            'price' => 2000,
            'qty'   => 4
        ],
    ];

    $output = $handler->parseReqTickets($session, $req);

    $this->assertEquals($expected, $output);
  }


  public function testBuildSeatsString() {
    $handler = $this->getHandler();
    $seats = array(
        array(
            'row'  => 7,
            'seat' => 11
        ),
        array(
            'row'  => 7,
            'seat' => 12
        ),
    );

    $str = $handler->buildSeatsString($seats, '0000000002', 2);

    $this->assertEquals('|2|0000000002|2|7|11|0000000002|2|7|12|', $str);
  }


  public function testBuildTicketsString() {
    $tickets = array(
        array(
            'type'  => '0001',
            'qty'   => 2,
            'price' => 1100
        ),
        array(
            'type'  => '0002',
            'qty'   => 1,
            'price' => 800
        ),
    );

    $str = $this->getHandler()->buildTicketsString($tickets);

    // The string is copied from the documentation from Cinemex.
    $this->assertEquals('|2|0001|2|1100|0002|1|800|', $str);
  }


  public function testAllocateSeatsSuccess() {
    $seats_layout = array(
        '1' => array(
            'seats' => array(
                '01' => '0',
                '02' => '0',
                '03' => '0',
            )
        )
    );
    $output = $this->getHandler()->allocateSeats($seats_layout, 2);
    $this->assertInternalType('array', $output);
    $this->assertEquals(2, sizeof($output));
  }

  public function testAllocateSeatsSuccessSplit() {
    $seats_layout = array(
        '1' => array(
            'seats' => array(
                '01' => '0',
                '02' => '1',
                '03' => '1',
            )
        ),
        '2' => array(
            'seats' => array(
                '01' => '1',
                '02' => '0',
                '03' => '0',
            )
        ),
    );
    $output = $this->getHandler()->allocateSeats($seats_layout, 2);
    $this->assertInternalType('array', $output);
    $this->assertEquals(2, sizeof($output));
  }

  public function testAllocateSeatsFail() {
    $seats_layout = array(
        '1' => array(
            'seats' => array(
                '01' => '0',
                '02' => '1',
                '03' => '1',
            )
        ),
        '2' => array(
            'seats' => array(
                '01' => '1',
                '02' => '1',
                '03' => '0',
            )
        ),
    );
    $output = $this->getHandler()->allocateSeats($seats_layout, 3);
    $this->assertFalse($output);
  }

  public function testAllocateSeatsSuccessIntensive() {
    $seats_layout = [];
    $row = ['01' => '0'];
    for ($i = 2 ; $i < 5000 ; $i++) {
      $row[(string)str_pad($i, 2, '0', STR_PAD_LEFT)] = '1';
    }
    for ($i = 1 ; $i < 1000 ; $i++) {
      $seats_layout[(string)$i] = array(
          'seats' => $row
      );
    }
    $output = $this->getHandler()->allocateSeats($seats_layout, 10);
    $this->assertInternalType('array', $output);
    $this->assertEquals(10, sizeof($output));
  }


  public function testGetTmpTransactionSuccess() {
    // Mock Memcached to return "bar"
    $mem = $this->getMockBuilder('\Memcached')
        ->setMethods(['get', 'getResultCode'])
        ->disableOriginalConstructor()
        ->getMock();
    $mem->expects($this->once())
        ->method('getResultCode')
        ->willReturn(\Memcached::RES_SUCCESS);
    $mem->expects($this->once())
        ->method('get')
        ->willReturn('bar');

    $handler = $this->getHandler(NULL, NULL, NULL, $mem);
    $output  = $handler->getTmpTransaction('foo');

    $this->assertEquals('bar', $output);
  }


  public function testGetTmpTransactionFail() {
    // Mock Memcached to return NULL
    $mem = $this->getMockBuilder('\Memcached')
        ->setMethods(['get'])
        ->disableOriginalConstructor()
        ->getMock();
    $mem->expects($this->once())
        ->method('get')
        ->willReturn(NULL);

    $handler = $this->getHandler(NULL, NULL, NULL, $mem);

    $this->setExpectedException('\SocialSnack\RestBundle\Exception\RestException');

    $handler->getTmpTransaction('abc');
  }


  public function testParseTicketsQty() {
    $handler = $this->getHandler();
    $input = [
        [
            'qty' => 1,
        ],
        [
            'qty' => 2,
        ],
        [
            'qty' => 3,
        ],
    ];
    $output = $handler->parseTicketsQty($input);
    $this->assertEquals(6, $output);
  }


  public function testParseTotalAmount() {
    $handler = $this->getHandler();
    $input = [
        [
            'qty'   => 1,
            'price' => 100,
        ],
        [
            'qty'   => 2,
            'price' => 200,
        ],
        [
            'qty'   => 3,
            'price' => 300,
        ],
    ];
    $output = $handler->parseTotalAmount($input);
    $this->assertEquals(1400, $output);
  }

}


class BuyHandlerTesteable extends BuyHandler {


  public function perform_purchase(Session $session, array $args, $transaction) {
    return parent::perform_purchase($session, $args, $transaction);
  }


  public function retryWithoutIE(Session $session, array $args, $transaction) {
    return parent::retryWithoutIE($session, $args, $transaction);
  }


  public function simulatedPurchase() {
    return parent::simulatedPurchase();
  }


  public function actualPurchase(Session $session, array $args, $transaction) {
    return parent::actualPurchase($session, $args, $transaction);
  }


  public function freezeSeats(Session $session, $seats_str, $tickets_str, array $transaction = NULL, $release_seats = FALSE) {
    return parent::freezeSeats($session, $seats_str, $tickets_str, $transaction, $release_seats);
  }


  public function releaseSeats(Session $session, $seats_str, $tickets_str, $ws_trans_id) {
    return parent::releaseSeats($session, $seats_str, $tickets_str, $ws_trans_id);
  }


  public function getSeatsLayout(Session $session) {
    return parent::getSeatsLayout($session);
  }


  public function getAvailableSeats(Session $session) {
    return parent::getAvailableSeats($session);
  }


  public function parse_seats_labels($res) {
    return parent::parse_seats_labels($res);
  }


  public function get_tracking_codes(Transaction $history) {
    return parent::get_tracking_codes($history);
  }


  public function build_thankyou_page(Transaction $history) {
    return parent::build_thankyou_page($history);
  }


  public function invalidateTransaction($transaction_id) {
    return parent::invalidateTransaction($transaction_id);
  }


  public function validateAvailableSeatsForSession(Session $session, $req_seats) {
    return parent::validateAvailableSeatsForSession($session, $req_seats);
  }


  public function buildTmpTransactionId(Session $session) {
    return parent::buildTmpTransactionId($session);
  }


  public function parseReqTickets($session, $tickets) {
    return parent::parseReqTickets($session, $tickets);
  }


  public function buildSeatsString($seats, $cat_code, $cat_num) {
    return parent::buildSeatsString($seats, $cat_code, $cat_num);
  }


  public function buildTicketsString($tickets) {
    return parent::buildTicketsString($tickets);
  }


  public function allocateSeats($seats_layout, $req_seats) {
    return parent::allocateSeats($seats_layout, $req_seats);
  }


  public function getTmpTransaction($trans_id) {
    return parent::getTmpTransaction($trans_id);
  }


  public function parseTicketsQty($tickets) {
    return parent::parseTicketsQty($tickets);
  }


  public function parseTotalAmount($tickets) {
    return parent::parseTotalAmount($tickets);
  }

}