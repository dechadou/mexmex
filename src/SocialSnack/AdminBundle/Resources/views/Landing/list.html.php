<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de Landings - Cinemex');
$view['slots']->start('body')
?>

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Listado de Landings
        </header>
        <div class="panel-body">
          <div class="adv-table">
            <div class="clearfix">
              <div class="btn-group">
                <a class="btn btn-default" id="editable-sample_new" href="<?php echo $view['router']->generate('admin_landing_add'); ?>">
                  Nueva landing <i class="icon-plus"></i>
                </a>
              </div>
            </div>
            <table  class="display table table-bordered table-striped" id="example">
              <thead>
              <tr>
                <th>Nombre</th>
                <th>Activa</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($landings as $landing) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $landing->getName(); ?></td>
                  <td><?php echo $landing->getActive() ? 'Activo' : 'Inactivo'; ?></td>
                  <td><a href="<?php echo $view['router']->generate('admin_landing_edit', array('id' => $landing->getId())); ?>">Editar</a></td>
                </tr>
              <?php
              }
              ?>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#example').dataTable({
        "aaSorting": [[4, "desc"]]
      });
    });
  </script>
<?php $view['slots']->stop(); ?>