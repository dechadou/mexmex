<?php

namespace SocialSnack\RestBundle\Controller\v1;

use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;

use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use SocialSnack\RestBundle\Service\Helper as RestHelper;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class BuyController extends BaseController {


  protected function check_ip_block() {
    /* @var $rest_utils \SocialSnack\RestBundle\Service\Utils */
    $rest_utils = $this->get( 'ss.rest.utils' );
    $ip         = FrontHelper::get_user_ip();
    if ( !$rest_utils->is_ip_allowed_to_buy( $ip ) ) {
      $this->container->get('graphite')->publish('purchase.ip_blocked', 1);
      $res = new JsonResponse( array( 'success' => FALSE, 'error' => 'ip-blocked' ), 403 );
      $res->send();
      die();
    }
  }

	/**
	 * $_POST['session_id'] integer Required
	 * $_POST['tickets']    array   Required [{type:int, qty:int},{...},...]
	 *
	 * @todo Maybe the whole process should be blocking?
	 * @todo This step should be skipped if seat allocation is not allowed?
	 * @todo Make sure the session is in a future date and time.
	 * @todo Think how to handle this if seat allocation is not allowed.
	 *
	 * @Route("/selectTickets", name="rest_v1_buy_select_tickets")
	 * @Method({"POST"})
	 */
	public function selectTicketsAction(Request $request) {
    $this->check_ip_block();

    $data = $this->get('rest.buy.handler')->select_tickets($request);
    return new JsonResponse($data);
	}


	/**
	 * Select seats.
	 *
	 * The response includes a timestamp and the selected seat.
	 * The client should always store the received timestamp and make sure its
	 * higher than the last response it has received and dislpay the selected
	 * seats included in the response.
	 * This way you make sure that the user is always seeing the last stored
	 * transaction information and prevents issues from asynchronous responses and
	 * stuff.
	 *
	 * $_POST['session_id']     int    Required
	 * $_POST['transaction_id'] string Required
	 * $_POST['seats']          array  Required
	 *
	 * @Route("/selectSeats", name="rest_v1_buy_select_seats")
	 * @Method({"POST"})
	 */
	public function selectSeatsAction(Request $request) {
    $this->check_ip_block();

    $data = $this->get('rest.buy.handler')->select_seats($request);
    return new JsonResponse($data);
	}


	/**
	 * $_POST['session_id']     int    Required
	 * $_POST['transaction_id'] string Required
	 * $_POST['tickets']        array  Required
	 * $_POST['seats']          array  Required
	 * $_POST['email']          string Required
	 * $_POST['name']           string Required
	 * $_POST['cc']             string Required
	 * $_POST['csc']            string Required
	 * $_POST['expire-month']   string Required
	 * $_POST['expire-year']    string Required
	 *
	 * @Route("/complete", name="rest_v1_buy_complete")
	 * @Method({"POST"})
	 */
	public function completeAction(Request $request) {
    $this->get('ss.cineplus_blocker')->handleRequest($request);

    // This is an ugly hack to fix a bad implementation of the iOS apps.
    // @todo Remove this ASAP!
    $res = $this->forward('SocialSnackRestBundle:v1/Buy:selectSeats');

    $this->authorize();

    $this->check_ip_block();

    $data = $this->get('rest.buy.handler')->complete_transaction($request, $this->user);
    return new JsonResponse($data);
	}


  /**
   * @Route("/invalidateTransaction", name="rest_v1_buy_invalidate")
   * @Method({"POST"})
   */
  public function invalidateTransactionAction(Request $request) {
    $this->get('rest.buy.handler')->invalidateTransaction(
        $request->request->get('transaction_id'),
        $request->request->get('session_id')
    );
    return new JsonResponse();
  }

}
