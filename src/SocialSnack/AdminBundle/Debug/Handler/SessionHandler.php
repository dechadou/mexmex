<?php

namespace SocialSnack\AdminBundle\Debug\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\AdminBundle\Debug\Result\SessionResult;
use SocialSnack\RestBundle\Handler\BuyHandler;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Observer\WsDebugger;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Request\DetalleSession;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session as HttpSession;

/**
 * Class SessionHandler
 * @package SocialSnack\AdminBundle\Debug\Handler
 * @author Guido Kritz
 */
class SessionHandler {

  protected $doctrine;

  protected $ws;

  protected $buy_handler;

  protected $http_session;

  protected $ws_observer;


  public function __construct(Registry $doctrine, WsRequest $ws, WsDebugger $ws_observer, BuyHandler $buy_handler, HttpSession $http_session) {
    $this->doctrine     = $doctrine;
    $this->ws           = $ws;
    $this->ws_observer  = $ws_observer;
    $this->buy_handler  = $buy_handler;
    $this->http_session = $http_session;
  }


  /**
   * @return \SocialSnack\WsBundle\Entity\SessionRepository
   */
  protected function getSessionRepository() {
    return $this->doctrine->getRepository('SocialSnackWsBundle:Session');
  }


  /**
   * @param int $session_id
   * @return bool|SessionResult
   */
  public function debug($session_id) {
    $this->ws->addObserver($this->ws_observer);
    $this->ws_observer->resetData();

    $sess_repo  = $this->getSessionRepository();
    $session    = $sess_repo->findWithCinemaAndTickets($session_id);

    if (!$session) {
      return FALSE;
    }

    $this->ws->setTimeout(15);

    $local = $this->parseLocalSession($session);
    $ws    = $this->parseWsSession($session);

    if ($ws) {
      $seats = $this->querySeats($session);
    } else {
      $seats = NULL;
    }

    if (WsHelper::session_allows_seat_allocation($session) && $seats && $seats['seats'] && $ws['tickets']) {
      $select_tickets = $this->selectTickets($session, $ws['tickets']);
    } else {
      $select_tickets = NULL;
    }

    if ($select_tickets) {
      $release_tickets = $this->releaseTickets();
    } else {
      $release_tickets = NULL;
    }

    $data = [
        'local'           => $local,
        'ws'              => $ws,
        'seats'           => $seats,
        'select_tickets'  => $select_tickets,
        'release_tickets' => $release_tickets,
    ];

    $result = new SessionResult($data, $this->ws_observer->getData());

    return $result;
  }


  protected function parseLocalSession(Session $session) {
    $tickets = [];
    foreach ($session->getTickets() as $ticket) {
      $tickets[$ticket->getData('TICKETTYPECODE')] = [
          'name'  => $ticket->getDescription(),
          'price' => $ticket->getPrice(),
      ];
    }

    ksort($tickets);

    return [
        'pricecode' => $session->getData('PRICECODE'),
        'tickets'   => $tickets,
        'active'    => $session->getActive(),
    ];
  }


  protected function parseWsSession(Session $session) {
    $ws = $this->ws;
    $ws->init('Consulta');
    $ws_sessions_res = $ws->request([
        'TipoConsulta' => Consulta::TIPOCONSULTA_SESIONES,
        'CodigoCine'   => $session->getCinema()->getLegacyId()
    ]);

    $ws_session = FALSE;
    foreach ($ws_sessions_res as $item) {
      if ($item->SESSIONID->__toString() == $session->getLegacyId()) {
        $ws_session = $item;
        break;
      }
    }

    if (!$ws_session) {
      return FALSE;
    }


    $ws->init('Consulta');
    $ws_prices_res = $ws->request([
        'TipoConsulta' => Consulta::TIPOCONSULTA_PRECIOS,
        'CodigoCine'   => $session->getCinema()->getLegacyId()
    ]);

    $ws_prices = [];
    foreach ($ws_prices_res as $item) {
      if ($item->PRICECODE->__toString() == $ws_session->PRICECODE->__toString()) {
        $ws_prices[$item->TICKETTYPECODE->__toString()] = [
            'name'  => $item->TICKETTYPEDESCRIPTION->__toString(),
            'price' => (int)$item->TICKETPRICE->__toString(),
        ];
      }
    }

    ksort($ws_prices);

    return [
        'pricecode' => $ws_session->PRICECODE->__toString(),
        'tickets' => $ws_prices
    ];
  }


  protected function querySeats(Session $session) {
    $ws = $this->ws;
    $ws->init('DetalleSession');

    $result = [];

    try {
      $seats = $ws->request([
          'Tipo'       => DetalleSession::TIPOCONSULTA_DISPONIBILIDAD,
          'Session'    => $session->getLegacyId(),
          'CodigoCine' => $session->getCinema()->getLegacyId(),
      ]);
    } catch (\Exception $e) {
      $seats = FALSE;
    }
    $result['seats'] = $seats;

    if (!WsHelper::session_allows_seat_allocation($session)) {
      return $result;
    }

    try {
      $layout = $ws->request([
          'Tipo'       => DetalleSession::TIPOCONSULTA_UBICACIONES,
          'Session'    => $session->getLegacyId(),
          'CodigoCine' => $session->getCinema()->getLegacyId(),
      ]);
    } catch (\Exception $e) {
      $layout= FALSE;
    }
    $result['layout'] = $layout;

    return $result;
  }


  protected function selectTickets(Session $session, array $tickets) {
    $req_tickets = [];

    foreach ($tickets as $ticket_id => $ticket) {
      $req_tickets[] = [
          'type' => $ticket_id,
          'qty'  => 1
      ];
    }

    $req = new Request([], [
        'session_id' => $session->getId(),
        'tickets'    => $req_tickets,
    ]);
    $req->setSession($this->http_session);

    try {
      $this->buy_handler->select_tickets($req);
      return TRUE;
    } catch (\Exception $e) {
      return FALSE;
    }
  }


  protected function releaseTickets() {
    try {
      $this->buy_handler->cleanupUserTransactions($this->http_session);
      return TRUE;
    } catch (\Exception $e) {
      return FALSE;
    }
  }


}
