<?php

namespace SocialSnack\FrontBundle\Service;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ConceptHelper
 * @package SocialSnack\FrontBundle\Service
 * @author Guido Kritz
 */
abstract class ConceptHelperAbstract {

  protected $doctrine;


  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  abstract protected function getConceptName();


  public function getCinemasLegacyIds() {
    $repo = $this->doctrine->getRepository('SocialSnackFrontBundle:Attribute');
    $attribute = $repo->findOneByValue($this->getConceptName());
    $ids = [];

    if ($attribute != NULL) {
      $cinema_repo = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema');
      $ids = $cinema_repo->findLegacyIdsByAttribute($attribute->getId());
    }

    return $ids;
  }


  protected function getCinemasLegacyIdsFallback() {
    return [];
  }


  protected function getStatesIdsForCinemas($cinema_legacy_ids) {
    /** @var QueryBuilder $qb */
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $qb
        ->addSelect('s.id')
        ->innerJoin('c.state', 's')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere($qb->expr()->in('c.legacy_id', $cinema_legacy_ids))
    ;

    $q = $qb->getQuery();
    $q->useResultCache(TRUE, 60 * 60, __CLASS__ . ':' . __METHOD__ . ':' . implode('|', $cinema_legacy_ids));
    $res = $q->getResult();

    if ($res) {
      return array_map(function($a) { return $a['id']; }, $res);
    }

    return FALSE;
  }


  protected function getAreasIdsForCinemas($cinema_legacy_ids) {
    /** @var QueryBuilder $qb */
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $qb
        ->addSelect('a.id')
        ->innerJoin('c.area', 'a')
        ->andWhere('a.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere($qb->expr()->in('c.legacy_id', $cinema_legacy_ids))
    ;

    $q = $qb->getQuery();
    $q->useResultCache(TRUE, 60 * 60, __CLASS__ . ':' . __METHOD__ . ':' . implode('|', $cinema_legacy_ids));
    $res = $q->getResult();

    if ($res) {
      return array_map(function($a) { return $a['id']; }, $res);
    }

    return FALSE;
  }


  /**
   * Finds out how many of the given cinemas belong to the state.
   *
   * @param int   $state_id
   * @param array $cinemas_ids
   * @return int
   */
  protected function intersectStateAndCinemas($state_id, $cinemas_ids) {
    /** @var QueryBuilder $qb */
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $qb
        ->select('COUNT(s.id)')
        ->innerJoin('c.state', 's')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere('s = :state_id')
        ->setParameter('state_id', $state_id)
        ->andWhere($qb->expr()->in('c.legacy_id', $cinemas_ids))
    ;

    $q = $qb->getQuery();

    $q->useResultCache(TRUE, 60 * 60, implode(':', [__METHOD__, $this->getConceptName(), $state_id]));

    return $count = $qb->getQuery()->getSingleScalarResult();
  }


  /**
   * Finds out how many of the given cinemas belong to the area.
   *
   * @param int   $area_id
   * @param array $cinemas_ids
   * @return int
   */
  protected function intersectAreaAndCinemas($area_id, $cinemas_ids) {
    /** @var QueryBuilder $qb */
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $qb
        ->select('COUNT(a.id)')
        ->innerJoin('c.area', 'a')
        ->andWhere('a.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere('a = :area_id')
        ->setParameter('area_id', $area_id)
        ->andWhere($qb->expr()->in('c.legacy_id', $cinemas_ids))
    ;

    $q = $qb->getQuery();

    $q->useResultCache(TRUE, 60 * 60, implode(':', [__METHOD__, $this->getConceptName(), $area_id]));

    return $count = $qb->getQuery()->getSingleScalarResult();
  }


  /**
   * Get the IDs of the states which have this concept.
   *
   * @return array|bool
   */
  public function getStatesIds() {
    $cinemas_ids = $this->getCinemasLegacyIds();
    if (!sizeof($cinemas_ids)) {
      return [];
    }
    $states_ids  = $this->getStatesIdsForCinemas($cinemas_ids);

    return $states_ids;
  }


  /**
   * Checks if any cinema in a state has this concept.
   *
   * @param int $state_id
   * @return bool
   */
  public function stateHasConcept($state_id) {
    $cinemas_ids = $this->getCinemasLegacyIds();
    if (!sizeof($cinemas_ids)) {
      return FALSE;
    }
    return $this->intersectStateAndCinemas($state_id, $cinemas_ids) > 0;
  }


  /**
   * Checks if any cinema in an area has this concept.
   *
   * @param int $area_id
   * @return bool
   */
  public function areaHasConcept($area_id) {
    $cinemas_ids = $this->getCinemasLegacyIds();
    if (!sizeof($cinemas_ids)) {
      return FALSE;
    }
    return $this->intersectAreaAndCinemas($area_id, $cinemas_ids) > 0;
  }


} 