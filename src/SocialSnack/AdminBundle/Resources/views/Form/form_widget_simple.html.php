<?php
if (!isset($type) || in_array($type, array('text', 'password', 'email', 'number'))) {
  $attr['class'] = (isset($attr['class']) ? ($attr['class'] . ' ') : '') . 'form-control';
}
?>
<input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes', array('attr' => $attr)) ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />