<?php

namespace SocialSnack\WsBundle\Exception\MovieGroup;

/**
 * Class NoGroupIdException
 * @package SocialSnack\WsBundle\Exception\MovieGroup
 * @author Guido Kritz
 */
class NoGroupIdException extends \Exception {

} 