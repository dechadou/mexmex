<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProfileType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
//        $builder->add('user', new UserType());
//        $builder->add(
//            'terms',
//            'checkbox',
//            array('property_path' => 'termsAccepted')
//        );

    parent::buildForm($builder, $options);
    $builder->setAttribute( 'novalidate', TRUE );
    $builder->add('first_name', 'text',     array( 'label' => 'Nombre' ) );
    $builder->add('last_name',  'text',     array( 'label' => 'Apellido' ) );
    $builder->add('email',      'email',    array( 'label' => 'Email' ) );
    $builder->add('plainPassword',   'password', array( 'label' => 'Contraseña', 'required' => FALSE ) );
    $builder->add('file',       'file',     array( 'label' => 'Imagen de perfil', 'required' => FALSE ) );
//    $builder->add('submit',     'submit',   array( 'label' => 'Guardar' ) );

    $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
      $user = $event->getData();
      $form = $event->getForm();

      $form->add('info_phone_mobile', 'text', array(
        'label' => 'Número de celular',
        'required' => FALSE,
        'mapped' => FALSE,
        'data' => $user->getInfo('mobile_number')
      ));

      $form->add('info_phone_carrier', 'choice', array(
        'label' => 'Operadora',
        'required' => FALSE,
        'mapped' => FALSE,
        'empty_value' => ' ',
        'data' => $user->getInfo('mobile_carrier'),
        'choices' => array(
          'telcel' => 'Telcel',
          'movistar' => 'Movistar',
          'iusacell' => 'Iusacell',
          'nextel' => 'nextel'
        )
      ));
    });
  }

  public function getName() {
    return 'socialsnack_user_profile';
  }

}
