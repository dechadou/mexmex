<?php

namespace SocialSnack\AdminBundle\Debug\Result;

class SessionResult extends ResultAbstract {

  /**
   * @inheritdoc
   */
  public function getDataForTable() {
    $local = $this->data['local'];
    $ws    = $this->data['ws'];

    $data = [
        'thead' => ['', 'DB', 'WS', ''],
        'tbody' => [],
    ];

    $data['tbody'][] = [
        'th' => 'Activa',
        'td' => [
            $local['active'] ? 'Sí' : 'No',
            $ws ? 'Sí' : 'No',
            $local['active'] && $ws
                ? self::TABLE_LABEL_SUCCESS
                : self::TABLE_LABEL_DANGER
        ],
    ];

    // If the session doesn't exists on the WS there's no point displaying any more information.
    if (!$ws) {
      return $data;
    }

    // Price codes.
    $data['tbody'][] = [
        'th' => 'Pricecode',
        'td' => [
            $local['pricecode'],
            $ws['pricecode'],
            $local['pricecode'] === $ws['pricecode']
                ? self::TABLE_LABEL_SUCCESS
                : self::TABLE_LABEL_DANGER
        ]
    ];

    // Tickets.
    $ticket_codes = array_unique(array_merge(
        array_keys($local['tickets']),
        array_keys($ws['tickets'])
    ));

    foreach ($ticket_codes as $ticket_code) {
      if (isset($local['tickets'][$ticket_code])) {
        $ticket_local_lbl = $local['tickets'][$ticket_code]['name']
            . '<br/>'
            . $local['tickets'][$ticket_code]['price'];
      } else {
        $ticket_local_lbl = '-';
      }

      if (isset($ws['tickets'][$ticket_code])) {
        $ticket_ws_lbl = $ws['tickets'][$ticket_code]['name']
            . '<br/>'
            . $ws['tickets'][$ticket_code]['price'];
      } else {
        $ticket_ws_lbl = '-';
      }

      if (!isset($local['tickets'][$ticket_code]) || !isset($ws['tickets'][$ticket_code]) || $local['tickets'][$ticket_code]['price'] != $ws['tickets'][$ticket_code]['price']) {
        $row_status = self::TABLE_LABEL_DANGER;
      } elseif ($local['tickets'][$ticket_code] !== $ws['tickets'][$ticket_code]) {
        $row_status = self::TABLE_LABEL_WARNING;
      } else {
        $row_status = self::TABLE_LABEL_SUCCESS;
      }

      $data['tbody'][] = [
          'th' => 'Boleto ' . $ticket_code,
          'td' => [
              $ticket_local_lbl,
              $ticket_ws_lbl,
              $row_status
          ]
      ];
    }

    if (!count($ticket_codes)) {
      $data['tbody'][] = [
          'th' => 'Boletos',
          'td' => [
              '-',
              '-',
              self::TABLE_LABEL_DANGER
          ]
      ];
    }

    if (FALSE === $this->data['seats']['seats']) {
      $seats_status = self::TABLE_LABEL_DANGER;
    } elseif (0 === $this->data['seats']['seats']) {
      $seats_status = self::TABLE_LABEL_WARNING;
    } else {
      $seats_status = self::TABLE_LABEL_SUCCESS;
    }
    $data['tbody'][] = [
        'th' => 'Asientos',
        'td' => [
            '-',
            $this->data['seats']['seats'],
            $seats_status
        ]
    ];

    if (isset($this->data['seats']['layout'])) {
      $data['tbody'][] = [
          'th' => 'Ubicaciones',
          'td' => [
              '-',
              $this->data['seats']['layout'] ? 'Sí' : 'No',
              $this->data['seats']['layout']
                  ? self::TABLE_LABEL_SUCCESS
                  : self::TABLE_LABEL_DANGER
          ]
      ];
    }

    if ($this->data['select_tickets'] !== NULL) {
      $data['tbody'][] = [
          'th' => 'Apartado Asientos',
          'td' => [
              '-',
              $this->data['select_tickets'] ? 'Ok' : 'Error',
              $this->data['select_tickets']
                  ? self::TABLE_LABEL_SUCCESS
                  : self::TABLE_LABEL_DANGER
          ]
      ];
    }

    if ($this->data['release_tickets'] !== NULL) {
      $data['tbody'][] = [
          'th' => 'Liberar Asientos',
          'td' => [
              '-',
              $this->data['release_tickets'] ? 'Ok' : 'Error',
              $this->data['release_tickets']
                  ? self::TABLE_LABEL_SUCCESS
                  : self::TABLE_LABEL_DANGER
          ]
      ];
    }

    return $data;
  }


  /**
   * @inheritdoc
   */
  public function getAnalysis() {
    $result = [];

    if (!$this->data['ws']) {
      if ($this->data['local']['active']) {
        $result[] = 'La función debería estar inactiva. Se recomienda actualizar las funciones del complejo.';
      } else {
        $result[] = 'La función está inactiva. Si se muestra en el frontend puede ser necesario refrescar el cache.';
      }

      return $result;
    }

    if (!$this->data['local']['active']) {
      $result[] = 'La función está inactiva pero no debería estarlo. Se recomienda actualizar las funciones del complejo.';
    }

    if ($this->data['local']['pricecode'] !== $this->data['ws']['pricecode']) {
      $result[] = 'La información no coincide con el WS. Se recomienda actualizar las funciones del complejo.';
    }

    if (!count($this->data['ws']['tickets'])) {
      $result[] = 'No se encontraron boletos para esta función. Contactar a Cinemex.';
    } elseif (!count($this->data['local']['tickets'])) {
      $result[] = 'No hay boletos en la base de datos para esta función. Actualizar funciones y boletos del complejo.';
    }

    if ($this->data['seats']['seats'] === FALSE) {
      $result[] = 'Error al consultar la disponibilidad de asientos.';
    }

    if ($this->data['seats']['seats'] === 0) {
      $result[] = 'No hay asientos disponibles para esta función.';
    }

    if (isset($this->data['seats']['layout']) && $this->data['seats']['layout'] === FALSE) {
      $result[] = 'Error al consultar las ubicaciones de la sala.';
    }

    if (FALSE === $this->data['select_tickets']) {
      $result[] = 'Error al apartar asientos.';
    }

    if (FALSE === $this->data['release_tickets']) {
      $result[] = 'Error al liberar asientos.';
    }

    // No errors.
    if (!count($result)) {
      $result[] = 'No se observan problemas.';
    }

    return $result;
  }

}