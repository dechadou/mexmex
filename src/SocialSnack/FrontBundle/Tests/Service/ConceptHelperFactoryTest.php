<?php

namespace SocialSnack\FrontBundle\Tests\Service;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\FrontBundle\Service\ConceptHelperFactory;

/**
 * Class ConceptHelperFactoryTest
 * @package SocialSnack\FrontBundle\Tests\Service
 * @author Guido Kritz
 */
class ConceptHelperFactoryTest extends WebTestCase {

  public function testCreateHelper() {
    $doctrine = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();

    $factory = new ConceptHelperFactory($doctrine);

    $helper = $factory->createHelper('Art');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\Art', $helper);

    $helper = $factory->createHelper('Platinum');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\Platinum', $helper);

    $helper = $factory->createHelper('Premium');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\Premium', $helper);

    $helper = $factory->createHelper('3D');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\V3D', $helper);

    $helper = $factory->createHelper('V3D');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\V3D', $helper);

    $helper = $factory->createHelper('X4D');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\X4D', $helper);

    $helper = $factory->createHelper('Xtreme');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\Xtreme', $helper);

    $helper = $factory->createHelper('CX');
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Service\ConceptHelper\Xtreme', $helper);
  }


  public function testCreateHelperNonexistent() {
    $doctrine = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();

    $factory = new ConceptHelperFactory($doctrine);

    $this->setExpectedException('\Exception');

    $factory->createHelper('FooBar');
  }

}