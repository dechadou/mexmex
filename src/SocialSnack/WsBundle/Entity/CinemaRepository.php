<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class CinemaRepository extends EntityRepository {


  /**
   * Find by many IDs using an IN() clause.
   *
   * @param string $col
   * @param array $ids
   * @param array $criteria
   * @return type
   */
	protected function _findManyBy( $col, $ids, $criteria = array() ) {
		if ( 1 == sizeof( $ids ) ) {
			return $this->findBy( array_merge( array( $col => $ids[0] ), $criteria ) );
		}

		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->addSelect( 'c' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
        ->where( 'c.active = 1' )
				->andWhere(
						$qb->expr()->in( 'c.' . $col, $ids )
				);

		if ( $criteria ) {
			foreach ( $criteria as $k => $v ) {
				$qb->andWhere( 'c.' . $k . ' = :' . $k )
						->setParameter( $k, $v );
			}
		}

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		return $query->execute();
	}


  /**
   * Find by many IDs using an IN() clause.
   *
   * @param array $ids
   * @param array $criteria
   * @return type
   */
	public function findManyBy( $ids, $criteria = array() ) {
		return $this->_findManyBy('id', $ids, $criteria);
	}


  /**
   * Find by many legacy IDs using an IN() clause.
   *
   * @param array $ids
   * @param array $criteria
   * @return type
   */
	public function findManyByLegacyIdBy( $ids, $criteria = array() ) {
		return $this->_findManyBy('legacy_id', $ids, $criteria);
	}


  /**
   * Override default findAll() method.
   * By default it only returns active cinemas.
   * Uses Result Cache.
   *
   * @param boolean $active_only Optional. Default = TRUE.
   * @param boolean $use_cache Optional. Default = TRUE.
   * @return type
   */
  public function findAll($active_only = TRUE, $use_cache = TRUE) {
    $qb = $this->createQueryBuilder('c');

    if ($active_only) {
      $qb->andWhere('c.active = 1');
    }

    $qb->orderBy('c.name', 'ASC');

		$query = $qb->getQuery();

    if ($use_cache) {
      $query->useResultCache( TRUE, 60 * 60 );
    }

		return $query->execute();
  }


  /**
   * Override default find() method.
   * By default it only returns active cinemas.
   * Uses Result Cache.
   *
   * @param mixed   $id          Entity record ID.
   * @param boolean $active_only Optional. Default = TRUE.
   * @param type    $lockMode    Not implemented.
   * @param type    $lockVersion Not implemented.
   * @param boolean $use_cache   Optional. Default = TRUE.
   * @return type
   */
  public function find($id, $active_only = TRUE, $lockMode = LockMode::NONE, $lockVersion = null, $use_cache = TRUE) {
    $pk = $this->getClassMetadata()->getSingleIdentifierColumnName();
    $qb = $this->createQueryBuilder('c');

    $qb->andWhere('c.' . $pk . ' = :id')
        ->setParameter('id', $id);

    if ( $active_only ) {
      $qb->andWhere('c.active = 1');
    }

		$query = $qb->getQuery();

    if ($use_cache) {
      $query->useResultCache(TRUE, 60 * 60, 'CinemaRepository:find:' . $id);
    }

    try {
      return $query->getSingleResult();
    } catch ( \Doctrine\ORM\NoResultException $e ) {
      return NULL;
    }
  }


  /**
   * Find cinemas playing a specific movie.
   *
   * @param int   $movie_id
   * @param int   $area_id    Optional.
   * @param int   $state_id   Optional.
   * @param array $cinema_ids Optional.
   * @return type
   * @throws \Exception
   */
	protected function findPlayingMovie( $movie_id, $area_id = NULL, $state_id = NULL, $cinema_ids = NULL ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'c' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
				->innerJoin( 'c.sessions', 's' )
				->innerJoin( 's.movie', 'm' )
				->where( 'm.id = :movie_id' )
				->andWhere( 's.active = 1' )
				->setParameter( 'movie_id', $movie_id );

    if ( !is_null($area_id) ) {
      // Filter by area.
      $qb->andWhere( 'c.area = :area_id')
          ->setParameter( 'area_id', $area_id );
    } elseif ( !is_null($state_id) ) {
      // Filter by state.
      $qb->andWhere( 'c.state = :state_id')
          ->setParameter( 'state_id', $state_id );
    }

    // Find only between some specific cinemas.
    if ( !is_null($cinema_ids) ) {
      $qb->andWhere($qb->expr()->in('c.id', $cinema_ids));
    }

		$query = $qb->getQuery();

		return $query->execute();
	}


  /**
   * Find cinemas playing a specific movie within a group of cinemas.
   *
   * @param int   $movie_id
   * @param array $cinema_ids
   * @return type
   */
	public function findPlayingMovieInCinemas($movie_id, $cinema_ids) {
    return $this->findPlayingMovie($movie_id, NULL, NULL, $cinema_ids);
  }


  /**
   * Find cinemas playing a specific movie within an area.
   *
   * @param int   $movie_id
   * @param int   $area_id
   * @return type
   */
	public function findPlayingMovieInArea($movie_id, $area_id) {
    return $this->findPlayingMovie($movie_id, $area_id);
  }


  /**
   * Find cinemas playing a specific movie within a state.
   *
   * @param int   $movie_id
   * @param int   $state_id
   * @return type
   */
	public function findPlayingMovieInState($movie_id, $state_id) {
    return $this->findPlayingMovie($movie_id, NULL, $state_id);
  }


	/**
	 * Find all cinemas with sessions for a movie from now to eternity (?).
	 *
	 * @param integer   $movie_id
	 */
	public function findAllWithMovieSessions( $movie_id ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'c' )
				->addSelect( 's' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
				->innerJoin( 'c.sessions', 's' )
				->innerJoin( 's.movie', 'm' );

		$qb->where( 's.movie = :movie_id' )
        ->andWhere( 's.active = 1' )
				->orderBy( 's.time' )
				->setParameter( 'movie_id', $movie_id );

		WsHelper::dql_apply_dates( $qb, new \DateTime( 'now' ), 0 );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		return $query->getResult();
	}


	/**
	 * Find a cinema and return all the sessions for a movie from now to eternity (?).
	 *
	 * @param integer   $cinema_ids
	 * @param integer   $movie_id
	 */
	public function findWithMovieSessions( $cinema_ids, $movie_id ) {
		if ( !$cinema_ids )
			throw new \Exception( 'ID not specified.' );

		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'c' )
				->addSelect( 's' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
				->leftJoin( 'c.sessions', 's' )
				->leftJoin( 's.movie', 'm' );

		$single = FALSE;
		if ( !is_array( $cinema_ids ) ) {
			$single = TRUE;
			$cinema_ids = array( $cinema_ids );
		}
//		$cinema_where = array();
//		foreach ( $cinema_ids as $cinema_id ) {
//			if ( $cinema_id instanceof \Doctrine\ORM\PersistentCollection )
//				$cinema_id = $cinema_id->getId();
//			$cinema_where[] = sprintf( 'c.id = %d', $cinema_id );
//		}

//		$qb->where( '(' . implode( ' OR ', $cinema_where ) . ')' )
		$qb->where( $qb->expr()->in( 'c.id', $cinema_ids ) )
				->andWhere( 's.movie = :movie_id' )
				->orderBy( 's.time' )
				->setParameter( 'movie_id', $movie_id );

		WsHelper::dql_apply_dates( $qb, new \DateTime( 'now' ), 0 );

//		$qb->setMaxResults( 10 );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );

		if ( !$single )
			return $query->getResult();

		try {
			return $query->getSingleResult();
		} catch ( NoResultException $e ) {
			return NULL;
		}
	}


	/**
	 * Find cinemas with sessions for a movie on a specific date (without returning
   * the sessions).
	 *
	 * @param integer   $movie
	 * @param \DateTime $date
	 * @param integer   $cinema_ids
	 */
	public function findHavingSessionsForDate( $date, $args = array(), $offset = 0, $limit = 10 ) {
		$qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect( 'c' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
				->innerJoin( 'c.sessions', 's' )
        ->where( 's.active = 1' );

    extract($args);

    if (isset($movie)) {
			$qb->innerJoin( 's.movie', 'm' );
      WsHelper::dql_apply_movie( $qb, $movie, 's' );
    }

		$single = FALSE;

    if (isset($cinema_ids)) {
      if ( !is_array( $cinema_ids ) ) {
        $single = TRUE;
        $cinema_ids = array( $cinema_ids );
      }
      $qb->andWhere( $qb->expr()->in( 'c.id', $cinema_ids ) );
    }

    if (isset($area_id)) {
      $qb->andWhere('c.area = :area')
          ->setParameter('area', $area_id);
    } elseif (isset($state_id)) {
      $qb->andWhere('c.state = :state')
          ->setParameter('state', $state_id);
    }

		WsHelper::dql_apply_dates( $qb, $date );

    $qb->groupBy('c.id');

    if ($offset) {
      $qb->setFirstResult($offset);
    }
    if ($limit) {
      $qb->setMaxResults( $limit );
    }

		$query = $qb->getQuery();
//    var_dump($query->getSQL());
//    var_dump($query->getParameters());
//    die();
    $query->useResultCache( TRUE, 30 * 60 );
    return $query->getResult();
	}


	/**
	 * Find cinemas and return all the sessions for a movie on a specific date.
	 *
	 * @param integer   $movie
	 * @param \DateTime $date
	 * @param integer   $cinema_ids
	 * @param integer   $exclude_ids
	 */
	protected function _findWithMovieSessionsForDate( $date, $movie = NULL, $cinema_ids = NULL, $exclude_ids = NULL, $attributes = NULL ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'c' )
				->addSelect( 'm' )
				->addSelect( 's' )
				->from( 'SocialSnackWsBundle:Cinema', 'c' )
				->leftJoin( 'c.sessions', 's' )
				->leftJoin( 's.movie', 'm' )
        ->andWhere( 's.active = 1' );

    WsHelper::dql_apply_movie( $qb, $movie, 's' );

    if ( $attributes ) {
      WsHelper::dql_apply_movie_attrs( $qb, $attributes );
    }

		$single = FALSE;

    if ( $cinema_ids ) {
      if ( !is_array( $cinema_ids ) ) {
        $single = TRUE;
        $cinema_ids = array( $cinema_ids );
      }
      $qb->andWhere( $qb->expr()->in( 'c.id', $cinema_ids ) );
    }


    if ( $exclude_ids ) {
      if ( !is_array( $exclude_ids ) ) {
        $exclude_ids = array( $exclude_ids );
      }
      $qb->andWhere( $qb->expr()->notIn( 'c.id', $exclude_ids ) );
    }

		$qb->orderBy( 's.time' );

		WsHelper::dql_apply_dates( $qb, $date );

//		$qb->setMaxResults( 10 );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 30 * 60 );

    $cinemas = array();
    foreach ( $query->getResult() as $cinema ) {
      $sessions = array();

      /* @var $_sess \SocialSnack\WsBundle\Entity\Session */
      foreach ( $cinema->getSessions() as $_sess ) {
        $_movie_id = $_sess->getMovie()->getId();
        if ( !isset( $sessions[$_movie_id] ) ) {
          $sessions[$_movie_id] = array(
              'movie'    => $_sess->getMovie(),
              'sessions' => array(),
          );
        }
        $sessions[$_movie_id]['sessions'][] = $_sess;
      }

      $cinemas[] = array(
          'cinema' => $cinema,
          'movies' => $sessions,
      );
    }
    return $cinemas;
	}


	/**
	 * Find a cinema and return all the sessions for a movie on a specific date.
	 *
	 * @param integer   $cinema_ids
	 * @param integer   $movie_id
	 * @param \DateTime $date
	 */
	public function findWithMovieSessionsForDate( $cinema_ids, $movie_id, $date ) {
    return $this->_findWithMovieSessionsForDate( $date, $movie_id, $cinema_ids );
	}


	public function findByAttrWithMovieSessionsForDate( $attributes, $date ) {
    return $this->_findWithMovieSessionsForDate( $date, NULL, NULL, NULL, $attributes );
	}


	public function findInAreaWithMovieSessionsForDate( $area, $movie_id, $date ) {
    $cinema_ids = array();
    foreach ( $area->getCinemas() as $cinema ) {
      $cinema_ids[] = $cinema->getId();
    }
    return $this->_findWithMovieSessionsForDate( $date, $movie_id, $cinema_ids );
	}


  public function findOtherWithMovieSessionsForDate( $exclude_ids, $movie_id, $date ) {
    return $this->_findWithMovieSessionsForDate( $date, $movie_id, NULL, $exclude_ids );
  }


	/**
	 *
	 * @param integer $cinema_id
	 * @param integer $results
   * @param integer $offset
	 * @return type
	 */
	public function findClosestCinemas( $cinema_id, $results = 3, $offset = 0 ) {
		if ( $cinema_id instanceof Cinema ) {
			$cinema_id = $cinema_id->getId();
		}

		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->addSelect( 'cd' )
				->addSelect( 'tc' )
				->from( 'SocialSnackWsBundle:CinemaDistance', 'cd' )
				->innerJoin( 'cd.to_cinema', 'tc' )
				->where( 'cd.from_cinema = :cinema_id' )
				->andWhere( 'tc.active=1' )
        ->addOrderBy('cd.distance', 'ASC')
				->setMaxResults( $results )
				->setParameter( 'cinema_id', $cinema_id );

    if ( $offset ) {
      $qb->setFirstResult($offset);
    }

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		return $query->getResult();
	}


	/**
	 *
	 * @param integer $cinema_id
	 * @param integer $results
   * @param integer $offset
	 * @return type
	 */
	public function findClosestCinemasIds( $cinema_id, $results = 3, $offset = 0 ) {
		if ( $cinema_id instanceof Cinema ) {
			$cinema_id = $cinema_id->getId();
		}

		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'cd' )
				->from( 'SocialSnackWsBundle:CinemaDistance', 'cd' )
				->where( 'cd.from_cinema = :cinema_id' )
				->setMaxResults( $results )
				->setParameter( 'cinema_id', $cinema_id );

    if ( $offset ) {
      $qb->setFirstResult($offset);
    }

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );

		$ids = array();
		foreach ( $query->getResult() as $r ) {
			$ids[] = $r->getToCinema()->getId();
		}
		return $ids;
	}


  /**
   * Return platinum Cinemas
   *
   * @param  boolean $ids_only
   * @return type
   */
  public function findPlatinum( $ids_only = FALSE ) {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect( $ids_only ? 'c.id' : 'c' )
        ->from( 'SocialSnackWsBundle:Cinema', 'c' )
        ->where( 'c.platinum = 1' )
        ->andWhere( 'c.active = 1' );
    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );

    if ( !$ids_only )
      return $query->getResult();

    $result = array();
    foreach ( $query->getResult() as $cinema ) {
      $result[] = $cinema['id'];
    }

    return $result;
  }


  public function findLegacyIds() {
    $qb = $this->createQueryBuilder('c');
    $qb
        ->addSelect('c.legacy_id')
        ->andWhere('c.active = 1')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }


  public function findByAttribute($attribute){
    $qb = $this->getEntityManager()->createQueryBuilder()
        ->addSelect( 'c' )
        ->from( 'SocialSnackWsBundle:Cinema', 'c' )
        ->where('c.attributes LIKE :attribute')
        ->setParameter('attribute','%'.$attribute.'%')
        ->andWhere('c.active = 1');

    $query = $qb->getQuery();
		$query->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . $attribute);
    return $query->getResult();
  }


  public function findLegacyIdsByAttribute($attribute){
    $qb = $this->getEntityManager()->createQueryBuilder()
        ->addSelect( 'c.legacy_id' )
        ->from( 'SocialSnackWsBundle:Cinema', 'c' )
        ->where('c.attributes LIKE :attribute')
        ->setParameter('attribute','%'.$attribute.'%')
        ->andWhere('c.active = 1');

    $query = $qb->getQuery();
		$query->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . $attribute);
    $res = $query->getScalarResult();

		return array_map('current', $res);
  }

}
