<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Image;

class PromoImportantType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', NULL, array(
                'label' => 'Título',
            ))
            ->add('url', NULL, array(
                'label' => 'URL',
            ))
            ->add('linkTarget', 'choice', array(
                'label'   => 'Abrir en',
                'attr'    => ['class' => 'form-control'],
                'choices' => [
                    '_self'  => 'En la misma ventana',
                    '_blank' => 'En una nueva ventana',
                ],
            ))
            ->add('sequence', NULL, array(
                'label' => 'Posición',
            ))
            ->add('target', 'choice', array(
                'label' => 'Mostrar en',
                'choices' => array(
                    'home' => 'General',
                    'maps' => 'Mapas de cines',
                    'cinemas' => 'Páginas de cines',
                    'movies' => 'Películas'
                ),
                'multiple' => TRUE,
                'expanded' => TRUE,
            ))
            ->add('file_thumb', 'file', array(
                'label' => 'Banner',
                'constraints' => array(
                    new Image(),
                ),
                'mapped' => FALSE,
                'required' => $builder->getData()->getId() === NULL,
                'image_preview_path' => 'thumb',
                'image_preview_size' => '490x116',
            ))
            ->add('file_image', 'file', array(
                'label' => 'Lightbox',
                'constraints' => array(
                    new Image(),
                ),
                'mapped' => FALSE,
                'required' => FALSE,
                'image_preview_path' => 'image',
                'image_preview_size' => '700x573',
            ))
        ;

        if ($builder->getData() && $builder->getData()->getImage()) {
          $builder->add('file_image_remove', 'checkbox', array(
              'label' => 'Eliminar lightbox',
              'mapped' => FALSE,
              'required' => FALSE,
          ));
        }

        $builder
            ->add('status', 'checkbox', array(
                'label' => 'Activo',
                'required' => FALSE,
            ))
            ->add('save', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SocialSnack\FrontBundle\Entity\PromoImportant'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_promoimportant';
    }
}
