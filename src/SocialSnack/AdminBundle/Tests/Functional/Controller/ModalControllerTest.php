<?php

namespace SocialSnack\AdminBundle\Tests\Functional\Controller;

use SocialSnack\AdminBundle\Tests\Controller\WebTestCase;
use SocialSnack\AdminBundle\Tests\Fixtures\Controller\ModalControllerData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData;

class ModalControllerTest extends WebTestCase {

  public function testIndexAction_Forbidden() {
    $client = $this->getLoggedInClient();
    $client->request(
        'GET',
        '/missisippi/modals/'
    );

    $this->assertSame(403, $client->getResponse()->getStatusCode());
  }


  public function testIndexAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/modals/'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  protected function _testAddAction_Image($imagePath) {
    $faker = \Faker\Factory::create();

    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        ]
    );
    $crawler = $client->request(
        'GET',
        '/missisippi/modals/add'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $title = $faker->title();
    $url = $faker->url();
    $filePath = $client->getKernel()->locateResource($imagePath);
    $form['form[file]']->upload($filePath);
    $cinemas = [
        'stateid-' . LoadStatesData::$states[0]->getId(),
        'areaid-' . LoadAreasData::$areas[0]->getId(),
        'cinemaid-' . LoadCinemasData::$cinemas[0]->getId(),
    ];

    $client->submit($form, [
        'form[title]' => $title,
        'form[type]'  => 'image',
        'form[section]' => [
            'home'
        ],
        'form[cinema]' => $cinemas,
        'form[dias]' => [0, 1, 2, 3, 4, 5, 6],
        'form[url]' => $url,
        'form[delay]' => 666,
        'form[only_once]' => 1,
        'form[status]' => 'publish',
    ]);

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertRegexp('/\/missisippi\/modals\/[0-9]+$/', $client->getResponse()->headers->get('location'));

    $crawler = $client->request('GET', $client->getResponse()->headers->get('location'));

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $this->assertSame($title, $form->get('form[title]')->getValue());
    $this->assertSame('image', $form->get('form[type]')->getValue());
    // @todo Assert checkboxes
    $this->assertSame($cinemas, $form->get('form[cinema]')->getValue());
    $this->assertSame($url, $form->get('form[url]')->getValue());
    $this->assertSame('666', $form->get('form[delay]')->getValue());
    $this->assertSame('1', $form->get('form[only_once]')->getValue());
    $this->assertSame('publish', $form->get('form[status]')->getValue());

    // Test that the image actually exists.
    $src = $crawler->filter('img')->eq(0)->extract(['src'])[0];
    $cms_upload_url = $client->getContainer()->getParameter('cms_upload_url');
    $cms_upload_path = $client->getContainer()->getParameter('cms_upload_path');
    $uploads_path = '/tmp/';
    $uploaded_path = preg_replace('/^' . preg_quote($cms_upload_url, '/') . '/', $uploads_path. $cms_upload_path, $src);
    $this->assertTrue(is_file($uploaded_path));
  }


  public function testAddAction_Image_LowerCaseExtension() {
    $this->_testAddAction_Image('@SocialSnackAdminBundle/Tests/Resources/test-upload-image.jpg');
  }


  public function testAddAction_Image_UpperCaseExtension() {
    $this->_testAddAction_Image('@SocialSnackAdminBundle/Tests/Resources/test-upload-image-2.JPG');
  }


  public function testAddAction_SWF() {
    $faker = \Faker\Factory::create();

    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        ]
    );
    $crawler = $client->request(
        'GET',
        '/missisippi/modals/add'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $title = $faker->title();
    $url = $faker->url();
    $filePath = $client->getKernel()->locateResource('@SocialSnackAdminBundle/Tests/Resources/test-upload-image.jpg');
    $form['form[file]']->upload($filePath);
    $cinemas = [
        'stateid-' . LoadStatesData::$states[0]->getId(),
        'areaid-' . LoadAreasData::$areas[0]->getId(),
        'cinemaid-' . LoadCinemasData::$cinemas[0]->getId(),
    ];

    $client->submit($form, [
        'form[title]' => $title,
        'form[type]'  => 'swf',
        'form[section]' => [
            'home'
        ],
        'form[cinema]' => $cinemas,
        'form[dias]' => [0, 1, 2, 3, 4, 5, 6],
        'form[url]' => $url,
        'form[width]' => 66,
        'form[height]' => 99,
        'form[position]' => 'top',
        'form[clicktag]' => 'foobar',
        'form[delay]' => 666,
        'form[only_once]' => 1,
        'form[status]' => 'publish',
    ]);

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertRegexp('/\/missisippi\/modals\/[0-9]+$/', $client->getResponse()->headers->get('location'));

    $crawler = $client->request('GET', $client->getResponse()->headers->get('location'));

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $this->assertSame($title, $form->get('form[title]')->getValue());
    $this->assertSame('swf', $form->get('form[type]')->getValue());
    // @todo Assert checkboxes
    $this->assertSame($cinemas, $form->get('form[cinema]')->getValue());
    $this->assertSame($url, $form->get('form[url]')->getValue());
    $this->assertSame('66', $form->get('form[width]')->getValue());
    $this->assertSame('99', $form->get('form[height]')->getValue());
    $this->assertSame('top', $form->get('form[position]')->getValue());
    $this->assertSame('foobar', $form->get('form[clicktag]')->getValue());
    $this->assertSame('666', $form->get('form[delay]')->getValue());
    $this->assertSame('1', $form->get('form[only_once]')->getValue());
    $this->assertSame('publish', $form->get('form[status]')->getValue());
  }


  public function testEditAction() {
    $faker = \Faker\Factory::create();

    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
            'SocialSnack\AdminBundle\Tests\Fixtures\Controller\ModalControllerData',
        ]
    );

    $route = sprintf('/missisippi/modals/%d', ModalControllerData::$modals[0]->getId());

    $crawler = $client->request(
        'GET',
        $route
    );

    $this->assertTrue($client->getResponse()->isSuccessful());

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $title = $faker->title();
    $url = $faker->url();
    $filePath = $client->getKernel()->locateResource('@SocialSnackAdminBundle/Tests/Resources/test-upload-image.jpg');
    $form['form[file]']->upload($filePath);
    $cinemas = [
        'stateid-' . LoadStatesData::$states[0]->getId(),
        'areaid-' . LoadAreasData::$areas[0]->getId(),
        'cinemaid-' . LoadCinemasData::$cinemas[0]->getId(),
    ];

    $client->submit($form, [
        'form[title]' => $title,
        'form[type]'  => 'swf',
        'form[section]' => [
            'home'
        ],
        'form[cinema]' => $cinemas,
        'form[dias]' => [0, 1, 2, 3, 4, 5, 6],
        'form[url]' => $url,
        'form[width]' => 66,
        'form[height]' => 99,
        'form[position]' => 'top',
        'form[clicktag]' => 'foobar',
        'form[delay]' => 666,
        'form[only_once]' => 1,
        'form[status]' => 'publish',
    ]);

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame($route, $client->getResponse()->headers->get('location'));

    $crawler = $client->request('GET', $client->getResponse()->headers->get('location'));

    $submit = $crawler->selectButton('submit');
    $form   = $submit->form();

    $this->assertSame($title, $form->get('form[title]')->getValue());
    $this->assertSame('swf', $form->get('form[type]')->getValue());
    // @todo Assert checkboxes
    $this->assertSame($cinemas, $form->get('form[cinema]')->getValue());
    $this->assertSame($url, $form->get('form[url]')->getValue());
    $this->assertSame('66', $form->get('form[width]')->getValue());
    $this->assertSame('99', $form->get('form[height]')->getValue());
    $this->assertSame('top', $form->get('form[position]')->getValue());
    $this->assertSame('foobar', $form->get('form[clicktag]')->getValue());
    $this->assertSame('666', $form->get('form[delay]')->getValue());
    $this->assertSame('1', $form->get('form[only_once]')->getValue());
    $this->assertSame('publish', $form->get('form[status]')->getValue());
  }


  public function testDeleteAction() {
    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
            'SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
            'SocialSnack\AdminBundle\Tests\Fixtures\Controller\ModalControllerData',
        ]
    );

    $modal_id = ModalControllerData::$modals[0]->getId();

    $client->request(
        'GET',
        sprintf('/missisippi/modals/%d/delete', $modal_id)
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/missisippi/modals/', $client->getResponse()->headers->get('location'));


    $client->request(
        'GET',
        sprintf('/missisippi/modals/%d', $modal_id)
    );

    $this->assertSame(404, $client->getResponse()->getStatusCode());
  }

}