<?php

namespace SocialSnack\WsBundle\Exception\MovieGroup;

/**
 * Class PreviousGroupFoundException
 * @package SocialSnack\WsBundle\Exception\MovieGroup
 * @author Guido Kritz
 */
class PreviousGroupFoundException extends \Exception {

} 