<?php

namespace SocialSnack\WsBundle\Observer;

use SocialSnack\WsBundle\Request\ConsultaAbstract;

/**
 * Class WsStats
 * @package SocialSnack\WsBundle\Observer
 * @author Guido Kritz
 */
class WsStats extends WsObserverAbstract {

  protected $memcached;

  protected $supportedMethods = ['request'];

  protected $subject;


  public function __construct(\Memcached $memcached) {
    $this->memcached = $memcached;
  }


  protected function request(ConsultaAbstract $ws_request, \Buzz\Message\Response $response = NULL, \Exception $e = NULL) {
    $this->subject = $ws_request;

    if (isset($e)) {
      $this->logSuccessStats(FALSE, $e);
    } else {
      $this->logSuccessStats(TRUE);
    }
  }


  protected function logSuccessStats($success, \Exception $e = NULL) {
    $keys = $this->subject->getSuccessStatsKeys($e);

    foreach ($keys as $key) {
      $this->memcached->add($key, 0);
      if ($success) {
        $this->memcached->decrement($key);
      } else {
        $this->memcached->increment($key);
      }
    }
  }

}