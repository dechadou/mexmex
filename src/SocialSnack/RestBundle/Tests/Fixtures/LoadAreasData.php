<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\StateArea;

class LoadAreasData extends AbstractFixture implements OrderedFixtureInterface {

  static $areas;

  public function load(ObjectManager $manager) {
    self::$areas = [];

    $area1 = new StateArea();
    $area1
        ->setActive(TRUE)
        ->setLegacyId(1001)
        ->setLegacyName('NORTE')
        ->setLegacyStateId(1001)
        ->setName('Norte')
        ->setState($this->getReference('State-1'))
    ;
    $this->addReference('Area-1-1', $area1);
    self::$areas[] = $area1;
    $manager->persist($area1);

    $area2 = new StateArea();
    $area2
        ->setActive(TRUE)
        ->setLegacyId(1002)
        ->setLegacyName('SUR')
        ->setLegacyStateId(1001)
        ->setName('Sur')
        ->setState($this->getReference('State-1'))
    ;
    $this->addReference('Area-1-2', $area2);
    self::$areas[] = $area2;
    $manager->persist($area2);

    $area3 = new StateArea();
    $area3
        ->setActive(TRUE)
        ->setLegacyId(1003)
        ->setLegacyName('ESTE')
        ->setLegacyStateId(1002)
        ->setName('Este')
        ->setState($this->getReference('State-2'))
    ;
    $this->addReference('Area-2-1', $area3);
    self::$areas[] = $area3;
    $manager->persist($area3);

    $manager->flush();
  }

  public function getOrder() {
    return 2;
  }
}