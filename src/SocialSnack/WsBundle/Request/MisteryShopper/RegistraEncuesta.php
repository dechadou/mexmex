<?php

namespace SocialSnack\WsBundle\Request\MisteryShopper;

use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;
use SocialSnack\WsBundle\Request\MisteryShopperAbstract;

class RegistraEncuesta extends MisteryShopperAbstract {

  protected $method = 'RegistraEncuesta';

  protected $default_args = array(
      'ClaveTransac' => NULL,
  );


  /**
   * Parse the request and return only the relevant nodes.
   *
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
   * @param \stdClass $res;
   * @return \SimpleXMLElement
   */
  public function parse_res($res) {
    if ( !$res->RegistraEncuestaResult ) {
      throw new RequestMsgException( 'Invalid response 0: ' . json_encode( $res ) );
    }

    $xml = simplexml_load_string( $res->RegistraEncuestaResult->any, null, LIBXML_NOCDATA );

    if (!isset($xml->RESPUESTA, $xml->RESPUESTA->MENSAJE)) {
      throw new UnexpectedWsResponseException();
    }

    return $xml->RESPUESTA->MENSAJE;
  }

}