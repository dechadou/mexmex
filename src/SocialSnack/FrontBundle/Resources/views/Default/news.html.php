<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['news-archive']); ?>
<?php $view['slots']->set('title', 'Novedades - Cinemex'); ?>
<?php $view['slots']->start('body') ?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('news_archive'); ?>">Novedades</a></li>
</ul>

<span class="heading-title">
  <span class="discicon icon-star icon-red"></span>
  Novedades
</span>

<div class="clearfix row archive-row" id="news-archive">

  <?php foreach ( $articles as $_article ) { ?>
  <?php $link = $view['router']->generate( 'news_single', array('id' => $_article->getId(), 'slug' => FrontHelper::sanitize_for_url($_article->getTitle())) ); ?>
  <article class="col col-sm-1-3 col-md-1-4 news-grid-item">
    <a href="<?php echo $link; ?>"><img src="<?php echo $view['fronthelper']->get_img_size($_article->getThumb(), '236x222', 'CMS'); ?>" alt="" /></a>
    <h1 class="news-grid-title"><a href="<?php echo $link; ?>"><?php echo FrontHelper::shorten($_article->getTitle(), 50 ); ?></a></h1>
  </article>
  <?php } ?>
  
<!--  <article class="col span_2_4 news-grid-item size21 featured">
    <a href="#"><img src="http://placehold.it/497x304" alt="" /></a>
    <h1 class="news-grid-title"><a href="#">Excelente actuación de Jim Carrey en Kick Ass 2</a></h1>
  </article>-->

</div>
<?php $view['slots']->stop(); ?>
