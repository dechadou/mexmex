<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Routing\Router;

class PurgeAllAreasHomeBillboard implements UrlTaskInterface {

  protected $router;

  protected $doctrine;

  public function __construct(Router $router, Registry $doctrine) {
    $this->router   = $router;
    $this->doctrine = $doctrine;
  }


  public function getUrlResources() {
    $resources = [];

    $states = $this->getActiveAreas();

    foreach ($states as $state) {
      $resources[] = [
          'path' => $this->router->generate('esi_movies_in_area', ['area_id' => $state['id']]),
          'env'  => 'front'
      ];
    }

    return $resources;
  }


  protected function getActiveAreas() {
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:StateArea')->createQueryBuilder('q');
    $qb
        ->select('q.id')
        ->where('q.active = 1')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }

}