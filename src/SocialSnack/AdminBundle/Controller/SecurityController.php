<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use \SocialSnack\AdminBundle\Entity\Admin;

class SecurityController extends BaseController {

  protected function get_required_role() {
    return NULL;
  }
  
  public function loginAction() {
    $request = $this->getRequest();
    $session = $request->getSession();

    // get the login error if there is one
    if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
      $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
    } else {
      $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
    }

    return $this->render('SocialSnackAdminBundle:Default:login.html.php', array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error' => $error,
    ));
  }

  public function blockIPAction(Request $request) {
    $this->check_roles('ROLE_IP_BLOCK');
    
    $ip = $request->request->get('ip');
    if (!$ip || !filter_var($ip, FILTER_VALIDATE_IP)) {
      throw new \Exception('Invalid IP');
    }

    $conn = $this->getDoctrine()->getManager()->getConnection();
    $query = "UPDATE DynOption SET value = TRIM(LEADING ',' FROM CONCAT(value, ',', :ip)) WHERE name = 'block_ips' AND value NOT LIKE :like";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('ip', sprintf('"%s"', $ip));
    $stmt->bindValue('like', sprintf('%%"%s"%%', $ip));
    $stmt->execute();

    return new \Symfony\Component\HttpFoundation\JsonResponse();
  }

}
