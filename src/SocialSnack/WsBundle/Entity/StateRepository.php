<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class StateRepository extends EntityRepository {
	
  public function findAll($active_only = TRUE) {
		$query = $this->getEntityManager()
				->createQuery(
							"SELECT s FROM SocialSnackWsBundle:State s" .
              ($active_only ? " WHERE s.active = 1" : " ") .
							"ORDER BY s.order DESC, s.name"
				);
    $query->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . ($active_only ? '1' : '0'));
    return $query->getResult();
  }
  
  
	public function findWithAreas( $id ) {
		$query = $this->getEntityManager()
				->createQuery(
							"SELECT s, a FROM SocialSnackWsBundle:State s
							JOIN s.areas a
							WHERE s.id = :id
							ORDER BY s.order DESC, s.name, a.name"
				)
				->setParameter( 'id', $id );
    
    $query->useResultCache( TRUE, 60 * 60, __METHOD__ . ':' . $id );
		
		try {
			return $query->getSingleResult();
    } catch (\Doctrine\ORM\NoResultException $e) {
			return null;
    }
	}
	
	
	public function findWithAreasAndCinemas( $id, $args = array() ) {
    $qb = $this->getEntityManager()->createQueryBuilder();
		$qb->addSelect('s')
        ->addSelect('a')
        ->addSelect('c')
        ->from('SocialSnackWsBundle:State', 's')
				->join('s.areas', 'a')
        ->join('a.cinemas', 'c')
        ->andWhere('s.id = :id')
        ->andWhere('s.active = 1')
        ->andWhere('a.active = 1')
        ->andWhere('c.active = 1')
        ->addOrderBy('s.order', 'DESC')
        ->addOrderBy('s.name')
        ->addOrderBy('a.name')
        ->addOrderBy('c.name')
				->setParameter( 'id', $id );
    
    foreach ( $args as $k => $v ) {
      $param = 'param_' . str_replace('.', '_', $k);
      $qb->andWhere("$k = :$param")
          ->setParameter($param, $v);
    }
    
		$query = $qb->getQuery();
    
    $cache_id = __METHOD__ . ':' . $id . '_' . @http_build_query( $args );
    $query->useResultCache( TRUE, 60 * 60, $cache_id );
    
		try {
			return $query->getSingleResult();
    } catch (\Doctrine\ORM\NoResultException $e) {
			return null;
    }
	}
	
	public function findAllWithAreas() {
		$query = $this->getEntityManager()
				->createQuery(
							"SELECT s, a FROM SocialSnackWsBundle:State s
							JOIN s.areas a
              WHERE a.active = 1 AND s.active = 1
              ORDER BY s.order DESC, s.name, a.name"
				);
		
    $query->useResultCache( TRUE, 60 * 60, __METHOD__ );

    return $query->getResult();
	}
	
	public function findAllWithAreasAndCinemas() {
		$query = $this->getEntityManager()
				->createQuery(
							"SELECT s, a, c FROM SocialSnackWsBundle:State s
							JOIN s.areas a
							JOIN a.cinemas c
              WHERE c.active = 1
							ORDER BY s.order DESC, s.name, a.name, c.name"
				);
		
    $query->useResultCache( TRUE, 60 * 60, __METHOD__ );

    return $query->getResult();
	}

  public function findAllStatesAndCinemas(){
    $rv = array();

    $query = $this->getEntityManager()->createQueryBuilder();
    $query->addSelect('s.id,s.name')
        ->from('SocialSnackWsBundle:State','s')
        ->andWhere('s.active = 1');
    $db = $query->getQuery()->getArrayResult();

    foreach($db as $d){
      $rv[$d['id']] = $d;
    }

    foreach($rv as $r){
      $query = $this->getEntityManager()
          ->createQuery(
              "SELECT c.legacy_name, IDENTITY(c.state),c.id FROM SocialSnackWsBundle:Cinema c
                WHERE c.state = ".$r['id']
          );
      $rv[$r['id']]['cinemas'] = $query->getArrayResult();
    }


   return $rv;
  }
	
}
