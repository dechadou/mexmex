<?php

namespace SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;

abstract class AbstractLoadData implements FixtureInterface {

  protected $cinema;

  public function load(ObjectManager $om) {
    $faker = Faker\Factory::create();

    $this->setUpCinema($om, $faker);
    $this->setUpMovies($om, $faker);

    $om->flush();
  }


  abstract protected function setUpMovies(ObjectManager $om, Faker\Generator $faker);


  protected function setUpCinema(ObjectManager $om, Faker\Generator $faker) {
    $state = new State();
    $state
        ->setActive(TRUE)
        ->setLegacyId($faker->randomNumber())
        ->setLegacyName($faker->state())
        ->setName($faker->state())
        ->setOrder(1)
        ->setTimezone('America/Mexico_City')
    ;
    $om->persist($state);

    $area = new StateArea();
    $area
        ->setLegacyName($faker->city())
        ->setLegacyId($faker->randomNumber())
        ->setName($faker->city())
        ->setActive(TRUE)
        ->setLegacyStateId($faker->randomNumber())
        ->setState($state)
    ;
    $om->persist($area);

    $cinema = new Cinema();
    $cinema
        ->setState($state)
        ->setActive(TRUE)
        ->setName($faker->company())
        ->setArea($area)
        ->setLegacyId($faker->randomNumber())
        ->setLegacyName($faker->company())
        ->setInfo('{}')
        ->setPlatinum(FALSE)
        ->setLat($faker->randomFloat())
        ->setLng($faker->randomFloat())
    ;
    $om->persist($cinema);
    $this->cinema = $cinema;
  }

}