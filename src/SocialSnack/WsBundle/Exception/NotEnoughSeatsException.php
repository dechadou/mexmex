<?php

namespace SocialSnack\WsBundle\Exception;

/**
 * Class NotEnoughSeatsException
 * @package SocialSnack\WsBundle\Exception
 * @author Guido Kritz
 */
class NotEnoughSeatsException extends RequestMsgException {

}