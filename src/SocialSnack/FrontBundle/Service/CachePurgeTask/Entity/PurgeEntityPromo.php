<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityPromo extends AbstractEntityTask implements UrlTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('promos_archive'),
            'env'  => 'front'
        ],
    ];
  }

}