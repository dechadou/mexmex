<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email',         'email',    array( 'label' => 'Email ' ) );
        $builder->add('first_name',    'text',     array( 'label' => 'Nombre ' ));
        $builder->add('last_name',     'text',     array( 'label' => 'Apellido ' ));
        $builder->add('plainPassword', 'password', array( 'label' => 'Contraseña ' ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SocialSnack\FrontBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}