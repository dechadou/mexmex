<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\PromoAType;
use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\PromoA;

class PromoAController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="social_snack_promoa_list", defaults={"active"=true})
   * @Route("/inactive", name="social_snack_promoa_list_inactive", defaults={"active"=false})
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction($active = TRUE) {
    $this->check_roles();

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:PromoA');
    $promos = $repo->findActive($active);

    return $this->render('SocialSnackAdminBundle:PromoA:list.html.php', array(
        'promosa' => $promos,
        'active'  => $active,
    ));
  }


  /**
   * @Route("/add", name="social_snack_promoa_add", defaults={"promoa_id"=null})
   * @Route("/{promoa_id}", name="social_snack_promoa")
   *
   * @param null|integer $promoa_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function editAction($promoa_id = NULL, Request $request) {
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:PromoA');
    $purge = NULL;
    $session = $request->getSession();

    if (is_null($promoa_id)) {
      $promo = new PromoA();
    } else {
      $promo = $repo->find($promoa_id);
    }

    $form = $this->createForm(new PromoAType(), $promo);
    $form->add('save', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      $em->persist($promo);
      $em->flush();

      $base_path = 'promosa/';
      if ($file = $form["image"]->getData()) {
        $filename = $promo->getId() . "-promo.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 1020, 129);
        $promo->setImage($base_path . $filename);
      }

      $em->flush();

      $purger = $this->container->get('ss.cache_purger');
      $purge = $purger->purge_cache(array('entity' => $promo));

      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);

      return $this->redirect($this->generateUrl(
          'social_snack_promoa',
          array('promoa_id' => $promo->getId())
      ));
    }

    return $this->render('SocialSnackAdminBundle:PromoA:edit.html.php', array(
        'promo'      => $promo,
        'form'       => $form->createView(),
        'purge'      => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/delete/{promoa_id}", name="social_snack_promoa_delete")
   *
   * @param $promoa_id
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteAction($promoa_id) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:PromoA');
    $promo = $repo->find($promoa_id);
    $em->remove($promo);
    $em->flush();
    return $this->redirect($this->generateUrl('social_snack_promoa_list'));
  }

}
