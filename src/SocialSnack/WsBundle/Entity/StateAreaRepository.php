<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class StateAreaRepository extends EntityRepository {
  
	public function findWithCinemas( $id, $args = array() ) {
    $qb = $this->createQueryBuilder('a');
		$qb->addSelect('a')
        ->addSelect('c')
        ->join('a.cinemas', 'c')
        ->andWhere('a.id = :id')
        ->andWhere('a.active = 1')
        ->andWhere('c.active = 1')
        ->addOrderBy('c.name')
				->setParameter( 'id', $id );
    
    foreach ( $args as $k => $v ) {
      $param = 'param_' . str_replace('.', '_', $k);
      $qb->andWhere("$k = :$param")
          ->setParameter($param, $v);
    }
    
		$query = $qb->getQuery();
    
    $cache_id = @http_build_query( $args );
    $query->useResultCache( TRUE, 60 * 60, 'StateAreaRepository:findWithCinemas_' . $cache_id );
    
		try {
			return $query->getSingleResult();
    } catch (\Doctrine\ORM\NoResultException $e) {
			return null;
    }
	}
  
}