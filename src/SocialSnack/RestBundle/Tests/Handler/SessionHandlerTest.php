<?php

namespace SocialSnack\RestBundle\Tests\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\RestBundle\Handler\SessionHandler;
use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;
use SocialSnack\WsBundle\Entity\Session;

/**
 * Class SessionHandlerTest
 * @package SocialSnack\RestBundle\Tests\Handler
 * @author Guido Kritz
 */
class SessionHandlerTest extends WebTestCase {

  /**
   * @param Registry $doctrine
   * @return SessionHandlerTesteable
   */
  protected function getHandler(Registry $doctrine = NULL) {
    $reflection = new \ReflectionClass('\SocialSnack\RestBundle\Tests\Handler\SessionHandlerTesteable');
    return $reflection->newInstanceArgs($this->getHandlerArgs($doctrine));
  }


  /**
   * @param array $methods
   * @param bool  $call_constructor
   * @param null  $constructor_args
   * @return SessionHandlerTesteable
   */
  protected function getHandlerMock(array $methods = [], $call_constructor = TRUE, $constructor_args = NULL) {
    $handler = $this->getMockBuilder('\SocialSnack\RestBundle\Tests\Handler\SessionHandlerTesteable')
        ->setMethods($methods);

    if (is_null($constructor_args)) {
      $constructor_args = $this->getHandlerArgs();
    }

    if (!$call_constructor) {
      $handler->disableOriginalConstructor();
    } else {
      $handler->setConstructorArgs($constructor_args);
    }

    return $handler->getMock();
  }


  protected function getHandlerArgs(Registry $doctrine = NULL) {
    return [
        $doctrine ?: $this->getContainer()->get('doctrine')
    ];
  }


  public function testGet() {
    // @todo Mock the repository instead
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ]);

    $handler = $this->getHandlerMock(['filterTicketsForMovieRating']);
    $handler->expects($this->once())
        ->method('filterTicketsForMovieRating')
        ->willReturn([]);

    $output = $handler->getSerialized(LoadSessionData::$sessions[0]->getId());
    $this->assertInternalType('array', $output);
  }


  protected function _testFilterTicketsForMovieRating($rating, $expected) {
    $tickets = [
        [
            'name'  => 'Adulto',
            'price' => 6000
        ],
        [
            'name'  => 'Mayor 60',
            'price' => 4500
        ],
        [
            'name'  => 'Menor',
            'price' => 4500
        ],
        [
            'name'  => 'Platino Menor CX 3D',
            'price' => 9000
        ],
    ];

    $handler = $this->getHandler();
    $output  = $handler->filterTicketsForMovieRating($tickets, $rating);

    $this->assertEquals($expected, $output);
  }


  public function testFilterTicketsForMovieRatingB() {
    $expected = [
        [
            'name'  => 'Adulto',
            'price' => 6000
        ],
        [
            'name'  => 'Mayor 60',
            'price' => 4500
        ],
        [
            'name'  => 'Menor',
            'price' => 4500
        ],
        [
            'name'  => 'Platino Menor CX 3D',
            'price' => 9000
        ],
    ];

    $this->_testFilterTicketsForMovieRating('B', $expected);
  }


  public function testFilterTicketsForMovieRatingC() {
    $expected = [
        [
            'name'  => 'Adulto',
            'price' => 6000
        ],
        [
            'name'  => 'Mayor 60',
            'price' => 4500
        ],
    ];

    $this->_testFilterTicketsForMovieRating('C', $expected);
  }


  public function testFilterTicketsForMovieRatingD() {
    $expected = [
        [
            'name'  => 'Adulto',
            'price' => 6000
        ],
        [
            'name'  => 'Mayor 60',
            'price' => 4500
        ],
    ];

    $this->_testFilterTicketsForMovieRating('D', $expected);
  }


  protected function _testFilterTicketsForSession(\DateTime $sessionDate, array $expected = NULL, array $tickets = NULL, $platinum = FALSE, $groupId = NULL) {
    if (is_null($tickets)) {
      $tickets = [
          ['name' => 'Adulto', 'price' => 6000],
          ['name' => '20 ANIV', 'price' => 6000],
          ['name' => 'Menor', 'price' => 6000],
          ['name' => '20 ANIV X4D', 'price' => 4500],
          ['name' => 'Mayor 60', 'price' => 6000],
          ['name' => '20 aniv', 'price' => 4500],
          ['name' => '2x1', 'price' => 6000],
          ['name' => 'MIERCOLES', 'price' => 4500],
          ['name' => 'Platino', 'price' => 6000],
          ['name' => 'MIÉRCOLES', 'price' => 4500],
          ['name' => 'Platino 3D', 'price' => 6000],
          ['name' => 'miércoles', 'price' => 4500],
          ['name' => 'MNF Adulto', 'price' => 4500],
          ['name' => 'BALLET PLATINO', 'price' => 4500],
          ['name' => 'EVENTO ESPECIAL', 'price' => 4500],
          ['name' => 'EVENTO EN VIVO', 'price' => 4500],
          ['name' => 'OBELA 2X1 PAQ', 'price' => 10000],
      ];
    }

    if (is_null($expected)) {
      $expected = $tickets;
    }

    $session = $this->getMock('SocialSnack\WsBundle\Entity\Session');
    $session->method('getDate')->willReturn($sessionDate);
    $session->method('getGroupId')->willReturn($groupId);
    if ($platinum) {
      $session->method('getTypes')->willReturn(['platinum']);
    }

    $doctrine = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();

    $handler = $this->getHandler($doctrine);
    $output  = $handler->filterTicketsForSession($tickets, $session);

    $this->assertSame($expected, $output);
  }

  public function testFilterTicketsForSession_Before() {
    $sessionDate = new \DateTime('2015-12-17');
    $this->_testFilterTicketsForSession($sessionDate);
  }

  public function testFilterTicketsForSession_After() {
    $sessionDate = new \DateTime('2016-02-01');
    $this->_testFilterTicketsForSession($sessionDate);
  }

  public function testFilterTicketsForSession_jan28() {
    $tickets = [
        ['name' => 'Adulto', 'price' => 6000],
        ['name' => 'CMX 20', 'price' => 6000],
        ['name' => 'Menor', 'price' => 6000],
        ['name' => 'Mayor 60', 'price' => 6000],
        ['name' => 'CMX 40', 'price' => 6000],
        ['name' => '20 ANIV', 'price' => 6000],
        ['name' => 'Platino 20 ANIV', 'price' => 6000],
        ['name' => 'CMX 30', 'price' => 6000],
    ];
    $expected = [
        ['name' => 'CMX 20', 'price' => 6000],
        ['name' => 'CMX 40', 'price' => 6000],
        ['name' => 'CMX 30', 'price' => 6000],
    ];
    $sessionDate = new \DateTime('2016-01-28');
    $this->_testFilterTicketsForSession($sessionDate, $expected, $tickets);
  }

  public function testFilterTicketsForSession_Filter_Array() {
    $expected = [
        [ 'name'  => '20 ANIV', 'price' => 6000 ],
        [ 'name'  => '20 ANIV X4D', 'price' => 4500 ],
        [ 'name'  => '20 aniv', 'price' => 4500 ],
        [ 'name'  => 'MIERCOLES', 'price' => 4500 ],
        [ 'name'  => 'MIÉRCOLES', 'price' => 4500 ],
        [ 'name'  => 'miércoles', 'price' => 4500 ],
        [ 'name'  => 'MNF Adulto', 'price' => 4500 ],
        [ 'name'  => 'BALLET PLATINO', 'price' => 4500 ],
        [ 'name'  => 'EVENTO ESPECIAL', 'price' => 4500 ],
        [ 'name'  => 'EVENTO EN VIVO', 'price' => 4500 ],
        [ 'name'  => 'OBELA 2X1 PAQ', 'price' => 10000 ],
    ];
    $sessionDate = new \DateTime('2016-01-01');
    $this->_testFilterTicketsForSession($sessionDate, $expected);
  }

  public function testFilterTicketsForSession_Filter_Entities() {
    $ticket1 = $this->getMock('SocialSnack\WsBundle\Entity\Ticket');
    $ticket1->method('getDescription')->willReturn('20 ANIV');
    $ticket2 = $this->getMock('SocialSnack\WsBundle\Entity\Ticket');
    $ticket2->method('getDescription')->willReturn('Adulto');
    $tickets = [$ticket1, $ticket2];
    $expected = [$ticket1];

    $sessionDate = new \DateTime('2016-01-31');
    $this->_testFilterTicketsForSession($sessionDate, $expected, $tickets);
  }

}


class SessionHandlerTesteable extends SessionHandler {

  public function filterTicketsForMovieRating(array &$tickets, $rating) {
    return parent::filterTicketsForMovieRating($tickets, $rating);
  }

  public function filterTicketsForSession(array &$tickets, Session $session) {
    return parent::filterTicketsForSession($tickets, $session);
  }

}