<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Monitorear transacciones - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Monitorear transacciones
      </header>
      <div class="panel-body" id="dynamic">
        <?php if ($item) { ?>
          <?php foreach ($item['ccs'] as $cc) { ?>
          <div class="form-group">
            <div class="col-sm-2">Nombre</div>
            <div class=""><?php echo $cc['name']; ?></div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">CC</div>
            <div class=""><?php echo $cc['cc']; ?></div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">CSC</div>
            <div class=""><?php echo $cc['csc']; ?></div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">Vencimiento</div>
            <div class=""><?php echo $cc['expire_month'] . ' / ' . $cc['expire_year']; ?></div>
          </div>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="panel-body">
        <div class="btns">
          <?php if (!$item) { ?>
          <a href="#" class="btn btn-default start-capture">Comenzar</a>
          <?php } else { ?>
          <a href="#" class="btn btn-default regen-capture">Regenerar datos</a>
          <a href="#" class="btn btn-default stop-capture">Detener</a>
          <?php } ?>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  function remove_btns() {
    $('.btns .btn').remove();
  }
  function build_start_btns() {
    $('<a/>', { href : '#', 'class' : 'btn btn-default start-capture', text : 'Comenzar' }).appendTo('.btns');
    $('.btns .btn').after(' ');
  }
  
  function build_stop_btns() {
    $('<a/>', { href : '#', 'class' : 'btn btn-default regen-capture', text : 'Regenerar datos' }).appendTo('.btns');
    $('<a/>', { href : '#', 'class' : 'btn btn-default stop-capture', text : 'Detener' }).appendTo('.btns');
    $('.btns .btn').after(' ');
  }
  
  function build_block_btns() {
    $('<a/>', { href : '#', 'class' : 'btn btn-default block-capture', text : 'Bloquear' }).appendTo('.btns');
    $('.btns .btn').after(' ');
  }
  
  function build_cc_info(cc) {
    var $div = $('<div/>');
    
    var obj = [
      {
        label : 'Nombre',
        data  : cc.name
      },
      {
        label : 'CC',
        data  : cc.cc
      },
      {
        label : 'CSC',
        data  : cc.csc
      },
      {
        label : 'Vencimiento',
        data  : cc.expire_month + ' / ' + cc.expire_year
      }
    ];
    
    $.each(obj, function(i, item) {
      var $row  = $('<div/>', { 'class' : 'form-group' }),
          $label = $('<div/>', { 'class' : 'col-sm-2', text : item.label }),
          $info  = $('<div/>', { text : item.data })
      ;
      
      $row.append($label);
      $row.append($info);
      $div.append($row);
    });
    
    $('#dynamic').children().remove();
    $('#dynamic').append($div);
  }
  
  function build_cap_info(cap) {
    
    if (!cap) {
      var $div = $('<div/>', { 'class' : 'alert alert-warning', text : 'No se capturó ninguna transacción.' });
      build_start_btns();
    } else {
      build_block_btns();
      var $div = $('<div/>');

      var obj = [
        {
          label : 'Función ID',
          data  : cap.session.id
        },
        {
          label : 'Fecha',
          data  : cap.session.date
        },
        {
          label : 'Complejo',
          data  : cap.session.cinema
        },
        {
          label : 'Película',
          data  : cap.session.movie
        },
        {
          label : 'IP',
          data  : cap.client_ip
        }
      ];

      $.each(obj, function(i, item) {
        var $row  = $('<div/>', { 'class' : 'form-group' }),
            $label = $('<div/>', { 'class' : 'col-sm-2', text : item.label }),
            $info  = $('<div/>', { text : item.data })
        ;

        $row.append($label);
        $row.append($info);
        $div.append($row);
      });

      var $input = $('<input/>', { id : 'block-ip', 'value' : cap.client_ip, type : 'hidden' });
      $div.append($input);
    }
    
    $('#dynamic').children().remove();
    $('#dynamic').append($div);
  }
  
  $('.btns').delegate('.start-capture, .regen-capture', 'click', function(e) {
    $('#dynamic').children().remove();
    e.preventDefault();
    remove_btns();
    
    $.ajax({
      url      : '<?php echo $view['router']->generate('social_snack_admin_report_monitor_start'); ?>',
      data     : {
        nocache : new Date().getTime(),
        force   : $(this).hasClass('regen-capture') ? 1 : 0
      },
      dataType : 'json',
      success  : function(res) {
        build_cc_info(res.ccs[0]);
        build_stop_btns();
      }
    });
  });
  
  $('.btns').delegate('.stop-capture', 'click', function(e) {
    e.preventDefault();
    remove_btns();
    
    $.ajax({
      url      : '<?php echo $view['router']->generate('social_snack_admin_report_monitor_stop'); ?>',
      data     : { nocache : new Date().getTime() },
      dataType : 'json',
      success  : function(res) {
        build_cap_info(res.cap);
      }
    });
  });
  
  $('.btns').delegate('.block-capture', 'click', function(e) {
    e.preventDefault();
    remove_btns();
    
    $.ajax({
      url      : '<?php echo $view['router']->generate('social_snack_admin_block_ip'); ?>',
      data     : { ip : $('#block-ip').val() },
      type     : 'post',
      dataType : 'json',
      success  : function(res) {
        $('#dynamic').children().remove();
        var $div = $('<div/>', { 'class' : 'alert alert-success', text : 'La IP fue bloqueada. El cambio se reflejará en unos minutos.' });
        $('#dynamic').append($div);
      }
    });
  });
</script>
<?php $view['slots']->stop(); ?>