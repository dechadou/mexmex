<?php
$view->extend('SocialSnackFrontBundle:JobApplication:base.html.php');
$view['slots']->set('title', 'Solicitud de Empleo - Cinemex');
?>

<?php $view['slots']->start('jobs_body'); ?>
<article class="entry-full">
  <h1 class="content-box-title">Misión</h1>

  <div class="entry-body">
    <h3>Misión</h3>

    <ul>
      <li>Estamos dedicados a ser los mejores en divertir a la gente.</li>
    </ul>

    <h3>Filosofía</h3>

    <ul>
      <li>Cinemex es su gente</li>
      <li>Nuestra relación se sustenta en la confianza</li>
      <li>Somos arquitectos de lealtades</li>
      <li>Lo más importante es divertirse y sonreir</li>
    </ul>

    <h3>Valores</h3>

    <ul>
      <li>Buscar y provocar relaciones duraderas</li>
      <li>Creer que en esencia la gente actúa por buena fe</li>
      <li>Tener una actitud de dar siempre lo mejor</li>
      <li>Hacerlo todo con pasión</li>
      <li>Ejercer rectitud en todas nuestras acciones y decisiones</li>
      <li>Superar de manera continua nuestros estándares</li>
      <li>Ser consistentes en el pensar, decir y actuar</li>
      <li>Aceptar las diferencias y tratar a todos con dignidad</li>
      <li>Disfrutar y gozar todo lo que hacemos</li>
    </ul>
  </div>

</article>
<?php $view['slots']->stop(); ?>
