<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Reportes - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Reportes
      </header>
      <div class="panel-body">
        <form action="<?php echo $view['router']->generate('social_snack_admin_report_salesxformat'); ?>" method="post">
          <h5>Reporte de ventas por concepto por medio.</h5>
          <div class="row">
            <div class="col-sm-2">
              <select name="year" class="form-control">
<!--                <option value="">Todos los años</option>-->
                <?php $start_year = 2013; $end_year = date('Y'); ?>
                <?php for ($y = $end_year ; $y >= $start_year ; $y--) { ?>
                <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Descargar</button>
            </div>
          </div>
        </form>
      </div>
        
      <div class="panel-body">
        <form action="<?php echo $view['router']->generate('social_snack_admin_report_ticketsxcinemaxapp'); ?>" method="post" id="ticketsxcinemaxapp">
          <h5>Reporte de ventas por cine por medio.</h5>
          <div class="row">
            <div class="col-sm-2">
              <select name="year" class="form-control">
                <!--                <option value="">Todos los años</option>-->
                <?php $start_year = 2013; $end_year = date('Y'); ?>
                <?php for ($y = $end_year ; $y >= $start_year ; $y--) { ?>
                  <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-2">
              <select name="month" class="form-control">
                <option value="">Todo el año</option>
                <?php foreach (['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'] as $i => $month) { ?>
                  <option value="<?php echo $i + 1; ?>"><?php echo $month; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Descargar</button>
            </div>
          </div>
        </form>
      </div>

      <div class="panel-body">
        <form action="<?php echo $view['router']->generate('social_snack_admin_report_transactionsbydate'); ?>" method="post">
          <h5>Reporte de ventas por día.</h5>
          <div class="row">
            <div class="col-sm-2">
              <?php $d = new \DateTime('yesterday'); ?>
              <input type="date" name="date" class="form-control" value="<?php echo $d->format('Y-m-d'); ?>" max="<?php echo $d->format('Y-m-d'); ?>" required />
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Descargar</button>
            </div>
          </div>
        </form>
      </div>

      <div class="panel-body">
        <form action="<?php echo $view['router']->generate('social_snack_admin_report_transactionbymovie'); ?>" method="post" id="transactionbymovie">
          <h5>Reporte de ventas por pelicula.</h5>
          <div class="row">
            <div class="col-sm-3">
              <select name="movie" class="form-control">
                <option value="">-- Seleccionar Pelicula --</option>

                <?php foreach($movies as $_movie){ ?>
                    <option value="<?php echo $_movie->getId()?>"><?php echo $_movie->getName() ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-2">

              <select name="year" class="form-control">
                <!--                <option value="">Todos los años</option>-->
                <?php $start_year = 2013; $end_year = date('Y'); ?>
                <?php for ($y = $end_year ; $y >= $start_year ; $y--) { ?>
                  <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-2">
              <select name="month" class="form-control">
                <option value="">Todo el año</option>
                <?php foreach (['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'] as $i => $month) { ?>
                  <option value="<?php echo $i + 1; ?>"><?php echo $month; ?></option>
                <?php } ?>
              </select>

            </div>
            <div class="col-sm-2">
              <select name="type" class="form-control">
                <option value="<?php echo \SocialSnack\AdminBundle\Handler\ReportHandler::MOVIE_SALES_REPORT_FULL; ?>">Reporte Completo</option>
                <option value="<?php echo \SocialSnack\AdminBundle\Handler\ReportHandler::MOVIE_SALES_REPORT_BY_TICKET; ?>">Por tipo de Ticket</option>
                <option value="<?php echo \SocialSnack\AdminBundle\Handler\ReportHandler::MOVIE_SALES_REPORT_BY_CONCEPT; ?>">Por Concepto</option>
                <option value="<?php echo \SocialSnack\AdminBundle\Handler\ReportHandler::MOVIE_SALES_REPORT_BY_VERSION; ?>">Por Versión</option>
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Descargar</button>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {

    $('select[name="movie"]').chosen({search_contains:true});

     $('#example').dataTable( {
      "order": [[ 0, "desc" ]],
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo $view['router']->generate('social_snack_admin_transactions_list_ajax'); ?>",
      "aoColumns": [
          null,
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false }
      ]
    });
    var table_api = $("#example").dataTable().api();

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input", function(e) { // Bind our desired behavior
          // If the length is 3 or more characters, or the user pressed ENTER, search
          if(this.value.length >= 3 || e.keyCode == 13) {
            // Call the API search function
            table_api.search(this.value).draw();
          }
          // Ensure we clear the search if they backspace far enough
          if(this.value == "") {
            table_api.search("").draw();
          }
          return;
        });
    
    $('#example').delegate('.block-ip', 'click', function(e) {
      e.preventDefault();
      var $t = $(this);
      var data = $t.data('post');
      
      if ($t.data('working')) {
        return;
      }
      
      if (!confirm('Si bloqueas la IP ' + data.ip + ' no se podrán realizar más transacciones desde ese origen. ¿Deseas continuar?')) {
        return;
      }
      
      $t.data({ working : true }).text('Procesando...');
      
      $.ajax({
        url : $t.attr('href'),
        type : 'post',
        data : data,
        dataType : 'json',
        success : function(res) {
          $t.replaceWith('Listo');
        },
        error : function() {
          alert('Ocurrió un error.');
          $t.data({ working : false }).text('Bloquear IP');
        }
      });
    });
  });


  $('#ticketsxcinemaxapp').bind('submit', function() {
    var $form = $(this);
    var y = $form.find('[name=year]').val();
    var m = $form.find('[name=month]').val();
    var d = new Date();

    if (!y) {
      return false;
    }

    if (y == d.getFullYear() && m > (d.getMonth() + 1)) {
      alert('La fecha seleccionada no puede ser posterior a la fecha actual.');
      return false;
    }
  });

</script>
<?php $view['slots']->stop(); ?>