<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use SocialSnack\FrontBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface {

  static $users = [];

  static $users_w_fb = [];

  static $users_wo_fb = [];


  public function load(ObjectManager $manager) {
    static::$users       = [];
    static::$users_w_fb  = [];
    static::$users_wo_fb = [];

    $user = new User();
    $user
        ->setEmail('homero@simpsons.com')
        ->setFirstName('Homero')
        ->setLastName('Simpson')
        ->setPassword('AIkq7VP8RAa6FeXuqf0bawW0fCDtzhQ10qNj3mGIdVMLLKULJkfRIEmMskSSOAIAL2rKHhZqLDUITbORUVIvoQ==') // foobar
        ->setSalt('5bb09e710a7df37213e57aec6363117d')
        ->setIecode(1234567890123456)
        ->setActive(TRUE)
        ->setFbuid(NULL)
    ;
    $manager->persist($user);
    static::$users[] = $user;
    static::$users_wo_fb[] = $user;

    $user = new User();
    $user
        ->setEmail('test2@test.com')
        ->setFirstName('Homer')
        ->setLastName('Simpson')
        ->setPassword('123') // @todo Replace with a well encoded password
        ->setIecode(1234567890123456)
        ->setFbuid(1234567890)
//        ->setCinema($this->getReference('Cinema-1'))
    ;
    $manager->persist($user);
    static::$users[] = $user;
    static::$users_w_fb[] = $user;

    $manager->flush();
  }

  public function getOrder() {
    return 5;
  }

}
