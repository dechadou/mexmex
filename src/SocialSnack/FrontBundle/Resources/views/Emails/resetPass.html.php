<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start( 'body' );
?>
<!-- BODY -->
<table class="body-wrap" bgcolor="#ebebeb" style="width: 100%; padding-bottom: 30px;">
	<tr>
		<td></td>
		<td class="container" align="" bgcolor="#ebebeb" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
				<table>
					<tr>
						<td>
							
							<h1 style="color: #d0033d">Recupera tu contraseña.</h1>
							<p>
								Para recuperar tu contraseña haz click en la siguiente liga:
							</p>
							<p>
                <a href="<?php echo $link; ?>" style="color:#d0033d;"><?php echo $link; ?></a>
							</p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->
      
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->
<?php
$view['slots']->stop();
?>