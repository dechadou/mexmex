<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData;
use Symfony\Component\HttpFoundation\Response;

class ArticlesControllerTest extends WebTestCase {

  public function setUp() {
    parent::setUp();

    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData'
    ]);
  }


  protected function getCollectionModel() {
    return [
        'articles' => [
            'type' => 'array',
            'each' => $this->getEntityModel(),
        ]
    ];
  }


  protected function getEntityModel() {
    return [
        'id'       => 'integer',
        'title'    => 'string',
        'content'  => 'string',
        'url'      => 'string',
        'cover'    => 'string',
        'thumb'    => 'string',
        'date'     => 'string',
        'tags'     => 'array',
        'featured' => 'boolean',
    ];
  }


  /**
   * @group functional
   */
  public function testDefault() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/v2/articles/',
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @todo Assert that the returned elements are actually relevant to the search.
   * @group functional
   */
  public function testSearch() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/v2/articles/search?q=foo',
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @group functional
   */
  public function testDetails() {
    $article_id = LoadArticlesData::$artices[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/v2/articles/%d', $article_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getEntityModel();
    $model['id'] = [
        'type'   => 'integer',
        'equals' => $article_id,
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

}