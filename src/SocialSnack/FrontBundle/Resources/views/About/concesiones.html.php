<img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-cinemex.jpg'); ?>"><?php $view->extend('SocialSnackFrontBundle:About:base.html.php'); ?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Concesiones</h1>

  <div class="quote lead">
    <p>Surtimos nuestros complejos con la mayor variedad y la mejor calidad de alimentos y bebidas que el mercado ofrece para consentir a nuestros invitados y asegurarnos que su experiencia en Cinemex sea la mejor.</p>
  </div>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-dulceria.png?4'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Dulcería</h2>

      <p>En nuestras dulcerías encontrarás variedad de alimentos para satisfacer todos tus antojos. Contamos con crujientes nachos,  sabrosos  hot dogs  y pizzas, diferentes tipos de helados y dulces, refrescos y por supuesto nuestras deliciosas palomitas.</p>
      <p>Orgullosamente, somos la única cadena de cines en México que ofrece “Palomitas Gourmet” creadas con maíz “Mushroom” el cual ofrece un mayor tamaño y mejor sabor. Contamos con 5 sabores de palomitas gourmet: mantequilla, caramelo, chile, light y queso cheddar.</p>
    </div>
  </div>

  <hr>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-mango.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Red Mango</h2>

      <p>Contamos con la franquicia maestra de helados Red Mango en México para ofrecer los mejores helados de yogurt del mercado, los cuales contienen ingredientes benéficos como cultivos vivos y activos, calcio y proteína. También contamos con smoothies y parfaits hechos con fruta natural para nuestros invitados que quieran elegir una opción más sana.</p>
      <p>Red mango es una franquicia internacional calificada con el primer lugar en el ranking de Premios 2011 “Zagat Fast Food Survey´s #1 Smoothie and Frozen Yogurt”</p>
    </div>
  </div>

  <hr>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-alavista.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Alavista</h2>

      <p>En Alavista se pueden disfrutar alimentos preparados al momento con productos frescos y la más alta calidad y por supuesto a la vista de nuestros invitados. Ofrece una gran variedad de chapatas y crepas tanto dulces como saladas.</p>
    </div>
  </div>

  <hr>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-cafecentral.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Café Central</h2>

      <p>En Cinemex, creamos Café Central  para ofrecer a nuestros invitados una excelente alternativa para disfrutar de un café o té de alta calidad acompañado de un delicioso pastel, muffin o panqué mientras disfrutan de su función.</p>
      <p>Nuestros invitados también encontrarán en Café Central bebidas frías como smoothies, frappes o chamoyadas.</p>
    </div>
  </div>

  <hr>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-lalocura.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>La locura</h2>

      <p>Como su nombre lo indica, La Locura crea un ambiente divertido con los mejores dulces a granel como gomitas, manguitos enchilados, checolines, nueces de la India, chocolates, cacahuates, entre otros.  El invitado puede escoger la cantidad y los sabores de su preferencia, y, al mismo tiempo se adentra en una experiencia divertida al ser ellos los creadores de su propio antojo.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
