<?php

namespace SocialSnack\RestBundle\Handler;
use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\User;
use SocialSnack\RestBundle\Entity\App;
use SocialSnack\RestBundle\Exception\InvalidResetPassCodeException;
use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\RestBundle\Exception\RestPassCodeUserMismatchException;
use SocialSnack\RestBundle\Exception\UserNotFoundException;
use SocialSnack\RestBundle\Service\Helper as RestHelper;
use SocialSnack\RestBundle\Service\Utils as RestUtils;
use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;

/**
 * Class AuthHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class AuthHandler {

  protected $container;

  protected $doctrine;

  protected $encoder_factory;

  protected $rest_utils;

  protected $contextHandler;

  public function __construct(ContainerInterface $container, Registry $doctrine, EncoderFactory $encoder_factory, RestUtils $rest_utils, ContextHandler $contextHandler) {
    $this->container       = $container;
    $this->doctrine        = $doctrine;
    $this->encoder_factory = $encoder_factory;
    $this->rest_utils      = $rest_utils;
    $this->contextHandler  = $contextHandler;
  }


  protected function getRepository() {
    return $this->doctrine->getRepository('SocialSnackFrontBundle:User');
  }


  protected function contextCanAuth() {
    $handler = $this->contextHandler;
    if (!$handler->getApiVersion() || $handler->getApiVersion() < 2) {
      return TRUE;
    }

    if (!$handler->getClient()->hasCap('direct_login')) {
      throw new AccessDeniedHttpException('Endpoint not allowed for this client.');
    }
  }


  public function auth(Request $request) {
    // Validate context first.
    $this->contextCanAuth();

    // Check required fields.
    $this->validateRequiredData($request);

    // Authenticate.
    $user = $this->handleAuthMethod($request);

    // Register GCM if available.
    $this->handleGCM($request, $user);

    return $user;
  }


  /**
   * Makes sure there's enough data to perform the login.
   * Otherwise throws an exception.
   *
   * @param Request $request
   * @return bool
   * @throws \SocialSnack\RestBundle\Exception\RestException
   * @throws \SocialSnack\RestBundle\Exception\MissingFieldsException
   */
  protected function validateRequiredData(Request $request) {
    $username       = $request->request->get('username') ? : $request->request->get('email');
    $password       = $request->request->get('password');
    $signed_request = $request->request->get('fbsigned_request');
    $access_token   = $request->request->get('fbaccess_token');

    if ( ( empty( $username ) || empty( $password ) ) && empty( $signed_request ) && empty( $access_token ) ) {
      throw new MissingFieldsException('Username and/or password and/or signed request missing.');
    }

    // Manual login and FB login fields can't be present at the same time.
    if ( ( !empty( $username ) || !empty( $password ) ) && ( !empty( $signed_request ) || !empty( $access_token ) ) ) {
      throw new RestException('invalid-data', 'Manual login and FB login can\'t be present at the same time.');
    }

    return TRUE;
  }


  /**
   * Decides which auth method should be used based on the input data and calls that method.
   *
   * @param Request $request
   * @return User
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function handleAuthMethod(Request $request) {
    $username       = $request->request->get('username') ? : $request->request->get('email');
    $password       = $request->request->get('password');
    $signed_request = $request->request->get('fbsigned_request');
    $access_token   = $request->request->get('fbaccess_token');

    // Authenticate user credentials.
    if ($username && $password) {
      return $this->authLogin($username, $password);
    }

    // Authenticate FB login.
    if ($signed_request || $access_token) {
      $fb_config = $this->getFbConfig($request);
      return $this->authFB($fb_config, $access_token, $signed_request);
    }

    // No auth method found for provided data.
    throw new RestException();
  }


  /**
   * Login with username and password.
   *
   * @param string $username
   * @param string $password
   * @return User
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function authLogin($username, $password) {
    $user = $this->getRepository()->findOneBy( array( 'email' => $username ) );

    if (!$user) {
      throw new RestException('invalid-credentials', 'Wrong login credentials.', 403);
    }

    $encoded_pass = $this->encodePassword($user, $password, $user->getSalt());

    if ($encoded_pass !== $user->getPassword()) {
      throw new RestException('invalid-credentials', 'Wrong login credentials.', 403);
    }

    return $user;
  }


  protected function encodePassword(User $user, $password, $salt) {
    $encoder_service = $this->encoder_factory;
    $encoder         = $encoder_service->getEncoder($user);

    return $encoder->encodePassword($password, $salt);
  }


  /**
   * Login with Facebook.
   *
   * @param array  $fb_config
   * @param string $access_token
   * @param string $signed_request
   * @return User
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function authFB($fb_config, $access_token = NULL, $signed_request = NULL) {/** @todo Use client IDs instead of this. */
    // Variables to verify the access token.
    $secret_key       = $fb_config['secret_key'];
    $app_id           = $fb_config['app_id'];
    $app_access_token = $fb_config['app_access_token'];
    $redirect_url     = '';

    if ($signed_request) {
      $fbuser = RestHelper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
    } else {
      $fbuser = RestHelper::validate_access_tokenX( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url );
    }

    $user = $this->getRepository()->findOneBy( array( 'fbuid' => $fbuser['user_id'] ) );

    if (!$user) {
      throw new RestException('invalid-credentials', 'Wrong login credentials.', 403);
    }

    return $user;
  }


  /**
   * Get FB config for the app requesting the login.
   *
   * @param Request $request
   * @return array
   */
  protected function getFbConfig(Request $request) {
    $from_frontend = $request->request->get( 'from_frontend' );

    if ($from_frontend) {
      $fb_config      = $this->container->getParameter( 'fb' );
    } else {
      $mobile_config  = $this->container->getParameter( 'mobile_app' );
      $fb_config      = $mobile_config['fb'];
    }

    return $fb_config;
  }


  /**
   * If GCM parameters are present, associate them to the user's account.
   *
   * @param Request $request
   * @param User    $user
   * @return bool|\SocialSnack\RestBundle\Entity\GCM
   */
  protected function handleGCM(Request $request, User $user) {
    if (!$request->request->has('uuid') || !$request->request->has('gcmid') || !$request->request->has('type')) {
      return FALSE;
    }

    return $this->rest_utils->setGCM(
        $request->request->get('uuid'),
        $request->request->get('gcmid'),
        $request->request->get('type'),
        $user
    );
  }


  public function generateOauthCode(User $user, App $app, $time) {
    return md5(implode('.', [$user->getId(), $time, $app->getSecretKey()]));
  }


  public function buildToken(User $user, App $app = NULL) {
    $app_config      = $this->container->getParameter('mobile_app');
    $secret_key      = $app_config['secret_key'];

    if ($app) {
      $secret_key .= ':' . $app->getSecretKey();
    }

    $firewall        = 'secured_area';
    $token           = new UsernamePasswordToken($user->getUsername(), $user->getPassword(), $firewall, array('ROLE_MOBILE'));
    $access_token    = $user->getId() . '.' . hash_hmac('sha256', serialize($token), $secret_key);

    return $access_token;
  }


  public function buildAuthResponse(User $user, Request $request, $from_frontend = FALSE) {
    if (!$from_frontend) {
      // 3th party app login via API.
      $access_token  = $this->buildToken($user, $this->contextHandler->getApiVersion() >= 2 ? $this->contextHandler->getClient() : NULL);
      $user_info     = $this->container->get('rest.user.handler')->serializeOne($user);

      return new JsonResponse( array(
          'access_token' => $access_token,
          'user_info'    => $user_info,
      ), 200 );

    } else {
      // Login from Cinemex website.
      //
      // Login the user.
      $token = new UsernamePasswordToken( $user, $user->getPassword(), 'main', $user->getRoles());
      $this->container->get("security.context")->setToken($token);
      $key = $this->container->getParameter('secret'); // your security key from parameters.yml
//      $token = new RememberMeToken($user, 'main', $key);
//      $this->get('security.context')->setToken($token);

      // Fire the login event
      // Logging the user in above the way we do it doesn't do this automatically
      $event = new InteractiveLoginEvent( $request, $token );
      $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);

      // write cookie for persistent session storing
      $providerKey = 'main'; // defined in security.yml
      $securityKey = $key; // defined in security.yml

      $userProvider = new EntityUserProvider($this->doctrine, 'SocialSnack\FrontBundle\Entity\User', 'username');

      $rememberMeService = new TokenBasedRememberMeServices(array($userProvider), $securityKey, $providerKey, array(
              'path' => '/',
              'name' => 'REMEMBERME',
              'domain' => null,
              'secure' => false,
              'httponly' => true,
              'lifetime' => 31536000, // 365 days
              'always_remember_me' => true,
              'remember_me_parameter' => '_remember_me')
      );

      $response = new JsonResponse();
      $rememberMeService->loginSuccess($request, $response, $token);

      // Set the welcome message.
      $request->getSession()->getFlashBag()->add( 'notice', sprintf( '¡Bienvenid@ <strong>%s</strong>! Gracias por registrarte en Cinemex.', $user->getFirstName() ) );

      // Initialize client side cache.
      $res  = $this->container->get( 'ss.front.utils' )->get_user_login_data( $user );
      $response->setData( array( 'success' => TRUE, 'data' => $res, 'redirect' => $this->container->get('router')->generate('user_profile') ) );
      return $response;
    }
  }


  /**
   * @param $code
   * @return \SocialSnack\FrontBundle\Entity\ResetCode
   */
  public function getResetPassCode($code) {
    return $this->doctrine
        ->getRepository('SocialSnackFrontBundle:ResetCode')
        ->findOneBy(array(
            'code'   => $code,
            'active' => 1
        ));
  }


  public function resetPass($code, $plainPassword, $email) {
    $em        = $this->doctrine->getManager();
    $user_repo = $this->doctrine->getRepository('SocialSnackFrontBundle:User');

    $code = $this->getResetPassCode($code);
    if (!$code) {
      throw new InvalidResetPassCodeException();
    }

    $user = $user_repo->findOneBy(array('email' => $email));
    if (!$user) {
      throw new UserNotFoundException();
    }

    // Make sure the provided code was generated for the user.
    if ($code->getUser()->getId() !== $user->getId()) {
      throw new RestPassCodeUserMismatchException();
    }

    // Set the new password.
    $password = $this->encodePassword($user, $plainPassword, $user->getSalt());
    $user->setPassword($password);

    // Disable the reset code (reset codes are valid for just one use).
    $code->setActive(FALSE);

    $em->flush();

    return TRUE;
  }

}