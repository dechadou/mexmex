<?php if ($widget == 'single_text') : ?>
  <?php echo $view['form']->block($form, 'form_widget_simple'); ?>
<?php else : ?>
  <div <?php echo $view['form']->block($form, 'widget_container_attributes'); ?>>
    <?php
    echo str_replace(
        array(
            '{{ year }}', '{{ month }}', '{{ day }}'
        ),
        array(
            $view['form']->widget($form['year'],  array('attr' => array('class' => 'month-year month'))),
            $view['form']->widget($form['month'], array('attr' => array('class' => 'month-year month'))),
            $view['form']->widget($form['day'],   array('attr' => array('class' => 'month-year month'))),
        ),
        $date_pattern
    );
    ?>
  </div>
<?php endif; ?>
