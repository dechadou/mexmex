<?php

namespace SocialSnack\WsBundle\Exception\MovieGroup;

/**
 * Class GrouperFromGrouperException
 * @package SocialSnack\WsBundle\Exception\MovieGroup
 * @author Guido Kritz
 */
class GrouperFromGrouperException extends \Exception {

} 