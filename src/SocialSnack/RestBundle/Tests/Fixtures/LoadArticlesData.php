<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Article;

class LoadArticlesData implements FixtureInterface {

  static $artices;

  public function load(ObjectManager $manager) {
    self::$artices = [];

    $now = new \DateTime('now');

    $article1 = new Article();
    $article1
        ->setContent('<p>First paragraph.</p><p>Second paragraph.</p>')
        ->setCover('cover.jpg')
        ->setDateCreated($now)
        ->setDateUpdated($now)
        ->setFeatured(TRUE)
        ->setStatus(Article::STATUS_PUBLISH)
        ->setTags('foo,article1')
        ->setThumb('thumb.jpg')
        ->setTitle('Article 1 title')
    ;
    self::$artices[] = $article1;
    $manager->persist($article1);

    $article2 = new Article();
    $article2
        ->setContent('<p>First paragraph.</p><p>Second paragraph.</p>')
        ->setCover('cover.jpg')
        ->setDateCreated($now)
        ->setDateUpdated($now)
        ->setFeatured(FALSE)
        ->setStatus(Article::STATUS_PUBLISH)
        ->setTags('bar,article2')
        ->setThumb('thumb.jpg')
        ->setTitle('Article 2 title')
    ;
    self::$artices[] = $article2;
    $manager->persist($article2);

    $article3 = new Article();
    $article3
        ->setContent('<p>First paragraph.</p><p>Second paragraph.</p>')
        ->setCover('cover.jpg')
        ->setDateCreated($now)
        ->setDateUpdated($now)
        ->setFeatured(FALSE)
        ->setStatus(Article::STATUS_PUBLISH)
        ->setTags('foo,article3')
        ->setThumb('thumb.jpg')
        ->setTitle('Article 3 title')
    ;
    self::$artices[] = $article3;
    $manager->persist($article3);

    $manager->flush();
  }

}