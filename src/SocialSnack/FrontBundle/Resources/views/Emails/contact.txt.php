Cinemex | cinemex.com

Hemos recibido un nuevo contacto en el Buzón Cinemex.

Nombre: <?php echo $contact->getName(); ?>

Email: <?php echo $contact->getEmail(); ?>

<?php if ( $value = $contact->getPhone() ) { ?>
Teléfono: <?php echo $value; ?>
<?php } ?>

<?php if ( $value = $contact->getIe() ) { ?>
Invitado N°: <?php echo $value; ?>
<?php } ?>

<?php if ( $cinema ) { ?>
Cine: <?php echo $cinema->getName(); ?>
<?php } ?>

<?php if ( $value = $contact->getService() ) { ?>
Servicio: <?php echo $value; ?>
<?php } ?>

<?php if ( $value = $contact->getType() ) { ?>
Tipo de mensaje: <?php echo $value; ?>
<?php } ?>

Mensaje:
<?php echo $contact->getMsg(); ?>

Términos y condiciones | <?php echo $view['router']->generate('tos_page', array(), TRUE); ?>