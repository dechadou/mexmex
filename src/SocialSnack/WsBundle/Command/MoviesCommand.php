<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\MovieInfo;
use SocialSnack\WsBundle\Request\ConsultaDetallada;

class MoviesCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:movies' )
			->setDescription( 'Fetch movies from the Web Service' )
      ->addOption( 'cinema_id', 'cid', InputOption::VALUE_OPTIONAL );
	}
	
	
	/**
	 * Fetch movies and movies info from the WS.
	 * 
	 * Since it doesn't seem to exist an effective method to retrieve the complete
	 * movie listing from the WS (all methods seems to return each movie repeated
	 * for every single cinema resulting on an unnecessary huge response with
	 * occasional timeouts) we are requesting movies by state to limit the size of
	 * the response.
	 * 
	 * @param \Symfony\Component\Console\Input\InputInterface $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 * @return type
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$time_start = microtime(true); 

		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
    
    if ( $cinema_id = $input->getOption( 'cinema_id' ) ) {
      $cinemas      = $cinema_repo->findBy( array( 'id' => $cinema_id ) ); 
    } else {
      $cinemas      = $cinema_repo->findBy( array( 'active' => 1 ) );
    }
		
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
		foreach ( $cinemas as $cinema ) {
			$output->writeln( sprintf( 'Cinema %s', $cinema->getName() ) );
      $this->getContainer()->get('old_sound_rabbit_mq.update_movies_producer')->publish( $cinema->getId() );
		}
		
		
		/* @todo Set active = 0 to those movies which are not present in the response. */
		
		$output->writeln( 'Didn\'t fail, yay!' );
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		
		$output->writeln( 'Total Execution Time: '. $execution_time . ' Mins' );
		
		
		
		return;
	}
	
	
}