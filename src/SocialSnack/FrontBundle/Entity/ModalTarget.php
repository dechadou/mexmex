<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Entity ModalTarget
 *
 * @ORM\Table(name="ModalTargets")
 * @ORM\Entity
 */
class ModalTarget {

  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var int
   *
   * @ORM\Column(name="modal_id", type="integer")
   */
  private $modal_id;

  /**
   * @var Modal
   *
   * @ORM\ManyToOne(targetEntity="Modal", inversedBy="targets")
   * @ORM\JoinColumn(name="modal_id", referencedColumnName="id", onDelete="CASCADE")
   */
  private $modal;


  /**
   * @var string
   * @ORM\Column(name="target", type="string")
   */
  private $target = null;

  /**
   * @var string
   * @ORM\Column(name="targetName", type="string")
   */
  private $targetName = null;


  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }


  /**
   * Set modal
   *
   * @param \SocialSnack\FrontBundle\Entity\Modal $modal
   * @return ModalCinemas
   */
  public function setModal(\SocialSnack\FrontBundle\Entity\Modal $modal = null)
  {
    $this->modal = $modal;

    return $this;
  }

  /**
   * Get modal_id
   *
   * @return Modal
   */
  public function getModalId()
  {
    return $this->modal_id;
  }


  /**
   * Set target
   *
   * @param string $target
   * @return Modal
   */
  public function setTarget($target)
  {
    $this->target = $target;
    return $this;
  }

  public function deleteTargets($target){

  }

  /**
   * get target
   *
   * @param string $target
   * @return Modal
   */
  public function getTarget(){
    return $this->target;
  }

  /**
   * Set targetName
   *
   * @param string $targetName
   * @return Modal
   */
  public function setTargetName($targetName)
  {
    $this->targetName = $targetName;
    return $this;
  }

  /**
   * get targetName
   *
   * @param string $targetName
   * @return Modal
   */
  public function getTargetName(){
    return $this->targetName;
  }





}