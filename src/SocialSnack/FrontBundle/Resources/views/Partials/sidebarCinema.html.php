<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div id="sidebar-mycinema">
<?php
echo $this->render( 'SocialSnackFrontBundle:Partials:sidebar/cinemaHeader.html.php', array(
    'date'            => $date,
    'cinema'          => $cinema,
    'cinema_platinum' => $cinema_platinum,
    'cinema_standard' => $cinema_standard,
    'single'          => $single,
    'available_dates' => $available_dates
));
?>

    <div class="mycinema-list" rel="main-list">
        <?php if (!sizeof($sessions)) : ?>
        No se encontraron funciones en este cine.
        <?php endif; ?>

        <?php foreach ($sessions as $_movie) : ?>
        <div class="mycinema-li">
            <span class="mycinema-item-rating" title="Clasificación"><?php echo $_movie['parent']->getData('RATING'); ?></span>

            <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="mycinema-item-title"><?php echo $_movie['parent']->getName(); ?></a>

            <?php
            echo $this->render('SocialSnackFrontBundle:Partials:sidebar/movieSessions.html.php', array(
                'items'    => $_movie['sessions'],
                'platinum' => $cinema->getPlatinum(),
                'checkout_ref' => $single ? 'cinema_side' : 'home_side',
            ));
            ?>
        </div>
        <?php endforeach; ?>
    </div>

    <?php if (isset($cinema_platinum) AND $cinema_platinum !== NULL) : ?>
    <p class="sidebar-cinema-link"><a class="btn btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_platinum); ?>"><i class="icon icon-platinum"></i> Ver Horarios en Platino</a></p>
    <?php endif; ?>

    <?php if (isset($cinema_standard) AND $cinema_standard !== NULL) : ?>
    <?php if (TRUE === $cinema_standard->getData('premium_only')): ?>
    <p class="sidebar-cinema-link"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_standard); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Premium</a></p>
    <?php else: ?>
    <p class="sidebar-cinema-link"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_standard); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Tradicional</a></p>
    <?php endif; ?>
    <?php endif; ?>

    <p id="mycinema-search-movie"><a class="btn btn-default btn-icon icon icon-search" href="<?php
        echo $view['router']->generate('billboard', array(
            'area_id' => $cinema->getArea()->getId(),
            'slug' => FrontHelper::sanitize_for_url($cinema->getArea()->getName())
        )); ?>/">Ver todos los horarios de <?php echo $cinema->getArea()->getName(); ?></a></p>
</div><!-- #sidebar-mycinema -->

<?php
echo $view['actions']->render(
    $view['router']->generate('esi_movies_in_area', array('area_id' => $cinema->getArea()->getId())),
    array('strategy' => 'esi')
);
?>
