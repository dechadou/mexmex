<?php
 
namespace SocialSnack\AdminBundle\Entity;
 
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Entity
 * @ORM\Table(name="admin")
 * @ORM\Entity(repositoryClass="AdminRepository")
 */
class Admin implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="user", type="string", length=255)
     */
    protected $user;
 
    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;
    
    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;
    
    /**
     * @Assert\Length(max = 50)
     */
    protected $plainPassword;
 
    /**
     * @ORM\ManyToMany(targetEntity="AdminRole")
     * @ORM\JoinTable(name="admin_adminrole",
     *     joinColumns={@ORM\JoinColumn(name="admin_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="adminrole_id", referencedColumnName="id")}
     * )
     */
    protected $roles;
    
    public function getId() {
      return $this->id;
    }
    public function getUsername() {
      return $this->user;
    }
    public function getPassword() {
      return $this->password;
    }
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    public function getSalt() {
      return $this->salt;
    }
    public function getRoles() {
      return $this->roles->toArray();
    }
    public function eraseCredentials(){
      $this->plainPassword = '';
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->salt  = md5(uniqid(null, true));
    }
    
    /**
     * Set user
     *
     * @param string $user
     * @return Admin
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Admin
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }
    
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    
        return $this;
    }
    
    /**
     * Set salt
     *
     * @param string $salt
     * @return Admin
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    
    /**
     * Set roles (remove all existing roles and add new roles from scratch).
     * 
     * @param array $roles
     * @return \SocialSnack\AdminBundle\Entity\Admin
     */
    public function setRoles($roles) {
      $this->roles->clear();
      foreach ($roles as $role) {
        $this->addRole($role);
      }
      
      return $this;
    }
    
    /**
     * Add roles
     *
     * @param \SocialSnack\AdminBundle\Entity\AdminRole $role
     * @return Admin
     */
    public function addRole(\SocialSnack\AdminBundle\Entity\AdminRole $role)
    {
        $this->roles->add($role);
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \SocialSnack\AdminBundle\Entity\AdminRole $roles
     */
    public function removeRole(\SocialSnack\AdminBundle\Entity\AdminRole $roles)
    {
        $this->roles->removeElement($roles);
    }
    
/**
     * Serializes the content of the current User object
     * @return string
     */
    public function serialize()
    {
        return \json_encode(
                array($this->user, $this->password, $this->salt,
                        $this->roles, $this->id));
    }

    /**
     * Unserializes the given string in the current User object
     * @param serialized
     */
    public function unserialize($serialized)
    {
        list($this->user, $this->password, $this->salt,
                        $this->roles, $this->id) = \json_decode(
                $serialized);
    }
}