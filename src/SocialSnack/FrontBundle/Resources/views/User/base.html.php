<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->start( 'body' );
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('user_profile'); ?>">Mi Perfil</a></li>
</ul>

<?php $view['slots']->output('section_banner'); ?>

<div class="clearfix row">
	
	<div class="narrow-side-col col">
		<?php
		echo $view->render(
				'SocialSnackFrontBundle:User:menu.html.php',
				array( 'route' => $route )
		);
		?>
	</div>
	
	<div class="wide-main-col col">
		<?php $view['slots']->output('user_body'); ?>
	</div>
	
</div>
<?php
$view['slots']->stop();
?>