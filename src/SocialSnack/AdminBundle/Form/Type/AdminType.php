<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('user', 'text', array(
            'label' => 'Usuario',
        ))
        ->add('password', 'password', array(
            'label' => 'Contraseña',
            'required' => $builder->getData()->getId() ? FALSE : TRUE,
            'mapped' => FALSE,
        ))
        ->add('roles', 'entity', array(
            'label' => 'Roles',
            'class' => 'SocialSnackAdminBundle:AdminRole',
            'property' => 'name',
            'multiple' => TRUE,
            'expanded' => TRUE,
        ))
    ;
  }


  public function getName() {
    return '';
  }

}