<?php

namespace SocialSnack\RestBundle\Service;

use Endroid\QrCode\QrCode;

use Passbook\Pass\Location;
use Passbook\Pass\DateField;
use Passbook\Pass\Field;
use Passbook\Pass\Barcode;
use Passbook\Pass\Image;
use Passbook\Pass\Structure;
use Passbook\Type\EventTicket;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class Utils {
  
  protected $container;
  
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }
  
  
  public function get_ip_captcha( $ip ) {
    /* @var $mem \Memcached */
    $mem = $this->container->get( 'memcached' );
    return $mem->get($this->get_ip_captcha_key($ip));
  }
  
  
  public function set_ip_captcha( $ip, $captcha ) {
    /* @var $mem \Memcached */
    $mem = $this->container->get( 'memcached' );
    return $mem->set($this->get_ip_captcha_key($ip), $captcha, 180);
  }
  
  
  public function get_ip_captcha_key( $ip ) {
    return md5( 'ip_captcha_' . $ip );
  }
  
  
  public function get_ip_trans_key( $ip, $trans ) {
    return md5( 'ip_' . $trans . '_trans_' . $ip );
  }
  
  
  /**
   * Determine whether the IP is allowed to perform a transaction or it is blocked.
   * 
   * @param string $ip IP address.
   * @return boolean TRUE = is allowed. FALSE = is not.
   */
  public function is_ip_allowed_to_buy( $ip ) {
    $blocked_ips = $this->container->get('doctrine')->getRepository('SocialSnackFrontBundle:Option')->getBlockedIPs();
    return !in_array($ip, $blocked_ips);
    
    /* @var $mem \Memcached */
    $mem = $this->container->get( 'memcached' );
    
    $allowed = TRUE;
    
    foreach ( array( 'failed', 'success' ) as $trans ) {
      $max = $this->container->getParameter( 'max_trans_' . $trans );
      $key = $this->get_ip_trans_key( $ip, $trans );
      $log = $mem->get( $key, NULL );

      if ( $mem->getResultCode() == \Memcached::RES_NOTFOUND || !$log )
        continue;

      if ( !isset( $log['attempts'] ) )
        continue;

      if ( $log['attempts'] >= $max ) {
        $allowed = FALSE;
      }
    }
    
    return $allowed;
  }
  
  
  /**
   * Get the count of $trans transactions for $ip IP.
   * 
   * @param string $ip
   * @param string $trans
   * @return boolean
   */
  protected function get_ip_trans( $ip, $trans ) {
    /* @var $mem \Memcached */
    $mem      = $this->container->get( 'memcached' );
    $key      = $this->get_ip_trans_key( $ip, $trans );
    $log      = $mem->get( $key, NULL );
    
    if ( $mem->getResultCode() == \Memcached::RES_NOTFOUND || !$log || !isset($log['attempts']) ) {
      return NULL;
    } else {
      return $log['attempts'];
    }
  }
  
  
  /**
   * Increment the count of $trans transactions for $ip IP.
   * 
   * @param string $ip
   * @param string $trans
   * @return boolean
   */
  protected function set_ip_trans( $ip, $trans ) {
    /* @var $mem \Memcached */
    $mem      = $this->container->get( 'memcached' );
    $key      = $this->get_ip_trans_key( $ip, $trans );
    $cas      = NULL;
    $log      = $mem->get( $key, NULL, $cas );
    $time     = $this->container->getParameter( 'max_trans_' . $trans . '_time' );
    $saved    = FALSE;
    $attempts = 0;
    
    if ( $mem->getResultCode() == \Memcached::RES_NOTFOUND || !$log ) {
      $log = array(
          'attempts' => 0,
          'timeout'  => 0
      );
    }
    
    $log['attempts']++;
    $timeout = $time * 60;
    
    do {
      $attempts++;
      
      if ( $cas ) {
        $saved = $mem->cas( $cas, $key, $log, $timeout );
      } else {
        $saved = $mem->set( $key, $log, $timeout );
      }
    } while ( !$saved && ( $attempts < 5 ) );
    
    return $saved;
  }
  
  
  /**
   * Increment the count of failed transactions for $ip IP.
   * 
   * @param string $ip
   * @return boolean
   */
  public function set_ip_trans_failed( $ip ) {
    return $this->set_ip_trans( $ip, 'failed' );
  }
  
  
  /**
   * Increment the count of successful transactions for $ip IP.
   * 
   * @param string $ip
   * @return boolean
   */
  public function set_ip_trans_success( $ip ) {
    return $this->set_ip_trans( $ip, 'success' );
  }
  
  
  public function process_security_enforcement( $ip, $action, \Symfony\Component\HttpFoundation\Request $request, \SocialSnack\RestBundle\Entity\App $client = NULL) {
    $output = array();

    if (is_null($client)) {
      return $output;
    }

    $rules  = $this->get_security_enforcement($ip, $action);
    $fails  = $this->get_ip_trans($ip, 'failed');
    $session = $request->getSession();
    $expected_captcha = $session->get('captcha');
    $captcha_expire   = $session->get('captcha_expire');
    $session->set('captcha', 'S74U956<K9C8c478[j6]47@%7764v39476531[|U"13C');
    
    foreach ( $rules as $rule ) {
      if ( preg_match('/^wait\*[0-9]+$/', $rule ) ) {
        $wait = (int)substr($rule, 5);
        $wait = min( $wait * $fails, 60 );
//        sleep($wait);
      }
      
      if ($client->getSetting('captcha')) {
        if ( 'captcha' === $rule ) {
          $request->attributes->set('captcha', TRUE);
          $output[] = 'captcha';
        }

        if ( 'verify_captcha' === $rule ) {
          $received_captcha = $request->request->get('captcha');
          $match_captcha    = $expected_captcha == $received_captcha;
          if (!$match_captcha || $captcha_expire < time()) {
            $output[] = 'captcha_failed';
          }
        }
      }
    }
    
    return $output;
  }
  
  
  public function get_security_enforcement( $ip, $action ) {
    $fails = $this->get_ip_trans($ip, 'failed');
    $rules = array();
    
    if ( 'purchase' == $action ) {
      if ( $fails > 4 ) {
        $rules[] = 'wait*5';
      } elseif ( $fails > 11 ) {
        $rules[] = 'wait*6';
      } elseif ( $fails > 20 ) {
        $rules[] = 'wait*10';
      }
      
//      if ( $fails >= 1 ) {
        $rules[] = 'captcha';
//      }
//      if ( $fails >= 2 ) {
        $rules[] = 'verify_captcha';
//      }
    }
    
    return $rules;
  }
  
  
  public function build_qr( $code, $prefix = '' ) {
//    $fs       = new Filesystem();
    $qrCode   = new QrCode();
    
		$qrCode->setSize( 90, 90 );
		$qrCode->setPadding( 5 );
    $qrCode->setText( $code );
    
    $content  = $qrCode->get( 'jpg' );
    $path     = 'dynamic/qr/';
    
//    do {
    $filename = md5( implode('-', array( gethostname(), $prefix, $code )) ) . '.jpg';
//    } while ( $fs->exists( $path . $filename ) );
    
    try {
//      $fs->dumpFile( $filename, $content );
      foreach ( $this->container->getParameter('static_fss_enabled') as $fs_id ) {
        $fs = $this->container->get('knp_gaufrette.filesystem_map')->get($fs_id);
        $fs->write($path . $filename, $content);
      }
    } catch ( IOException $e ) {
      return FALSE;
    }
    
    return $filename;
  }
  
 
  /**
   * Query if the movie has premium and platinum sessions.
   * 
   * @param \SocialSnack\WsBundle\Entity\Movie $movie
   * @param string $zone_type 'state' or 'area'.
   * @param int $zone_id state_id o area_id.
   * @return array
   */
  public function get_extra_movie_types($movie, $zone_type = NULL, $zone_id = NULL) {
    $types = array();
    $em = $this->container->get('doctrine')->getManager();
    
    $qb = $em->createQueryBuilder();
    $qb->select('s.id')
        ->from('SocialSnackWsBundle:Session', 's')
        ->innerJoin('s.cinema', 'c')
        ->where('c.platinum = 1')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere('s.group_id = :group_id')
        ->setParameter('group_id', $movie->getGroupId())
        ->setMaxResults(1);
    
    if ('state' === $zone_type) {
      $qb->andWhere('c.state = :state_id')
          ->setParameter('state_id', $zone_id);
    } elseif ('area' === $zone_type) {
      $qb->andWhere('c.area = :area_id')
          ->setParameter('area_id', $zone_id);
    }
    
    if ($qb->getQuery()->getResult()) {
      $types[] = 'platinum';
    }

    $qb = $em->createQueryBuilder();
    $qb->select('s.id')
        ->from('SocialSnackWsBundle:Session', 's')
        ->innerJoin('s.cinema', 'c')
        ->where('s.premium = 1')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->andWhere('s.group_id = :group_id')
        ->setParameter('group_id', $movie->getGroupId())
        ->setMaxResults(1);
    
    if ('state' === $zone_type) {
      $qb->andWhere('c.state = :state_id')
          ->setParameter('state_id', $zone_id);
    } elseif ('area' === $zone_type) {
      $qb->andWhere('c.area = :area_id')
          ->setParameter('area_id', $zone_id);
    }
    
    if ($qb->getQuery()->getResult()) {
      $types[] = 'premium';
    }
    
    return $types;  
  }


  /**
   * Registers/updates a device registration por push notifications.
   *
   * @param string $uuid  UUID
   * @param string $gcmid GCMID
   * @param string $type  Device type
   * @param \SocialSnack\FrontBundle\Entity\User $user
   * @return \SocialSnack\RestBundle\Entity\GCM
   */
  public function setGCM($uuid, $gcmid, $type, $user = NULL) {
    $em = $this->container->get('doctrine')->getManager();

    /** @var \SocialSnack\RestBundle\Entity\GCMRepository $repo */
    $repo = $this->container->get('doctrine')->getRepository('SocialSnackRestBundle:GCM');

    $gcm = $repo->findByUuidOrGcmid($uuid, $gcmid, $type);
    if (!$gcm) {
      $gcm = new \SocialSnack\RestBundle\Entity\GCM();
      $em->persist($gcm);
    }
    $gcm->setActive(1);
    $gcm->setDateRegistered(new \DateTime('now'));
    $gcm->setDevice($type);
    $gcm->setGcmid($gcmid);
    $gcm->setUuid($uuid);
    $gcm->setUser($user);
    $em->flush();
    
    return $gcm;
  }
  
  /**
   * 
   * @param \SocialSnack\FrontBundle\Entity\Transaction $trans
   * @return \Passbook\Type\EventTicket
   */
  public function build_passbook(\SocialSnack\FrontBundle\Entity\Transaction $trans) {
    /* @var $front_utils \SocialSnack\FrontBundle\Service\Utils */
    $front_utils = $this->container->get('ss.front.utils');

    // Create an event ticket
    $ticket = new EventTicket($trans->getId(), "Boletos de Cinemex para " . $trans->getMovie()->getName());
    
    // Ticket colors
    $ticket->setBackgroundColor('rgb(208, 3, 61)');
    $ticket->setForegroundColor('rgb(255, 255, 255)');
    $ticket->setLabelColor('rgb(255, 255, 255)');

    // Create pass structure
    $structure = new Structure();

    // Add movie primary field
    $primary = new Field('movie', $trans->getMovie()->getName());
    $primary->setLabel(FrontHelper::get_movie_attr_str( $trans->getMovie()->getAttr()));
    $structure->addPrimaryField($primary);

    // Add cinema secondary field
    $secondary = new Field('cinema', $trans->getCinema()->getName());
    $secondary->setLabel('Complejo');
    $structure->addSecondaryField($secondary);

    // Add date header field
    $header = new DateField('datetime', $trans->getSession()->getDateTime());
    $header->setLabel('Fecha');
    $header->setDateStyle(DateField::PKDateStyleShort);
    $header->setTimeStyle(DateField::PKDateStyleShort);
    $structure->addHeaderField($header);

    // Add seats auxiliary field
    $auxiliary = new Field('seats', FrontHelper::confirmation_get_seatslist($trans, ' / '));
    $auxiliary->setLabel('Asientos');
    $structure->addAuxiliaryField($auxiliary);

    // Add booking code auxiliary field
    $auxiliary = new Field('code', $trans->getCode());
    $auxiliary->setLabel('Booking');
    $structure->addAuxiliaryField($auxiliary);

    // Add auditorium auxiliary field
    $auxiliary = new Field('auditorium', FrontHelper::confirmation_get_screenname($trans));
    $auxiliary->setLabel('Sala');
    $structure->addAuxiliaryField($auxiliary);

    // Add tickets back field
    $back = new Field('tickets', FrontHelper::confirmation_get_ticketslist($trans, ' / '));
    $back->setLabel('Boletos');
    $structure->addBackField($back);

    // Add address back field
    $back = new Field('address', $trans->getCinema()->getData('DIRECCION'));
    $back->setLabel('Dirección');
    $structure->addBackField($back);

    // Add phone back field
    $back = new Field('phone', $trans->getCinema()->getData('TELEFONOS'));
    $back->setLabel('Teléfono');
    $structure->addBackField($back);

    // Relevant date and location
    $ticket->setRelevantDate($trans->getSession()->getDateTime());
    $location = new Location($trans->getCinema()->getLat(), $trans->getCinema()->getLng());
    $location->setRelevantText($trans->getCinema()->getName());
    $ticket->addLocation($location);
    
    // Add icon and logo
    $ticket->addImage(new Image(__DIR__.'/../Resources/Passbook/icons/icon.png', 'icon'));
    $ticket->addImage(new Image(__DIR__.'/../Resources/Passbook/icons/icon@2x.png', 'icon@2x'));
    $ticket->addImage(new Image(__DIR__.'/../Resources/Passbook/icons/logo.png', 'logo'));
    $ticket->addImage(new Image(__DIR__.'/../Resources/Passbook/icons/logo@2x.png', 'logo@2x'));
    
    // Add thumbnail
    $browser = $this->container->get('gremo_buzz');
    /* @var $browser \Buzz\Browser */
    $poster_url      = $front_utils->get_movie_poster_url($trans->getMovie(), 'passbook_th');
    $poster_url_2x   = $front_utils->get_movie_poster_url($trans->getMovie(), 'passbook_th_2x');
    $poster_bytes    = $browser->get($poster_url);
    $poster_bytes_2x = $browser->get($poster_url_2x);
    if (200 === $poster_bytes->getStatusCode() && 200 === $poster_bytes_2x->getStatusCode()) {
      $poster_tmp = tempnam(sys_get_temp_dir(), 'passbook');
      file_put_contents($poster_tmp, $poster_bytes);
      $ticket->addImage(new Image($poster_tmp, 'thumbnail'));
      
      $poster_tmp_2x = tempnam(sys_get_temp_dir(), 'passbook');
      file_put_contents($poster_tmp_2x, $poster_bytes_2x);
      $ticket->addImage(new Image($poster_tmp_2x, 'thumbnail@2x'));
    }

    // Set pass structure
    $ticket->setStructure($structure);

    // Add barcode
    $barcode = new Barcode('PKBarcodeFormatQR', $trans->getCode());
    $barcode->setAltText($trans->getCode());
    $ticket->setBarcode($barcode);

    /* @var $factory \Passbook\PassFactory */
    $factory = $this->container->get('pass_factory');
    $pass = $factory->package($ticket);
    
    /* @todo Upload to just one static */
//    $fs = $front_utils->get_shard_static($trans->getId());
    $fs = $this->container->getParameter('static_fss_enabled');
    $upload_file = $trans->getHash() . '.pkpass';
    $upload_path = 'dynamic/passbooks/' . $upload_file;
    $uploaded = $front_utils->upload_to_cdn($upload_path, file_get_contents($pass->getRealPath()), $fs, TRUE);
    
    if (!$uploaded) {
      return FALSE;
    }
    
    return $upload_path;
  }
  
}