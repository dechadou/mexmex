<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;
use Symfony\Component\HttpFoundation\Request;

class MoviesControllerTest extends WebTestCase {


  protected function getCollectionModel() {
    return [
        [
            'type' => 'array',
            'each' => $this->getEntityModel(),
        ],
    ];
  }


  protected function getEntityModel() {
    return [
        'id'            => 'integer',
        'name'          => 'string',
        'score'         => 'integer',
        'cover'         => 'string',
        'poster_small'  => 'string',
        'poster_medium' => 'string',
        'poster_big'    => 'string',
        'type'          => 'array',
        'info'          => [
            'model' => [
                'sinopsis'       => 'string',
                'original_title' => 'string',
                'director'       => 'string',
                'cast'           => 'string',
                'country'        => 'string',
                'genre'          => 'array',
                'rating'         => 'string',
                'year'           => 'string',
                'duration'       => 'string',
                'trailer'        => 'string',
            ],
        ],
        'attributes' => 'array',
        'url'        => 'string',
        'featured'   => 'boolean',
        'ribbons'    => 'array',
        'label'      => 'string',
    ];
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testDefaultAll() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
    ));

    $route = '/rest/movies/';
    $client->request(
        'GET',
        $route
    );

    $model = $this->getCollectionModel();
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  protected function _testDefaultZone($type, $get_id, $get_args) {
    $client = static::createClient();

    $container = $client->getContainer();

    // It is OK to mock a service in a functional test? @todo Find another way to do this.
    $handler_mock = $this->getServiceMockBuilder('rest.movie.handler')
        ->setMethods(['findMoviesIn'])
        ->enableOriginalConstructor()
        ->setConstructorArgs([$container->get('doctrine'), $container->get('ss.front.utils')])
        ->getMock();

    $container->set('rest.movie.handler', $handler_mock);

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ));

    $handler_mock
        ->expects($this->once())
        ->method('findMoviesIn')
        ->with($this->callback($get_args))
        ->willReturn(LoadMoviesData::$movies);

    $route = sprintf('/rest/movies/%s/%d', $type, $get_id());
    $client->request(
        'GET',
        $route
    );

    $model = $this->getCollectionModel();
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @group functional
   * @group api_v1
   */
  public function testDefaultState() {
    $this->_testDefaultZone(
        'area',
        function() { return LoadStatesData::$states[0]->getId() + 1000; },
        function() { return ['state', LoadStatesData::$states[0]->getId()]; }
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testDefaultArea() {
    $this->_testDefaultZone(
        'area',
        function() { return LoadAreasData::$areas[0]->getId(); },
        function() { return ['area', LoadAreasData::$areas[0]->getId()]; }
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testDetails() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
    ));

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $route = sprintf('/rest/movies/%d', $movie_id);
    $client->request(
        'GET',
        $route
    );

    $model = $this->getEntityModel();
    $model['id'] = [
        'type'   => 'integer',
        'equals' => $movie_id,
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testSessionsAll() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
    ));

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $route = sprintf('/rest/movies/%d/sessions', $movie_id);
    $client->request(
        'GET',
        $route
    );

    $this->assertJsonResponse($client->getResponse(), $this->getCollectionModel());
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testSessionsState() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ));

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $state_id = LoadStatesData::$states[0]->getId();
    $route = sprintf('/rest/movies/%d/sessions/area/%d', $movie_id, $state_id + 1000);
    $client->request(
        'GET',
        $route
    );

    $model = [
        [
            'type' => 'array',
            'each' => [
                'id' => 'integer',
                'state' => [
                    'model' => [
                        'id' => [
                            'type' => 'integer',
                            'equals' => $state_id,
                        ]
                    ]
                ],
                'sessions' => [
                    'type' => 'array',
                    'each' => [
                        'id' => 'string'
                    ]
                ]
            ]
        ]
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testSessionsArea() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ));

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $area_id = LoadAreasData::$areas[0]->getId();
    $route = sprintf('/rest/movies/%d/sessions/area/%d', $movie_id, $area_id);
    $client->request(
        'GET',
        $route
    );

    $model = [
        [
            'type' => 'array',
            'each' => [
                'id' => 'integer',
                'area' => [
                    'model' => [
                        'id' => [
                            'type' => 'integer',
                            'equals' => $area_id,
                        ]
                    ]
                ],
                'sessions' => [
                    'type' => 'array',
                    'each' => [
                        'id' => 'string'
                    ]
                ]
            ]
        ]
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testSessionsCinemas() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ));

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $cinema_ids = [];
    $cinema_ids[] = LoadCinemasData::$cinemas[0]->getId();
    $cinema_ids[] = LoadCinemasData::$cinemas[1]->getId();
    $route = sprintf('/rest/movies/%d/sessions?cinema_ids=', $movie_id) . implode(',', $cinema_ids);
    $client->request(
        'GET',
        $route
    );

    $model = [
        [
            'type' => 'array',
            'each' => [
                'id' => [
                    'type' => 'integer',
                    'in'   => $cinema_ids
                ],
                'sessions' => [
                    'type' => 'array',
                    'each' => [
                        'id' => 'string'
                    ]
                ]
            ]
        ]
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testVoteSuccess() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = $this->get_logged_in_client(LoadUserData::$users[0]);

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $route    = sprintf('/rest/movies/%d/vote', $movie_id);
    $client->request(
        'POST',
        $route,
        array('value' => 3)
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testVoteNotLogged() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = static::createClient();

    $movie_id = LoadMoviesData::$movies[0]->getId();
    $route    = sprintf('/rest/movies/%d/vote', $movie_id);
    $client->request(
        'POST',
        $route,
        array('value' => 3)
    );

    $this->assertEquals(401, $client->getResponse()->getStatusCode());
  }

  
  /**
   * @group functional
   * @group api_v1
   */
  public function testV1GetComingAction() {
    $client = static::createClient();
    $this->loadFixtures(array(
      '\SocialSnack\RestBundle\Tests\Fixtures\LoadMovieComingData',
    ));

    // This is used to test pagination.
    $count = 3;
    $page = 2;

    $route = '/rest/movies/coming?count=' . $count . '&page=' . $page;
    $client->request(
        'GET',
        $route
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());

    // Test valid response.
    $data = json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertObjectHasAttribute('movies', $data);
    $this->assertObjectHasAttribute('pagination', $data);

    // Test pagination.
    $this->assertEquals(3, sizeof($data->movies));
    $this->assertObjectHasAttribute('next', $data->pagination);
    $this->assertObjectHasAttribute('prev', $data->pagination);
  }

} 