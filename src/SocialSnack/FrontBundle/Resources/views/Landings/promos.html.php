<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Promociones - Cinemex');
$view['slots']->set('body_class', 'sponsored-promos');

$view['slots']->start('body');
?>

<style>
.section-oscar2014 iframe {
    display:block;
    height:4000px;
    width:100%;
}
</style>

<div class="section-oscar2014">
    <iframe src="//promocinemex.com/landing/promos" onload="scroll(0, 0);"></iframe>
</div>

<?php $view['slots']->stop(); ?>
