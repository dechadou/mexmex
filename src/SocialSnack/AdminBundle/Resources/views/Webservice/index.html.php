<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Webservice' );
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <?php if ($output) { ?>
    <pre><?php echo implode("\r\n", $output); ?></pre>
    <?php } ?>
    <section class="panel">
      <header class="panel-heading">
          Webservice
      </header>

      <div class="panel-body">
        <div class="form-horizontal tasi-form">
          <div class="form-group">
            <form action="<?php echo $view['router']->generate('social_snack_admin_ws_update_cinemas'); ?>" method="post">
              <label class="col-sm-2 col-sm-2 control-label">Actualizar cines</label>
              <div class="col-sm-4">
                <select name="area_id" class="form-control">
                  <?php foreach ($states as $state) { ?>
                  <optgroup label="<?php echo $state->getName(); ?>">
                    <option value="state/<?php echo $state->getId(); ?>">Todo <?php echo $state->getName(); ?></option>
                    <?php foreach ($state->getAreas() as $area) { ?>
                    <option value="area/<?php echo $area->getId(); ?>"><?php echo $area->getName(); ?></option>
                    <?php } ?>
                  </optgroup>
                  <?php } ?>
                </select>
              </div>
              <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Procesar</button>
              </div>
            </form>
          </div>

          <div class="form-group">
            <form action="<?php echo $view['router']->generate('social_snack_admin_ws_update_sessions'); ?>" method="post">
              <label class="col-sm-2 col-sm-2 control-label">Actualizar funciones</label>
              <div class="col-sm-4">
                <select name="cinema_id" class="form-control">
                  <?php foreach ($cinemas as $cinema) { ?>
                  <option value="<?php echo $cinema->getId(); ?>"><?php echo $cinema->getName(); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Procesar</button>
              </div>
            </form>
          </div>

          <div class="form-group">
            <form action="<?php echo $view['router']->generate('social_snack_admin_ws_group_movies'); ?>" method="post">
              <input type="hidden" name="redirect" value="<?php echo $view['router']->generate('social_snack_admin_ws_index'); ?>" />
              <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Agrupar y ordenar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>