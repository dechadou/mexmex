<?php
$menu = array(
    array(
        'label' => 'Cartelera',
        'route' => 'art',
    ),
//    array(
//        'label' => 'Próximos estrenos',
//        'route' => 'art_coming',
//    ),
//    array(
//        'label' => 'Festivales',
//        'route' => 'art_fests',
//    ),
    array(
        'label' => 'Cines sedes',
        'route' => 'art_cinemas',
    ),
);
?>
<div class="section-header section-art">
  <img src="<?php echo $view['assets']->getUrl( 'assets/img/header-art.jpg' ); ?>" alt="Casa de Arte" />
  <div class="section-nav">
    <ul>
      <?php foreach ( $menu as $item ) { ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'] ); ?>" class="<?php if ( $route == $item['route'] ) echo 'selected'; ?>"><?php echo $item['label']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>