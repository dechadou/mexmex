<div class="section-content landing-content landing-content-bg">
  <h1 class="landing-title"><?php echo $page->getName(); ?></h1>

  <div class="landing-promos">
    <div class="grid-row">
      <?php foreach ($page->getContent()['promos'] as $promo) { ?>
      <div class="grid-col-4">
        <a class="lightbox-img" href="<?php echo $view['assets']->getUrl($promo['image_path'], 'CMS'); ?>">
          <img src="<?php echo $view['assets']->getUrl($promo['thumb_path'], 'CMS'); ?>">
        </a>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
