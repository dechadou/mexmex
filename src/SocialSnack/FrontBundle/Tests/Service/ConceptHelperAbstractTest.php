<?php

namespace SocialSnack\FrontBundle\Tests\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ConceptHelperTest
 * @package SocialSnack\FrontBundle\Tests\Service
 * @author Guido Kritz
 */
class ConceptHelperAbstractTest extends WebTestCase {

  static $container;

  protected function setUp() {
    $kernel = static::createKernel();
    $kernel->boot();

    self::$container = $kernel->getContainer();
  }


  /**
   * @param array $methods
   * @return ConceptHelperAbstract
   */
  protected function getHelper(array $methods = []) {
    $doctrine = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();

    return $this->getMockBuilder('\SocialSnack\FrontBundle\Service\ConceptHelperAbstract')
        ->setMethods($methods)
        ->setConstructorArgs([$doctrine])
        ->getMockForAbstractClass();
  }


  public function testGetStatesIdsForCinemas() {

  }

  public function testGetStatesIds() {
    $cinema_ids = [1, 2, 3];
    $state_ids  = [4, 5, 6];

    $helper = $this->getHelper(['getCinemasLegacyIds', 'getStatesIdsForCinemas']);

    $helper->expects($this->once())
        ->method('getCinemasLegacyIds')
        ->willReturn($cinema_ids);

    $helper->expects($this->once())
        ->method('getStatesIdsForCinemas')
        ->with($cinema_ids)
        ->willReturn($state_ids);

    $output = $helper->getStatesIds();
    $this->assertEquals($state_ids, $output);
  }


  protected function _testStateHasConcept($intersect_result, $expected) {
    $cinema_ids = [1, 2, 3];
    $state_id   = 4;

    $helper = $this->getHelper(['getCinemasLegacyIds', 'intersectStateAndCinemas']);

    $helper->expects($this->once())
        ->method('getCinemasLegacyIds')
        ->willReturn($cinema_ids);

    $helper->expects($this->once())
        ->method('intersectStateAndCinemas')
        ->with($state_id, $this->identicalTo($cinema_ids))
        ->willReturn($intersect_result);

    $output = $helper->stateHasConcept($state_id);
    $this->assertEquals($expected, $output);
  }


  public function testStateHasConceptTrue() {
    $this->_testStateHasConcept(5, TRUE);
  }


  public function testStateHasConceptZero() {
    $this->_testStateHasConcept(0, FALSE);
  }


  public function testStateHasConceptNegative() {
    $this->_testStateHasConcept(-1, FALSE);
  }


  protected function _testAreaHasConcept($intersect_result, $expected) {
    $cinema_ids = [1, 2, 3];
    $state_id   = 4;

    $helper = $this->getHelper(['getCinemasLegacyIds', 'intersectAreaAndCinemas']);

    $helper->expects($this->once())
        ->method('getCinemasLegacyIds')
        ->willReturn($cinema_ids);

    $helper->expects($this->once())
        ->method('intersectAreaAndCinemas')
        ->with($state_id, $this->identicalTo($cinema_ids))
        ->willReturn($intersect_result);

    $output = $helper->areaHasConcept($state_id);
    $this->assertEquals($expected, $output);
  }


  public function testAreaHasConceptTrue() {
    $this->_testAreaHasConcept(5, TRUE);
  }


  public function testAreaHasConceptZero() {
    $this->_testAreaHasConcept(0, FALSE);
  }


  public function testAreaHasConceptNegative() {
    $this->_testAreaHasConcept(-1, FALSE);
  }


} 