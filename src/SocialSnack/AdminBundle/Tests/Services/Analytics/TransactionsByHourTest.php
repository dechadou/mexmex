<?php

namespace SocialSnack\AdminBundle\Tests\Services\Analytics;

class TransactionsByHourTest extends \PHPUnit_Framework_TestCase {

  protected $doctrine;

  public function setUp() {
    $this->doctrine = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();
  }

  protected function getServiceMock(array $methods = NULL) {
    return $this->getMockBuilder('SocialSnack\AdminBundle\Services\Analytics\TransactionsByHour')
        ->setConstructorArgs([$this->doctrine])
        ->setMethods($methods)
        ->getMock();
  }


  public function testGetComparableTodayDates() {
    $service = $this->getServiceMock(['getCurrentTime']);

    $service->expects($this->once())
        ->method('getCurrentTime')
        ->willReturn(new \DateTime('2015-06-14 16:20:15'));

    $output = $service->getComparableTodayDates();

    $expected = [
        'current' => [
            new \DateTime('2015-06-14 00:00:00'),
            new \DateTime('2015-06-14 16:00:00'),
        ],
        'compare' => [
            new \DateTime('2015-06-07 00:00:00'),
            new \DateTime('2015-06-08 00:00:00'),
        ]
    ];

    $this->assertSame($expected['current'][0]->getTimestamp(), $output['current'][0]->getTimestamp());
    $this->assertSame($expected['current'][1]->getTimestamp(), $output['current'][1]->getTimestamp());
    $this->assertSame($expected['compare'][0]->getTimestamp(), $output['compare'][0]->getTimestamp());
    $this->assertSame($expected['compare'][1]->getTimestamp(), $output['compare'][1]->getTimestamp());
  }


  public function testGetComparableLast24hsDates() {
    $service = $this->getServiceMock(['getCurrentTime']);

    $service->expects($this->once())
        ->method('getCurrentTime')
        ->willReturn(new \DateTime('2015-06-14 16:20:15'));

    $output = $service->getComparableLast24hsDates();

    $expected = [
        'current' => [
            new \DateTime('2015-06-13 16:00:00'),
            new \DateTime('2015-06-14 16:00:00'),
        ],
        'compare' => [
            new \DateTime('2015-06-06 16:00:00'),
            new \DateTime('2015-06-07 16:00:00'),
        ]
    ];

    $this->assertSame($expected['current'][0]->getTimestamp(), $output['current'][0]->getTimestamp());
    $this->assertSame($expected['current'][1]->getTimestamp(), $output['current'][1]->getTimestamp());
    $this->assertSame($expected['compare'][0]->getTimestamp(), $output['compare'][0]->getTimestamp());
    $this->assertSame($expected['compare'][1]->getTimestamp(), $output['compare'][1]->getTimestamp());
  }


  public function testGetRangeLabels() {
    $service = $this->getServiceMock();

    $expected = [
        '00hs',
        '01hs',
        '02hs',
        '03hs',
        '04hs',
        '05hs',
        '06hs',
        '07hs',
        '08hs',
        '09hs',
        '10hs',
        '11hs',
        '12hs',
        '13hs',
        '14hs',
        '15hs',
        '16hs',
        '17hs',
        '18hs',
        '19hs',
        '20hs',
        '21hs',
        '22hs',
        '23hs',
    ];

    $actual = $service->getRangeLabels(
        new \DateTime('2015-06-06 00:00:00'),
        new \DateTime('2015-06-07 00:00:00')
    );

    $this->assertSame($expected, $actual);
  }


  public function testGetRangeLabels_Restart() {
    $service = $this->getServiceMock();

    $expected = [
        '15hs',
        '16hs',
        '17hs',
        '18hs',
        '19hs',
        '20hs',
        '21hs',
        '22hs',
        '23hs',
        '00hs',
        '01hs',
        '02hs',
        '03hs',
        '04hs',
        '05hs',
        '06hs',
        '07hs',
        '08hs',
        '09hs',
        '10hs',
        '11hs',
        '12hs',
        '13hs',
        '14hs',
    ];

    $actual = $service->getRangeLabels(
        new \DateTime('2015-06-06 15:00:00'),
        new \DateTime('2015-06-07 15:00:00')
    );

    $this->assertSame($expected, $actual);
  }


}