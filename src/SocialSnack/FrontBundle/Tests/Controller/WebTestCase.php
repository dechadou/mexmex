<?php

namespace SocialSnack\FrontBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;
use SocialSnack\FrontBundle\Entity\Role;
use SocialSnack\FrontBundle\Tests\Fixtures\LoadUserData;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class WebTestCase extends LiipWebTestCase {
  
  protected function loadFixtures(array $classNames, $omName = null, $registryName = 'doctrine', $purgeMode = null) {
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
    $result = parent::loadFixtures($classNames, $omName, $registryName, $purgeMode);
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));
    return $result;
  }


  protected function getLoggedInClient(array $fixtures = NULL, $user = NULL) {
    $client = static::createClient();
    $session = $client->getContainer()->get('session');

    $fixtures = NULL !== $fixtures ? $fixtures : array(
        '\SocialSnack\FrontBundle\Tests\Fixtures\LoadUserData',
    );

    $this->loadFixtures($fixtures);

    if (is_null($user)) {
      $user = LoadUserData::$userIe;
    } elseif (is_callable($user)) {
      $user = $user();
    }

    $firewall = 'main';
    $token = new UsernamePasswordToken( $user, $user->getPassword(), $firewall, $user->getRoles());
    $session->set('_security_'.$firewall, serialize($token));
    $session->save();

    $cookie = new Cookie($session->getName(), $session->getId());
    $client->getCookieJar()->set($cookie);

    return $client;
  }

}