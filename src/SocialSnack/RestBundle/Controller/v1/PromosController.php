<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PromosController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_v1_promos_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Promos:default', ['request' => $request]);
	}

  /**
   * @Route("/{type}", name="rest_v1_promos_type")
   * @Method({"GET"})
   */
  public function typeAction($type, Request $request) {
    return $this->forward('SocialSnackRestBundle:Promos:type', ['type' => $type, 'request' => $request]);
  }

  /**
   * @Route("/{type}/{id}", name="rest_v1_promos_target")
   * @Method({"GET"})
   */
  public function targetAction($type, $id, Request $request) {
    return $this->forward('SocialSnackRestBundle:Promos:target', ['id' => $id, 'type' => $type, 'request' => $request]);
  }


}