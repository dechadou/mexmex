<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Invitado Especial se une a PAYBACK - Cinemex');
$view['slots']->start('iebody');
?>
<h1 class="content-box-title"><span class="icon icon-light icon-crown no-txt"></span> Invitado Especial se une a PAYBACK</h1>

<div class="entry-body">
  <p>
    A partir de  agosto  de 2014 Invitado Especial se une al  Programa de Lealtad más grande de México, PAYBACK.<br/>
    A partir de ahora, podrás utilizar tu tarjeta Invitado Especial Cinemex PAYBACK con los diferentes socios de PAYBACK. Podrás acumular puntos en American Express Vacations, Comercial Mexicana, Hertz, Interjet, 7-Eleven, Petro-7, Telmex, American Express, Krispy Kreme, Sbarro, El Farolito, Taco Inn, Neve Gelato, Arrachera House, Café Diletto, Sixtie´s Burger, Nuny’s.<br/>
    Por lo tanto, si ya eras Invitado Especial Cinemex no olvides pasar a tu complejo Cinemex de preferencia a cambiar tu tarjeta actual por la nueva tarjeta  de Invitado Especial, ya que a partir del 1° de enero de 2015 ya no la podrás utilizar.
  </p>

  <h3>Las visitas y los puntos acumulados IE se migrarán a la nueva tarjeta Invitado Especial</h3>
  <p>
    Las visitas y los puntos acumulados hasta ahora, se migrarán a tu nuevo plástico de Invitado Especial Cinemex PAYBACK y los podrás utilizar únicamente en Cinemex. <br/>
    Los puntos que empieces a acumular en tu nueva tarjeta Invitado Especial los podrás usar tanto en Cinemex como en los demás socios participantes del Programa PAYBACK.
  </p>

  <h3>Beneficios</h3>
  <p>No te preocupes, los beneficios continúan siendo los mismos. Seguirás gozando de los diferentes beneficios según el nivel que te corresponda. La diferencia es que ahora podrás acumular y redimir puntos en más establecimientos. </p>

  <h3>Monederos Payback</h3>
  <p>Ahora todos los monederos PAYBACK serán bienvenidos en nuestros complejos Cinemex para la acumulación y redención de puntos. Si cuentas con un monedero PAYBACK con imagen de algún otro socio y quieres gozar de los beneficios de Invitado Especial, sólo acude al Centro de Atención al Invitado y regístrate con Cinemex, no es necesario tramitar una nueva tarjeta.  Con sólo registrarte, podrás empezar a acumular visitas que te permitirán alcanzar el nivel Oro y Premium.</p>

  <h3>Nuevas fechas de vigencia para los niveles Oro y Premium</h3>
  <p>
    Ahora para alcanzar los niveles Oro y Premium tendrás que juntar las visitas suficientes para cada nivel en el mismo año en el que   te inscribas al Programa de Invitado Especial PAYBACK.<br/>
    Por ejemplo, si te inscribes en marzo  de 2015 tendrás hasta el 31 de diciembre de 2015 para juntar tus visitas y alcanzar el nivel Oro o Premium
  </p>

  <p>
    <strong>Para el nivel Oro =</strong> deberás acumular 12 visitas* <br/>
    <strong>Para el nivel Premium =</strong> deberás acumular 24 visitas*
  </p>

  <p>
    Por lo tanto sin importar el mes en el que realices tu inscripción, deberás juntar tus visitas para el 31 de diciembre del año en el cual te inscribiste y realizar el cambio de nivel a partir de enero del próximo año de tu inscripción.
  </p>

  <p>
    Sin embargo, si acumulas las visitas suficientes, ya sean 12 o 24, antes del 30 de junio de 2015, podrás realizar tu cambio de nivel a partir de julio y no tendrás que esperarte hasta diciembre
  </p>
</div>
<?php $view['slots']->stop(); ?>