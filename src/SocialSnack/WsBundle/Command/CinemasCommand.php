<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class CinemasCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:cinemas' )
			->setDescription( 'Fetch cinemas from the Web Service' )
      ->addOption( 'area_id',  'a', InputArgument::OPTIONAL )
      ->addOption( 'add_only', 'o', InputArgument::OPTIONAL, 'Don\'t update existing cinemas. Just add new.', TRUE );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
    $updater = $this->getContainer()->get('cinemex.cinema_updater');
    /* @var $updater \SocialSnack\WsBundle\Service\CinemaUpdater */
    $updater->setConsoleVerbose();
    return $updater->execute(array(
        'area_id'  => $input->getOption('area_id'),
        'add_only' => $input->getOption('add_only'),
    ));
	}
	
}