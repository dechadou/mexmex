#!/bin/bash

nohup php /var/www/cinemex/app/console rabbitmq:consumer update_movies &
nohup php /var/www/cinemex/app/console rabbitmq:consumer update_sessions &
nohup php /var/www/cinemex/app/console rabbitmq:consumer update_tickets &
nohup php /var/www/cinemex/app/console rabbitmq:consumer image_download &

