<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Webservice' );
$view['slots']->start('body');

?>
  <style>
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{ border-bottom:1px solid #ddd}
  </style>
  <section class="panel">
    <header class="panel-heading">
      Estado de actualización de carteleras
    </header>
    <div class="panel-body">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Complejo</th>
          <th>Periódica</th>
          <th>Manual</th>
          <th>Push</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($cinemas as $cinema) { ?>
          <tr>
            <th><?php echo $cinema->getName(); ?></th>
            <?php foreach (['cron', 'manual', 'ws'] as $source) { ?>
              <td><?php echo isset($entries[$cinema->getId()][$source]) ? $entries[$cinema->getId()][$source]['dateRan']->format('Y-m-d H:i:s') : '-'; ?></td>
            <?php } ?>
            <td>
              <?php
              $mostRecent = 0;
              foreach($entries[$cinema->getId()] as $_e) {
                if ($_e['id'] > $mostRecent) {
                  $mostRecent = $_e['id'];
                }

              }
              foreach (['cron', 'manual', 'ws'] as $source) {
                $status = isset($entries[$cinema->getId()][$source]) ? $entries[$cinema->getId()][$source]['status'] : '';
                $id     = isset($entries[$cinema->getId()][$source]) ? $entries[$cinema->getId()][$source]['id'] : '';
                if ($id == $mostRecent){
                  if ($status != '') {
                    switch($status){
                      case 'SUCCESS':
                        $label = 'success';
                        break;
                      case 'WARNING':
                        $label = 'warning';
                        break;
                      case 'ERROR':
                        $label = 'danger';
                        break;
                    }
                      ?>
                    <a href="#" class="expand">
                      <span class="label label-<?php echo $label ?> label-mini">
                        <?php echo $status ?>
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      </span>
                    </a>
                  <?php
                  }
                }
              }
              ?>
            </td>
          </tr>
          <tr colspan="5" style="display:none">
            <td colspan="5">
              <?php
              foreach (['cron', 'manual', 'ws'] as $source) {
                $status = isset($entries[$cinema->getId()][$source]) ? $entries[$cinema->getId()][$source]['status'] : '';
                $id     = isset($entries[$cinema->getId()][$source]) ? $entries[$cinema->getId()][$source]['id'] : '';
                if ($id == $mostRecent){
                  if ($status == 'ERROR'){
                    echo '<strong>Error:</strong> '.$entries[$cinema->getId()][$source]['result'];
                  } else if ($status == 'SUCCESS') {
                    echo '<strong>Importados:</strong> '.$entries[$cinema->getId()][$source]['result'];
                  } else if ($status == 'WARNING'){
                    echo '<strong>No hay funciones en este cine</strong>';
                  }
                }
              }
              ?>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </section>
  <script>
    $(document).ready(function(){
      $('a.expand').on('click',function(e){
        e.preventDefault();
        $(this).find('.glyphicon').toggleClass("glyphicon-minus").toggleClass("glyphicon-plus");
        $(this).parent().parent().next('tr').toggle();
      })
    })
  </script>
<?php $view['slots']->stop();