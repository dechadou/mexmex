<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Encuesta</title>

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,400italic,700|Roboto+Condensed:700">

    <style>
      @-webkit-keyframes fadeIn {
        0% {
          opacity: 0;
        }

        100% {
          opacity: 1;
        }
      }

      @keyframes fadeIn {
        0% {
          opacity: 0;
        }

        100% {
          opacity: 1;
        }
      }

      * {
        box-sizing:border-box;
      }
      *:before,
      *:after {
        box-sizing:border-box;
      }

      body {
        background:#fafafa;
        color:#373737;
        font-family:roboto, helvetica, sans-serif;
        font-size:16px;
        line-height:24px;
        margin:0;
        padding:0;
      }

      select {
        -webkit-appearance:none;
        -webkit-border-radius:0px;
      }

      .form-group {
        margin-bottom:24px;
      }

      .form-input {
        background:transparent;
        border:1px solid #f1f1f1;
        border-radius:3px;
        color:#f1f1f1;
        display:block;
        font-family:roboto, helvetica, sans-serif;
        font-size:16px;
        height:52px;
        line-height:24px;
        outline:none;
        padding:13px 14px;
        position:relative;
        width:100%;
      }

      .form-input:focus,
      .form-input-select.is-focused {
        box-shadow:0 0 7px rgba(255, 255, 255, .8);
      }

      .form-input::-moz-placeholder {
        color:rgba(255, 255, 255, .5);
        opacity:1;
      }

      .form-input:-ms-input-placeholder {
        color:rgba(255, 255, 255, .5);
      }

      .form-input::-webkit-input-placeholder {
        color:rgba(255, 255, 255, .5);
      }

      .form-input.has-placeholder {
        color:rgba(255, 255, 255, .5);
        opacity:1;
      }

      .form-input-select:after {
        border-left:6px solid transparent;
        border-right:6px solid transparent;
        border-top:6px solid #f1f1f1;
        content:'';
        margin:-3px 0 0;
        position:absolute;
        right:13px;
        top:50%;
      }

      .form-input-select select {
        height:100%;
        left:0;
        opacity:0;
        position:absolute;
        top:0;
        width:100%;
      }

      .btn {
        background:#ed023c;
        border:0;
        border-radius:3px;
        color:#f1f1f1;
        cursor:pointer;
        display:inline-block;
        font-family:inherit;
        font-size:inherit;
        line-height:inherit;
        padding:14px 50px 12px;
        text-transform:uppercase;
      }

      .container {
        margin:0 auto;
        padding:0 20px;
        max-width:810px;
      }

      .clear {
        clear:both;
      }

      .splash {
        background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/header-bg.png'); ?>);
        min-height:100vh;
      }

      .splash .container {
        left:50%;
        position:absolute;
        transform:translate3d(-50%, -55%, 0);
        top:50%;
      }

      .bubbles {
        box-sizing:initial;
        font-weight:300;
        height:52px;
        margin:0 auto 40px;
        list-style:none;
        padding:210px 250px;
        position:relative;
        width:190px;
      }

      .logo img {
        height:auto;
        width:190px;
      }

      .bubble,
      .point {
        height:190px;
        position:absolute;
        width:230px;
      }

      .bubble {
        background:#d5d5d5;
        border-radius:50%;
        box-shadow:0 5px 7px rgba(0, 0, 0, .2);
        left:50%;
        transform:translate3d(-50%, -50%, 0);
        transition:all .3s;
        top:50%;
      }

      .bubble p {
        left:50%;
        margin:0;
        position:absolute;
        text-align:center;
        transform:translate3d(-50%, -50%, 0);
        top:50%;
      }

      .point {
        position:absolute;
        animation:fadeIn 1s both;
        -webkit-animation:fadeIn 2s both;
        -moz-animation:fadeIn 2s both;
      }

      .point-1 {
        left:50%;
        transform:translate3d(-50%, 0, 0);
        top:0;
        -webkit-animation-delay:1s;
        -moz-animation-delay:1s;
      }

      .point-2 {
        opacity:1;
        margin-top:-115px;
        right:0;
        top:43%;

        -moz-animation-delay:.2s;
        -moz-animation-delay:.2s;
      }

      .point-3 {
        bottom:0;
        left:52%;
        animation-delay:2s;

        -webkit-animation-delay:.4s;
        -moz-animation-delay:.4s;
      }

      .point-4 {
        bottom:0;
        right:52%;

        -webkit-animation-delay:.6s;
        -moz-animation-delay:.6s;
      }

      .point-5 {
        margin-top:-115px;
        left:0;
        top:43%;

        -webkit-animation-delay:.8s;
        -moz-animation-delay:.8s;
      }

      .point.active,
      .point:hover,
      .bubble.active,
      .bubble:hover {
        z-index:1;
      }

      .bubble.active,
      .bubble:hover {
        background:#ed023c;
        color:#fff;
        font-size:18px;
        height:225px;
        width:265px;
      }

      .header h1 {
        display:none;
      }

      .header p {
        color:#7d7d7d;
        font-size:20px;
        font-weight:300;
        text-align:center;
      }

      .user-data {
        background:#ed023c url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/cinemex.png'); ?>) no-repeat 115% 50%;
        color:#f1f1f1;
        padding:100px 0;
      }

      .user-data-title,
      .user-data-form {
        float:left;
        width:50%;
      }

      .user-data-form label {
        display:block;
        font-size:14px;
        text-transform:uppercase;
      }

      .user-data-title {
        text-align:right;
      }

      .user-data-title h1 {
        font-size:50px;
        font-weight:300;
        line-height:1;
        margin:0 0 30px;
      }

      .user-data-title strong {
        display:block;
        font-weight:500;
      }

      .user-data-form {
        padding-left:30px;
      }

      .questions {
        background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/questions-bg.png'); ?>);
        padding:150px 0 1px;
      }

      .question {
        margin:0 0 150px;
      }

      .text {
        font-weight:300;
        font-size:25px;
        line-height:27.5px;
        margin:0 0 40px;
      }

      .text span {
        color:#ed023c;
        font-weight:500;
      }

      .answers {
        list-style:none;
        margin:0;
        padding:0;
      }

      .answers:before,
      .answers:after {
        display:table;
        content:" ";
      }

      .answers:after {
        clear:both;
      }

      .answer {
        float:left;
        position:relative;
        width:10%;
      }

      .answer label {
        border:4px solid #d7d7d7;
        border-radius:50%;
        color:#f1f1f1;
        display:block;
        font-size:14px;
        height:34px;
        line-height:26px;
        margin:0 auto;
        position:relative;
        text-align:center;
        width:34px;
      }

      .answer span {
        background:#d7d7d7;
        border:1px solid #d7d7d7;
        border-radius:50%;
        display:block;
        height:26px;
        line-height:25px;
        position:relative;
        width:26px;
        z-index:2;
      }

      .answer input {
        height:100%;
        left:0;
        opacity:0;
        margin:0;
        position:absolute;
        top:0;
        width:100%;
      }

      .answer:after,
      .answer label,
      .answer span {
        transition:all .2s;
      }

      .answer:after {
        background:#d7d7d7;
        content:'';
        height:2px;
        margin-top:-1px;
        position:absolute;
        top:50%;
        width:100%;
        z-index:1;
      }

      .answer:after {
        left:50%;
      }

      .answer:last-child:after {
        display:none;
      }

      .answer.active span {
        border-color:#ed023c;
        color:#ed023c;
      }

      .answer.current span {
        background-color:#ed023c;
        color:#f1f1f1;
        font-weight:700;
      }

      .answer.active:after {
        background-color:#ed023c;
      }

      .answer.current:after {
        background-color:#d7d7d7;
      }

      .help {
        background:#e7e7e7;
        color:#696969;
        font-size:13px;
        line-height:19.5px;
        left:50%;
        padding:6px 8px;
        position:absolute;
        text-align:center;
        transform:translate3d(-50%, 0, 0);
        top:110%;
        width:141px;
      }

      .help:after {
        border-bottom:5px solid #e7e7e7;
        border-left:5px solid transparent;
        border-right:5px solid transparent;
        content:'';
        position:absolute;
        top:-5px;
        left:50%;
        transform:translate3d(-50%, 0, 0);
      }

      .questions-send {
        text-align:center;
      }

      .footer {
        background:#575757;
        border-top:5px solid #ed023c;
        margin-top:100px;
        padding:58px 0;
        text-align:center;
      }

      .has-data .user-data-form {
        display:none;
      }

      .has-data .user-data-title {
        text-align:center;
        width:100%;
      }

      .has-data h1,
      .has-data img {
        display:inline-block;
        margin:0;
        text-align:right;
        vertical-align:middle;
      }

      .has-data h1 {
        margin-right:30px;
      }

      .error {
        background:#f2dede;
        border:1px solid #ebccd1;
        color:#a94442;
        margin-bottom:4em;
        padding:1.75em 1.5em;
      }

      .success {
        background:rgba(0, 0, 0, .5);
        height:100%;
        left:0;
        overflow:auto;
        position:fixed;
        top:0;
        width:100%;
        z-index:1000;
      }

      .success-message {
          background:#ed023c url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/header-bg-red.png'); ?>);
          left:50%;
          position:absolute;
          transform:translate(-50%, -50%);
          top:50%;
          border-radius:5px;
          padding:0 0 60px;
          max-width:650px;
          box-shadow:0 0 5px rgba(0, 0, 0, 1);
      }

      .success-message img {
        display:block;
        max-width:100%;
      }
    </style>
  </head>
  <body>
    <div class="splash">
      <div class="container">
        <ul class="bubbles" aria-hidden="true">
          <li class="logo"><img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/logo-cinemex-black.png'); ?>"></li>
          <li class="point point-1"><div class="bubble"><p>!Tu opinion es importante!</p></div></li>
          <li class="point point-2"><div class="bubble"><p>¿Te gustan las palominas acarameladas?</p></div></li>
          <li class="point point-3"><div class="bubble"><p>¿Fué buena tu experiencia en Cinemex?</p></div></li>
          <li class="point point-4"><div class="bubble"><p>¿Tu película se proyectó sin problemas?</p></div></li>
          <li class="point point-5"><div class="bubble"><p>¿Qué es lo que más te gusta de Cinemex?</p></div></li>
        </ul>

        <header class="header">
          <h1>!Tu opinion es importante!</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis<br>
            turpis vitae massa interdum, a malesuada lorem lobortis.</p>
        </header>
      </div>
    </div>

    <main>
      <div class="user-data <?php echo (isset($info->RESPUESTA->NUMEMPLEADO)) ? 'has-data' : NULL; ?>">
        <div class="container">
          <div class="user-data-title">
            <h1><strong>Cuéntanos</strong> tu experiencia</h1>
            <img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/bubble.png'); ?>">
          </div>

          <div class="user-data-form">
            <div class="form-group">
              <label for="cinema">¿Qué complejo visitaste?</label>

              <?php if (isset($info->RESPUESTA->PELICULA)) : ?>
                <input type="hidden" name="cinema" value="<?php echo $info->RESPUESTA->PELICULA; ?>">
              <?php else : ?>
                <div class="form-input form-input-select" data-placeholder="Selecciona un complejo">
                  <div class="value"></div>
                  <select id="cinema" name="cinema">
                    <option></option>
                    <option value="1">Cinemex Antara</option>
                  </select>
                </div>
              <?php endif; ?>
            </div>

            <div class="form-group">
              <label for="movie">¿Qué película viste?</label>
              <input id="movie" class="form-input" name="cinema" placeholder="Escribe el nombre de la pelÃ­cula que viste" <?php if (isset($info->RESPUESTA->PELICULA)) : ?>value="<?php echo $info->RESPUESTA->PELICULA; ?>" type="hidden"<?php endif; ?>>
            </div>

            <div class="form-group">
              <label for="date">¿Qué día fuiste?</label>
              <input id="date" class="form-input" name="date" placeholder="SelecciÃ³na una fecha" <?php
              if (isset($info->RESPUESTA->FECHA)) :
                $date = DateTime::createFromFormat('d/m/Y', $info->RESPUESTA->FECHA);
              ?>
                value="<?php echo $date->format('Y-m-d'); ?>"  type="hidden"
              <?php else : ?>
                type="date"
              <?php endif; ?>>
            </div>
          </div>

          <div class="clear"></div>
        </div>
      </div><!-- .user-data -->

      <div class="questions">
        <div class="container">
<?php if ($error === TRUE) : ?>
            <p class="error">Por favor, asegúrate de responder todas las preguntas y envía el formulario nuevamente.</p>
<?php endif; ?>

            <form method="post">
<?php foreach ($questions as $i => $question) : ?>
            <div class="question">
              <p class="text"><span><?php echo ($i + 1); ?>.</span> <?php echo $question['text']; ?>
                <?php if (isset($question['notes'])) : ?><small>(<?php echo $question['notes']; ?>)</small><?php endif; ?>
                </p>

              <ul class="answers">
                <?php for ($x = 1; $x <= 10; ++$x) : ?>
                  <li class="answer">
                    <label for="q<?php echo $i; ?>_<?php echo $x; ?>">
                      <input id="q<?php echo $i; ?>_<?php echo $x; ?>" name="form[<?php echo $question['field']; ?>]" value="<?php echo $x; ?>" type="radio">
                      <span><?php echo $x; ?></span>
                    </label>

                    <?php if ($x === 1) : ?>
                      <p class="help">Definitivamente<br> NO recomendaria</p>
                    <?php elseif ($x === 10) : ?>
                      <p class="help">Definitivamente<br> SI recomendaria</p>
                    <?php endif; ?>
                  </li>
                <?php endfor; ?>
              </ul>
            </div>
<?php endforeach; ?>

            <p class="questions-send">
              <button class="btn">Enviar</button>
            </p>
          </form>
        </div><!-- .container -->
      </div><!-- .questions -->
    </main>

    <footer class="footer">
      <img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/footer-logo.png'); ?>">
    </footer>

<?php if (isset($_GET['success'])) : ?>
    <div class="success">
      <div class="success-message"><img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/thanks.png'); ?>"></div>
    </div>
<?php endif; ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
      (function($){
        $('.form-input-select')
          .on('change', function(){
            var $el         = $(this),
                $select     = $el.find('select'),
                $value      = $el.find('.value'),
                placeholder = $el.data('placeholder'),
                value       = $select.val();

            if ($select.val() == '') {
              $value.text(placeholder);
              $el.addClass('has-placeholder');
            } else {
              $value.text($select.find(':selected').text());
              $el.removeClass('has-placeholder');
            }
          })
          .trigger('change');

        $('.form-input-select select')
          .on('focus', function(){
            var $select = $(this),
                $el     = $select.closest('.form-input-select');

            $el.addClass('is-focused')
          })
          .on('blur', function(){
            var $select = $(this),
              $el     = $select.closest('.form-input-select');

            $el.removeClass('is-focused')
          });

        $('.answers input').on('change', function(){
          var $input   = $(this),
              $list    = $input.closest('.answers'),
              $current = $input.closest('.answer');

          $list.find('.answer')
            .removeClass('active current');

          $current.prevAll('.answer').addClass('active');
          $current.addClass('active current');
        });
      })(jQuery);
    </script>
  </body>
</html>
