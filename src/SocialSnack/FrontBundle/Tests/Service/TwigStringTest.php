<?php

namespace SocialSnack\FrontBundle\Tests\Service;

use SocialSnack\FrontBundle\Service\TwigString;

class TwigStringTest extends \PHPUnit_Framework_TestCase {

  protected function _testIsFresh($currentTimestamp, $cacheTimestamp) {
    $service = $this->getMockBuilder('SocialSnack\FrontBundle\Service\TwigString')
        ->disableOriginalConstructor()
        ->setMethods(['getCurrentTimestamp'])
        ->getMock();

    $service->method('getCurrentTimestamp')
        ->willReturn($currentTimestamp);

    return $service->isFresh('foobar', $cacheTimestamp);
  }

  public function testIsFresh_Fresh() {
    $this->assertTrue($this->_testIsFresh(3600, 3599));
  }

  public function testIsFresh_Old() {
    $this->assertFalse($this->_testIsFresh(3800, 100));
  }

  public function testRender() {
    $twig = $this->getMock('Twig_Environment');
    $service = new TwigString($twig);

    $twig->expects($this->once())
        ->method('render')
        ->with(
            $this->identicalTo('foobar'),
            $this->identicalTo(['foo' => 'bar'])
        );

    $service->render('foobar', ['foo' => 'bar']);
  }

}