<?php

namespace SocialSnack\FrontBundle\Tests\Service\CachePurger;

class DoctrineCachePurgerTest extends \PHPUnit_Framework_TestCase {

  protected $browser;

  protected $router;

  protected $userAgent;

  public function setUp() {
    $this->browser   = $this->getMockBuilder('Buzz\Browser')->disableOriginalConstructor()->getMock();
    $this->router    = $this->getMockBuilder('Symfony\Component\Routing\Router')->disableOriginalConstructor()->getMock();
    $this->userAgent = 'foobar';
  }

  protected function getServiceMock(array $methods = NULL) {
    return $this->getMockBuilder('SocialSnack\FrontBundle\Service\CachePurger\DoctrineCachePurger')
        ->setConstructorArgs([$this->browser, $this->router, $this->userAgent])
        ->setMethods($methods)
        ->getMock();
  }


  public function testHandleSuccess() {
    $task = $this->getMock('SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface');
    $task->expects($this->once())->method('getDoctrineResources')->willReturn([
        ['ids' => ['foo', 'bar'], 'env' => 'front'],
        ['ids' => ['rab', 'oof'], 'env' => 'back'],
    ]);

    $service = $this->getServiceMock();
    $service->setEnv('front', ['hostname' => 'foobar.front', 'servers' => ['10.1.1.1', '10.1.1.2']]);
    $service->setEnv('back', ['hostname' => 'foobar.back', 'servers' => ['10.1.1.3', '10.1.1.4']]);

    $res = $this->getMock('Buzz\Message\MessageInterface');
    $res->method('getContent')->willReturn('["lorem","ipsum"]');

    $this->browser->expects($this->exactly(4))
        ->method('send')
        ->willReturn($res);

    $output = $service->handle($task);

    $expected = [
        '10.1.1.1: lorem',
        '10.1.1.1: ipsum',
        '10.1.1.2: lorem',
        '10.1.1.2: ipsum',
        '10.1.1.3: lorem',
        '10.1.1.3: ipsum',
        '10.1.1.4: lorem',
        '10.1.1.4: ipsum',
    ];
    $this->assertEquals($expected, $output);
  }


  public function testHandleTimeout() {
    $task = $this->getMock('SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface');
    $task->expects($this->once())->method('getDoctrineResources')->willReturn([
        ['ids' => ['foo', 'bar'], 'env' => 'front'],
        ['ids' => ['rab', 'oof'], 'env' => 'back'],
    ]);

    $service = $this->getServiceMock();
    $service->setEnv('front', ['hostname' => 'foobar.front', 'servers' => ['10.1.1.1', '10.1.1.2']]);
    $service->setEnv('back', ['hostname' => 'foobar.back', 'servers' => ['10.1.1.3', '10.1.1.4']]);

    $res = $this->getMock('Buzz\Message\MessageInterface');
    $res->method('getContent')->willReturn('["lorem","ipsum"]');

    $this->browser->expects($this->exactly(4))
        ->method('send')
        ->will($this->throwException(new \Exception('Foobar')));

    $output = $service->handle($task);

    $expected = [
        '10.1.1.1: Foobar',
        '10.1.1.2: Foobar',
        '10.1.1.3: Foobar',
        '10.1.1.4: Foobar',
    ];
    $this->assertEquals($expected, $output);
  }

}