<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

class PurgePremiumMap extends AbstractConceptMap {

  protected  function getRouteName() {
    return 'premium_cinemas';
  }


  protected function getConceptName() {
    return 'Premium';
  }

}