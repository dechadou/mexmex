<script>
  // FB SDK init callback
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo $view['fronthelper']->get_fb_app_id(); ?>', // App ID
      channelUrl : '<?php echo $view['assets']->getUrl( 'channel.html' ); ?>', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  };
</script>
<script>
  var url_prefix  = '<?php echo $is_dev ? '/app_dev.php/' : '/'; ?>';
  var rest_url    = '<?php echo $view['router']->generate( 'social_snack_rest' ); ?>';
  var debug       = true;
  var config      = {
    gmaps_api_key : '<?php echo $view['fronthelper']->get_gmaps_key(); ?>',
    fb : {
      app_id : '<?php echo $view['fronthelper']->get_fb_app_id(); ?>'
    }
  };
  var user_ie_url = '<?php echo $view['router']->generate( 'user_ie' ); ?>';
</script>