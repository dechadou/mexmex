<?php
/** @var \SocialSnack\FrontBundle\Entity\Landing $landing */
/** @var \SocialSnack\FrontBundle\Entity\LandingPage $page */

$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', $landing->getName() . ' - Cinemex');
$view['slots']->set('body_class', 'dynamic-sponsored landing-' . $page->getType());
?>

<?php $view['slots']->start('extra_styles'); ?>
<style>
  .dynamic-sponsored #sponsored-bg {
    background: url('<?php echo $view['assets']->getUrl($landing->getConfig()['bgimg'], 'CMS'); ?>');
  }
  body.dynamic-sponsored {
    background: #<?php echo $landing->getConfig()['bgcolor']; ?>;
  }
  .landing-content .sidebar-heading,
  .landing-content-bg {
    background: #<?php echo $landing->getConfig()['bgcolorcontainer']; ?>;
  }
  .landing-content .landing-title {
    color: #<?php echo $landing->getConfig()['titlecolor']; ?>;
  }
  .landing-content .sidebar-heading .btn {
    background: #<?php echo $landing->getConfig()['btcolor']; ?>;
    box-shadow: 0 2px 0 #<?php echo $landing->getConfig()['btcolorsec']; ?>;
  }
  .landing-content .sidebar-heading .btn-icon:after {
    background: #<?php echo $landing->getConfig()['btcolorsec']; ?>;
  }
  .section-nav a {
    background: #<?php echo $landing->getConfig()['tabscolor']; ?>;
  }
  .section-nav a.selected, .section-nav a:hover {
    background: #<?php echo $landing->getConfig()['tabscoloron']; ?>;
  }
</style>
<?php echo $landing->getConfigValue('htmlhead') ?: ''; ?>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
<div class="dynamic-landing">
  <?php if ($landing->getConfig()['header']) { ?>
  <div class="section-header">
    <img src="<?php echo $view['assets']->getUrl($landing->getConfig()['header'], 'CMS'); ?>" alt="<?php echo $landing->getName(); ?>">
  </div>
  <?php } ?>

  <?php if (count($landing->getPages()) > 1) { ?>
  <div class="section-nav">
    <ul>
      <?php foreach ($landing->getPages() as $_page) { ?>
        <li><a href="<?php echo $view['fronthelper']->get_permalink($_page); ?>" <?php if ($page->getId() === $_page->getId()) : ?>class="selected"<?php endif; ?>><?php echo $_page->getName(); ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>

  <?php echo $page_template; ?>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after'); ?>
<?php echo $landing->getConfigValue('htmlbody') ?: ''; ?>
<?php $view['slots']->stop(); ?>