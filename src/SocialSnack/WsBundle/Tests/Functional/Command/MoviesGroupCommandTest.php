<?php

namespace SocialSnack\WsBundle\Tests\Functional\Command;

use SocialSnack\WsBundle\Tests\Functional\WebTestCase;

class MoviesGroupCommandTest extends WebTestCase {

  protected function getAllMovies() {
    return $this->getContainer()->get('doctrine')
        ->getRepository('SocialSnackWsBundle:Movie')
        ->createQueryBuilder('q')
        ->select('q')
        ->getQuery()
        ->getResult();
  }


  /**
   * Movies without a group ID must be updated with a new unique group ID.
   *
   * @group functional
   */
  public function testAssignDefaultGroupId() {
    // Old movies without active sessions don't need to be taken care of.

    $this->loadFixtures([
        'SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand\AssignDefaultGroupIdData'
    ]);

    $this->runCommand('ws:movies:group');

    $movies = $this->getAllMovies();
    $groupIds = [];
    foreach ($movies as $movie) {
      // Each movie must have a group ID assigned.
      $this->assertNotEquals(0, $movie->getGroupId());
      $groupIds[] = $movie->getGroupId();
    }

    // Each group ID must be unique.
    $this->assertEquals(count($groupIds), count(array_unique($groupIds)));
  }


  /**
   * A grouper must be created for new groups.
   *
   * @group functional
   */
  public function testCreateNewGrouper() {
    $this->loadFixtures([
        'SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand\CreateNewGrouperData'
    ]);

    $this->runCommand('ws:movies:group');

    $movies = $this->getAllMovies();

    // New grouper must have been created.
    $this->assertEquals('grouper', $movies[3]->getType());

    // Versions with group must be associated to the grouper.
    $this->assertSame($movies[0]->getParent(), $movies[3]);
    $this->assertSame($movies[1]->getParent(), $movies[3]);

    // Version without group must be left intact.
    $this->assertNull($movies[2]->getParent());
  }


  /**
   * Grouper attributes must be set based on active children attributes.
   *
   * @group functional
   */
  public function testAttributesInheritance() {
    $this->loadFixtures([
        'SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand\AttributesInheritanceData'
    ]);

    $this->runCommand('ws:movies:group');

    $movies = $this->getAllMovies();

    $expectedAttrs = [
        'v4d'     => TRUE,
        'premium' => TRUE,
        'v3d'     => TRUE,
        'nfl'     => TRUE,
    ];
    $this->assertEquals((array)$movies[3]->getAttr(), $expectedAttrs);
  }

}