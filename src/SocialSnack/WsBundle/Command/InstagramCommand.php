<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Instagram;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class InstagramCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:instagram' )
			->setDescription( 'Fetch photos from Instagram API' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$em           = $doctrine->getManager();
    $conn         = $doctrine->getConnection();
    $repo         = $doctrine->getRepository( 'SocialSnackWsBundle:Instagram' );
    $instagram    = $this->getContainer()->getParameter( 'instagram' );
    $access_token = $instagram['access_token'];
    $url          = 'https://api.instagram.com/v1/users/self/media/recent?access_token=' . $access_token;
    $ch           = curl_init( $url );
    $count        = 0;
    
		curl_setopt( $ch, CURLOPT_HEADER,         FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 20 );
		curl_setopt( $ch, CURLOPT_TIMEOUT,        20 );
		$res = curl_exec( $ch );

		if ( !$res ) {
			throw new \Exception( curl_error( $ch ) );
		}

    $json = json_decode( $res );

    foreach ( array_reverse( $json->data ) as $item ) {
      $exists = $repo->findOneBy( array( 'legacy_id' => $item->id ) );
      if ( $exists )
        continue;
      $inst = new Instagram();
      $inst->setLegacyId( $item->id );
      $inst->setLink( $item->link );
      $inst->setThumb( $item->images->thumbnail->url );
      $inst->setImage( $item->images->standard_resolution->url );
      $inst->setData( json_encode( $item ) );
      $em->persist( $inst );
      $count++;
    }
    
    $em->flush();
    
    $output->writeln( sprintf( '%d new images.', $count ) );
  }
  
}