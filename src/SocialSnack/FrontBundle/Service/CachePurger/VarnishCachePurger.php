<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use Buzz\Message\Request;

class VarnishCachePurger extends CachePurgerUrlAbstract {

  protected function purge($path, $host, $server) {
    $req = new Request('BAN', $path, $server);
    $req->addHeader(sprintf('Host: %s', $host));

    // @todo Allow the use of wildcards optionally.
    if ($path !== '/') {
      $path .= '.*';
    }

    try {
      $res = $this->browser->send($req);
      if (preg_match('/<h1>(?<title>.+)<\/h1>/', $res->getContent(), $matches)) {
        $output = $matches['title'];
      } else {
        $output = 'Unexpected response.';
      }
    } catch (\Exception $e) {
      $output = $e->getMessage();
    }

    return sprintf('%s: Varnish Cache %s: %s', $server, $path, $output);
  }

}