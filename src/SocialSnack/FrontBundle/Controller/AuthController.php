<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\FrontBundle\Exception\CustomHttpException;
use SocialSnack\RestBundle\Exception\InvalidResetPassCodeException;
use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\RestBundle\Exception\RestPassCodeUserMismatchException;
use SocialSnack\RestBundle\Exception\UserNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use SocialSnack\FrontBundle\Form\Type\LoginType;
use SocialSnack\FrontBundle\Form\Type\RegistrationType;
use SocialSnack\FrontBundle\Form\Model\Registration;

class AuthController extends BaseController {
  
  /**
   * @Route("/registro", name="user_register")
   * @Method({"GET", "POST"})
   * @Template(engine="php")
   */
  public function registerAction() {
    $end_url = $this->getRequest()->get( 'redirect_to' );
    if ( !$end_url ) {
      $end_url = $this->generateUrl( 'user_profile' );
    }
    
    $registration = new Registration;
    $form = $this->createForm( new RegistrationType(), $registration, array(
        'action' => $this->generateUrl( 'user_register' ),
    ) );
    
    $response = $this->render(
        'SocialSnackFrontBundle:Auth:register.html.php',
        array(
            'no_cache_header_user' => TRUE,
            'form' => $form->createView(),
            'end_url' => $end_url,
        )
    );
    $response->setPublic();
    $response->setSharedMaxAge( 7 * 24 * 60 * 60 ); // 1 week.
    return $response;
  }
  
  
  /**
   */
  public function registerPostAction() {
    $request      = $this->getRequest();
    $registration = new Registration;
    $form         = $this->createForm( new RegistrationType(), $registration, array(
        'action' => $this->generateUrl( 'user_register' )
    ) );
    
    $form->handleRequest( $request );
    try {
      $pass_not_empty = $form->getData()->getUser()->getPlainPassword();
    } catch ( \Exception $e ) {
      $pass_not_empty = FALSE;
    }
    if ( $form->isValid() && $pass_not_empty ) {
      $em           = $this->getDoctrine()->getManager();
      $registration = $form->getData();
      $user         = $registration->getUser();
      
      $factory = $this->get('security.encoder_factory');
      $encoder = $factory->getEncoder($user);
      $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
      $user->eraseCredentials();
      $em->persist( $user );
      $em->flush();
      
      // Deliver the welcome email.
      $this->get('old_sound_rabbit_mq.send_mail_producer')->publish( json_encode( array(
          'type' => 'welcome',
          'user_id' => $user->getId()
      ) ) );
      
      // Now login the user.
      $token = new UsernamePasswordToken( $user, $user->getPassword(), 'main', $user->getRoles());
      $this->get("security.context")->setToken($token);

      // Fire the login event
      // Logging the user in above the way we do it doesn't do this automatically
      $event = new InteractiveLoginEvent($request, $token);
      $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

      $response = new RedirectResponse( $this->generateUrl( 'user_profile' ) );
      $response->headers->setCookie( new \Symfony\Component\HttpFoundation\Cookie( 'fetch_cache', '1', 0, '/', null, false, false ) );
      
      return $response;
    }
    
    return $this->redirect( $this->generateUrl( 'user_register' ) );
  }
  
  
  /**
   * @Route("/login", name="user_login")
   * @Method({"GET"})
   * @Template(engine="php")
   */
  public function loginAction() {
    $flash_msgs = $this->get_flash_bag();
    
    return array(
				'no_cache_header_user' => TRUE,
				'flash_msgs' => $flash_msgs
		);
  }
  
  
  /**
   * @Route("/recuperar-password", name="user_forgot_pass")
   * @Method({"GET"})
   * @Template(engine="php")
   */
  public function forgotPassAction() {
    $flash_msgs = $this->get_flash_bag();
    
    return array(
				'no_cache_header_user' => TRUE,
				'flash_msgs' => $flash_msgs
		);
  }
  
  
  /**
   * @Route("/restablecer-password/", name="user_reset_pass")
   * @Method({"GET"})
   * @Template(engine="php")
   */
  public function resetPassAction(Request $request) {
    /** @var \SocialSnack\RestBundle\Handler\AuthHandler $handler */
    $handler    = $this->get('rest.auth.handler');
    $flash_msgs = $this->get_flash_bag();
    $success    = $request->query->get('success');

    if (!$success) {
      $code = $request->query->get('code');
      $code = $handler->getResetPassCode($code);
    } else {
      $code = NULL;
    }

    if (!$code && !$success) {
      throw new CustomHttpException(400, 'Este enlace ya no es válido. Por favor, inicia el proceso nuevamente.');
    }

    return array(
        'no_cache_header_user' => TRUE,
        'flash_msgs'           => $flash_msgs,
        'code'                 => $code ? $code->getCode() : NULL,
        'success'              => $success,
		);
  }


  /**
   * @Route("/restablecer-password/")
   * @Method({"POST"})
   * @Template(engine="php")
   *
   * @param Request $request
   * @return JsonResponse
   * @throws RestException
   */
  public function resetPassPostAction(Request $request) {
    $handler  = $this->get('rest.auth.handler');
    $code     = $request->request->get('code');
    $password = $request->request->get('password');
    $email    = $request->request->get('email');

    try {
      if ( !$password || !$code || !$email ) {
        throw new MissingFieldsException();
      }

      $handler->resetPass($code, $password, $email);
    } catch (\Exception $e) {

      if ($e instanceof MissingFieldsException) {
        $errorcode = 'missing-fields';
        $error     = 'Por favor, completa todos los campos';
      } elseif ($e instanceof InvalidResetPassCodeException) {
        $errorcode = 'invalid-code';
        $error     = 'El código de recuperación de contraseña no es válido.';
      } elseif ($e instanceof UserNotFoundException) {
        $errorcode = 'user-not-found';
        $error     = 'No se encontró ningún usuario con el email ingresado.';
      } elseif ($e instanceof RestPassCodeUserMismatchException) {
        $errorcode = 'email-mismatch';
        $error     = 'El código de recuperación de contraseña no coincide con el email ingresado.';
      } else {
        $errorcode = 'error';
        $error     = 'Ocurrió un error. Por favor, verifica los datos ingresados e inténtalo nuevamente.';
      }

      if ($request->isXmlHttpRequest()) {
        throw new RestException($errorcode, $e->getMessage());
      }
    }

    if ($request->isXmlHttpRequest()) {
      return new JsonResponse(array());
    } else {
      $args = [];

      if (isset($errorcode)) {
        $args['error'] = $errorcode;
        $args['code']  = $code;
        $request->getSession()->getFlashBag()->add('notice', $error);
      } else {
        $args['success'] = 1;
      }

      return $this->redirect($this->generateUrl('user_reset_pass', $args));
    }
  }
  
  
  /**
   * @Route("/logout", name="logout")
   */
  public function logoutAction() {
    
  }
    
  
  /**
   * @Route("/login_check", name="login_check")
   */
  public function loginCheckAction() {
    
  }
  
}