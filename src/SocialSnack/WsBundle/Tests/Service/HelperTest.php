<?php

namespace SocialSnack\WsBundle\Tests\Service;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Service\Helper;

/**
 * Class HelperTest
 * @package SocialSnack\WsBundle\Tests\Service
 * @author Guido Kritz
 */
class HelperTest extends WebTestCase {

  /**
   * @todo Maybe this code is too unclear for a unit test? Think if there's a better way to do it...
   */
  public function testGetPremiumCinemas() {
    $cinemas = Helper::get_premium_cinemas();
    $keys = TRUE;
    $values = TRUE;
    foreach ($cinemas as $k => $v) {
      $keys &= is_numeric($k);
      $values &= is_array($v) || '*' === $v;
    }

    $this->assertEquals(1, $keys);
    $this->assertEquals(1, $values);
  }

  /**
   * @todo Maybe this is too much hardcode for a unit test? Think if there's a better way to do it...
   */
  public function testSessionIsPremium() {
    $cinemas = Helper::get_premium_cinemas();
    $cinema_id = key($cinemas);
    $screen = $cinemas[$cinema_id][0];
    $session = new Session();
    $session->setLegacyCinemaId($cinema_id);
    $session->setInfo(json_encode(array('SCREENNAME' => 'Sala ' . $screen)));

    $this->assertTrue(Helper::session_is_premium($session));
  }

  public function testGetSessionScreenNumber() {
    $session = new Session();
    $session->setInfo(json_encode(array('SCREENNAME' => 'Sala 14')));

    $this->assertEquals(14, Helper::get_session_screen_number($session));
  }

  /**
   * @todo Not sure if is a good practice to declare a dummy class inside this same file...
   *       Using a mock seems to overkill tho...
   */
  public function testGetFromResultById() {
    $result = array(
        new Foobar(1),
        new Foobar(2),
        new Foobar(3),
    );

    $item = Helper::get_from_result_by_id($result, 2);

    $this->assertInstanceOf('\SocialSnack\WsBundle\Tests\Service\Foobar', $item);
    $this->assertEquals(2, $item->getId());
  }

  public function testGetFromResultByLegacyId() {
    $result = array(
        new Foobar(1),
        new Foobar(2),
        new Foobar(3),
    );

    $item = Helper::get_from_result_by_legacy_id($result, 2000);

    $this->assertInstanceOf('\SocialSnack\WsBundle\Tests\Service\Foobar', $item);
    $this->assertEquals(2, $item->getId());
  }

  public function testGetFromResultByLegacyIdBis() {
    $result = array(
        new Foobar(1),
        new Foobar(2),
        new Foobar(3),
    );

    $item = Helper::get_from_result_by_legacy_id_bis($result, 'bis2');

    $this->assertInstanceOf('\SocialSnack\WsBundle\Tests\Service\Foobar', $item);
    $this->assertEquals(2, $item->getId());
  }


  public function testSortMoviesDefault() {
    $m1 = new Movie();
    $m1
        ->setName('First')
        ->setPremiere(TRUE)
        ->setPosition(1000)
    ;

    $m2 = new Movie();
    $m2
        ->setName('Second')
        ->setPremiere(TRUE)
        ->setPosition(500)
    ;

    $m3 = new Movie();
    $m3
        ->setName('Third')
        ->setPremiere(FALSE)
        ->setPosition(400)
    ;

    $m4 = new Movie();
    $m4
        ->setName('Fourth')
        ->setPremiere(FALSE)
        ->setPosition(1)
    ;

    $input  = [$m4, $m3, $m1, $m2];
    Helper::sortMoviesDefault($input);

    $this->assertEquals('First',  $input[0]->getName());
    $this->assertEquals('Second', $input[1]->getName());
    $this->assertEquals('Third',  $input[2]->getName());
    $this->assertEquals('Fourth', $input[3]->getName());
  }


  public function testSortMoviesByName() {
    $m1 = new Movie();
    $m1->setName('AAA');

    $m2 = new Movie();
    $m2->setName('BBB');

    $m3 = new Movie();
    $m3->setName('ZZZ');

    $m4 = new Movie();
    $m4->setName('JJJ');

    $input  = [$m3, $m4, $m1, $m2];
    Helper::sortMoviesByName($input);

    $this->assertEquals('AAA', $input[0]->getName());
    $this->assertEquals('BBB', $input[1]->getName());
    $this->assertEquals('JJJ', $input[2]->getName());
    $this->assertEquals('ZZZ', $input[3]->getName());
  }

}


class Foobar {
  public $id;

  public function __construct($id) {
    $this->id = $id;
  }

  public function getId() {
    return $this->id;
  }

  public function getLegacyId() {
    return $this->id * 1000;
  }

  public function getLegacyIdBis() {
    return 'bis' . $this->id;
  }
}
