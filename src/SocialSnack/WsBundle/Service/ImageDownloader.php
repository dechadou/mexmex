<?php
namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;


class ImageDownloader extends WorkerAbstract {
  
  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
	protected $container;
	
	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;
	
  /**
   * @var \SocialSnack\WsBundle\Request\Request
   */
	protected $cinemex_ws;
	
  
  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
   * @param \SocialSnack\WsBundle\Request\Request $cinemex_ws
   */
	public function __construct($container, $doctrine, $cinemex_ws) {
		$this->container  = $container;
		$this->doctrine   = $doctrine;
		$this->cinemex_ws = $cinemex_ws;
	}
	
  
  public function execute($data) {
    $this->setConsoleVerbose();
		try {
      $ch = curl_init($data['url']);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			$bytes = curl_exec($ch);
      if ( curl_error($ch) ) {
        throw new \Exception(sprintf('cURL error: %s', curl_error($ch)));
      }
      if ( !$bytes ) {
        throw new \Exception(sprintf('Failed to download %s', $data['url']));
      }
      
      curl_close($ch);
      
			$this->consoleInfo(sprintf( 'Image downloaded: %s', $data['filename'] ));
			
			// Copy to CDN.
      /** @todo Use a more efficient system to replicate this across the nodes */
      $this->consoleInfo(sprintf( 'Upload %s to CDN', $data['filename'] ));
      $this->container->get('ss.front.utils')->upload_movie_poster($data['filename'], $bytes);
      unset($bytes);
      
		} catch ( \Exception $e ) {
      $this->setError($e->getMessage());
      $this->consoleInfo($e->getMessage());
			return FALSE;
		}
		return TRUE;
  }
  
}