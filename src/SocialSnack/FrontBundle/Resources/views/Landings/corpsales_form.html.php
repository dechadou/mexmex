<?php $view->extend('SocialSnackFrontBundle:Landings:corpsales.html.php'); ?>

<?php $view['slots']->start('corpsales_body'); ?>

<div class="content-box">
  <h1 class="content-box-title">Informes y contacto</h1>

<?php if ($success === TRUE) : ?>
  <p>Ya hemos recibido tus comentarios y consultas, te responderemos a la brevedad.</p>
<?php else : ?>
  <form class="std-form sym-form" method="post" data-basicvalidate="1" novalidate>
    <div class="form-errors form-row wide">
      <?php echo $view['form']->errors($form); ?>
    </div>

    <?php
    echo $view['form']->row($form['name']);
    echo $view['form']->row($form['email']);
    echo $view['form']->row($form['company']);
    echo $view['form']->row($form['phone']);
    echo $view['form']->row($form['event_type']);
    echo $view['form']->row($form['cinema']);
    echo $view['form']->row($form['event_date']);
    echo $view['form']->row($form['event_time']);
    echo $view['form']->row($form['capacity']);
    echo $view['form']->row($form['need_candy']);
    echo $view['form']->row($form['comments']);
    ?>

    <div class="form-row wide clear">
      <div id="checkout-captcha-row" style="display:block;">
        <label for="checkout-captcha-input">Ingresa el código de verificación:</label>

        <input type="text" name="captcha" id="checkout-captcha-input" value="" class="std-field" placeholder="Ingresa el código..."/>

        <figure id="checkout-captcha-fig">
          <img src="<?php echo $view['router']->generate('captcha'); ?>">
          <a class="captcha-reload icon icon-reload no-txt" title="Cargar otro código">Cargar otro código</a>
        </figure>
      </div>
    </div>

    <?php echo $view['form']->row($form['submit']); ?>
  </form>
  <?php
//  echo $view['form']->errors($form);
//  echo $view['form']->form($form, []);
  ?>

<?php endif; ?>
</div>

<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script>
(function($){
  $('#form_event_type').on('change', function(){
    var field = $(this),
        other = $('#form_cinema, #form_event_date_day, #form_event_date_month, #form_event_date_year, #form_event_time_hour, #form_event_time_minute, #form_capacity, #form_need_candy');

    if (jQuery.inArray(field.val(), ['tickets', 'etickets']) !== -1) {
      other
        .attr('disabled', 'disabled')
        .removeAttr('required');
    } else {
      other
        .removeAttr('disabled')
        .prop('required', true);
    }
  });
})(jQuery);
</script>
<?php $view['slots']->stop(); ?>
