<?php $view->extend('SocialSnackAdminBundle::base.html.php'); ?>

<?php $view['slots']->start('body'); ?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Hoy
          <?php echo sprintf('<small><span class="label label-%s">%s %s%%</span></small>',
              $today['transactions']['delta'] >= 0 ? 'success' : 'danger',
              $today['transactions']['delta'] > 0 ? '+' : '',
              number_format($today['transactions']['delta'], 2)
          ); ?>
        </header>
        <div class="panel-body">
          <p class="stats-list">
            <strong>Transacciones:</strong>
            <?php
            echo sprintf('%s <span class="compare">/ %s</span>',
                number_format($today['transactions']['count'], 0),
                number_format($today['transactions']['comp_count'], 0)
            );
            ?>
          </p>
<!--          <p class="stats-list">-->
<!--            <strong>Boletos:</strong>-->
<!--            --><?php
//            echo sprintf('%s <span class="compare">/ %s</span>',
//                number_format($today['tickets']['count'], 0),
//                number_format($today['tickets']['comp_count'], 0)
//            );
//            ?>
<!--          </p>-->
        </div>
      </section>

      <section class="panel">
        <header class="panel-heading">
          Últimas 24 horas
          <?php echo sprintf('<small><span class="label label-%s">%s %s%%</span></small>',
              $last24['transactions']['delta'] >= 0 ? 'success' : 'danger',
              $last24['transactions']['delta'] > 0 ? '+' : '',
              number_format($last24['transactions']['delta'], 2)
          ); ?>
        </header>
        <div class="panel-body">
          <p class="stats-list">
            <strong>Transacciones:</strong>
            <?php
            echo sprintf('%s <span class="compare">/ %s</span>',
                number_format($last24['transactions']['count'], 0),
                number_format($last24['transactions']['comp_count'], 0)
            );
            ?>
          </p>
<!--          <p class="stats-list">-->
<!--            <strong>Boletos:</strong>-->
<!--            --><?php
//            echo sprintf('%s <span class="compare">/ %s</span>',
//                number_format($last24['tickets']['count'], 0),
//                number_format($last24['tickets']['comp_count'], 0)
//            );
//            ?>
<!--          </p>-->
        </div>

        <div class="panel-body text-center">
          <canvas id="myChart" width="600px" height="350px"></canvas>
        </div>
      </section>
      <section class="panel">
        <header class="panel-heading">
          Transacciones por medio
        </header>

        <div class="panel-body text-center">
          <canvas id="myChart2" width="600px" height="350px"></canvas>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/chart-master/Chart.min.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/Chart.StackedBar.js/Chart.StackedBar.js') ?>"></script>
  <script>
    var ctx = $("#myChart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);

    var data = {
      labels: <?php echo json_encode($labels); ?>,
      datasets: [
        {
          data: <?php echo json_encode(array_values($transactions)); ?>,
          label: 'Últimas 24 horas',
          fillColor: "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor: "rgba(151,187,205,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)"
        },
        {
          data: <?php echo json_encode(array_values($transactions_7)); ?>,
          label: "7 días atrás",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)"
        }
      ]
    };
    var myLineChart = new Chart(ctx).Line(data, {
      bezierCurve : false,
      multiTooltipTemplate : "<%=datasetLabel %>: <%= value %>"
    });


    var ctx = $("#myChart2").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);

    var datasets = [];
    <?php
    $colors = ['77,77,77', '93,165,218', '250,164,58', '96,189,104', '178,118,178', '222,207,63', '241,88,84'];
    $i = 0;
    ?>
    <?php foreach ($trans_by_app as $app_id => $trans) { ?>
    <?php $color = $colors[$i]; $i++; if ($i >= sizeof($colors)) { $i = 0; } ?>
    datasets.push({
      data: <?php echo json_encode(array_values($trans)); ?>,
      label: '<?php echo $apps[$app_id]->getName(); ?>',
      fillColor: "rgba(<?php echo $color; ?>,0.9)",
      strokeColor: "rgba(<?php echo $color; ?>,1)"
    });
    <?php } ?>
    var data = {
      labels: <?php echo json_encode($labels); ?>,
      datasets: datasets
    };
    var myLineChart = new Chart(ctx).StackedBar(data, {
      multiTooltipTemplate : "<%=datasetLabel %>: <%= value %>"
    });
  </script>
<?php $view['slots']->stop(); ?>