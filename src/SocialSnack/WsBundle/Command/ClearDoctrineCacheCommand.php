<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClearDoctrineCacheCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:doctrinecc' )
			->setDescription( 'Clear doctrine cache' )
      ->addOption('cache_id', NULL, InputOption::VALUE_REQUIRED);
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$doctrine    = $this->getContainer()->get( 'doctrine' );
		$em          = $doctrine->getManager();
    $cache_id    = $input->getOption('cache_id');
		
    $output->writeln($cache_id);
    $cacheDriver = $em->getConfiguration()->getResultCacheImpl();
    $success = $cacheDriver->delete($cache_id);
    $output->writeln( $success ? 'Success!' : 'Fail.' );
	}
	
}