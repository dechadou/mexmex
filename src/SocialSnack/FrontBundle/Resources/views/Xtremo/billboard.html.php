<?php $view->extend('SocialSnackFrontBundle:Xtremo:base.html.php'); ?>

<?php $view['slots']->start('section_body'); ?>
  <div id="billboard-movies">
  <?php
  $movies_per_page = 18;
  $movies_cols     = 6;

  echo $view->render('SocialSnackFrontBundle:Partials:bodyMovies.html.php', [
    'movies'          => array_slice( $movies, 0, $movies_per_page ),
    '_movies'         => $_movies,
    'movies_per_page' => $movies_per_page,
    'movies_cols'     => $movies_cols,
  ]);
  ?>
  </div><!-- #billboard-movies -->
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('js_after'); ?>
<script type="text/javascript">var sscUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'clickserv.sitescout.com/conv/0983a61245b8ae91'; new Image().src = sscUrl;</script>

<script type="text/javascript">var sscUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'clickserv.sitescout.com/conv/74bddb0d14ed0550'; new Image().src = sscUrl;</script>

<script type="text/javascript">var sscUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'clickserv.sitescout.com/conv/dc15046c7b14f219'; new Image().src = sscUrl;</script>

<script type="text/javascript">var sscUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'clickserv.sitescout.com/conv/f7bdd20c2b922dbf'; new Image().src = sscUrl;</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', '888756381220410');
  fbq('track', "PageView");
  fbq('track', "cinemextremostarwarsovrmind");
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=888756381220410&ev=PageView&noscript=1"
      /></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Code for Cinemextremo - Starwars Conversion Page -->
<script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 936240956;
  var google_conversion_language = "en";
  var google_conversion_format = "3";
  var google_conversion_color = "ffffff";
  var google_conversion_label = "PTfkCNDGvGIQvM63vgM";
  var google_remarketing_only = false;
  /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
  <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/936240956/?label=PTfkCNDGvGIQvM63vgM&amp;guid=ON&amp;script=0"/>
  </div>
</noscript>
<?php $view['slots']->stop(); ?>
