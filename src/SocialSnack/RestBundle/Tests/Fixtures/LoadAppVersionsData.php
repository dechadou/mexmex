<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\RestBundle\Entity\AppVersion;

class LoadAppVersionsData implements FixtureInterface {

  public function load(ObjectManager $manager) {
    $versions = [
      '1.0.' . rand(0, 999),
      '1.1.' . rand(0, 999),
      '1.2.' . rand(0, 999),
      '2.0',
    ];

    foreach ($versions as $v) {
      foreach (['android', 'ios'] as $device) {
        $version = new AppVersion();
        $version
            ->setDevice($device)
            ->setVersion(substr($device, 0, 1) . $v)
        ;
        $manager->persist($version);
      }
    }

    $manager->flush();
  }
} 