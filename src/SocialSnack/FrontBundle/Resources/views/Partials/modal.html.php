<?php
$request = $this->container->get('request');
$routeName = isset($modal_params['route']) ? $modal_params['route'] : '';
$isAllowed = false;
$allowedModals = [];

if ($modals != false && !empty($modals)) {
  foreach($modals as $modal) {
    $modal['section'] = unserialize($modal['section']);
    $modal['dias'] = unserialize($modal['dias']);
    $sections = $modal['section'];

    if (in_array($routeName, $sections)) {
      $isAllowed = true;
      $allowedModals[] = $modal;
    }

    if (isset($modal['target']) && $routeName == 'cinema_single') {
      $isAllowed = true;
      $allowedModals[] = $modal;
    }

    if ($routeName == 'cinemas_archive' && in_array('cinemas_map',$sections) ) {
      $isAllowed = true;
      $allowedModals[] = $modal;
    }
  }
}

if ($isAllowed){
  ?>
  <style>
    .modalScale {
      background:  #FFF;
      padding: 20px 20px;
      text-align: left;
      display: block;
      /*max-width: px;
      height:  px;*/
      margin: 40px auto;
      position: relative;
      border: none;
    }
  </style>



  <style>
    .white-popup{
      margin:0 auto;
      width:600px;
    }
    .white-popup img{
      max-width: 90vw !important;
      max-height:90vh !important;
    }
  </style>

  <script>
    var currentModals = <?php echo json_encode($allowedModals) ?>;
    var displayedModal = currentModals[Math.floor(Math.random()*currentModals.length)];

    var mId       = displayedModal['id'];
    var mTitle    = displayedModal['title'];
    var mType     = displayedModal['type'];
    var mFile     = '<?php echo $this->container->getParameter('cms_upload_url'); ?>'+displayedModal['file'];
    var mWidth    = displayedModal['swf_width'];
    var mHeight   = displayedModal['swf_height'];
    var mUrl      = displayedModal['url'];
    var mDelay    = displayedModal['delay'];
    var mOnlyOnce = displayedModal['only_once'];
    var mPosition = displayedModal['position'];
    var mDias     = displayedModal['dias'];

    var d = new Date();
    var n = d.getDay();

    $(window).load(function() {
      if (!displayedModal) {
        return;
      }
      
      if (mDias.length != 0 && $.inArray(n,mDias) == -1) {
        return;
      }

      if (localStorage.getItem("modalAlreadyDisplayed"+mId) !== null) {
        return;
      }

      if (!swfobject.hasFlashPlayerVersion('9') && mType === 'swf') {
        return;
      }

      if (mType == 'swf') {
        window.location.hash='no-location';
        $('#popups').hide();
        ga('send', 'event', 'Modal - ' + mTitle, 'Opened', 'page: <?php echo $routeName ?>');
        $.magnificPopup.open({
          items: {
            src: '<div style="width:' + mWidth + 'px; margin:0 auto;"><div id="swfModal"></div></div>',
            type: 'inline'
          },

          callbacks: {
            open: function () {
              $('.mfp-bg').remove();
              swfobject.createSWF({data: mFile, width: mWidth, height: mHeight}, {wmode:"transparent",allowScriptAccess: 'always'}, 'swfModal');
            }
          }

        },0);
      }

      if (mType == 'image') {
        if (mUrl != '' && mUrl != null) {
          var source = "<div class='white-popup'><a href='" + mUrl + "' class='modal_redirect'><img src='" + mFile + "'></a></div>";
        } else {
          var source = "<div class='white-popup'><img src='" + mFile + "'></div>"
        }

        ga('send', 'event', 'Modal - ' + mTitle, 'Opened', 'page: <?php echo $routeName ?>');
        $.magnificPopup.open({
          items: {
            src: source,
            type: 'inline'
          },
          callbacks: {
            close: function () {
              ga('send', 'event', 'Modal - ' + mTitle, 'Closed by User', 'page: <?php echo $routeName ?>');
            }
          }
        }, 0);
      }

      setTimeout(function () {
        $.magnificPopup.close()
        ga('send', 'event', 'Modal - ' + mTitle, 'AutoClose', 'page: <?php echo $routeName ?>');
      }, mDelay * 1000);


      $('.modal_redirect').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        ga('send', 'event', 'Modal - ' + mTitle, 'Clicked', 'page: <?php echo $routeName ?>');
        window.open(href, '_blank')
      });

      if (mOnlyOnce == 1) {
        localStorage.setItem('modalAlreadyDisplayed'+mId, mId);
      }
    });

    function swfCloseModal() {
      $.magnificPopup.close();
      ga('send', 'event', 'Modal - ' + mTitle, 'AutoClose', 'page: <?php echo $routeName ?>');
    }

  </script>
<?php } ?>