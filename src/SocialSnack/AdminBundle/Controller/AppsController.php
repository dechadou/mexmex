<?php

namespace SocialSnack\AdminBundle\Controller;

use Buzz\Exception\RequestException;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use SocialSnack\FrontBundle\Entity\Notification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\AppVersionType;
use SocialSnack\RestBundle\Entity\AppVersion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AppsController extends BaseController {


  protected function get_required_role() {
    return 'ROLE_APPS';
  }


  /**
   * @Route("/appversions", name="admin_appversions_list")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function appVersionsAction(Request $request) {
    $this->check_roles();

    $versions = $this->getDoctrine()->getRepository('SocialSnackRestBundle:AppVersion')->findAll();

    return $this->render('SocialSnackAdminBundle:Apps:appVersions.html.php', array(
        'versions'   => $versions,
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
        'purge'      => $request->getSession()->getFlashBag()->get('purge'),
    ));
  }


  /**
   * @Route("/appversions/add", name="admin_appversions_add")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function appVersionsAddAction(Request $request) {
    $this->check_roles();

    return $this->_appVersionsAddEditAction(new AppVersion(), $request);
  }


  /**
   * @Route("/appversions/{id}", name="admin_appversions_edit")
   * @param Request    $request
   * @param AppVersion $version
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function appVersionsEditAction(Request $request, AppVersion $version) {
    $this->check_roles();

    return $this->_appVersionsAddEditAction($version, $request);
  }


  /**
   * @Route("/appversions/{id}/remove", name="admin_appversions_remove")
   * @param Request    $request
   * @param AppVersion $version
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function appVersionsRemoveAction(Request $request, AppVersion $version) {
    $this->check_roles();

    $em = $this->getDoctrine()->getManager();
    $em->remove($version);
    $em->flush();

    $cache_purger = $this->get('ss.cache_purger');
    $purge = $cache_purger->purge_cache(array('entity' => $version));
    $request->getSession()->getFlashBag()->set('purge', $purge);
    $request->getSession()->getFlashBag()->set('submit_msg', 'La versión ha sido eliminada.');
    return $this->redirect($this->generateUrl('admin_appversions_list'));
  }


  protected function _appVersionsAddEditAction(AppVersion $version = NULL, Request $request) {
    $form = $this->createForm(new AppVersionType(), $version);
    $form->add('submit', 'submit', array(
        'label' => 'Guardar',
    ));
    $form->handleRequest($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($version);
      $em->flush();

      $cache_purger = $this->get('ss.cache_purger');
      $purge = $cache_purger->purge_cache(array('entity' => $version));
      $request->getSession()->getFlashBag()->set('purge', $purge);
      $request->getSession()->getFlashBag()->set('submit_msg', 'Los datos fueron guardados.');
      return $this->redirect($this->generateUrl('admin_appversions_list'));
    }

    return $this->render('SocialSnackAdminBundle:Apps:appVersionsEdit.html.php', array(
        'version' => $version,
        'form'    => $form->createView(),
    ));
  }


  /**
   * @Route("/apps/push/send", name="admin_apps_push_send")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function pushSendAction(Request $request) {
    $this->check_roles();

    $form = $this->createFormBuilder();
    $form->add('gcmid', 'text', [
        'label' => 'GCMID',
    ]);
    $form->add('device', 'choice', [
        'label' => 'Dispositivo',
        'choices' => ['ios' => 'iOS', 'android' => 'Android']
    ]);
    $form->add('title', 'text', [
        'label' => 'Titulo (Android)',
        'required' => FALSE,
    ]);
    $form->add('message', 'textarea', [
        'label' => 'Mensaje'
    ]);
    $form->add('deep_link', 'text', [
        'label' => 'Deeplink',
    ]);
    $form->add('submit', 'submit', [
        'label' => 'Enviar'
    ]);

    $form = $form->getForm();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $form->getData();
      $msg_data = ['deep_link' => $data['deep_link']];
      if ('ios' === $data['device']) {
        $message = new iOSMessage();
        $message->setAPSBadge(1);
        $message->setAPSSound('default');
      } else {
        $message = new AndroidMessage();
        $message->setGCM(TRUE);
        $msg_data['title'] = $data['title'];
      }

      $message->setData($msg_data);
      $message->setMessage($data['message']);
      $message->setDeviceIdentifier($data['gcmid']);

      $sent = $this->container->get('rms_push_notifications')->send($message);

//      var_dump($this->container->get('rms_push_notifications')->getResponses($message->getTargetOS()));

      $request->getSession()->getFlashBag()->set('submit_msg', $sent
          ? 'La notificación se envió exitosamente.'
          : 'Error al enviar la notificación.'
      );

      return new RedirectResponse($this->generateUrl('admin_apps_push_send'));
    }

    return $this->render('SocialSnackAdminBundle:Apps:pushSend.html.php', array(
        'form' => $form->createView(),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/apps/push/bulk", name="admin_apps_push_bulk")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function pushBulkAction(Request $request) {
    $this->check_roles();

    $count = $this->getDoctrine()->getRepository('SocialSnackRestBundle:GCM')->getCount();
    $count = array_reduce($count, function($carry, $item) {
      $carry[$item['device']] = $item['cnt'];
      $carry['total'] += $item['cnt'];
      return $carry;
    }, ['total' => 0]);

    $devices = array_keys(array_slice($count, 1));

    $form = $this->createFormBuilder();
    $form->add('device', 'choice', [
        'label' => 'Dispositivos',
        'choices' => array_combine($devices, $devices),
        'multiple' => TRUE,
        'expanded' => TRUE,
    ]);
    $form->add('title', 'text', [
        'label' => 'Titulo (Android)',
        'required' => FALSE,
    ]);
    $form->add('message', 'textarea', [
        'label' => 'Mensaje',
    ]);
    $form->add('deep_link', 'text', [
        'label' => 'Deeplink',
    ]);
    $form->add('since_id', 'number', [
        'label' => 'Desde ID',
        'data'  => 1,
    ]);
    $form->add('_pages_limit', 'number', [
        'label' => 'Batches',
        'data'  => 0,
    ]);
    $form->add('batch_size', 'number', [
        'label' => 'Disp x batch',
        'data'  => 100,
    ]);
    $form->add('_save', 'checkbox', [
        'label' => 'Guardar',
        'required' => FALSE,
    ]);
    $form->add('submit', 'submit', [
        'label' => 'Enviar',
    ]);

    $form = $form->getForm();

    return $this->render('SocialSnackAdminBundle:Apps:pushBulk.html.php', array(
        'form' => $form->createView(),
        'count' => $count,
    ));
  }


  /**
   * @Route("/apps/push/bulk/process", name="admin_apps_push_bulk_process")
   * @param Request $request
   * @return JsonResponse
   */
  public function pushBulkProcessAction(Request $request) {
    $this->check_roles();

    $sent        = 0;
    $failed      = 0;
    $failed_ids  = [];
    $skipped_ids = [];
    $done_ids    = [];
    $form        = $request->request->get('form');
    $devices     = $form['device'];
    $message     = $form['message'];
    $title       = $form['title'] ?: 'Cinemex';
    $deep_link   = $form['deep_link'];
    $limit       = $form['batch_size'];
    $sinceId     = $form['since_id'];
    $save        = $form['_save'];
    $sentGcm     = $this->get('memcached')->get('sentGcm');

    if (!$sentGcm) {
      $sentGcm = [];
    }

    /** @var \RMS\PushNotificationsBundle\Service\Notifications $notifications */
    $notifications = $this->container->get('rms_push_notifications');

    /** @var \SocialSnack\RestBundle\Entity\GCMRepository $repo */
    $repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:GCM');

    $ids = $repo->findByDeviceSince(
        $devices,
        $sinceId,
        $limit
    );

    // Store message in DB.
    if ($save) {
      $em = $this->getDoctrine()->getManager();
      $time = new \DateTime('now');
      $notification = new Notification();
      $notification->setDate($time);
      $notification->setTitle($title);
      $notification->setContent($message);
      $notification->setDeepLink('');
      $em->persist($notification);
      $em->flush();
    }

    if (preg_grep('/android/i', $devices)) {
      $android_msg = new AndroidMessage();
      $android_msg->setGCM(TRUE);
      $android_msg->setGCMOptions(['time_to_live' => 24 * 60 * 60]);
      $android_msg->setData([
          'title' => $title,
          'deep_link' => $deep_link,
      ]);
      $android_msg->setMessage($message);
      $android_entries = [];
    }

    if (preg_grep('/iphone|ipod|ipad/i', $devices)) {
      $ios_msg = new iOSMessage();
      $ios_msg->setMessage($message);
      $ios_msg->setAPSBadge(1);
      $ios_msg->setAPSSound('default');
      $ios_msg->addCustomData('deep_link', $deep_link);
      $ios_entries = [];
    }

    $send_android = FALSE;
    $send_ios     = FALSE;

    foreach ($ids as $entry) {
      /** @var \SocialSnack\RestBundle\Entity\GCM $entry */

      if (in_array($entry->getGcmid(), $sentGcm)) {
        $skipped_ids[] = $entry->getId();
        continue;
      }

      $sentGcm[] = $entry->getGcmid();

      if (preg_match('/android/i', $entry->getDevice())) {
        $android_entries[] = $entry;
        $android_msg->addGCMIdentifier($entry->getGcmid());
        $send_android = TRUE;
      } elseif (preg_match('/iphone|ipod|ipad/i', $entry->getDevice())) {
        $ios_entries[] = $entry;
        $ios_msg->addGCMIdentifier($entry->getGcmid());
        $send_ios = TRUE;
      }
    }

    if ($send_ios && isset($ios_msg)) {
      try {
        $this->container->get('rms_push_notifications')->send($ios_msg);
      } catch (RequestException $e) {}

      $response = $notifications->getResponses($ios_msg->getTargetOS());
      foreach ($ios_entries as $i => $entry) {
        if (isset($response[$i])) {
          $failed++;
          $failed_ids[] = $entry->getId();
        } else {
          $sent++;
          $done_ids[] = $entry->getId();
        }
      }
    }

    if ($send_android && isset($android_msg)) {
      try {
        $notifications->send($android_msg);
      } catch (RequestException $e) {}

      if ($response = $notifications->getResponses($android_msg->getTargetOS())) {
        $android_result = @json_decode($response[0]->getContent());
      }
      if (isset($android_result, $android_result->results)) {
        foreach ($android_result->results as $i => $result) {
          if (isset($result->message_id)) {
            $sent++;
            $done_ids[] = $android_entries[$i]->getId();
          } else {
            $failed++;
            $failed_ids[] = $android_entries[$i]->getId();
          }
        }
      }
    }

    $this->get('memcached')->set('sentGcm', $sentGcm, 60 * 60);

    return new JsonResponse([
        'count'       => count($ids),
        'limit'       => $limit,
        'sent'        => $sent,
        'failed'      => $failed,
        'failed_ids'  => $failed_ids,
        'skipped_ids' => $skipped_ids,
        'done_ids'    => $done_ids,
        'last_id'     => count($ids) ? end($ids)->getId() : $sinceId,
    ]);
  }


  /**
   * @Route("/generateToken", name="admin_apps_generateToken")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Exception
   */
  public function generateTokenAction(Request $request) {
    $token = '';
    $user = NULL;

    $form = $this->createFormBuilder();
    $form->add('user_id', 'text', ['label' => 'ID']);
    $form->add('email', 'text', ['label' => 'Email']);
    $form->add('submit', 'submit', ['label' => 'Generar token']);

    $form = $form->getForm();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $request->request->get('form');
      $user_id = $data['user_id'];
      $email = $data['email'];
      $user = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:User')->find($user_id);
      if (!$user || $user->getEmail() !== $email) {
        throw new \Exception('Invalid user or email.');
      }
      $handler = $this->get('rest.auth.handler');
      $token = $handler->buildToken($user);
    }

    $request->getSession()->getFlashBag()->set('submit_msg', $token);
    
    return $this->render('SocialSnackAdminBundle:Apps:generateToken.html.php', [
        'user' => $user,
        'token' => $token,
        'form' => $form->createView(),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
    ]);
  }

}