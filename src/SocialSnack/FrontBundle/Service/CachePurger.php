<?php

namespace SocialSnack\FrontBundle\Service;

use SocialSnack\FrontBundle\Service\CachePurger\CachePurgerInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\Entity\EntityTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeAllAreasHomeBillboard;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeAllStatesCinemasMap;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeAreaHomeBillboard;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeCinemaPage;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeCinemaSessions;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeCinemaTickets;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeDatesByArea;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeDoctrineResult;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeHomepage;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeStateCinemasMap;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeUrl;

class CachePurger {

  protected $container;
  protected $tasks   = array();
  protected $results = array();

  protected $purgers = [];

  const ENV_FRONT  = 'front';
  const ENV_STATIC = 'static';
  const ENV_MOBILE = 'mobile';

  const PURGE_AREA_BILLBOARD       = 'purge_area_billboard';
  const PURGE_ALL_AREAS_BILLBOARDS = 'purge_areas_billboards';
  const PURGE_CONCEPT_MAP          = 'purge_concept_map';
  const PURGE_STATE_MAP            = 'purge_state_map';
  const PURGE_ALL_STATES_MAPS      = 'purge_states_maps';
  const PURGE_CINEMA_SINGLE        = 'purge_cinema_single';
  const PURGE_ALL_CINEMAS_SINGLES  = 'purge_cinemas_singles';
  const PURGE_HOME_BILLBOARD       = 'purge_home_billboard';
  const PURGE_CINEMA_TICKETS       = 'purge_cinema_tickets';
  const PURGE_CINEMA_SESSIONS      = 'purge_cinema_sessions';
  const PURGE_DOCTRINE             = 'purge_doctrine';
  const PURGE_DOCTRINE_ALL         = 'purge_doctrine_all';
  const PURGE_AVAILABLE_DATES      = 'purge_available_dates';
//  const PURGE_FRONT_ALL            = 'purge_front_all';


  public function __construct($container) {
    $this->container = $container;
  }


  public function addPurger(CachePurgerInterface $purger) {
    $this->purgers[] = $purger;
  }


  public function addPurgers(array $purgers) {
    foreach ($purgers as $purger) {
      $this->addPurger($purger);
    }
  }


  /**
   * Returns all the available purger services.
   *
   * @return array
   */
  protected function getPurgers() {
    return $this->purgers;
  }


  /**
   * Enqueue a cache purge task.
   *
   * @param array|CachePurgeTaskInterface $task
   * @return \SocialSnack\FrontBundle\Service\CachePurger
   */
  public function enqueue($task) {
    $this->tasks[] = $task;
    return $this;
  }


  /**
   * Process queued tasks.
   *
   * @param array|CachePurgeTaskInterface $task Optional.
   * @return array Array with the output for each task.
   */
  public function purge_cache($task = NULL) {
    $res = array();

    if ( $task ) {
      $this->enqueue($task);
    }

    foreach ($this->tasks as $i => $task) {
      $res = array_merge($res, $this->process_task($task));
      unset($this->tasks[$i]);
    }

    return $res;
  }


  /**
   * This is a new method part of the refactoring of cache purge service (WIP).
   *
   * @param CachePurgeTaskInterface $task
   * @return array
   */
  protected function processTask(CachePurgeTaskInterface $task) {
    $output = [];

    foreach ($this->getPurgers() as $purger) {
      if (!$purger->canHandle($task)) {
        continue;
      }

      $output = array_merge(
          $output,
          $purger->handle($task)
      );
    }

    return $output;
  }

  /**
   * Determine the corresponding method to proccess the task and call the method.
   * That method would determine the resources related to the task and purge
   * their caches.
   *
   * @param array $args
   * @return array Array with the output for the task/subtasks (if any).
   */
  protected function process_task($args) {
    if ($args instanceof CachePurgeTaskInterface) {
      return $this->processTask($args);
    }

    extract($args);

    if ( isset($entity) ) {
      return $this->process_entity($args);
    }

    if ( isset($type) ) {
      if ( 'assets' == $type ) {
        return $this->process_assets($args);
      }

      if ( 'urls' == $type ) {
        return $this->process_urls($args);
      }

      if ( 'method' == $type ) {
        return $this->process_method($args);
      }

      if ( 'doctrine' == $type ) {
        return $this->process_doctrine($args);
      }
    }
  }


  /**
   * The task is related to an entity.
   * Determine resources based on that entity and purge the cache for those
   * resources.
   *
   * @param array $args
   * @return array
   */
  protected function process_entity($args) {
    $task   = NULL;
    $entity = $args['entity'];

    $entityClass = explode('\\', get_class($entity));
    $entityClass = array_pop($entityClass);
    try {
      $task = $this->createEntityTask($entityClass);
    } catch (\InvalidArgumentException $e) {
      return [sprintf('No tasks available for entity %s', $entityClass)];
    }

    $task->setEntity($entity);

    return $this->processTask($task);
  }


  /**
   * @param string $entityId
   * @return EntityTaskInterface
   */
  protected function createEntityTask($entityId) {
    $class = 'SocialSnack\FrontBundle\Service\CachePurgeTask\Entity\PurgeEntity' . ucfirst($entityId);

    if (!class_exists($class)) {
      throw new \InvalidArgumentException(sprintf('Class %s does not exist.', $class));
    }

    return new $class(
        $this->container->get('router'),
        $this->container->get('ss.front.utils')
    );
  }


  protected function createConceptMapTask($concept) {
    $class = 'SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap\Purge' . ucfirst($concept) . 'Map';

    if (!class_exists($class)) {
      throw new \InvalidArgumentException(sprintf('Class %s does not exist.', $class));
    }

    return new $class(
        $this->container->get('router'),
        $this->container->get('doctrine')
    );
  }


  /**
   * @return PurgeUrl
   */
  protected function createUrlTask() {
    return new PurgeUrl();
  }


  /**
   * @return PurgeDoctrineResult
   */
  protected function createDoctrineResultTask() {
    return new PurgeDoctrineResult();
  }


  /**
   * Purge assets URLs.
   *
   * @param array $args
   * @return array
   */
  protected function process_assets($args) {
    $urls  = array();
    $files = $args['files'];
    $task  = $this->createUrlTask();

    foreach ($files as $file) {
      $task->addUrlResource($file, self::ENV_STATIC);
    }

    return $this->processTask($task);
  }


  /**
   * Purge URLs.
   *
   * @param array $args
   * @return array
   */
  protected function process_urls($args) {
    $urls = $args['urls'];
    $task = $this->createUrlTask();

    if (!is_array($urls)) {
      $urls = array($urls);
    }

    foreach ($urls as $url) {
      if (is_string($url)) {
        $url = array('path' => $url);
      }
      if ('/' !== substr($url['path'], 0, 1)) {
        $url['path'] = '/' . $url['path'];
      }
      $task->addUrlResource(
          $url['path'],
          isset($url['env']) ? $url['env'] : self::ENV_FRONT
      );
    }

    return $this->processTask($task);
  }


  /**
   * Call a method with more specific rules to perform the purge.
   *
   * @param array $args
   * @return array
   */
  protected function process_method($args) {
    extract($args);

    if ( !isset($method) || strpos($method, 'purge') !== 0 || !method_exists($this, '_' . $method) ) {
      return array('Method not valid: ' . $method);
    }

    return $this->{'_' . $method}($args);
  }


  protected function _purge_available_dates($args) {
    $task = new PurgeDatesByArea($this->container->get('doctrine'));

    return $this->processTask($task);
  }


  protected function _purge_cinema_tickets($args) {
    $doctrine = $this->container->get('doctrine');
    $cinema = $doctrine->getRepository('SocialSnackWsBundle:Cinema')->find($args['cinema_id']);

    $task = new PurgeCinemaTickets($doctrine);
    $task->setCinema($cinema);

    return $this->processTask($task);
  }


  protected function _purge_cinema_sessions($args) {
    $doctrine = $this->container->get('doctrine');
    $cinema = $doctrine->getRepository('SocialSnackWsBundle:Cinema')->find($args['cinema_id']);

    $task = new PurgeCinemaSessions($doctrine);
    $task->setCinema($cinema);

    return $this->processTask($task);
  }


  protected function _purge_concept_map($args) {
    $attribute = $args['attribute'];

    $task = $this->createConceptMapTask($attribute);

    return $this->processTask($task);
  }


  /**
   * Purge the map for a single state.
   *
   * @param array $args array('state_id' => $state_id)
   * @return array
   */
  protected function _purge_state_map($args) {
    $task = new PurgeStateCinemasMap(
        $this->container->get('router')
    );
    $task->setStateId($args['state_id']);

    return $this->processTask($task);
  }


  /**
   * Purge the maps for all states.
   *
   * @return array
   */
  protected function _purge_states_maps() {
    $task = new PurgeAllStatesCinemasMap(
        $this->container->get('router'),
        $this->container->get('doctrine')
    );

    return $this->processTask($task);
  }


  /**
   * Purge the billboard for a single area.
   *
   * @param array $args array('area_id' => $area_id)
   * @return array
   */
  protected function _purge_area_billboard($args) {
    $task = new PurgeAreaHomeBillboard(
        $this->container->get('router')
    );
    $task->setAreaId($args['area_id']);

    return $this->processTask($task);
  }


  /**
   * Purge the billboard for all areas.
   *
   * @return array
   */
  protected function _purge_areas_billboards() {
    $task = new PurgeAllAreasHomeBillboard(
        $this->container->get('router'),
        $this->container->get('doctrine')
    );

    return $this->processTask($task);
  }


  /**
   * Purge the details page for a cinema.
   *
   * @param array $args array('cinema_id' => $cinema_id)
   * @return array
   */
  protected function _purge_cinema_single($args) {
    $cinema_id   = $args['cinema_id'];
    $doctrine    = $this->container->get('doctrine');
    $cinema_repo = $doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $cinema      = $cinema_repo->find($cinema_id);

    $task = new PurgeCinemaPage(
        $this->container->get('router'),
        $this->container->get('ss.front.utils')
    );
    $task->setCinema($cinema);

    return array_merge(
        $this->process_entity(['entity' => $cinema]),
        $this->processTask($task)
    );
  }


  /**
   * Purge the details pages for all cinemas.
   *
   * @return array
   */
  protected function _purge_cinemas_singles() {
    $output      = array();
    $doctrine    = $this->container->get('doctrine');
    $cinema_repo = $doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas     = $cinema_repo->findAll();

    $task = new PurgeCinemaPage(
        $this->container->get('router'),
        $this->container->get('ss.front.utils')
    );

    foreach ($cinemas as $cinema) {
      $task->setCinema($cinema);
      $output = array_merge(
          $output,
          $this->processTask($task)
      );
    }

    return $output;
  }


  /**
   * Purge the home billboard cache.
   *
   * @return array
   */
  protected function _purge_home_billboard() {
    $task = new PurgeHomepage(
        $this->container->get('router')
    );

    return $this->processTask($task);
  }


  /**
   * Purge Doctrine's result cache for a cache ID.
   * $args should countain 'cache_ids' => array(). Otherwise the whole cache will be purged.
   *
   * @param array $args
   * @return array
   */
  protected function process_doctrine($args) {
    $task = $this->createDoctrineResultTask();

    extract($args);

    if (!isset($cache_ids)) {
      $cache_ids = [];
    }

    if (isset($cache_id)) {
      $cache_ids[] = $cache_id;
    }

    $task->addDoctrineResource($cache_ids, self::ENV_FRONT);

    return $this->processTask($task);
  }


  /**
   * Purge the whole Doctrine's result cache.
   *
   * @param array $args
   * @return array
   */
  protected function _purge_doctrine_all($args) {
    $em = $this->container->get('doctrine')->getManager();
    $success = $this->container->get('doctrine')->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
    $res = 'Doctrine Result Cache (all): ' . ($success ? 'success' : 'failure');

    return array($res);
  }


  /**
   * Purge Doctrine's result cache for a cache ID.
   * $args must have 'cache_id' => 'cache-id'.
   *
   * @param array $args
   * @return array
   */
  protected function _purge_doctrine($args) {
    extract($args);

    $res = 'Doctrine Result Cache ID ' . $cache_id . ': ';
    $em = $this->container->get('doctrine')->getManager();
    if ($em->getConfiguration()->getResultCacheImpl()->delete($cache_id)) {
      $res .= 'success.';
      $em->clear();
    } else {
      $res .= 'failure (or empty).';
    }

    return array($res);
  }


  /**
   * Purge Symfony cache for an URL with a host name.
   *
   * @param string $url
   * @param string $host
   * @return string
   */
  protected function __purge_cache_symfony($url, $host, $headers = array(), $method = 'PURGEX') {
    $ch = curl_init($url);
    $headers[] = "X-Host: $host";
    $headers[] = "Host: $host";
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $_res = curl_exec($ch);
    if (curl_errno($ch)) {
      $_res = curl_error($ch);
    }
    curl_close($ch);

    return $_res;
  }


  /**
   * Purge Varnish cache for an URL with a host name.
   *
   * @param string $url
   * @param string $host
   * @return string
   */
  protected function __purge_cache_varnish($url, $host) {
    $url .= '.*';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'BAN');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $host"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $_res = curl_exec($ch);
    if (curl_errno($ch)) {
      $_res = curl_error($ch);
    } else {
      if ( preg_match('/<h1>(?<title>.+)<\/h1>/', $_res, $matches) ) {
        $_res = $matches['title'];
      }
    }
    curl_close($ch);

    return $_res;
  }


  protected function _purge_front_all() {
    $res      = array();
    $prefix   = '/';
    $em       = $this->container->get('doctrine')->getManager();
    $f_caches = $this->container->getParameter('hosts_front_caches');
    $ips      = $this->container->getParameter('hosts_front_pub_ip');
    $host     = $this->container->getParameter('hosts_front_domain');
    $symfony  = in_array('symfony', $f_caches);
    $varnish  = in_array('varnish', $f_caches);

    $res = array_merge($res, $this->process_doctrine([]));

    // Purge caches on each server.
    foreach ($ips as $ip) {
      // Symphony cache
      if ( TRUE === $symfony ) {
        $url   = "http://{$ip}{$prefix}";
        $_res  = $this->__purge_cache_symfony(
            $url,
            $host,
            array(
                'User-Agent: ' . $this->container->getParameter('remote_cache_purge')['user_agent']
            ),
            'PURGE-ALL'
        );
        $res[] = $url . ":\r\n" . $_res;
      }

      // Varnish cache
      if ( TRUE === $varnish ) {
        $url   = "http://{$ip}{$prefix}.*";
        $_res  = $this->__purge_cache_varnish($url, $host);
        $res[] = $url . ":\r\n" . $_res;
      }
    }

    return $res;
  }

}