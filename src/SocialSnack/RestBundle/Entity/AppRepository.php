<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class AppRepository extends EntityRepository {


  public function findByAppId($app_id) {
    $qb = $this->createQueryBuilder('q');
    $qb
        ->select('q')
        ->andWhere('q.appId = :app_id')
        ->setParameter('app_id', $app_id)
    ;

    $q = $qb->getQuery();
    $q->useQueryCache(TRUE);
    $q->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . $app_id);

    try {
      return $q->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
  }


  public function findByCap($cap) {
    $qb = $this->createQueryBuilder('q');
    $qb
        ->select('q')
        ->andWhere($qb->expr()->like('q.caps', ':cap'))
        ->setParameter('cap', '%"' . $cap . '"%')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }


  public function findPublic() {
    $qb = $this->createQueryBuilder('q');
    $qb
        ->select('q')
        ->andWhere('q.public = 1')
        ->andWhere('q.active = 1')
    ;
    $q = $qb->getQuery();
    $q->useQueryCache(TRUE);
    $q->useResultCache(TRUE, 60 * 10, __METHOD__);
    return $q->getResult();
  }

} 