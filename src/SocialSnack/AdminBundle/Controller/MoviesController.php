<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Form\Type\TrackingCodeType;
use SocialSnack\RestBundle\Entity\TrackingCode;
use SocialSnack\WsBundle\Entity\Movie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

/**
 * @Route("/movies")
 */
class MoviesController extends BaseController {

  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="social_snack_movies_list")
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction() {
    $this->check_roles();
    
    $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
    $qb = $movie_repo->createQueryBuilder('m');
    $qb->select('m')
        ->andWhere('m.active = 1')
        ->andWhere($qb->expr()->isNull('m.parent'))
        ->orderBy('m.id', 'DESC')
        ->addOrderBy('m.name', 'ASC');
    $query = $qb->getQuery();
    $movies = $query->getResult();
    return $this->render('SocialSnackAdminBundle:Movies:list.html.php', array('movies' => $movies));
  }


  /**
   * @Route("/approve", name="social_snack_movies_approve")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function approveAction(Request $request) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');

    $movies = $movie_repo->findInactive();
    $formb = $this->createFormBuilder();
    foreach ($movies as $movie) {
      $formb->add('aprobar_' . $movie->getId(), 'checkbox', array(
          'required' => false
      ));
    }

    $form = $formb->getForm();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $form->getData();

      foreach ($movies as $movie) {
        if ($data['aprobar_' . $movie->getId()]) {
          $movie->setActive(1);
          $em->persist($movie);
          $em->flush();
        }
      }

      return $this->redirect($this->generateUrl('social_snack_movies_approve'));
    }
    return $this->render('SocialSnackAdminBundle:Movies:approveList.html.php', array('movies' => $movies, 'form' => $form->createView()));
  }


  /**
   * @Route("/{movie_id}", name="social_snack_admin_movie")
   * @ParamConverter("movie", options={"id" = "movie_id"})
   * @param Movie $movie
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function editAction(Movie $movie, Request $request) {
    $this->check_roles();
    
    $purge = NULL;
    $em = $this->get('doctrine')->getManager();

    $attributes = FrontHelper::get_attributes_collection();
    
    // Groupers get version attributes from their children, so if we are editing
    // a grouper, let's just display the NON-version attributes here.
    if ('grouper' === $movie->getType()) {
      $movie_attributes = array_filter($attributes, function($a) { return $a['version'] === FALSE; });
    } else {
      $movie_attributes = $attributes;
    }
    $movie_attributes = array_map(function($a) { return $a['display_name']; }, $movie_attributes);
    $defaults = array();
    foreach ($movie->getAttr() as $key => $value) {
      if ($value == 1) {
        $defaults[] = $key;
      }
    }

    $form_builder = $this->createFormBuilder()
        ->add('movie_name', 'text', array(
            'data' => (is_string($movie->getName())) ? $movie->getName() : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('group_id', 'text', array(
            'data' => (is_string($movie->getGroupId())) ? $movie->getGroupId() : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('fixed_position', 'text', array(
            'data' => $movie->getFixedPosition(),
            'required' => false,
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('active', 'checkbox', array(
            'required' => false,
            'data' => $movie->getActive(),
        ))
        ->add('featured', 'checkbox', array(
            'required' => false,
            'data' => $movie->getFeatured(),
        ))
        ->add('ESTRENO', 'checkbox', array(
            'required' => false,
            'data' => (is_string($movie->getData('ESTRENO')) && $movie->getData('ESTRENO') == "1") ? true : false
        ))
        ->add('attribute', 'choice', array(
            'choices' => $movie_attributes,
            'multiple' => true,
            'data' => $defaults,
            'expanded' => true
        ))
        ->add('NOMBREORIGINAL', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('NOMBREORIGINAL'))) ? $movie->getData('NOMBREORIGINAL') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('DIRECTOR', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('DIRECTOR'))) ? $movie->getData('DIRECTOR') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('ACTORES', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('ACTORES'))) ? $movie->getData('ACTORES') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('GENERO', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('GENERO'))) ? $movie->getData('GENERO') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('PAISORIGEN', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('PAISORIGEN'))) ? $movie->getData('PAISORIGEN') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('ANIO', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('ANIO'))) ? $movie->getData('ANIO') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('SINOPSIS', 'textarea', array(
            'required' => false,
            'data' => (is_string($movie->getData('SINOPSIS'))) ? $movie->getData('SINOPSIS') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('RATING', 'text', array(
            'data' => (is_string($movie->getData('RATING'))) ? $movie->getData('RATING') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('DURACION', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('DURACION'))) ? $movie->getData('DURACION') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('TRAILER', 'text', array(
            'required' => false,
            'data' => (is_string($movie->getData('TRAILER'))) ? $movie->getData('TRAILER') : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('poster', 'file');
    
    if ('grouper' === $movie->getType()) {
      $children_attributes = array_filter($attributes, function($a) { return $a['version'] === TRUE; });
      $children_attributes = array_map(function($a) { return $a['display_name']; }, $children_attributes);
      
      foreach ($movie->getChildren() as $child) {
        $defaults = array();
        foreach ($child->getAttr() as $key => $value) {
          if ($value == 1) {
            $defaults[] = $key;
          }
        }
        $form_builder->add('attribute_' . $child->getId(), 'choice', array(
            'choices' => $children_attributes,
            'multiple' => true,
            'data' => $defaults,
            'expanded' => true
        ));
      }
    }
    
    $form_builder->add('save', 'submit');
    $form = $form_builder->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $cache_purger = $this->get('ss.cache_purger');

      if (!$_FILES["movie_poster"]["error"] &&
              (($_FILES["movie_poster"]["type"] == "image/gif") || ($_FILES["movie_poster"]["type"] == "image/jpeg") || ($_FILES["movie_poster"]["type"] == "image/jpg") || ($_FILES["movie_poster"]["type"] == "image/pjpeg") || ($_FILES["movie_poster"]["type"] == "image/x-png") || ($_FILES["movie_poster"]["type"] == "image/png"))) {

        $bytes = file_get_contents($_FILES["movie_poster"]["tmp_name"]);
        $poster = $this->container->get('ss.front.utils')->name_movie_poster(15);

        $this->container->get('ss.front.utils')->upload_movie_poster($poster, $bytes);
        $movie->setPosterUrl($poster);
      }

      $data = $form->getData();
      
      $movie->setName((string) $data['movie_name']);
      $movie->setGroupId($data['group_id']);
      $movie->setActive($data['active']);

      if ($data['ESTRENO']) {
        $estreno = 1;
      } else {
        $estreno = 0;
      }
      $movie->setPremiere((bool) $estreno);
      $movie->setFixedPosition((int) $data['fixed_position']);
      $movie->setFeatured($data['featured']);

      $movie_info = json_decode($movie->getInfo());
      $movie_info->NOMBREORIGINAL = $data['NOMBREORIGINAL'];
      $movie_info->DIRECTOR       = $data['DIRECTOR'];
      $movie_info->ACTORES        = $data['ACTORES'];
      $movie_info->GENERO         = $data['GENERO'];
      $movie_info->PAISORIGEN     = $data['PAISORIGEN'];
      $movie_info->ANIO           = $data['ANIO'];
      $movie_info->SINOPSIS       = $data['SINOPSIS'];
      $movie_info->RATING         = $data['RATING'];
      $movie_info->DURACION       = $data['DURACION'];
      $movie_info->TRAILER        = $data['TRAILER'];
      $movie_info->ESTRENO        = (string) $estreno;
      $movie->setInfo(json_encode($movie_info));
      
      
      $save_attribute = array();
      
      // Set attributes for children.
      if ('grouper' === $movie->getType()) {
        foreach ($movie->getChildren() as $child) {
          $key = 'attribute_' . $child->getId();
          if (isset($data[$key]) && is_array($data[$key])) {
            $save_child_attribute = array();
            foreach ($data[$key] as $attribute) {
              $save_child_attribute[$attribute] = TRUE;
            }
            $child->setAttributes(json_encode($save_child_attribute));
            
            // Set children attributes to parent too.
            $save_attribute = array_merge($save_attribute, $save_child_attribute);
          }
          
          $child->setName($movie->getName());
          $child->setPremiere($movie->getPremiere());
          $child->setPosterUrl($movie->getPosterUrl());
          $child->setInfo($movie->getInfo());
        }
      }

      // Set the attributes for the movie.
      if (isset($data['attribute']) && is_array($data['attribute'])) {
        foreach ($data['attribute'] as $attribute) {
          $save_attribute[$attribute] = TRUE;
        }
        $movie->setAttributes(json_encode($save_attribute));
      }
      
      
      $em->persist($movie);
      $em->flush();
      
      $cache_purger->enqueue(array('entity' => $movie));
      
      // If the movie belongs to Casa de Arte, purge that page too.
      if ($movie->getAttr('art')) {
        $cache_purger->enqueue(array(
            'type' => 'urls',
            'urls' => $this->generateUrl('art'),
        ));
      }
      $purge = $cache_purger->purge_cache();
    }

    return $this->render('SocialSnackAdminBundle:Movies:edit.html.php', array(
        'movie' => $movie,
        'form'  => $form->createView(),
        'purge' => $purge,
    ));
  }


  /**
   * @Route("/addToGroupAsk/{movie_id}", name="admin_movies_addToGroupAsk")
   * @ParamConverter("movie", options={"id" = "movie_id"})
   * @param Movie $movie
   * @param Request $request
   * @return Response
   */
  public function addToGroupAskAction(Movie $movie, Request $request) {
    return $this->render('SocialSnackAdminBundle:Movies:addToGroupAsk.html.php', array(
        'movie' => $movie,
        'groupers' => $this->getGroupers(),
    ));
  }


  protected function getGroupers() {
    $qb = $this->getDoctrine()
        ->getRepository('SocialSnackWsBundle:Movie')
        ->createQueryBuilder('m');

    $qb->select('m')
        ->andWhere('m.type = :type')
        ->setParameter('type', 'grouper')
    ;
    $q = $qb->getQuery();

    return $q->getResult();
  }


  protected function getGroups() {
    $qb = $this->getDoctrine()
        ->getRepository('SocialSnackWsBundle:Movie')
        ->createQueryBuilder('m');

    $qb->select('m.name, m.group_id')
        ->andWhere($qb->expr()->isNull('m.parent'))
        ->orderBy('m.id', 'DESC')
    ;
    $q = $qb->getQuery();

    return $q->getResult();
  }


  /**
   * @Route("/createGroupFromMovie/{movie_id}", name="admin_movies_createGroupFromMovie")
   * @ParamConverter("movie", options={"id" = "movie_id"})
   * @param Movie $movie
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function createGroupFromMovieAction(Movie $movie, Request $request) {
    /** @var \SocialSnack\WsBundle\Handler\MovieGroupHandler $handler */
    $handler = $this->get('ws.handler.moviegroup');
    $handler->createGroupFromMovie($movie);

    return $this->redirect($this->generateUrl(
        'social_snack_admin_movie',
        ['movie_id' => $movie->getParent()->getId()]
    ));
  }


  /**
   * @Route("/addMovieToGroup/{movie_id}/to/{group_id}", name="admin_movies_addMovieToGroup")
   * @ParamConverter("movie", options={"id" = "movie_id"})
   * @param Movie $movie
   * @param string $group_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function addMovieToGroupAction(Movie $movie, $group_id, Request $request) {
    /** @var \SocialSnack\WsBundle\Handler\MovieGroupHandler $handler */
    $handler = $this->get('ws.handler.moviegroup');
    $handler->addMovieToGroup($movie, $group_id);

    return $this->redirect($this->generateUrl(
        'social_snack_admin_movie',
        ['movie_id' => $movie->getParent()->getId()]
    ));
  }


  /**
   * @Route("/inactive", name="social_snack_movies_inactive")
   * @return Response
   */
  public function inactiveAction() {
    $this->check_roles();

    $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
    $qb = $movie_repo->createQueryBuilder('m');
    $qb->select('m')
        ->andWhere('m.active = 0')
        ->andWhere($qb->expr()->isNull('m.parent'))
        ->orderBy('m.id', 'DESC')
        ->addOrderBy('m.name', 'ASC');
    $query = $qb->getQuery();
    $movies = $query->getResult();
    return $this->render('SocialSnackAdminBundle:Movies:inactive_list.html.php', array('movies' => $movies));
  }

  /**
   * @Route("/ungroupMovie/{movie_id}", name="admin_movie_ungroupMovie")
   * @ParamConverter("movie", options={"id" = "movie_id"})
   * @param Movie $movie
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function ungroupMovieAction(Movie $movie, Request $request) {
    /** @var \SocialSnack\WsBundle\Handler\MovieGroupHandler $handler */
    $handler = $this->get('ws.handler.moviegroup');
    $handler->ungroupMovie($movie);

    return $this->redirect($this->generateUrl(
        'social_snack_admin_movie',
        ['movie_id' => $movie->getId()]
    ));
  }


  /**
   * @Route("/dissolveGroup/{group_id}", name="admin_movies_dissolveGroup")
   * @param string $group_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function dissolveGroupAction($group_id, Request $request) {
    /** @var \SocialSnack\WsBundle\Handler\MovieGroupHandler $handler */
    $handler = $this->get('ws.handler.moviegroup');
    $handler->dissolveGroup($group_id);

    return $this->redirect($this->generateUrl('social_snack_movies_list'));
  }


  /**
   * @Route("/trackingCodes/", name="admin_movies_trackingCodes")
   * @Route("/trackingCodes/inactive", name="admin_movies_trackingCodes_inactive", defaults={"inactive"=true})
   *
   * @param Request $request
   * @return Response
   */
  public function trackingCodesListAction($inactive = FALSE, Request $request) {
    $this->check_roles();

    $codes = $this->getDoctrine()
        ->getRepository('SocialSnackRestBundle:TrackingCode')
        ->findBy(['active' => !$inactive]);

    return $this->render('SocialSnackAdminBundle:Movies:trackingCodesList.html.php', [
      'codes'    => $codes,
      'inactive' => $inactive,
    ]);
  }


  /**
   * @Route("/trackingCodes/add", name="admin_movies_trackingCodes_add")
   * @Route("/trackingCodes/{id}", name="admin_movies_trackingCodes_edit")
   * @param null|integer $id
   * @param Request      $request
   * @return Response
   */
  public function trackingCodesEditAction($id = NULL, Request $request) {
    $this->check_roles();

    if (is_null($id)) {
      $code = new TrackingCode();
    } else {
      $code = $this->getDoctrine()
        ->getRepository('SocialSnackRestBundle:TrackingCode')
        ->find($id);
    }

    $form = $this->createForm(new TrackingCodeType(), $code);
    $form->add('submit', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($code);
      $em->flush();

      return $this->redirect($this->generateUrl('admin_movies_trackingCodes_edit', [
          'id' => $code->getId()
      ]));
    }

    return $this->render('SocialSnackAdminBundle:Movies:trackingCodesEdit.html.php', [
        'form' => $form->createView(),
        'code' => $code,
    ]);
  }


  /**
   * @Route("/trackingCodes/{id}/delete", name="admin_movies_trackingCodes_delete")
   * @param TrackingCode $code
   * @param Request      $request
   * @return Response
   */
  public function trackingCodesDeleteAction(TrackingCode $code, Request $request) {
    $this->check_roles();

    $handler = $this->get('rest.trackingcode.handler');
    $handler->removeCodeAssociations($code);

    $em = $this->getDoctrine()->getManager();
    $em->remove($code);
    $em->flush();

    return $this->redirect($this->generateUrl('admin_movies_trackingCodes'));
  }


  /**
   * @Route("/trackingCodes/{id}/assign", name="admin_movies_trackingCodes_assign")
   * @param TrackingCode $code
   * @param Request      $request
   * @return Response
   */
  public function trackingCodeAssignAction(TrackingCode $code, Request $request) {
    $handler = $this->get('rest.trackingcode.handler');
    $assigned = $handler->getAssignedGroups($code);

    if ($request->query->has('group_id')) {
      $group_id = $request->query->get('group_id');
      $action   = $request->query->get('action');
      $exists   = in_array($group_id, $assigned);

      // Assign.
      if (!$exists && $action !== 'remove') {
        $handler = $this->get('ws.handler.moviegroup');
        $handler->assignTrackingCode($code, $group_id);
      }

      // Remove.
      if ($action === 'remove' && $exists) {
        $handler = $this->get('ws.handler.moviegroup');
        $handler->removeTrackingCode($code, $group_id);
      }

      return $this->redirect($this->generateUrl('admin_movies_trackingCodes_assign', ['id' => $code->getId()]));
    }

    return $this->render('SocialSnackAdminBundle:Movies:trackingCodesAssign.html.php', [
        'code'     => $code,
        'groups'   => $this->getGroups(),
        'assigned' => $assigned,
    ]);
  }
}