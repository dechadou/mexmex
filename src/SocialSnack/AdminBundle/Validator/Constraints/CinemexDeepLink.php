<?php

namespace SocialSnack\AdminBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class CinemexDeepLink extends Constraint {

  public $message = 'The string "%string%" is not a valid Cinemex deep link.';

}