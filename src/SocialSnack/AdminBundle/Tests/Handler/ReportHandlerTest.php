<?php

namespace SocialSnack\AdminBundle\Tests\Handler;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\AdminBundle\Handler\ReportHandler;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\RestBundle\Entity\App;
use SocialSnack\RestBundle\Tests\Fixtures\LoadPurchasesData;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;

/**
 * Class ReportHandlerTest
 * @package SocialSnack\AdminBundle\Tests\Handler
 * @author Guido Kritz
 */
class ReportHandlerTest extends WebTestCase {

  protected function getHandler($doctrine = NULL) {
    ReportHandler::$apps_cache = NULL;
    return new ReportHandlerTesteable($doctrine ?: $this->getContainer()->get('doctrine'));
  }

  protected function getHandlerMock(array $methods = [], $doctrine = NULL) {
    ReportHandler::$apps_cache = NULL;
    return $this->getMockBuilder('\SocialSnack\AdminBundle\Tests\Handler\ReportHandlerTesteable')
        ->setMethods($methods)
        ->setConstructorArgs([$doctrine ?: $this->getContainer()->get('doctrine')])
        ->getMock();
  }

  protected function getTransactionsByDateTransaction() {
    $m = new Movie();
    $c = new Cinema();
    $t = new Transaction();
    $t
        ->setMovie($m)
        ->setCinema($c)
        ->setTickets(json_encode([['name' => 'Adulto', 'qty' => 2], ['name' => 'Menor', 'qty' => 3]]))
        ->setInfo('{"RESULTADO":"0","TRANSACCION":"49213","BOOKING":"49213","BRANCH":"233 ","TIPOERROR":"0","CLAVEERROR":"0","MENSAJE":"Venta exitosa","NUMIE":"300575301","NOMBREIE":"STEVEN  MANLLO DIECK"}')
    ;
    return $t;
  }

  public function testTransactionsByDate() {
    // Mock query result.
    $handler = $this->getHandlerMock(['queryTransactionsByDate']);
    $ts = [];
    for ($i = 0 ; $i < 1 ; $i++) {
      $ts[] = $this->getTransactionsByDateTransaction();
    }
    $handler->method('queryTransactionsByDate')
        ->willReturn($ts);

    $date = new \DateTime('yesterday');
    $output = $handler->transactionsByDate($date);

    $this->assertInternalType('array', $output);
    $this->assertGreaterThan(1, sizeof($output));

    foreach ($output as $line) {
      $this->assertInternalType('array', $line);
    }
  }

  public function testTransactionsByDateFailToday() {
    $this->setExpectedException('\Exception');
    $date = new \DateTime('now');
    $this->getHandler()->transactionsByDate($date);
  }


  public function testSerializeTransactionsByDateRow() {
    $handler = $this->getHandler();
    $output = $handler->serializeTransactionsByDateRow($this->getTransactionsByDateTransaction());
    $this->assertInternalType('array', $output);
  }


  public function testGetAppsById() {
    $handler = $this->getHandler();
    $output = $handler->getAppsById();
    $this->assertInternalType('array', $output);
  }


  public function testGetAppName() {
    $a = new App();
    $a
        ->setAppId('abc')
        ->setName('ABC')
    ;
    $handler = $this->getHandlerMock(['getAppsById']);
    $handler->method('getAppsById')
        ->willReturn(['abc' => $a]);

    $output = $handler->getAppName('abc');
    $this->assertEquals('ABC', $output);
  }


  public function testGetCinemasNamesById() {
    $handler = $this->getHandler();
    $output = $handler->getCinemasNamesById();
    $this->assertInternalType('array', $output);
  }


  public function testGetYearMonthRangeYear() {
    $year = 2014;
    $month = '';
    $handler = $this->getHandler();
    $output = $handler->getYearMonthRange($year, $month);
    $this->assertInternalType('array', $output);
    $this->assertEquals(new \DateTimeImmutable('2014-01-01 00:00:00'), $output[0]);
    $this->assertEquals(new \DateTimeImmutable('2015-01-01 00:00:00'), $output[1]);
  }


  public function testGetYearMonthRangeMonth() {
    $year = 2014;
    $month = '10';
    $handler = $this->getHandler();
    $output = $handler->getYearMonthRange($year, $month);
    $this->assertInternalType('array', $output);
    $this->assertEquals(new \DateTimeImmutable('2014-10-01 00:00:00'), $output[0]);
    $this->assertEquals(new \DateTimeImmutable('2014-11-01 00:00:00'), $output[1]);
  }


  public function testGetYearMonthRangeMonthNextYear() {
    $year = 2014;
    $month = '12';
    $handler = $this->getHandler();
    $output = $handler->getYearMonthRange($year, $month);
    $this->assertInternalType('array', $output);
    $this->assertEquals(new \DateTimeImmutable('2014-12-01 00:00:00'), $output[0]);
    $this->assertEquals(new \DateTimeImmutable('2015-01-01 00:00:00'), $output[1]);
  }


  public function testTicketsByCinemaByApp() {
    $handler = $this->getHandler();
    $output = $handler->ticketsByCinemaByApp(2014, NULL);
    $this->assertInternalType('array', $output);

    //@todo Actually test the data using fixtures or something.
  }

}


// Make private methods publicly available for testing.
class ReportHandlerTesteable extends ReportHandler {

  public function serializeTransactionsByDateRow($row) {
    return parent::serializeTransactionsByDateRow($row);
  }

  public function getAppsById() {
    return parent::getAppsById();
  }

  public function getAppName($app_id) {
    return parent::getAppName($app_id);
  }

  public function getCinemasNamesById() {
    return parent::getCinemasNamesById();
  }

  public function getYearMonthRange($year, $month = NULL) {
    return parent::getYearMonthRange($year, $month);
  }
}