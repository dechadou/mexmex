<?php
$view->extend('SocialSnackFrontBundle:Premium:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Premium</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-00.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-03.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-04.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-00.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-01.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-02.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-03.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-04.jpg'); ?>"></a>
        </div>
      </div>
    </div>

    <div class="grid-col-5">
      <p>En Cinemex®PREMIUM, una idea Original Cinemex®, podrás disfrutar la Magia del Cine en salas con butacas más grandes que te proporcionarán mayor comodidad, asientos numerados y con mayor privacidad entre butaca y butaca.</p>
      <p>Además, nuestro staff estará esperándote a tu llegada para ofrecerte nuestros productos de dulcería y llevártelos directo a la sala.</p>
      <p>Las Salas Premium son únicas en México y un concepto exclusivo de Cinemex®.</p>
      <p>Disfruta la experiencia de siempre, ahora mucho más confortable.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
