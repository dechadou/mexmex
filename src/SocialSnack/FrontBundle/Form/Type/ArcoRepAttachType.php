<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArcoRepAttachType extends AbstractType {
  
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('attach_id', 'file', array(
        'label' => 'Identificación oficial del representante legal (pasaporte, credencial de elector, cartilla)',
        'required' => FALSE,
        'attr' => array('class' => 'whole'),
        'constraints' => array(
            new NotBlank(array('groups' => array('rep'))),
            new File(array(
                'maxSize' => '6144k',
                'maxSizeMessage' => 'El tamaño del archivo supera el límite permitido.',
                'uploadErrorMessage' => 'No se pudo subir el archivo.'
            ))
        )
    ));
    $builder->add('attach_power', 'file', array(
        'label' => 'Poder donde se acredite la representación del titular',
        'required' => FALSE,
        'attr' => array('class' => 'whole'),
        'constraints' => array(
            new NotBlank(array('groups' => array('rep'))),
            new File(array(
                'maxSize' => '6144k',
                'maxSizeMessage' => 'El tamaño del archivo supera el límite permitido.',
                'uploadErrorMessage' => 'No se pudo subir el archivo.'
            ))
        )
    ));
  }

  
  public function getName() {
    return 'arco_rep';
  }
  
}