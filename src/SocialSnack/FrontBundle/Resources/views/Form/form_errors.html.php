<?php if ($errors) : ?>
<ul class="msg msg-error balloon balloon-below field-validation">
  <?php foreach ($errors as $error): ?>
  <li><?php echo $error->getMessage() ?></li>
  <?php endforeach; ?>
</ul>
<?php endif ?>
