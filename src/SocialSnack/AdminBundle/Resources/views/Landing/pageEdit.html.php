<?php
/** @var \SocialSnack\FrontBundle\Entity\LandingPage $page */

$title = $page->getId()
    ? sprintf('Editar sección %s', $page->getName())
    : 'Nueva sección';

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', $title . ' - Cinemex');
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <strong><?php echo $title; ?></strong>
          <a href="<?php echo $view['fronthelper']->get_permalink($page); ?>">(ver en front)</a>
          - <a href="<?php echo $view['router']->generate('admin_landing_edit', ['id' => $page->getLanding()->getId()]); ?>">Landing <?php echo $page->getLanding()->getName(); ?></a>
        </header>

        <div class="panel-body">
          <?php if (sizeof($submit_msg)) { ?>
            <div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
          <?php } ?>

          <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
          <?php echo $view['form']->form($form); ?>
        </div>
      </section>
    </div>
  </div>

<script>
  (function() {
    var $add = $('<a href="#">+</a>');
    var $promos = $('#promos');

    $promos.closest('.form-group').children('label').append(' ').append($add);
    $promos.children('.form-group').children('label').remove();

    $add.on('click', function(e) {
      e.preventDefault();

      var $new = $promos.attr('data-prototype');
      $new = $($new.replace(/__name__/g, $promos.children().length));

      $new.children('label').remove();
      $new.appendTo($promos);
    });
  })();
</script>
<?php $view['slots']->stop(); ?>