<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Aviso de privacidad integro para Cineusuario (página de internet) - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Aviso de privacidad integro para Cineusuario (página de internet)</h1>

<div class="entry-body align-justify">

  <p>Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>


  <h3>DATOS Y SU FINALIDAD:</h3>
  <p>Los Datos Personales que le son solicitados, serán tratados con las siguientes finalidades primarias:</p>
  <ul>
    <li>Realizar la Compra de boletos de cine por internet o a través de nuestra aplicación.</li>
    <li>Recibir la cartelera en el correo electrónico que nos proporciona;</li>
    <li>Invitarlo a participar en nuestros eventos; y </li>
    <li>Enviarle promociones de “Cinemex”.</li>
  </ul>
  <p>Asimismo, podremos tratar sus Datos Personales con las siguientes finalidades secundarias: </p>
  <ul>
    <li>Enviarle promociones de nuestros socios comerciales; y </li>
    <li>Envío de noticias relacionadas con el mundo del cine.</li>
  </ul>
  <p>En caso de no estar de acuerdo con nuestras finalidades secundarias, será necesario que nos los haga saber por medio de la Forma de Derechos ARCO que está en la siguiente página de internet: </p>
  <p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>

  <p>Le informamos que podrá manifestar su negativa para el tratamiento de sus Datos Personales en un plazo de 5 días hábiles después de haber otorgado su consentimiento al presente Aviso de Privacidad. En estos casos, sus Datos Personales, serán colocados en una Lista de Exclusión de Cineusuarios, en la cual únicamente personas autorizadas por “Cinemex” tendrán acceso a sus Datos Personales. Podrá manifestar su negativa a través de la página de internet:</p>
  <p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>

  <p>Los Datos Personales proporcionados a “Cinemex”, serán resguardados y administrados en términos de lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad (en adelante y conjuntamente “la Legislación”), en tal sentido, nos comprometemos a salvaguardar tal información bajo los principios de lealtad y responsabilidad. La información obtenida se encuentra protegida por medidas de seguridad físicas, tecnológicas y administrativas, apegadas a normas, que buscan salvaguardar los datos en términos de lo dispuesto en “la Legislación”.</p>

  <p>Los Datos Personales que se recaban, serán conservados por un periodo de 10 años en medios físicos y electrónicos (tomando en consideración aspectos administrativos, contables, fiscales y jurídicos), después de la conclusión de la relación comercial sostenida y posteriormente serán descartados a efecto de evitar un tratamiento indebido de los mismos, con fundamento en el artículo 11, segundo párrafo de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.</p>

  <h3>MEDIOS PARA EJERCER LOS DERECHOS “ARCO” Y REVOCACIÓN DEL CONSENTIMIENTO:</h3>

  <p>Podrá ejercer tus derechos de acceso, rectificación, cancelación y oposición contenidos en “la Legislación”, únicamente mediante el llenado del formato correspondiente que podrá obtener de la página de internet:</p>
  <p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>

  <p>Una vez presentada su solicitud en el formato prestablecido, “Cinemex” podrá solicitarle en un periodo no mayor a 5 días hábiles, la información y/o documentación necesaria para su seguimiento, así como para la acreditación de su identidad, de acuerdo a los términos que marca la Legislación. Por lo que contará con 10 días hábiles posteriores a su recepción, para atender este requerimiento. De lo contrario su solicitud se tendrá por no presentada.</p>

  <p>Asimismo, en un plazo posterior de 20 días hábiles “Cinemex” emitirá una resolución, la cual le será notificada por los medios de contacto que haya establecido en su solicitud. Una vez emitida la resolución y en caso de que la misma sea procedente (parcial o totalmente), “Cinemex” contará con 15 días hábiles para adoptar dicha resolución. </p>

  <p>Los términos y plazos indicados en los párrafos anteriores, podrán ser ampliados una sola vez en caso de ser necesario y se le deberá notificar a través de los medios de contacto que haya establecido. </p>

  <p>La revocación y el ejercicio de los Derechos ARCO serán gratuitos, debiendo usted cubrir únicamente los gastos justificados de envío, o el costo de reproducción en copias u otros formatos establecidos en su solicitud.</p>

  <p>Asimismo, le informamos que le asiste su derecho para acudir ante el Instituto Federal de Acceso a la Información y Protección de Datos en caso de considerar que su derecho a la protección de Datos Personales ha sido vulnerado. Para mayor información visite www.ifai.org.mx</p>

  <h3>IV. TRANSFERENCIA DE DATOS</h3>
  <p>“Cinemex”, se abstendrá de vender, arrendar o alquilar sus Datos Personales con un tercero, pero por razones de operación y servicio serán transferidos únicamente con Empresas:</p>
  <ul>
    <li>De Comunicación, a efecto de mantener informado de nuestros eventos y promociones o de nuestros patrocinadores y socios comerciales; y</li>
    <li>De investigaciones de mercado para hacer evaluaciones del servicio.</li>
  </ul>
  <p>Estas empresas se encuentran obligadas a mantener la confidencialidad de la información proporcionada  y a no utilizarla de manera diversa a la establecida en el presente Aviso de Privacidad. Se entiende que consientes la transferencia de tus datos.</p>

  <h3>CAMBIOS AL AVISO DE PRIVACIDAD Y VULNERACIÓN DE SEGURIDAD:</h3>
  <p>“Cinemex” tiene el derecho de efectuar cualquier cambio o modificación al Aviso de Privacidad. Tales modificaciones podrán ser consultadas en: <a href="http://cinemex.com/privacidadcineusuario">http://cinemex.com/privacidadcineusuario</a></p>

  <p>En caso de que ocurra una vulneración de seguridad en cualquier fase del tratamiento de sus Datos Personales, “Cinemex” a través de la figura del responsable lo hará del conocimiento general, a través de la siguiente página de internet:</p>
  <p><a href="http://cinemex.com/privacidadcineusuario">http://cinemex.com/privacidadcineusuario</a>, para que tome las medidas correspondientes a fin de resguardar sus derechos.</p>
  <p>Fecha de última actualización: 5/Enero/2015</p>
</div>
<?php $view['slots']->stop(); ?>