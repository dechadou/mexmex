<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntitySession extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('checkout', ['session_id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('mobile_checkout', ['session_id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\WsBundle\Entity\SessionRepository::findWithMovie:' . $this->entity->getId(),
                'SocialSnack\WsBundle\Entity\SessionRepository::findWithMovieAndCinema:' . $this->entity->getId(),
                'SocialSnack\WsBundle\Entity\SessionRepository::findWithCinema:' . $this->entity->getId()
            ],
            'env' => 'front'
        ]
    ];
  }

}