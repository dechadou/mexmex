<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['homepage']); ?>
<?php $view['slots']->set('title', 'Cinemex - La magia del cine'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="canonical" href="http://cinemex.com" />
<link rel="alternate" href="cinemex://com.cinemex/home" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
  <?php
  echo $this->render(
      'SocialSnackFrontBundle:Partials:mainSlider.html.php',
      array(
          'promosa'         => $promosa
      )
  );
  ?>
				<div class="clearfix row">

					<div id="billboard-movies" class="col col-sm-3-5 col-md-2-3">
            <div class="tabs">
              <ul class="file-tabs tabs-links hidden col-xs-hidden col-sm-hidden">
                <li><a href="#" class="tab selected">En cartelera</a></li>
                <li><a href="#" class="tab tab-next-week">Próxima semana</a></li>
                <li><a href="#" class="tab">Próximos estrenos</a></li>
                <li><a href="#" class="tab">Preventas</a></li>
              </ul>

              <div class="tabs-content">
                <div class="tab-content selected" rel="main-billboard">
                  <?php
                  $movies_per_page = 16;
                  $movies_cols     = 4;
                  echo $view->render(
                      'SocialSnackFrontBundle:Partials:bodyMovies.html.php',
                      array(
                          'movies'          => array_slice( $movies, 0, $movies_per_page ),
                          '_movies'         => $_movies,
                          'movies_per_page' => $movies_per_page,
                          'movies_cols'     => $movies_cols,
                      )
                  );
                  ?>
                </div>
                <div class="tab-content">
                  <?php
                  echo $view->render(
                      'SocialSnackFrontBundle:Partials:bodyComingMovies.html.php',
                      array(
                          'movies'      => $friday,
                          'movies_cols' => $movies_cols
                      )
                  );
                  ?>
                </div>
                <div class="tab-content">
                  <?php
                  echo $view->render(
                      'SocialSnackFrontBundle:Partials:bodyComingMovies.html.php',
                      array(
                          'movies'      => $coming,
                          'movies_cols' => $movies_cols,
                          'is_friday'   => FALSE
                      )
                  );
                  ?>
                </div>
                <div class="tab-content tab-content-presales">
                  <?php
                  echo $view->render(
                      'SocialSnackFrontBundle:Partials:bodyMovies.html.php',
                      array(
                          'movies'      => $presales,
                          'movies_per_page' => $movies_per_page,
                          'movies_cols'     => $movies_cols,
                      )
                  );
                  ?>
                </div>
              </div>
            </div>

					</div><!-- #billboard-movies -->

					<div class="dynamic-container col col-sm-2-5 col-md-1-3 require-location" data-route="partials/sidebarCinema/{id}/{args}" data-source="fn get_cinemasidebar_args" data-listeners="user_login change_location" data-loadcb="mycinema_sidebar_loaded">

						<div id="location-sidebar" class="popup location-popup">
							<?php echo $this->render( 'SocialSnackFrontBundle:Popups:locationSide.html.php'); ?>
						</div><!-- #location-sidebar -->

					</div>

				</div>
        <script>
          var is_home = true;
        </script>
<?php $view['slots']->stop(); ?>
