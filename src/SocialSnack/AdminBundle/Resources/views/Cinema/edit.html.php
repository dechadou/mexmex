<?php

use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Editar cine ' . $cinema->getName() . ' - Cinemex');
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Editar <strong><?php echo $cinema->getName(); ?></strong>
        <a href="<?php echo $view['fronthelper']->get_permalink($cinema); ?>">(ver en front)</a>
      </header>
      
      <div class="panel-body">
        <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
        <form class="form-horizontal tasi-form" method="post">
          <?php echo $view['form']->errors($form) ?>
          
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['name']); ?>
              <?php echo $view['form']->errors($form['name']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Activo</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['active']); ?>
              <?php echo $view['form']->errors($form['active']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Platino</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['platinum']); ?>
              <?php echo $view['form']->errors($form['platinum']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Estado</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['state_id']); ?>
              <?php echo $view['form']->errors($form['state_id']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Area</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['area_id']); ?>
              <?php echo $view['form']->errors($form['area_id']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Latitud</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['lat']); ?>
              <?php echo $view['form']->errors($form['lat']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Longitud</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['lng']); ?>
              <?php echo $view['form']->errors($form['lng']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Dirección</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['DIRECCION']); ?>
              <?php echo $view['form']->errors($form['DIRECCION']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Colonia</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['COLONIA']); ?>
              <?php echo $view['form']->errors($form['COLONIA']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Teléfonos</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['TELEFONOS']); ?>
              <?php echo $view['form']->errors($form['TELEFONOS']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Contacto</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['CONTACTO']); ?>
              <?php echo $view['form']->errors($form['CONTACTO']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['EMAIL']); ?>
              <?php echo $view['form']->errors($form['EMAIL']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Atributos</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['attributes']); ?>
              <?php echo $view['form']->errors($form['attributes']); ?>
            </div>
          </div>

          <button type="button" class="btn btn-default" onclick="window.location.href = '../cinema_list';">Cancelar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>