<?php $view->extend('SocialSnackFrontBundle::lightTheme.html.php'); ?>

<?php $view['slots']->start('body'); ?>
<form method="POST" action="" class="oauth-grant-dialog">
  <input name="token" value="<?php echo $token; ?>" type="hidden" />
  <input name="redirect_uri" value="<?php echo $redirect_uri; ?>" type="hidden" />
  <input name="client_id" value="<?php echo $client_id; ?>" type="hidden" />
  <p class="oauth-grant-msg">
    ¿Deseas permitir que <strong><?php echo $client->getName(); ?></strong> acceda a tu información personal?
  </p>
  <div class="btn-row">
    <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Permitir</button>
    <a href="<?php echo $cancel_uri; ?>" class="btn btn-icon icon icon-larr">Cancelar</a>
  </div>
</form>
<?php $view['slots']->stop(); ?>