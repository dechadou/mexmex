<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\FrontBundle\Exception\CustomHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\FrontBundle\Entity\Contact;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Endroid\QrCode\QrCode;
use SocialSnack\FrontBundle\Exception\DisabledSessionException;

class CheckoutController extends BaseController {


  /**
   * Find the session by session ID and make sure everything is ok before going
   * to the checkout screen.
   *
   * If there's some issue, attempt to fix it or redirect to some nice error page.
   *
   * @param int $session_id
   * @return \SocialSnack\WsBundle\Entity\Session
   * @throws NotFoundHttpException
   * @throws DisabledSessionException
   */
  protected function get_session( $session_id ) {
		/* @var $session_repo \SocialSnack\WsBundle\Entity\SessionRepository */
		$session_repo = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Session' );

		/* @var $session \SocialSnack\WsBundle\Entity\Session */
		$session = $session_repo->findWithMovieAndCinemaAndTickets( $session_id );

    if ( !$session ) {
      throw $this->createNotFoundException();
    }

    // Make sure we're allowed to sell in this cinema.
    $this->_check_cinema_disabled($session->getCinema());


		// Make sure the queried session is some time in the future.
    $now = new \DateTime('now', new \DateTimeZone('America/Mexico_City'));
    if ($timezone = $session->getCinema()->getState()->getTimezone()) {
      $now->setTimezone(new \DateTimeZone($timezone));
    }
    $session_time = new \DateTime(
        $session->getDateTime()->format('Y-m-d H:i:s'),
        new \DateTimeZone($timezone ? $timezone : 'America/Mexico_City')
    );

		if ( $now > $session_time || !$session->getActive() ) {
      throw new DisabledSessionException($session);
    }

    // Movies not available for online selling.
    if (in_array($session->getMovie()->getGroupId(), WsHelper::getNoOnlineSellingGroupIds())) {
      $e = new CustomHttpException(200, 'Para adquirir boletos a esta función debes acudir directamente a la taquilla del cine.');
      $e
          ->setText('Sujeto a disponibilidad.')
          ->setPageTitle('Aviso');
      throw $e;
    }

    // Apply tickets filters. ie: hardcoded rules to remove/hide tickets on specific cases.
    $session->setTickets(
        $this->get('rest.session.handler')->applyTicketsFilters($session->getTickets(), $session)
    );

    // There're tickets for this session?
		if ( !sizeof($session->getTickets()) ) {
      // Maybe some kind of alert could be triggered here. With some kind of buffering to prevent a ton of alerts being
      // sent each time an user lands here.
      throw new DisabledSessionException($session);

      // Purge the cache first.
      $cache_purger = $this->container->get('ss.cache_purger');
      /* @var $cache_purger \SocialSnack\FrontBundle\Service\CachePurger */
      $purge = $cache_purger->purge_cache(array(
          'entity' => $session,
      ));

      // We need to detach the entity first to avoid wired issues with the updater service.
      $this->getDoctrine()->getManager()->detach($session);

      // For some reason there're no tickets available for this session, so the
      // user won't be able to buy. Try to fetch the data from the WS again.
      $service = $this->container->get('cinemex.session_updater');
      $updated = $service->execute(array(
          'cinema_id' => $session->getCinema()->getId(),
          'propagate' => TRUE,
          'enqueue'   => FALSE,
      ));

      // Fetch the session again. Hopefully now with the correct data.
      $session = $session_repo->findWithMovieAndCinemaAndTickets( $session_id );

      // If we had no luck and there're no tickets, display an error message.
      if ( !sizeof($session->getTickets()) ) {
        throw new DisabledSessionException($session);
      }
    }

    return $session;
  }


  /**
   * @Route("/legacy/")
   */
  public function legacyCheckoutAction(Request $request) {
    if (!$request->query->has('session_id') || !$request->query->has('cinema_id')) {
      throw $this->createNotFoundException();
    }

    $is_mobile = ($request->getHttpHost() === 'm.' . $this->container->getParameter('domain'));

    $legacy_id        = $request->query->get('session_id');
    $legacy_cinema_id = $request->query->get('cinema_id');
    $repo = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Session');
    $session = $repo->findOneBy([
        'legacy_id'        => $legacy_id,
        'legacy_cinema_id' => $legacy_cinema_id,
    ]);

    if (!$session) {
      throw $this->createNotFoundException();
    }

    $params = ['session_id' => $session->getId()];

    if ($request->query->has('ref')) {
      $params['ref'] = $request->query->get('ref');
    }

    $redirect_url = $this->generateUrl(
        $is_mobile ? 'mobile_checkout' : 'checkout',
        $params
    );

    if ($request->query->has('no-location')) {
      $redirect_url .= '#no-location';
    }

    return $this->redirect($redirect_url, 301);
  }


	/**
	 * @Route("/{session_id}/", requirements={"session_id" = "[0-9]+"}, name="checkout")
	 * @Template(engine="php")
   * @Cache(public=true, smaxage="60", maxage="30")
	 */
	public function checkoutAction( $session_id ) {
		$session = $this->get_session($session_id);

    $ref = $this->getRequest()->query->get('ref');
    if ( !$ref ) {
      $ref = 'default';
    }

    $show_captcha = TRUE;
    $user         = $this->getUser();
    $carriers     = $this->container->getParameter('sms_carriers');

		return array(
        'session' => $session,
        'ref'     => $ref,
        'captcha' => $show_captcha,
        'user'     => $user,
        'carriers' => $carriers
    );
	}


//	public function checkoutTestAction() {
//   * @Route("/test")
//   * @Template(engine="php")
//    $ref = $this->getRequest()->query->get('ref');
//    if ( !$ref ) {
//      $ref = 'default';
//    }
//
//    $show_captcha = FALSE;
//    $ts = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Transaction')->findBy(array(), array('id' => 'DESC'), 1);
//    $t = $ts[0];
//    $sessions = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Session')->findBy(array(), NULL, 1);
//    $session = $sessions[0];
//
//		return array(
//        'session' => $session,
//        'ref'     => $ref,
//        'captcha' => $show_captcha,
//        't'       => $t,
//    );
//	}


	/**
	 * @Route("/m/{session_id}/", requirements={"session_id" = "[0-9]+"}, name="mobile_checkout")
	 * @Template(engine="php")
   * @Cache(public=true, smaxage="60", maxage="30")
	 */
  public function checkoutMobileAction($session_id) {
		$session = $this->get_session($session_id);

    $ref = $this->getRequest()->query->get('ref');
    if ( !$ref ) {
      $ref = 'mobile';
    }

		return array(
        'session' => $session,
        'user'    => $this->getUser(),
        'ref'     => $ref,
    );
  }

}
