<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadNotificationsData;

class NotificationsControllerTest extends WebTestCase {

  public function testDefault() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadNotificationsData',
    ]);
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/notifications/'
    );

    $model = [
        'notifications' => [
            'type' => 'array',
            'each' => [
                'id'        => 'integer',
                'date'      => 'string',
                'title'     => 'string',
                'content'   => 'string',
                'deep_link' => 'string',
            ]
        ]
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  public function testTarget() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadNotificationsData',
    ]);

    $noti_id = LoadNotificationsData::$notifications[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/notifications/%d', $noti_id)
    );

    $model = [
        'id'        => [
            'type'   => 'integer',
            'equals' => $noti_id,
        ],
        'date'      => 'string',
        'title'     => 'string',
        'content'   => 'string',
        'deep_link' => 'string',
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

}