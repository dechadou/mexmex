<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StateArea
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="sta_state_idx", columns={"active","state_id"})
 * })
 * @ORM\Entity(repositoryClass="SocialSnack\WsBundle\Entity\StateAreaRepository")
 */
class StateArea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

		/**
		 * @var integer
		 * 
		 * @ORM\Column(type="integer")
		 */
		private $legacy_id;

		/**
		 * @var integer
		 * 
		 * @ORM\Column(type="integer")
		 */
		private $legacy_state_id;
		
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = '';

		/**
		 * @var string
		 * 
		 * @ORM\Column(type="string", length=255)
		 */
		private $legacy_name;
		
		/**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="areas")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
		private $state;
		
		/**
		 * @ORM\OneToMany(targetEntity="Cinema", mappedBy="area")
		 */
		private $cinemas;
		
		/**
		 * @var boolean
		 * 
		 * @ORM\Column(type="boolean")
		 */
		private $active = 1;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StateArea
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name ? $this->name : $this->legacy_name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return StateArea
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set state
     *
     * @param \SocialSnack\WsBundle\Entity\State $state
     * @return StateArea
     */
    public function setState(\SocialSnack\WsBundle\Entity\State $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \SocialSnack\WsBundle\Entity\State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set legacy_name
     *
     * @param string $legacyName
     * @return StateArea
     */
    public function setLegacyName($legacyName)
    {
        $this->legacy_name = $legacyName;
    
        return $this;
    }

    /**
     * Get legacy_name
     *
     * @return string 
     */
    public function getLegacyName()
    {
        return $this->legacy_name;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return StateArea
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set legacy_state_id
     *
     * @param integer $legacyStateId
     * @return StateArea
     */
    public function setLegacyStateId($legacyStateId)
    {
        $this->legacy_state_id = $legacyStateId;
    
        return $this;
    }

    /**
     * Get legacy_state_id
     *
     * @return integer 
     */
    public function getLegacyStateId()
    {
        return $this->legacy_state_id;
    }
		
		
		public function toArray( $include_cinemas = FALSE ) {
      $output = array(
					'id'       => $this->id,
					'name'     => $this->name,
					'state_id' => $this->getState()->getId(),
			);
      
      if ( $include_cinemas ) {
        $output['cinemas'] = array();
        foreach ( $this->getCinemas() as $cinema ) {
          $output['cinemas'][] = $cinema->toArray();
        }
      }
      
			return $output;
		}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cinemas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add cinemas
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemas
     * @return StateArea
     */
    public function addCinema(\SocialSnack\WsBundle\Entity\Cinema $cinemas)
    {
        $this->cinemas[] = $cinemas;
    
        return $this;
    }

    /**
     * Remove cinemas
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemas
     */
    public function removeCinema(\SocialSnack\WsBundle\Entity\Cinema $cinemas)
    {
        $this->cinemas->removeElement($cinemas);
    }

    /**
     * Get cinemas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCinemas()
    {
        return $this->cinemas;
    }
}