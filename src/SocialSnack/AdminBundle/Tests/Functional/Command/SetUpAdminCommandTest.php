<?php

namespace SocialSnack\AdminBundle\Tests\Functional\Command;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\AdminBundle\Command\SetUpAdminCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class SetUpAdminCommandTest extends WebTestCase {

  static $kernel;

  public function setUp() {
    static::$kernel = static::createKernel();
    static::$kernel->boot();
  }

  public function testSetUpSuccess() {
    // Start with an empty DB.
    $this->loadFixtures([]);

    $application = new Application(static::$kernel);
    $application->add(new SetUpAdminCommand());
    $command = $application->find('admin:setup');
    $commandTester = new CommandTester($command);

    $dialog = $command->getHelper('dialog');
    $dialog->setInputStream($this->getInputStream(
        "administrator\n"   // Username
      . "fo0barFoobar!\n"  // Password
    ));

    $commandTester->execute(array('command' => $command->getName()));

    // Perform the login.
    $client = self::createClient();
    $crawler = $client->request(
        'GET',
        '/admin_login'
    );

    $form = $crawler->selectButton('login')->form();
    $form->setValues([
        '_username' => 'administrator',
        '_password' => 'fo0barFoobar!',
    ]);

    $client->submit($form);

    $this->assertRegexp('/\/missisippi\/$/', $client->getResponse()->headers->get('location'));
    $this->assertSame(302, $client->getResponse()->getStatusCode());
  }

  public function testUsernameLength() {
    // Start with an empty DB.
    $this->loadFixtures([]);

    $application = new Application(static::$kernel);
    $application->add(new SetUpAdminCommand());
    $command = $application->find('admin:setup');
    $commandTester = new CommandTester($command);

    $dialog = $command->getHelper('dialog');
    $dialog->setInputStream($this->getInputStream(
        "admin\n"         // Username
      . "administrator\n" // Username
      . "Fo0barFoobar!\n" // Username
    ));

    $commandTester->execute(array('command' => $command->getName()));

    $this->assertSame(1, substr_count($commandTester->getDisplay(), 'Username is too short.'));
  }

  public function testPasswordStrength() {
    // Start with an empty DB.
    $this->loadFixtures([]);

    $application = new Application(static::$kernel);
    $application->add(new SetUpAdminCommand());
    $command = $application->find('admin:setup');
    $commandTester = new CommandTester($command);

    $dialog = $command->getHelper('dialog');
    $dialog->setInputStream($this->getInputStream(
        "administrator\n"   // Username
      . "foobarfoobarf\n"  // Password
      . "foobarFoobarf\n"  // Password
      . "fo0barFoobarf\n"  // Password
      . "fo0bar!\n"        // Password
      . "fo0barFoobar!\n"  // Password
    ));

    $commandTester->execute(array('command' => $command->getName()));

    $this->assertSame(4, substr_count($commandTester->getDisplay(), 'Password is too weak.'));
  }

  protected function getInputStream($input) {
    $stream = fopen('php://memory', 'r+', false);
    fputs($stream, $input);
    rewind($stream);

    return $stream;
  }


  public function testAlreadyInitialized() {
    // Load fixture with an existing admin.
    $this->loadFixtures([
        'SocialSnack\AdminBundle\Tests\Fixtures\Command\SetUpAdminCommand\AlreadyInitializedData'
    ]);

    $output = $this->runCommand('admin:setup');

    $this->assertRegexp('/ERROR: Admins table is not empty./', $output);
  }

}