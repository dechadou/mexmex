<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['cinemom']);
$view['slots']->set('title', 'CineMá - Cinemex');
$view['slots']->start('body')
?>
        <?php
        echo $view->render(
            'SocialSnackFrontBundle:Sections:cinemomMenu.html.php',
            array( 'route' => $route )
        );
        ?>

				<div class="cinemom-content">
          <h1 class="content-box-title">Términos y condiciones</h1>
          
          <ol>
	<li>El programa Cinemá está destinado a las primeras funciones de los días jueves y domingos en complejos Cinemex participantes.</li>
	<li>La película a proyectarse será el estreno de la semana, no necesariamente será una película infantil y no se proyectarán películas de clasificación C. Las funciones y horarios, así como los cines participantes podrán ser consultados en Cinemex.com</li>
	<li>Las características dentro de la sala, en funciones Cinemá son:
		<ul>
			<li>La luz se mantiene encendida al 50%</li>
			<li>La temperatura se mantiene templada</li>
			<li>El sonido de la película se proyecta a un nivel moderado</li>
			<li>Se ofrece servicio a la sala de dulcería, Café Central, Alavista y Red Mango</li>
		</ul>
	</li>
	<li>Las carriolas de los bebés deberán ser colocadas debajo de la pantalla dentro de la sala con el fin de no estorbar el pasillo en caso de emergencia.</li>
	<li>El precio del boleto de la función Cinemá será el precio de la matiné del complejo Cinemex elegido. El bebé, siempre y cuando sea menor a 2 años, no pagará boleto de entrada.</li>
	<li>La programación de Cinemá está destinada a adultos, por lo cual no se permitirá la entrada a niños mayores de 2 años. No se permitirá la entrada a personas que no vengan acompañados de un bebé. Debe de haber por lo menos un bebé, por grupo.</li>
	<li>El cuidado y la vigilancia de los bebés y/o menores de edad es exclusiva de los adultos que lo acompañan por lo que Cinemex en ningún momento será responsable de dichos menores.</li>
	<li>Los cines participantes deberán consultarse en Cinemex.com, sección Cinemá.</li>
	<li>En la cartelera del cine estará identificada la función Cinemá con el logo adjunto al título de la película.</li>
</ol>

				</div>
<?php $view['slots']->stop(); ?>