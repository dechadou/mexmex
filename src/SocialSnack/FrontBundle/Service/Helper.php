<?php

namespace SocialSnack\FrontBundle\Service;

class Helper {


  /**
   * Format price from a price string with decimals but no decimal separator
   * (ie: 6500 = 65.00).
   *
   * @param string $price
   * @param int    $decimal_places
   * @param string $decima_sep
   * @param string $thousands_sep
   * @param string $sign
   * @return string
   */
	public static function format_price( $price, $decimal_places = 2, $decima_sep = '.', $thousands_sep = ',', $sign = '$' ) {
		return $sign . number_format( (float)substr_replace( $price, '.', $decimal_places * -1, 0 ), $decimal_places, $decima_sep, $thousands_sep );
	}


	/**
	 * Parse URL arguments in format /key1-value1/.../keyn-valuen/
	 *
	 * @param string $arguments
	 * @return array
	 */
	public static function parse_url_arguments( $arguments ) {
		$arguments   = array_filter(explode( '/', $arguments ));
		$args        = array();
		foreach ( $arguments as $arg ) {
			$arg = explode( '-', $arg );
			$key = array_shift( $arg );
			$arg = implode( '-', $arg );
			$args[ $key ] = $arg;
		}
		return $args;
	}


	/**
	 * Sanitize string.
	 * Remove non alphanumeric characters and replace them with dashes.
	 *
	 * @param string $str Raw string.
	 * @return string Sanitized string.
	 */
	public static function sanitize_for_url( $str ) {
    $str = strtolower( trim( $str ) );
    $str = self::transliterate_to_ascii( $str );

    $str = preg_replace('/[^\da-z]/i', '-', $str);
    $str = preg_replace('/\-+/i', '-', $str);
    $str = preg_replace('/^-+/', '', $str);
    $str = preg_replace('/-+$/', '', $str);
    return $str;
	}

  /**
   * Replaces special/accented UTF-8 characters by ASCII-7 "equivalents".
   *
   * @author  Andreas Gohr <andi@splitbrain.org>
   * @param   string  $str    string to transliterate
   * @param   integer $case   -1 lowercase only, +1 uppercase only, 0 both cases
   * @return  string
   */
  public static function transliterate_to_ascii($str, $case = 0) {
    static $utf8_lower_accents = NULL;
    static $utf8_upper_accents = NULL;

    if ($case <= 0) {
      if ($utf8_lower_accents === NULL) {
        $utf8_lower_accents = array(
          'à' => 'a',  'ô' => 'o',  'ď' => 'd',  'ḟ' => 'f',  'ë' => 'e',  'š' => 's',  'ơ' => 'o',
          'ß' => 'ss', 'ă' => 'a',  'ř' => 'r',  'ț' => 't',  'ň' => 'n',  'ā' => 'a',  'ķ' => 'k',
          'ŝ' => 's',  'ỳ' => 'y',  'ņ' => 'n',  'ĺ' => 'l',  'ħ' => 'h',  'ṗ' => 'p',  'ó' => 'o',
          'ú' => 'u',  'ě' => 'e',  'é' => 'e',  'ç' => 'c',  'ẁ' => 'w',  'ċ' => 'c',  'õ' => 'o',
          'ṡ' => 's',  'ø' => 'o',  'ģ' => 'g',  'ŧ' => 't',  'ș' => 's',  'ė' => 'e',  'ĉ' => 'c',
          'ś' => 's',  'î' => 'i',  'ű' => 'u',  'ć' => 'c',  'ę' => 'e',  'ŵ' => 'w',  'ṫ' => 't',
          'ū' => 'u',  'č' => 'c',  'ö' => 'o',  'è' => 'e',  'ŷ' => 'y',  'ą' => 'a',  'ł' => 'l',
          'ų' => 'u',  'ů' => 'u',  'ş' => 's',  'ğ' => 'g',  'ļ' => 'l',  'ƒ' => 'f',  'ž' => 'z',
          'ẃ' => 'w',  'ḃ' => 'b',  'å' => 'a',  'ì' => 'i',  'ï' => 'i',  'ḋ' => 'd',  'ť' => 't',
          'ŗ' => 'r',  'ä' => 'a',  'í' => 'i',  'ŕ' => 'r',  'ê' => 'e',  'ü' => 'u',  'ò' => 'o',
          'ē' => 'e',  'ñ' => 'n',  'ń' => 'n',  'ĥ' => 'h',  'ĝ' => 'g',  'đ' => 'd',  'ĵ' => 'j',
          'ÿ' => 'y',  'ũ' => 'u',  'ŭ' => 'u',  'ư' => 'u',  'ţ' => 't',  'ý' => 'y',  'ő' => 'o',
          'â' => 'a',  'ľ' => 'l',  'ẅ' => 'w',  'ż' => 'z',  'ī' => 'i',  'ã' => 'a',  'ġ' => 'g',
          'ṁ' => 'm',  'ō' => 'o',  'ĩ' => 'i',  'ù' => 'u',  'į' => 'i',  'ź' => 'z',  'á' => 'a',
          'û' => 'u',  'þ' => 'th', 'ð' => 'dh', 'æ' => 'ae', 'µ' => 'u',  'ĕ' => 'e',  'ı' => 'i',
        );
      }

      $str = str_replace(
        array_keys($utf8_lower_accents),
        array_values($utf8_lower_accents),
        $str
      );
    }

    if ($case >= 0) {
      if ($utf8_upper_accents === NULL) {
        $utf8_upper_accents = array(
          'À' => 'A',  'Ô' => 'O',  'Ď' => 'D',  'Ḟ' => 'F',  'Ë' => 'E',  'Š' => 'S',  'Ơ' => 'O',
          'Ă' => 'A',  'Ř' => 'R',  'Ț' => 'T',  'Ň' => 'N',  'Ā' => 'A',  'Ķ' => 'K',  'Ĕ' => 'E',
          'Ŝ' => 'S',  'Ỳ' => 'Y',  'Ņ' => 'N',  'Ĺ' => 'L',  'Ħ' => 'H',  'Ṗ' => 'P',  'Ó' => 'O',
          'Ú' => 'U',  'Ě' => 'E',  'É' => 'E',  'Ç' => 'C',  'Ẁ' => 'W',  'Ċ' => 'C',  'Õ' => 'O',
          'Ṡ' => 'S',  'Ø' => 'O',  'Ģ' => 'G',  'Ŧ' => 'T',  'Ș' => 'S',  'Ė' => 'E',  'Ĉ' => 'C',
          'Ś' => 'S',  'Î' => 'I',  'Ű' => 'U',  'Ć' => 'C',  'Ę' => 'E',  'Ŵ' => 'W',  'Ṫ' => 'T',
          'Ū' => 'U',  'Č' => 'C',  'Ö' => 'O',  'È' => 'E',  'Ŷ' => 'Y',  'Ą' => 'A',  'Ł' => 'L',
          'Ų' => 'U',  'Ů' => 'U',  'Ş' => 'S',  'Ğ' => 'G',  'Ļ' => 'L',  'Ƒ' => 'F',  'Ž' => 'Z',
          'Ẃ' => 'W',  'Ḃ' => 'B',  'Å' => 'A',  'Ì' => 'I',  'Ï' => 'I',  'Ḋ' => 'D',  'Ť' => 'T',
          'Ŗ' => 'R',  'Ä' => 'A',  'Í' => 'I',  'Ŕ' => 'R',  'Ê' => 'E',  'Ü' => 'U',  'Ò' => 'O',
          'Ē' => 'E',  'Ñ' => 'N',  'Ń' => 'N',  'Ĥ' => 'H',  'Ĝ' => 'G',  'Đ' => 'D',  'Ĵ' => 'J',
          'Ÿ' => 'Y',  'Ũ' => 'U',  'Ŭ' => 'U',  'Ư' => 'U',  'Ţ' => 'T',  'Ý' => 'Y',  'Ő' => 'O',
          'Â' => 'A',  'Ľ' => 'L',  'Ẅ' => 'W',  'Ż' => 'Z',  'Ī' => 'I',  'Ã' => 'A',  'Ġ' => 'G',
          'Ṁ' => 'M',  'Ō' => 'O',  'Ĩ' => 'I',  'Ù' => 'U',  'Į' => 'I',  'Ź' => 'Z',  'Á' => 'A',
          'Û' => 'U',  'Þ' => 'Th', 'Ð' => 'Dh', 'Æ' => 'Ae', 'İ' => 'I',
        );
      }

      $str = str_replace(
        array_keys($utf8_upper_accents),
        array_values($utf8_upper_accents),
        $str
      );
    }

    return $str;
  }


	/**
	 * Extract YouTube ID from YouTube URL.
	 * URL must be in ?v=(id) format.
	 *
	 * @todo Handle more possible URLs.
	 *
	 * @param string $url URL.
	 * @return string ID.
	 */
	public static function get_youtube_id_from_url( $url ) {
		if ( !$url || !is_string( $url ) )
			return FALSE;

		if ( !preg_match( '/\?v\=(?<id>.+)/', $url, $match ) )
			return NULL;

		return $match['id'];
	}


	/**
	 *
	 * @param array $unordered
	 * @param array $distance_rels
   * @return array
	 */
	public static function order_cinemas_by_distance( $unordered, $distance_rels ) {
		// Move the distances into a key => value array using cinemas IDs as keys.
		$ref = array();
		foreach ( $distance_rels as $rel ) {
			$ref[ $rel->getToCinema()->getId() ] = $rel->getDistance();
		}

		asort( $ref );


		$ordered = array();
    foreach ( $ref as $key => $value ) {
			foreach ( $unordered as $cinema ) {
				if ( $key == $cinema['cinema']->getId() ) {
					$ordered[] = $cinema;
					unset( $ref[ $key ] );
					break;
				}
			}
    }
		return $ordered;
	}


  /**
   * Return current user IP address.
   *
   * @return string
   */
  public static function get_user_ip() {
    if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ( $ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
      $ips = explode(',', $ip);
      $ips = array_map('trim', $ips);
      return array_shift($ips);
    } elseif ( isset($_SERVER['REMOTE_ADDR']) ) {
      return $_SERVER['REMOTE_ADDR'];
    } else {
      return '';
    }
  }


  /**
   * Parse the attributes column form Movie entity and return a human-readable
   * array of strings.
   *
   * @param \stdClass|array $attrs Attributes object.
   * @param string $glue
   * @param boolean $strict If strict, element value must be true.
   * @return string
   */
  public static function get_movie_attr_str($attrs, $glue = ' ', $strict = TRUE) {
    $types = self::get_movie_attrs($attrs, $strict, TRUE);
    return implode($glue, self::sort_movie_attrs($types));
  }


  public static function sort_movie_attrs($attrs) {
    $order = ['platinum', 'premium', 'cinemom', 'extreme', 'cx', 'v4d', 'v3d', 'digital'];
    $output = [];

    foreach ($order as $index) {
      foreach ($attrs as $attr => $label) {
        if ($attr === $index) {
          $output[$attr] = $label;
          unset($attrs[$attr]);
        }
      }
    }

    foreach ($attrs as $attr => $label) {
      $output[$attr] = $label;
    }

    return $output;
  }


  /**
   * @param stdClass|array $attrs
   * @param bool $strict
   * @param bool $associative
   * @return array
   */
  public static function get_movie_attrs($attrs, $strict = TRUE, $associative = FALSE) {
    $names = array();
    $collection = array_filter(self::get_attributes_collection(), function($a) {
      return $a['version'];
    } );

    foreach ( $attrs as $attr => $v ) {
      // Handle non associative arrays...
      if (is_numeric($attr)) {
        $attr = $v;
        $v = TRUE;
      }

      // @todo Does this $strict argument actually make sense? Is it used anywhere? I would be nice to remove it...
      if ( $strict && !$v ) {
        continue;
      }

      if ( isset($collection[$attr]) ) {
        $name = $collection[$attr]['display_name'];
        if ($associative) {
          $names[$attr] = $name;
        } else {
          $names[] = $name;
        }
      }
    }

    $i = array_search('Digital', $names);
    if ($i !== FALSE) {
      if (sizeof($names) > 2) {
        unset($names[$i]);
      } else {
        $names[$i] = 'Tradicional';
      }
    }

    return $names;
  }


  /**
   * @param $attr
   * @return null
   */
  public static function get_movie_attr_name($attr) {
    $col = self::get_attributes_collection(array('version' => TRUE));

    if (isset($col[$attr])) {
      return $col[$attr]['display_name'];
    }

    return NULL;
  }


  /**
   * List all posible attributes for a movie.
   *
   * @param array $filter
   * @return array
   */
  public static function get_attributes_collection( $filter = NULL ) {
    $result = array(
        'digital'       => array('display_name' => 'Digital', 'version' => TRUE),
        'v3d'           => array('display_name' => '3D', 'version' => TRUE),
        'v4d'           => array('display_name' => 'X4D', 'version' => TRUE),
        'cx'            => array('display_name' => 'CinemeXtremo', 'version' => TRUE),
        'lang_es'       => array('display_name' => 'Español', 'version' => TRUE),
        'lang_en'       => array('display_name' => 'Inglés', 'version' => TRUE),
        'lang_fr'       => array('display_name' => 'Francés', 'version' => TRUE),
        'lang_de'       => array('display_name' => 'Alemán', 'version' => TRUE),
        'lang_jp'       => array('display_name' => 'Japonés', 'version' => TRUE),
        'hfr'           => array('display_name' => 'HFR', 'version' => TRUE),
        'premium'       => array('display_name' => 'Premium', 'version' => TRUE),
        'macro'         => array('display_name' => 'Macropantalla', 'version' => TRUE),
        'jumbo'         => array('display_name' => 'Jumbo', 'version' => TRUE),
        'platinum'      => array('display_name' => 'Platino', 'version' => TRUE),
        'cinemom'       => array('display_name' => 'CineMá', 'version' => TRUE),
        'art'           => array('display_name' => 'Casa de Arte', 'version' => FALSE),
        'alt'           => array('display_name' => 'Espacio Alternativo', 'version' => FALSE),
        'alt_sports'    => array('display_name' => 'Espacio Alternativo Deportes', 'version' => FALSE),
        'nfl'           => array('display_name' => 'NFL', 'version' => FALSE),
        'preview'       => array('display_name' => 'Pre-estreno', 'version' => FALSE),
        'exclusive'     => array('display_name' => 'Exclusiva', 'version' => FALSE),
        'presale'       => array('display_name' => 'Pre-venta', 'version' => FALSE),
        'rerelease'     => array('display_name' => 'Re-estreno', 'version' => FALSE),
        'fest'          => array('display_name' => 'Festivales', 'version' => FALSE),
        'classics'      => array('display_name' => 'Clásicos', 'version' => FALSE),
        'oscar_nominee' => array('display_name' => 'Nominada al Oscar', 'version' => FALSE),
        'oscar_winner'  => array('display_name' => 'Ganadora del Oscar', 'version' => FALSE),
    );

    if ( !is_array($filter) || !sizeof($filter) ) {
      return $result;
    }

    $result = array_filter($result, function($item) use($filter) {
      $match = FALSE;
      foreach ( $filter as $k => $v ) {
        $match = ( $item[$k] === $v );
      }
      return $match;
    });

    return $result;
  }


  /**
	 * Replace URLs, mentions and hashtags with <a/> links.
	 *
	 * @param string $status_text
	 * @return string
	 */
	public static function linkify_twitter_status( $status_text ) {
		// linkify URLs
		$status_text = preg_replace(
			'/(https?:\/\/\S+)/',
			'<a href="\1" target="_blank" rel="nofollow">\1</a>',
			$status_text
		);

		// linkify twitter users
		$status_text = preg_replace(
			'/(^|\s)(@\w+)/',
			'\1<a href="http://twitter.com/\2" target="_blank">\2</a>',
			$status_text
		);

		// linkify tags
		$status_text = preg_replace(
			'/(^|\s)#([\wáéíóúñ]+)/i',
			'\1<a href="http://twitter.com/search?q=%23\2&src=hash" target="_blank">#\2</a>',
			$status_text
		);

		return $status_text;
	}


  public static function confirmation_get_screenname($transaction) {
    $session = $transaction->getSession();
    $result  = $session->getData('SCREENNAME') . ' ';
    if ( $session->getCinema()->getPlatinum() ) {
      $result .= 'Platino';
    } elseif ( $session->getPremium() ) {
      $result .= 'Premium';
    } elseif ( $session->getExtreme() ) {
      $result .= 'CinemeXtremo';
    } elseif ( $session->getMovie()->getAttr('v4d') ) {
      $result .= 'X4D';
    } else {
      $result .= 'Tradicional';
    }
    return $result;
  }


  public static function confirmation_get_ticketslist($transaction, $glue) {
    $tickets = array();
    foreach ( $transaction->getTickets() as $ticket ) {
      $tickets[] = $ticket->qty . ' ' . $ticket->name;
    }
    return implode( $glue, $tickets );
  }


  /**
   *
   * @param \SocialSnack\FrontBundle\Entity\Transaction $transaction
   * @param string $glue
   * @return type
   */
  public static function confirmation_get_seatslist($transaction, $glue) {
    return implode( $glue, explode( ',', $transaction->getRealSeats() ) );
  }


  public static function format_date($date, $format) {
    $formatter = new \IntlDateFormatter( \Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC' );
    $formatter->setPattern($format);
    return ucfirst( $formatter->format($date) );
  }


  public static function get_session_nice_date($date) {
    $formatter = new \IntlDateFormatter( \Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC' );
    $formatter->setPattern( 'EEEE dd \'de\' LLLL' );
    return ucfirst( $formatter->format($date) );
  }

  public static function shorten($str, $len, $add = '...') {
    if ( strlen($str) <= $len )
      return $str;

    $str = substr($str, 0, $len) . $add;
    return $str;
  }


  /**
   *
   * @param array $args
   * @param mixed $entity Some entity with a getState() method or an State entity.
   * @param array $available_dates
   * @return \DateTime
   */
  public static function get_billboard_date( $args, $entity, $available_dates = NULL ) {
    $today = new \DateTime( 'today' );

    switch ( get_class($entity) ) {
      case 'SocialSnack\WsBundle\Entity\State':
        $timezone = $entity->getTimezone();
        break;
      default:
        $timezone = $entity->getState()->getTimezone();
        break;
    }
    if (isset($args[ 'date' ])) {
      $date = new \DateTime( $args['date'] );
    } elseif ($available_dates) {
      $date = $available_dates[0]['date'];
    } else {
      $date = $today;
    }

    if ( $date->format('Y-m-d') == $today->format('Y-m-d') ) {
      $date = new \DateTime('now', new \DateTimeZone('America/Mexico_City'));

      if ( $timezone ) {
        $date->setTimezone(new \DateTimeZone($timezone));
      }
    }

    return $date;
  }


  /**
   * URL is mobile?
   *
   * @param string $uri
   * @return boolean
   */
  public static function is_mobile( $uri ) {
    return preg_match('/^https?\:\/\/m\.[a-zA-Z\.]+\/(app_dev\.php\/)?.+/', $uri);
  }


  public static function get_movie_sessions_block_data($item, $platinum = FALSE) {
    $data = '';

    // CinemeXtremo
    if ($item['movie']->getAttr('extreme')) {
      $data .= ' data-cx="1"';
    }
    // Platino
    if ($item['movie']->getAttr('platinum')) {
      $data .= ' data-platinum="1"';
    }
    // Premium
    if ($item['movie']->getAttr('premium')) {
      $data .= ' data-premium="1"';
    }
    // X4D
    if ($item['movie']->getAttr('v4d')) {
      $data .= ' data-v4d="1"';
    }
    // 3D
    if ($item['movie']->getAttr('v3d')) {
      $data .= ' data-v3d="1"';
    }

    if ($data === '') {
      $data = ' data-trad="1"';
    }

    // Language attributes:
    // Spanish
    if ($item['movie']->getAttr('lang_es')) {
      $data .= ' data-lang-es="1"';
    }
    // English
    if ($item['movie']->getAttr('lang_en')) {
      $data .= ' data-lang-en="1"';
    }

    return $data;
  }


  /**
   * @param $array
   * @param $path
   * @return mixed
   */
  public static function get_array_path($array, $path) {
    $c = function($v, $w) {
      return $w ? (isset($v[$w]) ? $v[$w] : NULL) : $v;
    };
    return array_reduce(preg_split('~\[\'|\'\]~', $path), $c, $array);
  }

}
