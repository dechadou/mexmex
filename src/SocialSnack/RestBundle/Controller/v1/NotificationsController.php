<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NotificationsController extends BaseController {

	/**
	 * 
	 * @Route("/", name="rest_v1_notification_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Notifications:default', ['request' => $request]);
	}


  /**
   * @Route("/{id}", name="rest_v1_notificaation_detail")
   * @Method({"GET"})
   */
  public function targetAction($id, Request $request) {
    return $this->forward('SocialSnackRestBundle:Notifications:target', ['id' => $id, 'request' => $request]);
  }


}