<?php

namespace SocialSnack\WsBundle\Tests\Request;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;

class MuestraCatalogosTest extends LiipWebTestCase {

  public function testConstructor() {
    $ws = $this->getContainer()->get('cinemex_ws');
    $res = $ws->init('Arco\MuestraCatalogos');
    $this->assertEquals($ws, $res);
  }

}