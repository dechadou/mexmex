<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\FrontBundle\Entity\LandingPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class LandingPageType extends AbstractType {


  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder->add('type', 'choice', [
        'choices' => [
            LandingPage::TYPE_IMAGE   => 'Imagen',
            LandingPage::TYPE_HTML    => 'HTML',
            LandingPage::TYPE_CINEMAS => 'Cines',
            LandingPage::TYPE_MOVIES  => 'Películas',
            LandingPage::TYPE_PROMOS  => 'Promos',
        ]
    ]);
    $builder->add('name', 'text', ['label' => 'Nombre']);
    $builder->add('position', 'number', ['label' => 'Orden']);
    $builder->add('active', 'checkbox', [
        'label'    => 'Activa',
        'required' => FALSE,
    ]);

    $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'postSetData']);
    $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'postSubmit']);
  }


  public function postSetData(FormEvent $event) {
    $form = $event->getForm();
    $page = $event->getData();

    switch ($form->get('type')->getData()) {
      case LandingPage::TYPE_HTML:
        $form->add('html', 'textarea', [
            'label'  => 'HTML',
            'mapped' => FALSE,
            'data'   => isset($page->getContent()['html']) ? $page->getContent()['html'] : '',
            'attr'   => ['class' => 'form-control-code'],
        ]);
        break;

      case LandingPage::TYPE_IMAGE:
        $form->add('image', 'file', [
            'label'  => 'Imagen',
            'mapped' => FALSE,
            'required' => !isset($page->getContent()['image']),
            'image_preview_path' => 'content[image]',
            'image_preview_size' => '',
        ]);
        break;

      case LandingPage::TYPE_CINEMAS:
        $form->add('cinemas', 'text', [
            'label'  => 'Cines',
            'mapped' => FALSE,
            'data'   => isset($page->getContent()['cinemas']) ? $page->getContent()['cinemas'] : '',
        ]);
        break;

      case LandingPage::TYPE_MOVIES:
        $form->add('movies', 'text', [
            'label'  => 'Películas',
            'mapped' => FALSE,
            'data'   => isset($page->getContent()['movies']) ? $page->getContent()['movies'] : '',
        ]);
        break;

      case LandingPage::TYPE_PROMOS:
        $form->add('promos', 'collection', [
            'type'   => new LandingPromoType(),
            'label'  => 'Promos',
            'mapped' => FALSE,
            'allow_add' => TRUE,
            'data'   => isset($page->getContent()['promos']) ? $page->getContent()['promos'] : [],
        ]);
        break;
    }
  }


  public function postSubmit(FormEvent $event) {
    $form = $event->getForm();
    $page = $event->getData();

    switch ($form->get('type')->getData()) {
      case LandingPage::TYPE_HTML:
        $page->setContent(['html' => $form->has('html') ? $form->get('html')->getData() : '']);
        break;

      case LandingPage::TYPE_IMAGE:
        // Store the image and set the path.
        break;

      case LandingPage::TYPE_CINEMAS:
        $page->setContent(['cinemas' => $form->has('cinemas') ? $form->get('cinemas')->getData() : '']);
        break;

      case LandingPage::TYPE_MOVIES:
        $page->setContent(['movies' => $form->has('movies') ? $form->get('movies')->getData() : '']);
        break;

      case LandingPage::TYPE_PROMOS:
        if ($form->has('promos')) {
          foreach ($form->get('promos')->getData as $promo) {
            // Resize and upload files
            // Store paths
          }
        }
        break;

    }
  }


  public function getName() {
    return '';
  }

}