<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\PromoImportant;
use SocialSnack\AdminBundle\Form\Type\PromoImportantType;

class PromoImportantController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="social_snack_promoimportant_list")
   * @Route("/inactive", name="social_snack_promoimportant_list_inactive", defaults={"active"=false})
   *
   * @param bool $active
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction($active = TRUE) {
    $this->check_roles();

    $repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:PromoImportant');
    $promos = $repo->findBy(['status' => $active], ['sequence' => 'ASC']);

    return $this->render('SocialSnackAdminBundle:PromoImportant:list.html.php', array(
        'active' => $active,
        'promos' => $promos,
    ));
  }


  /**
   * @Route("/add", name="social_snack_promoimportant_add")
   * @Route("/{promo_id}", name="social_snack_promoimportant_edit")
   *
   * @param null    $promo_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function editAction($promo_id = NULL, Request $request) {
    $this->check_roles();
    
    $session = $request->getSession();
    $purge = NULL;
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:PromoImportant');
    
    if (!is_null($promo_id)) {
      $promo = $repo->find($promo_id);
    } else {
      $promo = new PromoImportant();
    }
    
    $form = $this->createForm(new PromoImportantType, $promo);
    $form->handleRequest($request);

    if ($form->isValid()) {
      $em->persist($promo);
      $em->flush();
      
      $base_path = 'promosimportant/';

      if ($file = $form["file_thumb"]->getData()) {
        $filename = $promo->getId() . "-promoimportant-thumb.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 490, 116);
        $purge_files[] = '/' . $this->container->getParameter('cms_upload_path') . $base_path . $filename;
        $promo->setThumb($base_path . $filename);
      }
      
      if ($file = $form["file_image"]->getData()) {
        $filename = $promo->getId() . "-promoimportant-image.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 700, 573);
        $purge_files[] = '/' . $this->container->getParameter('cms_upload_path') . $base_path . $filename;
        $promo->setImage($base_path . $filename);
      } elseif (isset($form['file_image_remove']) && $form['file_image_remove']->getData()) {
        // @todo Delete the image file.
        $promo->setImage('');
      }

      $em->flush();

      $purger = $this->get('ss.cache_purger');
      $purge = $purger->purge_cache(array('entity' => $promo));
      
      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);
      return $this->redirect($this->generateUrl(
          'social_snack_promoimportant_edit',
          array('promo_id' => $promo->getId())
      ));
    }

    return $this->render('SocialSnackAdminBundle:PromoImportant:edit.html.php', array(
        'promo' => $promo,
        'form'  => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/delete/{promo_id}", name="social_snack_promoimportant_delete")
   * @param $promo_id
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteAction($promo_id) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:PromoImportant');
    $promo = $repo->find($promo_id);
    $em->remove($promo);
    $em->flush();
    return $this->redirect($this->generateUrl('social_snack_promoimportant_list'));
  }

}
