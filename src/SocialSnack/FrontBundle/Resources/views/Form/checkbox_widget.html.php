<label class="check-row">
  <input type="checkbox" <?php echo $view['form']->block($form,'widget_attributes');
    if (isset($value)) : ?> value="<?php echo $value; ?>"<?php endif;
    if ($checked) : ?> checked="checked"<?php endif;
    if ($required) : ?> data-validators="not_empty"<?php endif; ?>/>
</label>
