<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Symfony\Component\Routing\Router;

class PurgeHomepage implements UrlTaskInterface, DoctrineTaskInterface {

  protected $router;

  public function __construct(Router $router) {
    $this->router   = $router;
  }


  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('home'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('esi_js_stuff'),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'movie_findby_..0.0.0',
                'movie_findby_..0.0.1',
                'MovieComingRepository:findNextFriday',
                'MovieComingRepository:findFurther',
            ],
            'env' => 'front'
        ]
    ];
  }

}