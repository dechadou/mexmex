<?php

namespace SocialSnack\WsBundle\Tests\Request;

use SocialSnack\WsBundle\Request\Consulta;

/**
 * Class ConsultaTest
 * @package SocialSnack\WsBundle\Tests\Request
 * @author Guido Kritz
 */
class ConsultaTest extends WebTestCase {

  protected function get_ws_method() {
    return 'Consulta';
  }

  public function testErrorResponseWithMessage() {
    // @todo Replace this XML with an actual XML found in a real world example. I'm not sure if this is the right response format.
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;MENSAJE&gt;FooBar&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\RequestMsgException');

    self::$ws->request(array(
        'TipoConsulta' => 1
    ));
  }

  public function testUnexpectedResponse() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;MENSAJE&gt;FooBar&lt;/MENSAJE&gt;&lt;/CMXXML&gt;</string>');

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\UnexpectedWsResponseException');

    self::$ws->request(array(
        'TipoConsulta' => 1
    ));
  }

  public function testConsultaPeliculasSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;PELICULA&gt;&lt;CINE&gt;1044&lt;/CINE&gt;&lt;CINENAME&gt;Cinemex Antara&lt;/CINENAME&gt;&lt;MOVIE&gt;HO00015294&lt;/MOVIE&gt;&lt;MOVIENAME&gt;&lt;![CDATA[50 SOMBRAS DE GREY DIG ING]]&gt;&lt;/MOVIENAME&gt;&lt;MOVIESHORTNAME&gt;&lt;![CDATA[50 SOMBRAS DE GREY D]]&gt;&lt;/MOVIESHORTNAME&gt;&lt;RATING&gt;s/c&lt;/RATING&gt;&lt;DURACION&gt;106&lt;/DURACION&gt;&lt;POSTERMOBILE1&gt;&lt;/POSTERMOBILE1&gt;&lt;FILMCODE&gt;20284&lt;/FILMCODE&gt;&lt;/PELICULA&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>'
    );

    $tipo_consulta = 1;
    $this->assertEquals($tipo_consulta, Consulta::TIPOCONSULTA_PELICULAS);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta
    ));

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    foreach ($res as $movie) {
      $this->assertInstanceOf('\SimpleXMLElement', $movie);
      $this->assertObjectHasAttribute('MOVIE', $movie);
      break; // Test just one element of the array.
    }
  }

  public function testConsultaSesionesSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;SESION&gt;&lt;CINE&gt;1233&lt;/CINE&gt;&lt;MOVIE&gt;HO00015143&lt;/MOVIE&gt;&lt;SESSIONID&gt;12278&lt;/SESSIONID&gt;&lt;SESSIONDATE&gt;27/11/2014 02:35:00 p.m.&lt;/SESSIONDATE&gt;&lt;SEATSAVAILABLE&gt;139&lt;/SEATSAVAILABLE&gt;&lt;SEATALLOCATIONON&gt;Y&lt;/SEATALLOCATIONON&gt;&lt;PRICECODE&gt;$650&lt;/PRICECODE&gt;&lt;CHILDALLOWED&gt;Y&lt;/CHILDALLOWED&gt;&lt;CINECODE&gt;CNA&lt;/CINECODE&gt;&lt;FORMATVIDEO&gt;DIGITAL&lt;/FORMATVIDEO&gt;&lt;IDIOMA&gt;Inglés&lt;/IDIOMA&gt;&lt;SCREEN&gt;10&lt;/SCREEN&gt;&lt;SCREENNAME&gt;Sala 10&lt;/SCREENNAME&gt;&lt;SCREENTYPE&gt;Convencional&lt;/SCREENTYPE&gt;&lt;/SESION&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>'
    );

    $tipo_consulta = 9;
    $this->assertEquals($tipo_consulta, Consulta::TIPOCONSULTA_SESIONES);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta
    ));

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    foreach ($res as $movie) {
      $this->assertInstanceOf('\SimpleXMLElement', $movie);
      $this->assertObjectHasAttribute('SESSIONID', $movie);
      break; // Test just one element of the array.
    }
  }

  public function testConsultaPreciosSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;PRECIO&gt;&lt;CINE&gt;1044&lt;/CINE&gt;&lt;TICKETTYPECODE&gt;0001&lt;/TICKETTYPECODE&gt;&lt;TICKETTYPEDESCRIPTION&gt;Adulto&lt;/TICKETTYPEDESCRIPTION&gt;&lt;PRICECODE&gt;$064&lt;/PRICECODE&gt;&lt;TICKETPRICE&gt;7500&lt;/TICKETPRICE&gt;&lt;HOCODE&gt;1&lt;/HOCODE&gt;&lt;/PRECIO&gt;&lt;PRECIO&gt;&lt;CINE&gt;1044&lt;/CINE&gt;&lt;TICKETTYPECODE&gt;0001&lt;/TICKETTYPECODE&gt;&lt;TICKETTYPEDESCRIPTION&gt;Adulto&lt;/TICKETTYPEDESCRIPTION&gt;&lt;PRICECODE&gt;$066&lt;/PRICECODE&gt;&lt;TICKETPRICE&gt;7500&lt;/TICKETPRICE&gt;&lt;HOCODE&gt;1&lt;/HOCODE&gt;&lt;/PRECIO&gt;&lt;PRECIO&gt;&lt;CINE&gt;1044&lt;/CINE&gt;&lt;TICKETTYPECODE&gt;0001&lt;/TICKETTYPECODE&gt;&lt;TICKETTYPEDESCRIPTION&gt;Adulto&lt;/TICKETTYPEDESCRIPTION&gt;&lt;PRICECODE&gt;$067&lt;/PRICECODE&gt;&lt;TICKETPRICE&gt;7500&lt;/TICKETPRICE&gt;&lt;HOCODE&gt;1&lt;/HOCODE&gt;&lt;/PRECIO&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>'
    );

    $tipo_consulta = 3;
    $this->assertEquals($tipo_consulta, Consulta::TIPOCONSULTA_PRECIOS);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta
    ));

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    foreach ($res as $movie) {
      $this->assertInstanceOf('\SimpleXMLElement', $movie);
      $this->assertObjectHasAttribute('TICKETTYPECODE', $movie);
      break; // Test just one element of the array.
    }
  }

  public function testConsultaEstadosYZonasSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;ESTADOSYZONAS&gt;&lt;CODSTATE&gt;1&lt;/CODSTATE&gt;&lt;STATENAME&gt;AGUASCALIENTES&lt;/STATENAME&gt;&lt;CODZONE&gt;1&lt;/CODZONE&gt;&lt;ZONENAME&gt;AGUASCALIENTES&lt;/ZONENAME&gt;&lt;/ESTADOSYZONAS&gt;&lt;ESTADOSYZONAS&gt;&lt;CODSTATE&gt;2&lt;/CODSTATE&gt;&lt;STATENAME&gt;BAJA CALIFORNIA&lt;/STATENAME&gt;&lt;CODZONE&gt;1&lt;/CODZONE&gt;&lt;ZONENAME&gt;MEXICALI&lt;/ZONENAME&gt;&lt;/ESTADOSYZONAS&gt;&lt;ESTADOSYZONAS&gt;&lt;CODSTATE&gt;2&lt;/CODSTATE&gt;&lt;STATENAME&gt;BAJA CALIFORNIA&lt;/STATENAME&gt;&lt;CODZONE&gt;2&lt;/CODZONE&gt;&lt;ZONENAME&gt;TIJUANA&lt;/ZONENAME&gt;&lt;/ESTADOSYZONAS&gt;&lt;ESTADOSYZONAS&gt;&lt;CODSTATE&gt;3&lt;/CODSTATE&gt;&lt;STATENAME&gt;BAJA CALIFORNIA SUR&lt;/STATENAME&gt;&lt;CODZONE&gt;1&lt;/CODZONE&gt;&lt;ZONENAME&gt;CABO SAN LUCAS&lt;/ZONENAME&gt;&lt;/ESTADOSYZONAS&gt;&lt;ESTADOSYZONAS&gt;&lt;CODSTATE&gt;3&lt;/CODSTATE&gt;&lt;STATENAME&gt;BAJA CALIFORNIA SUR&lt;/STATENAME&gt;&lt;CODZONE&gt;2&lt;/CODZONE&gt;&lt;ZONENAME&gt;LA PAZ&lt;/ZONENAME&gt;&lt;/ESTADOSYZONAS&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>'
    );

    $tipo_consulta = 5;
    $this->assertEquals($tipo_consulta, Consulta::TIPOCONSULTA_ESTADOS_ZONAS);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta
    ));

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    foreach ($res as $movie) {
      $this->assertInstanceOf('\SimpleXMLElement', $movie);
      $this->assertObjectHasAttribute('ZONENAME', $movie);
      break; // Test just one element of the array.
    }
  }

} 