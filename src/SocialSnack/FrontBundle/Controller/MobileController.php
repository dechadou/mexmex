<?php

namespace SocialSnack\FrontBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SocialSnack\FrontBundle\Exception\CustomHttpException;
use SocialSnack\WsBundle\Entity\Cinema;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use SocialSnack\FrontBundle\Entity\Contact;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Entity\Movie;
use Endroid\QrCode\QrCode;

class MobileController extends BaseController {


  /**
   * @param Request $request
   * @return null|RedirectResponse
   */
  protected function getMobileLandingRedirect(Request $request) {
    $ua = $this->get('ss.front.utils')->getMobileOS($request->server->get('HTTP_USER_AGENT'));

    //Check user agent
    switch ($ua) {
      case 'ios':
        return new RedirectResponse($this->generateUrl('appios'));
        break;

      case 'android':
        return new RedirectResponse($this->generateUrl('appandroid'));
        break;

      case 'windows':
        return new RedirectResponse($this->generateUrl('appwindows'));
        break;
    }

    return NULL;
  }


  /**
   * @Route("/apps/")
   * @param Request $request
   * @return null|RedirectResponse
   */
  public function appsAction(Request $request) {
    $response = $this->getMobileLandingRedirect($request);
    if ($response instanceof RedirectResponse) {
      return $response;
    }

    return new RedirectResponse($this->generateUrl('mobile_index'));
  }


  /**
   * @Route("/", name="mobile_index")
   * @param Request $request
   * @return RedirectResponse
   */
  public function indexAction(Request $request) {
    // Redirect to download app landing?
    $skip_landing = $request->cookies->get('skip_store_landing');
    if (!$skip_landing) {
      $response = $this->getMobileLandingRedirect($request);
      if ($response instanceof RedirectResponse) {
        return $response;
      }
    }

    // If cinema cookie present, redirect to cinema billboard
    $m_cinema_id = $request->cookies->get('m_cinema_id');
    if ($m_cinema_id) {
      $url = $this->generateUrl('mobile_billboard', array('cinema_id' => $m_cinema_id));
    } else {
      $url = $this->generateUrl('mobile_location');
    }

    $response = new RedirectResponse($url);
    return $response;
  }


  /**
   * @Route("/skip_store_landing", name="skip_store_landing")
   */
  public function skipStoreLandingAction(Request $request) {
    $redirect_url = $request->query->has('redirect_url')
        ? $request->query->get('redirect_url')
        : $this->generateUrl('mobile_index') . '?redirect'
    ;

    $response = new RedirectResponse($redirect_url);
    $response->headers->setCookie(new Cookie('skip_store_landing', '1',time() + 2 * 24 * 60 * 60));
    return $response;
  }


  /**
   * @Route("/ubicacion", name="mobile_location")
   */
  public function locationAction() {
    // Dialog to select cinema

    $doctrine      = $this->get( 'doctrine' );
		$state_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
		$states        = $state_repo->findAllWithAreasAndCinemas();

    $response = $this->render('SocialSnackFrontBundle:Mobile:index.html.php', array(
        'states' => $states,
    ));
    $response->setPublic();
    $response->setMaxAge( 60 * 60 );
    $response->setSharedMaxAge( 24 * 60 * 60 );
    return $response;
  }


  /**
   * Set a cookie with the selected cinema ID and redirect to the cinema's
   * billboard.
   *
   * @Route("/setCinema", name="mobile_set_cinema")
   * @Method({"POST"})
   */
  public function setCinemaPostAction() {
    $cinema_id = $this->getRequest()->get('cinema');
    $response = new RedirectResponse($this->generateUrl('mobile_billboard', array( 'cinema_id' => $cinema_id )));
    $response->headers->setCookie(new Cookie('m_cinema_id', $cinema_id, 0, '/', NULL, FALSE, FALSE));
    return $response;
  }


  /**
   * @Route("/cartelera/{cinema_id}/{arguments}",
   *   name="mobile_billboard",
   *   requirements={"cinema_id"="\d+", "arguments"=".*"},
   *   defaults={"arguments"=""}
   * )
   * @ParamConverter("cinema", class="SocialSnackWsBundle:Cinema", options={"id" = "cinema_id"})
   */
  public function billboardAction(Cinema $cinema, $arguments = '') {
    // List movies with sessions in this cinema with args.

    // Parse URL arguments.
    $args   = FrontHelper::parse_url_arguments( $arguments );
    $single = isset( $args[ 'single' ] );

    // DB stuff.
    $session_repo    = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Session' );
    $cinema_repo     = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
    $movie_repo      = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
    $available_dates = $session_repo->findAvailableDatesByCinema($cinema->getId());

    /* @var $session_repo \SocialSnack\WsBundle\Entity\SessionRepository */
    /* @var $cinema_repo  \SocialSnack\WsBundle\Entity\CinemaRepository  */
    /* @var $movie_repo   \SocialSnack\WsBundle\Entity\MovieRepository   */

    //$this->_sidebar_check_disabled($cinema, $single);

    // Get sessions based on date.
    $date = FrontHelper::get_billboard_date($args, $cinema, $available_dates);

    $sessions = $movie_repo->findAllSessionsInCinemaByDate( $cinema->getId(), $date );

    // PLATINO
    $cinema_platinum = NULL;
    $cinema_standard = NULL;

    if ($cinema->getPlatinum() === FALSE) {
      // Por coordenadas
      $cinema_platinum = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => TRUE,
          'active'   => TRUE,
      ));
    } else {
      $cinema_standard = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => FALSE,
          'active'   => TRUE,
      ));
    }

    $response = new Response();
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 15 * 60 );

    // Render the view.
    $response->setContent( $this->renderView( 'SocialSnackFrontBundle:Mobile:billboard.html.php', array(
        'date'            => $date,
        'cinema'          => $cinema,
        'sessions'        => $sessions,
        'single'          => $single,
        'available_dates' => $available_dates,
        'cinema_platinum' => $cinema_platinum,
        'cinema_standard' => $cinema_standard,
    ) ) );

    return $response;
  }


  /**
   * @Route("/pelicula/{movie_id}/{cinema_id}/{arguments}",
   *   name="mobile_movie",
   *   requirements={"movie_id"="\d+", "cinema_id"="\d+", "arguments"=".*"},
   *   defaults={"arguments"=""}
   * )
   * @Route("/pelicula/{movie_id}/{arguments}",
   *   name="mobile_movie_alt",
   *   requirements={"movie_id"="\d+", "arguments"=".*"},
   *   defaults={"arguments"="", "cinema_id"=null}
   * )
   * @ParamConverter("movie",  class="SocialSnackWsBundle:Movie",  options={"id" = "movie_id"})
   */
  public function movieSingleAction(Movie $movie, $cinema_id, $arguments) {
    // Show movie details
    // List movie sessions in cinema with args.

    // Parse URL arguments.
		$args   = FrontHelper::parse_url_arguments( $arguments );

		$movie_repo    = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$cinema_repo   = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );

    if ( $parent = $movie->getParent() ) {
      return $this->redirect($this->container->get('ss.front.utils')->get_permalink($parent));
    }

    if ($cinema_id) {
      $cinema = $cinema_repo->find( $cinema_id );

      if (!$cinema) {
        throw $this->createNotFoundException();
      }

      // Find closest cinemas.
      /* @var $cc \SocialSnack\WsBundle\Entity\CinemaDistance */
      $closer = array();
      foreach ( $cinema->getClosestCinemas() as $cc ) {
        $closer[$cc->getToCinema()->getId()] = $cc->getDistance();
      }
      asort($closer);
      $closer_ids = array_slice(array_keys($closer), 0, 10);

      // Find dates with sessions to display.
      $available_dates = $movie_repo->findAvailableDatesInCinema( $movie, $cinema_id );

      // Get sessions based on date.
      $date = FrontHelper::get_billboard_date($args, $cinema, $available_dates);

      $sessions  = $movie_repo->findMovieSessionsInCinemaByDate( $movie, $cinema->getId(), $date );
    } else {
      $cinema = NULL;
      $sessions = array();
      $closer_ids = NULL;
      $date = NULL;
      $available_dates = NULL;
    }

		if ( $closer_ids ) {
			$closer_cinemas = $cinema_repo->findHavingSessionsForDate( $date, array('movie' => $movie, 'cinema_ids' => $closer_ids), 0, NULL );
		} else {
			$closer_cinemas = NULL;
		}
    if ( $closer_cinemas ) {
      $ordered = array();
      foreach ( $closer as $key => $value ) {
        foreach ( $closer_cinemas as $cc ) {
          if ( $key == $cc->getId() ) {
            $ordered[] = $cc;
            unset( $closer[ $key ] );
            break;
          }
        }
      }
      $closer_cinemas = $ordered;
    }
    $response = $this->render(
        'SocialSnackFrontBundle:Mobile:movieSingle.html.php',
        array(
            'movie'    => $movie,
            'cinema'   => $cinema,
            'closer_cinemas' => $closer_cinemas,
            'sessions' => $sessions,
            'date' => $date,
            'available_dates' => $available_dates
        )
    );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 ); // 15 minutes.
    $response->setSharedMaxAge( 15 * 60 ); // 15 minutes.
    return $response;
  }


  /**
	 * @Route("/checkout/{session_id}/", requirements={"session_id" = "[0-9]+"})
	 * @Template(engine="php")
	 */
	public function checkoutAction( $session_id ) {
    $response = new RedirectResponse($this->generateUrl('mobile_checkout', array('session_id' => $session_id)));
    return $response;
  }

  /**
	 * @Route(
	 *   "/cine/{cinema_id}/{slug}",
	 *   name="mobile_cinema",
	 *   requirements={"id" = "[0-9]+"},
	 *   defaults={"slug" = ""}
	 * )
   * @ParamConverter("cinema", class="SocialSnackWsBundle:Cinema", options={"id" = "cinema_id"})
	 * @Template(engine="php")
	 */
	public function cinemaSingleAction(Cinema $cinema) {
    $cinema_repo = $this->get( 'doctrine' )->getRepository('SocialSnackWsBundle:Cinema');
    $movie_repo  = $this->get( 'doctrine' )->getRepository('SocialSnackWsBundle:Movie');

    $movies = $movie_repo->findMoviesInCinema($cinema->getId());

    // PLATINO
    $cinema_platinum = NULL;
    $cinema_standard = NULL;

    if ($cinema->getPlatinum() === FALSE) {
      // Por coordenadas
      $cinema_platinum = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => TRUE,
          'active'   => TRUE,
      ));
    } else {
      $cinema_standard = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => FALSE,
          'active'   => TRUE,
      ));
    }

    $response = $this->render('SocialSnackFrontBundle:Mobile:cinemaSingle.html.php', array(
        'movies'          => $movies,
        'cinema'          => $cinema,

        'cinema_standard' => $cinema_standard,
        'cinema_platinum' => $cinema_platinum
    ));

    $response->setPublic();
    $response->setMaxAge( 2 * 60 * 60 ); // 2 hours.
    $response->setSharedMaxAge( 24 * 60 * 60 ); // 1 day.
    return $response;
  }

  /**
   * @Route("/login", name="mobile_login")
   * @Method({"GET"})
   */
  public function loginAction() {
    $request = $this->getRequest();
    $flash_msgs = $this->get_flash_bag();

    // Logout the current user;
    if ( $request->get('force-logout') ) {
      $this->get('security.context')->setToken(null);
      $this->get('request')->getSession()->invalidate();
    }

    $redirect_to = $request->get('redirect_to');

    $response = $this->render('SocialSnackFrontBundle:Mobile:login.html.php',
        array(
            'target_path' => $redirect_to,
            'flash_msgs'  => $flash_msgs
        )
    );
    $response->setPublic();
    $response->setMaxAge( 60 * 60 );
    $response->setSharedMaxAge( 24 * 60 * 60 );
    return $response;
  }


  /**
   * @Route("/privacidadcompra", name="mobile_checkout_privacy_page")
   * @Template(engine="php")
   */
  public function checkoutPrivacyAction() {
    $response = $this->render(
        'SocialSnackFrontBundle:Mobile:checkoutPrivacy.html.php',
				array(
            'route' => $this->getRequest()->attributes->get('_route'),
				)
    );
    $response->setPublic();
    $response->setMaxAge( 2 * 60 * 60 ); // 2 hours.
    $response->setSharedMaxAge( 7 * 24 * 60 * 60 ); // 1 week.
    return $response;
  }


  /**
   * contactAction function
   *
   * @Route("/contacto", name="mobile_contact")
   * @Method({"GET"})
   * @Template(engine="php")
   *
   * @access public
   * @return void
   */
  public function contactAction() {
    $doctrine = $this->getDoctrine();

    $cinemas  = $doctrine->getRepository('SocialSnackWsBundle:Cinema')
        ->findBy(['active' => TRUE], ['name' => 'ASC']);

    $response = $this->render('SocialSnackFrontBundle:Mobile:contact.html.php', array(
        'route'   => $this->getRequest()->attributes->get('_route'),
        'cinemas' => $cinemas
      ));

    $response->setPublic();
    $response->setSharedMaxAge(24 * 60 * 60);
    $response->setMaxAge(60 * 60);

    return $response;
  }


  /**
   * @Route("/contacto", name="mobile_ontact_post")
   * @Template(engine="php")
   * @Method({"POST"})
   */
  public function contactPostAction() {
    $request  = $this->getRequest();
    $em       = $this->getDoctrine()->getManager();
    $fields   = array( 'name', 'email', 'phone', 'ie', 'cinema', 'service', 'type', 'msg' );
    $required = array( 'name', 'email', 'msg' );
    $valid    = TRUE;
    $success  = FALSE;

    foreach ( $required as $field ) {
      if ( !$request->get( $field ) ) {
        $valid = FALSE;
        break;
      }
    }

    if ( !$valid )
      return new JsonResponse( array(), 400 );

    $contact = new Contact();
    $contact->setEmail(   $request->get( 'email' ) );
    $contact->setMsg(     $request->get( 'msg' ) );
    $contact->setName(    $request->get( 'name' ) );
    $contact->setCinema(  $request->get( 'cinema' )  ? $request->get( 'cinema' )  : NULL );
    $contact->setIe(      $request->get( 'ie' )      ? $request->get( 'ie' )      : NULL );
    $contact->setPhone(   $request->get( 'phone' )   ? $request->get( 'phone' )   : NULL );
    $contact->setService( $request->get( 'service' ) ? $request->get( 'service' ) : NULL );
    $contact->setType(    $request->get( 'type' )    ? $request->get( 'type' )    : NULL );

    try {
      $em->persist( $contact );
      $em->flush();
      $success = TRUE;
    } catch ( \Exception $e ) {}

    if ( !$success )
      return new JsonResponse( array(), 400 );

    // Deliver the email.
    $this->get('cinemex.mailer')->enqueue( array(
        'type' => 'contact',
        'contact_id' => $contact->getId()
    ) );

    return new JsonResponse();
  }


  /**
   * @Route("/cartelera/{args}", defaults={"args" = ""}, requirements={"args" = ".*"})
   * @Route("/cines/{args}", defaults={"args" = ""}, requirements={"args" = ".*"})
   */
  public function notImplementedAction() {
    return new RedirectResponse($this->generateUrl('mobile_index'));
  }


  /**
   * @Route("/encuesta/{transaccion}", name="encuesta",
   *   requirements={"transaccion"=".+"}
   * )
   * @Template(engine="php")
   */
  public function pollAction($transaccion, Request $request) {
    return $this->forward('SocialSnackFrontBundle:Poll:poll', [
        'transaccion' => $transaccion
    ]);
  }


  /**
   * @Route("/restablecer-password/")
   * @Method({"GET"})
   * @Template(engine="php")
   */
  public function resetPassAction(Request $request) {
    /** @var \SocialSnack\RestBundle\Handler\AuthHandler $handler */
    $handler    = $this->get('rest.auth.handler');
    $flash_msgs = $this->get_flash_bag();
    $success    = $request->query->get('success');

    if (!$success) {
      $code = $request->query->get('code');
      $code = $handler->getResetPassCode($code);
    } else {
      $code = NULL;
    }

    if (!$code && !$success) {
      throw new CustomHttpException(400, 'Este enlace ya no es válido. Por favor, inicia el proceso nuevamente.');
    }

    return array(
        'no_cache_header_user' => TRUE,
        'flash_msgs'           => $flash_msgs,
        'code'                 => $code ? $code->getCode() : NULL,
        'success'              => $success,
    );
  }

  /**
   * @Route("/terminos")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function tosAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


}
