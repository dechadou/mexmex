<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Promo;
use SocialSnack\FrontBundle\Entity\PromoA;
use SocialSnack\FrontBundle\Entity\PromoIE;
use SocialSnack\FrontBundle\Entity\PromoImportant;

class LoadPromosData implements FixtureInterface {

  static $promos;
  static $promosA;
  static $promosIE;
  static $promosImportant;

  public function load(ObjectManager $manager) {
    self::$promos          = [];
    self::$promosA         = [];
    self::$promosIE        = [];
    self::$promosImportant = [];

    $promo = new Promo();
    $promo
        ->setTitle('Promo 1')
        ->setContent('Lorem ipsum')
        ->setImage('image.jpg')
        ->setSequence(1)
        ->setStatus(TRUE)
        ->setSubtitle('Promo 1 subtitle')
        ->setThumb('thumb.jpg')
        ->setUrl('http://www.google.com')
    ;
    self::$promos[] = $promo;
    $manager->persist($promo);

    $promoA = new PromoA();
    $promoA
        ->setUrl('http://www.google.com')
        ->setStatus(TRUE)
        ->setSequence(1)
        ->setImage('image.jpg')
        ->setTitle('Promo A 1')
    ;
    self::$promosA[] = $promoA;
    $manager->persist($promoA);

    $promoIE = new PromoIE();
    $promoIE
        ->setTitle('Promo IE 1')
        ->setImage('image.jpg')
        ->setActive(TRUE)
        ->setCategories(['gold', 'platinum'])
        ->setThumb('thumb.jpg')
    ;
    self::$promosIE[] = $promoIE;
    $manager->persist($promoIE);

    $promoImp = new PromoImportant();
    $promoImp
        ->setThumb('thumb.jpg')
        ->setImage('image.jpg')
        ->setTitle('Promo Important 1')
        ->setSequence(1)
        ->setStatus(TRUE)
        ->setTarget(['home'])
        ->setUrl('http://www.google.com')
    ;
    self::$promosImportant[] = $promoImp;
    $manager->persist($promoImp);


    $manager->flush();
  }

}