<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArcoEntry
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ArcoEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=30)
     */
    private $ref;
    
    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_submitted", type="datetime")
     */
    private $dateSubmitted;

    
    public function __construct() {
      $this->dateSubmitted = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return ArcoEntry
     */
    public function setData($data)
    {
        $this->data = (object)$data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set dateSubmitted
     *
     * @param \DateTime $dateSubmitted
     * @return ArcoEntry
     */
    public function setDateSubmitted($dateSubmitted)
    {
        $this->dateSubmitted = $dateSubmitted;
    
        return $this;
    }

    /**
     * Get dateSubmitted
     *
     * @return \DateTime 
     */
    public function getDateSubmitted()
    {
        return $this->dateSubmitted;
    }
    
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function jsonEncodeData() {
      if ( isset($this->data->CveDerechoAccDat) ) {
        $this->ref =
            $this->dateSubmitted->format('d/m/Y-h:i') .
            '-' .
            strtoupper(substr($this->data->CveDerechoAccDat, 0, 3));
      }
      if ( !is_string($this->data) ) {
        $this->data = json_encode($this->data);
      }
    }
    
    
    /**
     * @ORM\PostLoad
     * @ORM\PostUpdate
     * @ORM\PostPersist
     */
    public function jsonDecodeData() {
      if ( is_string($this->data) ) {
        $this->data = json_decode($this->data);
      }
    }
    

    /**
     * Set ref
     *
     * @param string $ref
     * @return ArcoEntry
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }
}