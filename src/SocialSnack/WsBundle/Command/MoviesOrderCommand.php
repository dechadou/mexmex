<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\MovieInfo;
use SocialSnack\WsBundle\Request\ConsultaDetallada;

class MoviesOrderCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:movies:order' )
			->setDescription( 'Calculate movies order' );
	}
	
	
	/**
	 * Fetch movies and movies info from the WS.
	 * 
	 * Since it doesn't seem to exist an effective method to retrieve the complete
	 * movie listing from the WS (all methods seems to return each movie repeated
	 * for every single cinema resulting on an unnecessary huge response with
	 * occasional timeouts) we are requesting movies by state to limit the size of
	 * the response.
	 * 
	 * @param \Symfony\Component\Console\Input\InputInterface $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 * @return type
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$time_start = microtime(true);
    
		$output->writeln( '*** Remember to group first! ***' );

		$doctrine   = $this->getContainer()->get( 'doctrine' );
    $conn       = $doctrine->getConnection();
    $sql        = "
        CREATE TEMPORARY TABLE `tmp`
        SELECT count( * ) as `count`, group_id
          FROM `Session` s
          WHERE s.`date` >= :date
          AND s.active = 1
          GROUP BY group_id;

        UPDATE `Movie` m
        	LEFT JOIN `tmp` t ON m.group_id = t.group_id
          SET m.`position` = t.`count`
          WHERE t.group_id = m.group_id;

        UPDATE `Movie` m
        	LEFT JOIN `tmp` t ON m.group_id = t.group_id
        	SET m.`position` = 0
        	WHERE t.group_id IS NULL;

        DROP TABLE tmp;";
    $stmt       = $conn->prepare($sql);
    $stmt->bindValue( 'date', date( 'Y-m-d' ) );
    $stmt->execute();
    
		$output->writeln( 'Done.' );
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		
		$output->writeln( 'Total Execution Time: '. $execution_time . ' Mins' );
		
		
		
		return;
	}
	
	
}