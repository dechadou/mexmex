<?php
/* @var $entry \SocialSnack\FrontBundle\Entity\ArcoEntry */
/* @var $data array */
?>
Fecha de solicitud: <?php echo $entry->getDateSubmitted()->format('d-m-Y'); ?>

Folio: <?php echo $entry->getRef(); ?>


<?php
$owner = array(
  'Primer Nombre' => $data->owner->PrimerNombre,
  'Segundo Nombre' => $data->owner->SegundoNombre,
  'Apellido Paterno' => $data->owner->ApellidoPaterno,
  'Apellido Materno' => $data->owner->ApellidoMaterno,
  'Fecha de nacimiento' => $data->owner->FechaNac,
  'Domicilio' => $data->owner->Domicilio,
  'Código Postal' => $data->owner->CP,
  'Estado' => $data->owner->Estado,
  'Identificación' => $data->owner->Identificacion,
  'Email' => $data->owner->Email,
  'Teléfono' => $data->owner->Telefono,
  'Horario de contacto' => $data->owner->Horario,
  'Anexo que acredite la identidad del titular' => $data->owner->attach_id->url,
);
$output = [];
foreach ( $owner as $label => $value ) {
  if ( $value ) {
    $output[] = sprintf('%s: %s', $label, $value);
  }
}
?>
<?php if ( sizeof($output) ) { ?>
Datos Del Titular
-----------------
<?php echo implode("\r\n", $output); ?>
<?php } ?>

<?php
$rep = array(
  'Primer Nombre' => $data->rep->first_name,
  'Segundo Nombre' => $data->rep->middle_name,
  'Apellido Paterno' => $data->rep->last_name,
  'Apellido Materno' => $data->rep->mlast_name,
  'Email' => $data->rep->email,
  'Identificación oficial' => $data->rep_attach->attach_id ? $data->rep_attach->attach_id->url : NULL,
  'Poder donde se acredite la representación del titular' => $data->rep_attach->attach_power ? $data->rep_attach->attach_power->url : NULL,
);
$output = [];
foreach ( $owner as $label => $value ) {
  if ( $value ) {
    $output[] = sprintf('%s: %s', $label, $value);
  }
}
?>

  
<?php if ( sizeof($output) ) { ?>
Representante Del Titular
-------------------------
<?php echo implode("\r\n", $output); ?>
<?php } ?>


Relación con Cinemex: <?php echo $data->Tipo_RelacionCMX; ?>
<?php
if ( $value = $data->NumEmpleado ) {
  echo "\r\nNúmero de empleado " . $value;
}
if ( $value = $data->NumIE ) {
  echo "\r\nNúmero de Invitado Especial " . $value;
}
if ( $value = $data->rel_provider ) {
  echo "\r\nCompañía " . $value;
}
if ( $value = $data->SolicitaTipo ) {
  echo "\r\nTipo de puesto " . $value;
}
if ( $value = $data->SolicitaComplejo ) {
  echo "\r\nComplejo " . $value;
}
if ( $value = $data->RFC ) {
  echo "\r\nRFC " . $value;
}
if ( $value = $data->RelacionOtro ) {
  echo "\r\n" . $value;
}
?>

Solicitud: <?php echo $data->CveDerechoAccDat; ?>
<?php
if ( $value = $data->Comentario ) {
  echo "\r\nComentarios: " . $value;
}
if ( $value = $data->DatoModificar ) {
  echo "\r\nDato a modificar: " . $value;
}
if ( $value = $data->DatoActual ) {
  echo "\r\nDato actual: " . $value;
}
if ( $value = $data->DatoNuevo ) {
  echo "\r\nActualizar por: " . $value;
}
if ( $data->attach_doc && $value = $data->attach_doc->url ) {
  echo "\r\nDocumentación adjunta: " . $value;
}
if ( $data->attach_doc2 && $value = $data->attach_doc2->url ) {
  echo "\r\nDocumentación adicional adjunta: " . $value;
}
?>