<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Form\Type\LandingPageType;
use SocialSnack\AdminBundle\Form\Type\LandingType;
use SocialSnack\FrontBundle\Entity\Landing;
use SocialSnack\FrontBundle\Entity\LandingPage;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class LandingController
 * @package SocialSnack\AdminBundle\Controller
 * @author Guido Kritz
 */
class LandingController extends BaseController {

  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="admin_landing_list")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction(Request $request) {
    $landings = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Landing')->findAll();

    return $this->render('SocialSnackAdminBundle:Landing:list.html.php', [
        'landings' => $landings,
    ]);
  }


  /**
   * @Route("/add", name="admin_landing_add")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function addAction(Request $request) {
    $landing = new Landing();
    return $this->_editAction($landing, $request);
  }


  /**
   * @Route("/{id}", name="admin_landing_edit")
   * @param Landing $landing
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function editAction(Landing $landing, Request $request) {
    return $this->_editAction($landing, $request);
  }


  /**
   * @param Landing $landing
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function _editAction(Landing $landing, Request $request) {
    $form = $this->createForm(new LandingType(), $landing);

    $form->add('submit', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      /* @var $cache_purger \SocialSnack\FrontBundle\Service\CachePurger */
      $cache_purger = $this->get('ss.cache_purger');

      if ($file = $form->get('bgimg')->getData()) {
        $filename = uniqid() . '-bgimg.jpg';
        $base_path = 'landings/';
        $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
        $landing->setConfigValue('bgimg', $base_path . $filename);
      }

      if ($file = $form->get('header')->getData()) {
        $filename = uniqid() . '-header.jpg';
        $base_path = 'landings/';
        $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
        $landing->setConfigValue('header', $base_path . $filename);
      }

      if ($file = $form->get('marker')->getData()) {
        $filename = uniqid() . '-marker.jpg';
        $base_path = 'landings/';
        $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
        $landing->setConfigValue('marker', $base_path . $filename);
      }

      // Persist changes.
      $em = $this->getDoctrine()->getManager();
      $em->persist($landing);
      $em->flush();

      // Purge cache.
      $purge = $cache_purger->purge_cache(['entity' => $landing]);
      $this->getRequest()->getSession()->getFlashBag()->set('purge', $purge);

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados exitosamente.');

      return $this->redirect($this->generateUrl('admin_landing_edit', ['id' => $landing->getId()]));
    }

    return $this->render('SocialSnackAdminBundle:Landing:edit.html.php', [
        'form'       => $form->createView(),
        'landing'    => $landing,
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
        'purge'      => $this->getRequest()->getSession()->getFlashBag()->get('purge'),
    ]);
  }


  /**
   * @Route("/{id}/pages/add", name="admin_landingPage_add")
   * @param Landing $landing
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function pageAddAction(Landing $landing, Request $request) {
    $page = new LandingPage();
    $page->setLanding($landing);
    return $this->_pageEditAction($page, $request);
  }


  /**
   * @Route("/pages/{id}", name="admin_landingPage_edit")
   * @param LandingPage $page
   * @param Request     $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function pageEditAction(LandingPage $page, Request $request) {
    return $this->_pageEditAction($page, $request);
  }


  /**
   * @param LandingPage $page
   * @param Request     $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  protected function _pageEditAction(LandingPage $page, Request $request) {
    $form = $this->createForm(new LandingPageType(), $page);

    $form->add('submit', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      $base_path = 'landings/';

      /* @var $cache_purger \SocialSnack\FrontBundle\Service\CachePurger */
      $cache_purger = $this->get('ss.cache_purger');

      if (LandingPage::TYPE_IMAGE === $page->getType() && $form->has('image') && ($file = $form->get('image')->getData())) {
        $filename = uniqid() . '-page-img.jpg';
        $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
        $page->setContentValue('image', $base_path . $filename);
      }

      if (LandingPage::TYPE_PROMOS === $page->getType() && $form->has('promos')) {
        $promos = [];

        foreach ($form->get('promos')->getData() as $row) {
          $promo = ['name' => $row['name']];

          if ($file = $row['thumb']) {
            $filename = uniqid() . '.jpg';
            $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
            $promo['thumb_path'] = $base_path . $filename;
          } else {
            $promo['thumb_path'] = $row['thumb_path'];
          }

          if ($file = $row['image']) {
            $filename = uniqid() . '.jpg';
            $this->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
            $promo['image_path'] = $base_path . $filename;
          } else {
            $promo['image_path'] = $row['image_path'];
          }

          $promos[] = $promo;
        }

        $page->setContentValue('promos', $promos);
      }

      // Persist changes.
      $em = $this->getDoctrine()->getManager();
      $em->persist($page);
      $em->flush();

      // Purge cache.
      $purge = $cache_purger->purge_cache(['entity' => $page]);
      $this->getRequest()->getSession()->getFlashBag()->set('purge', $purge);

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados exitosamente.');

      return $this->redirect($this->generateUrl('admin_landingPage_edit', ['id' => $page->getId()]));
    }

    return $this->render('SocialSnackAdminBundle:Landing:pageEdit.html.php', [
        'form'       => $form->createView(),
        'page'       => $page,
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
        'purge'      => $this->getRequest()->getSession()->getFlashBag()->get('purge'),
    ]);
  }

}