<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData;

class ArticlesControllerTest extends WebTestCase {

  public function setUp() {
    parent::setUp();

    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData'
    ]);
  }


  protected function getCollectionModel() {
    return [
        [
            'type' => 'array',
            'each' => $this->getEntityModel(),
        ]
    ];
  }


  protected function getEntityModel() {
    return [
        'id'       => 'integer',
        'title'    => 'string',
        'content'  => 'string',
        'url'      => 'string',
        'cover'    => 'string',
        'thumb'    => 'string',
        'date'     => 'string',
        'tags'     => 'array',
        'featured' => 'boolean',
    ];
  }


  /**
   * @group functional
   */
  public function testDefault() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/articles/',
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @todo Assert that the returned elements are actually relevant to the search.
   * @group functional
   */
  public function testSearch() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/articles/search?q=foo',
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $model = ['articles' => $model[0]];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @group functional
   */
  public function testDetails() {
    $article_id = LoadArticlesData::$artices[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/articles/%d', $article_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $model[0]['each']['id'] = [
        'type'   => 'integer',
        'equals' => $article_id,
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

}