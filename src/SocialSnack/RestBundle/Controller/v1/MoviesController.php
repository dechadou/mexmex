<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\WsBundle\Entity\MovieComing;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MoviesController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_v1_movies_default")
	 * @Route("/area/{area_id}")
	 * @Method({"GET"})
	 */
	public function defaultAction($area_id = NULL, Request $request) {
    $context_handler = $this->get('rest.context.handler');

    list($area_id, $state_id) = $context_handler->normalizeAreaIds($area_id);

    return $this->forward('SocialSnackRestBundle:Movies:default', ['area_id' => $area_id, 'state_id' => $state_id, 'request' => $request]);
	}
	
	
	/**
	 * @Route("/search", name="rest_v1_movies_search")
	 * @Method({"GET"})
	 */
	public function searchAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Movies:search', ['request' => $request]);
	}
		
	
	/**
	 * Return detailed info for movie(s).
	 * 
	 * @Route("/{id}", name="rest_v1_movies_single", requirements={"id"="[0-9]+"})
	 * @Method({"GET"})
	 */
	public function detailsAction($id) {
		return $this->forward('SocialSnackRestBundle:Movies:details', ['id' => $id]);
	}
	
	
	/**
	 * @Route("/{id}/cinemas", name="rest_v1_movies_cinemas")
	 * @Method({"GET"})
	 */
	public function cinemasAction( $id ) {
		/** @todo Everything :D */
		return new JsonResponse( array( 'error' => 'not-implemented' ), 501 );
	}


  /**
   * $_GET['date']       Optional.
   * $_GET['cinema_id']  Optional.
   *
   * @Route("/{id}/sessions", name="rest_v1_movies_sessions")
   * @Route("/{id}/sessions/area/{area_id}")
   * @Route("/{id}/sessions/favcinemas", name="rest_v1_movies_sessions_fav")
   * @Route("/{id}/sessions/cinemas", name="rest_v1_movies_sessions_cinemas")
   * @Method({"GET"})
   *
   * @param integer $id
   * @param integer $area_id
   * @param Request $request
   * @return JsonResponse
   * @throws RestException
   */
	public function sessionsAction($id, $area_id = NULL, Request $request) {
    /** @var \SocialSnack\RestBundle\Handler\ContextHandler $context_handler */
    $context_handler = $this->get('rest.context.handler');

    list($area_id, $state_id) = $context_handler->normalizeAreaIds($area_id);

    return $this->forward('SocialSnackRestBundle:Movies:sessions', [
        'id'       => $id,
        'area_id'  => $area_id,
        'state_id' => $state_id,
        'request'  => $request
    ]);
	}
  
  
  /**
   * $_POST['value'] Required.
   * 
   * @Route("/{id}/vote", name="rest_v1_movies_vote")
   * @Method({"POST", "GET"})
   */
  public function voteAction($id, Request $request) {
    return $this->forward('SocialSnackRestBundle:Movies:vote', ['id' => $id, 'request' => $request]);
  }


  /**
   * @Route("/coming", name="rest_v1_movies_coming")
   * @Method({"GET"})
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function comingAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Movies:coming', ['request' => $request]);
  }


  /**
   * @Route("/coming/{id}", name="rest_v1_movies_comingSingle")
   * @Method({"GET"})
   *
   * @param MovieComing $movie
   * @param Request     $request
   * @return JsonResponse
   */
  public function comingSingleAction(MovieComing $movie, Request $request) {
    return $this->forward('SocialSnackRestBundle:Movies:comingSingle', ['movie' => $movie, 'request' => $request]);
  }

}