<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<!-- If you delete this meta tag, the ground will open and swallow you. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $subject; ?></title>

<style>
* {
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 42px; color: #d0033d; margin-top: 30px; }
h2 { font-weight:200; font-size: 37px; color: #707073; }
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px; color: #d0033d; margin: 5px 0; }
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

p, ul {
	margin-bottom: 10px;
	font-weight: normal;
	font-size:16px;
	line-height:1.6;
}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

.content table { width: 100%; }

.clear { display: block; clear: both; }


@media only screen and (max-width: 600px) {

	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}

	table.social div[class="column"] {
		width:auto!important;
	}

}
</style>
</head>

<body bgcolor="#ebebeb" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="color: #707073; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; width: 100%!important; height: 100%;">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#ffffff" style="border-bottom: 2px solid #d0033d; width: 100%;">
	<?php if ($banner_path = $view['fronthelper']->get_email_banner_url()) { ?>
	<tr>
		<td></td>
		<td><img src="http:<?php echo $view['assets']->getUrl($banner_path, 'CMS'); ?>" style="display:block;margin:0 auto;width:100%;height:auto;"></td>
		<td></td>
	</tr>
	<?php } ?>
	<tr>
		<td></td>
		<td class="header container" align="" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">

			<!-- /content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
				<table>
					<tr>
						<td class="logo" style="padding: 10px 0 15px 0;">
							<a href="http://cinemex.com" style="border:0;">
								<img src="http:<?php echo $view['assets']->getUrl('assets/img/mails/logo.jpg'); ?>" alt="Cinemex - La magia del cine" style="border:0;" />
							</a>
						</td>
					</tr>
				</table>
			</div><!-- /content -->

		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->

<?php $view['slots']->output('body'); ?>

<!-- FOOTER -->
<table class="footer-wrap" style="width: 100%;	clear:both!important; background: #cbcbcb; padding: 10px 0;">
	<tr>
		<td></td>
		<td class="container" style="	display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">

				<!-- content -->
				<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
					<table style="width: 100%;">
						<tr>
							<td align="center">
								<p style="font-size: 12px; margin: 0; text-align: center;">
									<a href="<?php echo $view['router']->generate('tos_page', array(), TRUE); ?>" style="color: #555555;">Términos y condiciones</a>
								</p>
							</td>
						</tr>
					</table>
				</div><!-- /content -->

		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>
