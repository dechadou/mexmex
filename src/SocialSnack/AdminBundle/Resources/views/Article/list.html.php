<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Artículos - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Listado de Artículos
            </header>
            <div class="panel-body">
                  <div class="adv-table">
                    <div class="clearfix">
                        <div class="btn-group">
                            <button class="btn green" id="editable-sample_new" onclick="window.location.href='<?php echo $view['router']->generate('social_snack_article_add'); ?>'">
                                Nuevo artículo <i class="icon-plus"></i>
                            </button>
                        </div>
                    </div>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                        <tr>
                            <th>Thumb</th>
                            <th>Título</th>
                            <th>Estado</th>
                            <th>Creado</th>
                            <th>Editar</th>
                        </tr>
                        </thead>
                        <tbody>
<?php
foreach ($articles as $article) {
?>
                        <tr class="gradeX">
                            <td><img src="<?php echo $view['fronthelper']->get_img_size($article->getThumb(), '236x222', 'CMS'); ?>" alt="<?php echo $article->getTitle(); ?>" width="236" height="222" /></td>
                            <td><?php echo $article->getTitle(); ?></td>
                            <td><?php echo $article->getStatus(); ?></td>
                            <td><?php echo $article->getDateCreated()->format('Y-m-d'); ?></td>
                            <td><a href="article/<?php echo $article->getId(); ?>">Editar</a></td>
                        </tr>
<?php
}
?>
                      </table>
                  </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#example').dataTable( {
            "aaSorting": [[ 4, "desc" ]]
        } );
    } );
</script>
<?php $view['slots']->stop(); ?>