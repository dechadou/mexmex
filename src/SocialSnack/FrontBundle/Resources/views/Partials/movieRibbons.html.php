<?php
$the_oscars_movies = [];
?>

<?php if ( $movie->getAttr( 'exclusive' ) ) { ?>
<span class="movie-poster-ribbon ribbon-exclusive">Exclusiva</span>
<?php } elseif ( $movie->getAttr( 'preview' ) ) { ?>
<span class="movie-poster-ribbon ribbon-preview">Preestreno</span>
<?php } elseif ( $movie->getAttr( 'presale' ) ) { ?>
<span class="movie-poster-ribbon ribbon-pre">Preventa</span>
<?php } elseif ( $movie->getAttr( 'rerelease' ) ) { ?>
<span class="movie-poster-ribbon ribbon-rerelease">Re-estreno</span>
<?php } elseif ( $movie->getAttr('tour') ) { ?>
<span class="movie-poster-ribbon ribbon-tour-de-cine">Tour de cine</span>
<?php } elseif ( $movie->getAttr('fest') ) { ?>
<span class="movie-poster-ribbon ribbon-festivales">Festivales</span>
<?php } elseif ( $movie->getPremiere() ) { ?>
<span class="movie-poster-ribbon ribbon-new">Estreno</span>
<?php } ?>

<?php if ( $movie->getAttr('oscar_nominee') ) { ?>
<span class="movie-poster-ribbon ribbon-oscars">Nominada al Oscar</span>
<?php } elseif ( $movie->getAttr('oscar_winner') ) { ?>
<span class="movie-poster-ribbon ribbon-winner">Ganadora del Oscar</span>
<?php } ?>

<?php if ( $movie->getAttr( 'v3d' ) ) { ?><span class="movie-poster-ribbon ribbon-3d">3D</span><?php } ?>
<?php if ( $movie->getAttr( 'v4d' ) ) { ?><span class="movie-poster-ribbon ribbon-x4d">4D</span><?php } ?>

<?php if ( $movie->getAttr( 'classics' ) ) { ?><span class="movie-poster-ribbon ribbon-classics">Clásicos</span><?php } ?>
