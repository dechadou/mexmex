<div class="content-box">
  <h1 class="content-box-title">Restablecer contraseña</h1>

  <?php if ($success) { ?>
    <div class="msg">¡Felicidades! Ya puedes ingresar con tu nueva contraseña.</div>

  <?php } else { ?>
    <p class="checkout-legend">
      Ingresa tu email y tu nueva contraseña.
    </p>

    <?php
    echo $view->render(
        'SocialSnackFrontBundle::flashMsgs.html.php',
        array( 'flash_msgs' => $flash_msgs )
    );
    ?>
    <form action="<?php echo $view['router']->generate( 'user_reset_pass' ); ?>" method="post" data-basicvalidate="1" data-validcb="pass_reset_submit">
      <div class="std-form clearfix">
        <input type="hidden" name="code" value="<?php echo $code; ?>" />
        <div class="form-row half">
          <label for="login-user">Email</label>
          <input type="text" name="email" id="login-email" placeholder="Email" data-validators="not_empty email" data-flyvalidate="1" />
        </div>
        <div class="form-row half clear">
          <label for="login-password">Nueva contraseña</label>
          <input type="password" name="password" id="login-password" placeholder="Nueva contraseña" data-validators="not_empty" data-flyvalidate="1" />
        </div>
        <div class="form-row half">
          <label for="login-password">Repetir contraseña</label>
          <input type="password" name="password2" id="login-password2" placeholder="Repetir contraseña" data-validators="not_empty repeat" data-flyvalidate="1" data-repeat-ref="#login-password" />
        </div>
        <div class="form-row clear">
          <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Guardar</button>
        </div>
      </div>
    </form>
  <?php } ?>
</div>