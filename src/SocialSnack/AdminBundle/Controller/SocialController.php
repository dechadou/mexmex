<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class SocialController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }
  

  protected function handleController($items, $route, $view) {
    $em = $this->get('doctrine')->getManager();
    
    $formb = $this->createFormBuilder();
    foreach ($items as $item) {
      $formb->add('aprobar_' . $item->getId(), 'checkbox', array(
          'required' => false
      ));
    }
    
    $formb->add('bulk_action', 'choice', array(
        'choices' => array(
            'active' => 'Activar',
            'unactive' => 'Desactivar',
        ),
        'attr' => array(
          'class' => 'form-control',
        ),
    ));

    $form = $formb->getForm();
    $form->handleRequest($this->getRequest());

    // Process form.
    if ($form->isValid()) {
      $data = $form->getData();

      $active = $data['bulk_action'] === 'active';
      foreach ($items as $item) {
        if ($data['aprobar_' . $item->getId()]) {
          $item->setActive($active);
          $em->persist($item);
          $em->flush();
        }
      }
      
      // Purge cache.
      $cache_purger = $this->get('ss.cache_purger');
      $options = array(
          'type' => 'urls',
          'urls' => $this->generateUrl('esi_social_footer')
      );
      $cache_purger->enqueue($options);
      $purge = $cache_purger->purge_cache();

      return $this->redirect($this->generateUrl($route));
    }
    
    return $this->render('SocialSnackAdminBundle:Social:' . $view . '.html.php', array(
        'title' => 'Instagram',
        'items' => $items,
        'form'  => $form->createView(),
    ));
  }
  
  public function instagramAction() {
    $this->check_roles();
    
    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Instagram');
    $items = $repo->findBy(array(), array('id' => 'DESC'), 50, 0);
    
    return $this->handleController($items, 'social_snack_admin_social_instagram', 'instagram');
  }

  public function twitterAction() {
    $this->check_roles();
    
    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Twitter');
    $items = $repo->findBy(array(), array('id' => 'DESC'), 50, 0);

    return $this->handleController($items, 'social_snack_admin_social_twitter', 'twitter');
  }
  
}