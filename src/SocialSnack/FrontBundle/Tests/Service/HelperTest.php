<?php

namespace SocialSnack\FrontBundle\Tests\Service;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Bundle\FrameworkBundle\Tests\Functional\WebTestCase;

/**
 * Class HelperTest
 * @package SocialSnack\FrontBundle\Tests\Service
 * @author Guido Kritz
 */
class HelperTest extends WebTestCase {


  public function testFormatPrice() {
    $input = '1234567890';
    $expected = '$12,345,678.90';
    $output = FrontHelper::format_price($input);
    $this->assertEquals($expected, $output);
  }


  public function testParseUrlArguments() {
    $input = '/key1-value1/key2-value2/key3-value3/';
    $expected = array(
      'key1' => 'value1',
      'key2' => 'value2',
      'key3' => 'value3',
    );
    $output = FrontHelper::parse_url_arguments($input);
    $this->assertEquals($expected, $output);
  }


  public function testSanitizeForUrl() {
    $input = 'Lorem ipsum #"$%#$%"!! AAA 9345983450! ---.. ,dsgdfg';
    $output = FrontHelper::sanitize_for_url($input);
    $this->assertRegExp('/^[a-zA-Z0-9\-]+$/', $output);
  }


  public function testGetYoutubeIdFromUrl() {
    $id = FrontHelper::get_youtube_id_from_url('https://www.youtube.com/watch?v=2SKewErNzzo');
    $this->assertEquals('2SKewErNzzo', $id);
  }


  /**
   * @todo
   */
  public function testOrderCinemasByDistance() {
    //FrontHelper::order_cinemas_by_distance()
  }


  /**
   * @todo I have no idea how to test this...
   */
  public function testGetUserIp() {

  }


  public function testSortMovieAttrs() {
    $input = [
        'premium'  => 'Premium',
        'digital'  => 'Digital',
        'v3d'      => '3D',
        'platinum' => 'Platino',
        'v4d'      => 'X4D',
        'lang_es'  => 'Español',
        'foo'      => 'Bar',
        'cinemom'  => 'CineMá',
        'cx'       => 'CinemeXtremo',
    ];
    $expected = [
        'platinum' => 'Platino',
        'premium'  => 'Premium',
        'cinemom'  => 'CineMá',
        'cx'       => 'CinemeXtremo',
        'v4d'      => 'X4D',
        'v3d'      => '3D',
        'digital'  => 'Digital',
    ];
    $result = FrontHelper::sort_movie_attrs($input);
    // Make sure all elements are present.
    $this->assertEquals(sizeof($input), sizeof($result));
    // Only the first part of the array is relevant, the other part is unordered.
    $this->assertEquals($expected, array_slice($result, 0, sizeof($expected)));
  }


  public function testGetMovieAttrStrDefault() {
    $str = FrontHelper::get_movie_attr_str(array('digital' => TRUE, 'premium' => TRUE, 'lang_en' => TRUE));
    $this->assertInternalType('string', $str);
    $this->assertEquals('Premium Inglés', $str); // Notice the ordered values

    $str = FrontHelper::get_movie_attr_str(array('digital' => TRUE, 'lang_en' => TRUE));
    $this->assertInternalType('string', $str);
    $this->assertEquals('Tradicional Inglés', $str); // Notice the ordered values
  }


  public function testGetMovieAttrStrGlue() {
    $str = FrontHelper::get_movie_attr_str(array('digital' => TRUE, 'premium' => TRUE, 'lang_en' => TRUE), ', ');
    $this->assertInternalType('string', $str);
    $this->assertEquals('Premium, Inglés', $str);
  }


  public function testGetMovieAttrsStrictTrue() {
    $names = FrontHelper::get_movie_attrs(array('lang_en' => TRUE, 'premium' => TRUE));
    $this->assertInternalType('array', $names);
    $this->assertEquals(array('Inglés', 'Premium'), $names);

    $names = FrontHelper::get_movie_attrs(array('lang_en' => TRUE, 'premium' => FALSE));
    $this->assertInternalType('array', $names);
    $this->assertEquals(array('Inglés'), $names);
  }


  public function testGetMovieAttrsStrictFalse() {
    $names = FrontHelper::get_movie_attrs(array('lang_en' => TRUE, 'premium' => FALSE), FALSE);
    $this->assertInternalType('array', $names);
    $this->assertEquals(array('Inglés', 'Premium'), $names);
  }


  public function testGetMovieAttrsAssociative() {
    $expected = array(
      'lang_en' => 'Inglés',
      'v3d'     => '3D',
    );
    $input = array_keys($expected);

    $names = FrontHelper::get_movie_attrs($input, TRUE, TRUE);
    $this->assertInternalType('array', $names);
    $this->assertEquals($expected, $names);
  }


  public function testGetMovieAttrsNonAssociative() {
    $names = FrontHelper::get_movie_attrs(array('lang_en', 'v3d'));
    $this->assertInternalType('array', $names);
    $this->assertEquals(array('Inglés', '3D'), $names);
  }


  public function testGetMovieAttrName() {
    $name = FrontHelper::get_movie_attr_name('digital');
    $this->assertInternalType('string', $name);
    $this->assertEquals('Digital', $name);
  }


  public function testGetArrayPath() {
    $input = [
      'foo' => [
        'bar' => 'foobar'
      ]
    ];
    $expected = 'foobar';
    $output = FrontHelper::get_array_path($input, "foo['bar']");
    $this->assertEquals($expected, $output);
  }

} 