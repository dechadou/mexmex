<?php

namespace SocialSnack\RestBundle\Handler;

use SocialSnack\FrontBundle\Entity\User;
use SocialSnack\RestBundle\Entity\App;

class ContextHandler {

  protected $apiVersion;

  /** @var App */
  protected $client;

  /** @var User */
  protected $user;

  /** @var \Memcached */
  protected $memcached;

  public function __construct(\Memcached $memcached) {
    $this->memcached = $memcached;
  }


  /**
   * @param integer $apiVersion
   * @return $this
   */
  public function setApiVersion($apiVersion) {
    $this->apiVersion = $apiVersion;

    return $this;
  }


  /**
   * @return integer
   */
  public function getApiVersion() {
    return $this->apiVersion;
  }


  /**
   * @param App $client
   * @return $this
   */
  public function setClient(App $client) {
    $this->client = $client;

    return $this;
  }


  /**
   * @return App
   */
  public function getClient() {
    return $this->client;
  }


  /**
   * @param User $user
   * @return $this
   */
  public function setUser(User $user) {
    $this->user = $user;

    return $this;
  }


  /**
   * @return User
   */
  public function getUser() {
    return $this->user;
  }


  public function trackClientHit() {
    if (!$this->client) {
      throw new \Exception('Client not defined.');
    }

    $key = 'api_client_hit:' . $this->client->getAppId();
    $this->memcached->add($key, 0);
    $this->memcached->increment($key, 1);
  }


  public function normalizeAreaIds($area_id = NULL, $state_id = NULL) {
    if ($this->getApiVersion() < 2 && $area_id > 1000) {
      $state_id = $area_id - 1000;
      $area_id  = NULL;
    }

    return [$area_id, $state_id];
  }

}