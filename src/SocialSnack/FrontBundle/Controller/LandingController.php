<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\FrontBundle\Entity\Landing;
use SocialSnack\FrontBundle\Entity\LandingPage;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;

class LandingController extends BaseController {

    /**
     * Shortcut for views rendering
     * @param  string $landing_name View filename
     * @param  array  $landing_data Data to append to view
     * @return object
     */
    protected function _renderLanding($landing_name, array $landing_data = NULL) {
        $data = array(
            'route'  => $this->getRequest()->attributes->get('_route')
        );

        if (is_array($landing_data) AND !empty($landing_data)) {
            $data = array_merge($data, $landing_data);
        }

        $response = $this->render($landing_name, $data);
        $response->setPublic();
        $response->setSharedMaxAge(6 * 60 * 60);
        $response->setMaxAge(30 * 60);
        return $response;
    }

    /**
     * robocopAction function.
     *
     * @Route("/robocop", name="landing/robocop")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function robocopAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:robocop.html.php');
    }

    /**
     * oscarsAction function.
     *
     * @Route("/oscar2014", name="landing/oscar2014")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function oscar2014Action() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:oscar2014.html.php');
    }

    /**
     * carreraAction function.
     *
     * @Route("/carrera", name="landing/carrera")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function carreraAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:carrera.html.php');
    }

    /**
     * carteleraSmsAction function.
     *
     * @Route("/cartelerasms", name="landing/cartelerasms")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function carteleraSmsAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:cartelerasms.html.php');
    }

    /**
     * promosAction function.
     *
     * @Route("/promos", name="landing/promos")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function promosAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:promos.html.php');
    }

    /**
     * magiaAction function.
     *
     * @Route("/detrasdelamagia", name="landing/detrasdelamagia")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function magiaAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:magia.html.php');
    }

    /**
     * veranoAction function.
     *
     * @Route("/verano_preguntas", name="landing/verano_preguntas")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function veranoPreguntasAction() {
      return $this->redirect('/landing/verano-cinemex/', 301);
    }

    /**
     * veranoAction function.
     *
     * @Route("/verano_terminos", name="landing/verano_terminos")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function veranoTerminosAction() {
      return $this->redirect('/landing/verano-cinemex/', 301);
    }

    /**
     * cineopinionAction function.
     *
     * @Route("/cineopinion", name="landing/cineopinion")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function cineopinionAction() {
        return $this->_renderLanding('SocialSnackFrontBundle:Landings:cineopinion.html.php');
    }

    /**
     * classicsIndexAction function.
     *
     * @Route("/clasicos/", name="classics_index")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function classicsIndexAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:classics_index.html.php', [
        'current_page' => 'index'
      ]);
    }

    /**
     * classicsMoviesAction function.
     *
     * @Route("/clasicos/peliculas/", name="classics_movies")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function classicsMoviesAction() {
      $movies = $this->get('doctrine')
        ->getRepository('SocialSnackWsBundle:Movie')
        ->findByAttr(['classics' => TRUE]);

      return $this->_renderLanding('SocialSnackFrontBundle:Landings:classics_movies.html.php', [
        'movies'       => $movies,
        'current_page' => 'movies'
      ]);
    }

    /**
     * classicsCinemasAction function.
     *
     * @Route("/clasicos/cines/{state_id}", name="classics_cinemas")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function classicsCinemasAction($state_id = NULL) {
      if ($state_id === NULL OR $state_id === 'null') {
        $default_id = $this->container->getParameter('default_cinema');

        return $this->redirect($this->generateUrl('classics_cinemas', array('state_id' => $default_id)));
      }

      $cinemas = [
        1118, 1147, 1128, 1227, 1007, 1040, 1195, 1028, 1035, 1029, 1044, 1032,
        1033, 1025, 1009, 1092, 1163, 1060, 1065, 1099, 1067, 1101, 1047, 1041,
        1073, 1112, 1136, 1230, 1201, 1083, 1071, 1233, 1095, 1239, 1242, 1245,
        1249, 1254, 1256, 1251, 1267, 1260, 1265, 1273, 1276, 1246
      ];

      $map_data = $this->get('ss.ws.utils')->getCinemasMapData('classics_cinemas', $state_id, $cinemas);

      $data = array_merge([
        'current_page' => 'cinemas'
      ], $map_data);

      return $this->_renderLanding('SocialSnackFrontBundle:Landings:classics_cinemas.html.php', $data);
    }

    /**
     * apoyocinemexAction function.
     *
     * @Route("/apoyocinemex/", name="apoyocinemex")
     * @Template(engine="php")
     *
     * @access public
     * @return void
     */
    public function apoyocinemexAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:apoyocinemex.html.php');
    }

    /**
     * losCabosAction action
     *
     * @Route("/los-cabos/", name="los_cabos")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function losCabosAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:los_cabos.html.php');
    }

    /**
     * twitterAppAction action
     *
     * @Route("/lamagiadelcine/", name="twitterapp")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function twitterAppAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:twitterapp.html.php');
    }

    /**
     * @Route("/viajes-de-esperanza/", name="viajes_index")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function viajesIndexAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:viajes_index.html.php');
    }

    /**
     * @Route("/viajes-de-esperanza/terminos/", name="viajes_terms")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function viajesTermsAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:viajes_terms.html.php');
    }

    /**
     * @Route("/juegos/", name="juegos")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function juegosAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:iframe.html.php', [
        'url'    => '//promocinemex.com/landing/juegos',
        'height' => '1500px'
      ]);
    }

    /**
     * @Route("/ganadores/", name="landing_ganadores")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function ganadoresAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:iframe.html.php', [
        'url'    => '//analizan.com/cinemex/GanadoresTwitter',
        'height' => '700px'
      ]);
    }

    /**
     * @Route("/campana-corporativa/", name="landing_campana_corporativa")
     * @Template(engine="php")
     *
     * @access public
     * @return object
     */
    public function campanaCorporativaAction() {
      return $this->_renderLanding('SocialSnackFrontBundle:Landings:campana_corporativa.html.php');
    }

  /**
   * @Route("{slug}/", name="front_landing")
   * @ParamConverter("landing", options={"repository_method"="findWithPagesBySlug", "id"="slug"})
   *
   * @param Landing $landing
   * @param Request $request
   * @return Response
   */
  public function dynamicLandingAction(Landing $landing, Request $request) {
    return $this->forward('SocialSnackFrontBundle:Landing:dynamicLandingPage', [
        'landing' => $landing,
        'page_slug' => FrontHelper::sanitize_for_url($landing->getPages()[0]->getName()),
        'args' => '',
    ]);
  }

  /**
   * @Route("{slug}/{page_slug}/{args}",
   *   name="front_landing_page",
   *   requirements={
   *     "args"=".*"
   *   },
   *   defaults={
   *     "args"=""
   *   }
   * )
   * @ParamConverter("landing", options={"repository_method"="findWithPagesBySlug", "id"="slug"})
   *
   * @param Landing $landing
   * @param string $page_slug
   * @param string $args
   * @param Request $request
   * @return Response
   */
  public function dynamicLandingPageAction(Landing $landing, $page_slug = '', $args = '', Request $request) {
    foreach ($landing->getPages() as $_page) {
      if (FrontHelper::sanitize_for_url($_page->getName()) === $page_slug) {
        $page = $_page;
        break;
      }
    }

    if (!isset($page)) {
      throw $this->createNotFoundException();
    }

    $args = FrontHelper::parse_url_arguments($args);

    if (LandingPage::TYPE_CINEMAS === $page->getType()) {
      $state_id = isset($args['estado']) ? $args['estado'] : NULL;
      if (!$state_id) {
        $default_id = $this->container->getParameter('default_cinema');

        return $this->redirect($this->get('ss.front.utils')->get_permalink($page, FALSE, [
            'args' => 'estado-' . $default_id
        ]));
      }

      $cinemas = array_map('trim', explode(',', $page->getContent()['cinemas']));

      $map_data = $this->get('ss.ws.utils')->getCinemasMapData('classics_cinemas', $state_id, $cinemas);

      $data = array_merge([
          'page'     => $page,
          'fallback' => FALSE,
      ], $map_data);

      $template = $this->renderView('SocialSnackFrontBundle:Landings:dynamicCinemas.html.php', $data);

    } else if (LandingPage::TYPE_MOVIES === $page->getType()) {
      $group_ids = array_map('trim', explode(',', $page->getContent()['movies']));
      $qb = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Movie')->createQueryBuilder('q');
      $qb->addSelect('q')
          ->where($qb->expr()->in('q.group_id', $group_ids))
          ->andWhere('q.active = 1')
          ->andWhere('q.position > 0')
          ->andWhere($qb->expr()->isNull('q.parent'));
      $movies = $qb->getQuery()->getResult();

      $template = $this->renderView('SocialSnackFrontBundle:Landings:dynamicMovies.html.php', [
          'movies' => $movies,
          'page' => $page,
      ]);

    } else {
      $template = $this->renderView('SocialSnackFrontBundle:Landings:dynamic' . ucfirst(strtolower($page->getType())) . '.html.php', [
          'page' => $page,
      ]);
    }

    $res = $this->render('SocialSnackFrontBundle:Landings:dynamic.html.php', [
        'landing' => $page->getLanding(),
        'page' => $page,
        'page_template' => $template,
    ]);

    $res->setSharedMaxAge(60 * 60);
    $res->setMaxAge(10 * 60);
    return $res;
  }

}
