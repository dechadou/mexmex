<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\ClientType;
use SocialSnack\RestBundle\Entity\App;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/clients")
 */
class ClientsController extends BaseController {

  protected function get_required_role() {
    return 'ROLE_APPS';
  }


  /**
   * @Route("/", name="social_snack_admin_clients_list")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function defaultAction(Request $request) {
    $this->check_roles();

    $session = $request->getSession();

    $repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App');
    $apps = $repo->findAll();
    
    return $this->render('SocialSnackAdminBundle:Clients:list.html.php', array(
        'clients' => $apps,
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/add", name="social_snack_admin_clients_add", defaults={"client_id"= null})
   * @Route("/{client_id}", name="social_snack_admin_clients_edit")
   *
   * @param null    $client_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function editAction($client_id = NULL, Request $request) {
    $this->check_roles();

    $session = $request->getSession();

    if (is_null($client_id)) {
      $app = new App();
    } else {
      $repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App');
      $app = $repo->find($client_id);
    }
    
    if (!$app->getId()) {
      $front_utils = $this->container->get('ss.front.utils');
      if (!$app->getSecretKey()) {
        $app->setSecretKey($front_utils->random_string(50));
      }
      if (!$app->getAppId()) {
        $app->setAppId($front_utils->random_string(20));
      }
      if (!$app->getSetting('appPass')) {
        $app->setSetting('appPass', $front_utils->random_string(20));
      }
    }
    
    $form = $this->createForm(new ClientType(), $app);
    $form->handleRequest($this->getRequest());
      
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      
      $em->persist($app);
      $em->flush();

      $cache_purger = $this->get('ss.cache_purger');
      $purge = $cache_purger->purge_cache(array('entity' => $app));

      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);

      return $this->redirect($this->generateUrl('social_snack_admin_clients_list'));
    }
    
    return $this->render('SocialSnackAdminBundle:Clients:edit.html.php', array(
        'client' => $app,
        'form' => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/{id}/usage", name="social_snack_admin_clients_singleUsage")
   *
   * @param App     $app
   * @param Request $request
   */
  public function singleUsage(App $app, Request $request) {
    $date_to = new \DateTimeImmutable('now');
    $date_from = $date_to->sub(new \DateInterval('P1D'));
    $period = new \DatePeriod($date_from, new \DateInterval('PT1H'), $date_to);

    $repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:AppUsageEntry');
    $qb = $repo->createQueryBuilder('q');
    $qb
        ->select(['q.hits', 'q.misses', 'q.date'])
        ->where('q.app = :app')
        ->andWhere('q.date > :date_from')
        ->andWhere('q.date <= :date_to')
        ->setParameter('app', $app)
        ->setParameter('date_from', $date_from->format('Y-m-d H:i:s'))
        ->setParameter('date_to', $date_to->format('Y-m-d H:i:s'))
    ;
    $q = $qb->getQuery();
    $res = $q->getResult();

    $data2 = [];

    foreach ($res as $row) {
      $data2[$row['date']->format('YmdH')] = $row['misses'] + $row['hits'];
    }

    $values = [];
    $labels = [];
    $start_hour = (int)$date_from->format('H');

    $i = 0;
    foreach ($period as $d) {
      $i++;
      $hour = $start_hour + $i - 1;
      if ($hour >= 24) {
        $hour -= 24;
      }
      $labels[] = $hour;
      $values[] = isset($data2[$d->format('YmdH')]) ? $data2[$d->format('YmdH')] : 0;
    }

    return $this->render('SocialSnackAdminBundle:Clients:singleUsage.html.php', [
        'client' => $app,
        'data' => [
            'labels' => $labels,
            'values' => $values,
        ],
    ]);
  }


  /**
   * @Route("/{id}/getToken", name="social_snack_admin_clients_getToken")
   *
   * @param App     $app
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function getTokenAction(App $app, Request $request) {
    $this->check_roles();

    $token = NULL;
    $user = NULL;
    $user_id = NULL;
    $form = $this->createFormBuilder()
        ->add('user_id', 'text', ['label' => 'ID de usuario'])
        ->add('passphrase', 'text', ['label' => 'Passphrase'])
        ->add('submit', 'submit', ['label' => 'Enviar'])
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      // Validate passphrase.
      $pass     = $form->get('passphrase')->getData();
      $passHash = $this->container->getParameter('tokenGeneratorPassphraseHash');
      $secret   = $this->container->getParameter('secret');

      if ($passHash === hash('sha256', $pass . $secret)) {
        // Get user.
        $user_id = $form->get('user_id')->getData();
        $user    = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:User')->find($user_id);

        if ($user) {
          // Generate token.
          $token = $this->get('rest.auth.handler')->buildToken($user, $app);
        } else {
          $token = 'INVALID USER';
        }
      } else {
        $token = 'BAD PASSPHRASE';
      }
    }

    return $this->render('SocialSnackAdminBundle:Clients:getToken.html.php', [
        'client'  => $app,
        'user_id' => $user_id,
        'token'   => $token,
        'form'    => $form->createView(),
    ]);
  }
  
}

