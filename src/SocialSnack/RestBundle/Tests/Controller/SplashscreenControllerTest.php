<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadSplashesData;

class SplashscreenControllerTest extends WebTestCase {

  protected function getEntityModel() {
    return [
        'id'        => 'integer',
        'url_image' => 'string',
        'deep_link' => 'string',
    ];
  }


  protected function _test($url, $model) {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSplashesData',
    ]);

    $client = static::createClient();
    $client->request(
        'GET',
        is_string($url) ? $url : $url(),
        [],
        [],
        ['HTTP_X-API-Consumer_key' => 'abc']
    );

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api
   */
  public function testDefault() {
    $model = [
        'screens' => [
            'type' => 'array',
            'each' => $this->getEntityModel()
        ]
    ];
    $this->_test('/rest/v2/splashscreens/', $model);
  }


  /**
   * @group functional
   * @group api
   */
  public function testDetails() {
    $this->_test(
        function() { return sprintf('/rest/v2/splashscreens/%d', LoadSplashesData::$splashes[0]->getId()); },
        $this->getEntityModel()
    );
  }

}