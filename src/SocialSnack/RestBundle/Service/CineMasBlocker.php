<?php

namespace SocialSnack\RestBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CineMasBlocker {
  
  protected $container;
  
  protected $valid_types = array('visa', 'master', 'amex');
  
  
  public function __construct($container) {
    $this->container = $container;
  }
  
  
  protected function getRandomFirstName() {
    $names = array('Ximena', 'Valeria', 'Fernanda', 'Jose', 'Guadalupe', 'Dulce Maria', 'Ana Sofia', 'Ana Victoria', 'Paola', 'Ana Paula', 'Luz Maria', 'Esmeralda', 'Abril', 'Marisol', 'Lucero', 'Estrella', 'Santiago', 'Miguel Angel', 'Diego', 'Emiliano', 'Jose Angel', 'Luis Angel', 'Pablo', 'Gabriel', 'Carlos', 'Jose Luis', 'Jose Manuel', 'Jose Antonio', 'Luis Fernando');
    return $names[array_rand($names)];
  }
  
  
  protected function getRandomLastName() {
    $names = array('Garcia', 'Fernandez', 'Gonzalez', 'Rodriguez', 'Cruz', 'Martinez', 'Sanchez', 'Perez', 'Martin', 'Gomez', 'Jimenez', 'Ruiz', 'Hernandez', 'Diaz', 'Corrales', 'Alvarez', 'Muñoz', 'Romero', 'Alonso', 'Gutierrez');
    return $names[array_rand($names)];
  }
  
  
  protected function getVisaPrefixes() {
    return array(
        "4539",
        "4556",
        "4916",
        "4532",
        "4929",
        "40240071",
        "4485",
        "4716",
        "4",
    );
  }
  
  protected function getMasterPrefixes() {
    return array(
        "51",
        "52",
        "53",
        "54",
        "55",
    );
  }

  protected function getAmexPrefixes() {
    return array(
        "34",
        "37"
    );
  }
  
  
  protected function validateType($type) {
    if (!in_array(strtolower($type), $this->valid_types)) {
      throw new \Exception(sprintf('The type "%s" is not valid. Types accepted are: %s.',
          $type,
          implode(', ', array_map('ucfirst', $this->valid_types))
      ));
    }
  }
  
  protected function getPrefix($type, $count) {
    $valid_types = $this->valid_types;
    $this->validateType($type);
    
    $prefixes = $this->{'get' . ucfirst($type) . 'Prefixes'}();
    $i = (array)array_rand($prefixes, $count);
    
    $output = array();
    foreach ($i as $h) {
      $output[] = $prefixes[$h];
    }
    
    return $output;
  }
  
  protected function completeNumber($prefix, $length) {
    $ccnumber = $prefix;

    // Generate digits
    while ( strlen($ccnumber) < ($length - 1) ) {
      $ccnumber .= rand(0,9);
    }

    // Calculate sum
    $sum = 0;
    $pos = 0;

    $reversedCCnumber = strrev( $ccnumber );

    while ( $pos < $length - 1 ) {
      $odd = $reversedCCnumber[ $pos ] * 2;
      if ( $odd > 9 ) {
        $odd -= 9;
      }

      $sum += $odd;

      if ( $pos != ($length - 2) ) {
        $sum += $reversedCCnumber[ $pos +1 ];
      }
      $pos += 2;
    }

    // Calculate check digit
    $checkdigit = (( floor($sum/10) + 1) * 10 - $sum) % 10;
    $ccnumber .= $checkdigit;

    return $ccnumber;
  }
  
  protected function getNumberLength($type) {
    $this->validateType($type);
    
    switch (strtolower($type)) {
      case 'visa':
        return 16;
        break;
      case 'master':
        return 16;
        break;
      case 'amex':
        return 15;
        break;
    }
  }

  public function buildCardNumbers($type, $count = 1) {
    $prefixes = $this->getPrefix($type, $count);
    
    $length = $this->getNumberLength($type);
    
    for ($i = 0; $i < $count; $i++) {
      $ccnumber = $prefixes[array_rand($prefixes)];
      $ccnumber = $this->completeNumber($ccnumber, $length);
      $ccnumber = $this->formatCardNumber($ccnumber, $type);
      $result[] = $ccnumber;
    }

    return $result;
  }
  
  
  protected function formatCardNumber($number, $type) {
    $this->validateType($type);
    
    switch (strtolower($type)) {
      case 'visa':
      case 'master':
        return preg_replace('/^([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})$/', '$1 $2 $3 $4', $number);
        break;
      case 'amex':
        return preg_replace('/^([0-9]{4})([0-9]{6})([0-9]{5})$/', '$1 $2 $3', $number);
        break;
    }
  }
  
  
  public function buildSecurityCodes($type, $count = 1) {
    $length = 'amex' === strtolower($type) ? 4 : 3;
    $output = array();
    for ($i = 0 ; $i < $count ; $i++) {
      $csc = '';
      while ( strlen($csc) < ($length) ) {
        $csc .= rand(0,9);
      }
      $output[] = $csc;
    }
    
    return $output;
  }

  
  /**
   * 
   * @param int $count
   * @param array $types
   * @return array
   */
  public function buildCreditCards($count = 1, array $types = NULL) {
    if (is_null($types)) {
      $types = $this->valid_types;
    } elseif (!sizeof($types)) {
      $types = $this->valid_types;
    }
    
    $output = array();
    
    for ($i = 0 ; $i < $count ; $i++) {
      $type = $types[array_rand($types)];
      $cc = $this->buildCardNumbers($type);
      $csc = $this->buildSecurityCodes($type);
      $y = (int)date('Y');
      $output[] = array(
          'name'         => $this->getRandomFirstName() . ' ' . $this->getRandomLastName(),
          'cc'           => $cc[0],
          'csc'          => $csc[0],
          'expire_month' => str_pad(rand(1,12), 2, '0', STR_PAD_LEFT),
          'expire_year'  => rand($y + 1, $y + 30),
      );
    }
    
    return $output;
  }
  
  
  public function handleRequest(Request $request) {
    /* @var $mem \Memcached */
    $mem = $this->container->get('memcached');
    
    // Data to test against.
    $_test_info = $mem->get('cineplus_block_cc');
    if (!$_test_info) {
      return;
    }
    $test_info = array(
//        'name'         => array_map(function($a) { return $a['name']; },         $_test_info['ccs']),
        'cc'           => array_map(function($a) { return preg_replace('/[^0-9]/', '', $a['cc']); }, $_test_info['ccs']),
        'csc'          => array_map(function($a) { return $a['csc']; },          $_test_info['ccs']),
        'expire_month' => array_map(function($a) { return $a['expire_month']; }, $_test_info['ccs']),
        'expire_year'  => array_map(function($a) { return $a['expire_year']; },  $_test_info['ccs']),
    );
    
// Post data.
    $provided_card = array(
//        'name'         => $request->request->get('name'),
        'cc'           => preg_replace('/[^0-9]/', '', $request->request->get('cc')),
        'csc'          => $request->request->get('csc'),
        'expire_month' => $request->request->get('expire-month'),
        'expire_year'  => $request->request->get('expire-year'),
    );
    
    // Make sure all data matches the fake data generated by this service.
    foreach ($provided_card as $k => $v) {
      // If some field doesn't match, proceed with the requets normally.
      if (!in_array($v, $test_info[$k])) {
        return;
      }
    }
    
    // If we got so far, it means we caught a Cine+ transaction. Let's block it!
    
    // Register and block the IP address.
    $mem->set('cineplus_block_cap', array(
        'session_id' => $request->request->get('session_id'),
        'client_ip'  => $request->getClientIp(),
    ), 15 * 60);
    
    // Send an error response and terminate the execution.
    $res = new Response('', 500);
    $res->send();
    die();
  }
  
}