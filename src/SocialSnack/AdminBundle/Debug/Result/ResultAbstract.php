<?php

namespace SocialSnack\AdminBundle\Debug\Result;

/**
 * Class ResultAbstract
 * @package SocialSnack\AdminBundle\Debug\Result
 * @author Guido Kritz
 */
abstract class ResultAbstract {

  protected $data;

  protected $raw;

  const TABLE_LABEL_SUCCESS = '<span class="label label-success label-mini">OK</span>';
  const TABLE_LABEL_WARNING = '<span class="label label-warning label-mini">!</span>';
  const TABLE_LABEL_DANGER  = '<span class="label label-danger label-mini">!</span>';

  /**
   * @param mixed $data
   * @param array $raw
   */
  public function __construct($data, array $raw = NULL) {
    $this->data = $data;
    $this->raw  = $raw;
  }


  /**
   * Returns the result data.
   *
   * @return mixed
   */
  public function getData() {
    return $this->data;
  }


  /**
   * Returns the raw data from the WS requests.
   *
   * @return array
   */
  public function getRawData() {
    return $this->raw;
  }


  /**
   * Returns the data formatted to display the results table in the backend.
   *
   * @return array
   */
  abstract public function getDataForTable();


  /**
   * Returns an analysis of the current status and possible solutions if needed.
   *
   * @return array
   */
  abstract public function getAnalysis();


}