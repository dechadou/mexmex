<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start( 'body' );
?>
<!-- BODY -->
<table class="body-wrap" bgcolor="#ebebeb" style="width: 100%; padding-bottom: 30px;">
	<tr>
		<td></td>
		<td class="container" align="" bgcolor="#ebebeb" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5;">
				<table>
					<tr>
						<td>
							
							<h1 style="color: #d0033d">¡Bienvenid@ <?php echo $user->getFirstName(); ?>!</h1>
							<h2>¡Ya eres usuario de Cinemex.com!</h2>
							<p>
                Puedes ingresar a tu cuenta ingresando en <a href="http://cinemex.com">Cinemex.com</a> y completando el email y la contraseña que elegiste al registrarte. 
							<p>
                Selecciona tus salas favoritas, compra tus boletos en pocos clicks y disfruta de todos los beneficios que Cinemex te ofrece.
							</p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5; border-top: 1px solid #fbfbfb;">
				
				<table bgcolor="">
					<tr>
						<td class="small" width="20%" style="vertical-align: top; padding-right:10px;"><img src="http:<?php echo $view['assets']->getUrl('assets/img/mails/bullet-01.gif'); ?>" /></td>
						<td style="vertical-align: middle;">
							<h4 style="color: #d0033d">Comprar tus boletos</h4>
							<p class="">¡Nunca fue tan fácil! Accede a tus salas favoritas y disfruta de las mejores películas.</p>
						</td>
					</tr>
				</table>
			
			</div><!-- /content -->
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5; border-top: 1px solid #fbfbfb;">
				<table bgcolor="">
					<tr>
						<td class="small" width="20%" style="vertical-align: top; padding-right:10px;"><img src="http:<?php echo $view['assets']->getUrl('assets/img/mails/bullet-02.gif'); ?>" /></td>
						<td style="vertical-align: middle;">
							<h4 style="color: #d0033d">Disfruta al máximo</h4>
							<p class="">de todos los estrenos, premieres y funciones exclusivas.</p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-top: 1px solid #fbfbfb;">
				
				<table bgcolor="">
					<tr>
						<td class="small" width="20%" style="vertical-align: top; padding-right:10px;"><img src="http:<?php echo $view['assets']->getUrl('assets/img/mails/bullet-03.gif'); ?>" /></td>
						<td style="vertical-align: middle;">
							<h4 style="color: #d0033d">Conviértete en Invitado Especial </h4>
							<p class="">Acumula puntos por todas las compras que realices y canjéalos por lo que tu quieras en Cinemex.</p>
						</td>
					</tr>
				</table>
			
			</div><!-- /content -->
						
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->
<?php
$view['slots']->stop();
?>