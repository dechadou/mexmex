<?php
$title = 'Notificaciones Push';

use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', $title . ' - Cinemex');
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?php echo $title; ?></strong>
        </header>

        <div class="panel-body">
          <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
          <?php echo $view['form']->form($form); ?>
        </div>
      </section>
    </div>
  </div>

<script>
  var count = <?php echo json_encode($count); ?>;
  var $status;
  var sent, failed, failed_ids, skipped_ids, pages, page;

  $('form').on('submit', function(e) {
    e.preventDefault();

    var $selectedDevices = $('[name="form[device][]"]:checked');
    var cnt = 0;

    if (!$selectedDevices.length) {
      alert('Debes seleccionar al menos un dispositivo.');
      return false;
    }

    $selectedDevices.each(function() {
      cnt += parseInt(count[$(this).val()]) || 0;
    });

    if (!confirm('La notificación se enviará a ' + cnt + ' dispositivos. ¿Desea ontinuar?')) {
      return false;
    }

    sent = 0;
    failed = 0;
    failed_ids = [];
    skipped_ids = [];
    pages = parseInt($(this).find('[name="form[_pages_limit]"]').val());
    page = 1;

    lockForm();

    process(
        $(this),
        parseInt($(this).find('[name="form[since_id]"]').val())
    );
  });


  function process($form, since_id) {
    if (!since_id) {
      since_id = 1;
    }

    status('Procesando página ' + page + '...');

    $.ajax({
      url      : '<?php echo $view['router']->generate('admin_apps_push_bulk_process'); ?>',
      type     : 'post',
      dataType : 'json',
      data     : $form.serialize() + '&form[since_id]=' + since_id,
      success  : function(res) {
        sent += res.sent;
        failed += res.failed;
        failed_ids = failed_ids.concat(res.failed_ids);
        skipped_ids = skipped_ids.concat(res.skipped_ids);
        var next_id = parseInt(res.last_id) + 1;

        if (res.count >= res.limit && (!pages || page < pages)) {
          page++;
          process($form, next_id);
        } else {
          var msg = 'Envío completado. ' + sent + ' enviados. ' + failed + ' fallidos.';
          if (failed_ids.length) {
            msg += '<br>IDs fallidos: ' + failed_ids.join(',');
          }
          if (skipped_ids.length) {
            msg += '<br>IDs repetidos: ' + skipped_ids.join(',');
          }
          status(msg);
          $('form').find('[name="form[since_id]"]').val(next_id);
          unlockForm();
        }
      },
      error    : function() {
        status('Error en el envío procesando desde ID ' + since_id + '.');
        unlockForm();
      }
    });

    $form.find('[name="form[_save]"]').prop('checked', false);
  }


  function status(msg) {
    if (!$status) {
      $status = $('<div/>', { 'class' : 'alert alert-info' });
      $status.prependTo('.panel-body');
    }

    $status.html(msg);
  }


  function lockForm() {
    $('form').find('input,textarea,select').prop('readonly', true);
  }


  function unlockForm() {
    $('form').find('input,textarea,select').prop('readonly', false);
  }
</script>
<?php $view['slots']->stop(); ?>