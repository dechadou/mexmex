<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Aviso de privacidad integro para visitantes - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Aviso de privacidad integro para visitantes</h1>

<div class="entry-body align-justify">
  <p>Los datos personales e imágenes que se obtienen a través de videograbaciones,
    fotografías, registros o bitácoras de acceso a nuestras instalaciones (en
    adelante y conjuntamente los “Datos Personales”), son tratados por Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida
    Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro
    Obregón, C.P. 01210, México, D.F., con las siguientes finalidades primarias:</p>

  <ul>
    <li>Control;</li>
    <li>Vigilancia; y</li>
    <li>Seguridad, durante su vista a nuestras instalaciones.</li>
  </ul>

  <h2>I.  DATOS Y SU FINALIDAD:</h2>

  <p>Los Datos Personales recabados por “Cinemex”, serán resguardados y
    administrados en términos de lo dispuesto en la Ley Federal de Protección de
    Datos Personales en Posesión de los Particulares, su Reglamento y los
    Lineamientos del Aviso de Privacidad (en adelante y conjuntamente “la
    legislación”), en tal sentido, nos comprometemos a salvaguardar sus Datos
    Personales bajo los principios de lealtad y responsabilidad. Los Datos
    Personales obtenidos se encuentran protegidos por medidas de seguridad,
    físicas, tecnológicas y administrativas, apegadas a normas que buscan
    salvaguardar dichos Datos Personales en términos de lo dispuesto en
    “la legislación”</p>

  <p>Los Datos Personales que se recaban a través de videograbaciones y/o fotografías,
    serán conservados por un periodo de 7 días naturales en medios electrónicos
    (tomando en consideración aspectos administrativos, contables, fiscales y
    jurídicos), después de la fecha en que fueron tomadas las videograbaciones
    y/o fotografías; los Datos Personales obtenidos por medio de registros,
    bitácoras o formatos de acceso a las instalaciones, serán conservados por un
    periodo de 12 meses calendario. Una vez concluida la finalidad del registro,
    serán descartados a efecto de evitar un tratamiento indebido de los mismos,
    con fundamento en el artículo 11, segundo párrafo de la Ley Federal de
    Protección de Datos Personales en Posesión de los Particulares.</p>

  <h2>II. MEDIOS PARA EJERCER LOS DERECHOS “ARCO” Y REVOCACIÓN DEL CONSENTIMIENTO:</h2>

  <p>Podrá ejercer sus derechos de ARCO: Acceso, Rectificación, Cancelación y
    Oposición contenidos en la Ley Federal de Protección de Datos Personales en
    Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de
    Privacidad, únicamente mediante el llenado del formato correspondiente que
    podrá obtener de la página de internet:
    <a href="http://cinemex.com/arco/formulario">http://cinemex.com/arco/formulario</a>

  <p>Una vez presentada su solicitud en el formato prestablecido, “Cinemex” podrá
    solicitarle en un periodo no mayor a 5 días hábiles, la información y/o
    documentación necesaria para su seguimiento, así como para la acreditación de
    su identidad, de acuerdo a los términos que marca la “la legislación”. Por
    lo que contará con 10 días hábiles posteriores a su recepción, para atender
    este requerimiento. De lo contrario su solicitud se tendrá por no presentada.</p>

  <p>Asimismo, en un plazo posterior de 20 días hábiles “Cinemex” emitirá una
    respuesta, la cual le será notificada por los medios de contacto que haya
    establecido en su solicitud. Una vez emitido el resultado y en caso de que
    la misma sea procedente (parcial o totalmente), “Cinemex” contará con 15 días
    hábiles para adoptar dicho resultado.</p>

  <p>Los términos y plazos indicados en los párrafos anteriores, podrán ser
    ampliados una sola vez en caso de ser necesario y se le deberá notificar a
    través de los medios de contacto que haya establecido.</p>

  <p>La revocación  y el ejercicio de los Derechos ARCO serán gratuitos, debiendo
    usted cubrir únicamente los gastos justificados de envío, o el costo de
    reproducción en copias u otros formatos establecidos en su solicitud.</p>

  <p>Asimismo, le informamos que le asiste su derecho para acudir ante el Instituto
    Federal de Acceso a la Información y Protección de Datos en caso de considerar
    que su derecho a la protección de Datos Personales ha sido vulnerado. Para
    mayor información visite <a href="http://www.ifai.org.mx">www.ifai.org.mx</a></p>

  <h2>III.  TRANSFERENCIA DE DATOS:</h2>

  <p>“Cinemex”, se abstendrá de vender, arrendar o alquilar sus Datos Personales
    con un tercero, pero por razones de seguridad, operación y servicio serán
    transferidos únicamente a autoridades locales o federales que así lo requieran.</p>

  <h2>IV. CAMBIOS AL AVISO DE PRIVACIDAD Y VULNERACIÓN DE SEGURIDAD:</h2>

  <p>“Cinemex” tiene el derecho de efectuar cualquier cambio o modificación al
    Aviso de Privacidad. Tales modificaciones podrán ser consultadas en
   <a href="http://www.cinemex.com/privacidadvisitantes">www.cinemex.com/privacidadvisitantes.</a></p>

  <p>En caso de que ocurra una vulneración de seguridad en cualquier fase del
  tratamiento de sus Datos Personales, “Cinemex” a través de la figura del
  responsable lo hará del conocimiento general, a través de la siguiente página
  de internet: www.cinemex.com/privacidadvisitantes, para que tome las medidas
  correspondientes a fin de resguardar sus derechos.</p>

  <p>Fecha de última actualización: 5/Enero/2015</p>
</div>
<?php $view['slots']->stop(); ?>
