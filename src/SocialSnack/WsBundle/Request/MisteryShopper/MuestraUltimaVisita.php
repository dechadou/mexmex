<?php

namespace SocialSnack\WsBundle\Request\MisteryShopper;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Request\MisteryShopperAbstract;

class MuestraUltimaVisita extends MisteryShopperAbstract {

  protected $method = 'MuestraUltimaVisita';

  protected $default_args = array(
      'NumTransaccion' => NULL,
  );


  /**
   * Parse the request and return only the relevant nodes.
   *
   * @throws RequestMsgException
   * @param \stdClass $res;
   * @return \SimpleXMLElement
   */
  public function parse_res($res) {
    if ( !$res->MuestraUltimaVisitaResult ) {
      throw new RequestMsgException( 'Invalid response 0: ' . json_encode( $res ) );
    }

    $xml = simplexml_load_string( $res->MuestraUltimaVisitaResult->any, null, LIBXML_NOCDATA );

    return $xml;
  }

}