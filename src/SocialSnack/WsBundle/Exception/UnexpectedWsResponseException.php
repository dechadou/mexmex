<?php

namespace SocialSnack\WsBundle\Exception;

/**
 * Class UnexpectedWsResponseException
 * @package SocialSnack\WsBundle\Exception
 * @author Guido Kritz
 */
class UnexpectedWsResponseException extends \Exception {

} 