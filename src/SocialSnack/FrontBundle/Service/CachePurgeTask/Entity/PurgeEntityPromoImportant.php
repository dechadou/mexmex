<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityPromoImportant extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path'    => $this->router->generate('home'),
            'env'     => 'front'
        ],
        [
            'path'    => $this->router->generate('esi_important_promos'),
            'env'     => 'front'
        ],
        [
            'path'    => $this->frontUtils->get_cms_url($this->entity->getThumb(), '490x116'),
            'env'     => 'static'
        ],
        [
            'path'    => $this->frontUtils->get_cms_url($this->entity->getImage(), '700x573'),
            'env'     => 'static'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\PromoImportantRepository::findByTarget:home',
                'SocialSnack\FrontBundle\Entity\PromoImportantRepository::findByTarget:maps',
                'SocialSnack\FrontBundle\Entity\PromoImportantRepository::findByTarget:cinemas',
                'SocialSnack\FrontBundle\Entity\PromoImportantRepository::findByTarget:movies',
            ],
            'env' => 'front'
        ]
    ];
  }

}