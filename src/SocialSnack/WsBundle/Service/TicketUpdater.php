<?php
namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Ticket;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;


class TicketUpdater extends WorkerAbstract {
  
  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
	protected $container;
	
	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;
	
  /**
   * @var \SocialSnack\WsBundle\Request\Request
   */
	protected $cinemex_ws;
	
  
  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
   * @param \SocialSnack\WsBundle\Request\Request $cinemex_ws
   */
	public function __construct($container, $doctrine, $cinemex_ws) {
		$this->container  = $container;
		$this->doctrine   = $doctrine;
		$this->cinemex_ws = $cinemex_ws;
	}
	
  
  public function execute($data) {
		/* @var $movie  \SocialSnack\WsBundle\Entity\Movie */
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */

    $cinema_id = $data['cinema_id'];
    
		try {
			$req          = $this->cinemex_ws;
			$doctrine     = $this->doctrine;
			$em           = $doctrine->getManager();
      $conn         = $doctrine->getConnection();
			$cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
			$cinema       = $cinema_repo->find($cinema_id);

      $update_query = "UPDATE Ticket SET is_new = 0 WHERE cinema_id = :cinema_id";
      $delete_query = "DELETE FROM Ticket WHERE cinema_id = :cinema_id AND is_new = 0";
      
			if ( !$cinema )
        return TRUE;
      
      $this->consoleInfo(sprintf('Cinema: %s (%s)', $cinema->getName(), $cinema->getLegacyId()));

      try {
        $req->init( 'Consulta' );
        $res = $req->request( array(
            'TipoConsulta' => Consulta::TIPOCONSULTA_PRECIOS,
            'CodigoCine'   => $cinema->getLegacyId(),
        ) );
        $this->consoleInfo(sprintf('Tickets: %s', sizeof( $res )));
      } catch ( RequestMsgException $e ) {
        $this->setError($e->getMessage());
        $this->consoleInfo($e->getMessage());
        return FALSE;
      }

      // Set flag is_new = 0 for old tickets.
      $stmt = $conn->prepare( $update_query );
      $stmt->bindValue( 'cinema_id', $cinema->getId() );
      $stmt->execute();

      // Fetch tickets from DB.
      $qb = $doctrine->getManager()->createQueryBuilder();
      $qb->select('t')
          ->from('SocialSnackWsBundle:Ticket', 't')
          ->andWhere( 't.legacy_cinema_id = :legacy_cinema_id' )
          ->setParameter( 'legacy_cinema_id', $cinema->getLegacyId() );
      $tickets_result = $qb->getQuery()->execute();


      foreach ( $res as $_price ) {
        $legacy_id        = (string)$_price->PRICECODE;
        $type_id          = (string)$_price->TICKETTYPECODE;

        if ( !(string)$_price->TICKETTYPEDESCRIPTION || !(string)$_price->CINE || !(string)$_price->TICKETTYPECODE || !(string)$_price->TICKETPRICE ) {
          if (1 === sizeof($res)) {
            $this->alertNoTickets($cinema);
          }

          continue;
        }
        
        $ticket = current( array_filter( $tickets_result, function($a) use($legacy_id, $type_id) {
          return ( $a->getLegacyId() == $legacy_id ) && ( $a->getTypeId() == $type_id );
        } ) );
        if ( !$ticket ) {
          $ticket = new Ticket();
          $em->persist( $ticket );
        }
        $ticket->setLegacyId( $legacy_id );
        $ticket->setLegacyDescription( (string)$_price->TICKETTYPEDESCRIPTION );
        $ticket->setLegacyCinemaId( (string)$_price->CINE );
        $ticket->setTypeId( (string)$_price->TICKETTYPECODE );
        $ticket->setPrice( (string)$_price->TICKETPRICE );
        $ticket->setInfo( json_encode( $_price ) );
        $ticket->setCinema( $cinema );
        if ( !$ticket->getId() ) { // No ID = new record = INSERT.
          $ticket->setDescription( (string)$_price->TICKETTYPEDESCRIPTION );
        }
        $ticket->setIsNew( TRUE );
      }

      $em->flush();

      // Detach the tickets after the task is completed in order to ensure the entities are no longer cached and are
      // refreshed from the DB in case this task is ran again in the same tasks batch.
      $this->detach($tickets_result);

      // Delete old tickets.
      $stmt = $conn->prepare( $delete_query );
      $stmt->bindValue( 'cinema_id', $cinema->getId() );
      $stmt->execute();

      // Free memory.
      $ticket = NULL;
      $cinema = NULL;
      $cinema_repo = NULL;
      $ticket_repo = NULL;
      $res = NULL;
      $req = NULL;
      $em = NULL;
      $doctrine = NULL;
      
    } catch ( \Exception $e ) {
      $this->setError($e->getMessage());
			$this->consoleInfo($this->getError());
			return FALSE;
		}
		
		return TRUE;
  }


  /**
   * Detach from the entity manager all entities in a results collection.
   * @param array $results
   */
  protected function detach($results) {
    $em = $this->doctrine->getManager();
    foreach ($results as $entity) {
      $em->detach($entity);
    }
  }


  protected function alertNoTickets(Cinema $cinema) {
    $this->container->get('logger')->critical(sprintf(
        'WS returned no tickets for cinema %s (%d)',
        $cinema->getName(),
        $cinema->getLegacyId()
    ));
  }

}