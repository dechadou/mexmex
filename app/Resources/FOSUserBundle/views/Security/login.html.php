<?php $view->extend('FOSUserBundle::layout.html.php'); ?>

{% trans_default_domain 'FOSUserBundle' %}

<?php $view['slots']->start( 'fos_user_content' ); ?>

<?php if ( $error ) { ?>
<div><?php echo $error; ?></div>
<?php } ?>

<form action="<?php echo $view['router']->generate( 'fos_user_security_check' ); ?>" method="post">
    <input type="hidden" name="_csrf_token" value="<?php echo $csrf_token; ?>" />

    <label for="username">{{ 'security.login.username'|trans }}</label>
    <input type="text" id="username" name="_username" value="<?php echo $last_username; ?>" required="required" />

    <label for="password">{{ 'security.login.password'|trans }}</label>
    <input type="password" id="password" name="_password" required="required" />

    <input type="checkbox" id="remember_me" name="_remember_me" value="on" />
    <label for="remember_me">{{ 'security.login.remember_me'|trans }}</label>

    <input type="submit" id="_submit" name="_submit" value="{{ 'security.login.submit'|trans }}" />
</form>
<?php $view['slots']->stop(); ?>