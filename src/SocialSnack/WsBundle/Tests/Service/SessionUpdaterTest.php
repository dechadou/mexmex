<?php

namespace SocialSnack\WsBundle\Tests\Service;

use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Service\SessionUpdater;

class SessionUpdaterTest extends \PHPUnit_Framework_TestCase {

  protected $container;

  protected $doctrine;

  protected $ws;

  protected $ticketUpdater;

  protected $logger;

  protected $service;

  protected function setUp() {
    $this->service       = NULL;
    $this->container     = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->disableOriginalConstructor()->getMock();
    $this->doctrine      = $this->getMockBuilder('Doctrine\Bundle\DoctrineBundle\Registry')->disableOriginalConstructor()->getMock();
    $this->ws            = $this->getMockBuilder('SocialSnack\WsBundle\Request\Request')->disableOriginalConstructor()->getMock();
    $this->ticketUpdater = $this->getMockBuilder('SocialSnack\WsBundle\Service\TicketUpdater')->disableOriginalConstructor()->getMock();
    $this->logger        = $this->getMockBuilder('Monolog\Logger')->disableOriginalConstructor()->getMock();

    $this->ticketUpdater->method('execute')->willReturn(TRUE);
  }

  protected function getService() {
    return new SessionUpdater($this->container, $this->doctrine, $this->ws, $this->ticketUpdater, $this->logger);
  }


  protected function getServiceMock(array $methods = NULL, $callConstructor = TRUE) {
    $builder = $this->getMockBuilder('SocialSnack\WsBundle\Service\SessionUpdater');
    $builder->setMethods($methods);

    if (!$callConstructor) {
      $builder->disableOriginalConstructor();
    } else {
      $builder->setConstructorArgs([$this->container, $this->doctrine, $this->ws, $this->ticketUpdater, $this->logger]);
    }

    return $builder->getMock();
  }


  public function testInvalidInput() {
    $service = $this->getService();
    $this->setExpectedException('\InvalidArgumentException');
    $service->execute([]);
  }


  public function testCinemaNotFound() {
    $service = $this->getService();

    $repo = $this->getMockBuilder('SocialSnack\WsBundle\Entity\CinemaRepository')
        ->disableOriginalConstructor()
        ->getMock();

    $this->doctrine->method('getRepository')
        ->with('SocialSnackWsBundle:Cinema')
        ->willReturn($repo);


    $this->setExpectedException('\Exception');
    $service->execute(['cinema_id' => 1]);
  }


  public function testExecute() {
    $service = $this->getServiceMock([
        'updateTickets',
        'updateSessions'
    ]);

    $service->expects($this->once())
        ->method('updateTickets')
        ->willReturn(TRUE);

    $service->expects($this->once())
        ->method('updateSessions')
        ->willReturn(TRUE);

    $output = $service->execute(['cinema_id' => 1]);
    $this->assertTrue($output);
  }


  public function testUpdateTickets() {
    $this->ticketUpdater = $this->getMockBuilder('SocialSnack\WsBundle\Service\TicketUpdater')
        ->disableOriginalConstructor()
        ->setMethods(['execute'])
        ->getMock();
    $this->ticketUpdater->expects($this->once())
        ->method('execute')
        ->willReturn(FALSE); // Return FALSE to stop the process here since we're only testing this call.

    $service = $this->getServiceMock();
    $service->execute(['cinema_id' => 1]);
  }


  protected function setupUpdateSessionsService(array $extraMethods = []) {
    $this->service = $this->getServiceMock(array_merge(
        [
            'updateTickets',
            'getSessions',
            'getActiveSessionIds',
            'getMovies',
            'getTickets',
            'disableOldSessions',
            'logCinemaUpdate',
            'detach'
        ],
        $extraMethods
    ));
  }


  protected function _testUpdateSessions(\SimpleXMLElement $xml, array $sessions, array $movies, array $moviesLegacyIds, $tickets = NULL, array $activeSessions = []) {
    $service = $this->service;

    $service->expects($this->once())
        ->method('updateTickets')
        ->willReturn(TRUE);

    // Mock some entities for the tests.
    $state    = $this->getMock('SocialSnack\WsBundle\Entity\State');
    $cinema   = $this->getMock('SocialSnack\WsBundle\Entity\Cinema');
    $cinema->method('getState')->willReturn($state);

    $cinema_repo = $this->getMockBuilder('SocialSnack\WsBundle\Entity\CinemaRepository')
        ->disableOriginalConstructor()
        ->getMock();
    $cinema_repo->expects($this->once())
        ->method('find')
        ->with($this->equalTo(1))
        ->willReturn($cinema);
    $this->doctrine->expects($this->atLeastOnce())
        ->method('getRepository')
        ->with($this->equalTo('SocialSnackWsBundle:Cinema'))
        ->willReturn($cinema_repo);
    $this->ws->expects($this->once())
        ->method('Request')
        ->willReturn($xml->SESION);

    $service->expects($this->once())
        ->method('getSessions')
        ->with($this->identicalTo($cinema))
        ->willReturn($sessions);
    $service->expects($this->once())
        ->method('getMovies')
        ->with($this->equalTo($moviesLegacyIds))
        ->willReturn($movies);

    $this->service->expects($this->once())
        ->method('getActiveSessionIds')
        ->willReturn($activeSessions);

    if (NULL === $tickets) {
      $ticket1 = ['priceCode' => '$650', 'name' => 'ADULTO'];
      $tickets = [$ticket1];
    }

    if ($tickets !== FALSE) {
      $service->expects($this->atLeastOnce())
          ->method('getTickets')
          ->with($this->identicalTo($cinema))
          ->willReturn($tickets);
    }

    $em = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
    $em->expects($this->atLeastOnce())
        ->method('flush');
    $this->doctrine->method('getManager')
        ->willReturn($em);

    $service->expects($this->once())
        ->method('logCinemaUpdate');

    return $service->execute(['cinema_id' => 1]);
  }


  public function testUpdateSessions_UpdateExisting() {
    $this->setupUpdateSessionsService();

    // Mock some entities for the tests.
    // Mock result to return a session matching the WS results
    $session1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session1->method('getLegacyId')->willReturn(1001);
    // Hardcode a date to make sure is updated later.
    $session1->setDateTime(new \DateTime('2015-01-01 02:00:00'));
    // Hardcode some value to make sure is reseted later.
    $session1->setTypes(['foo']);
    $movie1   = $this->getMock('SocialSnack\WsBundle\Entity\Movie');
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $sessions = [
        $session1
    ];
    $movies = [
        $movie1
    ];

    // Mock response with a new session and an existing session.
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1001</SESSIONID><SESSIONDATE>01/01/2015 02:00:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>Convencional</SCREENTYPE></SESION>
    </RESPUESTA>');
    $moviesLegacyIds = ['FOOBAR666'];

    $this->service->expects($this->once())
        ->method('disableOldSessions')
        ->with($this->equalTo([]));

    $this->service->expects($this->once())
        ->method('detach')
        ->with($this->identicalTo($sessions));

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds);
    $this->assertTrue($output);

    // Check that the existing session was updated.
    $this->assertCount(1, $this->service->getUpdatedSessions());
    $this->assertSame($session1, $this->service->getUpdatedSessions()[0]);
    $this->assertSame('2015-01-01 14:00:00', $session1->getDateTime()->format('Y-m-d H:i:s'));
    $this->assertSame([], $session1->getTypes());
  }


  public function testUpdateSessions_SessionTypes() {
    $this->setupUpdateSessionsService();

    // Mock some entities for the tests.
    // Mock result to return a session matching the WS results
    $session1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session1->method('getLegacyId')->willReturn(1001);
    $movie1   = $this->getMock('SocialSnack\WsBundle\Entity\Movie');
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $sessions = [
        $session1
    ];
    $movies = [
        $movie1
    ];
    $ticket1 = ['priceCode' => '$640', 'name' => 'ADULTO'];
    $ticket2 = ['priceCode' => '$650', 'name' => 'CINEMÁ'];
    $tickets = [$ticket1, $ticket2];

    // Mock response with a new session and an existing session.
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1001</SESSIONID><SESSIONDATE>01/01/2015 02:00:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>Platino</SCREENTYPE></SESION>
    </RESPUESTA>');
    $moviesLegacyIds = ['FOOBAR666'];

    $this->service->expects($this->once())
        ->method('disableOldSessions')
        ->with($this->equalTo([]));

    $this->service->expects($this->once())
        ->method('detach')
        ->with($this->identicalTo($sessions));

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds, $tickets);
    $this->assertTrue($output);

    // Check that the existing session was updated.
    $this->assertCount(1, $this->service->getUpdatedSessions());
    $this->assertSame($session1, $this->service->getUpdatedSessions()[0]);
    $this->assertSame('2015-01-01 14:00:00', $session1->getDateTime()->format('Y-m-d H:i:s'));
    $this->assertSame(['cinemom', 'platinum'], $session1->getTypes());
    // @todo Check session types inherited from tickets.
    // @todo Check session types inherited from hardcoded auditoriums.
  }


  public function testUpdateSessions_NewSession() {
    $this->setupUpdateSessionsService();

    // Mock some entities for the tests.
    // Mock result to return no sessions.
    $sessions = [];
    $movie1   = $this->getMock('SocialSnack\WsBundle\Entity\Movie');
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $movies = [
        $movie1
    ];

    // Mock response with a new session and an existing session.
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1001</SESSIONID><SESSIONDATE>01/01/2015 02:00:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>Convencional</SCREENTYPE></SESION>
    </RESPUESTA>');
    $moviesLegacyIds = ['FOOBAR666'];

    $this->service->expects($this->once())
        ->method('disableOldSessions')
        ->with($this->equalTo([]));

    $this->service->expects($this->once())
        ->method('detach')
        ->with($this->identicalTo($sessions));

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds);
    $this->assertTrue($output);

    // Check if the new session was created.
    $this->assertCount(1, $this->service->getUpdatedSessions());
  }


  public function testUpdateSessions_MovieNotFound() {
    $this->setupUpdateSessionsService([
        'alertMovieNotFound',
    ]);

    // Mock some entities for the tests.
    // Mock result to return a session matching one of the WS results
    // Mock the result matching only one of the queried movies.
    $session1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session1->method('getLegacyId')->willReturn(1001);
    $movie1   = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Movie')->setMethods(['getLegacyId', 'getLegacyIdBis'])->getMock();
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $sessions = [
        $session1,
    ];
    $movies = [
        $movie1
    ];
    $moviesLegacyIds = ['FOOBAR667'];

    // Mock response with a new movie and an existing movie.
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR667</MOVIE><SESSIONID>1002</SESSIONID><SESSIONDATE>27/11/2014 02:35:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>Convencional</SCREENTYPE></SESION>
    </RESPUESTA>');

    $this->service->expects($this->once())
        ->method('alertMovieNotFound');

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds);
    $this->assertTrue($output);
  }


  public function testUpdateSessions_NewMovieVersion() {
    $this->setupUpdateSessionsService();

    // Mock some entities for the tests.
    // Mock result to return a session matching the WS results
    // Mock result with a move lacking the session attributes
    $session1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session1->method('getLegacyId')->willReturn(1001);
    $session2 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session2->method('getLegacyId')->willReturn(1002);
    $movie1   = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Movie')->setMethods(['getLegacyId', 'getLegacyIdBis'])->getMock();
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $sessions = [
        $session1,
        $session2,
    ];
    $movies = [
        $movie1
    ];
    $moviesLegacyIds = ['FOOBAR666'];

    // Mock response with a session with a type different from the provided movie
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1001</SESSIONID><SESSIONDATE>27/11/2014 02:35:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>X4D</SCREENTYPE></SESION>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1002</SESSIONID><SESSIONDATE>27/11/2014 02:35:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>X4D</SCREENTYPE></SESION>
    </RESPUESTA>');

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds);
    $this->assertTrue($output);

    $this->assertEquals(['v4d'], $session1->getTypes());

    // Check the session is associated to a new instance of Movie - not the existing one.
    $this->assertNotSame($movie1, $session1->getMovie());
    // Check the new version is created only once.
    $this->assertSame($session1->getMovie(), $session2->getMovie());
    // Check the new movie has the appropiate attributes.
    $this->assertTrue($session1->getMovie()->getAttr('v4d'));
  }


  public function testUpdateSessions_CX() {
    $this->setupUpdateSessionsService();

    // Mock some entities for the tests.
    $movie1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Movie')->setMethods(['getLegacyId', 'getLegacyIdBis'])->getMock();
    $movie1->method('getLegacyId')->willReturn(1001);
    $movie1->method('getLegacyIdBis')->willReturn('FOOBAR666');
    $movie1->setAttr('cx', TRUE);
    $movie2 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Movie')->setMethods(['getLegacyId', 'getLegacyIdBis'])->getMock();
    $movie2->method('getLegacyId')->willReturn(1002);
    $movie2->method('getLegacyIdBis')->willReturn('FOOBAR667');
    $movie2->setAttr('cx', TRUE);
    $movie2->setAttr('extreme', TRUE);
    $sessions = [];
    $movies = [
        $movie1,
        $movie2,
    ];
    $moviesLegacyIds = ['FOOBAR666', 'FOOBAR667'];

    // Mock response with a session with a type different from the provided movie
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR666</MOVIE><SESSIONID>1001</SESSIONID><SESSIONDATE>27/11/2014 02:35:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>CX</SCREENTYPE></SESION>
        <SESION><CINE>1233</CINE><MOVIE>FOOBAR667</MOVIE><SESSIONID>1002</SESSIONID><SESSIONDATE>27/11/2014 02:35:00 p.m.</SESSIONDATE><SEATSAVAILABLE>139</SEATSAVAILABLE><SEATALLOCATIONON>Y</SEATALLOCATIONON><PRICECODE>$650</PRICECODE><CHILDALLOWED>Y</CHILDALLOWED><CINECODE>CNA</CINECODE><FORMATVIDEO>DIGITAL</FORMATVIDEO><IDIOMA>Inglés</IDIOMA><SCREEN>10</SCREEN><SCREENNAME>Sala 10</SCREENNAME><SCREENTYPE>CX</SCREENTYPE></SESION>
    </RESPUESTA>');

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds);
    $this->assertTrue($output);

    $this->assertSame($this->service->getUpdatedSessions()[0]->getMovie(), $movie1);
    $this->assertSame($this->service->getUpdatedSessions()[1]->getMovie(), $movie2);
  }


  public function testUpdateSessions_NoSessionsInWS() {
    $this->setupUpdateSessionsService([
        'alertNoSessions',
    ]);

    // Mock some entities for the tests.
    $session1 = $this->getMockBuilder('SocialSnack\WsBundle\Entity\Session')->setMethods(['getLegacyId'])->getMock();
    $session1->method('getId')->willReturn(123);
    $session1->method('getLegacyId')->willReturn(1001);
    $sessions = [
        $session1,
    ];
    $movies = [];
    $moviesLegacyIds = [''];
    $activeSessions = [$session1->getId()];

    // Mock response with no sessions. Yes. The WS actually returns this when there're no sessions.
    $xml = new \SimpleXMLElement('<RESPUESTA>
        <SESION><CINE></CINE><MOVIE></MOVIE><SESSIONID></SESSIONID><SESSIONDATE></SESSIONDATE><SEATSAVAILABLE></SEATSAVAILABLE><SEATALLOCATIONON></SEATALLOCATIONON><PRICECODE></PRICECODE><CHILDALLOWED></CHILDALLOWED><CINECODE></CINECODE><FORMATVIDEO></FORMATVIDEO><IDIOMA></IDIOMA><SCREEN></SCREEN><SCREENNAME></SCREENNAME><SCREENTYPE></SCREENTYPE></SESION>
    </RESPUESTA>');

    $this->service->expects($this->once())
        ->method('disableOldSessions')
        ->with($this->equalTo([$session1->getId()]));

    $this->service->expects($this->once())
        ->method('alertNoSessions');

    $output = $this->_testUpdateSessions($xml, $sessions, $movies, $moviesLegacyIds, FALSE, $activeSessions);
    $this->assertCount(0, $this->service->getUpdatedSessions());
    $this->assertTrue($output);
  }

}