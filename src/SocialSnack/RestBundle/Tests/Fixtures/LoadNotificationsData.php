<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Notification;

class LoadNotificationsData implements FixtureInterface {

  static $notifications;

  public function load(ObjectManager $manager) {
    self::$notifications = [];

    $noti1 = new Notification();
    $noti1
        ->setContent('Lorem ipsum')
        ->setDate(new \DateTime('now'))
        ->setDeepLink('cinemex://test/123')
        ->setTitle('Notification 1')
    ;
    self::$notifications[] = $noti1;
    $manager->persist($noti1);

    $manager->flush();
  }

}