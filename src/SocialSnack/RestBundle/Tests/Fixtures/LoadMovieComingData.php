<?php
namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\MovieComing;

class LoadMovieComingData implements FixtureInterface {

  public function load(ObjectManager $manager) {
    for ($i = 1 ; $i <= 10 ; $i++) {
      $movie = new MovieComing();
      $movie->setName('Movie Coming ' . $i);
      $movie->setActive(TRUE);
      $movie->setReleaseDate(new \DateTime('+1 month'));
      $movie->setInfo(json_encode(array(
        'SINOPSIS' => 'Lorem ipsum...',
      )));
      $manager->persist($movie);
    }

    $manager->flush();
  }

}
