<?php
$title = $promo->getId()
    ? 'Editar promo destacada <strong>' . $promo->getTitle() . '</strong>'
    : 'Nueva promo destacada';
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', strip_tags($title) . ' - Cinemex');
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <?php echo $title; ?>
      </header>

      <div class="panel-body">
        <?php if (sizeof($submit_msg)) { ?>
          <div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
        <?php } ?>

        <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
        <?php echo $view['form']->form($form); ?>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>