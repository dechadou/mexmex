<?php

namespace SocialSnack\FrontBundle\Service;

use SocialSnack\WsBundle\Entity\Cinema;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper as TemplatingHelper;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class Utils extends TemplatingHelper {
  
  static $poster_sizes = array(
      '200x295'        => array('w' => 200,  'h' => 295,  'ext' => 'jpg'),
      '187x276'        => array('w' => 187,  'h' => 276,  'ext' => 'jpg'),
      'passbook_th'    => array('w' => 61,   'h' => 90,   'ext' => 'png'),
      'passbook_th_2x' => array('w' => 122,  'h' => 180,  'ext' => 'png'),
      'single'         => array('w' => 182,  'h' => 272,  'ext' => 'jpg'),
      'checkout'       => array('w' => 164,  'h' => 245,  'ext' => 'jpg'),
      'grid'           => array('w' => 154,  'h' => 230,  'ext' => 'jpg'),
      'confirmation'   => array('w' => 95,   'h' => 142,  'ext' => 'jpg'),
      'web_mobile'     => array('w' => 67,   'h' => 99,   'ext' => 'jpg'),
      'billboard'      => array('w' => 60,   'h' => 89,   'ext' => 'jpg'),
      'small'          => array('w' => 360,  'h' => 540,  'ext' => 'jpg'),
      'medium'         => array('w' => 750,  'h' => 1125, 'ext' => 'jpg'),
      'big'            => array('w' => 1080, 'h' => 1620, 'ext' => 'jpg'),
  );

  static $avatar_sizes = array(
      'default' => array('w' => 64,  'h' => 64,  'ext' => 'jpg'),
      'medium'  => array('w' => 360, 'h' => 360, 'ext' => 'jpg'),
  );
  
  protected $container;
  
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  public function getName() {
    return 'fronthelper';
  }

  /**
   * List the enabled static filesystems available.
   *
   * @return array
   */
  protected function get_statics_fss() {
    return $this->container->getParameter('static_fss_enabled');
  }

  /**
   * @param \SocialSnack\FrontBundle\Entity\User $user
   * @return array
   */
  public function get_user_login_data( $user ) {
    /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
    $cinema = $user->getCinema();
    $res    = array();

    // User entity info.
    $res['info'] = array(
        'id'               => $user->getId(),
        'iecode'           => $user->getIecode(),
        'fbuid'            => $user->getFbuid(),
        'first_name'       => $user->getFirstName(),
        'last_name'        => $user->getLastName(),
        'avatar'           => $user->getAvatar(),
    );

    $res['registered'] = TRUE;

    if ( $cinema ) {
      $res['preferred_cinema'] = $cinema->toArray();
    }

    $header_box = $this->container->get( 'templating' )->render(
        'SocialSnackFrontBundle:User:header_box.html.php',
        array( 'user' => $user )
    );

    $res['template'] = array(
        'header-user' => $header_box
    );
    
    return $res;
  }
  
  
  /**
   * Return the path to an image asset with the specified size in the filename.
   * 
   * @param string $url     Original URL.
   * @param string $size    Requested size.
   * @return string
   */
  public function get_img_size_path($url, $size, $package = NULL) {
    if ( !$url ) {
      return FALSE;
    }
    
    if ( $size ) {
      $pathinfo = pathinfo($url);
      
      if (isset(self::$poster_sizes[$size])) {
        $_size = self::$poster_sizes[$size];
        $size  = $_size['w'] . 'x' . $_size['h'];
        $ext   = $_size['ext'];
      } else {
        $ext = $pathinfo['extension'];
      }
      
      $url = ($pathinfo['dirname'] ? $pathinfo['dirname'] . '/' : '')
          . $pathinfo['filename'] . '-' . $size . '.' . $ext;
    }
    
    return $url;
  }
  
  
  /**
   * Return the URL to an image asset with the specified size in the filename.
   * 
   * @param string $url     Original URL.
   * @param string $size    Requested size.
   * @param string $package The name of the asset package to use
   * @return string A public path which takes into account the base path, the size and URL path
   */
  public function get_img_size($url, $size, $package = NULL) {
    $assets = $this->container->get('templating.helper.assets');
    $url = $this->get_img_size_path($url, $size);
    
    return $assets->getUrl($url,$package);
  }
  
  /**
   * Return the full URL for an user avatar.
   * 
   * @param string $avatar
   * @return string
   */
  public function get_avatar_url( $avatar, $size = '' ) {
    if ( preg_match( '/\/\//', $avatar ) )
      return $avatar;
		
		if ( $avatar == 'default-avatar.png' )
			return $this->container->getParameter( 'static_url' ) . 'assets/img/' . $avatar;
    
    /** @todo Implement $this->get_img_size() instead of using duplicated code. */
    if ( $size ) {
      $pathinfo = pathinfo($avatar);
      $filename = $pathinfo['filename'];
      $avatar = $filename . '-' . $size . '.jpg';
    }
    
    return $this->container->getParameter( 'avatars_url' ) . $avatar;
  }
  
  
  /**
   * Return the full URL for a movie poster.
   * 
   * @param string $poster
   * @return string
   */
  public function get_poster_url( $poster, $size = '' ) {
    return $this->get_img_size($poster, $size, 'MoviePosters');
  }
  
  /**
   * Return the full URL for a cms content
   * 
   * @param string $poster
   * @return string
   */
  public function get_cms_url( $poster, $size = '' ) {
    if ( preg_match( '/\/\//', $poster ) )
      return $poster;

      /** @todo Implement sizes */
    if ($size) {
      $parts = pathinfo($poster);
      if (!isset($parts['extension'])) {
        return FALSE;
      }
      $poster = $parts['dirname']."/".$parts['filename']."-".$size.".".$parts['extension'];
    }

    return $this->container->getParameter( 'cms_upload_url' ) . $poster;
  }
  
  
  public function get_static_url( $path, $absolute = FALSE ) {
    return $this->container->getParameter( $absolute ? 'static_abs_url' : 'static_url' ) . $path;
  }


  public function get_email_banner_url() {
    return $this->getOptions('email_banner_path');
  }
  
  
  /**
   * Return the movie poster URL for the requested poster size.
   * If the movie has a parent, return the parent's poster.
   * 
   * @param \SocialSnack\WsBundle\Entity\Movie $movie
   * @param string $size
   */
  public function get_movie_poster_url($movie, $size = '') {
    if ( $movie->getParent() && $movie->getParent()->getId() ) {
      $poster_url = $movie->getParent()->getPosterUrl();
    } else {
      $poster_url = $movie->getPosterUrl();
    }
    return $this->get_poster_url($poster_url, $size);
  }
  
  
  public function get_fb_app_id() {
    $fb = $this->container->getParameter( 'fb' );
    return $fb['app_id'];
  }
  
  
  public function get_gmaps_key() {
    return $this->container->getParameter( 'google_maps_key' );
  }
  
  
  public function get_permalink($entity, $absolute = FALSE, array $params = []) {
    $router = $this->container->get('router');
    $class = get_class($entity);
    
    // Not sure why sometimes the entity is a proxy but... just in case, let's handle that.
    $class = preg_replace("/^Proxies\\\\__CG__\\\\/", '', $class);
    
    switch ($class) {
      case 'SocialSnack\FrontBundle\Entity\Article':
        /* @var $entity \SocialSnack\FrontBundle\Entity\Article */
        $route_name = 'news_single';
        $params     = array(
            'id' => $entity->getId(),
            'slug' => FrontHelper::sanitize_for_url($entity->getTitle())
        );
        break;
      
      case 'SocialSnack\WsBundle\Entity\Movie':
        /* @var $entity \SocialSnack\WsBundle\Entity\Movie */
        $route_name = 'movie_single';
        $params     = array(
            'movie_id' => $entity->getId(),
            'slug' => FrontHelper::sanitize_for_url($entity->getName())
        );
        break;
      
      case 'SocialSnack\WsBundle\Entity\MovieComing':
        /* @var $entity \SocialSnack\WsBundle\Entity\MovieComing */
        $route_name = 'moviecoming_single';
        $params     = array(
            'movie_id' => $entity->getId(),
            'slug' => FrontHelper::sanitize_for_url($entity->getName())
        );
        break;
      
      case 'SocialSnack\WsBundle\Entity\Cinema':
        /* @var $entity \SocialSnack\WsBundle\Entity\Cinema */
        $route_name = 'cinema_single';
        $params     = array(
            'cinema_id' => $entity->getId(),
            'slug' => FrontHelper::sanitize_for_url($entity->getName())
        );
        break;

      case 'SocialSnack\FrontBundle\Entity\Landing':
        /* @var $entity \SocialSnack\FrontBundle\Entity\Landing*/
        $route_name = 'front_landing';
        $params     = array_merge(array(
            'slug' => FrontHelper::sanitize_for_url($entity->getName()),
        ), $params);
        break;

      case 'SocialSnack\FrontBundle\Entity\LandingPage':
        /* @var $entity \SocialSnack\FrontBundle\Entity\LandingPage */
        $route_name = 'front_landing_page';
        $params     = array_merge(array(
            'slug' => FrontHelper::sanitize_for_url($entity->getLanding()->getName()),
            'page_slug' => FrontHelper::sanitize_for_url($entity->getName()),
            'args' => '',
        ), $params);
        break;
    }
    
    if (isset($route_name)) {
      return $router->generate($route_name, $params, $absolute);
    }
  } 
  
  public function get_movie_link( $movie, $route = 'movie_single', $absolute = FALSE ) {
    return $this->container->get( 'router' )->generate( $route, array(
        'movie_id' => $movie->getId(),
        'slug'     => FrontHelper::sanitize_for_url( $movie->getName() )
    ), $absolute );
  }
  
  public function get_movie_link_abs( $movie, $route = 'movie_single' ) {
    return $this->get_movie_link($movie, $route, TRUE);
  }
  
  public function get_cinema_link( $cinema ) {
    return $this->get_permalink($cinema);
  }

  public function get_analytics_id() {
    return $this->container->getParameter('google_analytics_id');
  }


  
  /**
   * Upload CMS content.
   * 
   * @todo   Use a more efficient system to replicate this across the nodes
   * 
   * @param  string $filename  Destination filename
   * @param  string $bytes     File contents.
   * @param  string	$base	     Path to file
   * @param  int	  $width     Image width
   * @param  int	  $height    Image height
   * @param  string $extension Resized image extension
   * @return boolean
   */
  public function upload_cms_content($filename, $bytes, $base, $width = NULL, $height = NULL, $extension = NULL) {
    $filename = $this->container->getParameter('cms_upload_path') . $base . $filename;
    $statics   = $this->get_statics_fss();
    $overwrite = TRUE;

    $sizes = [
        array(), // Keep original image.
        array('w' => 100, 'h' => 100, 'ext' => 'jpg'), // Default backend thumb.
    ];
    if ($width && $height) {
      $sizes[] = array('w' => $width, 'h' => $height, 'ext' => $extension ?: 'jpg');
    }
    return $this->resize_n_upload_to_cdn(
      $filename,
      $bytes,
      $statics,
      $sizes,
      $overwrite
    );
  }
 
  /**
   * Process ARCO form submitted data.
   * Handle files upload, etc.
   * 
   * @param type $data
   */
  public function process_arco_form($form, &$data) {
    $uploads_path = $this->container->getParameter('arco_upload_path');
    $choices = array(
        'Tipo_RelacionCMX', 'SolicitaComplejo', 'CveDerechoAccDat', 'Estado', 'Identificacion'
    );
    
    foreach ( $data as $field_name => &$field ) {
      // Process nested forms.
      if ( is_array( $field ) ) {
        $this->process_arco_form($form->get($field_name), $field);
        continue;
      }
      
      if ( in_array($field_name, $choices) ) {
        $_choices = $form->get($field_name)->getConfig()->getOption('choices');
        if ( !$field || !isset($_choices[$field]) ) {
          $data[$field_name] = '';
        } else {
          $data[$field_name] = $_choices[$field];
        }
      }
      
      // Normalize dates to strings.
      if ( $field instanceof \DateTime ) {
        $data[$field_name] = $field->format('d-m-Y');
        continue;
      }
      
      // Handle file uploads.
      if ( $field instanceof \Symfony\Component\HttpFoundation\File\UploadedFile ) {
        $file = $field;

        if ( !$file->isValid() )
          continue;

        $extension = $file->guessExtension();
        if ( !$extension ) {
          $extension = '___';
        }
        $filename = gethostname() . uniqid($field_name, TRUE) . '.' . $extension;
        
        // Write file to disk.
        $file->move($uploads_path, $filename);
        
        // Pseudo-load-balance betweet static servers.
        $statics = $this->get_statics_fss();
        $static = $statics[array_rand($statics)];
        $base_url = 'http://' .
            $this->container->getParameter($static . '.host.public') .
            $this->container->getParameter($static . '.path') .
            $uploads_path;
        
        $fs = $this->container->get('knp_gaufrette.filesystem_map')->get($static);
        $fs->write($uploads_path . $filename, file_get_contents($uploads_path . $filename));

        $data[$field_name] = array(
            'host'     => gethostname(),
            'filename' => $filename,
            'url'      => $base_url . $filename,
        );
        
        continue;
      }
    }
  }
  
  
  
  /**
   * Upload movie poster to CDN.
   * 
   * @todo   Use a more efficient system to replicate this across the nodes
   * 
   * @param  string $filename Destination filename (without path).
   * @param  string $bytes    File contents.
   * @return boolean
   */
  public function upload_movie_poster($filename, $bytes) {
    $statics  = $this->get_statics_fss();
    $sizes    = array_values(self::$poster_sizes);
    array_splice($sizes, 0, 0, array(array())); // Empty array = Original size. No resize.
	
    return $this->resize_n_upload_to_cdn(
      'movie_posters/' . $filename,
      $bytes,
      $statics, 
      $sizes
    );
  }
  
  /**
   * Upload an image without resize CDN.
   * 
   * @todo   Use a more efficient system to replicate this across the nodes
   * 
   * @param  string $filename Destination filename (without path).
   * @param  string $bytes    File contents.
   * @param  string $base     Base path.
   * @return boolean
   */
  public function upload_without_rezise($filename, $bytes, $base) {
    $statics  = $this->get_statics_fss();

    return $this->upload_to_cdn(
      $base . $filename,
      $bytes,
      $statics,
      true
    );
  }
  
  
  public function random_string($length = 15) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    srand((double)microtime()*1000000);
    $string = '';
    for ($i=0; $i<$length; $i++){
       $string .= substr ($chars, rand() % strlen($chars), 1);
    }
    return $string;
  }
  
  /**
   * Make random movie poster file name.
   * 
   * 
   * @param  int $length 	String length
   * @return string
   */
  public function name_movie_poster($length = 15) {
    return $this->random_string($length) . '.jpg';
  }
  
  
  /**
   * @param string  $filename
   * @param string  $bytes
   * @param array   $filesystems
   * @param boolean $overwrite
   * @return boolean
   */
  public function upload_to_cdn($filename, $bytes, $filesystems, $overwrite = FALSE) {
    $success = TRUE;
    
    if (is_string($filesystems)) {
      $filesystems = array($filesystems);
    }
    
    foreach ( $filesystems as $fs_id ) {
      $fs = $this->container->get('knp_gaufrette.filesystem_map')->get($fs_id);
      $_success = $fs->write($filename, $bytes, $overwrite);
      
      if ( !$_success ) {
        $success = FALSE;
      }
    }
    
    return $success;
  }
  
  
  /**
   * @param string  $filename
   * @param string  $bytes
   * @param array   $filesystems
   * @param array   $sizes
   * @param boolean $overwrite
   * @return boolean
   */
  public function resize_n_upload_to_cdn($filename, $bytes, $filesystems, $sizes, $overwrite = FALSE) {
    $uploaded  = 0;
    $resizer   = $this->container->get('ss.image_resize');
    $pathinfo  = pathinfo($filename);
    $base_name = $pathinfo['filename'];
    
    $resizer->loadData($bytes);
    
    foreach ( $sizes as $size ) {
      if ( isset($size['w'], $size['h']) ) {
        $size_name = '-' . $size['w'] . 'x' . $size['h'];
        if ('auto' === $size['w']) {
          $resize_method = 'portrait';
        } elseif ('auto' === $size['h']) {

          $resize_method = 'landscape';
        } else {
          $resize_method = 'crop';
        }
        $resizer->resizeImage($size['w'], $size['h'], $resize_method);
        $size_data = $resizer->getResizedData($size['ext'], 85);
        $_filename = $base_name . $size_name . '.' . $size['ext'];
      } else {
        $size_data = $bytes;
        $_filename = $pathinfo['basename'];
      }
      $success = $this->upload_to_cdn(
          $pathinfo['dirname'] . '/' . $_filename,
          $size_data,
          $filesystems,
          $overwrite
      );
      if ( $success ) {
        $uploaded++;
      }
    }
    
    return $uploaded == sizeof($sizes);
  }
  
  
  /**
   * @param string $filename
   * @param array  $filesystems
   * @return boolean
   */
  public function delete_from_cdn($filename, $filesystems) {
    $success = TRUE;
    
    foreach ( $filesystems as $fs_id ) {
      $fs = $this->container->get('knp_gaufrette.filesystem_map')->get($fs_id);
      $_success = $fs->delete($filename);
      
      if ( !$_success ) {
        $success = FALSE;
      }
    }
    
    return $success;
  }
 
  
  /**
   * Handle user's avatar upload.
   * Resize and upload to CDN.
   * 
   * @todo Delete old the sizes from previous avatar.
   * 
   * @param \SocialSnack\FrontBundle\Entity\User $user
   * @return boolean
   */
  public function upload_avatar(&$user) {
    $uploaded = FALSE;
    $statics  = $this->get_statics_fss();
    $sizes    = array_values(self::$avatar_sizes);
    array_splice($sizes, 0, 0, array(array())); // Empty array = Original size. No resize.
    
    try {
      $base_name = sha1(gethostname().uniqid(mt_rand(), true));
      $extension = $user->getFile()->guessExtension();
      $data      = file_get_contents($user->getFile()->getRealPath());
      $filename  = $base_name . '.' . $extension; 
      
      $uploaded = $this->resize_n_upload_to_cdn('uploads/avatars/' . $filename, $data, $statics, $sizes);
    } catch ( \Exception $e ) {}
    
    if ( $uploaded ) {
      $user->setAvatar($filename);
      
      if ( $user->getTempAvatar() ) {
        try {
          $this->delete_from_cdn(
            'uploads/avatars/' . $user->getTempAvatar(),
            $statics
          );
        } catch ( \Exception $e ) {}
      }
    } else {
      $user->setAvatar('default-avatar.png');
    }
    
    return $uploaded;
  }
  
  /**
   * Check if the cinema is the list of cinemas with capability for QR codes.
   * 
   * @param Cinema $cinema
   * @return boolean
   */
  public function cinema_has_qr(Cinema $cinema) {
    $ids = [
        1032, 1207, 1227, 1033, 1027, 1171, 1044, 1199, 1025, 1030, 1177, 1024, 1040, 1028, 1208, 1043, 1042, 1217,
        1067, 1055, 1056, 1035, 1001, 1019, 1013, 1029, 1200, 1037, 1168, 1154, 1018, 1206, 1016, 1026, 1031, 1209,
        1007, 1276, 1277, 1172, 1011, 1235, 1245, 1247, 1258
    ];
    return in_array($cinema->getLegacyId(), $ids);
  }
  
  
  public function build_dyn_css() {
    $bgcolor = $this->getOptions('bgcolor');
    if ('#' != substr($bgcolor, 0, 1)) {
      $bgcolor = '#' . $bgcolor;
    }
    $file_name = 'assets/css/dyn-styles.css';
    $file_content = 'body.sponsored{background: ' . $bgcolor . ';}';
    $statics = $this->get_statics_fss();
    return $this->upload_to_cdn($file_name, $file_content, $statics, TRUE);
  }
 
  
  public function get_shard_static($criteria) {
    $statics = $this->get_statics_fss();
    $i = $criteria % 2
        ? 0
        : 1
    ;
    return $statics[$i];
  }
 
  
  public function getTransactionsTotal() {
    $conn = $this->container->get('doctrine')->getManager()->getConnection();
    $query = "SELECT COUNT(*) FROM Transaction";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    return (int)$stmt->fetchColumn();
  }


  public function getMobileOS($userAgent) {
    // Check Windows first because IE11 fakes its UA by including other UA's strings on it too.
    if (strstr($userAgent, 'Windows Phone')) {
      return 'windows';
    }

    if (strstr($userAgent, 'iPhone') || strstr($userAgent,'iPod')) {
      return 'ios';
    }

    if (stripos($userAgent, 'android')) {
      return 'android';
    }

    return NULL;
  }


  public function getOptions($key) {
    $options = $this->container->get('dyn_options');
    return $options->get($key);
  }

}
