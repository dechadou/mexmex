<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArcoRepType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name',  null,    array(
            'label' => 'Primer nombre',
            'required' => FALSE,
            'constraints' => array(
                new NotBlank(array('groups' => array('rep'))),
            ),
        ));
        $builder->add('middle_name', null,    array(
            'label' => 'Segundo nombre',
            'required' => FALSE,
        ));
        $builder->add('last_name',   null,    array(
            'label' => 'Apellido paterno',
            'required' => FALSE,
            'constraints' => array(
                new NotBlank(array('groups' => array('rep'))),
            ),
        ));
        $builder->add('mlast_name',  null,    array(
            'label' => 'Apellido materno',
            'required' => FALSE,
        ));
        $builder->add('email',       'email', array(
            'label' => 'Email',
            'required' => FALSE,
            'constraints' => array(
                new NotBlank(array('groups' => array('rep'))),
                new Email(array('groups' => array('rep'))),
            ),
        ));
    }

    public function getName()
    {
        return 'arco_rep';
    }
}