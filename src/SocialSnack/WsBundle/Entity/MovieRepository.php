<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\EntityRepository;

use SocialSnack\WsBundle\Service\Helper as WsHelper;

class MovieRepository extends EntityRepository {


  protected function _apply_default_order( &$qb ) {
    //$qb->addOrderBy( 'm.premiere', 'DESC' )
        $qb->addOrderBy( 'm.fixed_position', 'DESC' )
        ->addOrderBy( 'm.position', 'DESC' )
        ->addOrderBy( 'm.name', 'ASC' );
  }

  /**
   * Implementation of the find by method.
   * Unless asking for a specific ID it will only return movies without parent.
   * Default order applied.
   *
   * @param array $criteria
   * @param array $orderBy
   * @param int $limit
   * @param int $offset
   * @param boolean $no_children
   * @return type
   * @throws \Exception
   */
  protected function _find(array $criteria = array(), array $orderBy = null, $limit = null, $offset = null, $no_children = TRUE) {
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->addSelect( 'm' )
				->from( 'SocialSnackWsBundle:Movie', 'm' );

    if ( !isset( $criteria['id'] ) ) {
      $qb->andWhere( 'm.active = 1' );

      // If an ID was provided as criteria, don't mind if it has parent or not.
      if ( $no_children ) {
        $qb->andWhere( $qb->expr()->isNull( 'm.parent' ) )
            ->andWhere( 'm.position > 0' );
      }
    }

    // Apply where criterias.
    foreach ( $criteria as $k => $v ) {
      if ( substr($v, 0, 2) == '>=' ) {
        $qb->andWhere( $qb->expr()->gte( "m.$k", substr($v, 2) ) );
      } elseif ( substr($v, 0, 1) == '>' ) {
        $qb->andWhere( $qb->expr()->gt( "m.$k", substr($v, 1) ) );
      } else {
        $qb->andWhere( "m.$k = :$k" )
            ->setParameter( "$k", $v );
      }
    }

    if ( $orderBy ) {
      /* @todo Implement this */
      throw new \Exception( 'Order by not implemented.' );
    }

    // Default order for billboard.
    $this->_apply_default_order( $qb );

    return $qb;
  }


  /**
   * Implementation of the find by method.
   * Unless asking for a specific ID it will only return movies without parent.
   * Default order applied.
   *
   * @param array $criteria
   * @param array $orderBy
   * @param type $limit
   * @param type $offset
   * @param boolean $no_children
   * @return array
   * @throws \Exception
   */
  public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null, $no_children = TRUE) {
		$qb    = $this->_find( $criteria, $orderBy, $limit, $offset, $no_children );
    $query = $qb->getQuery();

    $cache_id = implode( '.', array(
        @http_build_query($criteria),
        @http_build_query($orderBy),
        (int)$limit,
        (int)$offset,
        (int)$no_children
    ) );
    $query->useResultCache( TRUE, 60 * 60, 'movie_findby_' . $cache_id );
    return $query->getResult();
  }


  public function findAll() {
    return $this->findBy(array());
  }


  public function findByAttr( $attributes, array $in_states = array() ) {
    if (empty($in_states)) {
      $qb = $this->_find( array(), null, null, null, true );
    } else {
      // we need to join with sessions so we need to use childrens
      $qb = $this->_find( array(), null, null, null, false );

      $where_in = $qb->expr()->in('c.state', $in_states);

      $qb->innerJoin('m.sessions', 's')
        ->innerJoin('m.parent', 'p')
        ->innerJoin('s.cinema', 'c')
        ->andWhere('s.active = 1')
        ->andWhere($where_in)
        ->groupBy('p');
    }

    WsHelper::dql_apply_movie_attrs( $qb, $attributes );

    $query = $qb->getQuery();

    $cache_id = @http_build_query( $attributes );
    $query->useResultCache( TRUE, 60 * 60, 'movie_findbyattr_' . $cache_id );

    return $query->getResult();
  }


	public function findManyBy( $ids, $criteria = array() ) {
		if ( 1 == sizeof( $ids ) ) {
			return $this->findBy( array_merge( array( 'id' => $ids[0] ), $criteria ) );
		}

		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->addSelect( 'm' )
				->from( 'SocialSnackWsBundle:Movie', 'm' )
				->where(
						$qb->expr()->in( 'm.id', $ids )
				);

		if ( $criteria ) {
			foreach ( $criteria as $k => $v ) {
				$qb->andWhere( 'm.' . $k . ' = :' . $k )
						->setParameter( $k, $v );
			}
		}

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		return $query->getResult();
	}

  public function findInactive() {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('m')->from('SocialSnackWsBundle:Movie', 'm');
    $qb->andWhere('m.active = 0');
    $qb->expr()->isNull( 'm.parent' );
    $qb->orderBy('m.id', 'DESC');
    $query = $qb->getQuery();
    return $query->getResult();
  }

  /**
   * List all the active movies with certain attributes
   * ordered by its session date
   *
   * @param  array  $attributes Associative array of attrs
   * @return array
   */
  public function findByAttrOrderBySessions(array $attributes) {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb
        ->addSelect('m')
        ->from('SocialSnackWsBundle:Movie', 'm')
        ->innerJoin('SocialSnackWsBundle:Session', 's', 'WITH', 'm.group_id = s.group_id')
        ->andWhere('s.active = 1')
        ->andWhere('m.active = 1')
        ->andWhere($qb->expr()->isNull('m.parent'))
        ->groupBy('m.id')
        ->addOrderBy('s.date', 'ASC')
        ->addOrderBy('s.time', 'ASC')
    ;

    WsHelper::dql_apply_dates($qb, new \DateTime('now'), 0);
    WsHelper::dql_apply_movie_attrs($qb, $attributes);

    $query = $qb->getQuery();

    $query->useResultCache(TRUE, 60 * 60);

    return $query->getResult();
  }

  /**
   * List all the available movies AKA billboard with movies playing now or future.
   *
   * @return array
   */
	public function findAvailable() {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'm' )
				->from( 'SocialSnackWsBundle:Movie', 'm' )
				->innerJoin( 'm.sessions', 's' )
        ->groupBy( 'm.id' );

		WsHelper::dql_apply_dates( $qb, new \DateTime( 'now' ), 0 );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		$movies = $query->getResult();

    // Remove grouped movies from level 0 and add their parents instead.
    $parent_ids = array();
    foreach ( $movies as $i => &$movie ) {
      if ( $movie->getParent() && $movie->getParent()->getId() ) {
        $parent_ids[] = $movie->getParent()->getId();
        unset( $movies[$i] );
      }
    }

    $parent_ids = array_unique($parent_ids);
    $parent_ids = array_filter( $parent_ids );
    if ($parent_ids) {
      $parents = $this->findManyBy($parent_ids);
    } else {
      $parents = array();
    }
    $movies = array_merge( $movies, $parents );

    WsHelper::sortMoviesDefault($movies);

    return $movies;
	}

	/**
	 * Return a list with all the future sessions dates for a movie in a cinema.
	 *
	 * @param type $movie
	 * @param type $cinema_id
	 * @return array
	 */
	public function findAvailableDatesInCinema( $movie, $cinema_id ) {
		/* @todo Define the right timezone */
//		date_default_timezone_set("UTC");

		$date_from = new \DateTime( 'now' );

		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb
				->addSelect( 's.date' )
				->from( 'SocialSnackWsBundle:Session', 's' )
				->where( 's.date_time >= :date_from' )
				->setParameter( 'date_from', $date_from->format( 'Y-m-d H:i:s' ) )
        ->andWhere( 's.active = 1' );

    if ( is_array( $cinema_id ) ) {
      $qb->andWhere( $qb->expr()->in( 's.cinema', $cinema_id ) );
    } else {
      $qb->andWhere( 's.cinema = :cinema_id' )
          ->setParameter( 'cinema_id', $cinema_id );
    }

    WsHelper::dql_apply_movie( $qb, $movie, 's' );

		$qb
				->groupBy( 's.date' )
				->orderBy( 's.date' );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 15 * 60 );
		return $query->getResult();
	}

	/**
	 * Find movie sessions in cinema today.
	 *
	 * @param integer $cinema_id
	 * @return type
	 */
	public function findMovieSessionsInCinemaToday( $movie_id, $cinema_id ) {
		/* @todo Define the right timezone */
//		date_default_timezone_set("UTC");

		return $this->_findSessionsInCinemaByDate( $cinema_id, new \DateTime( 'now' ), NULL, $movie_id );
	}


	/**
	 * Find movie sessions in cinema today.
	 *
	 * @param integer $cinema_id
	 * @return type
	 */
	public function findMovieSessionsInCinemaByDate( $movie_id, $cinema_id, $date_from, $date_to = NULL  ) {
		return $this->_findSessionsInCinemaByDate( $cinema_id, $date_from, $date_to, $movie_id );
	}


	/**
	 * Find all sessions in cinema today.
	 *
	 * @param integer $cinema_id
	 * @return type
	 */
	public function findAllSessionsInCinemaToday( $cinema_id ) {
		/* @todo Define the right timezone */
//		date_default_timezone_set("UTC");

		return $this->_findSessionsInCinemaByDate( $cinema_id, new \DateTime( 'now' ) );
	}


	/**
	 * Find all sessions in cinema by date.
	 *
	 * @param integer $cinema_id
	 * @param string $date_from
	 * @param string $date_to
	 * @return type
	 */
	public function findAllSessionsInCinemaByDate( $cinema_id, $date_from, $date_to = NULL ) {
		return $this->_findSessionsInCinemaByDate( $cinema_id, $date_from, $date_to );
	}


	/**
	 * Find all sessions in cinema by date.
	 *
	 * @param integer $state_id
	 * @param string $date_from
	 * @param string $date_to
	 * @return type
	 */
	public function findAllSessionsInStateByDate( $state_id, $date_from, $date_to = NULL ) {
		return $this->_findSessionsInCinemaByDate( NULL, $date_from, $date_to, NULL, $state_id );
	}


	/**
	 * Find sessions in cinema by date and/or by movie.
	 *
	 * @param integer $cinema_id
	 * @param string $date_from
	 * @param string $date_to
	 * @param integer $movie
	 * @param integer $state_id
	 * @param integer $area_id
	 * @return type
	 */
	protected function _findSessionsInCinemaByDate( $cinema_id, $date_from, $date_to = NULL, $movie = NULL, $state_id = NULL, $area_id = NULL ) {
    $_sessions = $this->_findSessionsByDate( $date_from, $date_to, $cinema_id, $movie, NULL, $state_id );
    $sessions  = array();
		$groups    = array();

    if ( !$_sessions )
      return array();

    $i = 0;

    /** @todo Use Utils::group_sessions_by_movie() instead */
    // Although this results are not directly used, by quering this entities now
    // they are already available for Doctrine to use them when they are needed.
    $movie_ids  = array_map( function($a) { return $a->getMovie()->getId(); }, $_sessions );
    $movie_ids  = array_unique( $movie_ids );
    if ($movie_ids) {
      $movies   = $this->findManyBy($movie_ids);
    }
    $parent_ids = array_map( function($a) { return $a->getParent() ? $a->getParent()->getId() : FALSE; }, $movies );
    $parent_ids = array_unique( $parent_ids );
    $parent_ids = array_values(array_filter( $parent_ids ));
    if ($parent_ids) {
      $parents  = $this->findManyBy($parent_ids);
    }

		/* @var $_sess \SocialSnack\WsBundle\Entity\Session */
		foreach ( $_sessions as $_sess ) {
      $i++;
			$_movie_id = $_sess->getMovie()->getId();
//      $_movie    = WsHelper::get_from_result_by_id( $movies,  $_movie_id );
      $_movie    = $_sess->getMovie();
			$_group_id = $_movie->getGroupId();
      $cinemom   = $_sess->isCinemom();
      $jumbo     = $_sess->isType('jumbo');
      $macro     = $_sess->isType('macro');

      if ( $cinemom ) {
        $_movie_id .= '-cinemom';
      }
      if ( $jumbo ) {
        $_movie_id .= '-jumbo';
      }
      if ( $macro ) {
        $_movie_id .= '-macro';
      }

      if ( !isset( $groups[$_group_id] ) ) {
        $groups[$_group_id] = array(
            'parent'   => $_movie->getParent() ? $_movie->getParent() : $_movie,
            'sessions' => array(),
        );
      }

      if ( !isset( $groups[$_group_id]['sessions'][$_movie_id] ) ) {
        $groups[$_group_id]['sessions'][$_movie_id] = array(
            'movie'    => $_movie,
            'sessions' => array(),
            'cinemom'  => $cinemom,
            'jumbo'    => $jumbo,
            'macro'    => $macro,
        );
      }
      $groups[$_group_id]['sessions'][$_movie_id]['sessions'][] = $_sess;
    }

		return $groups;
  }


  /**
   *
   * @param array $attributes
   * @param type $date_from
   * @param type $date_to
   * @return type
   */
  public function findAllSessionsByAttrByDate( $attributes, $date_from, $date_to = NULL ) {
    $_sessions = $this->_findSessionsByDate( $date_from, $date_to, NULL, NULL, $attributes );
    $cinemas = array();
    foreach ( $_sessions as $_session ) {
      $sessions = array();

        /* @var $_sess \SocialSnack\WsBundle\Entity\Session */
      foreach ( $cinema->getSessions() as $_sess ) {
        $_movie_id = $_sess->getMovie()->getId();
        if ( !isset( $sessions[$_movie_id] ) ) {
          $sessions[$_movie_id] = array(
              'movie'    => $_sess->getMovie(),
              'sessions' => array(),
          );
        }
        $sessions[$_movie_id]['sessions'][] = $_sess;
      }

      $cinemas[] = array(
          'cinema' => $cinema,
          'movies' => $sessions,
      );
    }
    return $cinemas;
  }


	/**
	 * Find sessions in cinema and/or by movie and/or by attribute, by date
	 *
	 * @param string  $date_from
	 * @param string  $date_to    Optional.
	 * @param integer $cinema_id  Optional.
	 * @param integer $movie      Optional.
   * @param array   $attributes Optional.
   * @param integer $state_id   Optional.
   * @param integer $area_id    Optional.
	 * @return type
	 */
	public function _findSessionsByDate( $date_from, $date_to = NULL, $cinema_id = NULL, $movie = NULL, $attributes = NULL, $state_id = NULL, $area_id = NULL ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->select( 's' )
				->from( 'SocialSnackWsBundle:Session', 's' )
        ->andWhere( 's.active = 1' );

    if ( $attributes ) {
      $qb->innerJoin( 'SocialSnackWsBundle:Movie', 'm', 'WITH', 's.movie = m.id' );
      WsHelper::dql_apply_movie_attrs( $qb, $attributes );
    }

		$qb->andWhere( 's.movie IS NOT NULL' );

    if ( $state_id ) {
			$qb->innerJoin('s.cinema', 'c')
        ->andWhere( 'c.state= :state_id' )
				->setParameter( 'state_id', $state_id );
    }

    if ( $cinema_id ) {
			$qb->andWhere( 's.cinema = :cinema_id' )
				->setParameter( 'cinema_id', $cinema_id );
    }

		if ( $movie ) {
      WsHelper::dql_apply_movie( $qb, $movie, 's' );
		}

		WsHelper::dql_apply_dates( $qb, $date_from, $date_to );

    $qb->addOrderBy('s.date_time', 'ASC');

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 15 * 60 );
		return $query->getResult();
	}


	/**
	 * Find movies playing in an area with optional date.
	 *
	 * @param type $cinema_id
	 * @param type $date_from
	 * @param type $date_to
	 * @return type
	 */
	protected function _findAllMoviesByCinema( $cinema_id, $date_from = NULL, $date_to = NULL ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 'm' )
				->from( 'SocialSnackWsBundle:Movie', 'm' )
				->innerJoin( 'm.sessions', 's' )
				->innerJoin( 's.cinema',   'c' )
        ->andWhere( 'm.active = 1' )
        ->andWhere( 's.active = 1' );

    if ( is_array( $cinema_id ) ) {
			$qb->andWhere( $qb->expr()->in( 'c.id', $cinema_id ) );
    } else {
			$qb->andWhere( 'c.id = :cinema_id' )
          ->setParameter( 'cinema_id', $cinema_id );
    }

		WsHelper::dql_apply_dates( $qb, $date_from, $date_to );

		$qb
				->groupBy( 'm.id' );

    // Default order for billboard.
    $this->_apply_default_order( $qb );

		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
		return $query->getResult();
	}


	/**
	 * Find movies playing in an area by date.
	 *
	 * @param type $cinema_id
	 * @param type $date_from
	 * @param type $date_to
	 * @return type
	 */
	public function findAllMoviesByCinemaByDate( $cinema_id, $date_from, $date_to = NULL ) {
		return $this->_findAllMoviesByCinema( $cinema_id, $date_from, $date_to );
	}


	/**
	 * Find movies playing in an area with optional date.
	 *
	 * @param type $area_id
	 * @param type $date_from
	 * @param type $date_to
   * @param boolean $is_state Whether $area_id represents an StateArea entity or a State entity.
	 * @return type
	 */
	protected function _findAllMoviesByArea( $area_id, $date_from = NULL, $date_to = NULL, $is_state = FALSE ) {
    return $this->findMoviesIn($is_state ? 'state' : 'area', $area_id, NULL, $date_from, $date_to);
	}


	/**
	 * Find movies playing in an area.
	 *
	 * @param type $area_id
	 * @return type
	 */
	public function findAllMoviesByArea( $area_id, $date_from = NULL ) {
    if (is_null($date_from)) {
      $date_from = date( 'Y-m-d' );
    }
		return $this->_findAllMoviesByArea( $area_id, $date_from );
	}


	/**
	 * Find movies playing in a state.
	 *
	 * @param type $state_id
	 * @return type
	 */
	public function findAllMoviesByState( $state_id, $date_from = NULL ) {
    if (is_null($date_from)) {
      $date_from = date( 'Y-m-d' );
    }
		return $this->_findAllMoviesByArea( $state_id, $date_from, NULL, TRUE );
	}


	/**
	 * Find movies playing in an area today.
	 *
	 * @param type $area_id
	 * @return type
	 */
	public function findAllMoviesByAreaToday( $area_id ) {
		return $this->_findAllMoviesByArea( $area_id, 'today' );
	}


	/**
	 * Find movies playing in an area by date.
	 *
	 * @param type $area_id
	 * @param type $date_from
	 * @param type $date_to
	 * @return type
	 */
	public function findAllMoviesByAreaByDate( $area_id, $date_from, $date_to = NULL ) {
		return $this->_findAllMoviesByArea( $area_id, $date_from, $date_to );
	}


  /**
   * @todo Write a function that actually retrieves movies without query
   *       the sessions first (if that's even possible...).
   *
   * @param type      $cinema_ids
   * @param \DateTime $date_from
   * @param \DateTime $date_to
   * @param boolean   $ungrouped
   * @param array     $order_by
   * @return type
   */
	public function findMoviesInCinema( $cinema_ids, $date_from = NULL, $date_to = NULL, $ungrouped = FALSE, $order_by = array() ) {
    // Define Doctrine's cache ID
    if ($date_from === NULL && $date_to === NULL && $ungrouped === FALSE && $order_by === array()) {
      $cache_id = __METHOD__ . ':' . implode(',', (array)$cinema_ids);
    } else {
      $cache_id = NULL;
    }

		$qb = $this->createQueryBuilder('m');

    if ( $ungrouped ) {
      $qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm = s.movie' );
    } else {
      $qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm.group_id = s.group_id' );
    }

    $qb->andWhere( $qb->expr()->in('s.cinema', $cinema_ids) )
        ->andWhere( 's.active = 1' );

    if ( !$ungrouped ) {
      $qb->andWhere( $qb->expr()->isNull('m.parent') )
          ->addGroupBy('m.group_id');
    } else {
      $qb->addGroupBy('m.id');
    }

    $this->_apply_default_order($qb);

    if ( is_null( $date_from ) ) {
      $date_from = new \DateTime( 'now' );
      if ( is_null( $date_to ) ) {
        $date_to = 0;
      }
    }

		WsHelper::dql_apply_dates( $qb, $date_from, $date_to );

		$query = $qb->getQuery();
		$query->useResultCache(TRUE, 60 * 60, $cache_id);
		return $query->execute();
	}


  public function findMovieIdsInArea( $area_id ) {
    return $this->findMoviesInArea($area_id, array('id'));
  }


  public function findMoviesInArea( $area_id, $fields = NULL, $date_from = NULL, $date_to = NULL ) {
    return $this->findMoviesIn('area', $area_id, $fields, $date_from, $date_to);
  }


  public function findMoviesInState( $state_id, $fields = NULL, $date_from = NULL, $date_to = NULL ) {
    return $this->findMoviesIn('state', $state_id, $fields, $date_from, $date_to);
  }


  /**
   *
   * @param string $type 'state' or 'area'.
   * @param int $id
   * @param array $fields
   * @param type $date_from
   * @param type $date_to
   * @return type
   */
  protected function findMoviesIn( $type, $id, $fields = NULL, $date_from = NULL, $date_to = NULL ) {
		$qb = $this->createQueryBuilder('m');

    if ( is_null($fields) ) {
      $qb->addSelect( 'm' );
    } else {
      foreach ( $fields as $field ) {
        $qb->addSelect( 'm.' . $field );
      }
    }

		$qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm.group_id = s.group_id' )
				->innerJoin( 's.cinema', 'c' );

    switch ($type) {
      case 'area':
        $qb->andWhere( 'c.area = :area' )
            ->setParameter( 'area', $id );
        break;
      case 'state':
        $qb->andWhere( 'c.state = :state' )
            ->setParameter( 'state', $id );
        break;
    }

    $qb->andWhere( $qb->expr()->isNull( 'm.parent' ) )
        ->andWhere( 's.active = 1' )
        ->andWhere( 'm.active = 1' )
        ->andWhere( 'c.active = 1' )
        ->addGroupBy('m.id');

    if ( $date_from ) {
      WsHelper::dql_apply_dates( $qb, $date_from, $date_to );
    }

    $this->_apply_default_order($qb);

		$query = $qb->getQuery();
		$query->useResultCache(TRUE, 60 * 60);
		return $query->execute();
  }


  public function findVersionsInArea( $group_id, $area_id, $fields = NULL ) {
    return $this->findVersionsIn('area', $group_id, $area_id, $fields);
  }


  public function findVersionsInState( $group_id, $state_id, $fields = NULL ) {
    return $this->findVersionsIn('state', $group_id, $state_id, $fields);
  }


  public function findVersionsInCinema( $group_id, $cinema_id, $fields = NULL ) {
    return $this->findVersionsIn('cinema', $group_id, $cinema_id, $fields);
  }


  public function findVersionsIn( $type, $group_id, $area_id, $fields = NULL ) {
		$qb = $this->getEntityManager()->createQueryBuilder();

    if ( is_null($fields) ) {
      $qb->addSelect( 'm' );
    } else {
      foreach ( $fields as $field ) {
        $qb->addSelect( 'm.' . $field );
      }
    }

    $qb->from( 'SocialSnackWsBundle:Movie', 'm' )
        ->innerJoin( 'm.sessions', 's' )
        ->andWhere( 'm.type != \'grouper\'')
        ->andWhere( 's.group_id = :group_id' )
        ->setParameter('group_id', $group_id);

    $qb->innerJoin( 's.cinema', 'c' );

    switch ($type) {
      case 'area':
        $qb->andWhere( 'c.area = :area' )
            ->setParameter( 'area', $area_id );
        break;
      case 'state':
        $qb->andWhere( 'c.state = :state' )
            ->setParameter( 'state', $area_id );
        break;
      case 'cinema':
        $qb->andWhere($qb->expr()->in('c', (array)$area_id));
        break;
    }

    $qb->andWhere( 's.active = 1' )
        ->addGroupBy('m.id');

    $this->_apply_default_order($qb);

		$query = $qb->getQuery();
		$query->useResultCache(TRUE, 60 * 60);
		return $query->execute();
  }

}
