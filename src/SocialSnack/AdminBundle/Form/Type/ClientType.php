<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
        ->add('name', NULL, array('label' => 'Nombre', 'attr' => array('class' => 'form-control')))
        ->add('description', NULL, array('label' => 'Descripción', 'attr' => array('class' => 'form-control')))
        ->add('active', NULL, array('label' => 'Activo', 'required' => FALSE))
        ->add('app_id', NULL, array('label' => 'Client ID', 'attr' => array('class' => 'form-control')))
        ->add('secret_key', NULL, array('label' => 'Secret Key', 'attr' => array('class' => 'form-control')))
        ->add('caps', 'choice', array(
            'label' => 'Permisos',
            'multiple' => TRUE,
            'expanded' => TRUE,
            'choices' => [
                'direct_login' => 'direct_login',
                'oauth' => 'oauth',
                'purchase' => 'purchase',
                'users_history' => 'users_history',
                'list_users' => 'list_users',
                'users_info' => 'users_info',
            ]
        ))
        ->add('settings', new ClientSettingsType(), ['label' => 'Configuración'])
        ->add('save', 'submit', array('label' => 'Guardar'));
  }

  public function getName() {
    return 'admin_client';
  }

}
