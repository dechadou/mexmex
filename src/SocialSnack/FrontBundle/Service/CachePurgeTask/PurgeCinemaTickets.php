<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\WsBundle\Entity\Cinema;

class PurgeCinemaTickets implements DoctrineTaskInterface {

  protected $doctrine;

  protected $cinema;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  public function setCinema(Cinema $cinema) {
    $this->cinema = $cinema;
  }


  public function getDoctrineResources() {
    $ids = [];

    foreach ($this->getTicketsIds() as $ticket) {
      $ids[] = 'SocialSnack\WsBundle\Entity\SessionRepository::_populate_tickets:' . $this->cinema->getId() . ':' . $ticket['legacy_id'];
    }

    return [
        [
            'ids' => $ids,
            'env' => 'front'
        ]
    ];
  }


  protected function getTicketsIds() {
    if (!$this->cinema) {
      throw new \Exception('Cinema not defined.');
    }

    $qb = $this->doctrine
        ->getRepository('SocialSnackWsBundle:Ticket')
        ->createQueryBuilder('q');
    $qb
        ->select('q.legacy_id')
        ->andWhere('q.cinema = :cinema')
        ->setParameter('cinema', $this->cinema)
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }

}