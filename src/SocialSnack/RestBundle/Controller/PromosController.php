<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PromosController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_promos_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    $handler = $result = $this->get('rest.promo.handler');

    $result = $handler->getAll();

    $response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 45 * 60 );

    return $response;
	}

  /**
   * @Route("/{type}", name="rest_promos_type")
   * @Method({"GET"})
   */
  public function typeAction($type, Request $request) {
    $handler = $result = $this->get('rest.promo.handler');

    $result = $handler->getByType($type);

    if (!$result) {
      return new JsonResponse( array('error' => 'Type not valid'), 404 );
    }

    $response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 45 * 60 );

    return $response;
  }

  /**
   * @Route("/{type}/{id}", name="rest_promos_target")
   * @Method({"GET"})
   */
  public function targetAction($type, $id, Request $request) {
    $handler = $result = $this->get('rest.promo.handler');
    $result = $handler->getByType($type,$id);

    if (!$result) {
      return new JsonResponse( array('error' => 'ID not found'), 404 );
    }

    $response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 45 * 60 );

    return $response;
  }


}