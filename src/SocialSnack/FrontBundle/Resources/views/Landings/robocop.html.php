<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Robocop - Cinemex');
$view['slots']->set('body_class', 'sponsored-robocop');

$view['slots']->start('body');
?>

<div class="section-robocop">
    <div class="robocop-banner"></div>

    <div class="robocop-buttons">
        <a data-landing-trigger class="btn" href="#robocop-bases" title="Bases y mecánica">Bases y mecánica</a>
        <a data-landing-trigger class="btn" href="#robocop-prices" title="Premio">Premio</a>
        <a data-landing-trigger class="btn" href="#robocop-terms" title="Términos y condiciones">Términos y condiciones</a>
    </div>

    <section id="robocop-bases" class="landing-content landing-target">
        <div class="content-box">
            <span class="icon icon-dark icon-close" data-landing-trigger></span>

            <h1 class="content-box-title">Bases y mecánicas</h1>

            <ul>
                <li>Dale follow en Twitter a @Depelicula40 y @Cinemex</li>
                <li>A partir del viernes 14 de febrero y hasta el jueves 20 de Febrero de 2014, se publicarán 8 preguntas referentes a la película, Cinemex y Los 40 Principales.</li>
                <li>Manda un correo con el asunto RoboCop, tus datos (nombre completo, teléfono y ciudad de residencia) y las respuestas correctas a los40.mx@gmail.com a partir del viernes 21 de Febrero a las 11 de la mañana. El primer correo recibido a partir de las 11am con las respuestas correctas, que cumpla con los requisitos de la promoción, se hará acreedor a un viaje doble a Miami, Florida.</li>
                <li>El ganador será notificado vía telefónica el viernes 21 de Febrero, y deberá enviar su pasaporte y visa vigente y los de su acompañante en un correo electrónico INMEDIATAMENTE DESPUÉS de la notificación.</li>
            </ul>
        </div><!-- .content-box -->
    </section><!-- .landing-content -->

    <section id="robocop-prices" class="landing-content landing-target">
        <div class="content-box">
            <span class="icon icon-dark icon-close" data-landing-trigger></span>

            <h1 class="content-box-title">Premio</h1>

            <ul>
                <li>Viaje a Miami 4 días 3 Noches en Hotel 4 estrellas con entrenamiento policial. (desde cómo proteger un edificio, tácticas de investigación, manejo de armas de aire, etc).</li>
                <li>Vuelo redondo Cd. De México-Miami para dos personas (uno debe ser adulto y el otro debe tener por lo menos 16 años cumplidos al momento de hacer el viaje.</li>
                <li>Transporte aeropuerto-hotel-aeropuerto.</li>
                <li>Servicio de la agencia en español.</li>
            </ul>
        </div><!-- .content-box -->
    </section><!-- .landing-content -->

    <section id="robocop-terms" class="landing-content landing-target">
        <div class="content-box">
            <span class="icon icon-dark icon-close" data-landing-trigger></span>

            <h1 class="content-box-title">Terminos</h1>

            <ul>
                <li>Las fechas estipuladas para el viaje son inamovibles e intransferibles.</li>
                <li>Cualquier gasto adicional no especificado en el premio, correrá por cuenta de los participantes.</li>
                <li>Todos los paquetes aplican saliendo de la Ciudad de México, si los ganadores  se encuentra en el Interior de la República, deberán trasladarse al Aeropuerto de la Ciudad de México por su cuenta, para poder realizar el viaje.</li>
                <li>Los servicios no utilizados no son reembolsables, y el paquete no puede ser canjeado o cambiado por su valor en efectivo.</li>
                <li>Una tarjeta de crédito o el depósito en efectivo para imprevistos pueden ser solicitados por el hotel en el registro o requerir un depósito en efectivo.</li>
            </ul>

            <p>No incluye imprevistos, propinas, llamadas telefónicas, o ningún otro gasto personal incurrido durante el viaje.</p>

            <hr>

            <h1 class="content-box-title">Condiciones</h1>

            <p>En caso de ser ganador es necesario contar con el boleto de la película “Robocop” en cualquier Cinemex, con fecha dentro del periodo  de vigencia de la promoción.</p>
            <p>Podrán participar en esta promoción todas las personas físicas mayores de edad  que cumplan cabalmente con las condiciones establecidas en estas Bases. La participación en esta promoción es única, en caso de que se detecten dos (2) o más registros con datos iguales de cada uno de los participantes  o similares serán descalificados. Los responsables de la promoción nos reservamos individualmente el derecho de eliminar a todo aquel invitado que lleve a cabo cualquier comportamiento irregular o ilegal en esta promoción.</p>
            <p>No podrán participar en ésta promoción, empleados de Los 40 Principales, Cinemex, Columbia Pictures o personal perteneciente a compañía alguna involucrada en esta promoción ni cónyuges, parientes en línea recta en cualquier grado o parientes en línea colateral hasta el segundo grado de las personas antes mencionadas.</p>
            <p>En caso de que el premio no sea reclamado por los ganadores, o el ganador no sea localizado en el tiempo establecido en las bases, el premio pasará al siguiente participante que haya resultado ganador de acuerdo a los términos antes mencionados.</p>
            <p>La promoción es vigente del viernes 7 de Febrero al 21 de Febrero de 2014 y válida en la República Mexicana. La inscripción a esta promoción es gratuita.</p>
            <p>Una vez que se realice la notificación a los ganadores y que se haya entregado la documentación requerida completa en tiempo y forma, la compañía organizadora proporcionará los datos personales de los ganadores a las empresas encargadas de la entrega del premio para que los identifiquen y localicen a fin de poder entregarles el premio correspondiente, por lo que los participantes de esta promoción otorgan su consentimiento con lo anterior, al participar y registrarse en esta promoción.</p>
            <p>No podrán ser acreedores al premio quienes hayan ganado en alguna otra promoción similar realizada por Los 40 Principales o Cinemex en los últimos 3 años.</p>
            <p>Recuerda que para poder reclamar el premio deberán  presentar identificación oficial vigente y tu boleto de entrada a la película RoboCop en Cinemex, de lo contrario el premio pasará de inmediato a quienes pudieran haber sido acreedores en términos de ésta mecánica.</p>
            <p>La entrega del premio se encuentra condicionada al cumplimiento previo de todas y cada una de las leyes federales, estatales y locales, regulaciones y reglamentos que sean aplicables. El no cumplimiento a cualquiera de los puntos previstos en  a las bases y condiciones, o cualquier falsedad en el registro o la conducta errónea y/o dolosa del ganador, provocará  la descalificación inmediata del participante y, consecuentemente todos los privilegios en su carácter de posible ganador serán terminados de manera inmediata.</p>

            <p>Cinemex Desarrollos, S.A. de C.V., (mencionado como “Cinemex”), con domicilio en Avenida Javier Barros Sierra, Número 540, Torre 1, PH1, Col. Santa Fe, C.P. 01210, Álvaro Obregón, México, D.F., te comunica lo siguiente:</p>
            <p>Los Datos Personales que te son solicitados, son con la finalidad primaria de poderte identificar como participante y en caso de ser ganador,  poder realizar la entrega del incentivo.</p>

            <p>Asimismo, podremos tratar tus Datos Personales con las siguientes finalidades secundarias:</p>
            <ul>
                <li>Proporcionarte información de eventos, boletines informativos y/o cartelera “Cinemex” por medios electrónicos, auditivos y/o mensajes de texto;</li>
                <li>Participación en trivias, y en su caso, entrega de premios y publicación de ganadores;</li>
                <li>Informarte y ofrecerte promociones y publicidad de “Cinemex”, de patrocinadores y socios comerciales;</li>
                <li>Llenado de encuestas; y</li>
                <li>Contactarte en fechas especiales.</li>
            </ul>

            <p>Al participar, estás aceptando los términos y condiciones de la promoción, así como, el  Aviso de Privacidad.</p>
            <p>Podrá conocer el Aviso de Privacidad Integro en la siguiente página de Internet: www.cinemex.com/privacidadpromociones</p>
            <p>Al participar en esta promoción el participante ganador, renuncia a cualquier reclamación o acción legal que pudieran ejercer y liberan de cualquier responsabilidad a la Compañía Organizadora o sus empresas filiales o subsidiarias y/o a cualquiera de los patrocinadores del premio, por situaciones resultantes del uso y disfrute del mismo.</p>
            <p>Todas las promociones de los Organizadores podrán ser suspendidas, canceladas o modificadas total o parcialmente, en cualquier momento dando el aviso a la autoridad correspondiente, así como haciendo la publicación en la página web o en el medio donde se publique la presente promoción. Los Organizadores son el órgano inapelable que interpretará todas las cuestiones que se susciten con relación a esta promoción y las bases y condiciones del concurso promocional.</p>
            <p>Enviar tu correo indica que conoces y aceptas las Reglas y Mecánica de la promoción.</p>
        </div><!-- .content-box -->
    </section><!-- .landing-content -->
</div>

<?php $view['slots']->stop(); ?>
