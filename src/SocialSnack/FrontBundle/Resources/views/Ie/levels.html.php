<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Niveles - Cinemex');
$view['slots']->start('iebody');
?>
<h1 class="content-box-title"><span class="icon icon-light icon-crown no-txt"></span> Niveles</h1>

<div class="entry-body">
  <p>Forma parte del mejor programa de recompensas que Premia tu Diversión, adquiere tu tarjeta y empieza a disfrutar de los beneficios de ser Especial.</p>

  <?php echo $this->render('SocialSnackFrontBundle:Ie:levelsTabs.html.php'); ?>

  <div id="ie-levels-basic" class="ie-levels-block">
    <h2 class="hidden">Básico</h2>

    <p>Para obtener este nivel sólo debes acudir al Centro de Atención al Invitado de cualquier complejo Cinemex o visitar <a href="http://www.payback.mx" target="_blank">payback.mx</a> e inscribirte. Es totalmente GRATIS.</p>

    <div class="special-box ie-program-benefits">
      <h3 class="special-box-title">Al inscribirte te regalaremos un boleto al 2x1* para que lo utilices en tu cine favorito y empieces a disfrutar de grandes beneficios como:</h3>

      <div class="special-box-hr"></div>

      <div class="special-box-content">
        <ul>
          <li class="two-lines"><i class="icon icon-red icon-tick2"></i> Acumulación del 5% del valor de tu compra en Cinemex en Puntos PAYBACK.<br>Con ellos podrás adquirir boletos gratis, palomitas, refrescos o todo lo que tú quieras</li>
          <li><i class="red">M</i> Martes 2x1</li>
          <li><i class="icon icon-red icon-king"></i> Invitaciones a Premieres</li>
          <li><i class="red">2x1</i> Boletos al 2x1 y a precio especial</li>
          <li><i class="icon icon-red icon-gift"></i> Regalo el día de tu cumpleaños</li>
          <li><i class="icon icon-red icon-star"></i> Promociones exclusivas</li>
          <li><i class="icon icon-red icon-plus"></i> Y muchas sorpresas más. </li>
        </ul>
      </div>
    </div>

    <h3>Demuéstranos tu pasión por el cine y podrás  subir a los niveles Oro y Premium.</h3>

    <p>
      <strong>¿Cómo hacerlo?</strong><br/>
      <span class="color-brand-main">Nivel Oro =</span> deberás acumular 12 visitas* <br/>
      <span class="color-brand-main">Nivel Premium =</span> deberás acumular 24 visitas*
    </p>

    <p><strong>Las visitas deben ser realizadas en el transcurso del año en el que te inscribiste al Programa de Invitado Especial Cinemex PAYBACK.</strong></p>

    <p>
      Por ejemplo, si te inscribes en marzo  de 2015 tendrás hasta el 31 de diciembre de 2015 para alcanzar el nivel Oro si acumulas 12 visitas, o alcanzar el nivel Premium si acumulas 24 visitas.  Dicho cambio de nivel lo podrás realizar a partir de enero de 2015.<br/>
      Por lo tanto sin importar el mes en el que realices tu inscripción, deberás juntar tus visitas para el 31 de diciembre del año en el cual te inscribiste.
    </p>


    <div class="disclaimer">
      <p>
        * El boleto 2x1 se enviará por correo electrónico en el momento de la inscripción a Invitado Especial Cinemex PAYBACK.<br/>
        Es válido de lunes a viernes, excepto miércoles en todos los complejos Cinemex. No aplica con otras promociones, eventos especiales ni premieres.
      </p>

      <p>* Una visita es la compra de boletos en taquilla, sin importar cuántos en una sola transacción, para una función determinada. Se podrán tener visitas múltiples en un mismo día siempre y cuando los boletos comprados sean para funciones que no se empalmen, sin importar si son en un mismo complejo CinemexMR. Sólo aplican boletos pagados con puntos de Invitado Especial PAYBACK, efectivo, tarjeta de crédito, tarjeta de débito o certificado de regalo CinemexMR al realizar el pago en taquilla, CAI, Internet, Aplicación CinemexMR y Línea CinemexMR, siempre y cuando se presente la tarjeta del programa Invitado Especial PAYBACK para registrar la visita.</p>
    </div>

  </div>

  <div id="ie-levels-gold" class="ie-levels-block">
    <h2 class="hidden">Oro</h2>

    <p>Para formar parte de este selecto grupo de Invitado Especial Oro, deberás acumular 12 visitas*  en Cinemex en el transcurso del año en el cual te inscribiste al Programa de Invitado Especial Cinemex PAYBACK.</p>

    <div class="special-box ie-program-benefits">
      <h3 class="special-box-title">Como Invitado Especial Oro, obtienes beneficios como:</h3>

      <div class="special-box-hr"></div>

      <div class="special-box-content">
        <ul>
          <li class="two-lines"><i class="icon icon-gold icon-tick2"></i> Acumulación del 8% del valor de tu compra en Cinemex en Puntos PAYBACK. <br>Con ellos podrás adquirir boletos gratis, palomitas, refrescos o todo lo que tú quieras</li>
          <li><i class="gold">M</i> Martes 2x1</li>
          <li><i class="icon icon-gold icon-king"></i> Invitaciones a Premieres</li>
          <li><i class="gold">2x1</i> Boletos al 2x1 y a precio especial</li>
          <li><i class="icon icon-gold icon-gift"></i> Regalo el día de tu cumpleaños</li>
          <li><i class="icon icon-gold icon-star"></i> Promociones exclusivas</li>
          <li><i class="icon icon-gold icon-plus"></i> Y muchas sorpresas más. </li>
        </ul>
      </div>
    </div>

    <h3>EL NIVEL ORO TIENE VIGENCIA DE UN AÑO Y PARA CONSERVARLO DEBES REALIZAR 12 VISITAS EN ESTE PERIODO, LA FECHA LÍMITE PARA CUMPLIR CON TUS VISITAS, es el 31 de diciembre del año en curso.</h3>

    <p>
      Si lograste mantener tu nivel Oro, la fecha para que renueves tu plástico es:<br/>
      &raquo; Enero y febrero del siguiente año.
    </p>

    <p>
      Así que ya lo sabes, cumple con tus visitas en el periodo de un año calendario y acude al Centro de Atención al Invitado para renovar tu tarjeta pagando la cuota de cambio de nivel.
    </p>

    <h3>¡Demuestra tu pasión por el cine y podrás mantener tu nivel!</h3>
    <p>
      En caso de que no realices las 12 visitas en el año para mantener tu nivel Oro pasarás al nivel Básico y tendrás que empezar de nuevo a acumular tus visitas.
    </p>

    <div class="disclaimer">
      <p>
        * Una visita es un boleto comprado con efectivo, tarjeta de débito/crédito o puntos PAYBACK que no se empalme con otra función.
      </p>
    </div>

  </div>

  <div id="ie-levels-premium" class="ie-levels-block">
    <h2 class="hidden">Premium</h2>

    <div class="tabs">
      <ul class="file-tabs tabs-links">
        <li><a class="tab selected" href="#">Premium</a></li>
        <li><a class="tab" href="#">AMEX</a></li>
      </ul>

      <div class="tab-content selected">
        <p>Para formar parte de este exclusivo grupo de Invitado Especial Premium, deberás acumular 24 visitas * en Cinemex en el transcurso del año calendario.</p>

        <div class="special-box ie-program-benefits">
          <h3 class="special-box-title">Como Invitado Especial Premium, además de tener todos los beneficios de los niveles Básico y Oro, obtendrás beneficios como:</h3>

          <div class="special-box-hr"></div>

          <div class="special-box-content">
            <ul>
              <li class="two-lines"><i class="icon icon-black icon-tick2"></i> Acumulación del 10% del valor de tu compra en Cinemex en Puntos PAYBACK. <br>Con ellos podrás adquirir boletos gratis, palomitas, refrescos o todo lo que tú quieras.</li>
              <li><i class="black">M</i> Martes 2x1</li>
              <li><i class="icon icon-black icon-king"></i> Invitación a premieres</li>
              <li><i class="black">2x1</i> Boletos al 2x1 y a precio especial</li>
              <li><i class="icon icon-black icon-gift"></i> Regalo el día de tu cumpleaños</li>
              <li><i class="icon icon-black icon-soda"></i> Un relleno gratis de tus palomitas y refresco grandes</li>
              <li><i class="icon icon-black icon-user"></i> Fila preferencial en Dulcería</li>
              <li><i class="icon icon-black icon-tarr"></i> Ascenso a las Salas 3D y/o Platino</li>
            </ul>
          </div>
        </div>

        <p>
          EL NIVEL PREMIUM TIENE VIGENCIA DE UN AÑO Y PARA CONSERVARLO DEBES REALIZAR 24 VISITAS EN ESTE PERIODO, LA FECHA LÍMITE PARA CUMPLIR CON TUS VISITAS, es el 31 de diciembre del año en curso.
        </p>

        <p>
          Si lograste mantener tu nivel Premium, la fecha para que renueves tu plástico es:<br/>
          &raquo; Enero y febrero del siguiente año.
        </p>

        <p>
          Así que ya lo sabes, cumple con tus visitas en el periodo de un año calendario y acude al Centro de Atención al Invitado para renovar tu tarjeta pagando la cuota de cambio de nivel.
        </p>

        <h3>¡Demuestra tu pasión por el cine y podrás mantener tu nivel!</h3>

        <p>
          En caso de que no realices las 24 visitas en el año para mantener tu nivel Premium pasarás al nivel Oro o Básico y tendrás que empezar de nuevo a acumular tus visitas.
        </p>

        <div class="disclaimer">
          <p>
            * Una visita es un boleto comprado con efectivo, tarjeta de débito/crédito o Puntos PAYBACK que no se empalme con otra función.
          </p>
        </div>
      </div>


      <div class="tab-content">
        <p>Si cuentas con The Platinum Card® o The Centurion Card American Express tendrás el beneficio de formar parte de Invitado Especial Cinemex American Express.</p>

        <div class="special-box ie-program-benefits">
          <h3 class="special-box-title">Como Invitado Especial Cinemex American Express obtienes los siguientes beneficios:</h3>

          <div class="special-box-hr"></div>

          <div class="special-box-content">
            <ul>
              <li class="two-lines"><i class="icon icon-black icon-tick2"></i> Acumulación del 10% del valor de tu compra en Cinemex en puntos.<br>Con ellos podrás adquirir boletos gratis, palomitas, refrescos o todo lo que tú quieras.</li>
              <li class="two-lines"><i class="black">15%</i> 15% de descuento presentando tu tarjeta de Invitado Especial en taquillas de salas Platino y Tradicionales.</li>
              <li><i class="icon icon-black icon-king"></i> Invitación a premieres</li>
              <li><i class="black">2x1</i> Boletos al 2x1 y a precio especial</li>
              <li><i class="icon icon-black icon-gift"></i> Regalo el día de tu cumpleaños</li>
              <li><i class="icon icon-black icon-soda"></i> Un relleno gratis de tus palomitas y refresco grandes</li>
              <li><i class="icon icon-black icon-user"></i> Fila preferencial en Dulcería</li>
              <li><i class="icon icon-black icon-tarr"></i> Ascenso a las Salas 3D y/o Platino</li>
              <li><i class="icon icon-black icon-plus"></i> Y muchas sorpresas más!</li>
            </ul>
          </div>
        </div>

        <p>
          Si aún no recibes tu Tarjeta de Invitado Especial Cinemex American Express, solicítala llamando a The Platinum Lifestyles Services al 01 800 716 11 03. Si ya cuentas con ella actívala en el Centro de Atención al Invitado de cualquier complejo Cinemex.
        </p>
      </div>

    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
