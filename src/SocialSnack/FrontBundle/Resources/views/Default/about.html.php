<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Acerca de Cinemex - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('about_page'); ?>">Acerca de Cinemex</a></li>
</ul>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render( 'SocialSnackFrontBundle:Sections:pagesMenu.html.php', array( 'route' => $route ) ); ?>
  </div>
  <div class="wide-main-col col content-box">
    <article class="entry-full">
      <h1 class="content-box-title">Acerca de Cinemex</h1>
      
      <div class="entry-body">
        <h3>Misión</h3>
        <ul><li>Estamos dedicados a ser los mejores en divertir a la gente.</li></ul>

        <h3>Filosofía</h3>
        <ul>
          <li>Cinemex es su gente</li>
          <li>Nuestra relación se sustenta en la confianza</li>
          <li>Somos arquitectos de lealtades</li>
          <li>Lo más importante es divertirse y sonreir</li>
        </ul>
        
        <h3>Valores</h3>
        <ul>
          <li>Buscar y provocar relaciones duraderas</li>
          <li>Creer que en esencia la gente actúa por buena fe</li>
          <li>Tener una actitud de dar siempre lo mejor</li>
          <li>Hacerlo todo con pasión</li>
          <li>Ejercer rectitud en todas nuestras acciones y decisiones</li>
          <li>Superar de manera continua nuestros estándares</li>
          <li>Ser consistentes en el pensar, decir y actuar</li>
          <li>Aceptar las diferencias y tratar a todos con dignidad</li>
          <li>Disfrutar y gozar todo lo que hacemos</li>
        </ul>
      </div>
        
    </article>
  </div>
</div>
<?php $view['slots']->stop(); ?>