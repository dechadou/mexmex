<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

class PurgePlatinumMap extends AbstractConceptMap {

  protected  function getRouteName() {
    return 'platinum_cinemas';
  }


  protected function getConceptName() {
    return 'Platinum';
  }

}