<?php

namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class LoopedCommandAbstract extends ContainerAwareCommand {

  protected $iterations = 0;

  protected $startTime = [];

  abstract protected function getMemoryLimit();

  abstract protected function getTimeLimit();

  abstract protected function getIterationsLimit();

  /**
   * Checks the status of the process.
   * If it has been running for a very long time or increased too much on memory consumption, let's terminate.
   */
  protected function checkStatus() {
    $terminate = FALSE;

    $mem = memory_get_usage();
    if (!$mem > $this->getMemoryLimit()) {
      $this->log('warning', sprintf(
          'Command %s exceeded allowed memory limit of %d bytes (using %d bytes).',
          $this->getName(),
          $this->getMemoryLimit(),
          $mem
      ));
      $terminate = TRUE;
    }

    $elapsed = $this->getElapsedTime();
    if ($elapsed >= $this->getTimeLimit()) {
      $this->log('warning', sprintf(
          'Command %s exceeded maximum execution time of %d seconds (ran for %d seconds).',
          $this->getName(),
          $this->getTimeLimit(),
          $elapsed
      ));
      $terminate = TRUE;
    }

    if ($this->iterations > $this->getIterationsLimit()) {
      $this->log('warning', sprintf(
          'Command %s exceeded maximum iterations (%d).',
          $this->getName(),
          $this->iterations
      ));
      $terminate = TRUE;
    }

    if ($terminate) {
      $this->terminate(FALSE);
    }
  }


  protected function startTimer($name = 'default') {
    $this->startTime[$name] = microtime(true);
  }


  protected function getElapsedTime($name = 'default') {
    $time_end = microtime(true);
    return ($time_end - $this->startTime[$name]);
  }


  protected function clearTimer($name = 'default') {
    unset($this->startTime[$name]);
  }


  protected function log($level, $message) {
    $logger = $this->getLogger();
    if (!$logger || !method_exists($logger, $level)) {
      return FALSE;
    }

    $logger->{$level}($message);
  }


  /**
   * @return \Monolog\Logger
   */
  protected function getLogger() {
    return NULL;
  }

  protected function terminate($normal = FALSE) {
    die();
  }

}