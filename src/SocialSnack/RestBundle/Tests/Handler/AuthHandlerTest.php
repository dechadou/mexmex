<?php

namespace SocialSnack\RestBundle\Tests\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\RestBundle\Handler\AuthHandler;
use SocialSnack\RestBundle\Service\Utils as RestUtils;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class AuthHandlerTest
 * @package SocialSnack\RestBundle\Tests\Handler
 * @author Guido Kritz
 */
class AuthHandlerTest extends WebTestCase {

  /**
   * @param ContainerInterface $container
   * @param Registry           $doctrine
   * @param EncoderFactory     $encoder_factory
   * @param RestUtils          $rest_utils
   * @return AuthHandler
   */
  protected function getHandler(ContainerInterface $container = NULL, Registry $doctrine = NULL, EncoderFactory $encoder_factory = NULL, RestUtils $rest_utils = NULL) {
    $reflection = new \ReflectionClass('\SocialSnack\RestBundle\Handler\AuthHandler');
    return $reflection->newInstanceArgs($this->getHandlerArgs($doctrine));
  }


  /**
   * @param array $methods
   * @param bool  $call_constructor
   * @param null  $constructor_args
   * @return AuthHandler
   */
  protected function getHandlerMock(array $methods = [], $call_constructor = TRUE, $constructor_args = NULL) {
    $handler = $this->getMockBuilder('\SocialSnack\RestBundle\Handler\AuthHandler')
        ->setMethods($methods);

    if (is_null($constructor_args)) {
      $constructor_args = $this->getHandlerArgs();
    }

    if (!$call_constructor) {
      $handler->disableOriginalConstructor();
    } else {
      $handler->setConstructorArgs($constructor_args);
    }

    return $handler->getMock();
  }


  /**
   * @param ContainerInterface $container
   * @param Registry           $doctrine
   * @param EncoderFactory     $encoder_factory
   * @param RestUtils          $rest_utils
   * @return array
   */
  protected function getHandlerArgs(ContainerInterface $container = NULL, Registry $doctrine = NULL, EncoderFactory $encoder_factory = NULL, RestUtils $rest_utils = NULL) {
    return [
        $container       ?: $this->getContainer(),
        $doctrine        ?: $this->getContainer()->get('doctrine'),
        $encoder_factory ?: $this->getContainer()->get('security.encoder_factory'),
        $rest_utils      ?: $this->getContainer()->get('ss.rest.utils')
    ];
  }


  public function testAuthMissingFields() {
    $request = new Request();
    $this->setExpectedException('\SocialSnack\RestBundle\Exception\MissingFieldsException');
    $output = $this->getHandler()->auth($request);
  }


  public function testAuthSuccess() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ]);

    $user = LoadUserData::$users[0];

    $request = new Request([], [
        'username' => $user->getEmail(),
        'password' => 'foobar'
    ]);

    $output = $this->getHandler()->auth($request);
    $this->assertInstanceOf('\SocialSnack\FrontBundle\Entity\User', $user);
    $this->assertEquals($user->getId(), $output->getId());
  }

} 