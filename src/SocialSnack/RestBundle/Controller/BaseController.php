<?php

namespace SocialSnack\RestBundle\Controller;

use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;

use SocialSnack\FrontBundle\Entity\MovieVote;

class BaseController extends Controller {

  protected $v = NULL;
  
  /**
   * @var \SocialSnack\RestBundle\Entity\App
   */
  protected $client;
  
  protected $access_level = 0;
  
  protected $auth_method = NULL;
  
	
	/**
	 * Methods which require user authentication.
	 * @var array
	 */
	static $auth_methods = array(
			'movieSeen',
			'movieUnseen',
			'movieWish',
			'movieUnwish',
			'movieVote',
	);
	
	
	/**
	 * Authenticated user
	 * @var \SocialSnack\FrontBundle\Entity\User
	 */
	protected $user;

	
	/**
	 * Build credentials token.
	 * 
	 * @param \SocialSnack\FrontBundle\Entity\User $user
	 * @return string
	 */
	protected function build_token($user) {
		return $this->get('rest.auth.handler')->buildToken($user, $this->client);
	}
	
	
	/**
	 * Check user authorization.
	 * 
	 * @todo Check user roles.
	 * @todo Allow two authorization methods: access_token for API use and standard
	 *       Symphony security for browser use.
	 */
	protected function authorize(Request $request = NULL) {
    if (!$request) {
      $request = $this->getRequest();
    }

    if ( $this->user = $this->getUser() ) {
      $this->auth_method = 'symfony';
      return TRUE;
    }

    $this->fix_headers($request);

    $header = $request->headers->get('Authorization');
		if ( !$header )
			return FALSE;

		$header = explode( ' ', $header, 2 );
		if ( 'token' != strtolower( $header[0] ) )
			return FALSE;

		$access_token  = $header[1];
		if ( empty( $access_token ) )
			return FALSE;

		$repo          = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
		list($user_id) = explode( '.', $access_token, 2 );
		$user          = $repo->find( (int)$user_id );
		
		if ( !$user )
			return FALSE;

		if ( $access_token == $this->build_token( $user ) ) {
			$this->user = $user;
      $this->auth_method = 'api';
			return TRUE;
		}
		return FALSE;
	}
	
	
	protected function _authorize(Request $request = NULL) {
		if (!$this->authorize($request)) {
			throw new RestException('not-authorized', 'Not authorized.', 401 );
		}
	}
	
  
  protected function get_fave_cinemas_ids( $user_id ) {
    $user_faves  = array();
    if ( !$this->authorize() )
      return $user_faves;
    
    $user = $this->user;
		if ( $user_id && $user && ($user->getId() == $user_id) ) {
			foreach ( $user->getCinemasFav() as $fav ) {
				$user_faves[] = $fav->getId();
			}
		}
    return $user_faves;
  }
  
  
  protected function success_auth_res($user, $from_frontend = FALSE) {
    return $this->get('rest.auth.handler')->buildAuthResponse(
        $user,
        $this->getRequest(),
        $from_frontend
    );
  }
  
  
  public function before() {
    $this->fix_headers();
    $this->check_api_version();
    $this->validate_client();
  }
  
  
  protected function fix_headers(Request $request = NULL) {
    if (!$request) {
      $request = $this->getRequest();
    }

    $headers = $request->headers;
    if (!$headers->has('Authorization') && function_exists('apache_request_headers')) {
      $all = apache_request_headers();

      if (isset($all['X-Authorization'])) {
        $request->headers->set('Authorization', $all['X-Authorization']);
      } elseif ( isset($all['Authorization']) ) {
        $request->headers->set('Authorization', $all['Authorization']);
      } elseif ( isset($all['Authorizationx']) ) {
        $request->headers->set('Authorization', $all['Authorizationx']);
      }
    }
  }
  
  
  /**
   * Define the requested API version.
   */
  protected function check_api_version() {
    $uri = $this->getRequest()->getUri();
    $match = preg_match('/^https?\:\/\/((api|staging|staging2|www)\.)?[a-zA-Z\.]+\/(app_dev\.php\/)?rest\/v([0-9\.]+)\//', $uri, $matches);
    if ( $match ) {
      $this->v = (float)array_pop($matches);
    }

    $this->get('rest.context.handler')->setApiVersion($this->v);
  }


  /**
   * @return float|null API version.
   */
  public function getApiVersion() {
    if (NULL === $this->v) {
      $this->check_api_version();
    }

    return $this->v;
  }


  /**
   * Authorize the client to perform the request.
   *
   * @return bool
   * @throws RestException
   */
  protected function validate_client() {
    // Version 1 of this API didn't require client credentials, so it's always fine.
    if ( $this->v < 2 ) {
      return TRUE;
    }
  
    // Identify the client.
    $headers = $this->getRequest()->headers;
    $consumer_key = $headers->get('X-API-Consumer-Key');
    if ( !$consumer_key ) {
      $consumer_key = $this->getRequest()->get('app_id');
    }
    if ( !$consumer_key ) {
      throw new RestException('credentials-missing', 'Consumer key missing.', 401);
    }

    $app_repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App');
    $app = $app_repo->findOneBy(array(
        'appId' => $consumer_key,
    ));
    
    // Make sure the client is valid and active.
    if (!$app || !$app->getActive()) {
      throw new RestException('invalid-client', 'Invalid client.', 401);
    }
    $this->getRequest()->attributes->set('vary', 'X-API-Consumer-Key');
    
    // Validate rate limits.
    // Not implemented yet.
    
    if ($app->getSetting('validate_referer')) {
      $this->validate_referer();
    }
    
    // All good so far.
    $this->client = $app;

    /** @var \SocialSnack\RestBundle\Handler\ContextHandler $context_handler */
    $context_handler = $this->get('rest.context.handler');
    $context_handler->setClient($app);
    $context_handler->trackClientHit();

    return TRUE;
  }
  
  
  protected function validate_referer() {
    $referer = $this->getRequest()->headers->get('referer');
    $domain  = $this->container->getParameter('domain');
    $valid   = preg_match('/^https?\:\/\/([a-z]+\.)?' . $domain . '\// ', $referer);
    
    if ( !$valid ) {
      throw new RestException('access-denied', 'Domain not allowed.', 401);
    }
    
    return $valid;
  }
  
  
  protected function validate_client_cap($cap) {
    if (!$this->client->hasCap($cap)) {
      throw new RestException('access-denied', 'You\'re not allowed to access this endpoint.', 403);
    }
    
    return TRUE;
  }
  
   
}
