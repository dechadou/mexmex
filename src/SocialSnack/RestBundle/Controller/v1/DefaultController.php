<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use SocialSnack\FrontBundle\Entity\MovieVote;
use SocialSnack\RestBundle\Service\Helper as RestHelper;

class DefaultController extends BaseController {
  
  
  /**
   * This route hsa no functionality.
   * Is defined just to be able to get the base route of the API.
   * 
   * @Route("/", name="social_snack_rest_v1")
   */
  public function defaultAction() {
    return new JsonResponse( array( 
        'error' => 'Endpoint missing.'
    ), 404 );
  }
  
  
  /**
   * @Route("/_version", name="social_snack_rest_v1_v")
   */
  public function versionAction() {
    return new JsonResponse(array( 
        'version' => '1'
    ));
  }
  

  /**
   * Check if there's already an user registered with FB account with UID = fbuid.
   * 
   * Requires $_POST['fbuid']
   * 
   * @Route("/checkFb")
   * @Method({"POST"})
   */
  public function checkFbAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:Default:checkFb', ['request' => $request]);
  }
  
  
	/**
	 * Login.
	 * 
	 * For manual login:
	 * Requires $_POST['username'] OR $_POST['email']
	 * Requires $_POST['password']
	 * 
	 * For FB login:
	 * Requires $_POST['fbsigned_request']
	 *
	 * @todo Make sure this is the best way to authenticate the user.
	 *
   * @Route("/login")
   * @Method({"POST"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function loginAction() {
		$req = $this->getRequest();
    $success = FALSE;
		
		// POST variables.
		$username         = $req->get( 'username' ) ? $req->get( 'username' ) : $req->get( 'email' );
		$password         = $req->get( 'password' );
		$signed_request   = $req->get( 'fbsigned_request' );
		$access_token     = $req->get( 'fbaccess_token' );
    $from_frontend    = $req->get( 'from_frontend' );
		$fb_login         = $signed_request || $access_token;
		
		// Variables to verify the access token.
    /** @todo Use client IDs instead of this. */
    if ( $from_frontend ) {
      $fb_config      = $this->container->getParameter( 'fb' );
    } else {
      $mobile_config  = $this->container->getParameter( 'mobile_app' );
      $fb_config      = $mobile_config['fb'];
    }
		$secret_key       = $fb_config['secret_key'];
		$app_id           = $fb_config['app_id'];
		$app_access_token = $fb_config['app_access_token'];
		$redirect_url     = '';
		
		$repo             = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
		
		// Check required fields.
		if ( ( empty( $username ) || empty( $password ) ) && empty( $signed_request ) && empty( $access_token ) )
			return new JsonResponse( array( 'error' => 'Username and/or password and/or signed request missing.' ), 400 );
		
		// Manual login and FB login fields can't be present at the same time.
		if ( ( !empty( $username ) || !empty( $password ) ) && ( !empty( $signed_request ) || !empty( $access_token ) ) )
			return new JsonResponse( array( 'error' => 'Manual login and FB login can\'t be present at the same time.' ), 400 );
		
		// Authenticate.
		if ( $username && $password ) {
			// Authenticate user credentials.
			$user            = $repo->findOneBy( array( 'email' => $username ) );
			if ($user) {
				$encoder_service = $this->get('security.encoder_factory');
				$encoder         = $encoder_service->getEncoder($user);
				$encoded_pass    = $encoder->encodePassword($password, $user->getSalt());
				$success         = $encoded_pass == $user->getPassword();
			}
		} else if ( $fb_login ) {
			// Authenticate FB login.
			try {
				if ($signed_request) {
          $fbuser = RestHelper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
        } else {
          $fbuser = RestHelper::validate_access_tokenX( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url );
        }
			} catch ( \Exception $e ) {
				return new JsonResponse( array( 'error' => 'Invalid signed request.' ), 400 );
			}
			
			$user = $repo->findOneBy( array( 'fbuid' => $fbuser['user_id'] ) );
			if ( $user )
			  $success = TRUE;
		}

		if ( !$success ) {
      $this->container->get('graphite')->publish('user.login.fail.wronglogin', 1);
			return new JsonResponse(array(
					'error'     => 'Wrong login credentials.',
					'errorcode' => 'invalid-credentials',
					'method'    => $fb_login ? 'facebook' : 'manual'
			), 403);
		}

    
    // If GCM fields are present, let's link the GCM to the user's account.
    if ($req->request->get('uuid') && $req->request->get('gcmid') && $req->request->get('type')) {
      $gcm = $this->container->get('ss.rest.utils')->setGCM(
          $req->request->get('uuid'),
          $req->request->get('gcmid'),
          $req->request->get('type'),
          $user
      );
    }
    

    $this->container->get('graphite')->publish('user.login.success.api', 1);
		return $this->success_auth_res( $user, $from_frontend );
	}


	/**
	 * User registration.
	 * 
	 * Requires $_POST['email']
	 * Requires $_POST['first_name']
	 * Requires $_POST['last_name']
	 * Requires $_POST['password']
   * 
	 * Optional $_POST['fbsigned_request']
	 * Optional $_POST['from_frontend']
	 * 
	 * @todo The registration must be handled by the framework.
	 * 
   * @Route("/register")
   * @Method({"POST"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function registerAction() {
		/* @var $user \SocialSnack\FrontBundle\Entity\User */
		
		$req = $this->getRequest();
    
		// POST variables.
		$username         = $req->get( 'email' );
		$email            = $req->get( 'email' );
		$first_name       = $req->get( 'first_name' );
		$last_name        = $req->get( 'last_name' );
		$password         = $req->get( 'password' );
		$signed_request   = $req->get( 'fbsigned_request' );
		$access_token     = $req->get( 'fbaccess_token' );
		$from_frontend    = $req->get( 'from_frontend' );
		$privacy_check    = !!$req->get( 'privacy_check' );
		
		// Variables to verify the access token.
    /** @todo Use client IDs instead of this. */
    if ( $from_frontend ) {
      $fb_config      = $this->container->getParameter( 'fb' );
    } else {
      $mobile_config  = $this->container->getParameter( 'mobile_app' );
      $fb_config      = $mobile_config['fb'];
    }
		$secret_key       = $fb_config['secret_key'];
		$app_id           = $fb_config['app_id'];
		$app_access_token = $fb_config['app_access_token'];
		$redirect_url     = '';
    
		$required = array(
				'email',
				'first_name',
				'last_name',
				'password',
		);

		if (
				$first_name !== filter_var($first_name, FILTER_SANITIZE_STRING)
				||
				$last_name  !== filter_var($last_name, FILTER_SANITIZE_STRING)
		) {
			return new JsonResponse( array( 'error' => 'Invalid data', 'errorcode' => 'invalid-data' ), 400 );
		}

		// Check required fields
		foreach ( $required as $field ) {
			if ( empty( ${ $field } ) )
				return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		}

		$em   = $this->getDoctrine()->getManager();
		$repo = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
    
    if ( $repo->findBy( array( 'email' => $email ) ) ) {
      return new JsonResponse( array( 'error' => 'Duplicated email', 'errorcode' => 'duplicated-email' ), 400 );
    }
		$user = new \SocialSnack\FrontBundle\Entity\User();
		
		// Authenticate FB login.
		if ( $signed_request || $access_token ) {
			try {
				if ($signed_request) {
          $fbuser = RestHelper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
        } else {
          $fbuser = RestHelper::validate_access_tokenX( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url );
        }
				$user->setFbuid( $fbuser['user_id'] );
        $user->setAvatar( '//graph.facebook.com/' . $fbuser['user_id'] . '/picture' );
        
        /**
         * @todo Download avatar.
         */
			} catch ( \Exception $e ) {
				return new JsonResponse( array( 'error' => 'Invalid signed request.', 'errorcode' => 'invalid-signed-request' ), 400 );
			}
		}
		
		$encoder_service = $this->get('security.encoder_factory');
		$encoder         = $encoder_service->getEncoder($user);
		$encoded_pass    = $encoder->encodePassword( $password, $user->getSalt() );
			
		$user->setEmail( $email );
		$user->setFirstName( $first_name );
		$user->setLastName( $last_name );
		$user->setPassword( $encoded_pass );
    $user->setPrivacyCheck( $privacy_check );
		
		$em->persist( $user );
		
		try {
			$em->flush();
		} catch ( \Exception $e ) {
      if ( $e->getPrevious() && $e->getPrevious()->getCode() == 23000 ) {
        if ( preg_match( '/Duplicate entry.+unique_email/', $e->getMessage() ) ) {
          return new JsonResponse( array( 'error' => 'Duplicated email', 'errorcode' => 'duplicated-email' ), 400 );
        } elseif ( preg_match( '/Duplicate entry.+unique_fbuid/', $e->getMessage() ) ) {
          return new JsonResponse( array( 'error' => 'Duplicated fbuid', 'errorcode' => 'duplicated-fbuid' ), 400 );
        }
      }
			return new JsonResponse( array( 'error' => 'error', 'd' => $e->getMessage() ), 400 );
		}

		$access_token = $this->build_token( $user );
    
    // Deliver the welcome email.
		try {
			$this->get('cinemex.mailer')->enqueue( array(
					'type' => 'welcome',
					'user_id' => $user->getId()
			) );
		} catch ( \Exception $e ) {}

    $this->container->get('graphite')->publish('user.signup.success', 1);

    
    // If GCM fields are present, let's link the GCM to the user's account.
    if ($req->request->get('uuid') && $req->request->get('gcmid') && $req->request->get('type')) {
      $gcm = $this->container->get('ss.rest.utils')->setGCM(
          $req->request->get('uuid'),
          $req->request->get('gcmid'),
          $req->request->get('type'),
          $user
      );
    }
    

		return $this->success_auth_res( $user, $from_frontend );
	}

  
  /**
   * Requires $_POST['email']
   * 
   * @Route("/recoverPass")
   * @Method({"POST"})
   */
  public function recoverPassAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:Default:recoverPass', ['request' => $request]);
  }
	

  /**
   * @Route("/registerGCM")
   * @Method({"POST"})
   */
  public function registerGCMAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:Default:registerGCM', ['request' => $request]);
  }


  /**
   * @Route("/appVersions/{device}", name="rest_v1_app_versions")
   */
  public function appVersionsAction($device) {
    return $this->forward('SocialSnackRestBundle:Default:appVersions', ['device' => $device]);
  }

}
