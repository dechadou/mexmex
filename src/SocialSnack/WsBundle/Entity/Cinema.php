<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cinema
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="cinema_active_idx", columns={"active"}),
 *   @ORM\Index(name="cinema_legacy_idx", columns={"legacy_id"}),
 *   @ORM\Index(name="cinema_attr_idx", columns={"attributes"})
 * })
 * @ORM\Entity(repositoryClass="CinemaRepository")
 */
class Cinema
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

		/**
		 * @var integer
		 * 
		 * @ORM\Column(type="integer")
		 */
		private $legacy_id;
		
		/**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="cinemas")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
		private $state;
		
		/**
     * @ORM\ManyToOne(targetEntity="StateArea", inversedBy="cinemas")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
		private $area;
		
    /**
     * @var string
     *
     * @ORM\Column(name="legacy_name", type="string", length=255)
     */
    private $legacy_name;
		
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

    /**
     * @var boolean
     *
     * @ORM\Column(name="platinum", type="boolean")
     */
    private $platinum;

		/**
		 * @var float
		 * 
		 * @ORM\Column(name="lat", type="float")
		 */
		private $lat;
		
		/**
		 * @var float
		 * 
		 * @ORM\Column(name="lng", type="float")
		 */
		private $lng;
		
		/**
		 * @var string
		 *
		 * @ORM\Column(name="cover", type="string", length=255, nullable=true)
		 */
		private $cover;

    /**
     * @var array
     *
     * @ORM\Column(name="attributes", type="string", length=255)
     */
    private $attributes = '';

		/**
		 * @ORM\OneToMany(targetEntity="\SocialSnack\WsBundle\Entity\Session", mappedBy="cinema")
		 */
		private $sessions;
		
		/**
		 * @ORM\OneToMany(targetEntity="\SocialSnack\WsBundle\Entity\CinemaDistance", mappedBy="from_cinema")
		 */
		private $closest_cinemas;
		
    /**
     * @var active
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = 1;
		
		private $decoded_info = null;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Cinema
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Cinema
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set platinum
     *
     * @param boolean $platinum
     * @return Cinema
     */
    public function setPlatinum($platinum)
    {
        $this->platinum = $platinum;
    
        return $this;
    }

    /**
     * Get platinum
     *
     * @return boolean 
     */
    public function getPlatinum()
    {
        return $this->platinum;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return Cinema
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set state
     *
     * @param \SocialSnack\WsBundle\Entity\State $state
     * @return Cinema
     */
    public function setState(\SocialSnack\WsBundle\Entity\State $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \SocialSnack\WsBundle\Entity\State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set area
     *
     * @param \SocialSnack\WsBundle\Entity\StateArea $area
     * @return Cinema
     */
    public function setArea(\SocialSnack\WsBundle\Entity\StateArea $area = null)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return \SocialSnack\WsBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set legacy_name
     *
     * @param string $legacyName
     * @return Cinema
     */
    public function setLegacyName($legacyName)
    {
        $this->legacy_name = $legacyName;
    
        return $this;
    }

    /**
     * Get legacy_name
     *
     * @return string 
     */
    public function getLegacyName()
    {
        return $this->legacy_name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Cinema
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
		
		
		/**
		 * Get data from the WS cinema object.
		 * 
		 * @todo Reuse this method instad or duplicate it.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->getInfo() );
			}
			if ( !isset( $this->decoded_info->{ $key } ) )
				return NULL;
			
			return $this->decoded_info->{ $key };
		}
		
		
		public function toArray( $populate_sessions = FALSE ) {
			$fields = array(
					'id',
					'name',
					'lat',
					'lng',
					'platinum',
			);
			$result = array();
			foreach ( $fields as $field ) {
				$result[ $field ] = $this->{ $field };
			}
			$result[ 'area' ]    = array(
          'id'   => $this->getArea()->getId(),
          'name' => $this->getArea()->getName()
      );
			$result[ 'state' ]   = array(
          'id'   => $this->getState()->getId(),
          'name' => $this->getState()->getName()
      );
			$result[ 'info' ]       = array(
					'address'         => $this->getData('DIRECCION') ?: '',
					'phone'           => $this->getData('TELEFONOS') ?: '',
          'neighborhood'    => $this->getData('COLONIA') ?: ''
			);

			
			if ( $populate_sessions ) {
				$result[ 'sessions' ] = array();
        if ( TRUE === $populate_sessions ) {
          $sessions = $this->getSessions();
        } else {
          $sessions = $populate_sessions;
        }
				foreach ( $sessions as $session ) {
					$result[ 'sessions' ][] = array(
							'id'        => $session->getId(),
							'date'      => strtotime( $session->getDateTime()->format( 'Y-m-d H:i:s' ) ),
							'cinema_id' => $session->getCinema()->getId(),
					);
				}
			}
			
			return $result;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Cinema
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    
        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Cinema
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    
        return $this;
    }

    /**
     * Get lng
     *
     * @return float 
     */
    public function getLng()
    {
        return $this->lng;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
    /**
     * Set cover
     *
     * @param string $cover
     * @return Cinema
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    
        return $this;
    }

    /**
     * Get cover
     *
     * @return string 
     */
    public function getCover()
    {
        return $this->cover;
    }
		

    /**
     * Add sessions
     *
     * @param \SocialSnack\WsBundle\Entity\Session $sessions
     * @return Cinema
     */
    public function addSession(\SocialSnack\WsBundle\Entity\Session $sessions)
    {
        $this->sessions[] = $sessions;
    
        return $this;
    }

    /**
     * Remove sessions
     *
     * @param \SocialSnack\WsBundle\Entity\Session $sessions
     */
    public function removeSession(\SocialSnack\WsBundle\Entity\Session $sessions)
    {
        $this->sessions->removeElement($sessions);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Add closest_cinemas
     *
     * @param \SocialSnack\WsBundle\Entity\CinemaDistance $closestCinemas
     * @return Cinema
     */
    public function addClosestCinema(\SocialSnack\WsBundle\Entity\CinemaDistance $closestCinemas)
    {
        $this->closest_cinemas[] = $closestCinemas;
    
        return $this;
    }

    /**
     * Remove closest_cinemas
     *
     * @param \SocialSnack\WsBundle\Entity\CinemaDistance $closestCinemas
     */
    public function removeClosestCinema(\SocialSnack\WsBundle\Entity\CinemaDistance $closestCinemas)
    {
        $this->closest_cinemas->removeElement($closestCinemas);
    }

    /**
     * Get closest_cinemas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClosestCinemas()
    {
        return $this->closest_cinemas;
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     * @return Cinema
     */
    public function setAttributes($attributes)
    {
        $this->attributes = json_encode($attributes);
    
        return $this;
    }

    /**
     * Get attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return json_decode($this->attributes);
    }
}