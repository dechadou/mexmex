Cinemex | cinemex.com


Nombre completo: <?php echo $data['name']; ?>

Email: <?php echo $data['email']; ?>

Compañia: <?php echo $data['company']; ?>

Teléfono: <?php echo $data['phone']; ?>

Tipo de evento: <?php echo $data['event_type']; ?>

<?php if (!in_array($data['event_type'], ['tickets', 'etickets'])) : ?>
Cine: <?php echo $data['cinema']->getName(); ?>

Fecha del evento: <?php echo $data['event_date']->format('d F Y'); ?>

Horario del evento: <?php echo $data['event_time']->format('G:i'); ?>

Capacidad de la sala requerida: <?php echo $data['capacity']; ?>

Requiere de productos de dulcería: <?php echo $data['need_candy']; ?>
<?php endif; ?>


Comentarios:
<?php echo $data['comments']; ?>
