<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Beneficios - Cinemex');
$view['slots']->start('iebody');
?>
<h1 class="content-box-title"><span class="icon icon-light icon-crown no-txt"></span> Beneficios</h1>

<div class="entry-body">
  <?php echo $this->render('SocialSnackFrontBundle:Ie:levelsTabs.html.php'); ?>

  <?php foreach ($benefits as $level => $items) : ?>
    <div id="ie-levels-<?php echo $level; ?>" class="ie-levels-block">
      <?php if (empty($items)) : ?>
        <p class="msg">No hay beneficios disponibles actualmente.</p>
      <?php else : ?>
        <div class="slider-container ie-benefits-slider" data-slider data-rotate="false">
          <div class="slider-slider">
            <?php foreach ($items as $benefit) : ?>
              <div class="slide" style="width:100%;height:350px;">
                <img src="<?php echo $view['assets']->getUrl($benefit->getImage(), 'CMS'); ?>" alt="<?php echo $benefit->getName(); ?>" />
                <p><?php echo $benefit->getDescription(); ?></p>
              </div>
            <?php endforeach; ?>
          </div>
          <div class="slider-nav">
            <?php if ($level === 'basic') $icon_color = 'red'; ?>
            <?php if ($level === 'gold') $icon_color = 'gold'; ?>
            <?php if ($level === 'premium') $icon_color = 'black'; ?>
            <a class="arrow-left"><span class="icon icon-larr no-txt icon-<?php echo $icon_color; ?>">Anterior</span></a>
            <a class="arrow-right"><span class="icon icon-rarr no-txt icon-<?php echo $icon_color; ?>">Siguiente</span></a>
          </div>
        </div>
      <?php endif; ?>

      <div class="clear"></div>
    </div>
  <?php endforeach; ?>

  <p class="ie-program-note icon icon-dark icon-eye"><a href="<?php echo $view['assets']->getUrl('/assets/img/ie-benefits-01.jpg'); ?>" class="ie-benefits-bt">Ver comparador de Beneficios</a></p>

  <div class="disclaimer">
    <p>* Una visita es la compra de boletos en taquilla, sin importar cuántos en una sola transacción, para una función determinada. Se podrán tener visitas múltiples en un mismo día siempre y cuando los boletos comprados sean para funciones que no se empalmen, sin importar si son en un mismo complejo CinemexMR. Sólo aplican boletos pagados con puntos de Invitado Especial PAYBACK, efectivo, tarjeta de crédito, tarjeta de débito o certificado de regalo CinemexMR al realizar el pago en taquilla, CAI, Internet, Aplicación CinemexMR y Línea CinemexMR, siempre y cuando se presente la tarjeta del programa Invitado Especial PAYBACK para registrar la visita.</p>
  </div>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after'); ?>
<script>
jQuery(document).ready(function($) {
  $('.ie-benefits-bt').magnificPopup({
    type  : 'image',
    items : [
      { src : '<?php echo $view['assets']->getUrl('/assets/img/ie-benefits-01.jpg'); ?>' },
      { src : '<?php echo $view['assets']->getUrl('/assets/img/ie-benefits-02.jpg'); ?>' }
    ],
    gallery : {
      enabled : true
    },
    image : {
      markup :
        '<div class="mfp-figure" id="ie-benefits-lightbox">'+
          '<div class="mfp-close"></div>'+
          '<div class="mfp-img"></div>'+
        '</div>',
      verticalFit : false
    }
  });
});
</script>
<?php $view['slots']->stop(); ?>
