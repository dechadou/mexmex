<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->start('body')
?>

<div class="clearfix row">
    <div class="narrow-side-col col">
        <?php
        echo $this->render( 'SocialSnackFrontBundle:Sections:pagesMenu.html.php', array(
          'route' => $route,
          'menu'  => array(
            array(
              'label' => 'Página inicial',
              'route' => array('jobs_index'),
              'class' => 'icon-cinemex'
            ),
            array(
              'label' => 'Mision y Valores',
              'route' => array('jobs_mission'),
              'class' => 'icon-cinemex',
            ),
            array(
              'label' => 'Nosotros',
              'route' => array('jobs_about'),
              'class' => 'icon-cinemex',
            ),
            array(
              'label' => 'Solicitud de empleo',
              'route' => array('jobs_form'),
              'class' => 'icon-cinemex',
            )
          )
        ));
        ?>
    </div>

    <div class="wide-main-col col content-box">
        <article class="entry-full">
            <?php $view['slots']->output('jobs_body'); ?>
        </article>
    </div>
</div>

<?php $view['slots']->stop();
