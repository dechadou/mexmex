<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PromoIEType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', NULL, array(
                'label' => 'Título',
            ))
            ->add('file_thumb', 'file', array(
                'label'    => 'Thumb',
                'mapped'   => FALSE,
                'required' => FALSE,
                'image_preview_path' => 'thumb',
                'image_preview_size' => '300x393'
            ))
            ->add('file_image', 'file', array(
                'label'    => 'Imagen grande',
                'mapped'   => FALSE,
                'required' => FALSE,
                'image_preview_path' => 'image'
            ))
            ->add('categories', 'choice', array(
                'label'    => 'Niveles',
                'multiple' => TRUE,
                'expanded' => TRUE,
                'choices'  => array(
                    'basic'   => 'Básico',
                    'gold'    => 'Oro',
                    'amex'    => 'AMEX',
                    'premium' => 'Premium',
                )
            ))
            ->add('active', NULL, array(
                'required' => FALSE,
                'label' => 'Activo',
            ))
            ->add('save', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SocialSnack\FrontBundle\Entity\PromoIE'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_promoie';
    }
}
