<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Forma de Derechos ARCO - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Forma de Derechos ARCO</h1>

<p>Formulario para el ejercicio de los Derechos de Acceso, Rectificación, Cancelación u Oposición según la Ley Federal de Protección de Datos Personales en Posesión de Particulares</p>

<?php echo $view['form']->form($form, array('attr' => array('novalidate' => 'novalidate', 'data-basicvalidate' => '1'))); ?>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('js_after'); ?>
<script>
jQuery( document ).ready( function( $ ) {
  $('#arco_Tipo_RelacionCMX').bind('change', function() {
    var $input;
    switch ($(this).val()) {
      case '1':
        $input = $('#arco_NumEmpleado');
        break;
      case '2':
        $input = $('#arco_NumIE');
        break;
      case '3':
        $input = $('#arco_rel_provider');
        break;
      case '4':
        $input = $('#arco_SolicitaComplejoCheck, #arco_SolicitaTipo');
        break;
      case '5':
        $input = $('#arco_RelacionOtro');
        break;
      case '6':
        $input = $('#arco_RFC');
        break;
      default:
        return false;
        break;
    }
    switch_validation( $('.arco-rel-input'), $input );
  }).trigger('change');
  
  $('#arco_SolicitaTipo').bind('change', function() {
    var $input;
    switch ($(this).val()) {
      case 'complejo':
        $input = $('#arco_SolicitaComplejo');
        break;
      default:
        $input = $('');
        break;
    }
    switch_validation( $('.arco-rel-type-input'), $input );
  }).trigger('change');
  
  var $derecho_acc = $('#arco_CveDerechoAccDat');
  $derecho_acc.delegate('input', 'change', function() {
    var $input;
    switch ($derecho_acc.find(':checked').val()) {
      case '1':
      case '5':
      case '6':
        $input = $('#arco_Comentario');
        break;
      case '2':
        $input = $('#arco_DatoModificar, #arco_DatoActual, #arco_DatoNuevo, #arco_attach_doc');
        break;
      default:
        return;
        break;
    }
    switch_validation( $('.arco-comment-input'), $input );
  }).trigger('change');
  
  function switch_validation($rows, $input) {
    $rows.addClass('hidden');
    $rows.find('input, select').val('').removeAttr('data-validators').disable_validators().trigger('change');
    $rows.find('label').removeClass('required');
    
    $row = $input.closest('.form-row');
    $row.removeClass('hidden');
    $input.attr({'data-validators' : 'not_empty'});
    $row.find('label').addClass('required');
  }
  
  $('.field-validation').closest('.form-row').bind('click', function() {
    $(this).find('.field-validation').fadeOut();
  });
  
  var $rep_fields = $( '#arco_rep_first_name, #arco_rep_last_name, #arco_rep_mlast_name, #arco_rep_email' );
  $rep_fields.bind('change', function( e ) {
    var has_val = false,
        $fields = $rep_fields.add('#arco_rep_attach_attach_id, #arco_rep_attach_attach_power'),
        $labels = $fields.closest('.form-row').find('label');
    $rep_fields.each(function() {
      if ( $(this).val() ) {
        has_val = true;
        return false;
      }
    });
    if ( has_val ) {
      $labels.addClass('required');
      $fields.attr({'data-validators' : 'not_empty'});
    } else {
      $labels.removeClass('required');
      $fields.removeAttr('data-validators').removeClass('error').disable_validators();
    }
  }).trigger('change');;
} );
</script>
<?php $view['slots']->stop(); ?>