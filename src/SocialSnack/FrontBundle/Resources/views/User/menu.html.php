<?php
$menu = array(
		array(
			'label' => 'Mis datos',
			'route' => array( 'user_profile', 'user_profile_edit' ),
			'class' => 'icon-user',
		),
		array(
			'label' => 'Mis compras',
			'route' => 'user_purchases',
			'class' => 'icon-ticket',
		),
		array(
			'label' => 'Invitado especial',
			'route' => 'user_ie',
			'class' => 'icon-crown',
		),
//		array(
//			'label' => 'Películas vistas',
//			'route' => 'user_movies_seen',
//			'class' => 'icon-like',
//		),
//		array(
//			'label' => 'Quieres ver',
//			'route' => 'user_wanna_see',
//			'class' => 'icon-eye',
//		),
);
?>
<ul class="panel-menu">
	<?php
	foreach ( $menu as $item ) {
		if ( !is_array( $item['route'] ) ) {
			$item['route'] = array( $item['route'] );
		}
		$current = ( in_array( $route, $item['route'] ) );
	?>
	<li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
	<?php
	}
	?>
</ul>