<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$places = array();

$view->extend('SocialSnackFrontBundle:Premium:base.html.php');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="clearfix row">
  <div class="side-col col">
    <div class="sidebar-heading">
      <span class="icon icon-star pad-left sidebar-title"><?php echo $state->getName(); ?></span>

      <div class="sidebar-selector">
        <a href="#" class="btn btn-default-alt switch-cinema btn-icon icon icon-rarr btn-icon-right" rel="ask_location">Selecciona otro estado</a>

        <select id="cinemas-select-city">
          <?php foreach ( $states as $_state ) { ?>
          <option value="<?php echo $_state->getId(); ?>" <?php if ( $_state->getId() == $state->getId() ) echo 'selected="selected"'; ?>><?php echo $_state->getName(); ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <?php if ($found) : ?>
    <ul class="collapsable-list">
      <?php foreach ( $tree as $item ) : $area = $item['area']; ?>
      <li>
        <a href="#" class="sidebar-filter collapsable-anchor"><?php echo $area->getName(); ?></a>

        <ul class="collapsable-sublist">
          <?php foreach ( $item['cinemas'] as $cinema ) : ?>
          <?php $places[] = array( 'lat' => $cinema->getLat(), 'lng' => $cinema->getLng(), 'id' => $cinema->getId() ); ?>
          <li id="cinema-item-<?php echo $cinema->getId(); ?>" class="cinema-item" data-pos="<?php echo $cinema->getLat() . ',' . $cinema->getLng(); ?>" data-address="<?php if ( is_string( $value = $cinema->getData( 'DIRECCION' ) ) ) { echo $value; } ?>">
            <a href="<?php echo $view['fronthelper']->get_permalink($cinema); ?>#filter=premium"><?php echo $cinema->getName(); ?></a>
          </li>
          <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach; ?>
    </ul>
  <?php endif; ?>
  </div>

  <div class="main-col col">
<?php if ($found) : ?>
    <div id="gmap"></div>
<?php else : ?>
    <p class="msg">Lo sentimos. Aún no hay salas Cinemex Platino en tu estado.</p>
<?php endif; ?>
  </div><!-- #cinema-details -->
</div>

<script>
  var fallback = <?php echo $fallback ? 'true' : 'false'; ?>;
  var places = <?php echo json_encode( $places ); ?>;
  var marker_img = '<?php echo $view['assets']->getUrl('assets/img/gmaps-marker.png'); ?>';
</script>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script>
$( document ).bind( 'user_init', function() {
  // Filter cinemas in the state of the user's preferred cinema.
  if ( user.preferred_cinema ) {
    var $in_state = $('[data-state="' + user.preferred_cinema.state_id + '"]'),
        $not_in   = $('.cinemas-grid-item').not( $in_state ),
        $grid     = $in_state.parent();

    // If no Platinum cinemas in the state, display all of them.
    if (!$in_state.length) {
      $not_in.removeClass('hidden');
    }

    $not_in.appendTo($grid);
    $in_state.removeClass('hidden');
    $grid.after( '<div class="end-body-bt" style="margin-top:50px;"><a href="#" class="btn btn-default btn-icon icon icon-reload" rel="grid-load-more">Ver todas las salas Platino</a></div>' );
    $( '.end-body-bt a' ).bind( 'click', function( e ) {
      e.preventDefault();
      $not_in.hide().removeClass('hidden').fadeIn();
      $( this ).parent().remove();
    } );
  }
} );
</script>
<?php $view['slots']->stop(); // js_after_vendor ?>
