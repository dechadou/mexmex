<?php

namespace SocialSnack\WsBundle\Service;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;

class Helper {


	/**
	 * Parse the coordinates string provided by the WS and return into decimal format.
	 *
	 * @param string $coord
	 * @return integer
	 */
	public static function coord_to_dec( $coord ) {
		$coord = trim( $coord );

		if ( is_float( $coord ) )
			return $coord;

		if ( preg_match( '/^[^0-9]*([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9\.]+)[^0-9A-Z]+([NSEOW])$/', $coord, $parts ) ) {
			return self::DMStoDEC( $parts[1], $parts[2], (float)$parts[3], $parts[4] );
		} else if ( preg_match( '/^[0-9\-\.]+$/', $coord ) ) {
      return (float)$coord;
    }

		return FALSE;
	}


	/**
	 * Converts DMS ( Degrees / minutes / seconds )
	 * to decimal format longitude / latitude
	 *
	 * @param integer $deg
	 * @param integer $min
	 * @param integer $sec
	 * @param string  $dir
	 */
	public static function DMStoDEC( $deg, $min, $sec, $dir ) {
		$dir = strtoupper( $dir );
		if ( 'N' == $dir || 'E' == $dir ) {
			$sign = 1;
		} else {
			$sign = -1;
		}

    return ( $deg + ( ( ( $min * 60 ) + ( $sec ) ) / 3600 ) ) * $sign;
	}


	public static function get_dates_range() {

	}


	/**
	 * Convert yyyymmdd string to yyyy-mm-dd string format.
	 *
	 * @param  string $in yyyymmdd
	 * @return string     yyyy-mm-dd
	 */
	public static function format_ymd( $in ) {
		$out = substr_replace( $in, '-', 4, 0 );
		$out = substr_replace( $out, '-', 7, 0 );
		return $out;
	}


	/**
	 *
	 * @param \Doctrine\ORM\QueryBuilder $qb
	 * @param type $date_from
	 * @param type $date_to Return sessions with date lt $date_to (lt not lte!).
	 *                      NULL for $date_to = $date_from.
	 *                      0 to return all the future sessions.
	 * @return boolean
	 */
	public static function dql_apply_dates( &$qb, $date_from = NULL, $date_to = NULL ) {
		/** @todo Define the right timezone */
//		date_default_timezone_set("UTC");

		if ( is_null( $date_from ) )
			return FALSE;

		$now       = new \DateTime( 'now' );
		$today     = new \DateTime( 'today' );
		if ( !( $date_from instanceof \DateTime ) ) {
			$date_from = new \DateTime( $date_from );
		}

		if ( $date_from == $today ) {
			$date_from = $now;
		}

		if ( is_null( $date_to ) ) {
			$date_to = $date_from;
		} else if ( $date_to && !( $date_to instanceof \DateTime ) ) {
			$date_to = new \DateTime( $date_to );
		}

		if ( $date_to && $date_from->format( 'Y-m-d' ) == $date_to->format( 'Y-m-d' ) ) {
			// Single day.
			$qb->andWhere( 's.date = :date_from' )
					->setParameter( 'date_from', $date_from->format( 'Y-m-d' ) );
		} else {
			// Dates range.
			$qb->andWhere( 's.date >= :date_from' )
					->setParameter( 'date_from', $date_from->format( 'Y-m-d' ) );

			// $date_to === 0 => Ad eternum
			if ( 0 !== $date_to ) {
				$qb->andWhere( 's.date < :date_to' )
						->setParameter( 'date_to',   $date_to->format( 'Y-m-d' ) );
			}
		}

		// If quering movies for today display only movies for the remaining hours of the day.
		if ( $date_from->format( 'Ymd' ) == $today->format( 'Ymd' ) ) {
			$qb->andWhere( $qb->expr()->gte( 's.date_time', ':full_date' ) )
					->setParameter( 'full_date', $date_from->format( 'Y-m-d H:i:s' ) );
		}

	}


  public static function dql_apply_movie_attrs( &$qb, $attributes ) {
    foreach ( $attributes as $k => $v ) {
      $qb->andWhere( $qb->expr()->like( 'm.attributes', ":attr_$k" ) )
          ->setParameter( "attr_$k", sprintf( '%%"%s":%s%%', $k, $v ? 'true' : 'false' ) );
    }
  }


  public static function dql_apply_movie( &$qb, $movie, $table ) {
    if ( $movie instanceof Movie ) {
      if ( $movie->getGroupId() ) {
        $qb->andWhere( $table.'.group_id = :group_id')
            ->setParameter( 'group_id', $movie->getGroupId() );
      } else {
        $qb->andWhere( $table.'.movie = :movie_id' )
            ->setParameter( 'movie_id', $movie );
      }
    } else {
      $qb->andWhere( $table.'.movie = :movie_id' )
          ->setParameter( 'movie_id', $movie );
    }
  }


	/**
	 * Parse the seats layout string returned by the Web Service and return as an
	 * array.
	 *
	 * @param string $str
	 * @return array
	 */
	public static function parse_seats_str( $str ) {
		$rows = explode( 'RowNumber', $str );
		$result = array();

		if ( !preg_match( '/CodCategoria\:([0-9]+)\|NumCategoria\:([0-9]+)\|/i', $rows[0], $matches ) )
			return FALSE;

		$cat_code = $matches[1];
		$cat_num  = $matches[2];

		for ( $i = 1 ; $i < sizeof( $rows ) ; $i++ ) {
			preg_match( '/^\:([0-9]+)\|RowName:([A-Z0-1]+)\|(.+)$/i', $rows[$i], $matches );
			$row_num  = (int)$matches[1];
			$row_name = $matches[2];
			$seats    = explode( '|', $matches[3] );
			$result[$row_num] = array(
					'name'  => $row_name,
					'seats' => array(),
			);
			foreach ( $seats as $seat ) {
				if ( !preg_match( '/^([0-9]+),([0-9A-Z]+)\:(.+)$/', $seat, $matches ) )
					continue;
				$result[$row_num]['seats'][(int)$matches[1]] = $matches[3];
			}
      $row_start = min(array_keys($result[$row_num]['seats']));
		}

    $seats_per_row = sizeof($seats);

    // If seats row letter missing (ie: jumps from A to C to E, etc) fill the
    // missing rows with empty (E) spaces. This is used in platinum theaters
    // Where the spacing between rows is bigger.
    foreach ( range(1, max(array_keys($result))) as $i ) {
      if ( !isset($result[$i]) ) {
        $result[$i] = array(
            'name'  => '',
            'seats' => array_fill( $row_start, $seats_per_row, 'E' )
        );
      }
    }

		return array(
				'seats'    => $result,
				'cat_code' => $cat_code,
				'cat_num'  => $cat_num,
		);
	}


	/**
	 * Visual representation of the theater.
	 *
	 * @todo Fill empty (non present) rows.
	 *
	 * @param array $seats_layout
	 */
	public static function seats_to_table( $seats_layout ) {
		echo '<table>';
		foreach ( $seats_layout as $row_num => $row ) {
			echo '<tr>';
			echo '<td style="text-align:center;height:25px;width:25px;">' . $row['name'] . '</td>';
			foreach ( $row['seats'] as $seat_num => $status ) {
				switch ( $status ) {
					case '0':
						$color = '#cccccc';
						$label = $seat_num;
						break;
					case '1':
						$color = '#FF0000';
						$label = 'X';
						break;
					case '2':
						$color = '#00FF00';
						$label = 'X';
						break;
					case 'E':
						$color = '#eeeeff';
						$label = '';
						break;
				}
				echo '<td style="text-align:center;height:25px;width:25px;background:' . $color . '" title="' . ( isset( $row['owner'] ) ? $row['owner'] : '' ) . '">' . $label . '</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
	}


	public static function merge_seats_to_layout( $seats_layout, $new_seats ) {
		foreach ( $new_seats as $seat ) {
			$seats_layout[$seat['row']]['seats'][$seat['seat']] = 1;
		}
		return $seats_layout;
	}


  public static function get_longest_common_subsequence( $string_1, $string_2 ) {
    $string_1_length = strlen($string_1);
    $string_2_length = strlen($string_2);
    $return = "";

    if ($string_1_length === 0 || $string_2_length === 0) {
      // No similarities
      return $return;
    }

    $longest_common_subsequence = array();

    // Initialize the CSL array to assume there are no similarities
    for ($i = 0; $i < $string_1_length; $i++) {
      $longest_common_subsequence[$i] = array();
      for ($j = 0; $j < $string_2_length; $j++) {
        $longest_common_subsequence[$i][$j] = 0;
      }
    }

    $largest_size = 0;

    for ($i = 0; $i < $string_1_length; $i++) {
      for ($j = 0; $j < $string_2_length; $j++) {
        // Check every combination of characters
        if ($string_1[$i] === $string_2[$j]) {
          // These are the same in both strings
          if ($i === 0 || $j === 0) {
            // It's the first character, so it's clearly only 1 character long
            $longest_common_subsequence[$i][$j] = 1;
          } else {
            // It's one character longer than the string from the previous character
            $longest_common_subsequence[$i][$j] = $longest_common_subsequence[$i - 1][$j - 1] + 1;
          }

          if ($longest_common_subsequence[$i][$j] > $largest_size) {
            // Remember this as the largest
            $largest_size = $longest_common_subsequence[$i][$j];
            // Wipe any previous results
            $return = "";
            // And then fall through to remember this new value
          }

          if ($longest_common_subsequence[$i][$j] === $largest_size) {
            // Remember the largest string(s)
            $return = substr($string_1, $i - $largest_size + 1, $largest_size);
          }
        }
        // Else, $CSL should be set to 0, which it was already initialized to
      }
    }

    // Return the list of matches
    return $return;
  }

  public static function parse_attrs_and_clean_title( $title ) {
    $codes = array(
        'dig'        => 'digital',
        'ing'        => 'lang_en',
        'esp'        => 'lang_es',
        'fra'        => 'lang_fr',
        'ale'        => 'lang_de',
        'jap'        => 'lang_jp',
        '2d'         => 'v2d',
        '3d'         => 'v3d',
        '4d'         => 'v4d',
        'x4d'        => 'v4d',
        '4dx'        => 'v4d',
        'hfr'        => 'hfr',
        'cx'         => 'cx',
        '-clasicos-' => 'classics'
    );
    $title = explode( ' ', $title );
    $attrs = array();
    while ( ( $key = strtolower( end( $title ) ) ) && key_exists( $key, $codes ) ) {
      $attrs[$codes[$key]] = TRUE;
      array_pop( $title );
    }
    $title = implode( ' ', $title );
    return array( $title, $attrs );
  }


  /**
   *
   * @param \SocialSnack\WsBundle\Entity\Session $session
   * @return boolean
   */
  public static function session_is_jumbo( $session ) {
    $cinemas = array(
        1039 => array( 11 ),  // Cinemex Ixtapaluca
    );

    if ( !$cinema_id = $session->getLegacyCinemaId() )
      return FALSE;

    if ( !isset( $cinemas[$cinema_id] ) )
      return FALSE;

    $screen = $session->getData('SCREENNAME');
    if ( !preg_match( '/(?<num>\d+)/', $screen, $match ) )
      return FALSE;
    $num = (int)$match['num'];

    return in_array( $num, $cinemas[$cinema_id] );
  }


  /**
   *
   * @param \SocialSnack\WsBundle\Entity\Session $session
   * @return boolean
   */
  public static function session_is_macro( $session ) {
    $cinemas = array(
        1163 => array( 6 ),  // Cinemex Morelia
    );

    if ( !$cinema_id = $session->getLegacyCinemaId() )
      return FALSE;

    if ( !isset( $cinemas[$cinema_id] ) )
      return FALSE;

    $screen = $session->getData('SCREENNAME');
    if ( !preg_match( '/(?<num>\d+)/', $screen, $match ) )
      return FALSE;
    $num = (int)$match['num'];

    return in_array( $num, $cinemas[$cinema_id] );
  }


  /**
   *
   * @param \SocialSnack\WsBundle\Entity\Session $session
   * @return boolean
   */
  public static function session_is_extreme( $session ) {
    $cinemas = array(
        1044 => array( 1 ),  // Antara
        1025 => array( 1 ),  // Universidad
        1032 => array( 15 ), // Santa Fe
        1029 => array( 19 ), // Mundo E
        1040 => array( 10 ), // Parque Delta
        1035 => array( 6 ),  // Aragon
        1055 => array( 7 ),  // Humberto Lobo
        1095 => array( 2 ),  // Plaza Mayor
    );

    if ( !$cinema_id = $session->getLegacyCinemaId() )
      return FALSE;

    if ( !isset( $cinemas[$cinema_id] ) )
      return FALSE;

    $screen = self::get_session_screen_number($session);
    if (!$screen) {
      return FALSE;
    }

    return in_array( $screen, $cinemas[$cinema_id] );
  }


  public static function get_premium_cinemas() {
    return array(
        1016 => array( 2,3,4,5 ), // Cinemex Cuauhtemoc
        1017 => array( 4,5,6,7 ), // Cinemex Galerías
        1007 => array( 4,5,6,7 ), // Cinemex Insurgentes
        1043 => array( 6,7,8,9 ), // Cinemex Patriotismo
        1195 => array( 1,2,3,4 ), // Cinemex Reforma
        1029 => array( 5,6,7,8,9,10 ), // Cinemex Mundo E
        1022 => array( 1,2,11,12 ), // Cinemex San Mateo
        1128 => array( 12,13 ), // Cinemex Galerías Saltillo
        1019 => array( 5,6,7,8 ), // Cinemex Ticoman
        1154 => array( 1,2,3,4 ), // Cinemex Valle Dorado
        1042 => array( 1,2,3,4,5,6,7,8,9 ), // Cinemex Interlomas
        1027 => array( 5,6,7,8 ), // Cinemex Loreto
        1046 => array( 7,8,9,10 ), // Cinemex Lerma
        1009 => array( 1,2,3,4,5,6,7 ), // Cinemex Metepec
        1203 => array( 1,3,7,9 ), // Cinemex Galerías León
        1092 => array( 1,2,3,4,5,6,7,8,9 ), // Cinemex Cordilleras
        1197 => array( 1,2,4,5,6,7 ), // Cinemex Zapopan
        1062 => array( 1,2,4,6 ), // Cinemex San Nicolás
        1060 => array( 1,2,4,10 ), // Cinemex Lindavista
        1055 => array( 6,8,10,12 ), // Cinemex Humberto Lobo
        1056 => array( 2,3,4,5 ), // Cinemex San Agustín
        1021 => array( 1,2,3,4,5,6 ), // Cinemex Diana
        1102 => array( 8,10,11,12 ), // Cinemex Plaza Real Reynosa
        1097 => array( 7,8,9,10 ), // Cinemex Zacatecas
        1233 => array( 7,8,9,10,11 ), // Cinemex CNA
        1247 => array( 1,2,3,4,5,6,7,8,9,10 ), // Cinemex Lomas Verdes
        1249 => array( 1,2,3,4 ), // Cinemex Triangulo Puebla
        1243 => array( 4,5,6,7 ), // Cinemex Reforma 222
        1245 => array( 6,7,8,9,10 ), // Duraznos
        1254 => array( 7,9,10,12 ), // Cinemex Boulevares Querétaro
        1273 => array( 1,2,3,4,5,6,7,8,9,10 ), // Cinemex Metrocentro Hermosillo
        1269 => array( 1,2,3,4,5,6,7,8 ), // Cinemex City Center Mérida
    );
  }
  /**
   *
   * @param \SocialSnack\WsBundle\Entity\Session $session
   * @return boolean
   */
  public static function session_is_premium( $session ) {
    $cinemas = self::get_premium_cinemas();

    if ( !$cinema_id = $session->getLegacyCinemaId() )
      return FALSE;

    if ( !isset( $cinemas[$cinema_id] ) )
      return FALSE;

    if ('*' === $cinemas[$cinema_id])
      return TRUE;

    $screen = self::get_session_screen_number($session);
    if (!$screen) {
      return FALSE;
    }

    return in_array( $screen, $cinemas[$cinema_id] );
  }


  public static function get_session_screen_number(Session $session) {
    $screen = $session->getData('SCREENNAME');
    if ( !preg_match( '/(?<num>\d+)/', $screen, $match ) )
      return FALSE;

    return (int)$match['num'];
  }


  public static function get_from_result_by_id( $result, $id ) {
    return current( array_filter( $result, function($a) use($id) { return $a->getId() == $id; } ) );
  }


  public static function get_from_result_by_legacy_id( $result, $id ) {
    return current( array_filter( $result, function($a) use($id) { return $a->getLegacyId() == $id; } ) );
  }


  public static function get_from_result_by_legacy_id_bis( $result, $id ) {
    return current( array_filter( $result, function($a) use($id) { return $a->getLegacyIdBis() == $id; } ) );
  }


  /**
   * Callback function for usort() used by self::sortMoviesDefault()
   *
   * @param Movie $a
   * @param Movie $b
   * @return int
   */
  public static function sortMoviesDefaultCB(Movie $a, Movie $b) {
    /*if ( $a->getPremiere() && !$b->getPremiere() )
      return -1;
    elseif ( !$a->getPremiere() && $b->getPremiere() )
      return 1;*/
    if ($a->getFixedPosition() > $b->getFixedPosition())
      return -1;
    else if ($a->getFixedPosition() < $b->getFixedPosition())
      return 1;


    if ( $a->getPosition() > $b->getPosition() )
      return -1;
    elseif ( $a->getPosition() < $b->getPosition() )
      return 1;

    return 0;
  }


  /**
   * Sorts a movie collection by premiere status and position.
   *
   * @param array $movies
   * @return bool
   */
  public static function sortMoviesDefault(&$movies) {
    return usort($movies, ['self', 'sortMoviesDefaultCB']);
  }


  /**
   * Callback function for usort() used by self::sortMoviesByName()
   *
   * @param Movie $a
   * @param Movie $b
   * @return int
   */
  public static function sortMoviesByNameCB(Movie $a, Movie $b) {
    if ($a->getName() === $b->getName()) {
      return 0;
    }

    return ($a->getName() < $b->getName()) ? -1 : 1;
  }


  /**
   * Sorts a movie collection by movie name.
   *
   * @param array $movies
   * @return bool
   */
  public static function sortMoviesByName(&$movies) {
    return usort($movies, ['self', 'sortMoviesByNameCB']);
  }


  public static function session_allows_seat_allocation(Session $session) {
    return ('Y' === $session->getData('SEATALLOCATIONON'));
  }


  public static function getNoOnlineSellingGroupIds() {
    return [
        '10954', // Alivio
    ];
  }

}
