<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class LandingPromoType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('name', 'text', ['label' => 'Nombre']);

    $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'postSetData']);
  }


  public function postSetData(FormEvent $event) {
    $form = $event->getForm();
    $data = $event->getData();

    $form->add('thumb', 'file', [
        'label' => 'Thumb',
        'required' => !$data['thumb_path'],
        'image_preview_path' => 'thumb_path',
        'image_preview_size' => '',
    ]);
    $form->add('thumb_path', 'hidden', [
        'label' => 'Thumb',
        'required' => FALSE,
    ]);
    $form->add('image', 'file', [
        'label' => 'Imagen',
        'required' => !$data['image_path'],
        'image_preview_path' => 'image_path',
        'image_preview_size' => '',
    ]);
    $form->add('image_path', 'hidden', [
        'label' => 'Imagen',
        'required' => FALSE,
    ]);

  }


  public function getName() {
    return '';
  }

}