<?php

namespace SocialSnack\WsBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionAggregatorCommand extends LoopedCommandAbstract {

  protected $batch_size = 100;

  protected $iterations = 0;

  protected $lock_id = 'ws:transaction:aggregator';

  /**
   * @var \SocialSnack\WsBundle\Handler\LockHandler
   */
  protected $lock_handler;

  protected $logger;

  protected $processed = 0;


  protected function configure() {
    $this
        ->setName('ws:transaction:aggregator')
        ->setDescription('Performs data aggregation tasks from Transactions.')
        ->addOption('processes', 'ps', InputOption::VALUE_OPTIONAL, 'Output of ps command.', NULL)
        ->addOption('test-email', NULL, InputOption::VALUE_OPTIONAL, 'Send an email to test the delivery system.', FALSE)
        ->setHelp('Run with "ps aux | grep php | xargs -0 -I ps php app/console ws:tasks:consume --processes ps"')
    ;
  }


  protected function execute(InputInterface $input, OutputInterface $output) {
    // Test emails
    if ($input->getOption('test-email')) {
      $this->triggerAlerts([
          'Prueba'
      ]);
      return;
    }

    // @todo Set a time limit
    set_time_limit( 0 );

    $this->lock_handler = $this->getContainer()->get('lock.handler');
    $this->logger       = $this->getContainer()->get('logger');

    // Set lock (and check before)
    $processes = $input->getOption('processes');
    $this->lock_handler->isLocked($this->lock_id, $processes);
    $this->lock_handler->setLock($this->lock_id);

    $this->startTimer();

    $this->aggregateHourlyStats();
    $this->analyzeTransactions();

    while (1) {
      $this->iterations++;

      $this->checkStatus();

      $result = $this->process();

      if ($result === 0) {
        $this->terminate();
      } elseif ($result !== FALSE) {
        $this->processed += $result;
      }
    } // while

    $this->terminate();
  }


  protected function terminate($normal = TRUE) {
    $this->lock_handler->releaseLock($this->lock_id);

    if (!$normal) {
      $this->logger->error(sprintf('Command %s terminated abnormally.', $this->getName()));
    }

    echo sprintf('%d transactions processed.', $this->processed) . "\r\n";

    die();
  }


  protected function process() {
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
    $doctrine = $this->getContainer()->get('doctrine');
    $conn     = $doctrine->getConnection();

    $max_id = $conn->fetchColumn('SELECT MAX(transaction_id) FROM TransactionDataTickets');

    $transactions = $doctrine->getRepository('SocialSnackFrontBundle:Transaction')->getAggregationDataSince($max_id, $this->batch_size);

    foreach ($transactions as $transaction) {

      $attributes = json_decode($transaction['attributes']);
      foreach ($attributes as $attr => $v) {
        if (!$v) {
          continue;
        }

        $conn->insert('TransactionDataAttr', [
            'movie_id'       => $transaction['movie_id'],
            'transaction_id' => $transaction['id'],
            'group_id'       => $transaction['group_id'],
            'attr'           => $attr,
            'qty'            => $transaction['tickets_count'],
            'amount'         => $transaction['amount'],
        ]);
      }

      $tickets = json_decode($transaction['tickets']);
      foreach ($tickets as $ticket) {
        $conn->insert('TransactionDataTickets', [
            'movie_id'       => $transaction['movie_id'],
            'transaction_id' => $transaction['id'],
            'group_id'       => $transaction['group_id'],
            'ticket'         => $ticket->name,
            'qty'            => $ticket->qty,
            'amount'         => $ticket->price * $ticket->qty,
        ]);
      }
    }

    return sizeof($transactions);
  }


  protected function aggregateHourlyStatsByApp() {
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
    $doctrine = $this->getContainer()->get('doctrine');
    $conn     = $doctrine->getConnection();

    // Date from: next hour from last inserted date.
    $max_date = $conn->fetchColumn("SELECT MAX(date) FROM TransactionsByAppHourly;");
    $date_from = new \DateTime($max_date);
    $date_from->add(new \DateInterval('PT1H'));

    // Date to: this hour, 0 minutes, 0 seconds.
    $date_to = new \DateTime('now');
    $date_to->setTime($date_to->format('H'), 0, 0);


    // Insert transactions by app between those dates.
    $insert = "INSERT INTO TransactionsByAppHourly
        (amount, tickets, app_id, date)
        SELECT COUNT(*), SUM(tickets_count), app_id, DATE_FORMAT(purchase_date, '%Y-%m-%d %H') as day
            FROM Transaction
            WHERE app_id IS NOT NULL
            AND purchase_date >= :date_from AND purchase_date < :date_to
            GROUP BY day, app_id;";

    $stmt = $conn->prepare($insert);
    $stmt->bindValue('date_from', $date_from, 'datetime');
    $stmt->bindValue('date_to', $date_to, 'datetime');

    $stmt->execute();
  }


  protected function aggregateHourlyStatsByCinema() {
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
    $doctrine = $this->getContainer()->get('doctrine');
    $conn     = $doctrine->getConnection();

    // Date from: next hour from last inserted date.
    $max_date = $conn->fetchColumn("SELECT MAX(date) FROM TransactionsByCinemaHourly;");
    $date_from = new \DateTime($max_date);
    $date_from->add(new \DateInterval('PT1H'));

    // Date to: this hour, 0 minutes, 0 seconds.
    $date_to = new \DateTime('now');
    $date_to->setTime($date_to->format('H'), 0, 0);


    // Insert transactions by cinema between those dates.
    $insert = "INSERT INTO TransactionsByCinemaHourly
        (amount, cinema_id, date)
        SELECT COUNT(*), cinema_id, DATE_FORMAT(purchase_date, '%Y-%m-%d %H') as day
            FROM Transaction
            WHERE purchase_date >= :date_from AND purchase_date < :date_to
            GROUP BY day, cinema_id;";

    $stmt = $conn->prepare($insert);
    $stmt->bindValue('date_from', $date_from, 'datetime');
    $stmt->bindValue('date_to', $date_to, 'datetime');

    $stmt->execute();
  }


  protected function aggregateHourlyStats() {
    $this->aggregateHourlyStatsByApp();
    $this->aggregateHourlyStatsByCinema();
  }


  /**
   * Look for transactions anomalies.
   */
  protected function analyzeTransactions() {
    /** @var \SocialSnack\WsBundle\Service\TransactionAnalyzer $analyzer */
    $analyzer = $this->getContainer()->get('ws.transaction.analyzer');
    $alerts = $analyzer->analyzeTransactions();

    if (sizeof($alerts)) {
      $this->triggerAlerts($alerts);
    }
  }



  /**
   * Deliver an alert email.
   *
   * @param array $alerts
   */
  protected function triggerAlerts(array $alerts) {
    $body = date('Y-m-d H:i') . "\r\n" . implode("\r\n\r\n", $alerts);

    // @todo Move hardcoded arguments to parameters!
    $message = \Swift_Message::newInstance()
        ->setSubject('Alerta de ventas de cinemex.com')
        ->setFrom(array('noresponder@cinemex.com' => 'Alerta cinemex.com'))
        ->setTo(['guido@socialsnack.com', 'juanpablo@socialsnack.com', 'lea@socialsnack.com'])
        ->setBody($body)
    ;
    $this->getContainer()->get('mailer')->send($message);
  }


  protected function getMemoryLimit() {
    return 67108864; // 64M
  }

  protected function getTimeLimit() {
    return 3600; // 1 hour
  }

  protected function getIterationsLimit() {
    return 50;
  }

  protected function getLogger() {
    return $this->logger;
  }

}