<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;

/**
 * Class X4D
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class X4D extends ConceptHelperAbstract {

  protected function getConceptName() {
    return 'X4D';
  }

} 