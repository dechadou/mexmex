<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start( 'body' );

$type = FrontHelper::get_movie_attr_str($session->getMovie()->getAttr(), ', ');

$cinema = $transaction->getData('CODIGOCINE');
$ticket = $transaction->getData('TRANSACCION');
$date		= $transaction->getPurchaseDate();
?>
<!-- BODY -->
<table class="body-wrap" bgcolor="#ebebeb" style="width: 100%; padding-bottom: 30px;">
	<tr>
		<td></td>
		<td class="container" align="" bgcolor="#ebebeb" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">

			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5;">
				<table>
					<tr>
						<td>

							<h1 style="color: #d0033d">¡Ya tienes tus boletos!</h1>

<?php if ( $view['fronthelper']->cinema_has_qr($transaction->getCinema()) ) : ?>
							<p>Esta es tu confirmación de compra. Pasa directo al acceso de tu sala mostrando el código QR en cualquier dispositivo móvil, tablet o impresión.</p>
<?php else : ?>
							<p>Esta es tu confirmación de compra. Pasa por tus boletos a taquilla indicando el código de compra o con la tarjeta con la que hiciste tu compra.</p>
<?php endif; ?>
						</td>
					</tr>
				</table>
			</div><!-- /content -->

			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5; border-top: 1px solid #fbfbfb;">
				<table bgcolor="" width="100%">
					<tr>
						<td style="vertical-align: middle; width: 50%;">
							<h4 style="color: #d0033d; font-size: 16px;">Código de compra</h4>
						</td>
						<td style="vertical-align: middle; width: 50%;">
							<h4 style="color: #d0033d; font-size: 16px;">Código QR</h4>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: middle; width: 50%;">
							<p style="font-size: 20px;"><?php echo $transaction->getCode(); ?></p>
						</td>
						<td style="vertical-align: middle; width: 50%;">
							<?php /*<p><img src="<?php echo $view['fronthelper']->get_static_url( $transaction->getQr(), TRUE ); ?>" /></p>*/ ?>
							<p><img src="http:<?php echo $view['assets']->getUrl($transaction->getQr(), 'QR'); ?>" /></p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->

			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-top: 1px solid #fbfbfb;">
				<table bgcolor="">
					<tr>
						<td style="vertical-align: middle;">
							<h2>Datos de la compra</h2>
							<ul>
								<li>
									Título:
									<?php echo $session->getMovie()->getName(); ?>
								</li>

								<?php if ($type) { ?>
								<li>
									Versión:
									<?php echo $type; ?>
								</li>
								<?php } ?>

								<li>
									Día:
									<?php echo FrontHelper::get_session_nice_date( $session->getDate() ); ?>
								</li>

								<li>
									Horario:
									<?php echo $session->getTime()->format( 'h:i A' ); ?>
								</li>

								<li>
									Cine:
									<?php echo $session->getCinema()->getName(); ?>
								</li>

								<li>
									Sala:
									<?php echo FrontHelper::confirmation_get_screenname($transaction); ?>
								</li>

								<li>
									Tus boletos:
									<?php echo FrontHelper::confirmation_get_ticketslist($transaction, ', '); ?>
								</li>

								<?php if ( $transaction->getSeats() ) { ?>
								<li>
									Asientos seleccionados:
									<?php echo FrontHelper::confirmation_get_seatslist($transaction, ' / '); ?>
								</li>
								<?php } ?>

								<?php if ( $iecode = $transaction->getData('NUMIE') ) { ?>
								<li>
									Invitado especial:
									<?php echo $iecode; ?>
								</li>
								<?php } ?>

								<li>
									Total pagado:
									<?php echo FrontHelper::format_price( $transaction->getAmount() ); ?>
								</li>
							</ul>
						</td>
					</tr>
				</table>
			</div><!-- /content -->

			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-top: 1px solid #fbfbfb;">
				<table>
					<tr>
						<td style="vertical-align: middle;">
							<h2>Datos para Comprobante Fiscal</h2>

							<ul>
								<li>
									Complejo: <?php echo $cinema; ?>
								</li>

								<li>
									No. Ticket: <?php echo $ticket; ?>
								</li>

								<li>
									Fecha de compra: <?php echo $date->format('d/m/Y'); ?>
								</li>
							</ul>

							<p><a href="http://webportal.edicomgroup.com/customers/cinemex/consulta-ticket-cfdi-cinemex.html" style="display:block;text-align: center;border:0;text-decoration: none;color: #d0033d;line-height: 3;margin-top: 40px;border-radius: 3px;">Solicita tu Factura</a></p>
						</td>
					</tr>
				</table>
			</div><!-- .content -->

		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->
<?php
$view['slots']->stop();
?>
