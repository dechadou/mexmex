<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;

/**
 * Class V3D
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class V3D extends ConceptHelperAbstract {


  protected function getConceptName() {
    return '3D';
  }

} 