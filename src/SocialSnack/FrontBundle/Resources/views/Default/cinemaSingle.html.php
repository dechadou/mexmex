<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['cinema-single']); ?>
<?php $view['slots']->set('title', $cinema->getName() . ' - Cinemex'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="canonical" href="<?php echo $view['fronthelper']->get_permalink($cinema, TRUE); ?>" />
<link rel="alternate" href="cinemex://com.cinemex/cinema/<?php echo $cinema->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php
$view['slots']->set('modal_params', array('state_id'=>$cinema->getState()->getId(),'area_id'=>$cinema->getArea()->getId(),'cinema_id'=>$cinema->getId()));
$cinefan = array(
    17,  // Cinemex Mirador
    18,  // Delicias
    25,  // Colima
    51,  // Misterios
    191, // Encinas
    125, // Puerto Vallarta
    126, // San Gaspar
    142, // La Silla
    113, // Pachuca
    168, // Constituyentes
    180, // Plaza Culiacán
    194, // Navojoa,
    206, // Plaza Bella Bravo
    214, // Las Palmas
    208, // Boca del Rio
    215, // Los Pinos
);
?>

<?php $view['slots']->start( 'og_tags' ); ?>
<meta property="og:image" content="https:<?php echo $view['assets']->getUrl('assets/img/share-img.jpg'); ?>" />
<meta property="og:type"  content="place" />
<meta property="og:title" content="<?php echo $cinema->getName(); ?>" />
<meta property="og:description" content="<?php echo substr( strip_tags(htmlspecialchars($cinema->getData('DIRECCION'))), 0, 200 ); ?>" />
<meta property="place:location:latitude"  content="<?php echo $cinema->getLat(); ?>">
<meta property="place:location:longitude" content="<?php echo $cinema->getLng(); ?>">
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'body' ); ?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('cinemas_archive'); ?>">Cines</a></li>
  <li><a href="<?php echo $view['router']->generate('cinemas_archive', array('id' => $cinema->getState()->getId())); ?>"><?php echo $cinema->getState()->getName(); ?></a></li>
  <li><a href="<?php echo $view['fronthelper']->get_cinema_link( $cinema ); ?>"><?php echo $cinema->getName(); ?></a></li>
</ul>

<div class="clearfix row">

	<div id="cinema-details" class="main-col col" itemscope itemtype="http://schema.org/LocalBusiness">
		<h1 class="cinema-details-title" itemprop="name">
      <span class="icon icon-star icon-white no-txt"></span> <?php echo $cinema->getName(); ?>
    </h1>

    <?php
    $address = $cinema->getData('DIRECCION');
    $phone   = $cinema->getData('TELEFONOS');
    $colonia = $cinema->getData('COLONIA');
    ?>
    <?php if ( $address || $phone ) { ?>
		<div class="cinema-details-info content-box-bg">
			<div class="cinema-single-map-trigger">
				<a class="btn btn-light btn-icon icon icon-rarr" href="#">Ver mapa</a>
			</div>

      <?php if ( $address && is_string($address) ) { ?>
      <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="icon icon-dark icon-small icon-marker">
        <strong>Dirección:</strong> <span itemprop="streetAddress"><?php echo $address; ?></span>
      </p>
      <?php } ?>
      <?php if ( $colonia && is_string($colonia) ) { ?>
        <p class="icon icon-dark icon-small icon-marker">
          <strong>Colonia:</strong> <span itemprop="colonia"><?php echo $colonia; ?></span>
        </p>
      <?php } ?>
      <?php if ( $phone && is_string($phone) ) { ?>
      <p class="icon icon-dark icon-small icon-phone">
        <strong>Teléfono:</strong> <span itemprop="telephone"><?php echo $phone; ?></span>
      </p>
      <?php } ?>

		</div>
    <?php } ?>

    <div id="gmap" class="cinema-single-map cinema-single-map-target" data-zoom="14"></div>

    <?php if (sizeof($movies)) { ?>
		<ul class="file-tabs tabs-links">
      <li><span class="tab selected">En cartelera</span></li>
    </ul>
		<div id="billboard-movies">
			<?php
      $movies_per_page = 0;
      $movies_cols     = 4;
      echo $view->render(
          'SocialSnackFrontBundle:Partials:bodyMovies.html.php',
          array(
              'movies'          => $movies,
//              '_movies'         => $_movies,
              'movies_per_page' => $movies_per_page,
              'movies_cols'     => $movies_cols,
              'link_to_single'  => FALSE,
          )
      );
      ?>
		</div><!-- #billboard-movies -->
    <?php } ?>

	</div><!-- #cinema-details -->

	<div class="side-col col">
		<div class="dynamic-container require-location" data-route="partials/sidebarCinema/<?php echo $cinema->getId(); ?>/single-1/{args}" data-source="fn get_cinemasidebar_args" data-listeners="change_location" data-loadcb="mycinema_sidebar_loaded">
		</div>

    <?php if (!empty($closer_cinemas)) : ?>
    <div id="sidebar-closer-cinemas">
      <div class="sidebar-heading-alt">
        <span class="icon icon-marker icon-white pad-left sidebar-title-alt">Cines Cercanos</span>
      </div>

      <div class="mycinema-list">
        <?php foreach ($closer_cinemas as $cd) : ?>
          <?php $closer_cinema = $cd->getToCinema(); ?>
          <div class="mycinema-li">
            <a class="mycinema-item-title" href="<?php echo $view['fronthelper']->get_cinema_link($closer_cinema) ?>"><?php echo $closer_cinema->getName(); ?></a>
            <?php if (is_string($closer_cinema->getData('DIRECCION'))) { ?>
            <p class="mycinema-item-address"><?php echo $closer_cinema->getData('DIRECCION'); ?></p>
            <?php } ?>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>

	</div>

</div>

<script>
	var places = [{id : <?php echo $cinema->getId(); ?>, lat : <?php echo $cinema->getLat(); ?>, lng : <?php echo $cinema->getLng(); ?>}];
  var marker_img = '<?php echo $view['assets']->getUrl('assets/img/gmaps-marker.png'); ?>';
</script>
<?php $view['slots']->stop(); // body ?>
<?php $view['slots']->start( 'js_after_vendor' ); ?>
<script>
$( document ).bind( 'user_init', function() {
  var cinema = get_cinema_by_id(<?php echo $cinema->getId(); ?>);
  set_cinema(cinema);
} );
var lego_shown = false;
</script>

<?php
$image           = [];
/*$special_cinemas = [26, 27, 28, 31, 32, 34, 40, 48, 156, 157, 158];
$new_xtreme      = [236, 237, 249,
  196, // Cinemex Altabrisa
];*/
/*$june_cinemas    = [
  1017, 1007, 1276, 1232, 1172, 1032, 1207, 1227, 1033, 1027, 1171, 1044, 1199,
  1025, 1030, 1177, 1024, 1040, 1028, 1208, 1002, 1042, 1067, 1055,
  1056, 1035, 1001, 1019, 1003, 1013, 1160, 1029, 1200, 1037, 1168, 1154, 1018,
  1206, 1016, 1026, 1005
];*/
/*
// Cinemex Sao Paulo
if ($cinema->getId() === 120) :
  $image[] = $view['assets']->getUrl('assets/img/layers/SaoPaulo.jpg');

// Cinemex Pabellón Polanco
elseif ($cinema->getId() === 80) :
  $image[] = $view['assets']->getUrl('assets/img/layers/DISPLAY-PRECIO_PREFERENCIAL-ALTA.jpg');

// Cinemex Paseo S. Pedro Platino
elseif ($cinema->getId() === 150) :
  $image[] = $view['assets']->getUrl('assets/img/layers/MAILING-PASEO-SAN-PEDRO.jpg');

// Cinemex Mundo Divertido Mexicali
elseif ($cinema->getId() === 263) :
  $image[] = $view['assets']->getUrl('assets/img/layers/MDMexicali.jpg');

// Cinemex Boulevares Querétaro
elseif ($cinema->getId() === 255) :
  $image[] = $view['assets']->getUrl('assets/img/layers/Boulevares_Premium.jpg');
  $image[] = $view['assets']->getUrl('assets/img/layers/Boulevares_Xtremo.jpg');

// Cinemex Puebla
elseif ($cinema->getId() === 165) :
  $image[] = $view['assets']->getUrl('assets/img/layers/MAIL_PUEBLA.jpg');
  $image[] = $view['assets']->getUrl('assets/img/layers/POPPuebla.jpg');

// Cinemex Cancún
elseif ($cinema->getId() === 170) :
  $image[] = $view['assets']->getUrl('assets/img/layers/POPcancun.jpg');

// Cinemex Forum Coatzacoalcos
elseif ($cinema->getId() === 211) :
  $image[] = $view['assets']->getUrl('assets/img/layers/POPcoatzacoalcos.jpg');

// Cinemex San Antonio
elseif ($cinema->getId() === 36) :
  $image[] = $view['assets']->getUrl('assets/img/layers/MAIL_SAN_ANTONIO.jpg');
  $image[] = $view['assets']->getUrl('promos/POP_Entrada_JUNIO.png', 'CMS');

// Cinemex Macroplaza Tijuana
elseif ($cinema->getId() === 4) :
  $image[] = $view['assets']->getUrl('assets/img/layers/600X800TIJUANA.jpg');

// Cinemex Fiesta Anáhuac
elseif ($cinema->getId() === 146) :
  $image[] = $view['assets']->getUrl('assets/img/layers/Fiesta_Anahuac_Xtremo.jpg');

// Nuevos Cinemex xTremo
elseif (in_array($cinema->getId(), $new_xtreme)) :
  $image[] = $view['assets']->getUrl('assets/img/layers/CX-POP-UP_600.png');

// Cinemas in Oaxaca state
elseif ($cinema->getState()->getId() === 20) :
  $image[] = $view['assets']->getUrl('assets/img/layers/macroplaza-oaxaca-apertura.jpg');

// Cinemas in Monterrey
elseif ($cinema->getState()->getId() === 16) :
  $image[] = $view['assets']->getUrl('assets/img/layers/Aviso_MTY.png');

// Cinemas in Puebla city
elseif ($cinema->getArea()->getId() === 56) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-puebla.jpg');

// Cinemas in Querétaro city
elseif ($cinema->getArea()->getId() === 57) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-queretaro.jpg');

// Cinemas in Cuenavaca city
elseif ($cinema->getArea()->getId() === 51) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-cuernavaca.jpg');

// Cinemex Cortijo
elseif ($cinema->getId() === 68) :
  $image[] = $view['assets']->getUrl('assets/img/layers/CORTIJO_banner.jpg');

// Cinemex Ixtapaluca
elseif ($cinema->getId() === 69) :
  $image[] = $view['assets']->getUrl('assets/img/layers/IXTAPALUCA_BANNER.jpg');

// Cinemex La via
elseif ($cinema->getId() === 50) :
  $image[] = $view['assets']->getUrl('assets/img/mail_LA_VIA.jpg');

// Cinemex Coapa Platino
elseif ($cinema->getId() === 88) :
  $image[] = $view['assets']->getUrl('assets/img/mail_COAPA_PLATINO.jpg');

// Cinemex Cervantes Saavedra
elseif ($cinema->getId() === 224) :
  $image[] = $view['assets']->getUrl('assets/img/CervantesSaavedra.jpg');

// Cinemex Plaza Real Reynosa
elseif ($cinema->getId() === 204) :
  $image[] = $view['assets']->getUrl('assets/img/layers/reynosa-ie-precios.jpg');
  $image[] = $view['assets']->getUrl('assets/img/layers/reynosa-ie-premium.jpg');

// Cinemex Macro Plaza Tijuana
elseif ($cinema->getId() === 4) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-macro-plaza.jpg');

// Cinemex Revolucion
elseif ($cinema->getId() === 144) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-cmx-revolucion.jpg');

// Cinemex La Silla
elseif ($cinema->getId() === 142) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-cmx-la-silla.jpg');

// Cinemex La Joya
elseif ($cinema->getId() === 49) :
  $image[] = $view['assets']->getUrl('assets/img/flyer-cmx-la-joya.jpg');

// Cinemex Zapopan. Legacy ID 1197
elseif ($cinema->getId() === 122) :
  $image[] = $view['assets']->getUrl('assets/img/zapopan-4d.jpg');

// Cinemex Universidad. Legacy ID 1025
elseif ($cinema->getId() === 95) :
  $image[] = $view['assets']->getUrl('assets/img/universidad-4D.jpg');

elseif ($cinema->getId() === 225) :
  $image[] = $view['assets']->getUrl('assets/img/layers/POPUP_CNA.png');

elseif ($cinema->getId() === 247) :
  $image[] = $view['assets']->getUrl('promos/CNA_WEB_LOMAS_V.jpg', 'CMS');

// Cinemex Parque Lindavista
elseif ($cinema->getId() === 249) :
  $image[] = $view['assets']->getUrl('assets/img/layers/LINDA_mailing.jpg');

// Cinemex Parque Altavista
elseif ($cinema->getId() === 86) :
  $image[] = $view['assets']->getUrl('assets/img/layers/Altavista_MAIL.jpg');
  $image[] = $view['assets']->getUrl('promos/POP_Entrada_JUNIO.png', 'CMS');

// Cines con Precios Especiales
elseif (in_array($cinema->getId(), $special_cinemas)) :
  $image[] = $view['assets']->getUrl(sprintf('assets/img/layers/precios-especiales/%s.jpg', $cinema->getId()));

// Cinefan
elseif (in_array($cinema->getId(), $cinefan)) :
  $image[] = $view['assets']->getUrl('promos/cinefan640x800.jpg', 'CMS');
*/

/*$fifty_shades = [
    1117,1073,1212,1186,1126,1142,1140,1256,1204,1138,1080,1069,1134,1051,1011,1110,1132,1050,1262,1242
];


// Promo de junio
if (in_array($cinema->getLegacyId(), $june_cinemas)):
  $image[] = $view['assets']->getUrl('promos/POP_Entrada_JUNIO.png', 'CMS');

if (in_array($cinema->getLegacyId(),$fifty_shades)):
  $image[] = $view['fronthelper']->get_img_size('modals/4c9b7b9604e9165fc7ede752f0eed3c3-500x590.jpg', '', 'CMS');
endif;
*/

/*$tuesday_images  = [];
$tuesday_cinemas = [
  1149, 1232, 1233, 1234, 1235, 1236, 1242, 1243, 1247, 1248, 1246, 1245, 1249,
  1254, 1253, 1252, 1256, 1251, 1255, 1267, 1266, 1261, 1263, 1262, 1260, 1265,
  1272, 1273, 1274, 1268, 1275, 1269, 1270, 1271
];
*/
/*if (in_array($cinema->getLegacyId(), $tuesday_cinemas)) :
  $tuesday_images[] = $view['assets']->getUrl('assets/img/layers/POP-UP-Martes-IEC.png');
endif;*/
?>

<script>
jQuery(document).ready(function($){
  /*var tuesday_images = <?php //echo json_encode($tuesday_images); ?>
  var images         = <?php //echo json_encode($image); ?>
      //today          = (new Date()).getDay();

  if (tuesday_images.length > 0 && (today === 1 || today === 2)) {
    images = images.concat(tuesday_images);
  }

  show_lightbox(images);*/
});
</script>
<?php $view['slots']->stop(); // js_after_vendor ?>


<?php $view['slots']->start('esi_featured_promos'); ?>
<?php
echo $view['actions']->render(
    $view['router']->generate('esi_important_promos', array('target' => 'cinemas')),
    array('strategy' => 'esi')
);
?>
<?php $view['slots']->stop(); ?>
