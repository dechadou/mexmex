<?php

namespace SocialSnack\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use SocialSnack\FrontBundle\Entity\Splashscreen;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class SplashscreenController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_splashscreen_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    $handler = $this->get('rest.splashscreen.handler');
    $splash  = $handler->getAll();
    $result  = array();

    foreach($splash as $sp) {
      $result[] = $handler->serializeOne($sp);
    };

    if ($this->get('rest.context.handler')->getApiVersion() >= 2) {
      $result = ['screens' => $result];
    }

    $response = new JsonResponse($result, 200);
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(45 * 60);

    return $response;
	}
		
	
	/**
	 * Return detailed info for splashscreen(s).
	 * 
	 * @Route("/{id}", name="rest_splashscreen_single", requirements={"id"="[0-9]+"})
	 * @Method({"GET"})
	 */
	public function detailsAction( $id ) {
    $handler = $this->get('rest.splashscreen.handler');
    $splash  = $handler->get($id);

		if (!$splash) {
      return new JsonResponse(array('error' => 'Invalid ID'), 404);
    }

    $result = $result[] = $handler->serializeOne($splash);

		$response = new JsonResponse($result, 200);
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(60 * 60);
    
    return $response;
	}

}