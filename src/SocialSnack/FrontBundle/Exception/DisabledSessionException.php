<?php

namespace SocialSnack\FrontBundle\Exception;

class DisabledSessionException extends \Symfony\Component\HttpKernel\Exception\HttpException {
  
  /**
   * @var \SocialSnack\WsBundle\Entity\Session
   */
  protected $session;
  

  /**
   * Constructor.
   *
   * @param string     $message  The internal exception message
   * @param \Exception $previous The previous exception
   * @param integer    $code     The internal exception code
   */
  public function __construct(\SocialSnack\WsBundle\Entity\Session $session, $message = null, \Exception $previous = null, $code = 0) {
    parent::__construct(404, $message, $previous, array(), $code);
    $this->setSession($session);
  }
    
  /**
   * 
   * @param \SocialSnack\WsBundle\Entity\Session $session
   */
  public function setSession(\SocialSnack\WsBundle\Entity\Session $session) {
    $this->session = $session;
  }
  
  /**
   * 
   * @return \SocialSnack\WsBundle\Entity\Session
   */
  public function getSession() {
    return $this->session;
  }
  
}