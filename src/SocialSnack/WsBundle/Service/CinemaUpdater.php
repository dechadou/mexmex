<?php
namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;


class CinemaUpdater extends WorkerAbstract {
  
  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
	protected $container;
	
	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;
	
  /**
   * @var \SocialSnack\WsBundle\Request\Request
   */
	protected $cinemex_ws;
	
  /**
   * @var array
   */
  protected $new_cinemas = [];
  
  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
   * @param \SocialSnack\WsBundle\Request\Request $cinemex_ws
   */
	public function __construct($container, $doctrine, $cinemex_ws) {
		$this->container  = $container;
		$this->doctrine   = $doctrine;
		$this->cinemex_ws = $cinemex_ws;
	}
	
  
  public function execute($data) {
    try {
      
			$req          = $this->cinemex_ws;
			$doctrine     = $this->doctrine;
			$em           = $doctrine->getManager();
      $cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
      $area_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:StateArea' );
      $area_id      = isset($data['area_id'])  ? $data['area_id']  : NULL;
      $state_id     = isset($data['state_id']) ? $data['state_id'] : NULL;
      $add_only     = isset($data['add_only']) ? $data['add_only'] : TRUE;

      if ( $area_id ) {
        $areas = $area_repo->findBy(array('id' => $area_id));
      } elseif ( $state_id ) {
        $areas = $area_repo->findBy(array('state' => $state_id));
      } else {
        $areas = $area_repo->findAll();
      }

      /* @var $area \SocialSnack\WsBundle\Entity\StateArea */
      foreach ( $areas as $area ) {
        $state = $area->getState();
        $this->consoleInfo(sprintf( '%s, %s', $state->getName(), $area->getName() ));

        try {
          $req->init( 'ConsultaDetallada' );
          $res = $req->request( array(
              'TipoConsulta' => \SocialSnack\WsBundle\Request\ConsultaDetallada::TIPOCONSULTA_CINES,
              'CodigoEstado' => $area->getLegacyStateId(),
              'CodigoZona'   => $area->getLegacyId(),
          ) );
        } catch ( \SocialSnack\WsBundle\Exception\RequestMsgException $e ) {
          $this->setError($e->getMessage());
          $this->consoleInfo($e->getMessage());
          continue;
        }

        foreach ( $res as $_cinema ) {
          if ( !( $cinema = $cinema_repo->findOneBy( array( 'legacy_id' => $_cinema->CODCINE ) ) ) ) {
            $cinema = new Cinema();
            $em->persist( $cinema );
            $this->new_cinemas[] = $_cinema->CODCINE->__toString();
          } else if ( $add_only ) {
            continue;
          }
          $cinema->setLegacyId( $_cinema->CODCINE );
          $cinema->setLegacyName( $_cinema->CINENAME );
          $cinema->setInfo( json_encode( $_cinema ) );
          $cinema->setPlatinum( $_cinema->COMPLEJO_INTPLATINO );
          $cinema->setState( $state );
          $cinema->setArea( $area );
          $cinema->setLat( (float)WsHelper::coord_to_dec( $_cinema->LATITUD ) );
          $cinema->setLng( (float)WsHelper::coord_to_dec( $_cinema->LONGITUD ) );
          $cinema->setActive( FALSE );
          if ( !$cinema->getId() ) { // No ID = new record = INSERT.
            $cinema->setName( ucwords( strtolower( $_cinema->CINENAME ) ) );
          }

          /* @todo Notify updates. */
        }

        $em->flush();
      }

      $this->consoleInfo(sprintf('%d new cinema(s).', count($this->new_cinemas)));
      $this->consoleInfo('Done.');
    
    } catch ( \Exception $e ) {
			$this->setError($e->getMessage());
      $this->consoleInfo($e->getMessage());
			return FALSE;
		}
		
		return TRUE;
  }
  
  
  public function updateCloser($args = []) {
    /** @var \SocialSnack\WsBundle\Service\CinemaDistanceUpdater $service */
		$service = $this->container->get('cinemex.cinema_distance_updater');
    $success = $service->execute($args);
    $this->consoleInfo($service->getConsoleLog());

    return $success;
  }
  
  
  public function getNewCinemas() {
    return count($this->new_cinemas);
  }


  public function getNewCinemasIds() {
    return $this->new_cinemas;
  }
  
}