<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', new UserType());
        $builder->add(
            'terms',
            'checkbox',
            array('property_path' => 'termsAccepted', 'label' => 'Acepto los términos y condiciones')
        );
    }

    public function getName()
    {
        return 'socialsnack_user_registration';
    }
}