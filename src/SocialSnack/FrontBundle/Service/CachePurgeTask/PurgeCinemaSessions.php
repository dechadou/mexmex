<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\WsBundle\Entity\Cinema;

class PurgeCinemaSessions implements DoctrineTaskInterface {

  protected $doctrine;

  protected $cinema;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  public function setCinema(Cinema $cinema) {
    $this->cinema = $cinema;
  }


  public function getDoctrineResources() {
    $ids = [];

    foreach ($this->getActiveSessionsIds() as $session) {
      $ids[] = 'SocialSnack\WsBundle\Entity\SessionRepository::findWithMovieAndCinema:' . $session['id'];
      $ids[] = 'SocialSnack\WsBundle\Entity\SessionRepository::findWithCinema:' . $session['id'];
    }

    return [
        [
            'ids' => $ids,
            'env' => 'front'
        ]
    ];
  }


  protected function getActiveSessionsIds() {
    if (!$this->cinema) {
      throw new \Exception('Cinema not defined.');
    }

    $qb = $this->doctrine
        ->getRepository('SocialSnackWsBundle:Session')
        ->createQueryBuilder('q');
    $qb
        ->select('q.id')
        ->andWhere('q.cinema = :cinema')
        ->andWhere('q.active = 1')
        ->setParameter('cinema', $this->cinema)
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }

}