<?php

namespace SocialSnack\WsBundle\Tests\Request;

/**
 * Class WebTestCase
 * @package SocialSnack\WsBundle\Tests\Request
 * @author Guido Kritz
 */
abstract class WebTestCase extends \Liip\FunctionalTestBundle\Test\WebTestCase {

  static $container;
  static $ws;
  static $browser_mock;

  public function setUp() {
    // Start the Symfony kernel.
    $kernel = static::createKernel();
    $kernel->boot();

    // Get the DI container.
    self::$container = $kernel->getContainer();

    // Mock the service "gremo_buzz".
    self::$browser_mock = $this->getServiceMockBuilder('gremo_buzz')->getMock();
    self::$container->set('gremo_buzz', self::$browser_mock);

    // Initialize the WS class.
    self::$ws = self::$container->get('cinemex_ws');
    self::$ws->init($this->get_ws_method());
  }


  abstract protected function get_ws_method();


  protected function setWsResponseContent($content) {
    $res = new \Buzz\Message\Response();
    $res->setContent($content);
    $res->setHeaders(array('OK 200'));

    self::$browser_mock
        ->method('post')
        ->with($this->getWsUrl($this->get_ws_method()))
        ->willReturn($res)
    ;
  }

  protected function getWsUrl($method) {
    return 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/' . $method;
  }

} 