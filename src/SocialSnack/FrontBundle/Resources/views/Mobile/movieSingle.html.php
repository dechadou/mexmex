<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
  <link rel="alternate" href="cinemex://com.cinemex/movie/<?php echo $movie->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
<?php
echo $this->render('SocialSnackFrontBundle:Mobile:partials/cinemaHeader.html.php', array(
    'cinema' => $cinema,
));
?>

<?php if ( $youtube_id = FrontHelper::get_youtube_id_from_url( $movie->getData( 'TRAILER' ) ) ) { ?>
<iframe width="100%" height="240" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
<?php } ?>

<?php if ($date && $available_dates) { ?>
<div class="sidebar-filters">
  <select name="date" class="sidebar-filter" id="movie-date">
    <?php
    echo $this->render( 'SocialSnackFrontBundle:Mobile:partials/dateSelector.html.php', array(
        'date'            => $date,
        'available_dates' => $available_dates,
    ) );
    ?>
  </select>
</div>
<?php } ?>

<?php foreach ( $sessions as $_movie ) { ?>
<div class="mycinema-li box2">
  <?php echo $this->render('SocialSnackFrontBundle:Mobile:partials/movieSessions.html.php', array(
      'items'    => $_movie['sessions'],
      'platinum' => $cinema->getPlatinum(),
  )); ?>
</div>
<?php } ?>

<div class="movie-details-main box3">
  <h1 class="movie-details-title content-box-title"><?php echo $movie->getName(); ?></h1>
  <?php
  $movie_info = array(
      array(
          'label' => 'Título original',
          'key'   => 'NOMBREORIGINAL',
      ),
      array(
          'label' => 'Director',
          'key'   => 'DIRECTOR',
      ),
      array(
          'label' => 'País',
          'key'   => 'PAISORIGEN',
      ),
      array(
          'label' => 'Año',
          'key'   => 'ANIO',
      ),
      array(
          'label' => 'Calificación',
          'key'   => 'RATING',
      ),
      array(
          'label'  => 'Duración',
          'key'    => 'DURACION',
          'format' => '%s minutos',
      ),
      array(
          'label' => 'Género',
          'key'   => 'GENERO',
      ),
      array(
          'label' => 'Sitio oficial',
          'key'   => 'WEBSITE',
      ),
  );
  if ( is_string( $movie->getData( 'ACTORES' ) ) ) {
    array_splice( $movie_info, 1, 0, array( array(
          'label' => 'Actores',
          'value' => implode( ', ', explode( ',' , $movie->getData( 'ACTORES' ) ) ),
    ) ) );
  }
  ?>
  <dl class="movie-details-info">
    <?php foreach ( $movie_info as $item ) { ?>
    <?php if ( ( ( isset( $item['value'] ) && ( $value = $item['value'] ) ) || ( $value = $movie->getData( $item['key'] ) ) ) && is_string( $value ) ) { ?>
    <dt><?php echo $item['label']; ?></dt>
    <dd><?php echo isset( $item['format'] ) ? sprintf( $item['format'], $value ) : $value; ?></dd>
    <?php } ?>
    <?php } ?>
  </dl>

  <h2 class="content-box-subtitle">Sinopsis</h2>
  <div class="content-text">
    <p><?php echo is_string( $movie->getData( 'SINOPSIS' ) ) ? $movie->getData( 'SINOPSIS' ) : ''; ?></p>
  </div>
</div>

<?php if ( $closer_cinemas ) { ?>
<div class="box2">
  <div class="pad1 boxsec">
    Ver esta película en cines cercanos
  </div>
  <ul class="generic-list alt-list">
    <?php foreach ( $closer_cinemas as $_cinema ) { ?>
    <li>
      <a href="<?php echo $view['router']->generate('mobile_movie', array('movie_id' => $movie->getId(), 'cinema_id' => $_cinema->getId())); ?>" class="pad1"><?php echo $_cinema->getName(); ?></a>
    </li>
    <?php } ?>
  </ul>
</div>
<?php } ?>
  
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('js_after'); ?>
<script>
jQuery( document ).ready( function( $ ) {
} );
</script>
<?php $view['slots']->stop(); ?>