<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Invitado Especial - Nuestro Programa - Cinemex');
$view['slots']->start('body')
?>

<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/header-ie.jpg'); ?>" alt="Invitado Especial" />
</div>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render('SocialSnackFrontBundle:Ie:menu.html.php', array('route' => $route)); ?>
  </div>

  <div class="wide-main-col col">
    <div class="ie-content ie-program content-box">
      <?php $view['slots']->output('iebody'); ?>
    </div>
  </div><!-- .wide-main-col -->
</div><!-- .row -->

<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after'); ?>
<?php $view['slots']->stop(); ?>
