<?php

namespace SocialSnack\FrontBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class SetMaintenanceCommand extends ContainerAwareCommand {

  public function configure() {
    $this
        ->setName('maintenance:mode')
        ->addOption('enable', 'E', InputOption::VALUE_OPTIONAL, 'Whether enable or disable the maintenance mode.', 1)
    ;
  }

  public function execute(InputInterface $input, OutputInterface $output) {
    $enable = $input->getOption('enable');
    $fs = new Filesystem();

    if ($enable) {
      $fs->rename('web/.htaccess', 'web/bak.htaccess');
      $fs->rename('web/maintenance.htaccess', 'web/.htaccess');
    } else {
      $fs->rename('web/.htaccess', 'web/maintenance.htaccess');
      $fs->rename('web/bak.htaccess', 'web/.htaccess');
    }
  }

}