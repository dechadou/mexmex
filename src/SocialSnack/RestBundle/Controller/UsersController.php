<?php
namespace SocialSnack\RestBundle\Controller;

use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use SocialSnack\RestBundle\Service\Helper as RestHelper;

class UsersController extends BaseController {
  
  
  protected function _getUser($user_id) {
    $repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:User');
    $user = $repo->find($user_id);
    
    if (!$user) {
      throw new RestException('user-not-found', 'User not found', 404);
    }
    
    return $user;
  }
  
  /**
   * List all the users.
   * 
   * @Route("/", name="rest_users_list")
   */
  public function defaultAction() {
    $this->validate_client_cap('list_users');
    
    $url    = $this->generateUrl('rest_users_list', array(), TRUE);
    $query  = $this->getRequest()->query;
    $res    = array();
    $since  = $query->get('since');
    $before = $query->get('before');
    $count  = $query->get('count');
    $repo   = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:User');
    $qb     = $repo->createQueryBuilder('u');
    
    // Set default/max number of results per page.
    if (!$count || $count > 50) {
      $count = 50;
    }
    
    $qb->select('u')
        ->setMaxResults($count)
        ->orderBy('u.id', 'DESC');
    
    // Set pagination offset.
    if ($since) {
      $qb->andWhere('u.id > :since_id')
          ->setParameter('since_id', $since);
    } elseif ($before) {
      $qb->andWhere('u.id < :before_id')
          ->setParameter('before_id', $before);
    }
    
    $q = $qb->getQuery();
    $users = $q->getResult();
    
    foreach ( $users as $user ) {
      $res[] = $user->toArray();
    }
    $data = array('users' => $res);
    
    // Build pagination links.
    if (sizeof($res)) {
      $qb = $repo->createQueryBuilder('u');
      $qb->select('MAX(u.id),MIN(u.id)');
      $query_ids = $qb->getQuery();
      $ids = $query_ids->getArrayResult();
      $ids = $ids[0];
      $pagination = array();
      $first = $res[0];
      $last  = end($res);
      $query->remove('since');
      $query->remove('before');
      if ( $ids[1] > $first['id'] ) {
        $query_args = $query->all();
        $query_args['since'] = $first['id'];
        $pagination['next'] = $url . '?' . http_build_query($query_args);
      }
      if ( $ids[2] < $last['id'] ) {
        $query_args = $query->all();
        $query_args['before'] = $last['id'];
        $pagination['prev'] = $url . '?' . http_build_query($query_args);
      }
      $data['pagination'] = $pagination;
    }
    
    return new JsonResponse($data);
  }
  
  
  /**
   * @Route("/{user_id}")
   * 
   * @param int $user_id
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function userAction($user_id) {
    $this->validate_client_cap('users_info');
    $user    = $this->_getUser($user_id);
    $handler = $this->get('rest.user.handler');
    return new JsonResponse($handler->serializeOne($user));
  }
  
  
  /**
   * @Route("/{user_id}/cinemas")
   * 
   * @param int $user_id
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function cinemasAction($user_id) {
    $this->validate_client_cap('users_info');
    $user = $this->_getUser($user_id);
    $cinemas = $this->get('rest.user.handler')->serializeUserFavCinemas($user);
    return new JsonResponse($cinemas);
  }
  
  
  /**
   * @Route("/{user_id}/purchaseHistory")
   * 
   * @param int $user_id
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function purchaseHistoryAction($user_id) {
    $this->validate_client_cap('users_history');
    $user = $this->_getUser($user_id);

    $output = $this->get('rest.user.handler')->getTransactionHistory($user->getId());

    /** @todo Update output format to match UserController::purchaseHistoryAction response */
    /** @todo Add pagination */
    return new JsonResponse( $output );
  }
  
}