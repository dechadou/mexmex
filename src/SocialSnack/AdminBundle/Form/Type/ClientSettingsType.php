<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientSettingsType extends AbstractType {

  public function getName() {
    return '';
  }

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('domain', 'text', ['label' => 'Dominio', 'required' => FALSE])
        ->add('contact', 'email', ['label' => 'Email de contacto', 'required' => FALSE])
        ->add('validate_referer', 'checkbox', ['label' => 'Validar referer', 'required' => FALSE])
        ->add('captcha', 'checkbox', ['label' => 'Captcha', 'required' => FALSE])
        ->add('appPass', 'text', ['label' => 'Pass doc', 'required' => FALSE])
    ;
  }

}