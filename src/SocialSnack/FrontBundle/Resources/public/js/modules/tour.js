var $cm_bt_shown = false;
var $cm_bt_cookie;

function billboard_tip() {
  return; // HACK! this tooltip is not being used now.

  // prevent double open
  if ($cm_bt_shown == true) {
    return;
  }

  $cm_bt_shown = true;

  $href = $('#header-area-select option[value=' + $('#header-area-select').val() + ']').data('url');

  if (location.pathname.indexOf(url_prefix + 'cartelera/') === 0) {
    return;
  }

  // display count
  var $cookie = parseInt($.cookie('cinemex_billboard_tip'));

  if (isNaN($cookie)) {
    $cookie = 0;
  }

  if ($cookie >= 1) {
    return;
  }

  $cm_bt_cookie = $cookie + 1;
  $next = "\u00a1Pru\u00e9balo!";
  $skip = true;

  $.tour({
    steps : [
      {
        title  : 'Cartelera por zonas',
        text   : '¡Prueba nuestro nuevo buscador avanzado de cartelera y encuentra exactamente las películas que deseas en 2 simples pasos!',
        dir    : 'top',
        step   : false,
        ref    : '#header-area',
        skip   : $skip,
        next   : $next,
        href   : $href,
        offset : {
          top  : 0,
          left : 0
        },
        precb  : function() {
          $.cookie('cinemex_billboard_tip', $cm_bt_cookie, {
            path: url_prefix
          });

          $('#overflow-ctl').css({ top : 0 });
          $('#site-top').hide();
          $('#site-header').css({
            position : 'absolute',
            zIndex : 'auto'
          });
        },
        cb     : function(step, nstep, nextfn) {
          $cm_bt_shown = false;

          $('#overflow-ctl').css({ top : 'auto' });
          $('#site-top').show();
          $('#site-header').css({
            position : 'fixed',
            zIndex : 500
          });
          nextfn();
        }
      }
    ]
  });
}


function start_tour( with_location ) {
  return; // Hack! the tour is not being used now.

  if ( !isset('is_home', window) || !window['is_home'] )
    return false;

  setCookie( 'cinemex_tour_shown', 1, null );

  var tour_steps = [];
  tour_steps.push( {
      title  : '¡Bienvenidos!',
      text   : 'Bienvenido al nuevo sitio de Cinemex. Te presentamos un pequeño tour de 2 minutos que te permitirá conocer y familiarizarte con las nuevas funcionalidades que hemos preparado para ti. Puedes saltar este paso dando click en "saltar tour".',
      next   : 'Comenzar',
      dir    : 'left',
      step   : false,
      ref    : '#site-logo a',
      offset : {
        top  : 10,
        left : 0
      },
      cb     : show_tooltip_side
  } );

  if ( with_location ) {
    tour_steps.push( {
        title  : 'Selecciona el cine de tu preferencia',
        text   : 'Aquí podrás acceder rápidamente a la cartelera de tu cine favorito. Encontrarás fácilmente las películas en exhibición y los horarios disponibles. ¡Haz click en ellos para comprar tus boletos!',
        dir    : 'right',
        step   : 1,
        ref    : '.side-col',
        offset : {
          top  : 0,
          left : 20
        },
        canvas : true,
        cb     : show_tooltip_side2
    } );
    tour_steps.push( {
        title  : 'Modifica tu cine de preferencia',
        text   : 'Si deseas seleccionar otro cine de preferencia, puedes hacer clic aquí y los resultados se actualizarán automáticamente. ',
        dir    : 'right',
        step   : 2,
        ref    : '.side-col .switch-cinema',
        offset : {
          top  : 0,
          left : 20
        },
        canvas : true,
        cb     : show_tooltip_main
    } );
  } else {
    tour_steps.push( {
        title  : 'Selecciona el cine de tu preferencia',
        text   : 'Así podrás acceder rápidamente a la cartelera de tu cine favorito. Encontrarás fácilmente las películas en exhibición y los horarios disponibles. ¡Haz click en ellos para comprar tus boletos!',
        dir    : 'right',
        step   : 1,
        ref    : '.side-col',
        offset : {
          top  : 20,
          left : 20
        },
        canvas : true,
        cb     : show_tooltip_main
    } );
    tour_steps.push( { } );
  }

  tour_steps.push( {
      title    : 'Películas en Exhibición',
      text     : '¿Buscas una película en particular? ¿No sabes qué ver? Aquí te presentamos la cartelera actualizada con todos los títulos disponibles para esta semana. Si deseas ver mayor información u horarios disponibles haz clic sobre el póster de la película.',
      dir      : 'left',
      step     : 3,
      ref      : '#billboard-movies',
      'class'  : 'side-col',
      offset   : {
        top    : 0,
        left   : -31
      },
      canvas   : {
        padding : 20
      },
      cb       : show_tooltip_search
  } );

  tour_steps.push( {
      title  : 'Buscador de Películas',
      text   : 'Aquí podrás encontrar la película que buscas muy fácilmente. Puedes obtener resultados ingresando parte del Título, Título Original, Actores o Directores.',
      dir    : 'right',
      step   : 4,
      ref    : '#top-search',
      offset : {
        top  : 0,
        left : 0
      },
      cb     : show_tooltip_reg
  } );

  tour_steps.push( {
      title  : 'Regístrate',
      text   : 'Te invitamos a darte de alta en nuestro nuevo sitio. ¡Puedes hacerlo rápido y de forma muy sencilla! Aquí podrás crear tu perfil para que nuestro sistema aprenda de ti en base a tus gustos y preferencias y así poderte ofrecer una cartelera mas específica, así como gozar de beneficios y promociones exclusivas y muchas sorpresas más.',
      dir    : 'right',
      step   : 5,
      ref    : '#header-user-login',
      skip   : false,
      offset : {
        top  : 0,
        left : 0
      },
      canvas : {
        padding : 20
      }
  } );

  $.tour({
    steps : tour_steps
  });


  function show_tooltip_side(step, nstep, nextfn) {
    var $ref = $( nstep.ref );
    $ref.addClass( 'tour-highlight' ).css( { position : 'relative' } );
    $.scroll_to( $ref, -40 );
    nextfn();
  }

  function show_tooltip_side2(step, nstep, nextfn) {
    var $ref = $( nstep.ref );
    $ref.addClass( 'tour-highlight' );
    $.scroll_to( $ref, -40 );
    nextfn();
  }

  function show_tooltip_main(step, nstep, nextfn) {
    var $ref = $( nstep.ref );
    $ref.addClass( 'tour-highlight' ).css( { position : 'relative' } );
    $.scroll_to( $ref, -40 );
    nextfn();
  }
  function show_tooltip_search(step, nstep, nextfn) {
    var $ref = $( nstep.ref );
    $ref.addClass( 'tour-highlight' );//.css( { position : 'relative' } );
    $.scroll_to( $ref, -40 );
    nextfn();
  }

  function show_tooltip_reg(step, nstep, nextfn) {
    var $ref = $( nstep.ref );
    $ref.addClass( 'tour-highlight' ).css( { position : 'relative' } );
    $.scroll_to( $ref, -40 );
    nextfn();
  }

}

( function() {

  var args;


  function end_tour() {
    $( '#tour-black' ).fadeOut( { complete : function() { $( this ).remove() } } );
    $( '#tour-block, #tour-canvas, .tour-tooltip' ).remove();
    $( '#overflow-ctl' ).removeClass( 'tour' );
    $( 'html, body' ).stop().animate( { scrollTop : 0 } );
  }

  function build_tour_tooltip( step ) {
    var $tooltip = $( '<div/>', { 'class' : 'tour-tooltip' } ),
        $title   = $( '<h3/>', { 'class' : 'tour-title', text : step.title } ),
        $text    = $( '<p/>', { text : step.text } ),
        $bottom  = $( '<div/>', { 'class' : 'tour-bottom' } ),
        $skip    = null,
        $next    = $( '<a/>', { 'class' : 'btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right', href : step.href ? step.href : '#', text : step.next ? step.next : 'Continuar' } ),
        $heading = $( '<div/>', { 'class' : 'tour-heading' } );
    ;

    $tooltip.append( $title );
    $tooltip.append( $text );
    if ( step.next !== false ) {
      $bottom.append( $next );
    }
    $tooltip.append( $bottom );
    $tooltip.hide().appendTo( 'body' );

    $tooltip.addClass( step.dir );
    if ( step['class'] ) {
      $tooltip.addClass( step['class'] );
    }

    if ( ( 'undefined' === typeof step.skip ) || ( false !== step.skip ) ) {
      $skip = $( '<a/>', { 'class' : 'tour-skip', href : '#', text : 'Saltar Tour' } );
    }

    if ( false !== step.step ) {
      $steps = $( '<ul/>', { 'class' : 'tour-steps' } );
      for ( var i = 1 ; i <= args.steps.length ; i++ ) {
        $( '<li/>', { 'class' : 'tour-step' + ( i == step.step ? ' selected' : '' ), text : i } ).appendTo( $steps );
      }
      $steps.delegate('li', 'click', function(e) {
        args.current_step = $(this).index();
        $next.trigger('click');
      });
      $heading.append( $steps );
      $tooltip.addClass( 'w-heading' );
      $tooltip.prepend( $heading );
      if ( $skip ) {
        $heading.append( $skip );
      }
    } else if ( $skip ) {
      $bottom.append( $skip );
    }

    if ( $skip ) {
      $skip.bind( 'click', function( e ) {
        e.preventDefault();
        end_tour();
      } );
    }

    var $ref = $( step.ref );

    if ( !$ref.length ) {
      end_tour();
      return;
    }

    if ( 'top' === step.dir ) {
      $tooltip.css( {
        left : $ref.offset().left + (($ref.outerWidth()-$tooltip.outerWidth())/2) + step.offset.left,
        top  : $ref.offset().top + $ref.outerHeight() + step.offset.top
      } );
    } else if ( 'left' === step.dir ) {
      $tooltip.css( {
        left : $ref.offset().left + $ref.outerWidth() + step.offset.left,
        top  : $ref.offset().top + step.offset.top
      } );
    } else {
      $tooltip.css( {
        left : $ref.offset().left - $tooltip.outerWidth( true ) + step.offset.left,
        top  : $ref.offset().top + step.offset.top
      } );
    }

    $tooltip.fadeIn();

    $next.bind( 'click', function( e ) {
      if ($(this).attr('href') == '#') {
        e.preventDefault();
      }

      $next.unbind( 'click' );
      $tooltip.remove();
      $ref.removeClass( 'tour-highlight' );

      var step   = args.steps[args.current_step],
          nstep  = args.steps[args.current_step + 1],
          nextfn = nstep ? next_step : end_tour;

      if ( 'function' === typeof step.cb ) {
        step.cb(step, nstep, nextfn);
      } else {
        nextfn();
      }
    } );

    $( '#tour-canvas' ).remove();
    if ( step.canvas ) {
      add_tour_canvas( $ref, step.canvas.padding );
    }
  }

  function add_tour_canvas( $ref, padding ) {
    var $canvas = $( '<div/>', { id : 'tour-canvas' } );
    var css     = {
        position   : 'absolute',
        top        : $ref.offset().top,
        left       : $ref.offset().left,
        width      : $ref.outerWidth(),
        height     : $ref.outerHeight(),
        background : 'white',
        zIndex     : 999991
    };
    if ( padding ) {
      css.width  += ( padding * 2 );
      css.height += ( padding * 2 );
      css.top    -= padding;
      css.left    -= padding;
    }
    $canvas.css( css );
    $canvas.appendTo( 'body' );
  }


  $.tour = function(_args) {
    args = _args;
    args.current_step = -1;

    setup_dom();
    next_step();
  };


  function next_step() {
    args.current_step++;

    var step = args.steps[args.current_step],
        $ref = $( step.ref );
    $ref.addClass( 'tour-highlight' );

    if ( 'function' === typeof step.precb ) {
      step.precb();
    }

    build_tour_tooltip( step );
  }


  function setup_dom() {
    if ( !$('#tour-black').length ) {
      $( '<div/>', { id : 'tour-black' } ).appendTo( 'body' ).animate( { opacity : 1 } );
    }
    if ( !$('#tour-block').length ) {
      $( '<div/>', { id : 'tour-block' } ).appendTo( 'body' );
    }
    $( '#overflow-ctl' ).addClass( 'tour' );
    $( 'html, body' ).scrollTop( 0 );
  }

} )();
