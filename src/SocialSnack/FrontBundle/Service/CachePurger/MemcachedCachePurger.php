<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\MemcachedTaskInterface;

class MemcachedCachePurger implements CachePurgerInterface {

  protected $memcached;

  public function __construct(\Memcached $memcached) {
    $this->memcached = $memcached;
  }


  public function canHandle(CachePurgeTaskInterface $task) {
    return $task instanceof MemcachedTaskInterface;
  }


  public function handle(MemcachedTaskInterface $task) {
    $output = [];

    foreach ($task->getMemcachedResources() as $id) {
      $success = $this->memcached->delete($id);
      $output[] = sprintf(
          '%s: %s',
          $id,
          $success ? 'success' : 'failure'
      );
    }

    return $output;
  }

}