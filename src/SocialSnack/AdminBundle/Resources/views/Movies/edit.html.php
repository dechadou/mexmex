<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Editar película ' . $movie->getName().' - Cinemex' );
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Editar <strong><?php echo $movie->getName(); ?></strong>
          <a href="<?php echo $view['fronthelper']->get_permalink($movie); ?>">(ver en front)</a>

          <?php if ('grouper' === $movie->getType()) { ?>
            <a href="<?php echo $view['router']->generate('admin_movies_dissolveGroup', ['group_id' => $movie->getGroupId()]); ?>" class="btn btn-default" onclick="return confirm('Esta acción no se puede deshacer. TODAS las películas del grupo se desagruparán. Deseas continuar?);">Disolver grupo</a>
          <?php } elseif (!is_null($movie->getParent())) { ?>
            <a href="<?php echo $view['router']->generate('admin_movie_ungroupMovie', ['movie_id' => $movie->getId()]); ?>" class="btn btn-default" onclick="return confirm('Esta acción no se puede deshacer. Deseas continuar?);">Desagrupar</a>
          <?php } else { ?>
            <a href="<?php echo $view['router']->generate('admin_movies_addToGroupAsk', ['movie_id' => $movie->getId()]); ?>" class="btn btn-default">Agrupar</a>
          <?php } ?>
        </header>

        <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
        <div class="panel-body">
          <form class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">

            <input type="hidden" name="movie_id" value="<?php echo $movie->getId(); ?>" />

            <?php echo $view['form']->row($form['movie_name'],     array('label' => 'Título')); ?>
            <?php echo $view['form']->row($form['active'],         array('label' => 'Activa')); ?>
            <?php echo $view['form']->row($form['featured'],       array('label' => 'Destacado')); ?>
            <?php echo $view['form']->row($form['ESTRENO'],        array('label' => 'Estreno')); ?>
            <?php echo $view['form']->row($form['attribute'],      array('label' => 'Atributos')); ?>
            <?php echo $view['form']->row($form['NOMBREORIGINAL'], array('label' => 'Título Original')); ?>
            <?php echo $view['form']->row($form['DIRECTOR'],       array('label' => 'Director')); ?>
            <?php echo $view['form']->row($form['ACTORES'],        array('label' => 'Actores')); ?>
            <?php echo $view['form']->row($form['GENERO'],         array('label' => 'Género')); ?>
            <?php echo $view['form']->row($form['PAISORIGEN'],     array('label' => 'País')); ?>
            <?php echo $view['form']->row($form['ANIO'],           array('label' => 'Año')); ?>
            <?php echo $view['form']->row($form['SINOPSIS'],       array('label' => 'Sinopsis')); ?>
            <?php echo $view['form']->row($form['RATING'],         array('label' => 'Clasificación')); ?>
            <?php echo $view['form']->row($form['DURACION'],       array('label' => 'Duración')); ?>

            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Trailer</label>
              <div class="col-sm-10">
                <?php echo $view['form']->widget($form['TRAILER']); ?>
                <br />
                <?php if ( $youtube_id = FrontHelper::get_youtube_id_from_url( $movie->getData( 'TRAILER' ) ) ) { ?>
                  <iframe width="300px" height="200px" style="width: 300px; height: 200px;" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                <?php } else { ?>
                  No hay trailer disponible.
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Poster</label>
              <div class="col-sm-10">
                <input type="file" value="" name="movie_poster">
                <br />
                <img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '200x295'); ?>" alt="<?php echo $movie->getName(); ?>" width="154" height="230" />
              </div>
            </div>

            <?php echo $view['form']->row($form['fixed_position'], array('label' => 'Posición')); ?>
            <?php echo $view['form']->row($form['group_id'],       array('label' => 'Group ID')); ?>

            <?php if ('grouper' === $movie->getType()): ?>
              <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Versiones</label>
                <div class="col-sm-10">
                  <?php foreach ($movie->getChildren() as $child): ?>
                    <div>
                      <label class="control-label">
                        <?php echo $child->getId(); ?>: <?php echo $child->getLegacyName(); ?>
                        <a href="<?php echo $view['router']->generate('social_snack_admin_movie', array('movie_id' => $child->getId())); ?>">(editar)</a>
                      </label>
                      <div>
                        <?php echo $view['form']->widget($form['attribute_' . $child->getId()]); ?>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            <?php endif; ?>

            <button type="button" class="btn btn-default" onclick="window.history.back();">Cancelar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </form>
        </div>
      </section>
    </div>
  </div>
<script>
  $(document).ready(function(){
    //var original_position = $('input#form_fixed_position').val()
    $('input#form_ESTRENO').on('click',function(e){
      if ($(this).is(':checked')){
        $('input#form_fixed_position').val(500);
      } else {
        $('input#form_fixed_position').val(0);
      }
    })
  })
</script>
<?php $view['slots']->stop(); ?>