<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set( 'title', $movie->getName() . ' - Cinemex' ); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="canonical" href="<?php echo $view['fronthelper']->get_permalink($movie, TRUE); ?>" />
<link rel="alternate" href="cinemex://com.cinemex/movieComing/<?php echo $movie->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'body' ); ?>
<div class="clearfix row">

	<div class="col col-sm-3-5 col-md-2-3">
		<div id="movie-details">
			<div class="movie-details-main content-box">
				<h1 class="movie-details-title content-box-title"><?php echo $movie->getName(); ?></h1>
				<?php
				$movie_info = array(
						array(
								'label' => 'Título original',
								'key'   => 'NOMBREORIGINAL',
						),
						array(
								'label' => 'Reparto',
								'key'   => 'ACTORES',
						),
						array(
								'label' => 'Director',
								'key'   => 'DIRECTOR',
						),
						array(
								'label' => 'País',
								'key'   => 'PAISORIGEN',
						),
						array(
								'label' => 'Año',
								'key'   => 'ANIO',
						),
						array(
								'label' => 'Calificación',
								'key'   => 'RATING',
						),
						array(
								'label'  => 'Duración',
								'key'    => 'DURACION',
								'format' => '%s minutos',
						),
						array(
								'label' => 'Género',
								'key'   => 'GENERO',
						),
						array(
								'label' => 'Sitio oficial',
								'key'   => 'WEBSITE',
						),
				);
				?>
				<dl class="movie-details-info">
					<dt>Estreno</dt>
					<dd><?php echo $movie->getReleaseDate()->format( 'd/m/Y' ); ?></dd>
					<?php foreach ( $movie_info as $item ) { ?>
					<?php if ( ( $value = $movie->getData( $item['key'] ) ) && is_string( $value ) ) { ?>
					<dt><?php echo $item['label']; ?></dt>
					<dd><?php echo isset( $item['format'] ) ? sprintf( $item['format'], $value ) : $value; ?></dd>
					<?php } ?>
					<?php } ?>
				</dl>

				<div class="movie-details-btns">
					<?php if ( $trailer_url = $movie->getData( 'TRAILER' ) ) { ?>
					<a href="#" class="btn btn-grey btn-icon icon icon-play" rel="tab-select" data-tab="movie-trailer-tab">Ver Tráiler</a>
					<?php } ?>
				</div>

				<img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '182x272'); ?>" alt="<?php echo $movie->getName(); ?>" width="182" height="272" class="movie-details-cover" />
			</div>

			<div class="movie-details-tabs tabs">
				<ul class="movie-details-tabs-links tabs-links">
					<li><a href="#" class="selected"><span class="discicon icon-red icon-star"></span> Trailer</a></li>
					<li><a href="#"><span  class="discicon icon-light icon-screen"></span> Sinopsis</a></li>
					<li><a href="#"><span  class="discicon icon-light icon-comments"></span> Comentarios</a></li>
				</ul>

				<div class="tabs-content">
					<div class="movie-details-tab tab-content selected" id="movie-trailer-tab">
						<?php if ( $youtube_id = FrontHelper::get_youtube_id_from_url( $movie->getData( 'TRAILER' ) ) ) { ?>
						<iframe width="599" height="450" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
						<?php } else { ?>
						No hay tráiler disponible.
						<?php } ?>
					</div>

					<div class="movie-details-tab tab-content">
						<p><?php echo is_string( $movie->getData( 'SINOPSIS' ) ) ? $movie->getData( 'SINOPSIS' ) : ''; ?></p>
					</div>

					<div class="movie-details-tab tab-content">
						<div class="fb-comments" data-href="<?php echo $view['fronthelper']->get_movie_link_abs($movie); ?>" data-colorscheme="light" data-numposts="5" data-width="599"></div>
					</div>
				</div>
			</div>
		</div><!-- #movie-details -->
	</div>

</div>
<?php
$view['slots']->stop();
?>


<?php $view['slots']->start('esi_featured_promos'); ?>
<?php
echo $view['actions']->render(
    $view['router']->generate('esi_important_promos', array('target' => 'movies')),
    array('strategy' => 'esi')
);
?>
<?php $view['slots']->stop(); ?>
