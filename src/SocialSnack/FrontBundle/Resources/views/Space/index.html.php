<?php
$view->extend('SocialSnackFrontBundle:Space:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Espacio Alternativo</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-ballet.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-04.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-ufc.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-3" href="#" style="width:20%"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-01.jpg'); ?>"></a>
          <a class="grid-col-3" href="#" style="width:20%"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-02.jpg'); ?>"></a>
          <a class="grid-col-3" href="#" style="width:20%"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-ballet.jpg'); ?>"></a>
          <a class="grid-col-3" href="#" style="width:20%"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-04.jpg'); ?>"></a>
          <a class="grid-col-3" href="#" style="width:20%"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-ufc.jpg'); ?>"></a>
        </div>
      </div>
    </div>

    <div class="grid-col-5">
      <p class="lead">Espacio Alternativo es un concepto original de Cinemex donde podrás disfrutar una gran variedad de contenido alterno al 7° arte, tales como: Ballet, Ópera, Eventos Deportivos, Eventos de Moda, entre otros.</p>

      <h2>Ballet</h2>
      <p>El Ballet Bolshoi exclusivo en Cinemex regresa con una sobresaliente temporada 2015-2016. Por 3° año consecutivo podrás disfrutar, a unas semanas de su interpretación en Moscú, las producciones de la temporada completa del Bolshoi. Iniciando en Octubre contará con 7 obras entre las que destacan EL CASCANUECES, DON QUIJOTE y ESPARTACO. Grandes intérpretes del ballet se darán cita en la escena de Moscú, tales como Svetlana Zakharova, David Hallberg, Olga Smirnova y Denis Rodkin, junto con el inigualable cuerpo de baile del Bolshoi.</p>

      <h2>Ópera</h2>
      <p>La ciudad de Salzburgo en Austria, situada en el corazón de Europa, se convierte anualmente en sede de uno de los festivales más importantes de Ópera a nivel mundial. Es por esto que Cinemex trae para ti en exclusiva, por primera vez en pantalla grande y con la mejor calidad de imagen y audio, una muestra de las mejores Óperas exhibidas en el Salzburger Festspiele 2014.</p>

      <h2>NFL</h2>
      <p>Vive las mejores jugadas y anotaciones de futbol americano tal y como si estuvieras en el estadio en nuestras transmisiones en vivo y en HD, todos los lunes y jueves por la noche. Disfruta la pasión de la NFL con el mejor ambiente, sólo en Cinemex.</p>

      <h2>Champions League</h2>
      <p>La mejor liga de futbol en el mundo se vive sólo en Cinemex. No te pierdas a los astros del balompié en un ambiente inmejorable y acompañado de una cerveza para gritar gol y celebrar la victoria de tu equipo.</p>

      <h2>UFC</h2>
      <p>Toda la adrenalina de la máxima liga de Artes Marciales Mixtas, el Ultimate Fighter Championship, se podrá disfrutar en exclusiva en las pantallas gigantes de Cinemex los sábados por la noche en vivo a través de UFC Network. Este año se podrán disfrutar más de 8 eventos de campeonato como la UFC 190 entre Ronda Rousey vs. Bethe Correia desde Río de Janeiro y eventos especiales como la final de temporada de The Ultimate Fighter en vivo desde la Arena Monterrey.</p>

      <p class="lead">Disfruta los mejores eventos especiales en Pantalla Gigante y como nunca antes, sólo en Cinemex.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
