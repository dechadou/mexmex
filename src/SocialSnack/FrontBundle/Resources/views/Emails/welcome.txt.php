Cinemex | cinemex.com

¡Bienvenid@ <?php echo $user->getFirstName(); ?>!
¡Ya eres usuario de Cinemex.com!

Puedes ingresar a tu cuenta ingresando en Cinemex.com y completando el email y la contraseña que elegiste al registrarte. 
Selecciona tus salas favoritas, compra tus boletos en pocos clicks y disfruta de todos los beneficios que Cinemex te ofrece

* Comprar tus boletos ¡Nunca fue tan fácil! Accede a tus salas favoritas y disfruta de las mejores películas.
* Disfruta al máximo de todos los estrenos, premieres y funciones exclusivas.
* Conviértete en Invitado Especial. Acumula puntos por todas las compras que realices y canjéalos por lo que tu quieras en Cinemex.

Términos y condiciones | <?php echo $view['router']->generate('tos_page', array(), TRUE); ?>