<?php

namespace SocialSnack\AdminBundle\Tests\Controller;

use SocialSnack\AdminBundle\Debug\Result\SessionResult;
use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;

/**
 * Class WebserviceControllerTest
 * @package SocialSnack\AdminBundle\Tests\Controller
 * @author Guido Kritz
 */
class WebserviceControllerTest extends WebTestCase {

  public function testSessionDebuggerActionForbidden() {
    $client = $this->getLoggedInClient();
    $client->request(
        'GET',
        '/missisippi/ws/sessionDebugger'
    );
    $this->assertEquals(403, $client->getResponse()->getStatusCode());
  }


  public function testSessionDebuggerActionGet() {
    $client = $this->getLoggedInClient(['ROLE_PROCESS']);
    $client->request(
        'GET',
        '/missisippi/ws/sessionDebugger'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testSessionDebuggerActionSubmig() {
    $res_data = [
        'local' => [
            'active' => 1,
            'pricecode' => 'foobar',
            'tickets' => [],
        ],
        'ws' => [
            'active' => 1,
            'pricecode' => 'foobar',
            'tickets' => [],
        ],
        'seats' => [
            'seats' => 0,
        ],
        'select_tickets' => NULL,
        'release_tickets' => NULL,
    ];
    $res = new SessionResult($res_data);
    
    $debugger_mock = $this->getMockBuilder('\SocialSnack\AdminBundle\Debug\Handler\DebugHandler')
        ->disableOriginalConstructor()
        ->getMock();
    $debugger_mock->expects($this->once())
        ->method('debugSession')
        ->willReturn($res);

    $fixtures = [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ];

    $client = $this->getLoggedInClient(['ROLE_PROCESS'], $fixtures);

    $client->getContainer()->set('admin.debug.handler', $debugger_mock);
    $session_id = LoadSessionData::$sessions[0]->getId();

    $crawler = $client->request(
        'GET',
        '/missisippi/ws/sessionDebugger?session_id=' . $session_id
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertGreaterThan(0, $crawler->filter('#debug')->count());
  }


  public function testCinemasUpdatesLogAction() {
    $client = $this->getLoggedInClient(['ROLE_PROCESS']);
    $client->request(
        'GET',
        '/missisippi/ws/cinemas/updatesLog'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }

}