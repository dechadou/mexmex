<?php

namespace SocialSnack\WsBundle\Command;
use Buzz\Exception\RequestException;
use SocialSnack\WsBundle\Entity\ServiceStatus;
use SocialSnack\WsBundle\Request\ConsultaAbstract;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckServicesStatusCommand
 * @package SocialSnack\WsBundle\Command
 * @author Guido Kritz
 */
class CheckServicesStatusCommand extends ContainerAwareCommand {

  protected $fail_count = 0;
  protected $stable_count = 0;
  protected $alerts = [];
  protected $threshold = 60;

  protected function configure() {
    $this
        ->setName('services:check_status')
        ->setDescription('Check the status of the services and trigger alerts in case of issues.')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $doctrine = $this->getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    $threshold = $this->threshold;

    // General WS status.
    $this->checkStatusByKey('ws_fails_general', 'ws_general', $threshold);

    // Purchases WS status.
    $this->checkStatusByKey('ws_fails_buy', 'ws_buy', $threshold);

    // Cinemas status.
    $cinemas = $doctrine->getRepository('SocialSnackWsBundle:Cinema')->findLegacyIds();
    foreach ($cinemas as $cinema) {
      $legacy_id = $cinema['legacy_id'];
      $this->checkStatusByKey('ws_fails_cinema_' . $legacy_id, 'ws_cinema', $threshold, $legacy_id);
      $this->checkStatusByKey('ws_fails_buy_' . $legacy_id, 'ws_cinema_buy', $threshold, $legacy_id);
    }

    // WS Servers status
    $ws_nodes = $this->getContainer()->getParameter('cinemex_ws_host');
    foreach ($ws_nodes as $ws_node) {
      $this->checkNode($ws_node, 'ws_fails_node_' . $ws_node);
      $this->checkStatusByKey('ws_fails_node_' . $ws_node, 'ws_node', $threshold, $ws_node);
    }

    $this->checkCinemasWithoutSessions();

    // Alert!
    if (sizeof($this->alerts)) {
      $this->triggerAlerts();
    }

    // Persist changes in the DB.
    $em->flush();
  }


  protected function checkStatusByKey($mem_key, $status_key, $threshold, $args = '') {
    /** @var \Memcached $memcached */
    $memcached = $this->getContainer()->get('memcached');

    $fails = $memcached->get($mem_key);

    if ($memcached->getResultCode() == \Memcached::RES_NOTFOUND) {
      return;
    }

    if ($fails >= $threshold) {
      $status = 'FAIL';
    } else {
      $status = 'STABLE';
    }

    $this->statusChange($status_key, $status, $args);
  }

  protected function statusChange($service, $status, $args = '') {
    $doctrine = $this->getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    // Check last error.
    $last_error = $doctrine->getRepository('SocialSnackWsBundle:ServiceStatus')->findBy(
        array(
            'service' => $service,
            'args'    => $args,
        ),
        array(
            'id' => 'DESC'
        ),
        1
    );

    if ($last_error) {
      $old_status = $last_error[0]->getStatus();
    } else {
      $old_status = 'NONE';
    }

    // If the last status is the same don't do anything.
    if ($old_status === $status) {
      return;
    }

    // Count
    if ($status === 'FAIL') {
      $this->fail_count++;
    } else {
      $this->stable_count++;
    }

    $method = sprintf(
        'statusChange%s_%sto%s',
        strtoupper($service),
        $old_status,
        $status
    );
    if (method_exists($this, $method)) {
      $this->{$method}($args);
    }

    // Store last status.
    $entry = new ServiceStatus();
    $entry
        ->setService($service)
        ->setArgs($args)
        ->setStatus($status)
    ;
    $em->persist($entry);
  }

  /**
   * WS General status changed from FAIL to STABLE.
   *
   * @param null $args
   */
  protected function statusChangeWS_GENERAL_FAILtoSTABLE($args = NULL) {
    $this->alerts[] = 'El estado general del Webservice parece haberse normalizado.';
  }

  /**
   * WS General status changed from STABLE to FAIL.
   *
   * @param null $args
   */
  protected function statusChangeWS_GENERAL_STABLEtoFAIL($args = NULL) {
    $this->statusChangeWS_GENERAL_NONEtoFAIL($args);
  }

  /**
   * WS General status set to FAIL with no previous status.
   *
   * @param null $args
   */
  protected function statusChangeWS_GENERAL_NONEtoFAIL($args = NULL) {
    $this->alerts[] = 'Posible falla general del Webservice.';
  }

  /**
   * WS Cinema status changed from FAIL to STABLE.
   *
   * @todo Replace the ID with the name of the cinema.
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_FAILtoSTABLE($legacy_id) {
    $this->alerts[] = sprintf('El estado del complejo %s parece haberse normalizado.', $legacy_id);
  }

  /**
   * WS Cinema status changed from STABLE to FAIL.
   *
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_STABLEtoFAIL($legacy_id) {
    $this->statusChangeWS_CINEMA_NONEtoFAIL($legacy_id);
  }

  /**
   * WS Cinema status set to FAIL with no previous status.
   *
   * @todo Replace the ID with the name of the cinema.
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_NONEtoFAIL($legacy_id) {
    $this->alerts[] = sprintf('Posible falla en el complejo %s.', $legacy_id);
  }

  /**
   * WS Cinema buy status changed from FAIL to STABLE.
   *
   * @todo Replace the ID with the name of the cinema.
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_BUY_FAILtoSTABLE($legacy_id) {
    $this->alerts[] = sprintf('El estado del complejo %s (proceso de compra) parece haberse normalizado.', $legacy_id);
  }

  /**
   * WS Cinema buy status changed from STABLE to FAIL.
   *
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_BUY_STABLEtoFAIL($legacy_id) {
    $this->statusChangeWS_CINEMA_BUY_NONEtoFAIL($legacy_id);
  }

  /**
   * WS Cinema buy status set to FAIL with no previous status.
   *
   * @todo Replace the ID with the name of the cinema.
   * @param $legacy_id
   */
  protected function statusChangeWS_CINEMA_BUY_NONEtoFAIL($legacy_id) {
    $this->alerts[] = sprintf('Posible falla en el complejo %s (proceso de compra).', $legacy_id);
  }

  /**
   * WS server status changed from FAIL to STABLE.
   *
   * @param $node_ip
   */
  protected function statusChangeWS_NODE_FAILtoSTABLE($node_ip) {
    $this->alerts[] = sprintf('El estado del servidor %s parece haberse normalizado.', $node_ip);
  }

  /**
   * WS server status changed from STABLE to FAIL.
   *
   * @param $node_ip
   */
  protected function statusChangeWS_NODE_STABLEtoFAIL($node_ip) {
    $this->statusChangeWS_NODE_NONEtoFAIL($node_ip);
  }

  /**
   * WS server status set to FAIL with no previous status.
   *
   * @param $node_ip
   */
  protected function statusChangeWS_NODE_NONEtoFAIL($node_ip) {
    $this->alerts[] = sprintf('Posible falla en el servidor %s.', $node_ip);
  }

  /**
   * Deliver an alert email.
   */
  protected function triggerAlerts() {
    $body = date('Y-m-d H:i') . "\r\n" . implode("\r\n\r\n", $this->alerts);

    // @todo Move hardcoded arguments to parameters!
    $message = \Swift_Message::newInstance()
        ->setSubject(sprintf(
            '[%d/%d] Alerta de estado de servicios de cinemex.com',
            $this->fail_count,
            $this->stable_count
        ))
        ->setFrom(array('noresponder@cinemex.com' => 'Alerta cinemex.com'))
        ->setTo('guido@socialsnack.com')
        ->setBody($body)
    ;
    $this->getContainer()->get('mailer')->send($message);
  }


  protected function checkCinemasWithoutSessions() {
    $doctrine = $this->getContainer()->get('doctrine');
    $conn = $doctrine->getManager()->getConnection();
    $q = "SELECT c.legacy_id, c.name, s.cinema_id as matches FROM Cinema c LEFT JOIN (SELECT cinema_id FROM Session WHERE active = 1 GROUP BY cinema_id) s ON c.id = s.cinema_id WHERE c.active = 1;";
    $stmt = $conn->prepare($q);
    $stmt->execute();
    $res = $stmt->fetchAll();

    foreach ($res as $cinema) {
      if ($cinema['matches'] === NULL) {
        $status = 'FAIL';
      } else {
        $status = 'STABLE';
      }

      $this->statusChange('ws_cinema_sessions', $status, $cinema['legacy_id']);
    }
  }


  protected function statusChangeWS_CINEMA_SESSIONS_NONEtoFAIL($legacy_id) {
    $this->statusChangeWS_CINEMA_SESSIONS_STABLEtoFAIL($legacy_id);
  }


  protected function statusChangeWS_CINEMA_SESSIONS_STABLEtoFAIL($legacy_id) {
    $this->alerts[] = sprintf('El complejo %d no tiene ninguna función activa.', $legacy_id);
  }


  protected function checkNode($host, $key) {
    $browser = $this->getContainer()->get('gremo_buzz');
    $memcached = $this->getContainer()->get('memcached');

    try {
      $res    = $browser->get('https://' . $host . ConsultaAbstract::PATH);
      $status = $res->getStatusCode();
    } catch (RequestException $e) {
      return FALSE;
    }

    $fails = $memcached->get($key);
    if ($fails && $fails >= $this->threshold && 200 === $status) {
      $memcached->set($key, $this->threshold - 1);
    }
  }

} 