<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Splashscreen;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

/**
 * Class SplashscreenHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Rodrigo Catalano
 */
class SplashscreenHandler {

  protected $contextHandler;

  protected $doctrine;

  protected $front_utils;

  public function __construct(ContextHandler $contextHandler, Registry $doctrine, FrontUtils $front_utils) {
    $this->contextHandler = $contextHandler;
    $this->doctrine       = $doctrine;
    $this->front_utils    = $front_utils;
  }


  /**
   * @return \SocialSnack\FrontBundle\Entity\SplashscreenRepository
   */
  protected function getRepository() {
    return $this->doctrine->getRepository('SocialSnackFrontBundle:Splashscreen');
  }


  public function getAll() {
    $splash_repo  = $this->getRepository();
    $splash_repo->useCache(TRUE);
    $result = $splash_repo->findActive();

    return $result;
  }


  public function get($id) {
    $splash_repo  = $this->getRepository();
    $splash_repo->useCache(TRUE);
    $result = $splash_repo->findActive($id);

    return $result;
  }


  public function serializeOne(Splashscreen $sp) {
    $result = array(
        'id' => $sp->getId(),
        'deep_link' => $sp->getDeeplink(),
        'image_v'   => $this->front_utils->get_cms_url($sp->getImageV()),
        'image_h'   => $this->front_utils->get_cms_url($sp->getImageH()),
    );

    if ($this->contextHandler->getApiVersion() < 2) {
      $result['url_image'] = $result['image_v'];
    }

    return $result;
  }

} 