<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityMovieComing extends AbstractEntityTask implements UrlTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->frontUtils->get_permalink($this->entity),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('home'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_movies_coming'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_movies_comingSingle', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_movies_coming'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_movies_comingSingle', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
    ];
  }

}