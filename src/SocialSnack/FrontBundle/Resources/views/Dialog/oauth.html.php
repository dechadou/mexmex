<?php $view->extend('SocialSnackFrontBundle::lightTheme.html.php'); ?>

<?php $view['slots']->start('body'); ?>
<div id="login-popup" class="popup popup-in-page">
  <div class="clearfix">
    <?php echo $this->render('SocialSnackFrontBundle:Partials:loginDialog.html.php'); ?>
  </div>
  <div class="oauth-login-cancel">
    <a href="<?php echo $cancel_uri; ?>">Cancelar</a>
  </div>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script>
  var no_location = true;
  var redirect_uri = '<?php echo $view['router']->generate('front_dialog_appGrantAccess', ['client_id' => $client_id, 'redirect_uri' => $redirect_uri]); ?>';
  var $reg = $('#login-popup-reg');
  $reg.attr('href', $reg.attr('href') + '#redirect_uri=' + redirect_uri);
  $(document).on('user_login', function(e) {
    document.location = redirect_uri;
  });
</script>
<?php $view['slots']->stop(); ?>