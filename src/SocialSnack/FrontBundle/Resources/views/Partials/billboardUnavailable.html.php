<div class="error-404 is-unavailable">
  <p>Por el momento no contamos con este concepto en tu ciudad.<br>
    <span class="highlights">Espéralo muy pronto.</span></p>
  <p><a href="<?php echo $view['router']->generate('home'); ?>" class="btn btn-default-alt switch-cinema btn-icon icon icon-larr btn-icon-left">Volver</a></p>
</div>
