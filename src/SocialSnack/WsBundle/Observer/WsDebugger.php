<?php

namespace SocialSnack\WsBundle\Observer;

use SocialSnack\WsBundle\Request\ConsultaAbstract;

/**
 * Class WsDebugger
 * @package SocialSnack\WsBundle\Observer
 * @author Guido Kritz
 */
class WsDebugger extends WsObserverAbstract {

  protected $data = [];

  protected $supportedMethods = ['request'];


  /**
   * Stores the request information in a buffer.
   *
   * @param ConsultaAbstract       $ws_request
   * @param \Buzz\Message\Response $response
   * @param \Exception             $e
   */
  protected function request(ConsultaAbstract $ws_request, \Buzz\Message\Response $response = NULL, \Exception $e = NULL) {
    $this->data[] = [
        'url' => $ws_request->getUrl(),
        'args' => $ws_request->getArgs(),
        'status_code' => $response ? $response->getStatusCode() : NULL,
        'response' => $response ? $response->getContent() : NULL,
        'error' => $e ? $e->getMessage() : NULL,
    ];
  }


  /**
   * Clears the stored data.
   */
  public function resetData() {
    $this->data = [];
  }


  /**
   * Returns the stored data.
   *
   * @return array
   */
  public function getData() {
    return $this->data;
  }

}