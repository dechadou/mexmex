<?php
$today = new \DateTime( 'today' );
$tomorrow = new \DateTime( 'tomorrow' );
foreach ( $available_dates as $_date ) {
  $_date     = $_date['date'];
  $date_lbl = '';
  if ( $today->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
    $date_lbl = 'Hoy, ';
  } else if ( $tomorrow->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
    $date_lbl = 'Mañana, ';
  }
  $formatter = new \IntlDateFormatter( \Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC' );
  $formatter->setPattern( 'EEEE dd \'de\' MMMM' );
  $date_lbl .= $formatter->format( $_date );
  $date_lbl = ucfirst( $formatter->format( $_date ) );
?>
<option value="<?php echo $_date->format( 'Ymd' ); ?>"<?php if ( $date->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) echo ' selected="selected"'; ?>><?php echo $date_lbl; ?></option>
<?php } ?>
<?php if ( !$available_dates ) { ?>
<option value="">Aún no hay fechas para esta película</option>
<?php } ?>
