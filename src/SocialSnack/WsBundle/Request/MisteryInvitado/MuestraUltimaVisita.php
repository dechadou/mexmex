<?php

namespace SocialSnack\WsBundle\Request\MisteryInvitado;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Request\MisteryInvitadoAbstract;

class MuestraUltimaVisita extends MisteryInvitadoAbstract
{

  protected $method = 'MuestraUltimaVisita';

  protected $default_args = array(
      'NumTransaccion' => NULL,
    );


  /**
   * Parse the request and return only the relevant nodes.
   *
   * @throws RequestMsgException
   * @param \stdClass $res ;
   * @return \SimpleXMLElement
   */
  public function parse_res($res)
  {
    if (!$res->MuestraUltimaVisitaResult) {
      throw new RequestMsgException('Invalid response 0: ' . json_encode($res));
    }

    $xml = simplexml_load_string($res->MuestraUltimaVisitaResult->any, NULL, LIBXML_NOCDATA);

    return $xml;
  }

}
