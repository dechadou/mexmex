<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title><?php $view['slots']->output( 'title', 'Cinemex' ); ?></title>

    <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/bootstrap-reset.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['fronthelper']->get_static_url('admin/assets/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/style.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/style-responsive.css') ?>" rel="stylesheet" />
    
    

    <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/advanced-datatable/media/js/jquery.js') ?>"></script>
    <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/bootstrap.min.js') ?>"></script>
  
  </head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="<?php echo $view['router']->generate('social_snack_adminlogin_check') ?>" method="post">
        <h2 class="form-signin-heading">log in</h2>
        <div class="login-wrap">
            <?php if ($error): ?>
                <div><?php echo $error->getMessage() ?></div>
            <?php endif; ?>
            <input autofocus type="text" id="username" class="form-control"  name="_username" value="<?php echo $last_username ?>" placeholder="User" />
            <input type="password" id="password" name="_password"  class="form-control" placeholder="Password" />
            <input type="submit" name="login" class="btn btn-lg btn-login btn-block" value="log in" />
        </div>

      </form>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>


  </body>
</html>
