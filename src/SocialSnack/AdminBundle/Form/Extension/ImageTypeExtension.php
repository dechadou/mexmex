<?php

namespace SocialSnack\AdminBundle\Form\Extension;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints\Null;

class ImageTypeExtension extends AbstractTypeExtension {

  public function getExtendedType() {
    return 'file';
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setOptional(array(
        'image_preview_path',
        'image_preview_size',
    ));
    $resolver->setDefaults(array(
        'image_preview_size' => NULL,
    ));
  }

  public function buildView(FormView $view, FormInterface $form, array $options) {
    if (array_key_exists('image_preview_path', $options)) {
      $parentData = $form->getParent()->getData();

      if (is_array($parentData)) {
        $imageUrl = FrontHelper::get_array_path($parentData, $options['image_preview_path']);
      } elseif (null !== $parentData) {
        $accessor = PropertyAccess::createPropertyAccessor();
        $imageUrl = $accessor->getValue($parentData, $options['image_preview_path']);
      } else {
        $imageUrl = null;
      }

      // set an "image_url" variable that will be available when rendering this field
      $view->vars['image_preview_path'] = $imageUrl;
      $view->vars['image_preview_size'] = $options['image_preview_size'];
    }
  }

} 