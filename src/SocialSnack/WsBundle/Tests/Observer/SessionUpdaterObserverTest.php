<?php

namespace SocialSnack\WsBundle\Tests\Observer;

use SocialSnack\FrontBundle\Service\CachePurger;

class SessionUpdaterObserverTest extends \PHPUnit_Framework_TestCase {

  protected $cachePurger;

  public function setUp() {
    $this->cachePurger = $this->getMockBuilder('SocialSnack\FrontBundle\Service\CachePurger')->disableOriginalConstructor()->getMock();
  }

  protected function getServiceMock(array $methods = NULL, $callConstructor = TRUE) {
    $builder = $this->getMockBuilder('SocialSnack\WsBundle\Observer\SessionUpdaterObserver');

    $builder->setMethods($methods);

    if (!$callConstructor) {
      $builder->disableOriginalConstructor();
    } else {
      $builder->setConstructorArgs([
          $this->cachePurger
      ]);
    }

    return $builder->getMock();
  }


  public function testCantHandle() {
    $observer = $this->getServiceMock();

    $task = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task->method('getService')->willReturn('foobar');

    $this->assertFalse($observer->update($task, TRUE, []));
  }


  public function testCachePurgeOnSuccess() {
    $observer = $this->getServiceMock();

    $this->cachePurger->expects($this->exactly(2))
        ->method('enqueue')
        ->withConsecutive(
            array($this->equalTo([
                'type'      => 'method',
                'method'    => CachePurger::PURGE_CINEMA_TICKETS,
                'cinema_id' => 666,
            ])),
            array($this->equalTo([
                'type'      => 'method',
                'method'    => CachePurger::PURGE_CINEMA_SESSIONS,
                'cinema_id' => 666,
            ]))
        );

    $this->cachePurger->expects($this->once())
        ->method('purge_cache');

    $task = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task->method('getService')->willReturn('cinemex.session_updater');
    $task->method('getData')->willReturn('a:4:{s:9:"cinema_id";i:666;s:9:"propagate";b:1;s:7:"enqueue";b:0;s:6:"source";s:4:"cron";}');

    $observer->update($task, TRUE, []);
  }

}