<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\RestBundle\Entity\App;

class LoadAppData implements FixtureInterface {

  static $apps;

  public function load(ObjectManager $manager) {
    self::$apps = [];

    $app = new App();
    $app
        ->setActive(TRUE)
        ->setName('Tests')
        ->setDescription('Test app')
        ->setSecretKey('abcabc')
        ->setSetting('appPass', 'p4ssw0rd')
        ->setCap('purchase', TRUE)
        ->setCap('direct_login', TRUE)
        ->setAppId('abc')
    ;
    $manager->persist($app);
    self::$apps[] = $app;

    $app = new App();
    $app
        ->setActive(TRUE)
        ->setName('With caps')
        ->setDescription('App with all capabilities')
        ->setSecretKey('capcap')
        ->setAppId('allcaps')
        ->setCap('list_users')
        ->setCap('users_info')
        ->setCap('users_history')
        ->setCap('list_users')
    ;
    $manager->persist($app);
    self::$apps[] = $app;

    $manager->flush();
  }
} 