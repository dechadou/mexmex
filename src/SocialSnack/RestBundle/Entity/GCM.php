<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * GCM
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="uuid_idx", columns={"uuid", "device"}),
 *   @ORM\Index(name="gcm_idx", columns={"gcmid", "device"})
 * })
 * @ORM\Entity(repositoryClass="GCMRepository")
 */
class GCM
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=127)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="gcmid", type="string", length=255)
     */
    private $gcmid;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=50)
     */
    private $device;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_registered", type="datetime")
     */
    private $dateRegistered;

    /**
     * @ORM\ManyToOne(targetEntity="\SocialSnack\FrontBundle\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return GCM
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set gcmid
     *
     * @param string $gcmid
     * @return GCM
     */
    public function setGcmid($gcmid)
    {
        $this->gcmid = $gcmid;
    
        return $this;
    }

    /**
     * Get gcmid
     *
     * @return string 
     */
    public function getGcmid()
    {
        return $this->gcmid;
    }

    /**
     * Set device
     *
     * @param string $device
     * @return GCM
     */
    public function setDevice($device)
    {
        $this->device = $device;
    
        return $this;
    }

    /**
     * Get device
     *
     * @return string 
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return GCM
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateRegistered
     *
     * @param \DateTime $dateRegistered
     * @return GCM
     */
    public function setDateRegistered($dateRegistered)
    {
        $this->dateRegistered = $dateRegistered;
    
        return $this;
    }

    /**
     * Get dateRegistered
     *
     * @return \DateTime 
     */
    public function getDateRegistered()
    {
        return $this->dateRegistered;
    }
    

    /**
     * Set user
     *
     * @param \SocialSnack\FrontBundle\Entity\User $user
     * @return GCM
     */
    public function setUser(\SocialSnack\FrontBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialSnack\FrontBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}