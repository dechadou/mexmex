<?php echo $subject . PHP_EOL; ?>
=====


Información general
-----

Nombre completo: <?php echo $data['general_name'] . PHP_EOL; ?>
Calle y número: <?php echo $data['general_address'] . PHP_EOL; ?>
Colonia: <?php echo $data['general_neighborhood'] . PHP_EOL; ?>
Código postal: <?php echo $data['general_postal_code'] . PHP_EOL; ?>
Estado: <?php echo $data['general_state'] . PHP_EOL; ?>
Municipio, Delegación o Ciudad: <?php echo $data['general_city'] . PHP_EOL; ?>
Teléfono celular: <?php echo $data['general_phone_mobile'] . PHP_EOL; ?>
Teléfono de casa: <?php echo $data['general_phone_home'] . PHP_EOL; ?>
Correo electrónico: <?php echo $data['general_email'] . PHP_EOL; ?>
Fecha de nacimiento: <?php echo $data['general_birthdate']->format('d/m/Y') . PHP_EOL; ?>
Edad: <?php echo $data['general_age'] . PHP_EOL; ?>
Estado civil: <?php echo $data['general_marital_status'] . PHP_EOL; ?>
¿Tiene hijos?: <?php echo $data['general_have_childrens'] . PHP_EOL; ?>
Numero de pre afiliación o IMSS: <?php echo $data['general_number_imss'] . PHP_EOL; ?>
RFC: <?php echo $data['general_number_rfc'] . PHP_EOL; ?>
CURP: <?php echo $data['general_number_curp']; ?>


Recuerda que por Ley, es tu responsabilidad cuidar los Datos Personales.
Cualquier duda o comentario envíalo a privacidad@cinemex.net


Área de interés
-----

<?php if ($data['area_available_trips'] == 1) { ?>Disponible para viajar: sí<?php echo PHP_EOL; } ?>
<?php if ($data['area_available_move'] == 1) {  ?>Disponible para cambio de residencia: sí<?php echo PHP_EOL; } ?>

<?php if ($data['area_type'] == 'cafes') : ?>
Complejo de interés: <?php echo $data['area_cafe'] . PHP_EOL; ?>
<?php
endif;

if ($data['area_type'] == 'cinemas') : ?>
Área de interés: <?php echo $data['area_name'] . PHP_EOL; ?>
Complejo de interés: <?php echo $data['cinema'] . PHP_EOL; ?>
<?php endif; ?>


Escolaridad
-----

Último grado de estudios: <?php echo $data['school_grade'] . PHP_EOL; ?>
Estudios actuales: <?php echo $data['school_actual'] . PHP_EOL; ?>


Trabajos anteriores
-----

<?php
for ($i = 1; $i <= $jobs_loop; ++$i) :
    if (empty($data["experience_{$i}company"])) {
        continue;
    }
?>
Empresa: <?php echo $data["experience_{$i}company"] . PHP_EOL; ?>
Puesto: <?php echo $data["experience_{$i}position"] . PHP_EOL; ?>
Periodo: <?php echo $data["experience_{$i}period"] . PHP_EOL; ?>
Sueldo: <?php echo $data["experience_{$i}salary"] . PHP_EOL; ?>
Motivo de salida: <?php echo $data["experience_{$i}exit_reason"] . PHP_EOL; ?>

<?php endfor; ?>

Datos familiares
-----

<?php foreach ($family as $field => $label) : ?>
<?php
if (empty($data["family_{$field}_name"])) {
    continue;
}

if ($label !== NULL) :
    echo "* {$label}\n\n";
endif;
?>
Nombre: <?php echo $data["family_{$field}_name"] . PHP_EOL; ?>
Edad: <?php echo $data["family_{$field}_age"] . PHP_EOL; ?>
Ocupación: <?php echo $data["family_{$field}_job"] . PHP_EOL; ?>
Empresa: <?php echo $data["family_{$field}_company"] . PHP_EOL; ?>

<?php endforeach; ?>

Tiene familia trabajando en cinemex: <?php echo $data["family_cinemex"] . PHP_EOL; ?>
<?php if ('no' != $data["family_cinemex"]) { ?>
Complejo en el que labora: <?php echo $data["family_cinema"] . PHP_EOL; ?>
<?php } ?>
