<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\FrontBundle\Entity\IeBenefit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormBuilderInterface;

class IeBenefitType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('name', 'text', ['label' => 'Nombre'])
        ->add('description', 'textarea', [
            'label' => 'Descripción',
            'required' => FALSE,
        ])
        ->add('imageFile', 'file', [
            'label' => 'Imagen',
            'mapped' => FALSE,
            'required' => $builder->getData()->getId() ? FALSE : TRUE,
            'constraints' => [
                new Assert\Image([
                    'mimeTypesMessage' => 'El archivo no es una imagen válida.'
                ]),
            ],
            'image_preview_path' => 'image',
            'image_preview_size' => '100x100',
        ])
        ->add('category', 'choice', [
            'label' => 'Categoría',
            'choices' => [
                IeBenefit::CATEGORY_BASIC   => 'Básico',
                IeBenefit::CATEGORY_GOLD    => 'Oro',
                IeBenefit::CATEGORY_PREMIUM => 'Premium',
            ],
            'attr' => ['class' => 'form-control']
        ])
        ->add('active', 'checkbox', [
            'label' => 'Activo'
        ])
    ;
  }

  public function getName() {
    return '';
  }

}