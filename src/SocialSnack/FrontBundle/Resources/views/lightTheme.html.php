<?php $view->extend('SocialSnackFrontBundle::skeleton.html.php'); ?>
<?php $view['slots']->set('body_class', 'light-theme'); ?>
<?php $view['slots']->start('base'); ?>
<div id="light-theme-logo">
  Cinemex
</div>
<?php $view['slots']->output('body'); ?>
<?php $view['slots']->stop(); ?>