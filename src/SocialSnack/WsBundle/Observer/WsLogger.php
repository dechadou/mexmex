<?php

namespace SocialSnack\WsBundle\Observer;

use Monolog\Logger;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Request\ConsultaAbstract;
use Symfony\Component\Validator\Constraints\Null;

/**
 * Class WsLogger
 *
 * Observer class to handle WS requests logging.
 *
 * @package SocialSnack\WsBundle\Observer
 * @author Guido Kritz
 */
class WsLogger extends WsObserverAbstract {

  protected $logger;

  protected $supportedMethods = ['request'];


  public function __construct(Logger $logger) {
    $this->logger = $logger;
  }


  /**
   * Logs the result of the ConsultaAbstract::request() method.
   *
   * @param ConsultaAbstract       $ws_request
   * @param \Buzz\Message\Response $response
   * @param \Exception             $e
   */
  protected function request(ConsultaAbstract $ws_request, \Buzz\Message\Response $response = NULL, \Exception $e = NULL) {
    $url = $ws_request->getUrl();

    if ($response) {
      $http_status = $response->getStatusCode();
      $res         = $response->getContent();
    } else {
      $http_status = NULL;
      $res         = NULL;
    }

    $time = $ws_request->getElapsedTime();
    $args = $ws_request->getArgs();
    $args = $this->sanitizeRequestArgs($args);

    $log_args_qs = http_build_query( $args );

    $log = "Client IP: " . FrontHelper::get_user_ip() . "\r\n"
        . "Request time: $time\r\n"
        . "Request URL: $url\r\n"
        . "Request Arguments: $log_args_qs\r\n";

    if ($http_status) {
      $log .= "Response code: $http_status\r\n";
    }

    $log .= "Stats: " . implode(', ' , $ws_request->getSuccessStatsKeys($e)) . "\r\n";

    if ($res) {
      $log .= "Response: $res\r\n";
    }

    if ($e) {
      $log .= "Error: " . $e->getMessage() . "\r\n";
    }

    $this->logger->info($log);
  }

}