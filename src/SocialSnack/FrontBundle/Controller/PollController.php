<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\WsBundle\Request\MisteryShopper\MuestraCatalogo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class PollController extends Controller {

  private function iePollAction($route_name, $transaction_id, $view)
  {
    // $transaction_id = 26249183;
    $request = $this->getRequest();

    $info = NULL;

    $ws = $this->get('cinemex_ws');
    $ws->init('MisteryInvitado\MuestraCatalogo');

    $questions = $ws->request([
      'catalogo'     => 3,
      'CveEncuesta'  => 2,
      'TipoCatalogo' => 'ENCUESTA'
    ]);

    $answers   = $ws->request([
      'catalogo'     => 3,
      'CveEncuesta'  => 2,
      'TipoCatalogo' => 'RESPUESTA'
    ]);

    $ws->init('MisteryInvitado\MuestraUltimaVisita');
    $info = $ws->request(['NumTransaccion' => $transaction_id]);

    $choices = range(1, 10);
    $choices = array_combine($choices, $choices);

    $form = $this->createFormBuilder()
      ->add('question_1', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_2', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_3', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_4', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_5', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_6', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_7', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_8', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_9', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_10', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->add('question_11', 'choice', array(
        'choices'  => $choices,
        'required' => TRUE
      ))
      ->getForm();

    $questions = array(
      array(
        'text' => '¿Qué tan probable es que recomiendes a Cinemex a un amigo o familiar?',
        'field' => 'question_1'
      ),
      array(
        'text' => '¿En general como evaluarías tu experiencia?',
        'field' => 'question_2'
      ),
      array(
        'text' => '¿Cómo consideras la amabilidad y trato del personal?',
        'field' => 'question_3'
      ),
      array(
        'text' => '¿Cómo consideras la limpieza del cine?',
        'notes' => 'Lobby, dulcería, salas, sanitarios',
        'field' => 'question_4'
      ),
      array(
        'text' => '¿Cómo consideras los tiempos de espera para ser atendido?',
        'notes' => 'Tiempo en fila',
        'field' => 'question_5'
      ),
      array(
        'text' => '¿Consideras que nuestro servicio es ágil y rápido?',
        'field' => 'question_6'
      ),
      array(
        'text' => '¿Consideras que nuestro personal está capacitado correctamente?',
        'field' => 'question_7'
      ),
      array(
        'text' => '¿Cómo consideras la calidad y sabor de tus alimentos?',
        'field' => 'question_8'
      ),
      array(
        'text' => '¿La película se proyectó sin problemas? ',
        'notes' => 'Audio, imagen, subtítulos',
        'field' => 'question_9'
      ),
      array(
        'text' => '¿La comodidad de tu butaca fue adecuada?',
        'field' => 'question_10'
      ),
      array(
        'text' => '¿La temperatura de la sala fue adecuada?',
        'field' => 'question_11'
      )
    );

    $form->handleRequest($request);

    $success = FALSE;
    $error   = FALSE;

    if ($request->isMethod('POST')) {
      $form->submit($request);

      if ($form->isValid()) {
        $data = $form->getData();

        $query = array(
          'ClaveTransac' => $transaction_id,
          'Respuesta1'   => (string) $_POST['question_1'],
          'Respuesta2'   => (string) $_POST['question_2'],
          'Respuesta3'   => (string) $_POST['question_3'],
          'Respuesta4'   => (string) $_POST['question_4'],
          'Respuesta5'   => (string) $_POST['question_5'],
          'Respuesta6'   => (string) $_POST['question_6'],
          'Respuesta7'   => (string) $_POST['question_7'],
          'Respuesta8'   => (string) $_POST['question_8'],
          'Respuesta9'   => (string) $_POST['question_9'],
          'Respuesta10'  => (string) $_POST['question_10'],
          'Respuesta11'  => (string) $_POST['question_11']
        );

        $ws->init('MisteryInvitado\RegistraEncuesta');

        try {
          $message = $ws->request($query);

          $success = TRUE;
          $error   = FALSE;
          $data    = ['status' => 'success'];
        } catch (\SocialSnack\WsBundle\Exception\UnexpectedWsResponseException $e) {
          $success = FALSE;
          $error   = TRUE;
          $data    = ['status' => 'error'];
        }
      } else {
        $success = FALSE;
        $error   = TRUE;
        $data    = ['status' => 'error'];
      }

      return new JsonResponse($data);
    }

    return $this->render($view, array(
      'info'      => $info,
      'questions' => $questions,
      'success'   => $success,
      'error'     => $error,
      'form'      => $form->createView()
    ));
  }

  /**
   * @Route(
   *   "/encuesta-iev1/{transaction_id}",
   *   name="encuesta_iev1",
   *   defaults={"transaction_id" = ""}
   * )
   *
   * @return Response
   */
  public function ieV1Action($transaction_id = NULL)
  {
    return $this->iePollAction('encuesta_iev1', $transaction_id, 'SocialSnackFrontBundle:Poll:poll_ie-v1.html.php');
  }

  /**
   * @Route(
   *   "/encuesta-iev2/{transaction_id}",
   *   name="encuesta_iev2",
   *   defaults={"transaction_id" = ""}
   * )
   *
   * @return Response
   */
  public function ieV2Action($transaction_id = NULL)
  {
    return $this->iePollAction('encuesta_iev2', $transaction_id, 'SocialSnackFrontBundle:Poll:poll_ie-v2.html.php');
  }

  /**
   * @Route(
   *   "/encuesta-iev3/{transaction_id}",
   *   name="encuesta_iev3",
   *   defaults={"transaction_id" = ""}
   * )
   *
   * @return Response
   */
  public function ieV3Action($transaction_id = NULL)
  {
    $questions = array();

    $questions[] = array(
      array(
        'text' => 'Pensando en tu última visita, en general, ¿qué tan satisfecho(a) estás con la experiencia que viviste?',
        'type' => 'range'
      ),
      array(
        'text' => 'Pensando en esta última visita ¿Qué tan probable es que recomiendes a un amigo o familiar acudir a este cine?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Por qué?',
        'type' => 'textarea'
      )
    );

    $questions[] = array(
      array(
        'text' => '¿Qué tan probable es que regreses a este cine la próxima vez que vayas a ver una película?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan de acuerdo estás en que este cine es mejor que otros?',
        'type' => 'range'
      )
    );

    $questions[] = array(
      array(
        'text' => '¿Qué tan satisfecho estás con el tiempo de espera para ser atendido?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con la rapidez con la que te atendió el personal?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con la amabilidad y trato del personal que te atendió?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con el conocimiento del personal que te atendió?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con los alimentos y bebidas?',
        'type' => 'range'
      )
    );

    $questions[] = array(
      array(
        'text' => '¿Qué tan satisfecho estás con la temperatura de la sala?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con la comodidad de la butaca?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con la calidad de la proyección (imagen, audio, subtítulos)?',
        'type' => 'range'
      ),
      array(
        'text' => '¿Qué tan satisfecho estás con la limpieza del cine?',
        'type' => 'range'
      )
    );

    $questions[] = array(
      array(
        'text' => '¿Qué tan satisfecho estás con lo que pagaste por todo lo que recibiste a cambio?',
        'type' => 'range'
      ),
      array(
        'text' => 'Durante tu estancia en el cine ¿hubo algo que no te agradó?',
        'type' => 'boolean'
      ),
      array(
        'text' => '¿Qué fue lo que no te agradó?',
        'type' => 'textarea'
      )
    );

    return $this->render('SocialSnackFrontBundle:Poll:poll_ie-v3.html.php', array(
      'questions_groups' => $questions
    ));

    return $this->iePollAction('encuesta_iev3', $transaction_id, 'SocialSnackFrontBundle:Poll:poll_ie-v3.html.php');
  }

  /**
   * @Route(
   *   "/encuesta-iev4/{transaction_id}",
   *   name="encuesta_iev4",
   *   defaults={"transaction_id" = ""}
   * )
   *
   * @return Response
   */
  public function ieV4Action($transaction_id = NULL)
  {
    return $this->iePollAction('encuesta_iev4', $transaction_id, 'SocialSnackFrontBundle:Poll:poll_ie-v4.html.php');
  }

  /**
   * @Route("/encuesta/{transaccion}", name="encuesta",
   *   requirements={"transaccion"=".+"}
   * )
   * @Template(engine="php")
   */
  public function pollAction($transaccion, Request $request) {
    $ws = $this->get('cinemex_ws');
    $ws->init('MisteryShopper\MuestraCatalogo');

    $questions = $ws->request(['catalogo' => MuestraCatalogo::CATALOGO_PREGUNTAS]);
    $answers   = $ws->request(['catalogo' => MuestraCatalogo::CATALOGO_RESPUESTAS]);

    $ws->init('MisteryShopper\MuestraUltimaVisita');
    $info = $ws->request(['NumTransaccion' => $transaccion]);


    $form = $this->createFormBuilder()
            ->add('question_1', 'choice', array(
                'choices' => array(
                    '7' => 'SI',
                    '8' => 'NO'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_2', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_3', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_4', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_5', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '6' => 'N/A'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_6', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_7', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '6' => 'N/A'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_8', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_9', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_10', 'choice', array(
                'choices' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'expanded' => true,
                'multiple' => false
                    )
            )
            ->add('question_11', 'textarea', array('required' => false))
            ->getForm();

    $form->handleRequest($request);
    $message = "";

    if ($form->isValid()) {
      $data = $form->getData();

      if ($data['question_11'] == "") {
        $data['question_11'] = ".";
      }

      $args = array(
          'ClaveTransac' => $transaccion,
          'Respuesta1' => (string) $data['question_1'],
          'Respuesta2' => (string) $data['question_2'],
          'Respuesta3' => (string) $data['question_3'],
          'Respuesta4' => (string) $data['question_4'],
          'Respuesta5' => (string) $data['question_5'],
          'Respuesta6' => (string) $data['question_6'],
          'Respuesta7' => (string) $data['question_7'],
          'Respuesta8' => (string) $data['question_8'],
          'Respuesta9' => (string) $data['question_9'],
          'Respuesta10' => (string) $data['question_10'],
          'Respuesta11' => (string) $data['question_11']
      );

      $ws->init('MisteryShopper\RegistraEncuesta');
      $message = $ws->request($args);
    }

    $response = $this->render('SocialSnackFrontBundle:Poll:poll.html.php', array(
        'info'      => $info,
        'questions' => $questions,
        'form'      => $form->createView(),
        'message'   => $message,
    ));

    return $response;
  }

}
