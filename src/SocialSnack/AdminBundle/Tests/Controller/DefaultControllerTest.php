<?php

namespace SocialSnack\AdminBundle\Tests\Controller;

class DefaultControllerTest extends WebTestCase {

  public function testNotLoggedRedirect() {
    $client = static::createClient();
    $client->request('GET', '/missisippi/');
    $this->assertTrue($client->getResponse()->isRedirect());
    $this->assertRegExp('/\/admin_login$/', $client->getResponse()->headers->get('Location'));
  }

}
