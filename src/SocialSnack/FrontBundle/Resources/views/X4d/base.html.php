<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'x4D - Cinemex');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/sections/x4d-header.png'); ?>">
</div>

<div class="section-nav">
  <ul>
    <li><a href="<?php echo $view['router']->generate('x4d_billboard'); ?>" <?php if ($current_page === 'billboard') : ?>class="selected"<?php endif; ?>>Cartelera</a></li>
    <li><a href="<?php echo $view['router']->generate('x4d_cinemas'); ?>" <?php if ($current_page === 'cinemas') : ?>class="selected"<?php endif; ?>>Cines</a></li>
    <li><a href="<?php echo $view['router']->generate('x4d_index'); ?>" <?php if ($current_page === 'index') : ?>class="selected"<?php endif; ?>>Experiencia X4D</a></li>
  </ul>
</div>

<div class="section-content">
<?php $view['slots']->output('section_body'); ?>
</div>
<?php $view['slots']->stop(); ?>
