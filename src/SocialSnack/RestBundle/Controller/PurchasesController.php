<?php
namespace SocialSnack\RestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class PurchasesController extends BaseController {
  
  /**
   * @Route("/", name="rest_purchases_list")
   * 
   * @param Request $request
   * @return \SocialSnack\RestBundle\Controller\JsonResponse
   */
  public function indexAction(Request $request) {
    $this->validate_client_cap('users_history');
    
    $url    = $this->generateUrl('rest_purchases_list', array(), TRUE);
    $query  = $this->getRequest()->query;
    $res    = array();
    $since  = $query->get('since');
    $before = $query->get('before');
    $count  = $query->get('count');
    $users  = $query->get('users_only');
    $repo   = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Transaction');
    $qb     = $repo->createQueryBuilder('u');
    
    // Set default/max number of results per page.
    if (!$count) {
      $count = 100;
    }
    if ($count > 250) {
      $count = 250;
    }
    
    $qb->select('u')
        ->setMaxResults($count)
        ->orderBy('u.id', 'DESC');

    // Set users_only filter.
    if ($users) {
      $qb->andWhere( $qb->expr()->isNotNull('u.user') );
    }
    
    // Set pagination offset.
    if ($since) {
      $qb->andWhere('u.id > :since_id')
          ->setParameter('since_id', $since);
    } elseif ($before) {
      $qb->andWhere('u.id < :before_id')
          ->setParameter('before_id', $before);
    }
    
    $q = $qb->getQuery();
    $transactions = $q->getResult();
    
    foreach ( $transactions as $trans ) {
      $res[] = $trans->toArray(array('user_id' => TRUE));
    }
    $data = array('items' => $res);
    
    // Build pagination links.
    if (sizeof($res)) {
      $qb = $repo->createQueryBuilder('u');
      $qb->select('MAX(u.id),MIN(u.id)');
      $query_ids = $qb->getQuery();
      $ids = $query_ids->getArrayResult();
      $ids = $ids[0];
      $pagination = array();
      $first = $res[0];
      $last  = end($res);
      $query->remove('since');
      $query->remove('before');
      if ( $ids[1] > $first['id'] ) {
        $query_args = $query->all();
        $query_args['since'] = $first['id'];
        $pagination['next'] = $url . '?' . http_build_query($query_args);
      }
      if ( $ids[2] < $last['id'] ) {
        $query_args = $query->all();
        $query_args['before'] = $last['id'];
        $pagination['prev'] = $url . '?' . http_build_query($query_args);
      }
      $data['pagination'] = $pagination;
    }
    
    return new JsonResponse($data);
  }
  
}
