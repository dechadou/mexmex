<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

/**
 * Class Premium
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class Premium extends ConceptHelperAbstract {

  protected function getConceptName() {
    return 'Premium';
  }

} 