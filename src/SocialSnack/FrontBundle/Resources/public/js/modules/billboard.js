var $main = $('#billboard-main');
var current_page = 0;
var loading = false;
var load_complete = false;

var current_state = get_state_by_id(state_id);

// Set new props
current_state.hasPremium  = _current_state.hasPremium;
current_state.hasPlatinum = _current_state.hasPlatinum;
current_state.hasX4d      = _current_state.hasX4d;
current_state.hasCx       = _current_state.hasCx;
current_state.has3d       = _current_state.has3d;


var $filters = $('[name=format-filter]'),
    $langs   = $('[name=lang-filter]'),
    $hours   = $('#hours-range');

jQuery.fn.NtoH = function(val) {
  val = Math.round(val * 2) / 2;
  var sign = 'a.m.';
  if ( val >= 12 && val < 13 ) {
    sign = 'p.m.';
  } else if ( val == 24 ) {
    val = 12;
  } else if ( val >= 13 ) {
    val -= 12;
    sign = 'p.m.';
  }
  var t = (val < 10 ? '0' : '') + Math.floor(val) + (val%1 ? ':30 ' : ':00 ') + sign;
  if ('12:00 a.m.' === t) {
    t = '11:59 p.m.';
  }
  $(this).html(t);
}


function bb_set_attributes() {
//  $('.billboard-block[data-platinum]').find('.mycinema-sessions-group').attr('data-platinum', 1);
  $('.mycinema-sessions-group').not('[data-v4d],[data-premium],[data-platinum]').attr('data-trad', 1);
}


$(window).on('beforeunload', function() {
    $(window).scrollTop(0); 
});

jQuery( document ).ready( function( $ ) {
  $(window).scrollTop(0);
  sticky_side = $('#billboard-side').sticky_sidebar();
  sticky_main = $('.billboard-block').sticky_billboard_title('.billboard-block-title');

  $hours.noUiSlider({
    range : default_hours,
    start : default_hours,
    step  : 0.5,
    serialization:{
      to : [
        [$('#horus-range-min'),'NtoH'],
        [$('#horus-range-max'),'NtoH']
      ]
    }
  }).change(function() {
    set_filters();
  });

  bb_set_attributes();
  parse_url_args();
  
  if (is_state) {
    load_page();
  } else {
    set_filters();
  }

  $( '#billboard-movie' ).bind( 'change', function( e ) {
    set_filters();
  } );

  $( '[name=format-filter]' ).bind( 'change', function( e ) {
    set_checks();
    set_filters();
  } );

  $( '[name=lang-filter]' ).bind( 'change', function( e ) {
    set_filters();
  } );

  $( '#billboard-date' ).bind( 'change', function( e ) {
    document.location = get_search_url();
    $('.narrow-side-col').blockui( 'loading' );
  } );

  $( '#billboard-area' ).bind( 'change', function( e ) {
    e.preventDefault();
    var val = $(this).val();
    var url = document.location + '';

    if (!val) {
      return false;
    }

    url = url.replace( /\/cartelera\/[a-z]+\-\d+\/[a-zA-Z0-9\-]+(\/.*)?/, '/cartelera/' + val + '/$1' );
    $('.narrow-side-col').blockui( 'loading' );
    document.location = url;
  } );

  $platinum_bt.unbind('click').click(function(e){
    e.preventDefault();
    if ( $platinum_bt.hasClass('pressed') ) {
      return false;
    }
    set_platinum(true);
    set_filters();
  });
  $no_platinum_bt.unbind('click').click(function(e){
    e.preventDefault();
    if ( $no_platinum_bt.hasClass('pressed') ) {
      return false;
    }
    set_platinum(false);
    set_filters();
  });
} );

var $platinum_bt    = $('#platinum-bt'),
    $no_platinum_bt = $('#no-platinum-bt');

function set_platinum(pressed) {
  if (current_state.hasPlatinum == false) {
    $no_platinum_bt
      .bind('click', false)
      .hide();

    $platinum_bt
      .bind('click', false)
      .hide();

    return;
  }

  if ( pressed ) {
    $no_platinum_bt.removeClass('pressed');
    $no_platinum_bt.find('span').text('Ver todas las salas');
    $no_platinum_bt.find('.icon').removeClass('icon-tick').addClass('icon-cinemex');
    $platinum_bt.addClass('pressed');
    $platinum_bt.find('span').text('Viendo salas Platino');
    $platinum_bt.find('.icon').removeClass('icon-platinum').addClass('icon-tick');
    $('.no-platinum').addClass('disabled').find('input').prop('disabled', pressed);
    $('.no-platinum').find('[name="format-filter"]').prop('checked', false);
  } else {
    $no_platinum_bt.addClass('pressed');
    $no_platinum_bt.find('span').text('Viendo todas las salas');
    $no_platinum_bt.find('.icon').addClass('icon-tick').removeClass('icon-cinemex');
    $platinum_bt.removeClass('pressed');
    $platinum_bt.find('span').text('Ver salas Platino');
    $platinum_bt.find('.icon').addClass('icon-platinum').removeClass('icon-tick');
    $('.no-platinum').removeClass('disabled').find('input').prop('disabled', pressed);
  }
}

function build_args_string() {
  var result = [];
  var hours = $hours.val().join(',');
  var movie = $('#billboard-movie').val();
  var filters = [];
  var langs = [];
  var date = $('#billboard-date').val();

  if ( date ) {
    result.push('date-' + date);
  }

  if ( default_hours.join(',') != hours ) {
    result.push('hours-' + hours);
  }

  if ( movie ) {
    result.push('movie-' + movie);
  }

  $filters.filter(':checked').each(function() {
    filters.push($(this).val());
  });
  if ( filters.length ) {
    result.push('filters-' + filters.join(','));
  }

  $langs.filter(':checked').each(function() {
    if ( $(this).val()) {
      langs.push($(this).val());
    }
  });
  if ( langs.length ) {
    result.push('langs-' + langs.join(','));
  }

  if ( $('#platinum-bt').hasClass('pressed') ) {
    result.push('platinum-1');
  }

  return result.join('/');
}

function get_search_url() {
  return document.location.href.replace(
      /cartelera\/([a-z]+\-\d+)\/([a-zA-Z0-9\-]+).*/,
      'cartelera/$1/$2/' + build_args_string()
  );
}

/**
 * Clear all the filters and set them to the default values.
 * 
 * @returns {undefined}
 */
function reset_filters() {
  $hours.val(default_hours);
  $('#platinum-bt').removeClass('pressed');
  set_check($filters, true);
  $filters.prop('checked', false);
  $('#billboard-date').val('');
  $('#billboard-movie').val('');
  set_platinum(false);
}

/**
 * Parse the arguments from the current URL and set the filters according to
 * those arguments.
 * 
 * @returns {undefined}
 */
function parse_url_args() {
  var match = document.location.href.match(/cartelera\/[a-z]+\-\d+\/[a-zA-Z0-9\-]+(.*)/);
  if ( !match || match.length < 2)
    return;
  args = match[1].substr(1).split('/');

  reset_filters();
  
  $.each(args, function(i, arg) {
    arg = arg.split('-', 2);
    switch (arg[0]) {
      case 'hours':
        $hours.val(arg[1].split(','));
        break;

      case 'platinum':
        if ('1' == arg[1]) {
          set_platinum(true);
        }
        break;

      case 'filters':
        $.each(arg[1].split(','), function(i, filter) {
          $filters.filter('[value=' + filter + ']').prop('checked', true);
        });
        break;

      case 'langs':
        $.each(arg[1].split(','), function(i, filter) {
          $langs.filter('[value=' + filter + ']').prop('checked', true);
        });
        break;

      case 'date':
        $('#billboard-date').val(arg[1]);
        break;

      case 'movie':
        $('#billboard-movie').val(arg[1]);
        break;
    }
  });

  set_checks();
}

window.addEventListener('popstate', function(event) {
  parse_url_args();
  set_filters(true);
});

var queriering = false;


/**
 * Process the filters and display the results.
 * 
 * @param {Boolean} dont_push   In is true it won't call history.pushState()
 * @param {Boolean} dont_scroll If is not true, will scroll to the top before applying the filters.
 * @returns {Boolean}
 */
function set_filters(dont_push, dont_scroll) {
  $('#billboard-cinema-search').val('');
  
  if (queriering == true) {
    return false;
  }

  var times     = $hours.val().map(function(a){return parseFloat(a);});
  var movie_id  = $( '#billboard-movie' ).val();
  var $cinemas  = $('.billboard-block');
  var $movies   = $('.billboard-li');
  var $versions;
  var $ses;
  var $m_movies;
  var $m_versions;
  var $m_cinemas = $cinemas;
  var $current_cinema;

  queriering = true;

  var $main_col = $('.wide-main-col');

  if (!dont_scroll) {
    $main_col.fadeOut();
    $.scroll_to($main_col, -116, false);
  }
  
  // Determine which cinema is in viewport BEFORE applying the fitlers.
  $('.cinema-match').each(function() {
    if ( $(this).visible() ) {
      $current_cinema = $(this);
      return false;
    } else if ( $(this).visible(true) ) {
      $current_cinema = $(this);
    }
  });

  $('.first-child').removeClass('first-child');
  $('.no-movies').remove();

  // Platinum filter.
  $m_cinemas = Filters.platinum($cinemas);

  // Filter by movie.
  if ( movie_id ) {
    $m_movies = $movies.filter('[data-movie-id="' + movie_id + '"]');
  } else {
    $m_movies = $movies;
  }
  $versions = $m_movies.find('.mycinema-sessions-group');

  // Filter by format.
  $m_versions = Filters.formats($versions);

  // Filter by language.
  $m_versions = Filters.languages($m_versions);

  // Filter by time.
  $ses = $m_versions.find('.mycinema-sessions').children();
  $ses.hide().removeClass('match');
  $ses.filter( function(){
    var t = parseInt($(this).data('time'))/100;
    return t >= times[0] && t <= times[1];
  }).show().addClass('match');

  // Exclude versions without sessions in the selected time range.
  $m_versions = $m_versions.filter(function() {
    return $(this).find('.match').length > 0;
  });
  $versions.removeClass('match-version').hide();
  $m_versions.addClass('match-version').show();

  // Exclude movies without versions marching filters from the matching movies.
  $m_movies = $m_movies.filter(function() {
    return $(this).find('.match-version').length > 0;
  });
  $movies.removeClass('movie-match').hide();
  $m_movies.addClass('movie-match').show()
  ;

  $cinemas.removeClass('cinema-match first-child').hide();
  $m_cinemas = $m_cinemas.filter(function() {
    return $(this).find('.movie-match').length > 0;
  })
    .addClass('cinema-match').show()
    .first().addClass('first-child');

  $m_cinemas.find('.billboard-movies').each(function() {
    $(this).find('.movie-match').first().addClass('first-child');
  });

  $main_col.fadeIn();
  queriering = false;

  if ( !dont_push && ('pushState' in history) ) {
    history.pushState({},document.title,get_search_url());
  }

  if ( !$m_cinemas.length ) {
    if (!is_state || load_complete) {
      no_results();
    } else {
      load_page();
      return;
    }
  }
  
  // Update positions of fixed positioned elements and stuff.
  sticky_main.update_refs();
  sticky_main.trigger();
  sticky_side.update();

  // Determine the new current cinema in case of previous current cinema is not
  // in the results.
  if ( $('.no-movies').length ) {
    $current_cinema = $('.no-movies');
  } else if ( !$current_cinema || !$current_cinema.length || $current_cinema.css('display') === 'none' ) {
    $current_cinema = $('.cinema-match:first')
  }
  // Scroll to the current cinema.
  //$.scroll_to($current_cinema, -116, true);
  
  
  sticky_side.trigger();
}

var Filters = {
  platinum : function($cinemas) {
    if ( $('#platinum-bt').hasClass('pressed') ) {
      return $cinemas.filter('[data-platinum="1"]');
    }

    return $cinemas;
  },

  formats : function($versions) {
    var formats = $('[name=format-filter]:checked').map(function(){return '[data-' + this.value + ']';});
    if ( formats.length ) {
      return $versions.filter(formats.toArray().join(''));
    }

    return $versions;
  },

  languages : function($versions) {
    var formats = $('[name=lang-filter]:checked').map(function(){return this.value ? '[data-lang-' + this.value + ']' : '';});
    formats = formats.toArray().filter(function(a) { return !!a; });
    if ( formats.length ) {
      return $versions.filter(formats.join(''));
    }

    return $versions;
  }
};


function no_results() {
  $( '<div class="no-movies col"><p class="big-msg-msg">No hemos encontrado horarios<br>con el criterio de búsqueda ingresado.</p><p class="big-msg-msg">¡Inténtalo nuevamente modificando los filtros!</p></div>' ).appendTo('.wide-main-col');
}

function set_checks() {
  if( $platinum_bt.hasClass('pressed') )
    return;

  set_check($filters, true);

  if ( $filters.filter('[value=trad]:checked').length ) {
    set_check($filters.filter('[value=premium],[value=v4d]'), false);
  }

  if ( $filters.filter('[value=premium]:checked').length ) {
    set_check($filters.filter('[value=trad]'), false);
  }

  if ( $filters.filter('[value=premium],[value=cx],[value=v3d]').filter(':checked').length ) {
    set_check($filters.filter('[value=v4d]'), false);
  }

  if ( $filters.filter('[value=v4d]:checked').length ) {
    set_check($filters.filter('[value=trad],[value=premium],[value=cx],[value=v3d]'), false);
  }

  //
  // Disable filters by state
  //

  if (!current_state.hasPremium) {
    set_check($filters.filter('[value=premium]'), false);
  }

  if (!current_state.hasX4d) {
    set_check($filters.filter('[value=v4d]'), false);
  }

  if (!current_state.hasCx) {
    set_check($filters.filter('[value=cx]'), false);
  }
}

function set_check($el, enabled) {
  $el
      .prop('disabled', !enabled)
      .closest('.check-row')[enabled ? 'removeClass' : 'addClass']('disabled');

  if (!enabled) {
    $el.prop('checked', false);
  }
}

$(document).bind('user_init', function(e) {
  var selected_area_id, $cinema;
  
  if ( isset('user.preferred_cinema.id', user) ) {
    $cinema = $('#bb-cinema-' + e.user.preferred_cinema.id);
  } else {
    $cinema = [];
  }
  
  if ( $cinema.length ) {
    var preferred_cinema = get_cinema_by_id(e.user.preferred_cinema.id);
    var cinemas_in_view = cinemas.filter(function(a) {
      return $('#bb-cinema-' + a.id).length;
    });
    cinemas_in_view = cinemas_in_view.map(function(a) {
      var d = haversine(a, preferred_cinema);
      return {
        id : a.id,
        d  : d
      };
    })
    cinemas_in_view.sort(function(a, b) {
      if (a.id == preferred_cinema.id)
        return 1;
      else
        return b.d - a.d;
    });
    
    $.each(cinemas_in_view, function(i, cinema) {
      var $cinema = $('#bb-cinema-' + cinema.id);
      $cinema.insertBefore('.billboard-block:first');
    });
    
    sticky_main.update_refs();
    sticky_main.trigger();
  }
  
  
  if ( !area_id ) {
    return;
  }
  
  if ( isset('selected_area_id', e.user) ) {
    selected_area_id = e.user.selected_area_id;
  } else if ( isset('preferred_cinema', e.user) && isset('area', e.user.preferred_cinema) ) {
    selected_area_id = e.user.preferred_cinema.area.id;
  } else {
    return;
  }
  
  if ( area_id === selected_area_id ) {
    return;
  }
  
  setCookie('suggest_area', area_id, 10);
});

if ( area_id ) {
  $('#header-area-select').val('zona-' + area_id).trigger('change');
} else {
  $('#header-area-select').val('estado-' + state_id).trigger('change');
}


// AJAX stuff.
function load_page(args) {
  if (loading)
    return;
  
  if (!args) args = {};
  if ('function' === typeof args) {
    args = { cb : args };
  }
  
  var data = {
    state_id : state_id,
    page     : current_page,
    date     : $('#billboard-date').val()
  };
  var url;
      
  if ('single' in args && args.single === true) {
    url = url_prefix + 'partials/billboardCinema';
    data.cinema_id = args.cinema_id;
  } else {
    url = url_prefix + 'partials/billboardPage';
  }
  
  current_page++;
  
//  if ( isset('preferred_cinema', user) ) {
//    data.cinema_id = user.preferred_cinema.id;
//  }
  
  if ( isset('area_id') ) {
    data.area_id = area_id;
  }
  
  loading = true;
  $main.addClass('loading');
  
  $.ajax({
    url      : url,
    dataType : 'html',
    data     : data,
    success  : function(res) {
      $main.removeClass('loading');
      loading = false;
      
      if (res.length) {
        $main.append(res);
        bb_set_attributes();
        sticky_main.append('.billboard-block');
        $('.billboard-block').each(function() {
          var $id = $('[id="' + this.id + '"]');
          if ($id.length > 1 && $id[0] == this) {
            $(this).remove();
          }
        });
        reload_bind();
        set_filters(true, true);
        
        if ('cb' in args) {
          args.cb();
        }
        
        $(window).trigger('scroll.bb_load');
      } else {
        load_complete = true;
        if (!$('.cinema-match').length) {
          no_results();
        }
      }
    },
    error : function() {
      $main.removeClass('loading');
      loading = false;
      $.msg_popup( 'Ocurrió un error. Por favor, inténtalo nuevamente.', 'msg-error', true );
    }
  })
}

function reload_unbind() {
  $(window).unbind('scroll.bb_load');
}

function reload_bind() {
  $(window).bind('scroll.bb_load', function(e) {
    e.preventDefault();
    if ( $(this).scrollTop() + $(this).height() > $main.position().top + $main.outerHeight() ) {
      reload_unbind();
      load_page();
    }
  });
}
if ( is_state ) {
  reload_bind();
}

// Search by Cinema name input.
var ac_data = cinemas.filter(
  is_state
  ? function(a) { return a.state.id == state_id; }
  : function(a) { return a.area.id  == area_id;  }
).map(function(a){ return {q:a.name, v:a.id}; });

var ac_cb = function($a) {
  var $cinema = $('#bb-cinema-' + $a.data('value'));
  if (is_state && !$cinema.length && !load_complete) {
    load_page({
      cb : function() { ac_cb($a); },
      single : true,
      cinema_id : $a.data('value')
    });
    $('body,html').animate({scrollTop:$main.position().top + $main.outerHeight()});
    return;
  }
  if (!$cinema.length || !$cinema.hasClass('cinema-match')) {
    $.msg_popup('No hemos encontrado horarios con el criterio de búsqueda ingresado en el complejo ' + $a.text());
    return;
  }
  $.scroll_to($cinema, -120);
};

$('#billboard-cinema-search').val('').autocomplete({
  autoposition : true,
  data         : ac_data,
  callback     : ac_cb,
  div_class    : 'std-field-ac'
});

$main.delegate('.bb-scroll-top', 'click', function(e) {
  e.preventDefault();
  $.scroll_to($main, -120);
});

$('#bb-reset-bt').bind('click', function(e) {
  e.preventDefault();
  reset_filters();
  set_filters();
});