<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

class PurgeUrl implements UrlTaskInterface {

  protected $resources;

  public function addUrlResource($path, $env) {
    $this->resources[] = ['path' => $path, 'env' => $env];
  }

  public function getUrlResources() {
    return $this->resources;
  }

}