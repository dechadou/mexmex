<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityPromoIE extends AbstractEntityTask implements UrlTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('ie_promos'),
            'env'  => 'front'
        ],
        [
            'path' => $this->frontUtils->get_cms_url($this->entity->getThumb(), '300x393'),
            'env'  => 'static'
        ],
        [
            'path' => $this->frontUtils->get_cms_url($this->entity->getImage()),
            'env'  => 'static'
        ],
    ];
  }

}