<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Webservice' );
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Importar películas
      </header>

      <div class="panel-body">
        <div id="status" class="alert alert-info">Haz click en "Buscar películas" para comenzar.</div>
        <a href="#" class="btn btn-primary" id="query-btn">Buscar películas</a>
        <div class="form-horizontal tasi-form" id="movies-table">
          <a href="#" class="btn btn-primary" id="import-btn" style="display: none;">Importar películas</a>
        </div>
      </div>
    </section>
  </div>
</div>
<script>
  var running = false, current_page = 0, ids = [], movies = [];
  var cinemas_count = <?php echo $cinemas_count; ?>;
  var pages = cinemas_count / 5;
  
  $('#query-btn').on('click', function(e) {
    e.preventDefault();
    
    if (running) {
      return;
    }
    
    $(this).hide();
    start_loading();
  });
  
  $('#import-btn').on('click', function(e) {
    e.preventDefault();
    
    if (running) {
      return;
    }
    
    $(this).hide();
    import_movies();
  });
  
  function start_loading() {
    movies = [];
    ids = [];
    current_page = 0;
    next_page();
  }
  
  function next_page() {
    current_page++;
        $('#status').text('Consultando carteleras... ' + Math.round((current_page-1)/pages*100) + '%');
    $.ajax({
      url : '<?php echo $view['router']->generate('social_snack_admin_ws_import_movies_ids'); ?>',
      data : {
        page : current_page
      },
      type : 'post',
      dataType : 'json',
      success : function(res) {
        if ('ids' in res) {
          ids = ids.concat(res.ids);
          next_page();
        } else {
          load_ids_complete();
        }
      },
      error : function() {
        alert('Ocurrió un error');
        load_ids_complete();
      }
    });
  }
  
  function load_ids_complete() {
    ids = $.unique(ids);
    ids = ids.reduce(function(prev, curr, i, arr) {curr = curr.split('|'); console.log(curr);prev[curr[0]] = curr[1]; return prev;},[]);
    
    if (!ids.length) {
      $('#status').text('No se encontraron películas.');
      $('#query-btn').show();
      running = false;
    }
    
    load_movies();
  }
  
  function load_movies() {
    $('#status').text('Consultando info películas...');
    $.ajax({
      url : '<?php echo $view['router']->generate('social_snack_admin_ws_import_movies_info'); ?>',
      data : {
        ids : Object.keys(ids)
      },
      type : 'post',
      dataType : 'json',
      success : function(res) {
        $('#status').text('Consulta completada.');
        running = false;
        if ('movies' in res) {
          movies = res.movies;
          $.each(res.movies, function(i, movie) {
            var $div = $('<div/>', {'class' : 'form-group'});
            var $label = $('<label/>', {'class' : 'col-sm-6 control-label', 'text' : movie.name});
            var $check = $('<input/>', {type : 'checkbox', value : movie.id, data : { key : ids[movie.id] }, checked : movie.c});
            var $span = $('<span/>', {'class' : 'col-sm-2', text : (movie.c ? 'Con info' : 'Sin info')});
            $label.appendTo($div);
            $check.appendTo($div);
            $span.appendTo($div);
            $div.prependTo('#movies-table');
          });
          if (res.movies.length) {
            $('#import-btn').show();
          }
        }
      },
      error : function() {
        running = false;
      }
    });
  }
  
  function import_movies() {
    $('#status').text('Importando películas...');
    import_next();
  }
  
  function import_next() {
    var $chk = $('#movies-table').find('input:checkbox:checked:first');
    
    if (!$chk.length) {
      group_movies();
      return;
    }
    
    $('#status').text('Importando ' + $chk.parent().find('label').text() + '...');
    
    $.ajax({
      url : '<?php echo $view['router']->generate('social_snack_admin_ws_import_movies_process'); ?>',
      data : {
        id : $chk.val(),
        key : $chk.data('key')
      },
      type : 'post',
      dataType : 'json',
      success : function(res) {
        $chk.remove();
        import_next();
      },
      error : function() {
        $chk.removeProp('checked');
        import_next();
      }
    });
  }
  
  function group_movies() {
    $('#status').text('Agrupando películas...');
    $.ajax({
      url : '<?php echo $view['router']->generate('social_snack_admin_ws_group_movies'); ?>',
      type : 'post',
      dataType : 'json',
      success : function(res) {
        $('#status').text('Listo!');
      },
      error : function() {
        $('#status').text('Error');
      }
    });
  }
  
  jQuery(document).ready(function($) {

  });
</script>
<?php $view['slots']->stop(); ?>