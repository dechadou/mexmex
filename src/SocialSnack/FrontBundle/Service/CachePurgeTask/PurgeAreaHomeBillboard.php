<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Symfony\Component\Routing\Router;

class PurgeAreaHomeBillboard implements UrlTaskInterface {

  protected $router;

  protected $areaId;

  public function __construct(Router $router) {
    $this->router   = $router;
  }


  public function setAreaId($areaId) {
    $this->areaId = $areaId;
  }


  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('esi_movies_in_area', ['area_id' => $this->areaId]),
            'env'  => 'front'
        ],
    ];
  }

}