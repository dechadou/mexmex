<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData;

/**
 * Class SessionsControllerTest
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class SessionsControllerTest extends WebTestCase {


  public function testSingleAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ));

    $client = static::createClient();
    $route  = sprintf('/rest/v2/sessions/%d', LoadSessionsData::$sessions[0]->getId());
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $model = [
        'id'             => 'integer',
        'date'           => 'integer',
        'tz_offset'      => 'integer',
        'cinema_id'      => 'integer',
        'premium'        => 'boolean',
        'extreme'        => 'boolean',
        'seatallocation' => 'boolean',
        'tickets'        => 'array',
        'screen_number'  => 'integer',
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  public function testSingleActionNotFound() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
    ));

    $client = static::createClient();
    $route  = sprintf('/rest/v2/sessions/%d', 666);
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertEquals(404, $client->getResponse()->getStatusCode());
  }

} 