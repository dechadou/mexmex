/////////////////
// BEGIN Checkout
/////////////////

var selected_seats = [],
    timer          = null,
    timedout       = false,
		silent_timeout = false,
		working        = false,
		$order_info    = $( '#order-info, #success-order-info' ),
		$trail         = $( '#checkout-trail' ).children(),
		seats_layout   = null,
    $process_div   = $( '#checkout-process' )
;

$( '.checkout-form' ).bind( 'submit', function( e ) {
	e.preventDefault();

	if ( working )
		return;

	var $form = $( this ),
	    step  = parseInt( $form.data( 'step' ) ),
			$bt   = $form.find( 'button[type=submit]' );

	switch ( step ) {
		case 1:
			step1_submit();
			break;

		case 2:
			step2_submit();
			break;

		case 3:
			step3_submit();
			break;
	}


	/**
	 * Submit step 1.
	 *
	 * @returns {void}
	 */
	function step1_submit() {

		// Make sure at least one ticket was requested and no more than 10.
		var tickets_num = 0;
		$form.find( 'input[type=number]' ).each( function() {
			tickets_num += parseInt( $( this ).val() );
		} );
		if ( tickets_num < 1 ) {
			step_error( 1, { message : 'Por favor, selecciona al menos un boleto.' } );
			return;
		}
		if ( tickets_num > 10 ) {
			step_error( 1, { message : 'Lo sentimos, el total de boletos por orden no puede ser superior a 10.' } );
			return;
		}


		// Pass requested tickets to step 3 form because this information is needed
		// to complete the checkout process.
		var $form3 = $( '#checkout-step-3-form' );
		$( '#checkout-step-3-form input[name^=tickets]' ).remove();
		$( '#checkout-step-1-form input[name^=tickets]' ).each( function() {
			var $t = $( this );
			$( '<input/>', { type : 'hidden', name : $t.attr( 'name' ), val : $t.val() } ).appendTo( $form3 );
		} );

		$process_div.blockui();
		$bt.addClass( 'btn-loading' );
		working = true;
		var data = $form.serialize();
		disableStepForm(1);

		CMX.post( 'buy/selectTickets', data, step1_success, step1_error );
	}

  function step1_error( jqxhr, status, error ) { step_error( 1, jqxhr.responseJSON || false ); }

	function step1_success_noalloc() {
    next_checkout_step();

		step1_add_process_info();
	}

	function step1_success( res ) {
		$bt.removeClass( 'btn-loading' );
    $process_div.unblockui();
		working = false;
		next_checkout_step();

		step1_add_process_info();

		// If no seat allocation allowed skip seats step.
		if ( !seatallocationallowed ) {
			return;
		}

		$( 'input[name=transaction_id]' ).val( res.transaction_id );


		// Build seats layout.
		if ( res.layout ) {
			build_seats_layout(res.layout, res.seats);
		}

		// Start the timer.
		if ( res.timeout_time ) {
			timedout = false;
			$( '#checkout-timeout' ).show();
			start_countdown( res.timeout_time, timer_out );
		}
	}


	/**
	 * Display the selected tickets in the sidebar.
	 *
	 * @returns {undefined}
	 */
	function step1_add_process_info() {
		$( '#side-total .number' ).text( $( '#checkout-step-1-form .qty-total-row .number' ).text() );
		$( '#side-total' ).fadeIn();

		var tickets_str = [],
				$tickets    = $( '#checkout-step-1-form .qty-selection .qty-row' );

		$tickets.each( function() {
			var $t   = $( this ),
			    text = $t.find( '.ticket-label' ).text(),
			    num  = parseInt( $t.find( 'input[type=number]' ).val() )
			;
			if ( num ) {
				tickets_str.push( num + ' ' + text );
			}
		} );

		$order_info.find( '.step-1, .step-2, .step-3' ).remove();

		if ( tickets_str.length ) {
			$( '<dt/>', { text : 'Tus boletos', 'class' : 'step-1 dt-row' } ).appendTo( $order_info );
			$( '<dd/>', { html : tickets_str.join( '<br/>' ), 'class' : 'step-1' } ).appendTo( $order_info );
		}
	}


	function step_error( step, res ) {
		if ( silent_timeout && timedout ) {
			silent_timeout = false;
			timer_out();
		}

		var msg;

    $process_div.unblockui();
		$bt.removeClass( 'btn-loading' );
		working = false;
		enableStepForm(step);

		if ( 3 == step ) {
			silent_timeout = false;
		}

    // Handle both errorcode and error properties for legacy compatibility.
    var errorcode = null;
		if ( res.errorcode ) {
      errorcode = res.errorcode;
    } else if ( res.error ) {
      errorcode = res.error;
    }

    switch ( errorcode ) {
      case 'seats-non-available':
        msg = 'Lo sentimos, los asientos seleccionados ya fueron ocupados.';
        break;

      case 'no-tickets':
        msg = 'Lo sentimos, esta función ya está agotada, por favor <a href="history.back(-1);">verifica otro horario o cine de tu preferencia</a>.';
        break;

      case 'captcha-failed':
        msg = 'El código de verificación que ingresaste no es correcto. Por favor, inténtalo nuevamente.';
        break;

      // Default error message.
      default:
        if ( res && isset('message', res) ) {
          msg = res.message;
        } else if ( 3 == step ) {
          msg = 'Tu banco rechazó la transacción. Por favor, ponte en contacto con ellos para conocer los detalles.';
        } else {
          msg = 'Ha ocurrido un error al ejecutar este proceso. Por favor, inténtalo nuevamente.';
        }
        break;
    }

    $.msg_popup( msg, 'msg-error', true );
	}


	/**
	 * Submit step 2.
	 * Make sure the timer hasn't run out of time.
	 * Make sure the selected seats are valid.
	 *
	 * @returns {unresolved}
	 */
	function step2_submit() {
		// Make sure the timer hasn't run out of time.
		if ( timedout ) {
			step_error( 2, 'Tu transacción ha expirado. Por favor, comienza nuevamente.' );
			/** @todo Redirect to step1 or refresh the page, etc. */
			return;
		}

		// Make sure the selected seats are valid.
		if ( !validate_selected_seats() )
			return;

    $process_div.blockui();
		$bt.addClass( 'btn-loading' );
		working = true;
		var data = $form.serialize();
		disableStepForm(2);

		CMX.post('buy/selectSeats', data + get_seats_query_string(), step2_success, step2_error);
	}

  function step2_error( jqxhr, status, error ) {
    var res = jqxhr.responseJSON;

    // Build seats layout.
    if (isset('layout', res) && 'seats-non-available' === res.errorcode) {
      build_seats_layout(res.layout, res.seats);
    }

    step_error( 2, res || false );
  }

	function step2_success( res ) {
		$bt.removeClass( 'btn-loading' );
    $process_div.unblockui();
		working = false;
		next_checkout_step();

		var tickets_str = [];
		$order_info.find( '.step-2, .step-3' ).remove();

		$.each( selected_seats, function( i, $seat ) {
			tickets_str.push( $seat.parent().parent().data( 'name' ) + $seat.parent().data( 'seat' ) );
		} );

//		if ( tickets_str.length ) {
//			tickets_str.sort();
//			$( '<dt/>', { text : 'Asientos seleccionados', 'class' : 'step-2 dt-row' } ).appendTo( $order_info );
//			$( '<dd/>', { html : tickets_str.join( ' / ' ), 'class' : 'step-2' } ).appendTo( $order_info );
//		}
	}

  /**
   * Submit step 3.
   * Validate form fields.
   * Attempt to complete the transaction.
   *
   * @returns {Boolean}
   */
	function step3_submit() {
		var ccnum  = $( '#checkout-cc' ).val(),
		    csc    = $( '#checkout-csc' ).val(),
				exp_m  = $( '#checkout-exp-month' ).val(),
				exp_y  = $( '#checkout-exp-year' ).val()
		;

		// Validate card holder name.
		if ( !$( '#checkout-name' ).run_validators() ) {
			step_error( 3, { message : 'Por favor, ingresa el nombre completo del titular de la tarjeta.' } );
			return false;
		}

		// Validate email address.
		if ( !$( '#checkout-email' ).run_validators() ) {
			step_error( 3, { message : 'Por favor, ingresa un email válido.' } );
			return false;
		}

		// Validate CC number.
		if ( !$( '#checkout-cc' ).run_validators() ) {
			step_error( 3, { message : 'Por favor, verifica el número de tarjeta ingresado.' } );
			return false;
		}

    // Make sure the supplied card ain't a debit card.
    if ( validator_fns.debit_card( $( '#checkout-cc' ).val() ) ) {
      if ( isset('ga') ) {
//        _gaq.push(['_trackEvent', 'Checkout attempt', 'Debit card']);
        ga('send', 'event', 'Checkout attempt', 'Debit card');
      }
      step_error( 3, { message : 'El banco emisor de tu tarjeta no autoriza ésta transacción con tarjeta de débito, por favor ingresa una tarjeta de crédito.' } );
			return false;
    }

		// Validate CSC number.
		if ( !$( '#checkout-csc' ).run_validators() ) {
			step_error( 3, { message : 'Por favor, verifica el código de seguridad ingresado.' } );
			return false;
		}

		// Validate expiration date.
		if ( !$( '#checkout-exp-month, #checkout-exp-year' ).run_validators() ) {
			step_error( 3, { message : 'Por favor, verifica la fecha de vencimiento de la tarjeta.' } );
			return false;
		}

    if ( $('.checkout-sms-edit').is(':checked') && ( $('#checkout-sms-number').val().length != 12 || !$('#checkout-sms-carrier').val() ) ) {
      step_error( 3, { message : 'Por favor, verifica el número de celular ingresado y asegúrate de seleccionar una operadora.' } );
      return false;
    }

    $process_div.blockui();
		$bt.addClass( 'btn-loading' );
		working = true;
		silent_timeout = true;
		var data = $form.serialize();
		disableStepForm(3);

		CMX.post('buy/complete', data + get_seats_query_string(), step3_success, step3_error);
	}

  function step3_error( jqxhr, status, error ) {
    if (jqxhr.responseJSON) {
      $('#checkout-captcha-input').val('');
      if (jqxhr.responseJSON.captcha) {
        reload_captcha();
      } else {
        $('#checkout-captcha-row').hide();
      }
    }
    step_error( 3, jqxhr.responseJSON || false );
  }

	function step3_success( res ) {
		$bt.removeClass( 'btn-loading' );
    $process_div.unblockui();
		working = false;
		next_checkout_step();

    $( '#checkout-confirmation' ).html( res.html );
    $( '#checkout-timeout' ).hide();

    stop_countdown();
    ga_checkout( res.id );
	}

  /**
   * Build the query string with requested seats information to pass as form data.
   *
   * @returns {String}
   */
	function get_seats_query_string() {
		var seats_str = '';
		$.each( selected_seats, function( i, $seat ) {
			seats_str += '&seats[' + i + '][row_name]=' + $seat.parent().parent().data( 'name' );
			seats_str += '&seats[' + i + '][row]=' + $seat.parent().parent().data( 'row' );
			seats_str += '&seats[' + i + '][seat]=' + $seat.parent().data( 'seat' );
		} );
		return seats_str;
	}

	/**
	 * Check that the selection does't leave one empty seat alone between the
	 * selected seats and the previously occupied seats.
	 *
	 * @returns {Boolean}
	 */
	function validate_selected_seats() {
		var invalid = false;

		$.each( selected_seats, function( i, seat ) {
			var $parent  = seat.parent(),
					$prev    = $parent.prev(),
					$prev2   = $prev.prev(),
					$prev_s  = $prev.find('span'),
					$prev2_s = $prev2.find('span'),
					$next    = $parent.next(),
					$next2   = $next.next(),
					$next_s  = $next.find('span'),
					$next2_s = $next2.find('span')
			;

			// Check if there's only one seat free at any side of the selected seat.
			if (
					( $prev_s.hasClass('seat-0') && ( $prev2_s.hasClass('seat-1') || $prev2_s.hasClass('seat-selected') ) )
					||
					( $next_s.hasClass('seat-0') && ( $next2_s.hasClass('seat-1') || $next2_s.hasClass('seat-selected') ) )
			) {
				invalid = true;
			}

			// The only way one seat left alone is allowed is when there's no more room
			// en the theater than the number of requested tickets + 1.
			if ( invalid && ( $( '#seats-layout' ).find( '.seat.seat-0' ).length > selected_seats.length + 1 ) ) {
				return false;
			}
		} );

		if ( invalid ) {
			step_error( 2, { message : 'No puedes dejar sólo un asiento libre entre los asientos ocupados y los que has seleccionado.' } );
			return false;
		}

		return true;
	}


	function timer_out() {
		timedout = true;

		if ( silent_timeout )
			return;

		$process_div.hide();
    $( '.msg-timeout' ).fadeIn();
    $.scroll_to( $( '.msg-timeout' ) );

    CMX.post('/buy/invalidateTransaction', {
			transaction_id : $('input[name="transaction_id"]').val(),
			session_id : $('input[name="session_id"]').val()
		});
	}

} );


$( '#seats-layout' ).delegate( '.seat-0', 'click', function(e) {
	if ( timedout )
		return;

	var $old_seat = selected_seats.shift()
	    $new_seat = $( this );

	if ( $new_seat.hasClass( 'seat-selected' ) )
		return false;

	selected_seats.push( $new_seat );

	$old_seat.removeClass( 'seat-selected' ).addClass( 'seat-0' );
	$old_seat.parent().data( { selected : 0 } );
	$new_seat.removeClass( 'seat-0 seat-E' ).addClass( 'seat-selected' );
	$new_seat.parent().data( { selected : 1 } );
} );


/**
 * Timer countdown.
 *
 * @param {integer}  timeout Time out time in minutes.
 * @param {function} cb      Callback function.
 * @returns {undefined}
 */
function start_countdown( timeout, cb ) {
	// Convert minutes to seconds.
	timeout *= 60;

	timer = setInterval( function() {

		var min = Math.floor( timeout / 60 ),
		    sec = timeout % 60;

		// Update the clock.
		$( '#timeout' ).html(
				( min < 10 ? '0' : '' ) + min +
				'<span class="sep">:</span>' +
				( sec < 10 ? '0' : '' ) + sec
		);

		// Countdown finished?
		if ( 0 == timeout ) {
			stop_countdown();

			if ( cb )
				cb();

			return;
		}

		// Decrease time.
		timeout--;

	}, 1000 );
}


/**
 * Stop the countdown timer.
 *
 * @returns {undefined}
 */
function stop_countdown() {
	if ( timer ) {
		clearInterval( timer );
		timer = null;
	}
}


$('.qty-q').integersOnly();

$( '.qty-selection' )
	.delegate('.qty-q', 'keyup change', function(e) {
		checkout_sum_qtys($(this).closest('.qty-selection'));
	})
	.delegate('.qty-q', 'focus', function(e) {
		var $t = $(this);
		$t.select();
		$t.parent().addClass('focus')
	})
	.delegate('.qty-q', 'blur', function(e) {
		var $t = $(this);
		var v = $t.val();
		if (!v.length) {
			v = '0';
			$t.val('0');
		}
		//v = parseInt(v);
		//var max = parseInt($t.attr('max'));
		//var min = parseInt($t.attr('min'));
    //
		//if (max && v > max) {
		//	$t.val(max);
		//}
		//if (v < min) {
		//	$t.val(min);
		//}
		$t.parent().removeClass('focus');
	});

$('.qty-selection').each (function() {
	checkout_sum_qtys($(this));
});

/**
 * Display the sum of quantities of each kind of ticket.
 *
 * @param {type} $table
 * @returns {undefined}
 */
function checkout_sum_qtys( $table ) {
	var total = 0,
	    str   = '';

	$table.find( '.qty-q' ).each( function( e ) {
		var $t    = $( this ),
				price = $t.data( 'price' ),
				qty   = Math.floor($t.val())
		;

		if ( isNaN( price ) || isNaN( qty ) )
			return;

		total += ( qty * price );
	} );

	if ( total ) {
		str = total + '';
	} else {
		str = '000';
	}

	$table.find( '.qty-total-row .number' ).text( str.slice( 0, str.length - 2 ) + '.' + str.slice( str.length - 2 ) );
}


/**
 * Build the seats layout DOM.
 *
 * @param layout
 * @param seats
 */
function build_seats_layout(layout, seats) {
  var $table = $( '#seats-layout' );

  // @todo Why do I keep this variable? :S
  seats_layout = layout;

  // Start from scratch.
  $table.children().remove();

  // Build the seats layout DOM.
  for ( var row_num in layout ) {
    var row  = layout[row_num],
        $row = $( '<tr/>', { data : { row : row_num, name : row.name } } );


    for ( var seat_num in row.seats ) {
      var status = row.seats[seat_num];
      var $td = $( '<td/>', {
        'class' : 'seat-' + row_num + '-' + seat_num,
        data    : { seat : seat_num, status : status }
      } );
      $( '<span/>', { 'class' : 'seat seat-' + status + ( !row.name ? ' seat-EE' : '' ) } ).appendTo( $td );
      $td.prependTo( $row );
    }

    $( '<td/>', { text : row.name, 'class' : 'row-name first' } ).prependTo( $row );
    $( '<td/>', { text : row.name, 'class' : 'row-name last' } ).appendTo( $row );

    $row.prependTo( $table );
  }

  // Set selected seats.
  selected_seats = [];
  $.each( seats, function( i, seat ) {
    var $seat = $( '.seat-' + seat.row + '-' + seat.seat ).find( 'span' );
    selected_seats.push( $seat );
    $seat.removeClass( 'seat-0 seat-1 seat-E' ).addClass( 'seat-selected' );
    $seat.parent().data( { selected : 1 } );
  } );

  // Enable layout scroll.
  scroll_theater();
}


/**
 * Build the scroll arrows and stuff to pan large theater rooms.
 * Bind click events for scroll arrows.
 *
 * @returns {undefined}
 */
function scroll_theater() {
  var $theater = $( '#theater-layout' ),
      $layout  = $( '#seats-layout' );
  set_h_scroll($layout, $theater, 'seats-scroll-bt', 'seats-shadow');
  $(window)
    .unbind('resize',scroll_theater)
    .bind('resize',scroll_theater);
}


$( '[rel="checkout-back"]' ).bind( 'click', function( e ) {
  e.preventDefault();
  next_checkout_step( true );
} );

$( '#checkout-step-1' ).data(
  'display_cb', function() {
    stop_countdown();
    $( '#checkout-timeout' ).hide();
  }
);

// First captcha display.
if ($('#checkout-captcha-row').data('captcha-load')) {
  $( '#checkout-step-3' ).data(
      'display_cb', function() {
        reload_captcha();
        $( '#checkout-step-3' ).data('display_cb', null);
      }
  );
}

/**
 * Move forward to the next (or previous) checkout step.
 *
 * @param {boolean} prev If true, move backwards.
 * @returns {undefined}
 */
function next_checkout_step( prev ) {
  var $curr = $( '.checkout-step.selected' ),
      $next = $curr[ prev ? 'prevAll' : 'nextAll' ]( '.checkout-step:first' ),
      $curr_t = $trail.filter( '.selected' ),
      $next_t = $curr_t[ prev ? 'prev' : 'next' ](),
      next_id = $next.attr('id'),
			nextStepN = next_id.split('-').pop()
  ;

  $trail
    .removeClass('done');

  $next_t
    .prevAll()
    .addClass('done');

  $curr.hide().removeClass( 'selected' );
  $next.show().addClass( 'selected' );

  $curr_t.removeClass( 'selected' );
  $next_t.addClass( 'selected' );

  $.scroll_to( $( '#checkout-trail' ) );

  // If the target step has a "display_cb" data attribute, trigger the callback.
  if (fn = $next.data( 'display_cb' )) {
    fn();
  }

	enableStepForm(nextStepN);

  // Track virtual page view in Google Analytics.
  if ( next_id && ga ) {
    ga('send', 'pageview', document.location.pathname + '/' + next_id);
  }
}


/**
 * Disables form fields.
 * Useful to make sure a form remains unaltered during submit or after the user has move to the next steps.
 *
 * @param step
 */
function disableStepForm(step) {
	var $form = $('#checkout-step-' + step + '-form');
	$form.find('input, select').prop('disabled', true);
}


/**
 * Enables form fields.
 *
 * @param step
 */
function enableStepForm(step) {
	var $form = $('#checkout-step-' + step + '-form');
	$form.find('input, select').prop('disabled', false);
}


/**
 * Track the transaction with Google Analytics.
 *
 * @param {string|integer} trans_id Transaction ID.
 * @returns {undefined}
 */
function ga_checkout( trans_id ) {
  if ( !isset('ga') )
    return false;

  var total    = $( '#checkout-step-1-form .qty-total-row .number' ).text(),
      $tickets = $( '#checkout-step-1-form .qty-selection .qty-row' ),
      cinema   = get_cinema_by_id(checkout_cinema_id),
      area     = get_area_by_id(checkout_area_id),
      state    = get_state_by_id(checkout_state_id)
  ;

//  _gaq.push(['_addTrans',
//    trans_id,       // transaction ID - required
//    cinema.name,    // affiliation or store name
//    total,          // total - required
//    '0',            // tax
//    '0',            // shipping
//    area.name,      // city
//    state.name,     // state or province
//    'México'        // country
//  ]);
  ga('ecommerce:addTransaction', {
    'id': trans_id,               // Transaction ID. Required
    'affiliation': cinema.name,   // Affiliation or store name
    'revenue': total,             // Grand Total
    'shipping': 0,                // Shipping
    'tax': 0                      // Tax
  });


  $tickets.each( function() {
    var $t     = $( this ),
        text   = $t.find( '.ticket-label' ).text(),
        $qty   = $t.find( '.qty-q' ),
        qty    = parseInt( $qty.val() ),
        uprice = $qty.data( 'price' ) + '',
        type   = $t.find( '.ticket-type' ).val()
    ;
    if ( !qty )
      return;

    uprice = uprice.substr( 0, uprice.length - 2 ) + '.' + uprice.substr( uprice.length - 2 );

//    _gaq.push(['_addItem',
//      trans_id,         // transaction ID - required
//      type,             // SKU/code - required
//      'Boleto',         // product name
//      text,             // category or variation
//      uprice,           // unit price - required
//      qty               // quantity - required
//    ]);
    ga('ecommerce:addItem', {
      'id': trans_id,         // Transaction ID. Required
      'name': 'Boleto',       // Product name. Required
      'sku': type,            // SKU/code
      'category': text,       // Category or variation
      'price': uprice,        // Unit price
      'quantity': qty         // Quantity
    });
  } );

//  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
  ga('ecommerce:send');
}

// Reload captcha
function reload_captcha() {
  $('#checkout-captcha-row').show();
  $('#checkout-captcha-fig').children('img').unbind('load').remove();
  $('<img/>', {src : url_prefix + 'captcha?' + new Date().getTime()}).hide().appendTo('#checkout-captcha-fig').bind('load', function() {
    $(this).fadeIn();
  });
}
jQuery(document).ready(function($) {
  $('.captcha-reload').bind('click', reload_captcha);
});

// SMS
var $smsFields = $('.checkout-sms-fields');

if (jQuery.fn.mask) {
	$('#checkout-sms-number').mask('99-9999-9999');
}

$('body').on('change', '.checkout-sms-edit', function(ev){
    ev.preventDefault();

    $(this).is(':checked') && $smsFields.fadeIn() || $smsFields.fadeOut();
});

$('.checkout-sms-edit').trigger('change');

$('#checkout-reload-bt').on('click', function(e) {
	e.preventDefault();
	document.location.reload();
});
///////////////
// END Checkout
///////////////
