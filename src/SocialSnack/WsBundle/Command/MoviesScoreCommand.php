<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\MovieInfo;
use SocialSnack\WsBundle\Request\ConsultaDetallada;

class MoviesScoreCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:movies:score' )
			->setDescription( 'Recalculate movies score' );
	}
	
	
	/**
	 * @param \Symfony\Component\Console\Input\InputInterface $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 * @return type
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$doctrine   = $this->getContainer()->get( 'doctrine' );
    $conn       = $doctrine->getConnection();
    $sql        = "UPDATE Movie m, MovieVote mvv
        SET m.score = ( SELECT ROUND(AVG(mv.score),2) FROM MovieVote mv WHERE mv.movie_id = m.id )
        WHERE m.id = mvv.movie_id;";
    $stmt       = $conn->prepare($sql);
    $stmt->execute();
    
		$output->writeln( 'Didn\'t fail, yay!' );
		return;
	}
	
	
}
