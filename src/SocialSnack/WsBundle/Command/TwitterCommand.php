<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Twitter;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class TwitterCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:twitter' )
			->setDescription( 'Fetch tweets from Twitter API' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$em           = $doctrine->getManager();
    $conn         = $doctrine->getConnection();
    $repo         = $doctrine->getRepository( 'SocialSnackWsBundle:Twitter' );
    $twitter      = $this->getContainer()->getParameter( 'twitter' );
    $access_token = $twitter['access_token'];
    $url          = 'https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=cinemex&count=10';
    $ch           = curl_init( $url );
    $count        = 0;
    
		$query = "SELECT t.legacy_id FROM Twitter t ORDER BY id DESC LIMIT 1";
    $stmt  = $conn->prepare( $query );
    $stmt->execute();
    $max   = $stmt->fetchColumn();
    
    if ( $max ) {
      $output->writeln( 'Since ID: ' . $max );
      $url .= '&since_id=' . $max;
    }
    
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $ch, CURLOPT_HTTPHEADER,     array( 'Authorization: Bearer ' . $access_token ) );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 20 );
		curl_setopt( $ch, CURLOPT_TIMEOUT,        20 );
		$res = curl_exec( $ch );

		if ( !$res ) {
			throw new \Exception( curl_error( $ch ) );
		}
    
    $json = json_decode( $res );
    
    foreach ( array_reverse( $json ) as $item ) {
      $exists = $repo->findOneBy( array( 'legacy_id' => $item->id_str ) );
      if ( $exists )
        continue;
      $tweet = new Twitter();
      $tweet->setLegacyId( $item->id_str );
      $tweet->setScreenName( $item->user->screen_name );
      $tweet->setText( $item->text );
      $tweet->setThumb( $item->user->profile_image_url_https );
      $tweet->setUserName( $item->user->name );
      $tweet->setData( json_encode( $item ) );
      $em->persist( $tweet );
      $count++;
    }
    
    $em->flush();
    
    $output->writeln( sprintf( '%d new tweets.', $count ) );
  }
  
}