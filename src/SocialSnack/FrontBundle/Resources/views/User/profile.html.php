<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:User:base.html.php');
$view['slots']->set( 'title', 'Mis datos - Cinemex' );
$view['slots']->start( 'user_body' );
?>
<div class="content-box">
	<h1 class="content-box-title">Mis datos</h1>
	
	<div class="info-heading">
		<a href="<?php echo $view['router']->generate( 'user_profile_edit' ); ?>">Modificar</a>
	</div>
	
	<?php
	echo $view->render(
			'SocialSnackFrontBundle::flashMsgs.html.php',
			array( 'flash_msgs' => $flash_msgs )
	);
	?>
	
	<div id="user-profile" class="clearfix">
    <div class="avatar-64">
      <img src="<?php echo $view['fronthelper']->get_avatar_url( $user->getAvatar(), '64x64' ); ?>" alt="" width="64" height="64" />
    </div>

		<dl class="user-info-block">
			<dt>Nombre</dt>
			<dd><?php echo $user->getFullName(); ?></dd>
		</dl>
		<dl class="user-info-block">
			<dt>Email</dt>
			<dd><?php echo $user->getEmail(); ?></dd>
		</dl>
		<dl class="user-info-block">	
			<dt>Contraseña</dt>
			<dd>**********</dd>	
		</dl>
	</div><!-- #user-profile -->
	
</div>
<?php
$view['slots']->stop();
?>