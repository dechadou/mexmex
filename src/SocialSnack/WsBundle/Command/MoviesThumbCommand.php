<?php
namespace SocialSnack\WsBundle\Command;

use SocialSnack\FrontBundle\Service\Utils;

class MoviesThumbCommand extends AbstractThumbRegenCommand {

  protected function configure() {
    $this
        ->setName('cinemex:movies:generate_thumbs')
        ->setDescription('Regenerate movie posters thumbnails')
    ;
  }


  protected function getFiles() {
    $groups = [];

    // current active movies
    $repo = $this->getContainer()
        ->get('doctrine')
        ->getRepository('SocialSnackWsBundle:Movie');

    $groups[] = $repo->findAll();

    // coming movies
    $repo = $this->getContainer()
        ->get('doctrine')
        ->getRepository('SocialSnackWsBundle:MovieComing');

    $groups[] = $repo->findFurther();
    $groups[] = $repo->findNextFriday();

    unset($repo);

    // go ahead!
    $files = [];
    foreach ($groups as $movies) {
      foreach ($movies as $movie) {
        $files[] = $movie->getPosterUrl();
      }
    }

    $files = array_unique($files);

    return $files;
  }


  protected function getAvailableSizes() {
    return Utils::$poster_sizes;
  }


  protected function process($files, $sizes) {
    /** @var Utils $utils */
    $utils = $this->getContainer()->get('ss.front.utils');

    $servers = $this->getContainer()->getParameter('static_fss_enabled');

    foreach ($files as $filename) {
      $this->output->write(sprintf('File %s... ', $filename));

//      $original = 'http:' . $utils->get_poster_url($filename, NULL);
      $original = $utils->get_img_size_path($filename, NULL);
      $original = "http://static.cinemex.com/movie_posters/$original";

      try {
        $source = @file_get_contents($original);

        if (!$source) {
          throw new \Exception('download error');
        }

        $utils->resize_n_upload_to_cdn(
            'movie_posters/' . $filename,
            $source,
            $servers,
            $sizes,
            TRUE
        );

        $result = '<fg=green>saved</>';
      } catch (\Exception $e) {
        $result = sprintf('<fg=red>%s</>', $e->getMessage());
      }

      $this->output->writeln($result);
    }
  }

}
