<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;

/**
 * User
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="cinemex_user",
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_email",columns={"email"}),
 *     @ORM\UniqueConstraint(name="unique_fbuid",columns={"fbuid"})
 *   }
 * )
 * @UniqueEntity(fields="email", message="Ya existe un usuario registrado con ese email.")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingresa tu email.")
     * @Assert\Email(message="Por favor, ingresa un email válido.")
     */
    protected $email;

    /**
     * @Assert\Length(max = 50)
     */
    protected $plainPassword;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="first_name", type="string", length=127)
		 *
     * @Assert\NotBlank(message="Por favor, ingresa tu nombre.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max="127",
     *     minMessage="El nombre es muy corto.",
     *     maxMessage="El nombre es muy largo.",
     *     groups={"Registration", "Profile"}
     * )
		 */
		private $first_name;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="last_name", type="string", length=127)
		 *
     * @Assert\NotBlank(message="Por favor, ingresa tu apellido.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     max="127",
     *     minMessage="El apellido es muy corto.",
     *     maxMessage="El apellido es muy largo.",
     *     groups={"Registration", "Profile"}
     * )
		 */
		private $last_name;

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="fbuid", type="bigint", length=20, nullable=true)
		 */
		private $fbuid;

		/**
		 * @var integer
		 *
		 * @ORM\Column(name="iecode", type="bigint", length=20, nullable=true)
		 */
		private $iecode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="privacy_check", type="boolean")
     */
    private $privacy_check;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
		 */
		private $avatar;

		/**
     * @ORM\ManyToMany(targetEntity="\SocialSnack\WsBundle\Entity\Movie", inversedBy="users")
     * @ORM\JoinTable(name="user_movie_seen")
     */
    private $movies_seen;

		/**
     * @ORM\ManyToMany(targetEntity="\SocialSnack\WsBundle\Entity\Movie", inversedBy="users")
     * @ORM\JoinTable(name="user_movie_wish")
     */
    private $movies_wish;

		/**
     * @ORM\ManyToMany(targetEntity="\SocialSnack\WsBundle\Entity\Movie", inversedBy="users")
     * @ORM\JoinTable(name="user_movie_bought")
     */
    private $movies_bought;

		/**
		 * @var \SocialSnack\WsBundle\Entity\Cinema
     * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Cinema")
     */
    private $cinema;

		/**
     * @ORM\ManyToMany(targetEntity="\SocialSnack\WsBundle\Entity\Cinema", inversedBy="users")
     * @ORM\JoinTable(name="user_cinema_fav")
     */
    private $cinemas_fav;

		/**
     * @ORM\ManyToMany(targetEntity="\SocialSnack\WsBundle\Entity\Cinema", inversedBy="users")
     * @ORM\JoinTable(name="user_cinema_went")
     */
    private $cinemas_went;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var object
     *
     * @ORM\Column(name="info", type="json_array", nullable=true)
     */
    private $info;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @Assert\File(maxSize="6000000")
     */

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signup_date", type="datetime")
     */
    private $signup_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $update_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="login_date", type="datetime", nullable=true)
     */
    private $login_date;

    private $file;

    private $temp;



		public function __construct() {
      $this->active = true;
      $this->salt   = md5(uniqid(null, true));
			$this->setAvatar( 'default-avatar.png' );
      $this->privacy_check = FALSE;
			if ( !$this->signup_date ) {
				$this->signup_date = new \DateTime( 'now' );
			}
		}


    public function getEmail() {
      return $this->email;
    }


    public function getPassword() {
      return $this->password;
    }


    public function setPassword( $password ) {
      $this->password = $password;

      return $this;
    }


    public function getSalt() {
      return $this->salt;
    }


    public function setSalt( $salt ) {
      $this->salt = $salt;

      return $this;
    }


    public function getUsername() {
      return $this->email;
    }


    public function setUsername( $username ) {
      $this->email = $username;

      return $this;
    }


		/**
		 * Set email
		 *
		 * Replicate value tu username.
		 *
		 * @param string $email
     * @return User
		 */
		public function setEmail($email) {
			$this->email = $email;
			$this->username = $email;

      return $this;
		}

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add movies_seen
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesSeen
     * @return User
     */
    public function addMoviesSeen(\SocialSnack\WsBundle\Entity\Movie $moviesSeen)
    {
        $this->movies_seen[] = $moviesSeen;

        return $this;
    }

    /**
     * Remove movies_seen
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesSeen
     */
    public function removeMoviesSeen(\SocialSnack\WsBundle\Entity\Movie $moviesSeen)
    {
        $this->movies_seen->removeElement($moviesSeen);
    }

    /**
     * Get movies_seen
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoviesSeen()
    {
        return $this->movies_seen;
    }

    /**
     * Add movies_wish
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesWish
     * @return User
     */
    public function addMoviesWish(\SocialSnack\WsBundle\Entity\Movie $moviesWish)
    {
        $this->movies_wish[] = $moviesWish;

        return $this;
    }

    /**
     * Remove movies_wish
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesWish
     */
    public function removeMoviesWish(\SocialSnack\WsBundle\Entity\Movie $moviesWish)
    {
        $this->movies_wish->removeElement($moviesWish);
    }

    /**
     * Get movies_wish
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoviesWish()
    {
        return $this->movies_wish;
    }

    /**
     * Add movies_bought
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesBought
     * @return User
     */
    public function addMoviesBought(\SocialSnack\WsBundle\Entity\Movie $moviesBought)
    {
        $this->movies_bought[] = $moviesBought;

        return $this;
    }

    /**
     * Remove movies_bought
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $moviesBought
     */
    public function removeMoviesBought(\SocialSnack\WsBundle\Entity\Movie $moviesBought)
    {
        $this->movies_bought->removeElement($moviesBought);
    }

    /**
     * Get movies_bought
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoviesBought()
    {
        return $this->movies_bought;
    }

    /**
     * Set cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinema
     * @return User
     */
    public function setCinema(\SocialSnack\WsBundle\Entity\Cinema $cinema = null)
    {
        $this->cinema = $cinema;

        return $this;
    }

    /**
     * Get cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Add cinemas_fav
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemasFav
     * @return User
     */
    public function addCinemasFav(\SocialSnack\WsBundle\Entity\Cinema $cinemasFav)
    {
        $this->cinemas_fav[] = $cinemasFav;

        return $this;
    }

    /**
     * Remove cinemas_fav
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemasFav
     */
    public function removeCinemasFav(\SocialSnack\WsBundle\Entity\Cinema $cinemasFav)
    {
        $this->cinemas_fav->removeElement($cinemasFav);
    }

    /**
     * Get cinemas_fav
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCinemasFav()
    {
        return $this->cinemas_fav;
    }

    /**
     * Add cinemas_went
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemasWent
     * @return User
     */
    public function addCinemasWent(\SocialSnack\WsBundle\Entity\Cinema $cinemasWent)
    {
        $this->cinemas_went[] = $cinemasWent;

        return $this;
    }

    /**
     * Remove cinemas_went
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinemasWent
     */
    public function removeCinemasWent(\SocialSnack\WsBundle\Entity\Cinema $cinemasWent)
    {
        $this->cinemas_went->removeElement($cinemasWent);
    }

    /**
     * Get cinemas_went
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCinemasWent()
    {
        return $this->cinemas_went;
    }

    /**
     * Set fbuid
     *
     * @param integer $fbuid
     * @return User
     */
    public function setFbuid($fbuid)
    {
        $this->fbuid = $fbuid;

        return $this;
    }

    /**
     * Get fbuid
     *
     * @return integer
     */
    public function getFbuid()
    {
        return $this->fbuid;
    }

    /**
     * Set iecode
     *
     * @param integer $iecode
     * @return User
     */
    public function setIecode($iecode)
    {
        $this->iecode = $iecode;

        return $this;
    }

    /**
     * Get iecode
     *
     * @return integer
     */
    public function getIecode()
    {
        return $this->iecode;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->avatar)) {
            // store the old name to delete after the update
            $this->temp = $this->avatar;
            $this->avatar = null;
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            @unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath() && is_file($file)) {
            unlink($file);
        }
    }


    public function getAbsolutePath()
    {
        return null === $this->avatar
            ? null
            : $this->getUploadRootDir().'/'.$this->avatar;
    }

    public function getWebPath()
    {
        return null === $this->avatar
            ? null
            : $this->getUploadDir().'/'.$this->avatar;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/avatars';
    }

		public function getFullName() {
				return $this->first_name . ' ' . $this->last_name;
		}

		public function toArray() {
			return array(
					'id'         => $this->id,
					'first_name' => $this->first_name,
					'last_name'  => $this->last_name,
					'email'      => $this->email,
					'avatar'     => $this->getAvatar(),
					'fbuid'      => $this->fbuid,
					'iecode'     => $this->iecode,
					'cinema_id'  => $this->cinema ? $this->cinema->getId() : NULL,
			);
		}


    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
      $this->plainPassword = '';
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
        ) = unserialize($serialized);
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->active;
    }

    /**
     * Set signup_date
     *
     * @param \DateTime $signupDate
     * @return User
     */
    public function setSignupDate($signupDate)
    {
        $this->signup_date = $signupDate;

        return $this;
    }

    /**
     * Get signup_date
     *
     * @return \DateTime
     */
    public function getSignupDate()
    {
        return $this->signup_date;
    }

    /**
     * Set update_date
     *
     * @return User
     */
    public function setUpdateDate()
    {
        $this->update_date = new \DateTime('now');

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set login_date
     *
     * @return User
     */
    public function setLoginDate()
    {
        $this->login_date = new \DateTime('now');

        return $this;
    }

    /**
     * Get login_date
     *
     * @return \DateTime
     */
    public function getLoginDate()
    {
        return $this->login_date;
    }

    /**
     * Set privacy_check
     *
     * @param boolean $privacyCheck
     * @return User
     */
    public function setPrivacyCheck($privacyCheck)
    {
        $this->privacy_check = $privacyCheck;

        return $this;
    }

    /**
     * Get privacy_check
     *
     * @return boolean
     */
    public function getPrivacyCheck()
    {
        return $this->privacy_check;
    }


    public function getTempAvatar() {
      return $this->temp;
    }

    /**
     * Get info
     *
     * @return string|object
     */
    public function getInfo($key = NULL)
    {
      if ($key === NULL) {
        return $this->info;
      }

      if (array_key_exists($key, $this->info)) {
        return $this->info[$key];
      }

      return NULL;
    }

    /**
     * Add info
     *
     * @param string $key
     * @param mixed $value
     */
    public function addInfo($key, $value)
    {
      $this->info[$key] = $value;

      return $this;
    }

    /**
     * self::getInfo() alias for compatibility
     *
     * @param  string $key
     * @return mixed
     */
    public function getData($key)
    {
      return $this->getInfo($key);
    }

}
