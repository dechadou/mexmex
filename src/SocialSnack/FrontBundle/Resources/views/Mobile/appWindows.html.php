<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cinemex</title>

        <link href='//fonts.googleapis.com/css?family=Roboto:300,400,500,400italic,700|Roboto+Condensed:700' rel="stylesheet">

<style>
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

*:before,
*:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

html,
body {
    height:100%;
}

html,
body,
p {
    margin:0;
    padding:0;
}

html {
    font-size:62.5%;
    height:100%;
}

body {
/*    background:url(*/<?php //echo $view['assets']->getUrl('assets/img/mobile/bg.jpg'); ?>/*) no-repeat 50% 100%;*/
    color:#707070;
    font:300 10px/1.5em roboto,helvetica,sans-serif;
    height:100%;
    text-align:center;
}

strong {
    font-weight:500;
}

a {
    display:block;
    text-decoration:none;
}

img {
    display:block;
    height:auto;
    max-width:100%;
}

.container {
    margin-left:auto;
    margin-right:auto;
    max-width:768px;
}

#page {
    height:auto;
    min-height:100%;
    margin-bottom:-3.5rem;
    padding-bottom:3.5rem;
    padding-left: 20px;
    padding-right: 20px;
}

#header {
    background:white;
    border-bottom:1px solid #e9e9e9;
    line-height:5rem;
    height:5rem;
    text-transform:lowercase;
    top:0;
}

#header img {
    float:left;
    height:auto;
    margin:1rem;
    width:11rem;
}

#header a {
    color:#b31649;
    float:right;
    padding:0 1rem;
}

#body a {
    color:#707070;
    display:block;
    overflow:hidden;
    padding:2rem 1rem;
}

.device {
    margin:2rem auto;
    width:75%;
}

#footer {
    background:#d0033d;
    bottom:0;
    font-size:1.3rem;
    line-height:3.5rem;
    width: 100%;
}

#footer a {
    color:white;
}

.title {
    font-size: 1.4rem;
    line-height: 1.34em;
    margin-top: 2rem;
}
.banner {
    margin: -4rem auto 0;
    position: relative;
    padding-left: 5rem;
    width: 24rem;
}
.copy {
    color:black;
    font-size:1.3rem;
    line-height: 1.34em;
}
.store {
    position: absolute;
    left: 0;
    top: 50%;
    margin-top: -2.5rem;
    width: 4rem;
}
@media screen and (orientation: landscape) {
    #footer {
        position: fixed;
    }
    #page {
        padding-left: 3rem;;
        padding-right: 3rem;;
    }
    .device {
        float: right;
        margin: 0;
        width: 45%;
    }
    .title, .banner {
        float: left;
        width: 50%;
        margin-top: 2rem;
        text-align: left;
    }
    .title {
        margin-top: 4rem;
    }
}
@media screen and (min-width: 768px) {
    html {
        font-size:81.25%;
    }
    .title {
        margin-top: 8rem;
    }
    .banner {
        margin-top: 3rem;
    }
    /*.title {*/
        /*max-width: 30rem;*/
        /*margin: 0 auto;*/
    /*}*/
    /*.device {*/
        /*width: 60%;*/
    /*}*/
    /*.banner {*/
        /*margin-top: -11rem;*/
        /*margin-bottom: 2rem;*/
    /*}*/
}
/*@media screen and (min-width: 768px) {*/
    /*html {*/
        /*font-size:81.25%;*/
    /*}*/

    /*#body .device {*/
        /*width:75%;*/
    /*}*/

    /*#body .copy {*/
        /*font-size:1.5rem;*/
    /*}*/

    /*#body .banner {*/
        /*margin:0 3rem;*/
    /*}*/
/*}*/
</style>
    </head>
    <body>
        <div id="page">
<!--            <header id="header">-->
<!--                <div class="container">-->
<!--                    <img src="--><?php //echo $view['assets']->getUrl('assets/img/mobile/logo.svg'); ?><!--" alt="Cinemex">-->
<!---->
<!--                    <p><a href="--><?php //echo $view['router']->generate('skip_store_landing', $redirect_params); ?><!--">Ir a cinemex.com</a></p>-->
<!--                </div>-->
<!--            </header>-->

            <div id="body">
                <a href="https://www.microsoft.com/store/apps/9nblggh375cf">
                    <div class="container">
                        <p class="title"><strong>Con la nueva aplicación de Cinemex</strong>
                            podrás comprar de forma fácil y sencilla
                            los boletos para tus películas favoritas.</p>

                        <img class="device" src="<?php echo $view['assets']->getUrl('assets/img/mobile/device-windows.png'); ?>" alt="">

                        <div class="banner">
                            <img class="store" src="<?php echo $view['assets']->getUrl('assets/img/mobile/logo-windows-store.png'); ?>" alt="" />
                            <p class="copy">Descarga nuestra <strong>aplicación gratis</strong> en la Tienda Windows</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <footer id="footer">
            <a href="<?php echo $view['router']->generate('skip_store_landing', $redirect_params); ?>">
                <p class="container">Continuar a Cinemex.com</p>
            </a>
        </footer>
    </body>
</html>
