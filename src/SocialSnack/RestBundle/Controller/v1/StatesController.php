<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class StatesController extends BaseController {
  
  /**
   * @Route("/", name="rest_v1_states")
   */
  public function defaultAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:States:default', ['request' => $request]);
  }
  
}