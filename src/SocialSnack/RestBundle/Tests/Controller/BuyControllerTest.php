<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;

/**
 * Class BuyControllerTest
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class BuyControllerTest extends WebTestCase {

  protected $client;


  public function setUp() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));
    $this->getContainer()->get('memcached')->flush();
  }


  public function _testSelectTicketsAction($status_code, $data, $args = array()) {
    $defaults = array(
        'available_seats' => 10
    );
    $args = array_merge($defaults, $args);

    $ws_mock = $this->getServiceMockBuilder('gremo_buzz')
        ->getMock();

    $res = new \Buzz\Message\Response();
    $seats = $args['available_seats'];
    $res->setContent(sprintf(
        '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;DISPONIBLIDAD&gt;&lt;MENSAJE&gt;Asientos disponibles por este medio: %d&lt;/MENSAJE&gt;&lt;/DISPONIBLIDAD&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>',
        $seats
    ));
    $res->setHeaders(array('OK 200'));

    $ws_mock
        ->method('post')
        ->with('https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/DetalleSession')
        ->willReturn($res);

    $this->client = static::createClient();
    $this->client->getContainer()->set('gremo_buzz', $ws_mock);

    $session = isset($args['session'])
        ? $args['session']
        : LoadSessionData::$sessions[0]
    ;

    $post_data = array_merge(array(
        'session_id' => $session->getId(),
        'tickets' => array(
            array('type' => '0001', 'qty' => 2),
        ),
    ), $data);

    $this->client->request(
        'POST',
        '/rest/v2/buy/selectTickets',
        $post_data,
        array(),
        array(
            'HTTP_X-Api-Consumer-Key' => 'abc',
        )
    );

    $this->assertEquals($status_code, $this->client->getResponse()->getStatusCode());
    $json = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $json);

    return $json;
  }


  public function testSelectTicketsActionNoTickets() {
    $json = $this->_testSelectTicketsAction(400, array('tickets' => array(
        array('type' => '0001', 'qty' => 0),
    )));
    $this->assertEquals('no-tickets-selected', $json->errorcode);
  }


  public function testSelectTicketsActionSoldOut() {
    $json = $this->_testSelectTicketsAction(400, array(), array('available_seats' => 0));
    $this->assertEquals('no-tickets', $json->errorcode);
  }


  public function testSelectTicketsActionNotEnoughTickets() {
    $json = $this->_testSelectTicketsAction(400, array('tickets' => array(
        array('type' => '0001', 'qty' => 2),
    )), array('available_seats' => 1));
    $this->assertEquals('no-tickets', $json->errorcode);
  }


  public function _testSelectTicketsSuccessBuzzPost($url, $headers = array(), $content = '') {
    $res = new \Buzz\Message\Response();

    if (
        $url === 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/DetalleSession'
        &&
        preg_match('/^Tipo=1&/', $content)
    ) {
      $seats = 50;
      $res->setContent(sprintf(
          '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;DISPONIBLIDAD&gt;&lt;MENSAJE&gt;Asientos disponibles por este medio: %d&lt;/MENSAJE&gt;&lt;/DISPONIBLIDAD&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>',
          $seats
      ));
      $res->setHeaders(array('OK 200'));
      return $res;
    }

    if (
        $url === 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/DetalleSession'
        &&
        preg_match('/^Tipo=2&/', $content)
    ) {
      $res->setContent(file_get_contents(dirname(__FILE__) . '/../Fixtures/DetalleSessionTipo2.xml'));
      $res->setHeaders(array('OK 200'));
      return $res;
    }

    if (
        $url === 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/ApartadoAsientos'
        &&
        preg_match('/^Opcion=1&/', $content)
    ) {
      $res->setContent('<?xml version="1.0" encoding="utf-8"?> <CMXXML>   <RESPUESTA>     <ERROR>0</ERROR>     <MENSAJE>Asientos apartados correctamente</MENSAJE>     <CLAVETMP>20000178512</CLAVETMP>   </RESPUESTA> </CMXXML>');
      $res->setHeaders(array('OK 200'));
      return $res;
    }

    if (
        $url === 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/ApartadoAsientos'
        &&
        preg_match('/^Opcion=2&/', $content)
    ) {
      $res->setContent('<?xml version="1.0" encoding="utf-8"?> <CMXXML>   <RESPUESTA>     <ERROR>0</ERROR>     <MENSAJE>Se han liberado los asientos correctamente</MENSAJE>   </RESPUESTA> </CMXXML>');
      $res->setHeaders(array('OK 200'));
      return $res;
    }

    if ($url === 'https://ws.cinemex.test/wsCinemexWeb/wsCinemexWeb.asmx/TerminaCompra') {
      $res->setContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;0&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;2232979&lt;/TRANSACCION&gt;&lt;BOOKING&gt;146316&lt;/BOOKING&gt;&lt;BRANCH&gt;39  &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;UNI&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;521911|0|0||0000000001|2|0001|2|10|G|9|sala 11|521912|0|0||0000000001|2|0003|2|9|G|8|sala 11|521913|0|0||0000000001|2|0002|2|8|G|7|sala 11|521914|0|0||0000000001|2|0002|2|7|G|6|sala 11|521915|0|0||0000000001|2|0002|2|6|G|5|sala 11&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;0&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;0&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Venta exitosa&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
      $res->setHeaders(array('OK 200'));
      return $res;
    }
  }


  public function testSelectTicketsSuccess() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
    ));

    $tickets = [
        ['type' => '0001', 'qty' => 2],
    ];
    $seats = [
        ['row' => '13', 'seat' => 2],
        ['row' => '13', 'seat' => 3],
    ];

    $ws_mock = $this->getServiceMockBuilder('gremo_buzz')
        ->getMock();

    $ws_mock->method('post')
        ->will($this->returnCallback([$this, '_testSelectTicketsSuccessBuzzPost']));

    $this->client = static::createClient();
    $this->client->getContainer()->set('gremo_buzz', $ws_mock);

    $session = LoadSessionData::$sessions[0];

    $post_data = array(
        'session_id' => $session->getId(),
        'tickets'    => $tickets,
    );

    $this->client->request(
        'POST',
        '/rest/v2/buy/selectTickets',
        $post_data,
        array(),
        array(
            'HTTP_X-Api-Consumer-Key' => 'abc',
        )
    );

    $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    $json = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $json);

    // Select seats
    $post_data = array(
        'session_id' => $session->getId(),
        'transaction_id' => $json->transaction_id,
        'seats' => $seats,
    );

    $this->client = static::createClient();
    $this->client->getContainer()->set('gremo_buzz', $ws_mock);

    $this->client->request(
        'POST',
        '/rest/v2/buy/selectSeats',
        $post_data,
        array(),
        array(
            'HTTP_X-Api-Consumer-Key' => 'abc',
        )
    );


    // The last step!!!
    $post_data = array(
        'session_id'     => $session->getId(),
        'transaction_id' => $json->transaction_id,
        'tickets'        => $tickets,
        'seats'          => $seats,
        'email'          => 'test@test.com',
        'name'           => 'Homero Simpson',
        'cc'             => '4111111111111111',
        'csc'            => '666',
        'expire-month'   => '12',
        'expire-year'    => '99',
        'app_id'         => '',
        'ref'            => '',
    );

    $this->client = static::createClient();
    $this->client->getContainer()->set('gremo_buzz', $ws_mock);

    $this->client->request(
        'POST',
        '/rest/v2/buy/complete',
        $post_data,
        array(),
        array(
            'HTTP_X-Api-Consumer-Key' => 'abc',
        )
    );

    $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    $json = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $json);
    $this->assertEquals('B39  146316', $json->booking_code);
    $this->assertEquals(['G5', 'G6', 'G7', 'G8', 'G9'], $json->seats);
  }

}