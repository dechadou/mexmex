<?php
namespace SocialSnack\FrontBundle\Service;

class Graphite {
  
  private $container;
  private $config;
  private $metrics = array();
  
  
  public function __construct($container) {
    $this->container = $container;
    $this->config    = $this->container->getParameter('graphite');
    if ( $this->config['prefix'] ) {
      $this->config['prefix'] .= '.';
    }
  }
  
  
  /**
   * Publish enqueued metrics.
   * If arguments provided the metric is published without needing to enqueue it before.
   * 
   * @param type $endpoint
   * @param type $value
   * @param type $timestamp
   */
  public function publish($endpoint = FALSE, $value = FALSE, $timestamp = FALSE) {
    if ( $endpoint && $value ) {
      $this->add($endpoint, $value, $timestamp);
    }
    
    if (!$this->config['enabled'])
      return;
    
    try {
      $sock    = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
      $message = '';
      foreach ( $this->metrics as $metric ) {
        $message .= "{$this->config['api_key']} {$this->config['prefix']}{$metric['endpoint']} {$metric['value']}";
        if ( FALSE !== $metric['timestamp'] ) {
          $message .= $metric['timestamp'];
        }
        $message .= "\n";
      }
      socket_sendto($sock, $message, strlen($message), 0, $this->config['host'], $this->config['port']);
    } catch ( \Exception $e ) {
      /** @todo Log exceptions */
    }
  }
  
  
  /**
   * Enqueue a metric.
   * 
   * @param type $endpoint
   * @param type $value
   * @param type $timestamp
   */
  public function add($endpoint, $value, $timestamp = FALSE) {
    $this->metrics[] = array(
        'endpoint'  => $endpoint,
        'value'     => $value,
        'timestamp' => $timestamp
    );
  }
  
}