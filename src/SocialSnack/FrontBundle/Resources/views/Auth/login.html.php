<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Iniciar sesión - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('user_login'); ?>">Login</a></li>
</ul>

<div class="clearfix row">
	
	<div class="narrow-side-col col">
    <?php
    $menu = array(
        array(
          'label' => 'Iniciar sesión',
          'route' => 'user_login',
          'class' => 'icon-user',
        ),
        array(
          'label' => 'Registro',
          'route' => 'user_register',
          'class' => 'icon-user',
        ),
    );
    ?>
    <ul class="panel-menu">
      <?php
      foreach ( $menu as $item ) {
        if ( !is_array( $item['route'] ) ) {
          $item['route'] = array( $item['route'] );
        }
        $current = in_array( 'user_login', $item['route'] );
      ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
      <?php
      }
      ?>
    </ul>
  </div>
  
  <div class="wide-main-col col">
    <div class="content-box">
      <h1 class="content-box-title">Iniciar sesión</h1>
      
      <?php
      echo $view->render(
          'SocialSnackFrontBundle::flashMsgs.html.php',
          array( 'flash_msgs' => $flash_msgs )
      );
      ?>
      
      <form action="<?php echo $view['router']->generate( 'login_check' ); ?>" method="post">
        <div class="std-form clearfix">
          <?php /*<input type="hidden" value="<?php echo $view->container->get( 'form.csrf_provider' )->generateCsrfToken('authenticate'); ?>" name="_csrf_token" />*/ ?>
          <div class="form-row half">
            <label for="login-user">Email</label>
            <input type="text" name="_username" id="user" placeholder="Email" data-validators="not_empty email" data-flyvalidate="1" />
          </div>
          <div class="form-row half">
            <label for="login-pass">Contrseña</label>
            <input type="password" name="_password" id="pass" placeholder="Contraseña" data-validators="not_empty" data-flyvalidate="1" />
          </div>
        </div>
        <div class="btn-row">
          <a href="<?php echo $view['router']->generate('user_forgot_pass'); ?>" class="small forgot-password">¿Olvidaste tu contraseña?</a>
          <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Ingresar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start( 'js_after' ); ?>
<script>
  var redirect_after_login = true;
</script>
<?php $view['slots']->stop(); ?>