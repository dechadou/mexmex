<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Invitado Especial - Preguntas Frecuentes - Cinemex');
$view['slots']->start('body')
?>

<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/header-ie.jpg'); ?>" alt="Invitado Especial" />
</div>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render('SocialSnackFrontBundle:Ie:menu.html.php', array('route' => $route)); ?>
  </div>

  <div class="wide-main-col col">
    <img src="<?php echo $view['assets']->getUrl('assets/img/ie-faq-01.jpg'); ?>" alt="Preguntas frecuentes 01" class="ie-faq-img" />
    <img src="<?php echo $view['assets']->getUrl('assets/img/ie-faq-02.jpg'); ?>" alt="Preguntas frecuentes 02" class="ie-faq-img" />
  </div><!-- .wide-main-col -->
</div><!-- .row -->

<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after'); ?>
<?php $view['slots']->stop(); ?>
