<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

class PurgeArtMap extends AbstractConceptMap {

  protected  function getRouteName() {
    return 'art_cinemas';
  }


  protected function getConceptName() {
    return 'Art';
  }

}