<?php

namespace SocialSnack\WsBundle\Request\MisteryShopper;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Request\MisteryShopperAbstract;

class MuestraCatalogo extends MisteryShopperAbstract {

  const CATALOGO_PREGUNTAS  = 1;
  const CATALOGO_RESPUESTAS = 2;

	protected $method = 'MuestraCatalogo';
	
	protected $default_args = array(
      'catalogo' => NULL,
  );


	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws RequestMsgException
	 * @param \stdClass $res;
   * @return \SimpleXMLElement
	 */
	public function parse_res($res) {
    if ( !$res->MuestraCatalogoResult ) {
      throw new RequestMsgException( 'Invalid response 0: ' . json_encode( $res ) );
    }
    
    $xml = simplexml_load_string( $res->MuestraCatalogoResult->any, null, LIBXML_NOCDATA );

		return $xml;
	}
  
}