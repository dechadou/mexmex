<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use Symfony\Component\Routing\Router;

abstract class AbstractEntityTask implements EntityTaskInterface {

  protected $router;

  protected $frontUtils;

  protected $entity;

  public function __construct(Router $router, FrontUtils $frontUtils) {
    $this->router     = $router;
    $this->frontUtils = $frontUtils;
  }


  public function setEntity($entity) {
    $this->entity = $entity;

    return $this;
  }

}