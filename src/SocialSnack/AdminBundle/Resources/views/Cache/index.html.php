<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Borrar cache' );
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Borrar cache
      </header>
      
      <div class="panel-body">
        <form class="form-horizontal tasi-form" method="post">
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Cache de carteleras</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['zone']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Mapas de cines</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['cinemazone']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Mapas por concepto</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['conceptMap']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Página de cine</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['cinema']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Boletos de cine</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['cinemaTickets']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Cache de home</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['home']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Fechas</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['dates']); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Cache de Instagram/Twitter</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['social']); ?>
            </div>
          </div>
          <button type="submit" name="save" class="btn btn-primary" onclick="return confirm('Seguro que quiere borrar el cache?');">Borrar</button>
        </form>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>