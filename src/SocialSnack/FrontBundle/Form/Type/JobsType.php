<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class JobsType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    /**
     * GENERAL!
     */
    $builder->add('general_name', NULL, [
       'label'    => 'Nombre completo',
       'required' => TRUE
     ])
     ->add('general_address', NULL, [
       'label'    => 'Calle y número',
       'required' => TRUE
     ])
     ->add('general_neighborhood', NULL, [
       'label'    => 'Colonia',
       'required' => TRUE
     ])
     ->add('general_postal_code', NULL, [
       'label'    => 'Código postal',
       'required' => TRUE,
       'attr'     => array()
     ])
     ->add('general_state', 'entity', [
       'label'    => 'Estado',
       'empty_value' => 'Seleccione un estado',
       'required'    => TRUE,
       'class'    => 'SocialSnackWsBundle:State',
       'property' => 'name',
       'query_builder' => function(EntityRepository $er){
         return $er->createQueryBuilder('s')
         ->where('s.active = 1')
         ->orderBy('s.name', 'ASC');
       }
     ])
     ->add('general_city', NULL, [
         'label'    => 'Municipio, Delegación o Ciudad',
         'required' => FALSE
     ])
     ->add('general_phone_mobile', NULL, [
         'label'    => 'Teléfono celular',
         'required' => FALSE
     ])
     ->add('general_phone_home', NULL, [
         'label'    => 'Teléfono de casa',
         'required' => FALSE
     ])
     ->add('general_email', NULL, [
         'label'    => 'Correo electrónico',
         'required' => FALSE,
         'attr'     => [
           'type' => 'email'
         ],
         'constraints' => array(
             new Assert\Email(),
         )
     ])
     ->add('general_birthdate', 'birthday', [
       'label'       => 'Fecha de nacimiento (día/mes/año)',
       'years'       => $options['years'],
       'constraints' => [new Assert\NotBlank()],
       'format'      => 'dd/MM/yyyy'
     ])
     ->add('general_marital_status', 'choice', [
       'label'    => 'Estado civil',
       'required' => TRUE,
       'choices'  => [
         ''           => 'Seleccione un estado civil',
         'soltero'    => 'Soltero',
         'casado'     => 'Casado',
         'divorciado' => 'Divorciado',
         'viudo'      => 'Viudo'
       ]
     ])
     ->add('general_have_childrens', 'choice', [
       'label'    => '¿Tiene hijos?',
       'required' => TRUE,
       'choices'  => [
         ''   => 'Seleccionar una opción',
         'si' => 'Si',
         'no' => 'No'
       ]
     ])
     ->add('general_number_imss', NULL, [
       'label'    => 'Numero de pre afiliación o IMSS',
       'required' => FALSE,
     ])
     ->add('general_number_rfc', NULL, [
       'label'    => 'RFC',
       'required' => FALSE,
       'attr'     => [
         'maxlength' => 13
       ]
     ])
     ->add('general_number_curp', NULL, [
       'label'    => 'CURP',
       'required' => TRUE,
       'attr'     => [
         'maxlength' => 18
       ]
     ])
     ->add('general_gender', 'choice', [
       'label'    => 'Genero',
       'required' => TRUE,
       'choices'  => [
         ''          => 'Seleccione un genero',
         'masculino' => 'Masculino',
         'femenino'  => 'Femenino'
       ]
     ]);

    /**
     * GROUP!
     */
    $builder
     ->add('area_available_trips', 'checkbox', array(
       'label'    => 'Disponible para viajar',
       'required' => FALSE,
       'attr'     => array()
     ))
     ->add('area_available_move', 'checkbox', array(
       'label'    => 'Disponible cambio de residencia',
       'required' => FALSE,
       'attr'     => array()
     ));

    $builder->add('area_type', 'choice', array(
       'label'    => '¿Dónde quieres trabajar?',
       'required' => TRUE,
       'choices'  => array(
         ''        => 'Seleccione un opción',
         'cafes'   => 'Cafes',
         'cinemas' => 'Cinemas'
       ),
       'attr'     => array()
     ));

    // Cinemas
    $builder->add('area_name', 'choice', array(
       'label'    => 'Área de interés',
       'required' => TRUE,
       'choices'  => array(
         ''                     => 'Seleccione un area',
         'Gerente de cine'      => 'Gerente de cine',
         'Staff Multifuncional' => 'Staff Multifuncional',
         'Mantenimiento'        => 'Mantenimiento'
       ),
       'attr'     => array()
     ))
     ->add('area_state', 'choice', array(
       'label'       => 'Selecciona tu ciudad',
       'empty_value' => 'Selecciona tu ciudad',
       'required'    => TRUE,
       'attr'        => array(
          'class' => 'area-w-cinema-select'
        ),
       'choices'     => $options['states']
     ))
     ->add('cinema', 'choice', array(
       'label'       => 'Complejo de interés',
       'empty_value' => 'Seleccione un cinema',
       'required'    => TRUE,
       'attr'        => array(),
       'choices'     => $options['choices']
     ));

    // Cafes
    $builder
     ->add('area_cafe', 'choice', array(
       'label'       => 'Complejo de interés',
       'empty_value' => 'Seleccione un café',
       'required'    => TRUE,
       'attr'        => array(),
       'choices'     => $options['cafes']
     ));

    /**
     * ESCOLARIDAD!
     */
    $builder->add('school_grade', NULL, [
       'label'    => 'Último grado de estudios',
       'required' => FALSE
     ])
     ->add('school_actual', NULL, [
       'label'    => 'Estudios actuales',
       'required' => FALSE
     ]);

    /**
     * EXPERIENCE!
     */
    for ($i = 1; $i <= $options['jobs_loop']; ++$i) :
     $builder
       ->add("experience_{$i}company",     NULL, ['required' => FALSE])
       ->add("experience_{$i}position",    NULL, ['required' => FALSE])
       ->add("experience_{$i}period",      NULL, ['required' => FALSE])
       ->add("experience_{$i}salary",      NULL, ['required' => FALSE])
       ->add("experience_{$i}exit_reason", NULL, ['required' => FALSE]);
    endfor;

    /**
     * FAMILY!
     */
    foreach ($options['family'] as $member => $label) :
     $builder
       ->add("family_{$member}_name",    NULL, ['required' => FALSE])
       ->add("family_{$member}_age",     NULL, ['required' => FALSE])
       ->add("family_{$member}_job",     NULL, ['required' => FALSE])
       ->add("family_{$member}_company", NULL, ['required' => FALSE]);
    endforeach;

    $builder
     ->add('family_cinemex', 'choice', [
       'label'       => 'Tiene familia trabajando en cinemex',
       'empty_value' => 'Seleccione una opción',
       'choices' => [
         'si' => 'Si',
         'no' => 'No'
       ]
     ]);
     // ->add('family_cinema', 'choice', [
     //   'label'       => 'Complejo en el que labora',
     //   'empty_value' => 'Seleccione un complejo',
     //   'required'    => FALSE,
     //   'choices'     => $options['choices']
     // ]);

    $builder
      ->add('family_state', 'choice', array(
        'label'       => 'Selecciona tu ciudad',
        'empty_value' => 'Selecciona tu ciudad',
        'required'    => TRUE,
        'attr'        => array(
          'class' => 'area-w-cinema-select',
          'data-target' => '#family_cinema'
        ),
        'choices'     => $options['states']
      ))
      ->add('family_cinema', 'choice', array(
        'label'       => 'Complejo de interés',
        'empty_value' => 'Seleccione un cinema',
        'required'    => TRUE,
        'attr'        => array(),
        'choices'     => $options['choices']
      ));

    $builder->add('captcha', 'hidden');

    /**
     * SUBMIT!!!
     */
    $builder->add('submit', 'submit', array('label' => 'Enviar'));
  }

  public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'years'     => NULL,
        'choices'   => NULL,
        'family'    => NULL,
        'jobs_loop' => NULL,
        'states'    => NULL,
        'cinemas'   => NULL,
        'cafes'     => NULL
    ));
  }

  public function getName() {
    return '';
  }

}
