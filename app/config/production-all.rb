server "108.168.135.55", :app, :web, :primary => true
server "108.168.135.62", :app, :web, :primary => true
server "108.168.135.36", :app, :web, :primary => true
server "108.168.135.82", :app, :web, :primary => true
#set :clear_controllers, false
set :parameters_file, "parameters_prod.yml"

# Uncomment the following line if not delpoying master
#set :git_shallow_clone, 0
#set :branch, "d19ef54"

after "deploy:create_symlink", "deploy:syncassets"
after "deploy:restart", "deploy:apache_graceful"