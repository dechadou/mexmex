<?php
$method = strtoupper($method);
if ( in_array($method, array('GET', 'POST')) ) {
  $form_method = $method;
} else {
  $form_method = 'POST';
}
if (!isset($attr['class'])) {
  $attr['class'] = '';
}
$attr['class'] .= ' std-form sym-form';
?>
<form name="<?php echo $name; ?>" method="<?php echo $form_method; ?>" action="<?php echo $action; ?>"<?php foreach ( $attr as $attrname => $attrvalue ) { echo "$attrname=\"$attrvalue\""; } ?> <?php if ($multipart) echo 'enctype="multipart/form-data"'; ?>>
  <?php if ($form_method != $method) { ?><input type="hidden" name="_method" value="<?php echo $method; ?>" /><?php } ?>