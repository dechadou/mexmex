<?php

namespace SocialSnack\WsBundle\Observer;

use SocialSnack\FrontBundle\Service\CachePurger;
use SocialSnack\WsBundle\Entity\Task;

class SessionUpdaterObserver {

  protected $cachePurger;

  public function __construct(CachePurger $cachePurger) {
    $this->cachePurger = $cachePurger;
  }


  /**
   * @param Task $task
   * @return bool
   */
  protected function canHandle(Task $task) {
    if ($task->getService() !== 'cinemex.session_updater') {
      return FALSE;
    }

    return TRUE;
  }


  /**
   * @param Task    $task
   * @param boolean $success
   * @param array   $result
   * @return boolean
   */
  public function update(Task $task, $success, array $result) {
    if (!$this->canHandle($task)) {
      return FALSE;
    }

    if (!$success) {
      return;
    }

    $data = unserialize($task->getData());

    $this->cachePurger->enqueue([
        'type'      => 'method',
        'method'    => CachePurger::PURGE_CINEMA_TICKETS,
        'cinema_id' => $data['cinema_id'],
    ]);

    $this->cachePurger->enqueue([
        'type'      => 'method',
        'method'    => CachePurger::PURGE_CINEMA_SESSIONS,
        'cinema_id' => $data['cinema_id'],
    ]);

    $this->cachePurger->purge_cache();
  }

}