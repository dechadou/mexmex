<?php

namespace SocialSnack\RestBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class StatesController extends BaseController {
  
  /**
   * @Route("/")
   */
  public function defaultAction(Request $request) {
    $api_v      = $this->get('rest.context.handler')->getApiVersion();
    $state_repo = $this->getDoctrine()->getRepository('SocialSnackWsBundle:State');
    $states     = $state_repo->findAllWithAreas();
    $result     = array();

    foreach ( $states as $state ) {
      $_state = $state->toArray( TRUE );

      // API v1 compatibility.
      if ($api_v < 2) {
        $_state['areas'] = array_merge(array(array(
            'id'       => $state->getId() + 1000,
            'name'     => 'Todo ' . $state->getName(),
            'state_id' => $state->getId(),
        )), $_state['areas']);
      }

      $result[] = $_state;
    }

    $response = new JsonResponse($result);
    $response->setPublic();
    $response->setSharedMaxAge(24 * 60 * 60);
    $response->setMaxAge(15 * 60);

    return $response;
  }
  
}