<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['promos']); ?>
<?php $view['slots']->set('title', 'Promociones - Cinemex'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="alternate" href="cinemex://com.cinemex/promociones" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('promos_archive'); ?>">Promociones</a></li>
</ul>

<?php
echo $this->render('SocialSnackFrontBundle:Partials:mainSlider.html.php', array(
  'promosa' => $promosa
));
?>

<span class="bottom-title">
  <span class="discicon icon-star icon-red"></span>
  Promociones
</span>

<div class="clearfix archive-row" id="promos-archive">
  <?php foreach ($promos as $id => $promo) : ?>
  <article class="col col-sm-1-4 col-md-1-5 promos-grid-item">
    <a href="#" class="promo">
      <img src="<?php echo $view['fronthelper']->get_cms_url("promos/".$promo->getThumb(), '236x222'); ?>" alt="<?php echo $promo->getTitle(); ?>" />
    </a>

    <div class="ingrid-promo col movie-details-tabs hidden">
      <?php if (!empty($promo->getUrl())) : ?>
      <a href="<?php echo $promo->getUrl(); ?>" target="_blank">
        <img src="<?php echo $view['fronthelper']->get_cms_url("promos/".$promo->getImage(), '670x320'); ?>" alt="<?php echo $promo->getTitle(); ?>" class="ingrid-img" />
      </a>
      <?php else : ?>
      <img src="<?php echo $view['fronthelper']->get_cms_url("promos/".$promo->getImage(), '670x320'); ?>" alt="<?php echo $promo->getTitle(); ?>" class="ingrid-img" />
      <?php endif; ?>

      <div class="ingrid-content">
        <h1 class="ingrid-title"><?php echo $promo->getTitle(); ?></h1>
        <h1 class="ingrid-subtitle"><?php echo $promo->getSubtitle(); ?></h1>

        <?php if (!empty($promo->getContent())) : ?>
        <p><?php echo $promo->getContent(); ?></p>
        <?php endif; ?>

        <?php
        // <span class="ingrid-label">Descripción de la Promoción:</span>
        //
        // <p>Gana 1 de los 4 viajes a la NFL o 1 de los 2 viajes al Super Bowl. <a href="https://www.facebook.com/CinemexSitioOficial/app_598723916858561" target="_blank">Continuar Leyendo</a>.</p>
        ?>
      </div>
    </div>
  </article>
  <?php endforeach; ?>
</div>

<div class="disclaimer">
  <p>
    <a href="<?php echo $view['router']->generate('privacy_promos'); ?>">Aviso de privacidad para promociones, trivias y concursos</a>.
  </p>
</div>
<?php $view['slots']->stop(); ?>
