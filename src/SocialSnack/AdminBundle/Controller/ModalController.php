<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\ModalType;
use SocialSnack\FrontBundle\Entity\Modal;
use SocialSnack\FrontBundle\Entity\ModalTarget;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ModalController
 * @package SocialSnack\AdminBundle\Controller
 * @author Rodrigo Catalano
 * @author Guido Kritz
 */
class ModalController extends BaseController {

  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="social_snack_modal_list")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction() {
    $this->check_roles();

    $repo_modals = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Modal');
    $modals = $repo_modals->getList();

    $routes = $this->getRoutes();

    return $this->render('SocialSnackAdminBundle:Modal:list.html.php', array('modals' => $modals,'routes'=>$routes));
  }


  protected function fileName(){
    $filePath = md5(rand() * time());
    return $filePath;
  }


  protected function getZones(){
    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:State');
    $states        = $repo->findAllWithAreas();

    $rv = array();
    $rv['null'] = '-- Seleccionar --';
    foreach($states as $_state){
      $rv['stateid-'.$_state->getId()] = $_state->getName();
      foreach($_state->getAreas() as $_area){
        $rv['areaid-'.$_area->getId()] = '- ' . $_area->getName();
        foreach ($_area->getCinemas() as $_cinema) {
          $rv['cinemaid-'.$_cinema->getId()] = '--- ' . $_cinema->getName();
        }
      }
    }

    return $rv;
  }

  protected function getRoutes(){
    return array(
        'home' => 'Home',
        'cinemas_archive' => 'Cines',
        'cinemas_map' => 'Mapa de Cines',
        'platinum' => 'Platino',
        'premium_index' => 'Premium',
        'x4d_index' => 'X4D',
        'xtremo_index' => 'Cine Extremo',
        '3d_index' => '3D',
        'art' => 'Casa de Arte',
        'ea_index' => 'Espacio Alternativo',
        'cinemom' => 'Cinema',
        'ie_program' => 'Invitado Especial',
        'promos_archive' => 'Promociones',
        'contact' => 'Contacto',
        'about_index' => 'Nosotros',
        'jobs_index' => 'Empleos',
        'jobs_form' => 'Solicitud de Empleo'
    );
  }


  /**
   * @Route("/add", name="social_snack_modal_add", defaults={"modal_id"=null})
   * @Route("/{modal_id}", name="social_snack_modal")
   *
   * @param null    $modal_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function editAction($modal_id = NULL, Request $request) {
    $this->check_roles();

    $em         = $this->get('doctrine')->getManager();
    $modal_repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Modal');

    if (is_null($modal_id)) {
      $modal         = new Modal();
      $modal_cinemas = [];
      $targets       = [];

    } else {

      $modal = $modal_repo->find($modal_id);
      if (!$modal) {
        throw $this->createNotFoundException();
      }

      $modal_cinemas = $this->getDoctrine()
          ->getRepository('SocialSnackFrontBundle:ModalTarget')
          ->findBy(array('modal_id'=>$modal_id));
      $targets = array();

      foreach($modal_cinemas as $m){
        $targets[] = $m->getTarget();
      }
    }

    $form = $this->createForm(new ModalType(), $modal, [
        'routes'  => $this->getRoutes(),
        'zones'   => $this->getZones(),
        'targets' => $targets,
    ]);
    $form->add('submit', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      $purge_files = NULL;
      $base_path = 'modals/';

      $modal->setDateUpdated(new \DateTime('now'));


      // Remove old cinemas associations.
      foreach ($modal_cinemas as $modal_target) {
        $em->remove($modal_target);
      }

      // Create new cinemas associations.
      if ($cinema = $form->get('cinema')->getData()) {
        $zones = $this->getZones();

        foreach ($cinema as $cinemas) {
          $modal_cinemas = new ModalTarget();
          $modal_cinemas->setModal($modal);
          $modal_cinemas->setTarget($cinemas);
          $modal_cinemas->setTargetName($zones[$cinemas]);
          $em->persist($modal_cinemas);
        }
      }

      // File upload.
      if ($file = $form->get('file')->getData()) {
        $filename = $this->fileName();
        $ext      = substr(strrchr($file->getClientOriginalName(), "."), 1);

        // Limit image dimensions if bigger than threshold. Don't resize by default.
        $w = NULL;
        $h = NULL;
        $size_suffix = '';
        if ('swf' !== $modal->getType()) {
          $width = getimagesize($file->getPathName())[0];
          if ($width > 600) {
            $w = '600';
            $h = 'auto';
            $size_suffix = '-600xauto';
          }
        }

        $this->container->get('ss.front.utils')->upload_cms_content($filename . "." . $ext, file_get_contents($file->getPathName()), $base_path, $w, $h, $ext);
        $modal->setFile($base_path . $filename . $size_suffix . "." . $ext);
      }

      $em->persist($modal);
      $em->flush();

      $cache_purger = $this->get('ss.cache_purger');
      $cache_purger->enqueue(array('entity' => $modal));
      $purge = $cache_purger->purge_cache();
      $request->getSession()->getFlashBag()->set('purge', $purge);

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');

      return $this->redirect($this->generateUrl('social_snack_modal', array('modal_id' => $modal->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Modal:edit.html.php', array(
        'modal'      => $modal,
        'form'       => $form->createView(),
        'purge'      => $request->getSession()->getFlashBag()->get('purge'),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/{modal_id}/delete", name="social_snack_modal_delete")
   *
   * @param $modal_id
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteAction($modal_id) {
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Modal');
    $modal = $repo->find($modal_id);
    $em->remove($modal);
    $em->flush();
    return $this->redirect($this->generateUrl('social_snack_modal_list'));
  }
}
