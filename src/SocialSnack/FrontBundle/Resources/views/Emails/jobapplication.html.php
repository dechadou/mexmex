<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start('body');
?>
<table class="body-wrap" bgcolor="#ebebeb" style="width:100%;padding-bottom:30px;">
    <tr>
        <td></td>
        <td class="container" align="" bgcolor="#ebebeb" style="display:block!important;max-width:600px!important;margin:0 auto!important;clear:both!important;">
            <div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block; border-bottom: 1px solid #d5d5d5;">
                <h1><?php echo $subject; ?></h1>
            </div>

            <div class="content" style="
                padding:15px;
                max-width:600px;
                margin:0 auto;
                display:block;
                border-top: 1px solid #fbfbfb;
                border-bottom:1px solid #d5d5d5;">

                <h2>Información general</h2>

                <p>
                    <strong>Nombre completo:</strong> <?php echo $data['general_name']; ?><br>
                    <strong>Calle y número:</strong> <?php echo $data['general_address']; ?><br>
                    <strong>Colonia:</strong> <?php echo $data['general_neighborhood']; ?><br>
                    <strong>Código postal:</strong> <?php echo $data['general_postal_code']; ?><br>
                    <strong>Estado:</strong> <?php echo $data['general_state']; ?><br>
                    <strong>Municipio, Delegación o Ciudad:</strong> <?php echo $data['general_city']; ?><br>
                    <strong>Teléfono celular:</strong> <?php echo $data['general_phone_mobile']; ?><br>
                    <strong>Teléfono de casa:</strong> <?php echo $data['general_phone_home']; ?><br>
                    <strong>Correo electrónico:</strong> <?php echo $data['general_email']; ?><br>
                    <strong>Fecha de nacimiento:</strong> <?php echo $data['general_birthdate']->format('d/m/Y'); ?><br>
                    <strong>Edad:</strong> <?php echo $data['general_age']; ?><br>
                    <strong>Estado civil:</strong> <?php echo $data['general_marital_status']; ?><br>
                    <strong>¿Tiene hijos?:</strong> <?php echo $data['general_have_childrens']; ?><br>
                    <strong>Numero de pre afiliación o IMSS:</strong> <?php echo $data['general_number_imss']; ?><br>
                    <strong>RFC:</strong> <?php echo $data['general_number_rfc']; ?><br>
                    <strong>CURP:</strong> <?php echo $data['general_number_curp']; ?>
                </p>

                <p>Recuerda que por Ley, es tu responsabilidad cuidar los Datos Personales.<br>
                  Cualquier duda o comentario envíalo a <a href="mailto:privacidad@cinemex.net">privacidad@cinemex.net</a></p>
            </div>

            <div class="content" style="
                padding:15px;
                max-width:600px;
                margin:0 auto;
                display:block;
                border-top: 1px solid #fbfbfb;
                border-bottom:1px solid #d5d5d5;">

                <h2>Área de interés</h2>

                <p>
                    <?php if ($data['area_available_trips'] == 1) { ?><strong>Disponible para viajar:</strong> sí<br><?php } ?>
                    <?php if ($data['area_available_move'] == 1) {  ?><strong>Disponible para cambio de residencia:</strong> sí<?php } ?>
<?php if ($data['area_type'] == 'cafes') : ?>
                    <strong>Complejo de interés:</strong> <?php echo $data['area_cafe']; ?><br>
<?php
endif;

if ($data['area_type'] == 'cinemas') : ?>
                    <strong>Área de interés:</strong> <?php echo $data['area_name']; ?><br>
                    <strong>Complejo de interés:</strong> <?php echo $data['cinema']; ?><br>
<?php endif; ?>
                </p>
            </div>

            <div class="content" style="
                padding:15px;
                max-width:600px;
                margin:0 auto;
                display:block;
                border-top: 1px solid #fbfbfb;
                border-bottom:1px solid #d5d5d5;">

                <h2>Escolaridad</h2>

                <p>
                    <strong>Último grado de estudios:</strong> <?php echo $data['school_grade']; ?><br>
                    <strong>Estudios actuales:</strong> <?php echo $data['school_actual']; ?>
                </p>
            </div>

            <div class="content" style="
                padding:15px;
                max-width:600px;
                margin:0 auto;
                display:block;
                border-top: 1px solid #fbfbfb;
                border-bottom:1px solid #d5d5d5;">

                <h2>Trabajos anteriores</h2>

                <?php
                for ($i = 1; $i <= $jobs_loop; ++$i) :
                    if (empty($data["experience_{$i}company"])) {
                        continue;
                    }
                ?>
                <p>
                    <strong>Empresa:</strong> <?php echo $data["experience_{$i}company"]; ?><br>
                    <strong>Puesto:</strong> <?php echo $data["experience_{$i}position"]; ?><br>
                    <strong>Periodo:</strong> <?php echo $data["experience_{$i}period"]; ?><br>
                    <strong>Sueldo:</strong> <?php echo $data["experience_{$i}salary"]; ?><br>
                    <strong>Motivo de salida:</strong> <?php echo $data["experience_{$i}exit_reason"]; ?>
                </p>
                <?php endfor; ?>
            </div>

            <div class="content" style="
                padding:15px;
                max-width:600px;
                margin:0 auto;
                display:block;
                border-top: 1px solid #fbfbfb;">

                <h2>Datos familiares</h2>

                <?php
                foreach ($family as $field => $label) :
                    if (empty($data["family_{$field}_name"])) {
                        continue;
                    }
                ?>

                <?php if ($label !== NULL) : ?>
                <p><strong><?php echo $label; ?></strong></p>
                <?php endif; ?>

                <p>
                    <strong>Nombre:</strong> <?php echo $data["family_{$field}_name"]; ?><br>
                    <strong>Edad:</strong> <?php echo $data["family_{$field}_age"]; ?><br>
                    <strong>Ocupación:</strong> <?php echo $data["family_{$field}_job"]; ?><br>
                    <strong>Empresa:</strong> <?php echo $data["family_{$field}_company"]; ?>
                </p>
                <?php endforeach; ?>

                <p>
                    <strong>Tiene familia trabajando en cinemex:</strong> <?php echo $data["family_cinemex"]; ?><br>
                    <?php if ('no' != $data["family_cinemex"]) { ?>
                    <strong>Complejo en el que labora:</strong> <?php echo $data["family_cinema"]; ?>
                    <?php } ?>
                </p>
            </div>
        </td>
    </tr>
</table>

<?php $view['slots']->stop();?>
