<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\FrontBundle\Entity\User;

class LoadPurchaseHistoryData extends AbstractFixture implements OrderedFixtureInterface {

  static $users;

  static $transactions;

  public function load(ObjectManager $manager) {
    static::$users = [];
    static::$transactions = [];

    $faker = Faker\Factory::create();

    $user = new User();
    $user
        ->setEmail($faker->email())
        ->setFirstName($faker->firstName())
        ->setLastName($faker->lastName())
        ->setPassword('AIkq7VP8RAa6FeXuqf0bawW0fCDtzhQ10qNj3mGIdVMLLKULJkfRIEmMskSSOAIAL2rKHhZqLDUITbORUVIvoQ==') // foobar
        ->setActive(TRUE)
        ->setAvatar($faker->imageUrl())
        ->setFbuid(NULL)
        ->setIecode(NULL)
    ;
    $manager->persist($user);
    static::$users[] = $user;

    for ($i = 0; $i < 50; $i++) {
      $session = $this->getReference('Session-1');

      $transaction = new Transaction();
      $transaction
          ->setAmount($faker->randomNumber() * 100)
          ->setCinema($session->getcinema())
          ->setCode('B123 45678')
          ->setHash($faker->md5())
          ->setInfo(json_encode(array()))
          ->setMovie($session->getMovie())
          ->setPurchaseDate($faker->dateTime())
          ->setQr($faker->imageUrl())
          ->setRealSeats('C8,C9')
          ->setSeats('[{"name":"Adulto","qty":"2","price":7800,"type":"0001"}]')
          ->setTickets('[{"row":"3","seat":"9"},{"row":"3","seat":"8"}]')
          ->setTicketsCount(2)
          ->setSession($session)
          ->setUser($user)
      ;
      $manager->persist($transaction);
      static::$transactions[] = $transaction;
    }

    $manager->flush();
  }

  public function getOrder() {
    return 6;
  }

}