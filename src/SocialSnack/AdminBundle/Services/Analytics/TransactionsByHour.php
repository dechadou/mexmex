<?php

namespace SocialSnack\AdminBundle\Services\Analytics;

use Doctrine\Bundle\DoctrineBundle\Registry;

class TransactionsByHour {

  protected $doctrine;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  protected function getCurrentTime() {
    return new \DateTime('now');
  }


  public function getComparableTodayDates() {
    $current_to = $this->getCurrentTime();
    if ($current_to->format('i') < 15) {
      $current_to->sub(new \DateInterval('PT1H')); // Query transactions up to 1 hour ago.
    }
    $current_to->setTime($current_to->format('H'), 0, 0); // Remove the minutes and seconds.
    $current_from = clone $current_to;
    $current_from->setTime(0, 0, 0);

    $compare_from = clone $current_from;
    $compare_from->sub(new \DateInterval('P1W'));
    $compare_to = clone $compare_from;
    $compare_to->add(new \DateInterval('P1D'));

    return [
        'current' => [$current_from, $current_to],
        'compare' => [$compare_from, $compare_to]
    ];
  }


  public function getComparableLast24hsDates() {
    $current_to = $this->getCurrentTime();
    if ($current_to->format('i') < 15) {
      $current_to->sub(new \DateInterval('PT1H')); // Query transactions up to 1 hour ago.
    }
    $current_to->setTime($current_to->format('H'), 0, 0); // Remove the minutes and seconds.
    $current_from = clone $current_to;
    $current_from->sub(new \DateInterval('P1D'));

    $compare_from = clone $current_from;
    $compare_from->sub(new \DateInterval('P1W'));
    $compare_to = clone $current_to;
    $compare_to->sub(new \DateInterval('P1W'));

    return [
        'current' => [$current_from, $current_to],
        'compare' => [$compare_from, $compare_to]
    ];
  }


  public  function getTransactionsBetween($date_from, $date_to) {
    $transactions = [];
    $tickets = [];
    $_transactions = [];
    $conn = $this->doctrine->getConnection();

    $query = "SELECT SUM(amount) as a, SUM(tickets) as t, date FROM TransactionsByAppHourly WHERE date >= :date_from AND date < :date_to GROUP BY date";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to', $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();

    foreach ($stmt->fetchAll() as $row) {
      $_transactions[$row['date']] = [
          'transactions' => (int)$row['a'],
          'tickets'      => (int)$row['t'],
      ];
    }

    $period = new \DatePeriod($date_from, new \DateInterval('PT1H'), $date_to);
    foreach ($period as $d) {
      $i = $d->format('Y-m-d H:00:00');
      if (isset($_transactions[$i])) {
        $transactions[] = $_transactions[$i]['transactions'];
        $tickets[]      = $_transactions[$i]['tickets'];
      } else {
        $transactions[] = 0;
        $tickets[]      = 0;
      }
    }

    return [
        'transactions' => $transactions,
        'tickets'      => $tickets
    ];
  }


  public  function getTransactionsByAppBetween($date_from, $date_to) {
    $trans_by_app = [];
    $_trans_by_app = [];
    $conn = $this->doctrine->getConnection();
    $query = "SELECT app_id, amount, date FROM TransactionsByAppHourly WHERE date >= :date_from AND date < :date_to";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to', $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();

    foreach ($stmt->fetchAll() as $row) {
      if (!isset($_trans_by_app[$row['app_id']])) {
        $_trans_by_app[$row['app_id']] = [];
        $trans_by_app[$row['app_id']] = [];
      }
      $_trans_by_app[$row['app_id']][$row['date']] = (int)$row['amount'];
    }

    $period = new \DatePeriod($date_from, new \DateInterval('PT1H'), $date_to);
    foreach ($period as $d) {
      foreach ($_trans_by_app as $app_id => $_app_trans) {
        $trans_by_app[$app_id][] = isset($_app_trans[$d->format('Y-m-d H:00:00')]) ? $_app_trans[$d->format('Y-m-d H:00:00')] : 0;
      }
    }

    return $trans_by_app;
  }


  public function getRangeLabels($date_from, $date_to) {
    $period     = new \DatePeriod($date_from, new \DateInterval('PT1H'), $date_to);
    $start_hour = (int)$date_from->format('H');
    $labels     = [];

    for ($i = 0; $i < iterator_count($period); $i++) {
      $label = $start_hour + $i;
      if ($label >= 24) {
        $label -= 24;
      }
      $labels[] = ($label < 10 ? '0' : '') . $label . 'hs';
    }

    return $labels;
  }

}