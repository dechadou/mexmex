<?php $view->extend('SocialSnackFrontBundle:X4d:base.html.php'); ?>

<?php $view['slots']->start('section_body'); ?>

<div class="clearfix row">
  <div id="billboard-movies" class="col hidden">
  <?php
  $movies_per_page = 18;
  $movies_cols     = 6;

  echo $view->render('SocialSnackFrontBundle:Partials:bodyMovies.html.php', [
    'movies'          => array_slice( $movies, 0, $movies_per_page ),
    '_movies'         => $_movies,
    'movies_per_page' => $movies_per_page,
    'movies_cols'     => $movies_cols,
  ]);
  ?>
  </div><!-- #billboard-movies -->
</div>

<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script id="tpl-billboard-empty" type="text/template"><?php echo $x4d_empty; ?></script>
<script id="tpl-billboard-unavailable" type="text/template"><?php echo $x4d_unavailable; ?></script>

<script>
(function($){
  $(document).on('user_init', function(e){
    var params = {
      state: e.user.preferred_cinema.state.id,
      attr: {
        v4d: true
      }
    };

    $.getJSON(url_prefix + 'partials/getBillboard', params, function(response){
      var $target, $content;

      if (response.status === 'unavailable') {
        $content = $('#tpl-billboard-unavailable').html();
        $target  = $('.section-content');
      }

      if (response.items.length === 0) {
        $content = $('#tpl-billboard-empty').html();
        $target  = $('.section-content');
      } else {
        $content = render_movie_grid(response.items, {colClass : 'col-sm-1-5 col-md-1-6'});
        $target  = $('#billboard-movies .movies-grid');
      }

      $target.html($content);
    })
    .always(function(){
      $('.section-content, #billboard-movies').attr('style', 'display:block!important');
    });
  });
})(jQuery);
</script>
<?php $view['slots']->stop(); ?>
