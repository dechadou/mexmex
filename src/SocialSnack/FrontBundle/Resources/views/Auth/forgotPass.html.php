<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Recuperar contraseña - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('user_forgot_pass'); ?>">Recuperar Contraseña</a></li>
</ul>

<div class="clearfix row">
	
	<div class="narrow-side-col col">
    <?php
    $menu = array(
        array(
          'label' => 'Iniciar sesión',
          'route' => 'user_login',
          'class' => 'icon-user',
        ),
        array(
          'label' => 'Registro',
          'route' => 'user_register',
          'class' => 'icon-user',
        ),
    );
    ?>
    <ul class="panel-menu">
      <?php
      foreach ( $menu as $item ) {
        if ( !is_array( $item['route'] ) ) {
          $item['route'] = array( $item['route'] );
        }
        $current = in_array( 'user_login', $item['route'] );
      ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
      <?php
      }
      ?>
    </ul>
  </div>
  
  <div class="wide-main-col col">
    <div class="content-box">
      <h1 class="content-box-title">Recuperar contraseña</h1>
      
      <p class="checkout-legend">
        Ingresa tu email para recuperar tu contraseña.
      </p>
      
      <?php
      echo $view->render(
          'SocialSnackFrontBundle::flashMsgs.html.php',
          array( 'flash_msgs' => $flash_msgs )
      );
      ?>
      
      <form action="<?php echo $view['router']->generate( 'user_forgot_pass' ); ?>" method="post" data-basicvalidate="1" data-validcb="pass_recovery_submit">
        <div class="std-form clearfix">
          <div class="form-row half">
            <label for="login-user">Email</label>
            <input type="text" name="email" id="login-email" placeholder="Email" data-validators="not_empty email" data-flyvalidate="1" />
          </div>
          <div class="form-row clear">
            <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Continuar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>