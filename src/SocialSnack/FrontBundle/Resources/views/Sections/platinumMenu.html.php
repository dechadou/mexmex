<?php
$menu = array(
    array(
        'label' => 'Cartelera',
        'route' => 'platinum',
    ),
    array(
        'label' => 'Cines',
        'route' => 'platinum_cinemas',
    ),
    array(
        'label' => 'Platino',
        'route' => 'platinum_facilities',
    ),
    array(
        'label' => 'Menú Platino',
        'route' => 'platinum_menu',
    ),
);
?>
<div class="section-header section-platinum">
  <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-header.jpg'); ?>" alt="Cinemex Platino" />

  <div class="section-nav">
    <ul>
      <?php foreach ( $menu as $item ) { ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'] ); ?>" class="<?php if ( $route == $item['route'] ) echo 'selected'; ?>"><?php echo $item['label']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
