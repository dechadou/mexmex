<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceStatus
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ServiceStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=100)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="args", type="string", length=100)
     */
    private $args;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_checked", type="datetime")
     */
    private $dateChecked;


    public function __construct()
    {
        $this->dateChecked = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return ServiceStatus
     */
    public function setService($service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return string 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set args
     *
     * @param string $args
     * @return ServiceStatus
     */
    public function setArgs($args)
    {
        $this->args = $args;
    
        return $this;
    }

    /**
     * Get args
     *
     * @return string 
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ServiceStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dateChecked
     *
     * @param \DateTime $dateChecked
     * @return ServiceStatus
     */
    public function setDateChecked($dateChecked)
    {
        $this->dateChecked = $dateChecked;
    
        return $this;
    }

    /**
     * Get dateChecked
     *
     * @return \DateTime 
     */
    public function getDateChecked()
    {
        return $this->dateChecked;
    }
}
