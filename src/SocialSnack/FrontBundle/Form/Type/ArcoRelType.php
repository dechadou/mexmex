<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class ArcoRelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name',  null, array('label' => 'Primer nombre'));
        $builder->add('middle_name', null, array('label' => 'Segundo nombre'));
        $builder->add('last_name',   null, array('label' => 'Apellido paterno'));
        $builder->add('mlast_name',  null, array('label' => 'Apellido materno'));
        $builder->add('email',       'email', array('label' => 'Email'));
    }

    public function getName()
    {
        return 'arco_rep';
    }
}