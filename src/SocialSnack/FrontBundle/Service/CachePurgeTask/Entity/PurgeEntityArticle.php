<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityArticle extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('esi_news_footer'),
            'env'  => 'front'
        ],
        [
            'path' => $this->frontUtils->get_permalink($this->entity),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('news_archive'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('home'),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\ArticleRepository::findActive:0',
                'SocialSnack\FrontBundle\Entity\ArticleRepository::findActive:1',
            ],
            'env' => 'front'
        ]
    ];
  }

}