<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Los Cabos');
$view['slots']->set('body_class', 'sponsored-los-cabos');
?>

<?php $view['slots']->start('body'); ?>
<a class="los-cabos" href="<?php echo $view['router']->generate('home'); ?>">
  <img src="<?php echo $view['assets']->getUrl('assets/img/landings/LOS_CABOS_PROGRAMACION.jpg'); ?>">
</a>

<style>
.los-cabos,
.los-cabos img {
  display:block;
}

.los-cabos img {
  height:auto;
  max-width:100%;
}
</style>
<?php $view['slots']->stop(); ?>
