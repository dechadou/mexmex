<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TrackingCodeType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder->add('name', 'text', ['label' => 'Nombre']);
    $builder->add('description', 'text', ['label' => 'Descripción', 'required' => FALSE]);
    $builder->add('code', 'textarea', [
        'label' => 'Código',
        'attr'  => ['class' => 'form-control-code'],
    ]);
    $builder->add('active', 'checkbox', [
        'label'    => 'Activo',
        'required' => FALSE
    ]);
  }


  public function getName() {
    return '';
  }

}