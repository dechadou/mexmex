<?php

namespace SocialSnack\WsBundle\Request;

abstract class ConsultaArcoAbstract extends ConsultaAbstract {
  
  const PATH = '/WS_GuardaDatos/wsGuardaDatos.asmx';


  protected function requiresAuth() {
    return FALSE;
  }


  public function getUrl() {
    return 'https://' . $this->current_host . static::PATH;
  }


  protected function doRequest($exec = TRUE) {
    $client = $this->getSoapClient();
    $res    = $client->{ $this->method }( $this->args );
    
		return $this->_parse_res($res);
  }


  /**
   * @return \SoapClient
   */
  protected function getSoapClient() {
    $wsdl = $this->getUrl() . '?WSDL';
    return new \SoapClient($wsdl);
  }


  protected function _parse_res( $res ) {
		return $this->parse_res($res);
	}
  
}