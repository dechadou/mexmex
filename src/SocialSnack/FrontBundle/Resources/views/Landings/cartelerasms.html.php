<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Consulta la cartelera');
$view['slots']->set('body_class', 'sponsored-99000');
?>

<?php $view['slots']->start('body'); ?>
<div id="landing-99000">
    <img src="<?php echo $view['assets']->getUrl('promos/Pop_up_Re_9900.png', 'CMS'); ?>">

    <h1>CONSULTA LA CARTELERA MANDANDO UN MENSAJE DE TEXTO AL 99000 preguntando como tu quieras</h1>

    <p>Servicio disponible para usuarios Telcel, Movistar, Iusacell y Nextel.
        Costo del servicio $2.50 I.V.A. incluido por mensaje.
        Responsable del servicio TIMw.e México, S.A. de C.V.</p>
</div>
<?php $view['slots']->stop(); ?>
