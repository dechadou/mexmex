<?php
 
namespace SocialSnack\AdminBundle\Entity;
 
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;
 
/**
 * @ORM\Entity
 * @ORM\Table(name="admin_role")
 * @ORM\Entity(repositoryClass="AdminRoleRepository")
 */
class AdminRole implements RoleInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
 
    /**
     * @ORM\Column(name="role", type="string", length=255, unique=true)
     */
    protected $role;
    
    public function getId(){
        return $this->id;
    }
    public function getRole(){
        return $this->role;
    }
    public function getName(){
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AdminRole
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return AdminRole
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }
    
    /**
     * Serializes the content of the current User object
     * @return string
     */
    public function serialize()
    {
        return \json_encode(
                array($this->role, $this->name, $this->id));
    }

    /**
     * Unserializes the given string in the current User object
     * @param serialized
     */
    public function unserialize($serialized)
    {
        list($this->role, $this->name, $this->id) = \json_decode(
                $serialized);
    }
}