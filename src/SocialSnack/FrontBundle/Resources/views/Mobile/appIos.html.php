<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cinemex</title>

        <link href='//fonts.googleapis.com/css?family=Roboto:300,400,500,400italic,700|Roboto+Condensed:700' rel="stylesheet">

<style>
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

*:before,
*:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

html,
body {
    height:100%;
}

html,
body,
p {
    margin:0;
    padding:0;
}

html {
    font-size:62.5%;
    height:100%;
}

body {
    background:#f7f7f7;
    color:#707070;
    font:300 1.5rem/2.2rem roboto,helvetica,sans-serif;
    height:100%;
    text-align:center;
}

strong {
    font-weight:500;
}

a {
    display:block;
    text-decoration:none;
}

img {
    display:block;
    height:auto;
    max-width:100%;
}

.container {
    margin-left:auto;
    margin-right:auto;
    max-width:595px;
}

#page {
    background:url(<?php echo $view['assets']->getUrl('assets/img/mobile/bg.jpg'); ?>) no-repeat 50% 100%;
    background-size:cover;
    height:auto;
    min-height:100%;
    margin-bottom:-3.5rem;
    padding-bottom:3.5rem;
}

#header {
    background:white;
    border-bottom:1px solid #e9e9e9;
    line-height:5rem;
    height:5rem;
    text-transform:lowercase;
    top:0;
}

#header img {
    float:left;
    height:auto;
    margin:1rem;
    width:11rem;
}

#header a {
    color:#b31649;
    float:right;
    padding:0 1rem;
}

#body a {
    color:#707070;
    display:block;
    overflow:hidden;
    padding:2rem 1rem;
}

#body .device {
    margin:2rem auto;
    width:50%;
}

#body .copy {
    color:black;
    float:left;
    font-size:1.3rem;
    text-align:left;
}

#body .store {
    float:right;
    height:4.4rem;
}

#footer {
    background:#b31649;
    bottom:0;
    font-size:1.3rem;
    line-height:3.5rem;
}

#footer a {
    color:white;
}

@media screen and (min-width: 768px) {
    html {
        font-size:81.25%;
    }

    #body .device {
        width:75%;
    }

    #body .copy {
        font-size:1.5rem;
    }

    #body .banner {
        margin:0 3rem;
    }
}
</style>
    </head>
    <body>
        <div id="page">
            <header id="header">
                <div class="container">
                    <img src="<?php echo $view['assets']->getUrl('assets/img/mobile/logo.svg'); ?>" alt="Cinemex">

                    <p><a href="<?php echo $view['router']->generate('skip_store_landing', $redirect_params); ?>">Ir a cinemex.com</a></p>
                </div>
            </header>

            <div id="body">
                <a href="https://itunes.apple.com/mx/app/cinemex/id418163740">
                    <div class="container">
                        <p class="title"><strong>Con la nueva aplicación de Cinemex</strong><br>
                            podrás comprar de forma fácil y sencilla<br>
                            los boletos para tus películas favoritas.</p>

                        <img class="device" src="<?php echo $view['assets']->getUrl('assets/img/mobile/device-apple.png'); ?>" alt="">

                        <div class="banner">
                            <p class="copy">Descarga nuestra <strong>aplicación<br> gratis</strong> desde el Apple Store</p>

                            <img class="store" src="<?php echo $view['assets']->getUrl('assets/img/mobile/store-apple.svg'); ?>" alt="">
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <footer id="footer">
            <a href="<?php echo $view['router']->generate('skip_store_landing', $redirect_params); ?>">
                <p class="container">Continuar a Cinemex.com</p>
            </a>
        </footer>
    </body>
</html>
