<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['news-single']); ?>
<?php $view['slots']->set('title', $article->getTitle() . ' - Cinemex'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="canonical" href="<?php echo $view['fronthelper']->get_permalink($article, TRUE); ?>" />
<link rel="alternate" href="cinemex://com.cinemex/article/<?php echo $article->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'og_tags' ); ?>
<meta property="og:image" content="https:<?php echo $view['assets']->getUrl('assets/img/' . $article->getCover()); ?>" />
<meta property="og:type"  content="article" />
<meta property="og:title" content="<?php echo $article->getTitle(); ?>" />
<meta property="og:description" content="<?php echo substr( strip_tags($article->getContent()), 0, 200 ); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('news_archive'); ?>">Novedades</a></li>
</ul>

<div class="clearfix row">
  <div class="col col-md-3-4">
    <article class="entry-full" itemscope itemtype="http://schema.org/Article">
      <img src="<?php echo $view['fronthelper']->get_img_size($article->getCover(), '733x286', 'CMS'); ?>" alt="<?php echo $article->getTitle(); ?>" class="entry-img" itemprop="image" />
      <h1 class="entry-title" itemprop="name"><?php echo $article->getTitle(); ?></h1>
      <ul class="entry-meta-head">
        <li itemprop="datePublished" content="<?php echo $article->getDateCreated()->format('Y-m-d'); ?>"><span class="icon-small icon-calendar icon-dark"></span> <?php echo FrontHelper::format_date($article->getDateCreated(), 'dd \'de\' LLLL \'de\' yyyy')?></li>
        <li><span class="icon-small icon-comments icon-dark"></span> <fb:comments-count href="<?php echo $this_url; ?>"></fb:comments-count> comentarios</li>
      </ul>
      
      <div class="entry-body" itemprop="articleBody">
        <?php echo $article->getContent(); ?>
      </div>
      
      <div class="entry-share">
        <a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="<?php echo $this_url; ?>" data-text="" data-via="cinemex">Tweet</a>

        <div class="g-plusone" data-size="medium" data-count="true" data-href="<?php echo $this_url; ?>"></div>

        <div class="fb-like" data-send="false" data-layout="button_count" data-width="1" data-show-faces="false" data-action="recommend"></div>
      </div>
      
      <div class="content-box-bg">
        <div class="fb-comments" data-href="<?php echo $this_url; ?>" data-colorscheme="light" data-numposts="5" data-width="659"></div>
      </div>
        
    </article>
  </div>
  <div class="col col-md-1-4">
    <span class="bottom-title">
      <span class="discicon icon-star icon-red"></span>
      Relacionadas
    </span>
    
    <?php foreach ( $related as $_article ) { ?>
    <?php $link = $view['router']->generate( 'news_single', array('id' => $_article->getId(), 'slug' => FrontHelper::sanitize_for_url($_article->getTitle())) ); ?>
    <article class="col-sm-1-3 col-md-1 news-grid-item size11">
      <a href="<?php echo $link; ?>"><img src="<?php echo $view['fronthelper']->get_img_size($_article->getThumb(), '236x222', 'CMS'); ?>" alt="" /></a>
      <h1 class="news-grid-title"><a href="<?php echo $link; ?>"><?php echo FrontHelper::shorten($_article->getTitle(), 50 ); ?></a></h1>
    </article>
    <?php } ?>
  </div>
</div>
<?php $view['slots']->stop(); ?>