<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Doctrine\Bundle\DoctrineBundle\Registry;

class PurgeDatesByArea implements MemcachedTaskInterface {

  protected $doctrine;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  public function getMemcachedResources() {
    $states = $this->getStatesIds();
    $areas  = $this->getAreasIds();
    $ids    = [];
    $base   = 'findAvailableDatesBy%s:%s';

    foreach ($states as $state) {
      $ids[] = sprintf($base, 'state', $state['id']);
    }

    foreach ($areas as $area) {
      $ids[] = sprintf($base, 'area', $area['id']);
    }

    return $ids;
  }


  protected function getStatesIds() {
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:State')->createQueryBuilder('q');
    $qb
        ->select('q.id')
        ->where('q.active = 1')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }


  protected function getAreasIds() {
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:StateArea')->createQueryBuilder('q');
    $qb
        ->select('q.id')
        ->where('q.active = 1')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }

}