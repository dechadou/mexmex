<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['billboard']);
$view['slots']->set('title', 'Cartelera de ' . $area->getName() . ' - Cinemex');
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <?php if ( $is_state ) { ?>
  <li><a href="<?php echo $view['router']->generate('billboard', array('area_type' => 'estado', 'area_id' => $area->getId())); ?>"><?php echo $area->getName(); ?></a></li>
  <?php } else { ?>
  <li><span><?php echo $area->getState()->getName(); ?></span></li>
  <li><a href="<?php echo $view['router']->generate('billboard', array('area_id' => $area->getId())); ?>"><?php echo $area->getName(); ?></a></li>
  <?php } ?>
</ul>

<div class="row clearfix">
  <div class="col narrow-side-col col-sm-hidden">
    <div class="sticky-sidebar" id="billboard-side">
      <div class="sidebar-box">
        <div class="sidebar-box-title">Filtros de películas</div>
        <div class="sidebar-box-content">
          <div>
            <select id="billboard-area">
              <option value="">Selecciona tu ciudad</option>
              <?php foreach ( $states as $_state ) { ?>
              <optgroup label="<?php echo $_state->getName(); ?>">
                <?php if ( sizeof($_state->getAreas()) > 1 ) { ?>
                <option value="estado-<?php echo $_state->getId(); ?>" data-display-text="<?php echo $_state->getName(); ?>" <?php if ( $is_state && $area->getId() == $_state->getId() ) echo ' selected="selected"'; ?>>Buscar todo <?php echo $_state->getName(); ?></option>
                <?php } ?>
                <?php foreach ( $_state->getAreas() as $_area ) { ?>
                <option value="zona-<?php echo $_area->getId(); ?>" <?php if ( !$is_state && $area->getId() == $_area->getId() ) echo ' selected="selected"'; ?>><?php echo $_area->getName(); ?></option>
                <?php } ?>
              </optgroup>
              <?php } ?>
            </select>
          </div>
          <div>
            <input type="text" placeholder="Buscar un cine" class="std-field" id="billboard-cinema-search" />
          </div>
          <div>
            <select id="billboard-movie">
              <option value="">Todas las películas</option>
              <?php foreach ( $movies as $movie ) { ?>
              <option value="<?php echo $movie->getId(); ?>" <?php if (isset($args['movie']) && $movie->getId() == $args['movie']) echo ' selected="selected"'; ?>><?php echo $movie->getName(); ?></option>
              <?php } ?>
            </select>
          </div>
          <div>
            <select id="billboard-date">
              <?php
              $today = new \DateTime( 'today' );
              $tomorrow = new \DateTime( 'tomorrow' );

              foreach ( $available_dates as $_date ) {
                  $_date     = $_date['date'];
                  $date_lbl = '';

                  if ( $today->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
                      $date_lbl = 'Hoy, ';
                  } else if ( $tomorrow->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
                      $date_lbl = 'Mañana, ';
                  }

                  $formatter = new \IntlDateFormatter( \Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC' );
                  $formatter->setPattern( 'EEEE dd \'de\' MMMM' );
                  $date_lbl .= $formatter->format( $_date );
                  $date_lbl = ucfirst( $formatter->format( $_date ) );
              ?>
                  <option value="<?php echo $_date->format( 'Ymd' ); ?>"<?php if ( $date->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) echo ' selected="selected"'; ?>><?php echo $date_lbl; ?></option>
              <?php } ?>
            </select>
          </div>
          <a href="#" class="btn btn100" id="no-platinum-bt"><i class="icon icon-cinemex no-txt"></i> <span>Ver todas las salas</span></a>
          <a href="#" class="btn btn100" id="platinum-bt"><i class="icon icon-platinum no-txt"></i> <span>Ver salas Platino</span></a>
        </div>
      </div>

      <div class="sidebar-box">
        <div class="sidebar-box-title">Formato</div>
        <div class="sidebar-box-content">
          <div class="check-row no-platinum"><label><input type="checkbox" name="format-filter" value="trad" /> <span>Salas tradicionales</span></label></div>
          <div class="check-row no-platinum"><label><input type="checkbox" name="format-filter" value="premium" /> <span>Salas Premium</span></label></div>
          <div class="check-row no-platinum"><label><input type="checkbox" name="format-filter" value="cx" /> <span>CinemeXtremo</span></label></div>
          <div class="check-row"><label><input type="checkbox" name="format-filter" value="v3d" /> <span>3D</span></label></div>
          <div class="check-row no-platinum"><label><input type="checkbox" name="format-filter" value="v4d" /> <span>X4D</span></label></div>
        </div>
      </div>

      <div class="sidebar-box">
        <div class="sidebar-box-title">Idioma</div>
        <div class="sidebar-box-content">
          <div class="check-row"><label><input type="radio" name="lang-filter" value="" checked="checked" /> <span>Todos los idiomas</span></label></div>
          <div class="check-row"><label><input type="radio" name="lang-filter" value="es" /> <span>Español</span></label></div>
          <div class="check-row"><label><input type="radio" name="lang-filter" value="en" /> <span>Inglés</span></label></div>
        </div>
      </div>

      <div class="sidebar-box">
        <div class="sidebar-box-title">Horario</div>
        <div class="sidebar-box-content">
          <div id="hours-range"></div>
          <div class="align-center">
            <i class="icon icon icon-dark icon-clock no-txt"></i>
            <span id="horus-range-min"></span> -
            <span id="horus-range-max"></span>
          </div>
        </div>
      </div>
      <div class="sidebar-box">
        <a href="#" class="btn btn100 btn-icon icon icon-reload btn-light" id="bb-reset-bt">Limpiar filtros</a>
      </div>
    </div>
    
  </div>

  <div class="col wide-main-col col-sm-1" id="billboard-main">

    <?php echo $this->render('SocialSnackFrontBundle:Partials:billboard/block.html.php', array( 'data' => $data )); ?>

  </div>
</div>

<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script>
var _current_state = {
  hasPremium  : <?php echo $has_premium  ? 'true' : 'false'; ?>,
  hasPlatinum : <?php echo $has_platinum ? 'true' : 'false'; ?>,
  hasX4d      : <?php echo $has_x4d      ? 'true' : 'false'; ?>,
  hasCx       : <?php echo $has_cx       ? 'true' : 'false'; ?>,
  has3d       : <?php echo $has_v3d      ? 'true' : 'false'; ?>
};
</script>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after'); ?>
<script>
var area_id = <?php echo $is_state ? 'null' : $area->getId(); ?>;
var state_id = <?php echo $is_state ? $area->getId() : $area->getState()->getId(); ?>;
var is_state = <?php echo $is_state ? 'true' : 'false'; ?>;
var sticky_main, sticky_side;
var args = <?php echo json_encode($args); ?>;
var default_hours = ['<?php echo ($min_time < 9) ? '0' : '9' ?>.00', '24.00'];
var data_loaded = <?php echo $is_state ? 'false' : 'true'; ?>;
</script>
<?php foreach ($view['assetic']->javascripts(
    array(
        '@SocialSnackFrontBundle/Resources/public/js/modules/billboard.js',
    ),
    array('?yui_js')
) as $url): ?>
    <script src="<?php echo $view->escape($url) ?>?v4"></script>
<?php endforeach; ?>
<?php $view['slots']->stop(); ?>
