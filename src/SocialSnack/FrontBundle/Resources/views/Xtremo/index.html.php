<?php
$view->extend('SocialSnackFrontBundle:Xtremo:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">CinemeXtremo</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-03.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-04.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-02.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-01.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-03.jpg'); ?>"></a>
          <a class="grid-col-2  " href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/cinemextremo-04.jpg'); ?>"></a>
        </div>
      </div>
    </div>

    <div class="grid-col-5">
      <p class="lead">Vive la Magia del Cine, con la máxima intensidad y potencia, ahora en CinemeXtremo.</p>

      <p>Podrás disfrutar al máximo cada instante de tu película favorita en nuestras pantallas con un tamaño excepcional y una proyección digital que te permitirán gozar hasta el mínimo detalle.</p>

      <h2>Sonido 360°</h2>
      <p>Vive una experiencia de audio envolvente dentro de nuestras salas equipadas con el sistema de sonido Dolby Atmos, que te ofrece una mayor definición y fidelidad de cada detalle de audio.</p>
      <p>Encontrarás bocinas por todos lados de la sala que te permitirán sentir el sonido hasta los huesos con 30.000 watts de potencia y 24 canales de sonido envolvente.</p>

      <h2>Butacas envolventes</h2>
      <p>Nuestras butacas tienen un diseño ergonómico y deportivo que te permitirán vivir tu película con una comodidad inigualable y extrema.</p>

      <p class="lead">Atrévete a vivirlo, en CinemeXtremo!</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
