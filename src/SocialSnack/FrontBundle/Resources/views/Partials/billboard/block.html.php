<?php
foreach ( $data as &$_cinema ) {
  $cinema   = $_cinema['cinema'];
  $sessions = $_cinema['sessions'];
?>
<div class="billboard-block" <?php if ( TRUE === $cinema->getPlatinum() ) echo 'data-platinum="1"'; ?> id="bb-cinema-<?php echo $cinema->getId(); ?>">
  <h2 class="billboard-block-title">
    <?php echo $cinema->getName(); ?>
    
    <a href="#" class="bb-scroll-top">Ir al inicio</a>
  </h2>
  <?php if (is_string($value = $cinema->getData('DIRECCION'))) { ?>
  <p class="billboard-address">
    <i class="icon icon-marker icon-dark no-txt"></i>
    <span class="address"><?php echo $value; ?></span>
    <a href="<?php echo $view['fronthelper']->get_cinema_link($cinema); ?>" class="more-info">Más info</a>
  </p>
  <?php } ?>

  <div class="billboard-movies">
    <?php foreach ($sessions as $_movie) : ?>
    <div class="billboard-li" data-movie-id="<?php echo $_movie['parent']->getId(); ?>">
      <span class="mycinema-item-rating" title="Clasificación"><?php echo $_movie['parent']->getData('RATING'); ?></span>

      <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="billboard-movie-poster">
        <img src="<?php echo $view['fronthelper']->get_poster_url($_movie['parent']->getPosterUrl(), 'billboard'); ?>" alt="<?php echo $_movie['parent']->getName(); ?>" width="60" height="89"/>
      </a>
      <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="mycinema-item-title"><?php echo $_movie['parent']->getName(); ?></a>

      <?php
      echo $this->render('SocialSnackFrontBundle:Partials:sidebar/movieSessions.html.php', array(
          'items'    => $_movie['sessions'],
          'platinum' => $cinema->getPlatinum(),
          'checkout_ref' => 'bb',
      ));
      ?>
    </div>
    <?php endforeach; ?>
  </div>
</div>
<?php } ?>