<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

class OptionRepository extends EntityRepository {
  
  public function getBlockedIPs() {
    $qb = $this->createQueryBuilder('o');
    $qb->select('o.value')->where('o.name = :name')->setParameter('name', 'block_ips');
    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 1 * 60, 'Option:' . __METHOD__);
    $res = $query->getResult();
    if ($res) {
      return array_map(function($a) { return trim($a, '"'); }, explode(',', $res[0]['value']));
    } else {
      return array();
    }
  }
  
}