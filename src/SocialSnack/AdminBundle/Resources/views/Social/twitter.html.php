<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de películas a aprobar - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Twitter
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <form method="post" id="the-form">
            <div class="clearfix">
              <table  class="display table table-bordered table-striped" id="example">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Usuario</th>
                    <th>Tweet</th>
                    <th>Activo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($items as $item) { ?>
                  <tr class="gradeX">
                    <td><?php echo $view['form']->widget($form['aprobar_' . $item->getId()]); ?></td>
                    <td><?php echo $item->getScreenName(); ?></td>
                    <td><?php echo $item->getText(); ?></td>
                    <td><?php echo $item->getActive() ? 'Sí' : 'No'; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="clearfix">
              <div class="form-inline">
                <div class="form-group col-sm-3">
                  <?php echo $view['form']->widget($form['bulk_action']); ?>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Procesar</button>
                </div>
              </div>
            </div>
            
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[4, "desc"]]
    });
  });
  $('#the-form').bind('submit', function(e) {
    if (!$('[name^="form[aprobar_"]:checked').length) {
      alert('Debes seleccionar al menos un elemento');
      return false;
    }
  });
</script>
<?php $view['slots']->stop(); ?>