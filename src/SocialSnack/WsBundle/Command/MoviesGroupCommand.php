<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\MovieInfo;
use SocialSnack\WsBundle\Request\ConsultaDetallada;

use SocialSnack\WsBundle\Service\Helper    as WsHelper;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class MoviesGroupCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:movies:group' )
			->setDescription( 'Group movies' );
	}
	
	
	/**
	 * Fetch movies and movies info from the WS.
	 * 
	 * Since it doesn't seem to exist an effective method to retrieve the complete
	 * movie listing from the WS (all methods seems to return each movie repeated
	 * for every single cinema resulting on an unnecessary huge response with
	 * occasional timeouts) we are requesting movies by state to limit the size of
	 * the response.
	 * 
	 * @param \Symfony\Component\Console\Input\InputInterface $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 * @return type
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$time_start = microtime(true); 

		$doctrine   = $this->getContainer()->get( 'doctrine' );
    $em         = $doctrine->getManager();
    $movie_repo = $doctrine->getRepository( 'SocialSnackWsBundle:Movie' );
    $conn       = $doctrine->getConnection();
    $attributes = array_keys(FrontHelper::get_attributes_collection(array('version' => FALSE)));
    $set_parent = "UPDATE Movie SET parent_id = :parent_id
        WHERE
          `type` = 'single'
          AND
          `group_id` = :group_id";
    $active_sql = "SELECT COUNT(*) as cnt FROM Movie m INNER JOIN Session s ON m.id = s.movie_id
        WHERE s.active = 1 AND m.id = :movie_id";

    $this->setDefaultGroupIds();

    $groups = $this->findGroupIds();

    foreach ( $groups as $group ) {
      // Get Grouper (if exists).
      $parent = $movie_repo->findOneBy( array( 'type' => 'grouper', 'group_id' => $group['group_id'] ) );
      
      // Find children.
      $qb = $movie_repo->createQueryBuilder('m');
      $qb->select('m')->where('m.type = \'single\'')->andWhere('m.group_id = :group_id')->setParameter('group_id', $group['group_id']);
      $q = $qb->getQuery();
      $children = $q->getResult();
      if (!$children) {
        continue;
      }
      
      // If the grouper doesn't exists yet, let's create it.
      if ( !$parent ) {
        $parent = new Movie();
        $parent->setType( 'grouper' );
        $parent->setGroupId( $group['group_id'] );
        $name = '';
        $active = 0;

        // Get the movie title from its children's titles.
        foreach ( $children as $child ) {
          if ( !$name ) {
            $name = $child->getName();
            $active |= $child->getActive(); // If at least one child is active, make the grouper active.
            continue;
          }
          $name = WsHelper::get_longest_common_subsequence( $name, $child->getName() );
        }
        $parent->setName( trim( $name ) );
        $parent->setShortName( trim( $name ) );
        $parent->setInfo( $child->getInfo() );
        $parent->setPremiere( $child->getPremiere() );
        $parent->setPosterUrl( $child->getPosterUrl() );
        $parent->setActive( $active );
        $em->persist( $parent );
      }
      
      // Only keep non-version attributes.
      // Version attributes will be added later from those versions which still
      // have active sessions.
      foreach ( $parent->getAttr() as $k => $v ) {
        if ( !$v || !in_array($k, $attributes) ) {
          $parent->unsetAttr($k);
        }
      }
      
      // Get attributes and premiere flag from children.
      // Set children's parent.
      $premiere = FALSE;
      foreach ( $children as $child ) {
        $child->setParent( $parent );
        
        // The parent should have the attributes from all of its children, only
        // if they have active sessions. If their not, continue the foreach loop.
        $stmt = $conn->prepare( $active_sql );
        $stmt->bindValue('movie_id', $child->getId());
        $stmt->execute();
        $is_active = $stmt->fetchColumn();
        
        if ( !$is_active )
          continue;
        
        // Assign attributes to its parent.
        foreach ( $child->getAttr() as $k => $v ) {
          $parent->setAttr( $k, $v );
        }
        if ( $child->getPremiere() ) {
          $premiere = TRUE;
        }
      }
      $parent->setPremiere($premiere);
      $em->flush();
    }
    
		$output->writeln( 'Done.' );
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		
		$output->writeln( 'Total Execution Time: '. $execution_time . ' Mins' );
		
		
		
		return;
	}


  /**
   * Sets default group IDs for movies without grouping at the WS.
   * Workaround for the ugly grouping system used by the WS.
   */
  protected function setDefaultGroupIds() {
    $doctrine = $this->getContainer()->get( 'doctrine' );
    $conn     = $doctrine->getConnection();
    $no_group_s = "UPDATE Session SET group_id = CONCAT('ng', movie_id) WHERE group_id = '0'";
    $no_group_m = "UPDATE Movie SET group_id = CONCAT('ng', id) WHERE group_id = '0'";
    $conn->executeUpdate( $no_group_s );
    $conn->executeUpdate( $no_group_m );
  }


  /**
   * Find existing group IDs.
   *
   * @return array
   */
  protected function findGroupIds() {
    $doctrine = $this->getContainer()->get( 'doctrine' );
    $conn     = $doctrine->getConnection();
    $sql      = "SELECT group_id, COUNT(*) cnt FROM Movie m
        WHERE
          `type` = 'single'
        GROUP BY group_id
        HAVING cnt > 1";
    return $conn->fetchAll( $sql );
  }
	
	
}