<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Http\CsvResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CinemaController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }

  public function getAttributes(){
    $this->check_roles();

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Attribute');
    $attributes = $repo->findAll();
    $rv = array();
    foreach($attributes as $attribute){
      $rv[$attribute->getId()] = $attribute->getName();
    }

    return $rv;
  }
  

  public function indexAction() {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas = $repo->findAll(FALSE, FALSE);

    return $this->render('SocialSnackAdminBundle:Cinema:list.html.php', array('cinemas' => $cinemas));
  }

  public function editAction($cinema_id, Request $request) {
    $this->check_roles();
    
    $purge = NULL;
    $em = $this->get('doctrine')->getManager();
    $area_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:StateArea');
    $zones = $area_repo->findAll();

    $zonearray = array();
    $zonearray[0] = "Seleccione la zona";
    foreach ($zones as $zone) {
      $zonearray[$zone->getId()] = $zone->getName();
    }

    $state_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:State');
    $states = $state_repo->findAll();

    $statearray = array();
    $statearray[0] = "Seleccione el estado";
    foreach ($states as $state) {
      $statearray[$state->getId()] = $state->getName();
    }

    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
    $cinema = $repo->find($cinema_id, FALSE, 0, NULL, FALSE);
    $form = $this->createFormBuilder()
        ->add('name', 'text', array(
            'data' => (is_string($cinema->getName())) ? $cinema->getName() : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('active', 'checkbox', array(
            'required' => false,
            'data' => ($cinema->getActive() == 1) ? true : false
        ))
        ->add('platinum', 'checkbox', array(
            'required' => false,
            'data' => ($cinema->getPlatinum() == 1) ? true : false
        ))
        ->add('state_id', 'choice', array(
            'choices' => $statearray,
            'data' => $cinema->getState()->getId()
        ))
        ->add('area_id', 'choice', array(
            'choices' => $zonearray,
            'data' => $cinema->getArea()->getId()
        ))
        ->add('lat', 'text', array(
            'data' => (string) $cinema->getLat(),
            'attr' => array('class' => 'form-control')
        ))
        ->add('lng', 'text', array(
            'data' => (string) $cinema->getLng(),
            'attr' => array('class' => 'form-control')
        ))
        ->add('DIRECCION', 'text', array(
            'required' => false,
            'data' => (is_string($cinema->getData('DIRECCION'))) ? $cinema->getData('DIRECCION') : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('TELEFONOS', 'text', array(
            'required' => false,
            'data' => (is_string($cinema->getData('TELEFONOS'))) ? $cinema->getData('TELEFONOS') : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('COLONIA', 'text', array(
            'required' => false,
            'data' => (is_string($cinema->getData('COLONIA'))) ? $cinema->getData('COLONIA') : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('CONTACTO', 'text', array(
            'required' => false,
            'data' => (is_string($cinema->getData('CONTACTO'))) ? $cinema->getData('CONTACTO') : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('EMAIL', 'email', array(
            'required' => false,
            'data' => (is_string($cinema->getData('EMAIL'))) ? $cinema->getData('EMAIL') : '',
            'attr' => array('class' => 'form-control'),
            'constraints' => array(
                new Assert\Email(),
            ),
        ))
        ->add('attributes', 'choice', array(
            'required' => FALSE,
            'data' => $cinema->getAttributes(),
            'multiple'     => TRUE,
            'expanded'     => TRUE,
            'choices'      => $this->getAttributes(),
        ))
        ->add('save', 'submit')
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $cache_purger = $this->get('ss.cache_purger');
      $data = $form->getData();

      $cinema->setName((string) $data['name']);

      if ($data['active']) {
        $active = 1;
      } else {
        $active = 0;
      }
      $cinema->setActive($active);

      if ($data['platinum']) {
        $platinum = 1;
      } else {
        $platinum = 0;
      }
      $cinema->setPlatinum($platinum);



      $state = $state_repo->find((int) $data['state_id']);
      $area = $area_repo->find((int) $data['area_id']);

      // If the cinema was moved to a different state, the previous state needs
      // to be refreshed too.
      if ($cinema->getState()->getId() != $state->getId()) {
        $cache_purger->enqueue(array(
            'type' => 'urls',
            'urls' => $this->generateUrl('cinemas_archive', array('id' => $cinema->getState()->getId())),
        ));
      }
      
      $update_distances = FALSE;
      if (
             ($cinema->getArea()->getId() != $area->getId())
          || ($cinema->getLat() != $data['lat'])
          || ($cinema->getLng() != $data['lng'])
      ) {
        $update_distances = TRUE;
      }
      
      $cinema->setState($state);
      $cinema->setArea($area);
      $cinema->setLat((float) $data['lat']);
      $cinema->setLng((float) $data['lng']);
      $cinema->setAttributes($data['attributes']);

      $cinema_info = json_decode($cinema->getInfo());
      $cinema_info->CINENAME = (string) $data['name'];
      $cinema_info->DIRECCION = $data['DIRECCION'];
      $cinema_info->TELEFONOS = $data['TELEFONOS'];
      $cinema_info->CONTACTO = $data['CONTACTO'];
      $cinema_info->COLONIA = $data['COLONIA'];
      $cinema_info->EMAIL = $data['EMAIL'];
      $cinema_info->LATITUD = $data['lat'];
      $cinema_info->LONGITUD = $data['lng'];
      $cinema_info->COMPLEJO_INTPLATINO = (string) $platinum;
      $cinema->setInfo(json_encode($cinema_info));

      $em->persist($cinema);
      $em->flush();
//      
//      if ($update_distances) {
//        $this->get('cinemex.cinema_updater')->updateCloser();
//      }
//      
      $cache_purger->enqueue(array('entity' => $cinema));
      $purge = $cache_purger->purge_cache();
    }

    return $this->render('SocialSnackAdminBundle:Cinema:edit.html.php', array(
        'cinema' => $cinema,
        'form'   => $form->createView(),
        'purge'  => $purge
    ));
  }

  public function downloadIdsCsvAction(Request $request) {
    $this->check_roles();

    // Get query params.
    $download = $request->request->get('descargar');

    if (!$download) {
      return $this->render('SocialSnackAdminBundle:Cinema:download-cines.html.php');
    }

    $cinema_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    $qb = $cinema_repo->createQueryBuilder('c');
    $qb->select('c.legacy_id,c.name')
        ->andWhere('c.active = 1')
        ->orderBy('c.legacy_id', 'ASC');
    $query = $qb->getQuery();
    $cinemas = $query->getResult();

    $data = [
        ['ID', 'Complejo']
    ];
    foreach ($cinemas as $cinema) {
      $data[] = [$cinema['legacy_id'], $cinema['name']];
    }

    $response = new CsvResponse($data);
    $response->setFilename('ids-cines.csv');
    $response->setForExcel(TRUE);
    return $response;
  }

}
