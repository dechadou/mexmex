<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\RestBundle\Exception\NotFoundRestException;
use SocialSnack\WsBundle\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SessionsController extends BaseController {
	
	
	/**
	 * @Route("/{session_id}", name="rest_v1_sessions_single")
	 * @Method({"GET"})
	 */
	public function singleAction($session_id) {
    return $this->forward('SocialSnackRestBundle:Sessions:single', ['session_id' => $session_id]);
	}
	
	
}