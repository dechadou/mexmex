<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Landing
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="LandingRepository")
 */
class Landing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     */
    private $slug;

    /**
     * @var array
     *
     * @ORM\Column(name="config", type="json_array")
     */
    private $config;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="LandingPage", mappedBy="landing")
     */
    private $pages;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->config = [];
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Landing
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set config
     *
     * @param array $config
     * @return Landing
     */
    public function setConfig($config)
    {
        $this->config = $config;
    
        return $this;
    }

    /**
     * Get config
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Landing
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add pages
     *
     * @param \SocialSnack\FrontBundle\Entity\LandingPage $pages
     * @return Landing
     */
    public function addPage(\SocialSnack\FrontBundle\Entity\LandingPage $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \SocialSnack\FrontBundle\Entity\LandingPage $pages
     */
    public function removePage(\SocialSnack\FrontBundle\Entity\LandingPage $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }

    public function setConfigValue($key, $value)
    {
        $this->config[$key] = $value;
    }

    public function getConfigValue($key)
    {
        if (!isset($this->config[$key]))
        {
            return NULL;
        }

        return $this->config[$key];
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Landing
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}