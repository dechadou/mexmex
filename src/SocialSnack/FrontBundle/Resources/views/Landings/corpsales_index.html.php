<?php $view->extend('SocialSnackFrontBundle:Landings:corpsales.html.php'); ?>

<?php $view['slots']->start('corpsales_body'); ?>

<div class="content-box">
  <h1 class="content-box-title">Ventas corporativas</h1>

  <h1 class="content-box-subtitle">Boletos empresariales</h1>

  <div class="entry-body">
    <p>Los Boletos Empresariales son boletos con descuento que se venden a empresas o instituciones con la finalidad de ofrecerle a su personal o clientes el beneficio de disfrutar de sus películas favoritas en cualquier complejo Cinemex.</p>

    <div class="special-box">
      <h2 class="special-box-title">Condiciones</h2>

      <div class="special-box-hr"></div>

      <div class="special-box-content">
        <ul>
          <li>Compra mínima de 50 boletos y en múltiplos de 50 en boletos físicos</li>
          <li>Compra mínima de 100 boletos y en múltiplos de 50 en boletos electrónicos</li>
          <li>Vigencia de 3 meses irrenovable</li>
        </ul>
      </div>
    </div>
  </div>

  <h1 class="content-box-subtitle">Función de grupo</h1>

  <div class="entry-body">
    <p>Es un evento al que asisten cómo mínimo 50 personas a precio preferencial para disfrutar de una misma función que se encuentra en cartelera y que queda abierta al público en general.</p>
  </div>

  <h1 class="content-box-subtitle">Función privada</h1>

  <div class="entry-body">
    <p>Es un evento al que asisten una o varias personas a precio preferencial para disfrutar de una misma función que se encuentra en cartelera y sólo pueden ingresar  los invitados.</p>
  </div>

  <h1 class="content-box-subtitle">Renta de sala</h1>

  <div class="entry-body">
    <p>Es la renta del espacio de uno de nuestros auditorios para cualquier actividad o evento que requiera por ejemplo: capacitaciones, presentaciones de productos, etc.</p>
  </div>
</div>

<?php $view['slots']->stop(); ?>
