<?php

namespace SocialSnack\WsBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoviesByAreaCommand extends ContainerAwareCommand {

  public function configure() {
    $this
        ->setName('ws:movies:byarea')
        ->setDescription('Update the lists of movies by area')
    ;
  }

  public function execute(InputInterface $input, OutputInterface $output) {
    $doctrine = $this->getContainer()->get('doctrine');
    $em       = $doctrine->getManager();
    $conn     = $doctrine->getConnection();

    $delete   = $conn->prepare("DELETE FROM MoviesByArea;");
    $insert   = $conn->prepare("INSERT INTO MoviesByArea (movie_id,cinema_id,area_id,state_id) SELECT DISTINCT m0_.id, c2_.id, c2_.area_id, c2_.state_id FROM   Movie m0_ INNER JOIN   Session s1_   ON (m0_.group_id = s1_.group_id) INNER JOIN   Cinema c2_   ON s1_.cinema_id = c2_.id WHERE   m0_.parent_id IS NULL AND s1_.active = 1 AND m0_.active = 1 AND c2_.active = 1;");
    $delete->execute();
    $insert->execute();

    $delete   = $conn->prepare("DELETE FROM MovieVersionsByCinema;");
    $insert   = $conn->prepare("INSERT INTO MovieVersionsByCinema (movie_id,cinema_id,area_id,state_id,group_id,attributes)
        SELECT DISTINCT m0_.id, c2_.id, c2_.area_id, c2_.state_id, m0_.group_id, m0_.attributes
        FROM   Movie m0_ INNER JOIN   Session s1_   ON (m0_.id = s1_.movie_id) INNER JOIN   Cinema c2_   ON s1_.cinema_id = c2_.id
        WHERE   s1_.active = 1 AND m0_.active = 1 AND c2_.active = 1;");
    $delete->execute();
    $insert->execute();

    $delete   = $conn->prepare("DELETE FROM DatesByArea;");
    $insert   = $conn->prepare("INSERT INTO DatesByArea (date,area_id,state_id) SELECT DISTINCT s0_.date, c1_.area_id, c1_.state_id FROM Session s0_ INNER JOIN Cinema c1_ ON s0_.cinema_id = c1_.id WHERE s0_.active = 1;");
    $delete->execute();
    $insert->execute();
  }

}