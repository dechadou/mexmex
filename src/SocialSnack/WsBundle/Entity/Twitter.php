<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Twitter
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="tw_active_idx", columns={"active"})
 * })
 * @ORM\Entity
 */
class Twitter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="legacy_id", type="string", length=100)
     */
    private $legacy_id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255)
     */
    private $thumb;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=100)
     */
    private $user_name;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_name", type="string", length=50)
     */
    private $screen_name;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    
    public function __construct() {
      $this->active = 1;
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legacyId
     *
     * @param string $legacyId
     * @return Twitter
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacyId
     *
     * @return string 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Twitter
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     * @return Twitter
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
    
        return $this;
    }

    /**
     * Get thumb
     *
     * @return string 
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return Twitter
     */
    public function setUserName($userName)
    {
        $this->user_name = $userName;
    
        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set screenName
     *
     * @param string $screenName
     * @return Twitter
     */
    public function setScreenName($screenName)
    {
        $this->screen_name = $screenName;
    
        return $this;
    }

    /**
     * Get screenName
     *
     * @return string 
     */
    public function getScreenName()
    {
        return $this->screen_name;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Twitter
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }
    
    
    public function toArray() {
      return array(
          'id'          => $this->id,
          'legacy_id'   => $this->legacy_id,
          'text'        => $this->text,
          'thumb'       => $this->thumb,
          'user_name'   => $this->user_name,
          'screen_name' => $this->screen_name,
      );
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Twitter
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
}