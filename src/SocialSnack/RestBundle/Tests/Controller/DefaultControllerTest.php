<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadAppData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;

class DefaultControllerTest extends WebTestCase {

  protected $client;

  protected function request($method, $uri, array $parameters = array(), array $files = array(), array $server = array(), $load_fixtures = TRUE) {
    if (!isset($server['HTTP_X-API-Consumer-Key'])) {
      $server['HTTP_X-API-Consumer-Key'] = 'abc';
    }

    if ($load_fixtures) {
      $this->loadFixtures(array(
          '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
      ));
    }

    $this->client = static::createClient();

    $this->client->request(
        $method,
        $uri,
        $parameters,
        $files,
        $server
    );
  }


  public function testDefaultAction() {
    $this->request('GET', '/rest/v2/');
    $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

    // Test valid response.
    $data = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $data);
  }


  public function testVersionAction() {
    $this->request('GET', '/rest/v2/version');
    $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    // Test valid response.
    $data = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $data);
    $this->assertEquals(2, $data->version);
  }


  protected function _testCheckFb($load_fixtures, $fbuid, $expected) {
    $this->request('POST', '/rest/v2/checkFb', ['fbuid' => $fbuid], [], [], $load_fixtures);
    $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    // Test valid response.
    $data = @json_decode($this->client->getResponse()->getContent());
    $this->assertInstanceOf('\stdClass', $data);
    $this->assertEquals($expected, $data->registered);
  }


  public function testCheckFbFound() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ]);

    $this->_testCheckFb(FALSE, LoadUserData::$users_w_fb[0]->getFbuid(), TRUE);
  }


  public function testCheckFbNotFound() {
    $this->_testCheckFb(TRUE, 666, FALSE);
  }


  protected function _testLogin($params, $status_code, $method = 'POST', $validate_json = TRUE) {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ]);

    $this->request(
        $method,
        '/rest/v2/login',
        $params,
        [],
        [],
        FALSE
    );

    $this->assertEquals($status_code, $this->client->getResponse()->getStatusCode());

    // Validate response.
    if ($validate_json) {
      $response = $this->client->getResponse();
      $json     = @json_decode($response->getContent());
      $this->assertInstanceOf('\stdClass', $json);

      return $json;
    }
  }


	public function testLoginWrongMethod() {
    $right_username = 'homero@simpsons.com';
    $right_password = 'foobar';

    // Login must use POST method.
    $params = ['username' => $right_username, 'password' => $right_password];
    $this->_testLogin($params, 405, 'GET', FALSE);
  }


  public function testLoginMissingFields() {
    // Missing username and password.
    $params = [];
    $this->_testLogin($params, 400);
  }


  public function testLoginMissingPassword() {
    $right_username = 'homero@simpsons.com';
    $params = ['username' => $right_username];
    $this->_testLogin($params, 400);
  }


  public function testLoginMissingUsername() {
    $right_password = 'foobar';
    $params = ['password' => $right_password];
    $this->_testLogin($params, 400);
  }


  public function testLoginWrongCredentials() {
    $wrong_username = 'testuser';
    $wrong_password = 'testpass';
    $params = ['username' => $wrong_username, 'password' => $wrong_password];
    $this->_testLogin($params, 403);
  }


  public function testLoginWrongPassword() {
    $right_username = 'homero@simpsons.com';
    $wrong_password = 'testpass';
    $params = ['username' => $right_username, 'password' => $wrong_password];
    $this->_testLogin($params, 403);
  }


  public function testLoginSuccess() {
    $right_username = 'homero@simpsons.com';
    $right_password = 'foobar';
    $params = ['username' => $right_username, 'password' => $right_password];
    $json = $this->_testLogin($params, 200);

    // Access token.
    $this->assertNotEmpty($json->access_token);
    $this->assertModel([
        'avatar'        => 'string',
        'avatar_medium' => 'string',
    ], $json->user_info);
  }


  public function testLoginFB() {
		// @todo Test FB login
	}
	
	
	public function testRegister() {
		$client         = static::createClient();
		$endpoint       = '/rest/v2/register';
		$fields         = array(
			'email'       => uniqid() . '@' . uniqid() . '.com',
			'password'    => uniqid(),
			'first_name'  => uniqid(),
			'last_name'   => uniqid(),
		);
		
		// Register must use POST method.
		$client->request( 'GET', $endpoint, $fields );
		$this->assertEquals( 405, $client->getResponse()->getStatusCode() );
		
		// Correct register request.
    $this->request('POST', '/rest/v2/register', $fields);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		
		// Access token.
		$response = $this->client->getResponse();
		$json     = @json_decode( $response->getContent() );
		$this->assertFalse( is_null( $json ) );
		$this->assertNotEmpty( $json->access_token );
		
		// @todo Test FB register
	}


  public function testRecoverPass() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ]);

    $email = LoadUserData::$users[0]->getEmail();

    $client = static::createClient();

    $client->enableProfiler();

    $client->request(
        'POST',
        '/rest/v2/recoverPass',
        [
            'email' => $email,
        ],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );

    $mailCollector = $client->getProfile()->getCollector('swiftmailer');

    $collectedMessages = $mailCollector->getMessages();
    $message = $collectedMessages[0];

    // Asserting email data
    $this->assertInstanceOf('Swift_Message', $message);

    // Since emails in dev environments are sent to a hardcoded testing address,
    // the actual recipient is stored in the "X-Swift-To" header.
    $x_to = $message->getHeaders()->get('X-Swift-To')->getNameAddresses();
    $this->assertEquals($email, key($x_to));
  }


  /**
   * @todo Test that the entry was actually persisted.
   */
  public function testRegisterGCM() {
    $this->request(
        'POST',
        '/rest/v2/registerGCM',
        [
            'uuid'  => 'foo',
            'gcmid' => 'bar',
            'type'  => 'phpunit'
        ]
    );

    $this->assertTrue($this->client->getResponse()->isSuccessful());
  }

  protected function _testAppVersionsAction($device, $expected_version) {
    $url = sprintf('/rest/v2/appVersions/%s', $device);
    $client = static::createClient();
    $client->request(
        'GET',
        $url,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );
    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $json = @json_decode($response->getContent());
    $this->assertInstanceOf('\stdClass', $json);
    $this->assertObjectHasAttribute('version', $json);
    $this->assertEquals($json->version, $expected_version);
  }

  public function testAppVersionsAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppVersionsData',
    ]);

    $this->_testAppVersionsAction('ios', 'i2.0');
    $this->_testAppVersionsAction('android', 'a2.0');
  }

  public function testLoginApp() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
    ]);

    $url = '/rest/v2/login/app';
    $appId = LoadAppData::$apps[0]->getAppId();
    $appPass = LoadAppData::$apps[0]->getSetting('appPass');

    $this->request(
        'POST',
        $url,
        [
            'username' => $appId,
            'password' => $appPass,
        ],
        [],
        [],
        FALSE
    );
    $this->assertSame(200, $this->client->getResponse()->getStatusCode());
  }

  public function testLoginApp_Invalid() {
    $this->loadFixtures([]);

    $url = '/rest/v2/login/app';
    $appId = 'foo';
    $appPass = 'bar';

    $this->request(
        'POST',
        $url,
        [
            'username' => $appId,
            'password' => $appPass,
        ]
    );
    $this->assertSame(401, $this->client->getResponse()->getStatusCode());
  }

}
