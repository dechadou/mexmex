<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Términos y condiciones - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('tos_page'); ?>">Términos y condiciones</a></li>
</ul>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render( 'SocialSnackFrontBundle:Sections:pagesMenu.html.php', array( 'route' => $route ) ); ?>
  </div>
  <div class="wide-main-col col content-box">
    <article class="entry-full">
      <h1 class="content-box-title">Términos y condiciones</h1>
      
      <div class="entry-body">
        <h3>I. ÁMBITO DE APLICACIÓN</h3>
        <p>Estos Términos, aplican a todos los usuarios, Cine|usuarios y/o personas que de cualquier forma hagan uso y/o utilicen (i) la página de Internet cinemex.com; (ii) cualesquiera de las páginas y/o ligas de cinemex.com; (iii) reservación y/o compra de boletos a través de la página cinemex.com y/o a través de la línea telefónica de Cinemex, vigente para tales efectos; (iv) correo electrónico (v) en general, a cualesquier servicios, productos y/o promociones que se ofrezcan y/o provean en dichas páginas, salvo disposición expresa en específico en contrario en cada uno de dichos servicios.</p>
        <p>Tanto Cinemex como los Usuarios o Cine|usuarios, al momento de hacer uso de cualquiera de los Servicios, acuerdan voluntariamente por ese simple hecho y se obligan a sujetarse en todos sus términos y condiciones a estos Términos, así como a cualquier otra disposición, requerimiento, lineamiento o similar, que sea de tiempo en tiempo establecida en los Sitios, directamente o a través de los vínculos que muestren dicha información.</p>
        <p>Los Usuarios acceden libre y voluntariamente al Sitio, con el objeto de solicitar de Cinemex la prestación de los Servicios, sujetándose de igual forma libremente a los Términos vigentes, así como a cualquier disposición que sea emitida por Cinemex, directa o indirectamente, relacionada con los Servicios.</p>
        <p>La prestación de los Servicios está sujeta a la aceptación íntegra de los Términos aquí contenidos, por parte de cualquier persona.</p>

        <h3>II. DEFINICIONES</h3>
        <p>Ya sea establecidas como singular o plural, masculino o femenino, o en las conjugaciones de sus verbos, los siguientes conceptos tendrán el significado referido a continuación:</p>
        <p>Boletos: Significan los cupones impresos que otorgan a su portador el acceso a la película cinematográfica que se establece en dicho documento.</p>
        <p>Cargo por Servicio: Significa la cantidad señalada en el Sitio que será pagada por el Cine|usuario a Cinemex por la prestación de los Servicios, incluyendo el correspondiente Impuesto al Valor Agregado.</p>
        <p>Cinemex: Cadena Mexicana de Exhibición, S.A. de C.V., sus subsidiarias y/o afiliadas.</p>
        <p>Cine|usuario: Persona física registrada en cinemex.com, que ha cumplido los requisitos y provisto información requerida en dicha página por Cinemex para obtener una cuenta de correo electrónico con la terminación @cinemex.com, y/o para utilizar los Servicios, requisitando asimismo la solicitud de compra o formatos adicionales que establece el Sitio para cada caso.</p>
        <p>Cookie. Es un pequeño archivo que es almacenado en el disco duro de la computadora del Usuario; contiene caracteres particulares del Usuario de que se trate y el uso que éste da al sitio. El contenido puede o no tener información que identifica personalmente al Usuario.</p>
        <p>Complejo Cinemex: Establecimiento cinematográfico ostentado bajo la marca Cinemex®. De igual forma, se refiere al establecimiento cinematográfico ostentado bajo la marca Cinemex®, elegido por el Cine|usuario para recoger los Boletos u otros o productos adquiridos a través del Sitio.</p>
        <p>Información Personal: Aquella que individualmente identifica a un Usuario, como: nombre, dirección o correo electrónico, entre otras.</p>
        <p>Mal Uso: Se entenderá por mal uso:</p>
        <p>Hacer uso sin autorización del titular de los derechos de propiedad intelectual, de las marcas, imágenes, obras y productos que aparezcan en la página del Sitio.</p>
        <p>Alterar o modificar los sistemas de seguridad el Sitio.</p>
        <p>Manipular la información, Términos, condiciones o lineamientos que se mencionen en el Sitio.</p>
        <p>Incurrir en actos que se consideren como ingeniería en reversa y/o inversa.</p>
        <p>Utilizar sin autorización de su titular una cuenta de Cine|usuario para solicitar los Servicios.</p>
        <p>Utilizar sin autorización de su titular una tarjeta de crédito para solicitar los Servicios.</p>
        <p>Partes: Cinemex y el Usuario y/o Cine|usuario.</p>
        <p>Precio Unitario: Es la cantidad señalada en el Sitio para cada Boleto o producto, más el correspondiente Impuesto al Valor Agregado. El Precio Unitario se cobrará en Pesos (moneda del curso legal de la República Mexicana).</p>
        <p>Servicios: Los contenidos en los subincisos (i) a (v) del inciso 1, del Titulo I de estos Términos.</p>
        <p>Sitios: A los sitios en Internet y puntos para proveer servicios, referidos en los subincisos (i) a (v) del inciso 1, del Título I anterior. Dicha referencia será conjunta para todos ellos o de forma individual para cada uno de ellos. Estos Sitios son proveídos por Social Snack.</p>
        <p>Sitios Enlazados: A través de éstos, pone a disposición del Usuario, dispositivos técnicos de enlace, tales como, hipervínculos (links), anuncios (banners), botones, directorios y/o herramientas de búsqueda, entre otros, que les permiten acceder a páginas de Internet pertenecientes a terceros.</p>
        <p>Términos: El presente documento "Términos y Condiciones de Uso", así como cualesquier términos, condiciones, limites, responsabilidades, lineamientos, entre otros, presentes o futuros, temporales o permanentes, de uso y/o utilización de los Sitios y/o los Servicios.</p>
        <p>Usuario: Cualesquier persona que de cualquier forma acceda a los Sitios y/o utilice los Servicios.</p>

        <h3>III. ASPECTOS GENERALES</h3>
        <p>Los Servicios, son exclusivamente para uso personal y de ninguna forma podrán ser para uso comercial o de cualquier otro tipo. No se pueden utilizar los Servicios para vender o promocionar o comercializar un producto o servicio propio o de terceros, o para incrementar el tráfico a cualquier otro sitio o página de Internet para fines comerciales o cualquier distinto a personal y privado, salvo que exista un acuerdo expreso por anticipado con Cinemex.</p>
        <p>Cinemex se reserva el derecho de modificar, actualizar o dar por terminados, total o parcialmente, en cualquier tiempo, los Términos y/o los Servicios, por cualquier causa y sin responsabilidad alguna para Cinemex.</p>
        <p>Las Partes aceptan y reconocen que Cinemex se encuentra debidamente autorizado para hacer uso de la información que el Cine|usuario registre en el Sitio, siempre y cuando el uso de dicha información se limite a facilitar el envío al Cine|usuario de información sobre promociones, productos o servicios, que Cinemex comercialice, de tiempo en tiempo. Cinemex utilizará la información en concordancia con los Términos y Condiciones, así como con la Política de Privacidad publicada en el Sitio.</p>
        <p>Cinemex se reserva el derecho de diferir, suspender o denegar la prestación de los Servicios, en tanto el Cine|usuario no cumpla con las condiciones y requisitos exigidos por Cinemex en el Sitio.</p>
        <p>Cinemex se reserva el derecho de suspender, modificar o terminar en cualquier momento, total o parcialmente, los Servicios, sin responsabilidad alguna y sin mayor formalidad que las que las disposiciones vigentes así pudieran establecer.</p>
        <p>Cinemex se reserva el derecho de hacer cambios a la página de Internet cinemex.com, así como a realizar exclusiones o modificaciones a los Términos y Condiciones, en cualquier tiempo.</p>
        <p>Cinemex podrá suspender los Servicios en cualquier momento, por disposición legal, en caso de que Cine|usuario haga Mal Uso de los Sitios o en caso de detectar cualquier irregularidad con el Usuario o Cine|usuario. Todo lo anterior, no obstante las acciones, vías y/o derechos que Cinemex y/o las personas físicas o morales tengan que hacer valer en contra del responsable del Mal Uso o irregularidad referida.</p>
        <p>Cinemex se reserva el derecho a modificar en cualquier momento los Términos y/o los Servicios y a notificarle dicho acto al Usuario a través de la exposición de una versión actualizada los mismos en este sitio Web.</p>
        <p>Es responsabilidad del Usuario revisar periódicamente estos Términos, a fin de estar enterado de los Términos vigentes en cada momento.</p>
        <p>El uso del Servicio posterior a las modificaciones a los Términos y/o Servicios, implica el consentimiento del Usuario a dichos cambios.</p>
        <p>Los materiales e información presentados en el Sitio, son propiedad de Cinemex y/o de sus autores y/o de terceros que contratan con Cinemex la colocación de determinado material en el Sitio de Cinemex, y son utilizados para la promoción de sus productos y/o servicios o de los productos y/o servicios de dichos terceros.</p>
        <p>Cinemex es propietario de todos los derechos de propiedad intelectual, industrial y/o derecho de autor, relativos al diseño, funciones y/u operaciones que integran el Sitio, por lo que el uso del mismo no constituye una licencia al Cine|usuario para utilizar el nombre, diseños, signos distintivos, marcas y/o cualquier otro aplicable, propiedad de Cinemex.</p>
        <p>El Usuario acepta que ni los Términos, ni el Contrato ni el uso que realice del Servicio, crean entre Cinemex y dicho Usuario una asociación, sociedad, relación laboral, agencia, comisión o cualquier similar. Asimismo, el Usuario acepta que el presente documento no constituye una licencia, cesión o transmisión de ningún tipo de derecho.</p>
        <p>Si cualquier estipulación del presente documento se declara nula o no ejecutable de acuerdo con la legislación aplicable, incluida, pero sin limitarse a, la renuncia a garantías, limitaciones de responsabilidad y pago de daños y perjuicios establecidas previamente, dicha estipulación se considerará reemplazada por aquella otra estipulación válida y ejecutable que más se asemeje a la intención de la estipulación original, continuando vigentes las restantes estipulaciones de este documento.</p>
        <p>Los títulos sólo se incorporan para la comodidad de las Partes y no tienen significado legal o contractual alguno.</p>
        <p>Estos Términos son y serán regidos por las Leyes de la República Mexicana y cualquier disputa será sometida a la jurisdicción y competencia de las leyes y los tribunales de la Ciudad de México, Distrito Federal, renunciando expresamente el Usuario, a cualquier otra jurisdicción que pudiere corresponderle por motivo de su domicilio o nacionalidad, presente o futuro, o por cualquier otra causa.</p>
        <p>Estos Términos constituyen, junto con los demás acuerdos publicados en los Sitios, el total de acuerdos entre Cinemex y los Usuarios y sustituyen cualquier acuerdo previo, escrito o verbal.</p>

        <h3>IV. INFORMACIÓN ADICIONAL, CONTACTO</h3>
        <p>Si tiene comentarios o requiere información adicional, relacionada con Cinemex, Términos, Servicios, Sitios, entre otros, por favor no dude en ponerse en contacto con nosotros al teléfono (55) 52 01 58 00; o al teléfono vigente en dicho momento; o en Av. Javier Barros Sierra No.540, Torre 1, PH 1, Col. Santa Fe, CP. 01210, México D.F. o en el buzón de correo electrónico de Cinemex, buzon@cinemex.com.mx; en su caso, el personal que atienda la llamada o a cargo de dicha dirección de correo electrónico los pondrá en contacto con la persona adecuada para dar respuesta o atender la situación que se plantee.</p>

        <h3>V. POLÍTICA DE PRIVACIDAD</h3>
        <p>Cinemex reconoce la importancia de la privacidad de sus Usuarios, por lo que los Servicios están diseñados considerando la protección de la información que es proveída a Cinemex.</p>
        <p>La Política de Privacidad de Cinemex describe los tipos de Información Personal que recolectamos de nuestros Usuarios cuando utilizan nuestros Servicios, así como el destino y salvaguarda de la misma.</p>
        <p>Nuestra Política de Privacidad esta basada en lo siguiente:</p>
        <p>Recolección de Información.</p>
        <p>Aquellos Servicios que no requieren autentificar al Usuario, recolectan información general y no Información Personal, relativa al navegador que se utiliza, el tipo de conexión a Internet, el sistema operativo y otros elementos de configuración destinados a mejorar nuestros Servicios. Esta información puede ser guardada en una Cookie en la computadora del Usuario, para futura referencia al navegar en el Sitio.</p>
        <p>Algunos de nuestros Servicios requieren explícitamente que el Usuario abra una cuenta de Cine|usuario y/o proporcione Información Personal, en virtud de la funcionalidad del Sitio o por disposición legal aplicable. En este caso, el Sitio solicita algunos datos para la cuenta como nombre, dirección, edad, sexo y Complejo Cinemex favorito, entre otros, incluyendo Información Personal. Esta Información Personal puede solicitarse al Usuario al entrar a un Sitio o cuando solicita un Servicio.</p>
        <p>Para algunos Servicios, como la compra de boletos en línea, requerimos información de tarjeta de crédito u otras formas de pago, entre otros, la cual es contenida en forma cifrada en servidores y equipo seguros, en cumplimiento de las disposiciones legales aplicables.</p>
        <p>Al momento de registrarse en nuestro Sitio, el Usuario tiene la opción de solicitar no recibir correos electrónicos por parte de Cinemex ni de terceros.</p>
        <p>Cookies.</p>
        <p>Son utilizadas para mejorar la calidad del Servicio y para entender mejor la forma en que nuestros Usuarios interactúan con Cinemex. Esto se hace almacenando preferencias y otros datos del Usuario en las Cookies. La mayoría de los navegadores están configurados para aceptar Cookies. El Usuario puede deshabilitar esta aceptación automática en la configuración de su navegador, con la advertencia de que algunos de los Servicios ofrecidos pueden no funcionar adecuadamente sin dichas Cookies.</p>
        <p>El Usuario reconoce y acepta que Cinemex puede colocar Cookies en su computadora, así como permitir que terceros que se anuncian en los Sitios, de igual forma lo hagan.</p>
        <p>Información compartida.</p>
        <p>Cinemex no vende ni renta la Información Personal a otras personas físicas o morales. En todo caso, Cinemex podrá compartir la información personal identificable de los Usuarios, en cualquiera de los siguientes casos:</p>
        <p>Contamos con el consentimiento del Usuario.</p>
        <p>Proveemos dicha información, Información Personal o no, con personas morales o físicas de confianza, para el sólo hecho de procesar la información por encargo de Cinemex. Cuando esto sucede, se está bajo acuerdos que obligan a tales personas morales o individuos a procesar dicha información solamente bajo instrucciones de Cinemex y en concordancia con esta Política de Privacidad, Términos y medidas apropiadas de confidencialidad y seguridad.</p>
        <p>Si Cinemex es requerido por Ley o, estamos en el entendido en buena fe, que el acceso, preservación o divulgación de dicha información, incluso Información Personal, es razonablemente necesario para proteger los derechos de Cinemex, sus Usuarios o el público en general.</p>
        <p>Si el Usuario es Cine|usuario, podemos compartir la información entre Cinemex para proveer una experiencia más amigable y mejorar la calidad de nuestros Servicios y Productos.</p>
        <p>Cuando se trate de compartir información estadística de los Sitios y nuestros Usuarios. Esta información no identifica al Usuario, pero sí a las tendencias y comportamientos del mismo.</p>
        <p>Seguridad de la información.</p>
        <p>Cinemex toma las medidas conducentes para proteger la Información Personal y sus bases de datos, contra acceso o alteración no autorizados, divulgación o destrucción de información.</p>
        <p>Cinemex restringe el acceso a la Información Personal, solamente a aquellos empleados que requieren conocerla con objeto de operar, desarrollar o mejorar nuestros Servicios.</p>
        <p>Actualización de la Información</p>
        <p>Cinemex provee a los Usuarios y Cine|usuarios, mecanismos de actualización o corrección de la Información Personal previamente proveída, la cual es utilizada en nuestros Servicios.</p>

        <h3>VI. CAMBIOS A LA POLÍTICA DE PRIVACIDAD</h3>
        <p>La Política de Privacidad, así como los Términos, pueden cambiar en cualquier momento por cualquier causa. Normalmente los cambios serán menores, sin embargo, en algunas ocasiones habrá cambios significativos. Conforme realicemos dichos cambios, significativos o no, los publicaremos en el Sitio de que se trate; si los cambios son significativos, podremos dar a nuestros Usuarios algún otro tipo de aviso o notificación, sin que ocurra en todos los casos.</p>

        <h3>VII. SITIOS ENLAZADOS Y LIMITACIÓN DE RESPONSABILIDAD</h3>
        <p>El uso de los Sitios es bajo riesgo y responsabilidad de cada Usuario, la responsabilidad de Cinemex se limita en todo momento a los presentes Términos vigentes en cada momento, así como al cumplimiento de las disposiciones aplicables en aquello no establecido en los Términos.</p>
        <p>Los Sitios Enlazados pueden permitirle al Usuario abandonar los Sitios propiedad de Cinemex. Dichos sitios no son controlados por Cinemex, no siendo éste responsable del contenido de los mismos ni de cualquier aspecto que con ellos se vincule directa o indirectamente.</p>
        <p>Los Sitios Enlazados en los Sitios, se limita a facilitar a los usuarios la búsqueda de y/o acceso en internet, a la información disponible de dichos Sitios Enlazados; no presupone, ni se establece explícitamente, la existencia de alguna clase de vínculo, comisión, agencia, distribución, comercialización, responsabilidad, obligación o asociación entre Cinemex y los operadores, sociedades, individuos y/o cualesquier tercero, de los Sitios Enlazados y/o los terceros propietarios de dichos Sitios Enlazados.</p>
        <p>Cinemex no controla, aprueba ni hace propios los servicios, información, datos, archivos, productos y/o cualquier clase de material existente en los Sitios Enlazados, incluyendo ofertas, información, datos, concursos y/o promociones. El Usuario, por lo tanto, debe extremar la prudencia en la valoración y utilización de cualquier clase de material existente en los Sitios Enlazados.</p>
        <p>Cinemex no garantiza ni asume responsabilidad alguna por los daños y/o perjuicios de toda clase que puedan causarse por:</p>
        <p>El funcionamiento, disponibilidad, accesibilidad o continuidad de los Sitios Enlazados.</p>
        <p>El mantenimiento de los servicios, información, datos, archivos, productos y/o cualquier clase de material existente en los Sitios Enlazados.</p>
        <p>Las obligaciones, ofertas, concursos y/o promociones existentes en los Sitios Enlazados.</p>
        <p>Utilidad, veracidad, parcialidad, objetividad, entre otros, de la información contenida en los Sitios Enlazados.</p>
        <p>Virus no generados o trasladados por cualquiera de los Sitios.</p>
        <p>Mediante la utilización de los Servicios, el Usuario reconoce y acepta que Cinemex no garantiza de forma alguna, los Sitios Enlazados o la información proporcionada por terceros; de igual forma, no garantiza que los Sitios Enlazados funcionen correctamente, sean útiles para la realización de actividad alguna en particular o para cualquier otro fin, estén libres de información dañina, entre otros.</p>
        <p>Mediante el uso de los Servicios, el Usuario reconoce y acepta que Cinemex queda excluida de cualquier responsabilidad por los daños y perjuicios que pudieran haber sido causados por la veracidad de la información o calidad de servicios contenidos u ofrecidos por terceros o que se encuentre en los Sitios Enlazados, o los que surjan con relación a este Sitio.</p>
        <p>Tanto el acceso a los Sitios o a los Sitios Enlazados, como el uso que pueda hacerse de la información contenida en los mismos, son exclusiva responsabilidad del Usuario y no de Cinemex.</p>
        <p>Cinemex no es responsable de la información recibida de cualquier Sitio Enlazado. Cinemex facilita al Usuario estos vínculos como parte de los Servicios, en tal virtud, la inclusión de cualquier Sitio Enlazado no implica la aprobación de Cinemex respecto de la información que ahí se contenga, asimismo, no implica relación alguna entre Cinemex y el operador o propietario del mismo.</p>

        <h3>VIII. DERECHO DE PROPIEDAD DEL CONTENIDO</h3>
        <p>Todo el contenido de los anunciantes de Cinemex u otros proveedores de contenido, está protegido por el derecho de autor o por derechos de propiedad industrial o cualquier otra legislación.</p>
        <p>El Usuario podrá copiar el contenido del Correo Electrónico o de los Servicios exclusivamente para uso personal, no comercial, siempre y cuando mantenga intactos todos los avisos de derechos de autor y demás leyendas de propiedad industrial. El Usuario no podrá modificar, copiar, reproducir, volver a publicar, cargar, exponer, transmitir o distribuir de cualquier forma el contenido disponible a través del Servicio o de cualquier correo electrónico y sitios de Internet asociados, incluidos el código y el sistema (software).</p>

        <h3>IX. LIMITACIÓN DE RESPONSABILIDAD DE LOS SERVICIOS</h3>
        <p>La información contenida en los Servicios puede incluir errores tipográficos.</p>
        <p>De forma periódica se incorporan cambios a la información contenida.</p>
        <p>Cinemex o sus proveedores pueden introducir en cualquier momento mejoras o cambios en el Servicio.</p>
        <p>Cinemex no garantiza que el Servicio sea libre de interrupciones ni libre de errores, que los defectos sean corregidos o que el Servicio o el servidor están libres de virus u otros componentes nocivos.</p>
        <p>Cinemex no garantiza que por un error o defecto propio o ajeno de Cinemex en el Servicio se pierda la información almacenada por el Usuario.</p>
        <p>Cinemex no garantiza que el uso o los resultados del uso del Servicio o de los materiales puestos a disposición como parte del Servicio serán correctos, precisos, puntuales o de otro modo fidedignos.</p>
        <p>El Usuario acepta expresamente que Cinemex no será responsable del acceso no autorizado o alteración de cualquier material o datos enviados, recibidos, no enviados, así como de transacciones realizadas a través del Correo Electrónico. Asimismo, acepta que Cinemex no es responsable de conducta o contenido amenazador, difamatorio, obsceno, ofensivo o ilegal de algún tercero ni de infracción alguna de derechos de terceros, incluidos los derechos de propiedad intelectual y en general, el Usuario acepta que Cinemex no es responsable de contenido alguno enviado, utilizando o incluido en el Correo Electrónico, ya sea por el Usuario o cualquier tercero.</p>
        <p>Ni Cinemex ni sus proveedores garantizan la idoneidad, fiabilidad, disponibilidad, oportunidad y exactitud de los Servicios o del Correo Electrónico para cualquier propósito. Los Servicios se proporcionan "en el estado en que se encuentra" sin garantía de ningún tipo. Cinemex y sus proveedores renuncian por la presente a cualesquiera garantías o condiciones respecto de los Servicios o del Correo Electrónico, incluida cualquier garantía implícita y condiciones de comerciabilidad o idoneidad para un fin determinado, titularidad y no infracción.</p>
        <p>En ningún caso Cinemex o sus proveedores serán responsables de daños directos, indirectos, punitivos, incidentales, especiales, consecuenciales ni de algún otro tipo, incluidos, de manera enunciativa más no limitativa, los daños por pérdida de datos o de beneficios, que se deriven o tengan relación con el uso del Correo Electrónico o de los Servicios o Sitios o Sitios Enlazados, con la demora o imposibilidad para poder utilizar el Correo Electrónico o los Servicios o Sitios, con la imposibilidad de que el Usuario preste sus servicios o de cualesquiera información, sistemas (software), productos, servicios y gráficos relacionados obtenidos a través de los Servicios o a los cuales se haya tenido acceso.</p>
        <p>La jurisdicción aplicable en todo caso, así como la competencia, será la de México, Distrito Federal, en términos del presente documento. Si el Usuario no esta de acuerdo con todo o parte de los Términos, el Usuario deberá de inmediato suspender el uso de los Servicios.</p>

        <h3>X. SEGURIDAD</h3>
        <p>Mediante el uso de los Sitios, el Usuario reconoce y acepta que Cinemex no garantiza la seguridad de los Sitios Enlazados y, en particular, que los Usuarios puedan efectivamente acceder a las distintas páginas de Internet que representan los Sitios Enlazados, ni que a través de éstos puedan transmitir, difundir, almacenar o poner a disposición de terceros su contenido.</p>
        <p>Mediante el uso de los Sitios, el Usuario reconoce y acepta que Cinemex queda excluida de cualquier responsabilidad por los daños y perjuicios de toda naturaleza que pudieran ser causados por la falta de seguridad de los Sitios Enlazados.</p>

        <h3>XI. CALIDAD</h3>
        <p>Mediante el uso de los Sitios Enlazados, el Usuario reconoce y acepta que Cinemex no controla y no garantiza la calidad, ausencia de virus o similares, en el contenido de los Sitios Enlazados, ni la ausencia de otros elementos que pudieran producir alteraciones en sus sistemas, equipo o en los documentos electrónicos y/o archivos almacenados en sus sistemas y/o equipo informáticos.</p>

        <h3>XII. PROPIEDAD INTELECTUAL</h3>
        <p>Mediante el uso de los Sitios y los Servicios, el Usuario reconoce y acepta que Cinemex queda excluida de cualquier responsabilidad que pudiera ser causada por el uso no autorizado de las marcas u otros derechos de propiedad intelectual, industrial y/o derechos de autor de terceros o contenidos en los Sitios Enlazados.</p>
        <p>Las eventuales referencias que se hagan en los Sitios a cualquier producto, servicio, proceso, Sitio Enlazado, hipertexto, marca, nombre comercial o cualquier otra información en la que se utilicen marcas, signos distintivos y/o dominios, el nombre comercial o el nombre del fabricante, suministrador, etc., que sean titularidad de terceros, en ningún momento constituirán, ni implicarán respaldo o recomendación alguna por parte de Cinemex, y en ningún caso Cinemex se asigna propiedad ni responsabilidad sobre los mismos, salvo estipulación expresa en contrario.</p>

        <h3>XIII. MARCAS PROPIEDAD INTELECTUAL</h3>
        <p>Todo el contenido del Servicio es: Copyright © 1996-2005 Cadena Mexicana de Exhibición, S.A. de C.V. o sus proveedores. Reservados todos los derechos.</p>
        <p>MARCAS. Cinemex y otros productos Cinemex a los que se hizo, hace, hará referencia anteriormente, en este documento y/o en los Sitios, son marcas o marcas registradas de Cadena Mexicana de Exhibición, S.A. de C.V. Los nombres de compañías, servicios y productos aquí mencionados pueden ser marcas de sus respectivos propietarios.</p>
        <p>Los ejemplos de compañías, organizaciones, productos, personas y hechos aquí representados son ficticios. No se pretende o no debería inferirse ninguna asociación con ninguna compañía, organización, producto, persona o hecho real.</p>
        <p>Cualesquiera derechos que no hayan sido expresamente otorgados aquí están reservados.</p>
        <p>©1996-2005 Grupo Cinemex, S.A. de C.V. Reservados todos los derechos.</p>



        <h3>XIV. ELEMENTOS DE LOS SITIOS</h3>
        <p>Para proporcionar la información contenida en los Sitios, tal como dibujos, diseños, sonido, videos, textos, fotografías, etc., Cinemex hubiere contratado a terceros, personas físicas o morales, para realizar los estudios e investigaciones correspondientes, así como los dibujos, diseños, sonidos, videos, textos y/o fotografías, entre otros, que se muestren en los Sitios.</p>
        <p>Cinemex establece que, en aquellos materiales en los que no sea su titular, ni haya participado directamente en el desarrollo de toda la información contenida en los Sitios, algunos de los textos, gráficos, vínculos y/o el contenido de algunos artículos incluidos en la misma, podrían no ser veraces o no estar actualizados, por lo que Cinemex no tiene responsabilidad alguna.</p>
        <p>De igual manera, podrán existir en los Sitios, discrepancias entre las carteleras cinematográficas impresas en medios de comunicación y la cartelera electrónica que se muestra en el Sitio, ya que puede ser que la primera no siempre se encuentre actualizada.</p>

        <h3>XV. CORREO ELECTRÓNICO</h3>
        <p>Las Partes acuerdan y se obligan a que los requisitos para ser Usuario del Servicio de Correo Electrónico son los siguientes:</p>
        <p>Ser una persona mayor de 18 años, en pleno uso y goce de sus facultades o,</p>
        <p>Ser menor de edad o incapaz legalmente, siempre y cuando se cuente con la autorización de sus padres o tutores. En caso que dichos Usuarios no obtengan previamente la autorización de sus padres o tutores, aceptan que actuarán bajo su propia responsabilidad y sin autorización y/o consentimiento de Cinemex, liberando a Cinemex de cualquier responsabilidad y debiendo ser responsable el Usuario y/o su Tutor en términos de las disposiciones legales aplicables.</p>
        <p>Ser Cine|usuario.</p>
        <p>A fin de que el Usuario acepte los Términos, deberá presionar el botón izquierdo del ratón de su computadora, sobre la palabra "acepto" y completar el proceso de registro necesario en los términos que se indican y la forma establecida. Dicha aceptación lleva implícita la declaración del Usuario mediante la cual manifiesta que reúne los requisitos necesarios para obtener una cuenta de Correo Electrónico, quedando obligado en los términos y condiciones aquí descritos.</p>
        <p>El procedimiento de apertura de cuenta de Correo Electrónico es el siguiente:</p>
        <p>Registro de Datos. El procedimiento de inscripción se completa mediante el ingreso de los datos del Usuario en el formulario de registro. Es obligación y responsabilidad exclusiva del Usuario que la información que ingrese a dicho formulario sea verídica, precisa y actualizada.</p>
        <p>Cuando el Usuario se registra en el Sitio, se solicitan, entre otros, los siguientes datos: nombre, fecha de nacimiento, sexo, código postal.</p>
        <p>Una vez llenado el formulario de registro, el Usuario deberá ingresar la contraseña de su elección y el nombre de su cuenta de Correo Electrónico. Atendiendo a que los nombres de cuenta de Correo Electrónico no pueden ser repetidos o iguales, en caso que se reporte que dicho nombre elegido por el Usuario sea igual a algún Usuario anterior, le será requerido que ingrese un nombre distinto. Cualquier usuario que abuse de la libertad de designación en el nombre de su cuenta de Correo Electrónico, tal como utilizar palabras que atenten en contra de la moral y buenas costumbres, podrá ser restringido a discreción de Cinemex, sin previa notificación, del uso de los Servicios.</p>
        <p>El Usuario será el único responsable de mantener la confidencialidad de su contraseña y el nombre de su cuenta de Correo Electrónico.</p>
        <p>El Usuario será el único responsable de todas y cada una de las operaciones que se realicen mediante la utilización de su cuenta de Correo Electrónico.</p>
        <p>El Usuario acepta y se obliga a notificar inmediatamente a Cinemex acerca de cualquier anormalidad, uso no autorizado o violación a la seguridad de su cuenta de Correo Electrónico</p>

        <h3>XVI. PARTICIPACIÓN EN PROMOCIONES DE LOS ANUNCIANTES</h3>
        <p>Cualquier transacción con los anunciantes de los Servicios o del Correo Electrónico, participación en promociones, incluidos la entrega y el pago de bienes y servicios y cualesquiera otros términos o condiciones, garantías o declaraciones relativas a estas transacciones o promociones, se realizan única y exclusivamente entre el Usuario y el anunciante o cualquier otro tercero.</p>
        <p>Cinemex no será de forma alguna, responsable de dichas transacciones o promociones.</p>

        <h3>XVII. PROHIBICIONES EN EL USO DEL SERVICIO DE CORREOELECTRÓNICO Y DE LOS SERVICIOS EN GENERAL</h3>
        <p>El Usuario deberá en todo momento hacer uso de su cuenta de Correo Electrónico exclusivamente para fines lícitos y de conformidad con lo dispuesto en los presentes Términos, así como leyes y/o disposiciones aplicables. Queda prohibido cualquier uso comercial no autorizado del Correo Electrónico, o bien, la reventa de los Servicios.</p>
        <p>El Servicio se prestará exclusivamente a personas físicas para uso personal, por lo que el Usuario, al aceptar estas condiciones, se compromete a utilizar el Servicio sin otro fin que el personal.</p>
        <p>El Usuario se obliga a cumplir con la legislación aplicable, ya sea nacional ó internacional y será único responsable de todos los actos u omisiones que sucedan en relación con su Correo Electrónico, la contraseña, nombre y, en general, con el contenido de las transmisiones a través del Correo Electrónico, así como por el uso de los Servicios.</p>
        <p>El Usuario, de manera enunciativa más no limitativa, se obliga a no llevar a cabo las siguientes actividades:</p>
        <p>Usar el Servicio en relación con encuestas, concursos, esquemas piramidales, cartas en cadena, mensajes no deseados, correo no solicitado (spam) o cualesquiera otros mensajes duplicativos o no solicitados (comerciales o de otro tipo).</p>
        <p>Difamar, insultar, acosar, acechar, amenazar o infringir de cualquier otra forma los derechos de terceros (tales como el derecho a la intimidad o a la propia imagen).</p>
        <p>Publicar, distribuir o divulgar cualquier información o material inapropiado, sacrílego, difamatorio, ilícito, obsceno, indecente o ilegal.</p>
        <p>Anunciar u ofrecer la venta o compra de cualesquiera productos o servicios.</p>
        <p>Recopilar o de cualquier otro modo, recabar información sobre terceros, incluidas sus direcciones de correo electrónico, sin su consentimiento.</p>
        <p>Crear una identidad falsa con el propósito de engañar a Cinemex o a terceros respecto a la identidad del remitente o del origen del mensaje.</p>
        <p>Transmitir o cargar archivos que contengan virus, caballos de Troya, gusanos, bombas de tiempo, robots de cancelación de noticias o cualesquiera otros programas perjudiciales o nocivos para el Correo Electrónico o los Servicios.</p>
        <p>Transmitir o cargar materiales que contengan sistemas (software) u otras obras protegidas por la legislación aplicable, salvo que el Usuario sea titular de los derechos o tenga la autorización necesaria para ello utilizar dichas obras.</p>
        <p>Interferir o interrumpir redes conectadas con el Servicio o infringir las normas, directivas o procedimientos de dichas redes.</p>
        <p>Intentar obtener acceso de forma no autorizada al Servicio, a otras cuentas, a sistemas informáticos o a redes conectadas con el Servicio, a través de búsqueda automática de contraseñas o por otros medios.</p>
        <p>Infringir cualquier ley, reglamento o norma de cualquier país, en relación con la transmisión de datos técnicos o software exportado.</p>
        <p>Interferir con el uso o disfrute del Servicio por parte de otros Usuarios o con el uso o disfrute de servicios similares por parte de otras personas o entidades.</p>
        <p>Cinemex no tiene la obligación de supervisar el Servicio o el uso que un Usuario haga del mismo, ni conservar los contenidos de las sesiones del Usuario. Sin embargo, Cinemex se reserva el derecho a supervisar, revisar, conservar o revelar en cualquier momento cualquier información que pueda ser necesaria para cumplir con cualquier ley o reglamento, procedimiento legal o requerimiento administrativo aplicable.</p>

        <h3>XVIII. SUSPENSIÓN DE LOS SERVICIOS Y DEL CORREO ELECTRÓNICO</h3>
        <p>Cinemex puede dar por terminado en cualquier momento, con o sin causa, el acceso del Usuario a todo o parte del Servicio o del Correo Electrónico.</p>
        <p>Cinemex puede interrumpir o suspender la cuenta de Correo Electrónico de un Usuario por inactividad, que se define como falta de acceso a dicha cuenta por un período prolongado de tiempo, según dicho período sea determinado por Cinemex. El período de tiempo que actualmente Cinemex considera como período de tiempo "prologado" puede ser consultado directamente con Cinemex a la información proporcionada en este documento. Con la interrupción del Servicio, el derecho a utilizar el Servicio o el Correo Electrónico cesa inmediatamente y la cuenta puede ser reasignada a otro Usuario. La información que haya estado almacenada en la cuenta del Usuario será eliminada al momento de la interrupción por falta de uso, sin responsabilidad alguna para Cinemex.</p>
        <p>Si el Usuario desea cancelar su Correo Electrónico o el uso de los Servicios, el Usuario solo deberá interrumpir el uso del mismo.</p>

        <h3>XIX. CORREO NO SOLICITADO E INDEMNIZACIÓN POR DAÑOS</h3>
        <p>Cinemex cancelará o eliminará inmediatamente cualquier Correo Electrónico cuando considere, a su discreción, que dicha cuenta transmite o está de cualquier otra forma relacionada con mensajes de correo molesto (spam) o masivo (bulk) no solicitado. Además, dado que los daños podrán no ser cuantificables, el Usuario se compromete a pagar a Cinemex la cantidad de $5.00 (Cinco Dólares 00/100) Dólares de los Estados Unidos de Norteamérica o su equivalente en pesos mexicanos por cada mensaje molesto o de correo electrónico masivo no solicitado transmitido desde su Correo Electrónico o bien relacionado con la misma. En el supuesto de que los daños puedan ser razonablemente calculados, el Usuario se compromete a pagar a Cinemex la reparación de los mismos.</p>

        <h3>XX. PRIVACIDAD DE LOS USUARIOS DEL SERVICIO DE CORREO ELECTRÓNICO</h3>
        <p>Cinemex respetará, en todo momento, la privacidad de los Usuarios de los Servicios, por lo que no supervisará, modificará ni divulgará la información que el Usuario utilice sin su previo consentimiento.</p>
        <p>En caso de ser necesario, Cinemex solicitará autorización del Usuario para supervisar, modificar o divulgar la información referida.</p>
        <p>No obstante lo anterior, el Usuario acepta que Cinemex acceda a, supervise, modifique o divulgue la información contenida en su cuenta de Correo Electrónico, sin necesidad de consentimiento previo del Usuario cuando Cinemex actué de buena fe o el los siguientes casos:</p>
        <p>Para cumplir con requerimientos o procedimientos legales.</p>
        <p>Para proteger y defender los derechos de autor o cualquier otro derecho.</p>
        <p>Para hacer cumplir las condiciones de los Servicios.</p>
        <p>Para proteger los intereses y derechos de otros Usuarios o de terceros.</p>
        <p>Con el fin de responder a necesidades técnicas.</p>
        <p>Salvo que el Usuario solicite expresamente a Cinemex que no divulgue su información a terceros con fines de mercadotecnia y/o publicitarios, el Usuario faculta a Cinemex para que éste proporcione a terceros la información obtenida a través del registro del Usuario.</p>
        <p>El Usuario acepta que su dirección de protocolo Internet sea transmitida al destinatario de cada mensaje enviado desde su Correo Electrónico.</p>
        <p>El Usuario acepta que toda o parte de la Información Personal que proporcione a Cinemex podrá ser almacenada por éste fuera del país o estado de residencia del Usuario.	</p>

        <h3>XXI. LIBERACIÓN DE RESPONSABILIDAD</h3>
        <p>El Usuario acepta indemnizar y sacar en paz y a salvo a Cinemex, sus matrices, subsidiarias, compañías afiliadas, sus directivos y empleados, respecto de cualquier reclamación, demanda o daño, incluidos los honorarios razonables de abogados, interpuesto por cualquier tercero debido al uso de los Servicios.</p>
        <p>El Usuario deberá cubrir los montos que por daños y/o perjuicios se generen a cualquier tercero por cualquier Mal Uso o uso irregular o contrario a las disposiciones legales, de los Servicios.</p>

        <h3>XXII. ALMACENAMIENTO DE MENSAJES, MENSAJES EXTERNOS Y OTRAS LIMITACIONES</h3>
        <p>El Usuario acepta que la cantidad de espacio de almacenamiento de Correo Electrónico por Usuario es limitada, por lo que la recepción o envío de algunos mensajes de correo podrá no ser procesada debido a restricciones de espacio o limitaciones de envío de mensajes externos.</p>
        <p>El Usuario deberá contactar a Cinemex a través de la información que ha quedado establecida para conocer las limitaciones de espacio de las cuentas de Correo Electrónico.</p>

        <h3>XXIII. RESERVACIÓN Y COMPRAVENTA ELECTRÓNICA</h3>
        <p>Una vez que el Usuario hace uso de los Sitios, en caso que decida realizar la compraventa electrónica de Boletos y/o productos que así estén disponibles en los Sitios, Cinemex acepta vender y el Cine|usuario acepta comprar , la cantidad de Boletos y/o productos seleccionados por el Cine|usuario en el Sitio, acorde con el cumplimiento de requisitos que se establezcan en cada caso. A partir de ese momento, los presentes Términos se establecen en un contrato válido y vigente entre Cinemex y el Usuario y/o Cine|usuario.</p>
        <p>El Cine|usuario pagará a Cinemex mediante cargo a su tarjeta de crédito, el Precio Unitario por cada uno de los Boletos y/o productos adquiridos, según lo previsto en los incisos cuatro, cinco y seis siguientes de este Título XIII, referido a la contraprestación.</p>
        <p>El Cine|usuario se obliga a requisar la información solicitada en el Sitio, a efecto de poder realizar cualquier tipo de compra o solicitud de información a través del Sitio.</p>
        <p>Una vez que el Cine|usuario: (i) se haya registrado como Cine|usuario del Sitio, (ii) haya cumplido con las condiciones y requisitos exigidos; y (iii) después de haber seleccionado el Número de Boletos y/o productos deseados, Cinemex le presentará en pantalla al Usuario un mensaje con confirmación y/o algún similar, que documentará la aceptación de Cinemex a la solicitud de los Servicios del Cine|usuario. Dicho mensaje con confirmación, contendrá una clave única y establecerá los términos y condiciones bajo los cuales se hará la entrega de los Boletos o productos adquiridos.</p>
        <p>Una vez requisitada y aceptada la solicitud electrónica de compra de los Servicios por Cinemex a través del mensaje con confirmación, el Cine|usuario se compromete a cumplir con su obligación de pagar a Cinemex el Precio Unitario de cada uno de los Boletos y/o bienes adquiridos, independientemente de que haga uso o no de los mismos.</p>
        <p>El pago a que hace referencia en el subinciso inmediato anterior, se efectuará con cargo directo a la tarjeta de crédito del Cine|usuario, o a aquel método de pago que Cinemex pueda tener vigente en dicho momento, razón por la cual, como parte de su solicitud de compra electrónica, deberá proporcionar el número de tarjeta de crédito correspondiente, así como los datos que sobre la misma le sean requeridos o aquellos otros aplicables.</p>
        <p>El Cine|usuario libera de responsabilidad y se compromete a sacar en paz y a salvo a Cinemex por cualquier demanda, reclamo, daño o perjuicio sufrido, derivado del uso de la tarjeta de crédito que el Cine|usuario utilice para solicitar los Servicios.</p>
        <p>El Cine|usuario deberá presentarse en la taquilla del Complejo de su elección a recoger los Boletos y/o productos que hubiera adquirido o reservado a través del Sitio, sujeto, entre otros, a los términos y condiciones señaladas al efecto en su mensaje de correo electrónico de confirmación. Al momento de presentarse, el Usuario tendrá la opción de presentar el mensaje con confirmación que recibió en pantalla y que puede imprimir o solamente mencionar su nombre de Cine|usuario, esto sin perjuicio de lo establecido como requisito en el siguiente inciso nueve y los demás de este título.</p>
        <p>Cinemex no se encuentra obligado a entregar Boleto o producto alguno, en tanto no exista certeza de que la contraprestación por el pago de los Servicios haya tenido verificativo. Cinemex requerirá, entre otros, que el Cine|usuario presente la tarjeta de pago e identificación oficial, para recoger los productos o Boletos cubiertos o reservados en términos de este Titulo.</p>
        <p>Cinemex hará su mejor esfuerzo para proporcionar los Servicios a través del Sitio, así como dar buen uso a la información que el Cine|usuario proporcione al momento de registrarse en el Sitio. Cinemex no se hace responsable por fallas en el sistema o Internet, que limiten o impidan la prestación de los Servicios.</p>
        <p>El Cine|usuario se obliga a respetar y cumplir lo establecido en los Términos.</p>
      </div>
        
    </article>
  </div>
</div>
<?php $view['slots']->stop(); ?>