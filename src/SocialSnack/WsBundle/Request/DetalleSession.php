<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class DetalleSession extends ConsultaAbstract {

	const     TIPOCONSULTA_DISPONIBILIDAD     = 1;
	const     TIPOCONSULTA_UBICACIONES        = 2;
	
	protected $method       = 'DetalleSession';
	
	protected $default_args = array(
			'Tipo'              => 0,
			'Session'           => 0,
			'CodigoCine'        => 0,
	);
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws \Exception
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
   * @param \SimpleXMLElement $xml
   * @return mixed
	 */
	public function parse_res( $xml ) {
		$node = '';
		switch ( $this->args[ 'Tipo' ] ) {
			case self::TIPOCONSULTA_DISPONIBILIDAD:
				$node = 'DISPONIBLIDAD';
				if ( isset( $xml->{$node} )
						&& isset( $xml->{$node}->MENSAJE )
						&& preg_match( '/^Asientos disponibles por este medio\: ([0-9]+)$/', $xml->{$node}->MENSAJE, $match ) 
				) {
					return $match[1];
        }
				break;
				
			case self::TIPOCONSULTA_UBICACIONES:
				$node = 'CADENAASIENTOS';
				if ( isset( $xml->{$node} )
						&& isset( $xml->{$node}->LOCALIDAD )
						&& $res = WsHelper::parse_seats_str( $xml->{$node}->LOCALIDAD )
				) {
					return $res;
        }
				break;
			
			default:
				throw new \Exception( 'Invalid parameter Tipo.' );
				break;
		}
		
		if ( $xml->MENSAJE ) {
			throw new RequestMsgException( $xml->MENSAJE );
    } else {
			throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
    }
	}
}