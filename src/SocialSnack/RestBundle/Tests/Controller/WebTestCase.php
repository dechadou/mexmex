<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;
use SocialSnack\FrontBundle\Entity\User;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class WebTestCase
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class WebTestCase extends LiipWebTestCase {

  static $container;

  public function setUp() {
    // Start the Symfony kernel.
    $kernel = static::createKernel();
    $kernel->boot();

    // Get the DI container.
    self::$container = $kernel->getContainer();
  }


  protected function loadFixtures(array $classNames, $omName = null, $registryName = 'doctrine', $purgeMode = null) {
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
    $result = parent::loadFixtures($classNames, $omName, $registryName, $purgeMode);
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));
    return $result;
  }


  protected function get_logged_in_client(User $user) {
    $client = static::createClient();
    $session = $client->getContainer()->get('session');

    $firewall = 'main';
    $token = new UsernamePasswordToken( $user, $user->getPassword(), $firewall, $user->getRoles());
    $session->set('_security_'.$firewall, serialize($token));
    $session->save();

    $cookie = new Cookie($session->getName(), $session->getId());
    $client->getCookieJar()->set($cookie);

    return $client;
  }


  protected function assertJsonResponse(Response $res, $model = NULL, $statusCode = 200) {
    $this->assertEquals($statusCode, $res->getStatusCode());
    $this->assertEquals('application/json', $res->headers->get('Content-Type'));
    $data = json_decode($res->getContent());
    $this->assertNotNull($data); // Make sure the JSON could be properly decoded.
    if ($model) {
      $this->assertModel($model, $data);
    }

    return $data;
  }


  protected function getValue($element, $key) {
    if (is_array($element)) return $element[$key];
    if (is_object($element)) return $element->{$key};
    throw new \Exception('Element is neither an object or an array.');
  }


  protected function assertModel($expected, $actual) {
    foreach ($expected as $k => $v) {
      if (is_array($v)) {
        if (0 === $k) {
          $actualValue = $actual;
        } else {
          $actualValue = $this->getValue($actual, $k);
        }

        if (isset($v['type'])) {
          $this->assertInternalType($v['type'], $actualValue);

          if ('array' === $v['type'] && isset($v['each'])) {
            foreach ($actualValue as $e) {
              $this->assertModel($v['each'], $e);
            }
          }

          if ('array' === $v['type'] && isset($v['in'])) {
            $this->assertTrue(in_array($actualValue, $v['in']));
          }
        }

        if (isset($v['equals'])) {
          $this->assertEquals($actualValue, $v['equals']);
        }

        if (isset($v['model'])) {
          $this->assertModel($v['model'], $actualValue);
        }

        continue;
      }
      $this->assertObjectHasAttribute($k, $actual);

      if ('null' === $v) {
        $this->assertNull($this->getValue($actual, $k));
      } elseif ('\\' === substr($v, 0, 1)) {
        $this->assertInstanceOf($v, $this->getValue($actual, $k));
      } else {
        $this->assertInternalType($v, $this->getValue($actual, $k));
      }
    }
  }


} 