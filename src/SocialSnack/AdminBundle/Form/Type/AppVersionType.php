<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AppVersionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('device', 'choice', array(
                'choices' => [
                    'android' => 'Android',
                    'ios' => 'iOS',
                ],
                'label' => 'Dispositivo',
                'attr' => ['class' => 'form-control'],
            ))
            ->add('version', 'text', array(
                'label' => 'Versión',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SocialSnack\RestBundle\Entity\AppVersion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'socialsnack_restbundle_appversion';
    }
}
