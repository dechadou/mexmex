<?php
namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

class NotificationRepository extends EntityRepository {

  public function getList($order=null,$limit=null){
    $qb = $this->createQueryBuilder('a');
    $qb->addSelect('a');

    if ($order != null){
      $qb->orderBy('a.'.$order, 'DESC');
    } else {
      $qb->orderBy('a.date', 'DESC');
    }

    if ($limit != null) {
      $qb->setMaxResults($limit);
    } else {
      $qb->setMaxResults(30);
    }
    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 10 * 60);
    return $query->getResult();
  }

}
