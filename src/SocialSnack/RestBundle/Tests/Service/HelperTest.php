<?php

namespace SocialSnack\RestBundle\Tests\Service;

use SocialSnack\RestBundle\Service\Helper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HelperTest extends WebTestCase {

	
	protected $container;
	
	protected $mobile_config;
	
	protected $signed_request = '';
	
	/**
	 * {@inheritDoc}
	 */
	public function setUp() {
		static::$kernel = static::createKernel();
		static::$kernel->boot();
		
		$this->container      = static::$kernel->getContainer();
		$this->mobile_config  = $this->container->getParameter( 'mobile_app' );
		$this->signed_request = "AnmpqqyJ5k-QNl4FT31Qn-xWB280Rp29TvDt68O2gRg.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUE4U05acnJYUHpKUmlkM1RJZjI1RlFDM1NCRUZlM2NnXzhvVjIxQjJVbUdPZTlqSFoyYWl0dVpYQ2YyMjZyYXFCbHoxZUxGaXhwSXZKd1d5OEdTR0hCRUJMNUhLcjRaS0E0ZG5OZFZ5eTA2YmdfZER5VjJlMGxxX0taLWwtZU5ERDVTU1VfZWN5MUVkWTVUTWlKdWdrVU9WT25ET2hJTVlRY0Ywa3IyRmNuY2w1a1Z6NTc4aEprWVl1aW1pejlhQkhtTjFLTjR4LVFpb3p5cTNzZkZ0dEZyNGRkTHJndWJnVnVSV0hIRnFIak4wS0p4WTdCdlVJTUhVcnZkS0pEczl3N0JrVXhJd0VHVWZDN1BuOUNVZUhKWnhhZHFoazduRlk1NVNqNzNJemdfS2Y0Wk05dFdQZ18wN3lxdnhyaG9qeDd0RjJlV2hmUExval90VlZUZEhtZSIsImlzc3VlZF9hdCI6MTM4MTI2Njg4OCwidXNlcl9pZCI6IjEwMDAwNjc5MTUyOTkzMCJ9";
	}
	
	public function testParse_signed_request() {
		$signed_request = $this->signed_request;
		$secret_key     = $this->mobile_config['fb']['secret_key'];
		
		$data = Helper::parse_signed_request( $signed_request, $secret_key );
		$this->assertNotNull( $data );
		
		$signed_request = uniqid() . '.' . uniqid();
		$data = Helper::parse_signed_request( $signed_request, $secret_key );
		$this->assertNull( $data );
	}
	
	public function testParse_signed_requestX() {
		$signed_request = $this->signed_request;
		$secret_key     = $this->mobile_config['fb']['secret_key'];
		
		$data = Helper::parse_signed_requestX( $signed_request, $secret_key );
		$this->assertNotNull( $data );
		
		$signed_request = uniqid();
		$this->setExpectedException('Exception');
		$data = Helper::parse_signed_requestX( $signed_request, $secret_key );
	}
	
//	public function testValidate_signed_request() {
//		$signed_request   = $this->signed_request;
//		$secret_key       = $this->mobile_config['fb']['secret_key'];
//		$app_id           = $this->mobile_config['fb']['app_id'];
//		$app_access_token = $this->mobile_config['fb']['app_access_token'];
//		$redirect_url     = '';
//
//		$data = Helper::validate_signed_request( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
//		$this->assertNotNull( $data );
//		$this->assertNotEmpty( $data['user_id'] );
//
//		$signed_request = uniqid();
//		$data = Helper::validate_signed_request( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
//		$this->assertNull( $data );
//	}
//
//	public function testValidate_signed_requestX() {
//		return;
//
//		$signed_request   = $this->signed_request;
//		$secret_key       = $this->mobile_config['fb']['secret_key'];
//		$app_id           = $this->mobile_config['fb']['app_id'];
//		$app_access_token = $this->mobile_config['fb']['app_access_token'];
//		$redirect_url     = '';
//
//		$data = Helper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
//		$this->assertNotNull( $data );
//		$this->assertNotEmpty( $data['user_id'] );
//
//		$signed_request = uniqid();
//		$this->setExpectedException('Exception');
//		$data = Helper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
//	}

  public function testNum_hash() {
    $len = 10;
    $hash = Helper::num_hash(1, uniqid(), $len);
    $this->assertEquals($len, strlen($hash));
    $this->assertRegexp('/^[0-9]+$/', $hash);
  }



  // ^ Old tests... maybe useless?


  public function testGetSessionMemcachedKey() {
    $session = $this->getMock('\SocialSnack\WsBundle\Entity\Session');
    $session->method('getId')->willReturn(1);
    $session->method('getLegacyId')->willReturn(1001);
    $key = Helper::get_session_memcached_key($session);

    $this->assertRegExp('/^[0-9A-Za-z]+$/', $key);
  }


  public function testBuildBookingCode() {
    $output = Helper::buildBookingCode(1, 23456);
    $this->assertEquals('B1   23456', $output);
    $output = Helper::buildBookingCode(12, 3456);
    $this->assertEquals('B12  3456', $output);
    $output = Helper::buildBookingCode(123, 456);
    $this->assertEquals('B123 456', $output);
  }

}
