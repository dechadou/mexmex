<?php
if (!defined('FTP_ASCII')) { define('FTP_ASCII', 1); }
if (!defined('FTP_BINARY')) { define('FTP_BINARY', 2); }
 
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
						new SocialSnack\WsBundle\SocialSnackWsBundle(),
            new SocialSnack\FrontBundle\SocialSnackFrontBundle(),
            new SocialSnack\RestBundle\SocialSnackRestBundle(),
						new Endroid\Bundle\QrCodeBundle\EndroidQrCodeBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new SocialSnack\AdminBundle\SocialSnackAdminBundle(),
            new Eo\PassbookBundle\EoPassbookBundle(),
            new Gremo\BuzzBundle\GremoBuzzBundle(),
            new RMS\PushNotificationsBundle\RMSPushNotificationsBundle(),
            new Rollerworks\Bundle\PasswordStrengthBundle\RollerworksPasswordStrengthBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }
        if (in_array($this->getEnvironment(), array('test'))) {
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new Liip\FunctionalTestBundle\LiipFunctionalTestBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
