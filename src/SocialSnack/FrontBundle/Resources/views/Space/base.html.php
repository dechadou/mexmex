<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Espacio Alternativo - Cinemex');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/sections/espacio-alternativo-header.jpg'); ?>">
</div>

<div class="section-content">
<?php $view['slots']->output('section_body'); ?>
</div>
<?php $view['slots']->stop(); ?>
