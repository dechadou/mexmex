<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

/**
 * Class SessionHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class SessionHandler {

  protected $doctrine;


  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  protected function getRepository($persistentObjectName, $persistentManagerName = NULL) {
    return $this->doctrine->getRepository($persistentObjectName, $persistentManagerName);
  }


  /**
   * Get a session.
   *
   * @param int $session_id
   * @return array|null
   */
  public function getSerialized($session_id) {
    $session_repo = $this->doctrine->getRepository('SocialSnackWsBundle:Session');

    // Use findWithMovie because movie is used later to filter tickets by movie rating.
    /** @var \SocialSnack\WsBundle\Entity\Session $session */
    $session = $session_repo->findWithMovie($session_id);

    if (!$session) {
      return NULL;
    }

    $session_repo->_populate_tickets($session);
    $session->setTickets($this->applyTicketsFilters($session->getTickets(), $session));

    $result = $session->toArray();

    return $result;
  }


  public function applyTicketsFilters(array $tickets, Session $session) {
    $tickets = $this->filterTicketsForMovie($tickets, $session->getMovie());
    $tickets = $this->filterTicketsForSession($tickets, $session);

    return $tickets;
  }


  /**
   * @param array   $tickets
   * @param Session $session
   * @return array
   */
  protected function filterTicketsForSession(array $tickets, Session $session) {
    $dateStart = new \DateTime('2015-12-18');
    $dateEnd   = new \DateTime('2016-01-31');

    if ($session->getDate() == new \DateTime('2016-01-28')) {
      foreach ($tickets as $i => $ticket) {
        $name = is_array($ticket) ? $ticket['name'] : $ticket->getDescription();
        if (!preg_match('/^CMX/i', $name)) {
          unset($tickets[$i]);
        }
      }
    } else if ($session->getDate() >= $dateStart and $session->getDate() <= $dateEnd) {
      foreach ($tickets as $i => $ticket) {
        $name = is_array($ticket) ? $ticket['name'] : $ticket->getDescription();
        if (!preg_match('/(20 ANIV|MIÉR|MIER|miér|FESTIVAL|MNF|BALLET|Evento especial|EVENTO EN VIVO|OBELA 2X1)/i', $name)) {
          unset($tickets[$i]);
        }
      }
    } else {
      foreach ($tickets as $i => $ticket) {
        $name = is_array($ticket) ? $ticket['name'] : $ticket->getDescription();
        if (preg_match('/(20 ANIV)/i', $name)) {
          unset($tickets[$i]);
        }
      }
    }

    $tickets = array_values($tickets);

    return $tickets;
  }


  /**
   * Apply specific movie-based tickets filters.
   *
   * @param array $tickets
   * @param Movie $movie
   * @return array
   */
  protected function filterTicketsForMovie(array $tickets, Movie $movie) {
    if (in_array($movie->getGroupId(), WsHelper::getNoOnlineSellingGroupIds())) {
      return array();
    }

    $tickets = $this->filterTicketsForMovieRating($tickets, $movie->getData('RATING'));

    return $tickets;
  }


  /**
   * Filter tickets based on movie rating.
   *
   * @param array  $tickets
   * @param string $rating
   * @return array
   */
  protected function filterTicketsForMovieRating(array $tickets, $rating) {
    // Remove tickets for minors from C rated movies.
    if (in_array(strtolower($rating), ['c', 'd'])) {
      foreach ($tickets as $i => $ticket) {
        $name = is_array($ticket) ? $ticket['name'] : $ticket->getDescription();
        if (preg_match('/\bmenor\b/i', $name)) {
          unset($tickets[$i]);
        }
      }
    }

    return $tickets;
  }

}
