var chan = Channel.build({
    window: document.getElementById("sessionbridge").contentWindow,
    origin: "*",
    scope: "testScope"
});

$.localStorage = {
  setItem : function( k, v, cb ) {
    chan.call({
        method: "setItem",
        params: {k:k, v:v},
        success: cb
    });
  },
  getItem : function( k, cb ) {
    chan.call({
        method: "getItem",
        params: k,
        success: cb
    });
  },
  removeItem : function( k, cb ) {
    chan.call({
        method: "removeItem",
        params: k,
        success: cb
    });
  }
}