<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovieInfo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MovieInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

		/**
		 * @ORM\OneToOne(targetEntity="Movie", mappedBy="movieinfo")
		 * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
		 */
		private $movie;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return MovieInfo
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set movie
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $movie
     * @return MovieInfo
     */
    public function setMovie(\SocialSnack\WsBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;
    
        return $this;
    }

    /**
     * Get movie
     *
     * @return \SocialSnack\WsBundle\Entity\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }
}