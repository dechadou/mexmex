<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures\Command\SetUpAdminCommand;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\AdminBundle\Entity\Admin;
use SocialSnack\AdminBundle\Entity\AdminRole;

class AlreadyInitializedData implements FixtureInterface {

  public function load(ObjectManager $om) {
    $faker = Faker\Factory::create();

    $roles = [
        'ROLE_ADMIN' => 'Admin'
    ];

    $admin = new Admin();
    $admin
        ->setPassword($faker->password())
        ->setSalt($faker->sha1())
        ->setUser($faker->userName())
    ;
    $om->persist($admin);

    foreach ($roles as $id => $name) {
      $role = new AdminRole();
      $role
          ->setName($name)
          ->setRole($id)
      ;
      $om->persist($role);

      $admin->addRole($role);
    }

    $om->flush();
  }

}