<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;
use SocialSnack\WsBundle\Entity\Cinema;

class MemcachedStatsCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'tasks:memcached:stats' )
			->setDescription( 'Get Memcached stats' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
    $servers = $this->getContainer()->getParameter('session_memcached_servers');
    $mem     = $this->getContainer()->get('memcached');
    $graph   = $this->getContainer()->get('graphite');
    $stats   = $mem->getStats();
    $used    = 0;
    $total   = 0;
    
    foreach( $servers as $server ) {
      $h = fsockopen($server['host'], $server['port']);
      fwrite($h,"stats slabs\r\n");
      $buffer = '';
      while(!feof($h)) {
        $buffer .= @fgets($h,128);
        if (preg_match('/END/', $buffer)) {
          break;
        }
      }
      preg_match('/total_malloced (?<mem>\d+)/', $buffer, $matches);
      $_used  = (int)$matches['mem'];
      $_total = $stats["{$server['host']}:{$server['port']}"]['limit_maxbytes'];
      $used  .= $_used;
      $total .= $_total;
      $host   = str_replace('.','_',$server['host']);
      $value  = round($_used/$_total*100);
      $output->writeln(sprintf('host %s: %d', $host, $value));
      $graph->add('memcached.mem.' . $host, $value);
    }
    $value  = round($used/$total*100);
    $graph->add('memcached.mem.total', $value);
    $output->writeln(sprintf('total: %d', $value));
    $graph->publish();
  }
  
}