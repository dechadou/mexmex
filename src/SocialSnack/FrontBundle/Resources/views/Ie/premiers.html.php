<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Premieres - Cinemex');
$view['slots']->start('iebody');

$movies = array(
    'basic'   => array(
        /*
        array(
            'title'   => 'Dallas Buyers Club',
            'date'    => ['Jueves 06 de Febrero', '20:00 hrs'],
            'notes'   => NULL,
            'trailer' => '//www.youtube.com/watch?v=SfrHR3FdZYk',
            'cover'   => '/movie_posters/coming/elclubdelosdesahuciados-200x295.jpg',
            'cinema'  => array(
                'name'      => 'Mundo E',
                'permalink' => '//cinemex.com/cine/52/cinemex-mundo-e'
            )
        )
        //*/
    ),
    'gold'    => array(),
    'amex'    => array(),
    'premium' => array()
);
?>
<h1 class="content-box-title">Premieres</h1>

<p>Selecciona tu tarjeta de Invitado Especial Cinemex y entérate las premieres a las que puedes asistir.</p>

<div class="entry-body">
    <ul class="ie-levels-tabs clearfix">
        <li>
            <div class="tab" data-target="#ie-levels-basic">
                <p>Básico</p>
                <div class="hr"></div>
                <div class="content">
                    <img src="<?php echo $view['assets']->getUrl('assets/img/card-basica.gif'); ?>" alt="" />
                </div>
            </div>
        </li>
        <li>
            <div class="tab" data-target="#ie-levels-gold">
                <p>Oro</p>
                <div class="hr"></div>
                <div class="content">
                    <img src="<?php echo $view['assets']->getUrl('assets/img/card-oro.gif'); ?>" alt="" />
                </div>
            </div>
        </li>
        <li>
            <div class="tab" data-target="#ie-levels-amex">
                <p>Amex</p>
                <div class="hr"></div>
                <div class="content">
                    <img src="<?php echo $view['assets']->getUrl('assets/img/card-amex.gif'); ?>" alt="" />
                </div>
            </div>
        </li>
        <li>
            <div class="tab" data-target="#ie-levels-premium">
                <p>Premium</p>
                <div class="hr"></div>
                <div class="content">
                    <img src="<?php echo $view['assets']->getUrl('assets/img/card-premium.gif'); ?>" alt="" />
                </div>
            </div>
        </li>
    </ul>

<?php foreach ($movies as $level => $items) : ?>
    <div id="ie-levels-<?php echo $level; ?>" class="ie-levels-block">
<?php if (empty($items)) : ?>
        <p class="msg">No hay premieres disponibles actualmente.</p>
<?php else : ?>
<?php
foreach ($items as $movie) :
    //$date = new DateTime($movie['date']);
?>
        <div class="movie-details-main">
            <h1 class="movie-details-title content-box-title" itemprop="name"><?php echo $movie['title']; ?></h1>

            <dl class="movie-details-info">
                <dt>Complejo</dt>
                    <dd><?php echo $movie['cinema']['name']; ?></dd>

                <dt>Día</dt>
                    <dd><?php echo $movie['date'][0]; ?></dd>

                <dt>Hora</dt>
                    <dd><?php echo $movie['date'][1]; ?></dd>
            </dl>

            <div class="movie-detail-notes">
            <?php echo $movie['notes']; ?>
            </div>

            <div class="movie-details-btns">
                <a href="<?php echo $movie['trailer']; ?>" class="btn btn-grey btn-icon icon icon-play popup-youtube" rel="tab-select" data-tab="movie-trailer-tab">Ver Tráiler</a>
            </div>

            <img src="<?php echo $view['assets']->getUrl($movie['cover']); ?>" alt="Actividad Paranormal Los Marcados" width="182" height="272" class="movie-details-cover" itemprop="image">
        </div>
<?php endforeach; ?>
<?php endif; ?>
    </div><!-- .ie-levels-[level] -->
<?php endforeach; ?>

    <div class="disclaimer">
        <p>Acude 5 días antes al complejo donde se realizará la función presentando tu tarjeta de Invitado Especial Cinemex para obtener tu pase doble. El cupo está sujeto a disponibilidad de la sala.</p>
    </div>
</div><!-- .entry-body -->
<?php $view['slots']->stop(); ?>
