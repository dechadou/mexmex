<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\FrontBundle\Entity\IeBenefit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\FrontBundle\Entity\Contact;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Endroid\QrCode\QrCode;


class IeController extends BaseController {

  /**
   * @Route("/programa", name="ie_program")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function programAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/terminos", name="ie_tos")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function tosAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/niveles", name="ie_levels")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function levelsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/beneficios", name="ie_benefits")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function benefitsAction() {
    $benefits = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:IeBenefit')->findActive();
    $groups = array(
        IeBenefit::CATEGORY_BASIC   => array(),
        IeBenefit::CATEGORY_GOLD    => array(),
        IeBenefit::CATEGORY_PREMIUM => array(),
    );

    foreach ($benefits as $benefit) {
      $groups[$benefit->getCategory()][] = $benefit;
    }

    return array(
        'route'    => $this->getRequest()->attributes->get('_route'),
        'benefits' => $groups,
    );
  }


  /**
   * @Route("/preguntas-frecuentes", name="ie_faq")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function faqAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/promociones", name="ie_promos")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function promosAction() {
    $promos = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:PromoIE')->findBy(array('active' => 1));
    $groups = array(
        'basic' => array(),
        'gold' => array(),
        'amex' => array(),
        'premium' => array(),
    );

    foreach ($promos as $promo) {
      foreach ($promo->getCategories() as $category) {
        $groups[$category][] = $promo;
      }
    }

    return array(
        'route'  => $this->getRequest()->attributes->get('_route'),
        'promos' => $groups,
    );
  }


  /**
   * @Route("/premieres", name="ie_premiers")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function premiersAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/puntos", name="ie_points")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function pointsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/payback", name="ie_payback")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function paybackAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }

  /**
   * @Route("/promo-jaguar", name="ie_promo_jaguar")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  protected function promoJaguarAction() {
    if ($this->getRequest()->getMethod() === 'POST' AND isset($_POST['code']) AND is_numeric($_POST['code'])) {
      $request = $this->get('cinemex_ws');
      $request->init('ConsultaActualizaciones');

      $response = $request->request([
        'Codigo' => $_POST['code'],
        'Tipo'   => 2
      ]);

      $items = [];

      foreach ($response->CODIGO as $item) {
        $data    = (array) $item->attributes();
        $items[] = $data['@attributes'];
      }

      $message = (string) $response->RESPUESTA->ERRORDESC;

      if (empty($message)) {
        $message = NULL;
      }

      $response = new JsonResponse();
      $response->setData([
        'count'   => count($items),
        'records' => $items,
        'message' => $message
      ]);

      return $response;
    }

    $response = $this->render('SocialSnackFrontBundle:Ie:promoJaguar.html.php', [
      'route' => $this->getRequest()->attributes->get('_route')
    ]);

    return $response;
  }

}
