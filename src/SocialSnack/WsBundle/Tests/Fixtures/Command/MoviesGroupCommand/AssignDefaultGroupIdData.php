<?php

namespace SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand;

use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;

class AssignDefaultGroupIdData extends AbstractLoadData {

  /**
   * Create two movies without group IDs and one with.
   *
   * Expected tests results: the two movies without group IDs should get a unique new one assigned.
   *
   * @param ObjectManager   $om
   * @param Faker\Generator $faker
   */
  protected function setUpMovies(ObjectManager $om, Faker\Generator $faker) {
    $movies = [];

    for ($i = 0 ; $i < 3 ; $i++) {
      $date = $faker->dateTimeBetween('+1 days', '+1 years');

      $movie = new Movie();
      $movie
          ->setLegacyId($faker->randomNumber())
          ->setActive(TRUE)
          ->setFeatured(FALSE)
          ->setFixedPosition(0)
          ->setInfo('{}')
          ->setLegacyIdBis($faker->word())
          ->setLegacyName($faker->title())
          ->setName($faker->title())
          ->setParent(NULL)
          ->setPosition(0)
          ->setPosterUrl($faker->imageUrl())
          ->setShortName($faker->title())
          ->setPremiere(FALSE)
          ->setType('single');
      $om->persist($movie);
      $movies[] = $movie;

      $session = new Session();
      $session
          ->setLegacyId($faker->randomNumber())
          ->setPlatinum(FALSE)
          ->setInfo('{}')
          ->setActive(TRUE)
          ->setCinema($this->cinema)
          ->setCinemom(FALSE)
          ->setDate($date)
          ->setDateTime($date)
          ->setExtreme(FALSE)
          ->setGroupId(0)
          ->setLegacyCinemaId($this->cinema->getLegacyId())
          ->setLegacyId($faker->randomNumber())
          ->setMovie($movie)
          ->setPremium(FALSE)
          ->setTime($date)
          ->setTz('America/Mexico_City')
          ->setUpdating(FALSE);
      $om->persist($session);
    }

    $movies[0]->setGroupId(0);
    $movies[1]->setGroupId(0);
    $movies[2]->setGroupId('foobar');
  }

}