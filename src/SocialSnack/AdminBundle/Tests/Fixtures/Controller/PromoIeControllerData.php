<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures\Controller;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\PromoIE;

class PromoIeControllerData implements FixtureInterface {

  static $promos;

  public function load(ObjectManager $manager) {
    self::$promos = [];

    $faker = \Faker\Factory::create();

    $promo = new PromoIE();
    $promo
        ->setActive(TRUE)
        ->setCategories(['basic', 'gold', 'amex', 'premium'])
        ->setImage($faker->imageUrl())
        ->setThumb($faker->imageUrl())
        ->setTitle($faker->title())
    ;
    self::$promos[] = $promo;
    $manager->persist($promo);

    $manager->flush();
  }

}