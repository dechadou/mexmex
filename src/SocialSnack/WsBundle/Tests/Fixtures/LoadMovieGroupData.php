<?php

namespace SocialSnack\WsBundle\Tests\Fixtures;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\Movie;

/**
 * Class LoadMovieGroupData
 * @package SocialSnack\WsBundle\Tests\Fixtures
 * @author Guido Kritz
 */
class LoadMovieGroupData implements FixtureInterface {

  static $grouped;
  static $ungrouped;
  static $lonely; // One movie grouped alone
  static $groupers;

  public function load(ObjectManager $manager) {
    self::$grouped = [];
    self::$ungrouped = [];
    self::$lonely = [];
    self::$groupers = [];
    
    $_movie = new Movie();
    $_movie
        ->setInfo(json_encode(array()))
        ->setName('Movie A')
        ->setShortName('M. A')
        ->setPremiere(FALSE)
        ->setActive(TRUE)
        ->setPosterUrl('')
    ;

    // Ungrouped
    for ($i = 1 ; $i <= 3 ; $i++) {
      $legacy_id = 111 + $i;
      $movie = clone $_movie;
      $movie
          ->setLegacyId($legacy_id)
          ->setGroupId('ng' . $legacy_id)
          ->setType('single')
      ;
      $manager->persist($movie);
      self::$ungrouped[] = $movie;
    }

    // Grouper
    $grouper = clone $_movie;
    $grouper
        ->setGroupId('hg222')
        ->setType('grouper')
    ;
    $manager->persist($grouper);
    self::$groupers[] = $grouper;

    // Grouped
    for ($i = 1 ; $i <= 3 ; $i++) {
      $legacy_id = 221 + $i;
      $movie = clone $_movie;
      $movie
          ->setLegacyId($legacy_id)
          ->setGroupId('hg' . $legacy_id)
          ->setType('single')
          ->setParent($grouper)
      ;
      $manager->persist($movie);
      self::$grouped[] = $movie;
    }

    // Group with just one movie
    $grouper = clone $_movie;
    $grouper
        ->setGroupId('hg333')
        ->setType('grouper')
    ;
    $manager->persist($grouper);
    self::$groupers[] = $grouper;
    $movie = clone $_movie;
    $movie
        ->setLegacyId(333)
        ->setGroupId('hg333')
        ->setType('single')
        ->setParent($grouper)
    ;
    $manager->persist($movie);
    self::$lonely[] = $movie;
    
    $manager->flush();
  }

} 