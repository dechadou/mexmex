<?php

namespace SocialSnack\WsBundle\Request;

use Buzz\Browser;
use Buzz\Exception\RequestException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

abstract class ConsultaAbstract {

  const PATH = '/wsCinemexWeb/wsCinemexWeb.asmx';

  protected $args = array();

  protected $attempts;

  protected $available_hosts;

  protected $browser;

  protected $client;

  protected $current_host;

  protected $default_args = array();

  protected $hosts;

  protected $key;

  protected $memcached;

  protected $method;

  protected $observers = [];

  protected $retries = 3;

  protected $signature;

  protected $start_time = [];


  /**
	 * Constructor method.
	 *
	 * @param array  $hosts  Array of WS host.
	 * @param string $client WS client ID.
	 * @param string $key    WS client's key.
   * @param Browser $browser
	 */
	public function __construct(array $hosts, $client, $key, Browser $browser) {
    $this->hosts   = $hosts;
    $this->client  = $client;
    $this->key     = $key;
    $this->browser = $browser;
    $this->args    = $this->default_args;
	}


  public function getMethod() {
    return $this->method;
  }


  public function pickHost() {
    if (empty($this->hosts)) {
      throw new \Exception('No WS hosts available.');
    }

    if (empty($this->current_host)) {
      $this->available_hosts = $this->hosts;
    }

    if (empty($this->available_hosts)) {
      $this->available_hosts = $this->hosts;
    }

    $i = array_rand($this->available_hosts);
    $this->current_host = $this->available_hosts[$i];
    return $this->current_host;
  }


  protected function rotateHost() {
    if (($key = array_search($this->current_host, $this->available_hosts)) !== false) {
      unset($this->available_hosts[$key]);
    }

    return $this->pickHost();
  }


  public function getLastUsedHost() {
    return $this->current_host;
  }


  public function getUrl() {
    return 'https://' . $this->current_host . static::PATH . '/' . $this->method;
  }
	
	
	/**
	 * Return the curret date.
	 * 
	 * @return int
	 */
	protected function get_date() {
		date_default_timezone_set( 'US/Central' );
		return (int)date( 'd' );
	}
	
	
	/**
	 * Build the signature for the WS requests.
	 * The signature is built using the client's key and the curret date (Central
	 * time).
	 * 
	 * @return string
	 */
	protected function build_signature() {
		$day = $this->get_date();
		return $day . $this->key . ( ( (int)$this->key * (int)$day ) - $day );
	}
	
	
	/**
	 * Add arguments to the request.
	 * 
	 * @param array $args Arguments in $key => $value format.
	 */
	public function set_args( $args ) {
		$this->args = $args + $this->args;
	}


  protected function requiresAuth() {
    return TRUE;
  }


  public function setRetries($retries) {
    $this->retries = $retries;
  }


  public function request($exec = TRUE) {
    $this->attempts = 0;
    while ($this->attempts < $this->retries) {
      $this->attempts++;
      try {
        return $this->doRequest($exec);
      } catch (\Exception $e) {
        if ($e instanceof RequestException) {
          $this->rotateHost();
        } else {
          throw $e;
        }
      }
    }

    throw $e;
  }


	/**
	 * Perform the WS request.
	 * 
   * @param boolean $exec
	 * @return \SimpleXMLElement Request response.
	 * @throws \Exception
	 */
	protected function doRequest($exec = TRUE) {
    if ($this->requiresAuth()) {
      $this->args['Cliente']  = $this->client;
      $this->args['ClaveCif'] = $this->build_signature();
    }

    $url     = $this->getUrl();
    $args_qs = http_build_query( $this->args );

    if (!$exec) {
      $ch = curl_init( $url );
      curl_setopt( $ch, CURLOPT_POST,           TRUE );
      curl_setopt( $ch, CURLOPT_POSTFIELDS,     $args_qs );
      curl_setopt( $ch, CURLOPT_HEADER,         FALSE );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
      curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
      curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
      curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 15 );
      curl_setopt( $ch, CURLOPT_TIMEOUT,        30 );
      return $ch;
    }

    $this->startTimer();

    try {
      /** @var \Buzz\Message\Response $post */
      $post       = $this->browser->post($url, array(), $args_qs);
      $res        = $post->getContent();
      $parsed_res = $this->parse_response($res);

      $this->notify($this, 'request', $post);

    } catch (\Exception $e) {
      $this->notify($this, 'request', isset($post) ? $post : NULL, $e);
      throw $e;
    }

    return $parsed_res;
	}


  protected function notify() {
    foreach ($this->observers as $observer) {
      call_user_func_array([$observer, 'update'], func_get_args());
    }
  }
  
  
  public function parse_response($res) {
    $res = $this->__parse_res(trim($res));
		return $this->_parse_res($res);
  }
  
  /**
   * Parse the response to obtain a valid XML.
   * 
   * @param string $res
   * @return \SimpleXMLElement
   * @throws \Exception
   */
  protected function __parse_res( $res ) {
    $xml = simplexml_load_string( trim( $res ) );
    $xml = html_entity_decode( $xml );
    $xml = $this->additional_format( $xml );
    $xml = simplexml_load_string( $xml, null, LIBXML_NOCDATA );
		
    return $xml;
  }

	
	/**
	 * Parse the response and return only response node.
	 *
   * @throws UnexpectedWsResponseException
	 * @param \SimpleXMLElement $xml
   * @return mixed
	 */
	protected function _parse_res( $xml ) {
		if ( !$xml || !isset( $xml->RESPUESTA ) ) {
			throw new UnexpectedWsResponseException(json_encode( $xml ));
    }

		return $this->parse_res( $xml->RESPUESTA );
	}
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 * 
	 * @param \SimpleXMLElement $xml
   * @return mixed
	 */
	abstract protected function parse_res( $xml );
	
	
	/**
	 * Sadly the WS response doesn't follow the XML specifications and returns
	 * an invalid document. Some ugly modifications should be applied to the
	 * string before decoding into an XML object.
	 * 
	 * @param string $xml
	 * @return string
	 */
	protected function additional_format( $xml ) {
		return $xml;
	}


  public function getArgs() {
    return $this->args;
  }

	
	/**
	 * Retrieve argument value.
	 * 
	 * @param  string $key
	 * @return string
	 */
	public function get_arg( $key ) {
		return $this->args[ $key ];
	}


  /**
   * @param \Memcached $memcached
   * @return $this
   */
  public function setMemcached(\Memcached $memcached) {
    $this->memcached = $memcached;

    return $this;
  }


  /**
   * @param \Exception $e
   * @return array
   */
  public function getSuccessStatsKeys(\Exception $e = NULL) {
    $keys = array(
      'ws_fails_general',
      'ws_fails_node_' . $this->getLastUsedHost(),
    );

    if (isset($this->args['CodigoCine'])) {
      $keys[] = 'ws_fails_cinema_' . $this->args['CodigoCine'];
    }

    return $keys;
  }


  protected function startTimer($name = 'default') {
    $this->start_time[$name] = microtime(true);
  }


  public function getElapsedTime($name = 'default') {
    $time_end = microtime(true);
    return ($time_end - $this->start_time[$name]);
  }


  protected function clearTimer($name = 'default') {
    unset($this->start_time[$name]);
  }


  public function addObserver($observer) {
    $this->observers[] = $observer;
  }

}