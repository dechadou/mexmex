<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>
<?php $view['slots']->start('body'); ?>
<?php
echo $this->render( 'SocialSnackFrontBundle:Mobile:partials/cinemaHeader.html.php', array(
    'cinema'          => $cinema,
) );
?>

<div class="sidebar-filters">
  <select name="date" class="sidebar-filter" id="billboard-date">
    <?php
    echo $this->render( 'SocialSnackFrontBundle:Mobile:partials/dateSelector.html.php', array(
        'date'            => $date,
        'available_dates' => $available_dates,
    ) );
    ?>
  </select>
</div>

<?php if (isset($cinema_platinum) AND $cinema_platinum !== NULL) : ?>
<p class="sidebar-cinema-link mar1"><a class="btn btn-icon icon icon-rarr btn-icon-right btn100 align-center" href="<?php echo $view['router']->generate('mobile_billboard', array('cinema_id' => $cinema_platinum->getId())); ?>"><i class="icon icon-platinum"></i> Ver Horarios en Platino</a></p>
<?php endif; ?>

<?php if (isset($cinema_standard) AND $cinema_standard !== NULL) : ?>
<p class="sidebar-cinema-link mar1"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn100 align-center" href="<?php echo $view['router']->generate('mobile_billboard', array('cinema_id' => $cinema_standard->getId())); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Tradicional</a></p>
<?php endif; ?>

<div class="mycinema-list" rel="main-list">
  <?php if ( !sizeof( $sessions ) ) { ?>
  No se encontraron funciones en este cine.
  <?php } ?>
  <?php foreach ( $sessions as $_movie ) { ?>

  <?php $poster_url = $view['fronthelper']->get_poster_url($_movie['parent']->getPosterUrl(), '67x99'); ?>
  <?php $movie_url = $view['router']->generate('mobile_movie', array( 'movie_id' => $_movie['parent']->getId(), 'cinema_id' => $cinema->getId() )); ?>
  <div class="mycinema-li box2">
    <div class="pad1 clearfix">
      <a href="<?php echo $movie_url; ?>"><img src="<?php echo $poster_url; ?>" alt="<?php echo $_movie['parent']->getName(); ?>" width="67" height="99" class="movie-details-cover" /></a>
      <a href="<?php echo $movie_url; ?>" class="mycinema-item-title"><?php echo $_movie['parent']->getName(); ?></a>
      <p class="mycinema-item-specs">
        <?php if ($_movie['parent']->getPremiere()) echo '<span>Estreno</span>'; ?>
        <span><?php echo $_movie['parent']->getData('RATING'); ?></span>
      </p>
    </div>

    <?php echo $this->render('SocialSnackFrontBundle:Mobile:partials/movieSessions.html.php', array(
        'items'    => $_movie['sessions'],
        'platinum' => $cinema->getPlatinum(),
    )); ?>

  </div>
  <?php } ?>
</div>
<?php $view['slots']->stop(); ?>
