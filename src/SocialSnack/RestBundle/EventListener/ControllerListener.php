<?php
namespace SocialSnack\RestBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use SocialSnack\RestBundle\Controller\BaseController;


/**
 * Before and after filters for controllers.
 */
class ControllerListener {
  
  
  /**
   * Filter ran before the controller action is called.
   * 
   * @param \Symfony\Component\HttpKernel\Event\FilterControllerEvent $event
   */
  public function onKernelController(FilterControllerEvent $event) {
    $controller = $event->getController();
  
    // $controller passed can be either a class or a Closure. This is not usual
    // in Symfony2 but it may happen. If it is a class, it comes in array format.
    if (!is_array($controller)) {
      return;
    }
    
    $controller = $controller[0];
    
    if (!($controller instanceof BaseController)) {
      return;
    }
    if ( method_exists($controller, 'before') ) {
      $controller->before();
    }
  }
    
  
  /**
   * Filter ran after controller action has returned a response.
   * Add the Vary header to the response if needed.
   * 
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $vary = $event->getRequest()->attributes->get('vary');
    if ($vary) {
      $event->getResponse()->setVary($vary);
    }
    
    if ($event->getRequest()->attributes->get('captcha') && $event->getRequest() instanceof JsonResponse) {
      $data = $event->getResponse()->getContent();
      $json = json_decode($data);
      $json->captcha = TRUE;
      $event->getResponse()->setData($json);
    }
  }
  
}