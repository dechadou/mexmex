<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Task;

class SessionsEnqueueCommand extends ContainerAwareCommand {
	
  
	protected function configure() {
		$this
        ->setName( 'ws:sessions:enqueue' )
        ->setDescription( 'Enqueue to fetch sessions from the Web Service' )
        ->addOption( 'cinema_id', 'cid', InputOption::VALUE_OPTIONAL )
        ->addOption( 'area_id',   'aid', InputOption::VALUE_OPTIONAL )
        ->addOption( 'state_id',  'sid', InputOption::VALUE_OPTIONAL );
	}
	
  
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
    $updater      = $this->getContainer()->get( 'cinemex.session_updater' );
		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$em           = $doctrine->getManager();
		$cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
    $tasks        = array();
    
    if ( $cinema_id = $input->getOption( 'cinema_id' ) ) {
      $cinemas = $cinema_repo->findBy( array( 'id' => $cinema_id ) );
    } elseif ( $state_id = $input->getOption( 'state_id' ) ) {
      $cinemas = $cinema_repo->findBy( array( 'state' => $state_id, 'active' => 1 ) );
    } elseif ( $area_id = $input->getOption( 'area_id' ) ) {
      $cinemas = $cinema_repo->findBy( array( 'area' => $area_id, 'active' => 1 ) );
    } else {
      $cinemas = $cinema_repo->findAll();
    }
    
    
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
		/* @var $task   \SocialSnack\WsBundle\Entity\Task   */
		foreach ( $cinemas as $cinema ) {
			$output->writeln( sprintf( 'Cinema %s', $cinema->getName() ) );
      $task = new Task();
      $task->setService('cinemex.session_updater');
      $task->setData(serialize(array(
          'cinema_id' => $cinema->getId(),
          'propagate' => TRUE,
          'enqueue'   => FALSE,
          'source'    => 'cron',
      )));
      $em->persist($task);
		}

    $task = new Task();
    $task->setService('cinemex.groupandorder');
    $em->persist($task);

    $em->flush();
    
		$output->writeln( 'Didn\'t fail, yay!' );
		return;
		
	}
	
}