<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\Cinema;

class LoadCinemasData extends AbstractFixture implements OrderedFixtureInterface {

  static $cinemas;

  public function load(ObjectManager $manager) {
    self::$cinemas = [];

    $cinema1 = new Cinema();
    $cinema1
        ->setActive(TRUE)
        ->setArea($this->getReference('Area-1-1'))
        ->setAttributes('')
        ->setCover('cover.jpg')
        ->setInfo(json_encode(['DIRECCION' => 'Avenida Siempreviva 123', 'TELEFONOS' => '555-5555', 'COLONIA' => 'Springfield']))
        ->setLat(1.234)
        ->setLegacyId(1001)
        ->setLegacyName('CINE 1')
        ->setLng(2.345)
        ->setName('Cine 1')
        ->setPlatinum(FALSE)
        ->setState($this->getReference('State-1'))
    ;
    $this->addReference('Cinema-1', $cinema1);
    self::$cinemas[] = $cinema1;
    $manager->persist($cinema1);
    
    $cinema2 = new Cinema();
    $cinema2
        ->setActive(TRUE)
        ->setArea($this->getReference('Area-1-2'))
        ->setAttributes('')
        ->setCover('cover.jpg')
        ->setInfo(json_encode(['DIRECCION' => 'Avenida Siempreviva 123', 'TELEFONOS' => '555-5555', 'COLONIA' => 'Springfield']))
        ->setLat(1.234)
        ->setLegacyId(1001)
        ->setLegacyName('CINE 2 PLATINO')
        ->setLng(2.345)
        ->setName('Cine 2 Platino')
        ->setPlatinum(TRUE)
        ->setState($this->getReference('State-1'))
    ;
    self::$cinemas[] = $cinema2;
    $manager->persist($cinema2);
    
    $cinema3 = new Cinema();
    $cinema3
        ->setActive(TRUE)
        ->setArea($this->getReference('Area-2-1'))
        ->setAttributes('')
        ->setCover('cover.jpg')
        ->setInfo(json_encode(['DIRECCION' => 'Avenida Siempreviva 123', 'TELEFONOS' => '555-5555', 'COLONIA' => 'Springfield']))
        ->setLat(1.234)
        ->setLegacyId(1001)
        ->setLegacyName('CINE 1')
        ->setLng(2.345)
        ->setName('Cine 1')
        ->setPlatinum(FALSE)
        ->setState($this->getReference('State-2'))
    ;
    self::$cinemas[] = $cinema3;
    $manager->persist($cinema3);

    $manager->flush();
  }


  public function getOrder() {
    return 3;
  }

}