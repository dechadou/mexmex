<?php
$view->extend('SocialSnackFrontBundle:Mobile:base.html.php');
$view['slots']->set( 'title', 'Restablecer contraseña - Cinemex' );
$view['slots']->start('body')
?>
<div class="box1">
  <?php echo $view->render('SocialSnackFrontBundle:Partials:resetPassForm.html.php', ['code' => $code, 'success' => $success, 'flash_msgs' => $flash_msgs]); ?>
</div>
<?php $view['slots']->stop(); ?>