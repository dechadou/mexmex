<?php $view->extend('FOSUserBundle::layout.html.php'); ?>

<?php $view['slots']->set( 'fos_user_content', $this->render( 'FOSUserBundle:Registration:register_content.html.php', array( 'form' => $form ) ) );
