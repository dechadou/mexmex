<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Invitado Especial - Cinemex');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/header-ie.jpg'); ?>" alt="Invitado Especial" />
</div>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render('SocialSnackFrontBundle:Ie:menu.html.php', array('route' => $route)); ?>
  </div>

  <div id="promo-jaguar" class="wide-main-col col content-box">
    <div class="entry-body">
      <div id="promo-jaguar-step1">
        <p class="copy">Gracias a todos los que participaron en nuestra promoción <mark>“Gánate un Jaguar con Invitado Especial Cinemex PAYBACK”</mark></p>

        <br><br>

        <h1>Felicitamos a <mark>Víctor Manuel Barreto</mark> nuestro ganador<br>
          <small>con el folio 3,248,228</small>.</h1>

        <div class="car-bg"><img class="car" src="<?php echo $view['assets']->getUrl('assets/img/landings/promo-jaguar/car.png'); ?>" /></div>

        <p>Número de permiso SEGOB: 20140833PS02. Vigencia de la promoción: del 8 de diciembre de 2014 al 4 de marzo de 2015.</p>
      </div>


    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
