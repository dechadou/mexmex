<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;

/**
 * Class Art
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class Art extends ConceptHelperAbstract {


  protected function getConceptName() {
    return 'Art';
  }


  public function getCinemas() {
    $cinema_repo = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $ids         = $this->getCinemasLegacyIds();

    if (!sizeof($ids)) {
      return [];
    }

    $cinemas = $cinema_repo->findManyByLegacyIdBy($ids);

    return $cinemas;
  }


} 