<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
Cinemex | cinemex.com

¡Ya tienes tus boletos!

<?php if ( $view['fronthelper']->cinema_has_qr($transaction->getCinema()) ) : ?>
Esta es tu confirmación de compra. Pasa directo al acceso de tu sala mostrando el código QR en cualquier dispositivo móvil, tablet o impresión.
<?php else : ?>
Esta es tu confirmación de compra. Pasa por tus boletos a taquilla indicando el código de compra o con la tarjeta con la que hiciste tu compra.
<?php endif; ?>

Datos de la compra

Título: <?php echo $session->getMovie()->getName(); ?>

<?php $type = FrontHelper::get_movie_attr_str($session->getMovie()->getAttr(), ', '); ?>
<?php if ( $type ) { ?>
Versión: <?php echo $type; ?>
<?php } ?>

Día: <?php echo FrontHelper::get_session_nice_date( $session->getDate() ); ?>

Horario: <?php echo $session->getTime()->format( 'h:i A' ); ?>

Cine: <?php echo $session->getCinema()->getName(); ?>

Sala: <?php echo FrontHelper::confirmation_get_screenname($transaction); ?>

Tus boletos:
<?php echo FrontHelper::confirmation_get_ticketslist($transaction, ', '); ?>

<?php if ( $transaction->getSeats() ) { ?>
Asientos seleccionados:
<?php echo FrontHelper::confirmation_get_seatslist($transaction, ' / '); ?>
<?php } ?>

<?php if ( $iecode = $transaction->getData('NUMIE') ) { ?>
Invitado especial:
<?php echo $iecode; ?>
<?php } ?>

Total pagado:
<?php echo FrontHelper::format_price( $transaction->getAmount() ); ?>

Términos y condiciones | <?php echo $view['router']->generate('tos_page', array(), TRUE); ?>
