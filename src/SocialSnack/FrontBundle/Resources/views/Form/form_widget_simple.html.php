<?php $type = isset($type) ? $view->escape($type) : 'text'; ?>
<?php $validators = array(); ?>
<?php if ($required) $validators[] = 'not_empty'; ?>
<?php if ('email' === $type) $validators[] = 'email'; ?>
<input type="<?php echo $type; ?>" <?php echo $view['form']->block($form,'widget_attributes'); ?><?php if (isset($value)) { ?> value="<?php echo $value; ?>"<?php } ?><?php if ($validators) { ?> data-validators="<?php echo implode(' ', $validators); ?>"<?php } ?>/>
