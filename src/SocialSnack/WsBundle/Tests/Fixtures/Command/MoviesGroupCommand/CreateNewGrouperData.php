<?php

namespace SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand;

use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;

class CreateNewGrouperData extends AbstractLoadData {

  /**
   * Create two movies with same group ID.
   * Create another movie with another group ID.
   *
   * Expected test result: a new grouper movie is created and assigned as parent of the two movies with matching group ID.
   *
   * @param ObjectManager   $om
   * @param Faker\Generator $faker
   */
  protected function setUpMovies(ObjectManager $om, Faker\Generator $faker) {
    $movies = [];

    for ($i = 0 ; $i < 3 ; $i++) {
      $date = $faker->dateTimeBetween('+1 days', '+1 years');

      $movie = new Movie();
      $movie
          ->setLegacyId($faker->randomNumber())
          ->setActive(TRUE)
          ->setFeatured(FALSE)
          ->setFixedPosition(0)
          ->setInfo('{}')
          ->setLegacyIdBis($faker->word())
          ->setLegacyName($faker->title())
          ->setName($faker->title())
          ->setParent(NULL)
          ->setPosition(0)
          ->setPosterUrl($faker->imageUrl())
          ->setShortName($faker->title())
          ->setPremiere(FALSE)
          ->setType('single');
      $om->persist($movie);
      $movies[] = $movie;

      $session = new Session();
      $session
          ->setLegacyId($faker->randomNumber())
          ->setPlatinum(FALSE)
          ->setInfo('{}')
          ->setActive(TRUE)
          ->setCinema($this->cinema)
          ->setCinemom(FALSE)
          ->setDate($date)
          ->setDateTime($date)
          ->setExtreme(FALSE)
          ->setGroupId(0)
          ->setLegacyCinemaId($this->cinema->getLegacyId())
          ->setLegacyId($faker->randomNumber())
          ->setMovie($movie)
          ->setPremium(FALSE)
          ->setTime($date)
          ->setTz('America/Mexico_City')
          ->setUpdating(FALSE);
      $om->persist($session);
    }

    $movies[0]->setGroupId('foo');
    $movies[1]->setGroupId('foo');
    $movies[2]->setGroupId('bar');
  }

}