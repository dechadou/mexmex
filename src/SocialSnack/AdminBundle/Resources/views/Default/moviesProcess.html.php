<?php
$request = $this->container->get('request');
$routeName = $request->get('_route');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Proceso en ejecución - Cinemex</title>
    
    <script>
      alert("Se terminó la ejecución del proceso");
    </script>
  </head>

  <body>
    <p>Se terminó la ejecución del proceso</p>
  </body>
</html>
