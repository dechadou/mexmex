<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityAppVersion extends AbstractEntityTask implements UrlTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('rest_app_versions', ['device' => $this->entity->getDevice()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_app_versions', ['device' => $this->entity->getDevice()]),
            'env'  => 'front'
        ],
    ];
  }

}