<?php

namespace SocialSnack\WsBundle\Tests\Command;

use SocialSnack\WsBundle\Command\TasksConsumeCommand;

class TasksConsumeCommandTest extends \PHPUnit_Framework_TestCase {

  public function testFilterTasks() {
    $command = new TasksConsumeCommandTesteable();

    $task1 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task1->method('getService')->willReturn('foo');
    $task1->method('getData')->willReturn(serialize(['cinema_id' => 1]));
    $task2 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task2->method('getService')->willReturn('foo');
    $task2->method('getData')->willReturn(serialize(['cinema_id' => 2]));
    $task3 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task3->method('getService')->willReturn('foo');
    $task3->method('getData')->willReturn(serialize(['cinema_id' => 1]));
    $task4 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task4->method('getService')->willReturn('bar');
    $task4->method('getData')->willReturn(serialize(['cinema_id' => 2]));
    $task5 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task5->method('getService')->willReturn('bar');
    $task5->method('getData')->willReturn(serialize(['cinema_id' => 1]));
    $task6 = $this->getMock('SocialSnack\WsBundle\Entity\Task');
    $task6->method('getService')->willReturn('bar');
    $task6->method('getData')->willReturn(serialize(['cinema_id' => 2]));

    $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
        ->disableOriginalConstructor()
        ->setMethods(['remove', 'flush'])
        ->getMock();

    $em->expects($this->at(0))
        ->method('remove')
        ->with($task3);
    $em->expects($this->at(1))
        ->method('remove')
        ->with($task6);
    $em->expects($this->atLeastOnce())
        ->method('flush');

    $command->setEntityManager($em);

    $tasks    = [$task1, $task2, $task3, $task4, $task5, $task6];
    $expected = [$task1, $task2, $task4, $task5];
    $output   = $command->filterTasks($tasks);

    $this->assertSame($expected, $output);
  }

}

class TasksConsumeCommandTesteable extends TasksConsumeCommand {

  public function filterTasks(array $tasks) {
    return parent::filterTasks($tasks);
  }

}