<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;

/**
 * Class ArticleHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Rodrigo Catalano
 */
class ArticleHandler {

  protected $doctrine;

  protected $front_utils;


  public function __construct(Registry $doctrine, FrontUtils $front_utils) {
    $this->doctrine = $doctrine;
    $this->front_utils = $front_utils;
  }

  public function generateArray($articles){
    $result = [];

    foreach ( $articles as $article ) {
      $tags = [];
      if ($article->getTags() != false){
        $tags = unserialize($article->getTags(FALSE));
      }
      $result[] = array(
          'id'            => $article->getId(),
          'title'         => $article->getTitle(),
          'content'       => $article->getContent(),
          'url'           => $this->front_utils->get_permalink($article,true),
          'cover'         => $this->front_utils->get_cms_url($article->getCover(),'733x286', 'CMS'),
          'thumb'         => $this->front_utils->get_cms_url($article->getThumb(),'236x222','CMS'),
          'date'          => $article->getDateCreated()->format('c'),
          'tags'          => $tags,
          'featured'      => $article->getFeatured()
      );
    }

    return $result;

  }

  public function getAll($limit=null,$offset=null) {

    /* @var $article_repo \SocialSnack\FrontBundle\Entity\ArticleRepository */
    $article_repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Article' );

    if ($limit == 0) $limit = null;
    if ($offset == 0) $offset = null;
    $articles = $article_repo->findBy(array(), array('dateCreated' => 'DESC'),$limit,$offset);
    return $this->generateArray($articles);
  }

  public function search($term){
    /* @var $article_repo \SocialSnack\FrontBundle\Entity\ArticleRepository */
    $article_repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Article' );
    $articles = $article_repo->search($term);
    return $this->generateArray($articles);
  }

} 