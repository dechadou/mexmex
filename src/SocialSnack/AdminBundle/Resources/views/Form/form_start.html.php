<?php $method = strtoupper($method) ?>
<?php if (!isset($attr['class'])) $attr['class'] = ''; ?>
<?php $attr['class'] .= ' form-horizontal tasi-form'; ?>
<?php $form_method = $method === 'GET' || $method === 'POST' ? $method : 'POST' ?>
<form name="<?php echo $form->vars['name'] ?>" method="<?php echo strtolower($form_method) ?>" action="<?php echo $action ?>"<?php foreach ($attr as $k => $v) { printf(' %s="%s"', $view->escape($k), $view->escape($v)); } ?><?php if ($multipart): ?> enctype="multipart/form-data"<?php endif ?>>
<?php if ($form_method !== $method): ?>
    <input type="hidden" name="_method" value="<?php echo $method ?>" />
<?php endif ?>