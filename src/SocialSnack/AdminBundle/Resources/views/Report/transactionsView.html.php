<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Transacción '.$t->getCode().' - Cinemex' );
$view['slots']->start('body');
?>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Transacción <strong><?php echo $t->getCode(); ?></strong>
      </header>
      
      <div class="panel-body">
        <div class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Booking</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getCode(); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Fecha de compra</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getPurchaseDate()->format('Y-m-d H:i'); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Función</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getSession()->getDateTime()->format('Y-m-d H:i'); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Película</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getMovie()->getName(); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Cine</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getCinema()->getName(); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Boletos</label>
            <p class="col-sm-10 form-control-static">
              <?php echo FrontHelper::confirmation_get_ticketslist($t, "<br/>"); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Asientos</label>
            <p class="col-sm-10 form-control-static">
              <?php echo FrontHelper::confirmation_get_seatslist($t, '<br/>') ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Total</label>
            <p class="col-sm-10 form-control-static">
              $ <?php echo number_format($t->getAmount()/100, 2, '.', ''); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getData('NombreCliente'); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Email</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getData('Email'); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Número de tarjeta</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getData('TDC'); ?>
            </p>
          </div>
          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Número de Invitado Especial</label>
            <p class="col-sm-10 form-control-static">
              <?php echo $t->getData('NUMIE'); ?>
            </p>
          </div>
          <button type="button" class="btn btn-default" onclick="window.history.back();">Volver</button>
        </div>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>