<?php

require_once __DIR__ . '/AppKernel.php';

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppCache extends HttpCache {

  
  protected function invalidate(Request $request, $catch = false) {
    if ('PURGEX' === $request->getMethod() || 'PURGE' === $request->getMethod()) {
      return $this->invalidate_req($request);
    }
    
    if ('PURGE-ALL' === $request->getMethod()) {
      return $this->invalidate_all($request);
    }
    
    return parent::invalidate($request, $catch);
  }
  
  
  /**
   * Purge Symfony's HTTP Cache for a specific URL.
   * 
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function invalidate_req(Request $request) {
    $response = new Response();
    $res      = '';
    $found    = FALSE;
    $domain   = $request->headers->get('X-Host');
    $uri      = preg_replace('/(https{0,1})\:\/\/(.+?)\//', '$1://' . $domain . '/', $request->getUri());
    
    if (!$this->getStore()->purge($uri)) {
      $res .= "$uri not found.\r\n";
    } else {
      $res .= "$uri purged.\r\n";
      $found = TRUE;
    }
    
    if ( $found ) {
      $response->setStatusCode(200, 'Purged');
    } else {
      $response->setStatusCode(404, 'Not purged');
    }
    $response->setContent($res);
    return $response;
  }
  
  
  /**
   * Purge the whole Symfony's HTTP Cache.
   * 
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function invalidate_all(Request $request) {
    $ua = $request->headers->get('User-Agent');
    
    /** @todo Find a way to get this value dynamically instead of hardcoded. */
    if ($ua != '6;811/6cs}1~4o_W]8641r-X67;^[G^6A188A635My@6ji1l3') {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
    
    $dir = $this->kernel->getCacheDir() . '/http_cache';
    $count = 0;
    
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );
    foreach ($files as $fileinfo) {
      if (!$fileinfo->isDir()) {
        @unlink($fileinfo->getRealPath());
        $count++;
      }
    }

    $response = new Response();
    $response->setStatusCode(200, 'Purged');
    $response->setContent(sprintf('%d resources purged.', $count));
    return $response;
  }

  
  protected function getOptions() {
    return array(
        'debug' => true,
//            'default_ttl'            => 0,
//            'private_headers'        => array('Authorization', 'Cookie'),
//            'allow_reload'           => false,
//            'allow_revalidate'       => false,
//            'stale_while_revalidate' => 2,
//            'stale_if_error'         => 60,
    );
  }

}
