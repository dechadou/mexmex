<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

abstract class BaseController extends Controller {

  abstract protected function get_required_role();
  
  protected function check_roles($role = FALSE) {
    if (false === $this->get('security.context')->isGranted($role ? $role : $this->get_required_role())) {
      throw new AccessDeniedHttpException();
    }
  }
  
}