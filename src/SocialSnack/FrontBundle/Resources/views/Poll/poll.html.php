<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Component\HttpKernel\Controller\ControllerReference;

$description = 'Cadena de cines en México. Presenta información relevante sobre cartelera, salas, películas en exhibición, concursos y mucho más.';
?>
<!DOCTYPE html>
<html>
	<head>
    <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
		<meta charset="UTF-8" />
		<title><?php $view['slots']->output( 'title', 'Cinemex - Encuesta Invitado Especial' ); ?></title>
		<link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/poll.css' ); ?>" />
		<link rel="shortcut icon" href="<?php echo $view['assets']->getUrl( '/favicon.ico' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
		<meta name="description" content="<?php echo $description; ?>" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript">
		$(function() {
			$("input[type=radio]").each(function() {
				$(this).closest('label').addClass('icon_' + $(this).val());
			});
			$("input[type=radio]").after("<br />");
			$("input[type=radio]").on({
				click: function(){
					$val = $(this).val();
					$id = $(this).parent().parent().parent().attr('id');
					$("#icon_"+$id).removeClass();
					$("#icon_"+$id).addClass('icon');
					$("#icon_"+$id).addClass('icon_'+$val);
					$(this).closest('.question-line').find('.check-row').removeClass('active');
					$(this).closest('.check-row').addClass('active');
				}
			});
		});
		</script>
	</head>

	<body>
		<div class="wrapper">
			<div class="photo"></div>
			<div class="logo"></div>
			<?php if ($message) { ?>
			<hr />
      <div class="success">Gracias. Tu opinión es importante</div>
			<!--<div class="message"><?php echo $message; ?></div>-->
			<?php } else { ?>
			<div class="cuentanos">Cuéntanos tu experiencia</div>
			<hr />
			<div class="fake-form">
				<div class="fake-line">
					<label for="COMPLEJO">Complejo que visitaste</label>
					<input type="text" id="COMPLEJO" value="<?php echo $info->RESPUESTA->COMPLEJO; ?>" readonly />
				</div>
				<div class="fake-line">
					<label for="PELICULA">Película que viste</label>
					<input type="text" id="PELICULA" value="<?php echo $info->RESPUESTA->PELICULA; ?>" readonly />
				</div>
				<div class="fake-line">
					<label for="FECHA">Fecha de visita</label>
					<input type="text" id="FECHA" value="<?php echo $info->RESPUESTA->FECHA; ?>" readonly />
				</div>
			</div>
			<form method="post">
			<div class="real-form">
			<?php foreach ((array)$questions->RESPUESTA->DESCRIPCION as $id => $question) {  ?>
				<div class="question-line <?php echo ($id == 10)?"last":""; ?>">
			<?php 	if ($id == 1) { ?>
					<h3>En escala de 0 al 4 en donde 0 es la calificación más baja y 4 la más alta, describe tu experiencia:</h3>
			<?php 	} ?>
					<div class="question"><?php echo $question; ?></div>

			<?php 	if ($id == 10) { ?>
					<?php echo $view['form']->widget($form['question_'.$questions->RESPUESTA->CVE[$id]]); ?>
			<?php 	} else { ?>
					<div class="icon" id="icon_<?php echo $id; ?>"></div>
					<div class="poll" id="<?php echo $id; ?>"><?php echo $view['form']->widget($form['question_'.$questions->RESPUESTA->CVE[$id]]); ?></div>
			<?php 	} ?>
				</div>
			<?php 	} ?>
			</div>
			<div class="submit-holder">
				<input type="submit" class="submit-button" value="Enviar" />
			</div>
			</form>
			<div class="logobn"></div>
			<?php } ?>
		</div>
	</body>
</html>
