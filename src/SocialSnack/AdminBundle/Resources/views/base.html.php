<?php
$request = $this->container->get('request');
$routeName = $request->get('_route');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <link rel="shortcut icon" href="img/favicon.png">

  <title><?php $view['slots']->output('title', 'Cinemex'); ?></title>

  <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/bootstrap.min.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/bootstrap-reset.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/assets/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/assets/advanced-datatable/media/css/jquery.dataTables.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/style.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/style-responsive.css') ?>" rel="stylesheet" />
  <link href="<?php echo $view['fronthelper']->get_static_url('admin/css/chosen.min.css') ?>" rel="stylesheet" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />


  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/jquery.dcjqaccordion.2.7.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/jquery.scrollTo.min.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/jquery.nicescroll.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/advanced-datatable/media/js/jquery.dataTables.min.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/advanced-datatable/media/js/dataTables.bootstrap.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/respond.min.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/common-scripts.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/ckeditor/ckeditor.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/swfobject.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/chosen.jquery.min.js') ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/js/chosen.proto.min.js') ?>"></script>

</head>

<body>
<section id="container" class="">
  <!--header start-->
  <header class="header white-bg">
    <div class="sidebar-toggle-box">
      <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
    </div>
    <!--logo start-->
    <a href="<?php echo $view['router']->generate('social_snack_admin_dashboard'); ?>" class="logo" >Cinemex<span>SocialSnack</span></a>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">
    </div>

    <a href="<?php echo $view['router']->generate('home'); ?>" id="head-goto-front">Ir al front</a>
  </header>
  <!--header end-->
  <!--sidebar start-->
  <aside>
    <div id="sidebar"  class="nav-collapse ">

      <!-- sidebar menu start-->
      <ul class="sidebar-menu" id="nav-accordion">

        <?php if ($app->getSecurity()->isGranted('ROLE_CONTENT')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (in_array($routeName, array('social_snack_movies_list', 'social_snack_admin_movie', 'social_snack_coming_homepage', 'social_snack_coming_movie', 'social_snack_add_coming_movie', 'social_snack_movies_approve'))) ? "active" : ""; ?>" >
              <i class="icon-film"></i>
              <span>Películas</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_movies_list'); ?>">Películas activas</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_movies_approve'); ?>">Aprobar películas</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_coming_homepage'); ?>">Próximas películas</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_movies_trackingCodes'); ?>">Códigos de tracking</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (in_array($routeName, array('social_snack_admin_cinema_list', 'social_snack_admin_cinema'))) ? "active" : ""; ?>" >
              <i class="icon-video-camera"></i>
              <span>Cines</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_cinema_list'); ?>">Listado de cines</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_cinema_download'); ?>">Descargar Listado</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (in_array($routeName, array('social_snack_article_list', 'social_snack_article_add', 'social_snack_article'))) ? "active" : ""; ?>">
              <i class="icon-pencil"></i>
              <span>Artículos</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_article_list'); ?>">Listado de artículos</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_article_add'); ?>">Agregar artículo</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (preg_match('/^social_snack_modal/', $routeName)) ? "active" : ""; ?>">
              <i class="icon-pencil"></i>
              <span>Modales</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_modal_list'); ?>">Listado de modales</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_modal_add'); ?>">Agregar Modal</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (preg_match('/^admin_splashscreen_/', $routeName)) ? "active" : ""; ?>">
              <i class="icon-pencil"></i>
              <span>SplashScreen</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('admin_splashscreen_list'); ?>">Listado de SP</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_splashscreen_add'); ?>">Agregar SplashScreen</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_snack_promo/', $routeName) ? "active" : ""; ?>">
              <i class="icon-gift"></i>
              <span>Promos</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_promoie_benefits_list'); ?>">Beneficios IE</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_promoie_list'); ?>">Promos IE</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_promoa_list'); ?>">Promos A</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_promoimportant_list'); ?>">Promos Destacadas</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_promo_list'); ?>">Promos</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_snack_attribute/', $routeName) ? "active" : ""; ?>">
              <i class="icon-tags"></i>
              <span>Atributos</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_attribute'); ?>">Lista de atributos</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_snack_admin_social_/', $routeName) ? "active" : ""; ?>" >
              <i class="icon-comment"></i>
              <span>Social</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_social_instagram'); ?>">Instagram</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_social_twitter'); ?>">Twitter</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^admin_config_/', $routeName) ? "active" : ""; ?>">
              <i class="icon-css3"></i>
              <span>Configuración</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('admin_config_background'); ?>">Cambiar fondos</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_config_emailBanner'); ?>">Banner email</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_config_options'); ?>">Opciones</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^admin_landing_/', $routeName) ? "active" : ""; ?>">
              <i class="icon-file"></i>
              <span>Landings</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('admin_landing_list'); ?>">Landings</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_landing_add'); ?>">Nueva landing</a></li>
            </ul>
          </li>
        <?php } ?>

        <?php if ($app->getSecurity()->isGranted('ROLE_CACHE')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_?snack_cache_/', $routeName) ? "active" : ""; ?>">
              <i class="icon-undo"></i>
              <span>Cache</span>
            </a>
            <ul class="sub">
              <li><a href="<?php echo $view['router']->generate('social_snack_cache_purge'); ?>">Limpiar cache</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_cache_url'); ?>">Por URL</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_cache_doctrine'); ?>">Doctrine</a></li>
            </ul>
          </li>
        <?php } ?>

        <?php if ($app->getSecurity()->isGranted('ROLE_PROCESS')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_?snack_admin_ws_/', $routeName) ? "active" : ""; ?>">
              <i class="icon-random"></i>
              <span>Webservice</span>
            </a>
            <ul class="sub">
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_ws_index'); ?>">Procesos</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_ws_import_movies'); ?>">Importar películas</a></li>
              <li><a href="<?php echo $view['router']->generate('socialsnack_admin_ws_session_debugger'); ?>">Debug funciones</a></li>
              <li><a href="<?php echo $view['router']->generate('socialsnack_admin_ws_ping_servers'); ?>">Estado servidores</a></li>
              <li><a href="<?php echo $view['router']->generate('socialsnack_admin_ws_cinemas_UpdatesLog'); ?>">Log actualizaciones</a></li>
            </ul>
          </li>
        <?php } ?>

        <?php if ($app->getSecurity()->isGranted('ROLE_REPORTS')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_snack_admin_(transactions|report)/', $routeName) ? "active" : ""; ?>">
              <i class="icon-list"></i>
              <span>Reportes</span>
            </a>
            <ul class="sub">
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_transactions_graphs'); ?>">Gráficos</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_transactions_list'); ?>">Transacciones</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_reports'); ?>">Reportes</a></li>
              <li><a href="<?php echo $view['router']->generate('social_snack_admin_report_monitor'); ?>">Monitor</a></li>
              <?php /*<li><a  href="<?php echo $view['router']->generate('social_snack_promoimportant_list'); ?>">Usuarios registrados</a></li>*/ ?>
            </ul>
          </li>
        <?php } ?>

        <?php if ($app->getSecurity()->isGranted('ROLE_APPS')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo (preg_match('/^(admin_app|social_snack_admin_clients_)/', $routeName)) ? "active" : ""; ?>">
              <i class="icon-mobile-phone"></i>
              <span>Apps</span>
            </a>
            <ul class="sub">
              <li><a href="<?php echo $view['router']->generate('admin_appversions_list'); ?>">Versiones Apps</a></li>
              <li><a href="<?php echo $view['router']->generate('admin_apps_push_send'); ?>">Consola Push</a></li>
              <li><a href="<?php echo $view['router']->generate('admin_apps_push_bulk'); ?>">Push Masivo</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_clients_list'); ?>">Clientes API</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_clients_add'); ?>">Nuevo cliente</a></li>
              <li><a  href="<?php echo $view['router']->generate('admin_apps_generateToken'); ?>">Obtener token</a></li>
            </ul>
          </li>
        <?php } ?>

        <?php if ($app->getSecurity()->isGranted('ROLE_ADMINS')) { ?>
          <li class="sub-menu">
            <a href="javascript:;" class="<?php echo preg_match('/^social_snack_admin_admin_/', $routeName) ? "active" : ""; ?>" >
              <i class="icon-user"></i>
              <span>Usuarios</span>
            </a>
            <ul class="sub">
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_admin_list'); ?>">Listado de usuarios</a></li>
              <li><a  href="<?php echo $view['router']->generate('social_snack_admin_admin_add'); ?>">Nuevo usuario</a></li>
            </ul>
          </li>
        <?php } ?>

        <li>
          &nbsp;
        </li>

        <li>
          <a  href="<?php echo $view['router']->generate('social_snack_admin_logout'); ?>">
            <i class="icon-signout"></i>
            <span>Log-out</span>
          </a>
        </li>
      </ul>
      <!-- sidebar menu end-->

    </div>
  </aside>
  <!--sidebar end-->
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper">
      <!-- page start-->

      <?php if (isset($purge) && !empty($purge)) { ?>
        <div><pre><?php echo implode("\r\n", $purge); ?></pre></div>
      <?php } ?>

      <?php $view['slots']->output('body'); ?>

      <!-- page end-->
    </section>
  </section>
  <!--main content end-->
</section>
</body>
</html>
