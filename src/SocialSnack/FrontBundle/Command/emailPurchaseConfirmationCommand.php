<?php

namespace SocialSnack\FrontBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\Asset\UrlPackage;

class emailPurchaseConfirmationCommand extends ContainerAwareCommand {

  protected function configure() {
    $this->setName('purchase:email_confirmation')
        ->addArgument('transaction_id', InputArgument::REQUIRED, 'Transaction ID')
        ->addOption('recipients', 'r', InputArgument::IS_ARRAY, 'Recipients email addresses.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $container = $this->getContainer();
    $container->enterScope('request');
    $container->set('request', new Request(), 'request');

    // @todo Following code is duplicated from \SocialSnack\FrontBundle\EventListener\AssetPackageInjector. Fix that.
    $assetsHelper = $container->get('templating.helper.assets');
    $assetsHelper->addPackage('QR', new UrlPackage($container->getParameter('qr_codes_url')));

    $transaction_id = $input->getArgument('transaction_id');
    $transaction    = $container->get('doctrine')->getRepository('SocialSnackFrontBundle:Transaction')->find($transaction_id);
    $recipients     = explode(',', $input->getOption('recipients'));

    if (!$transaction) {
      throw new \InvalidArgumentException(sprintf('Transaction %d doesn\'t exist.', $transaction_id));
    }

    $output->write(sprintf('Enviando confirmación de compra para transacción <fg=yellow>%d</> (<fg=yellow>%s</>) a <fg=yellow>%s</>...',
            $transaction_id,
            $transaction->getCode(),
            implode('</>, <fg=yellow>', $recipients))
    );

    $this->getContainer()->get('cinemex.mailer')->enqueue(array(
        'type'       => 'purchase',
        'trans_id'   => $transaction_id,
        'recipients' => $recipients,
    ));

    $output->writeln(' <fg=green>OK</>');
  }

}