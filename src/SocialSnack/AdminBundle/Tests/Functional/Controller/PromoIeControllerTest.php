<?php

namespace SocialSnack\AdminBundle\Tests\Functional\Controller;

use SocialSnack\AdminBundle\Tests\Controller\WebTestCase;
use SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData;
use SocialSnack\AdminBundle\Tests\Fixtures\Controller\PromoIeControllerData;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PromoIeControllerTest extends WebTestCase {

  /**
   * @group functional
   */
  public function testListAction_Forbidden() {
    $client = $this->getLoggedInClient([]);
    $client->request(
        'GET',
        '/missisippi/promosie'
    );
    $this->assertSame(403, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testListAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/promosie'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testListInactiveAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/promosie_inactive'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testAddAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/promosie/add'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testEditAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\PromoIeControllerData',
    ]);

    $promoId = PromoIeControllerData::$promos[0]->getId();
    $client->request(
        'GET',
        sprintf('/missisippi/promosie/%d', $promoId)
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   */
  public function testDeleteAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\PromoIeControllerData',
    ]);

    $promoId = PromoIeControllerData::$promos[0]->getId();
    $route   = sprintf('/missisippi/promosie/%d/delete', $promoId);

    $client->request(
        'GET',
        $route
    );
    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/missisippi/promosie', $client->getResponse()->headers->get('location'));

    $client->request(
        'GET',
        $route
    );
    $this->assertSame(404, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   */
  public function testBenefitsListAction_Forbidden() {
    $client = $this->getLoggedInClient([]);
    $client->request(
        'GET',
        '/missisippi/ieBenefits'
    );
    $this->assertSame(403, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   */
  public function testBenefitsListAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData',
    ]);

    $crawler = $client->request(
        'GET',
        '/missisippi/ieBenefits'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());

    $this->assertGreaterThan(
        0,
        $crawler->filter('html:contains("' . IeBenefitsData::$benefits[0]->getName() . '")')->count()
    );
  }


  /**
   * @group functional
   */
  public function testBenefitsListInactiveAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData',
    ]);

    $crawler = $client->request(
        'GET',
        '/missisippi/ieBenefits/inactive'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());

    $this->assertSame(
        0,
        $crawler->filter('html:contains("' . IeBenefitsData::$benefits[0]->getName() . '")')->count()
    );
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testBenefitsAddAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/ieBenefits/add'
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   */
  public function testBenefitsAddAction_Post() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);

    $imageFile = new UploadedFile($client->getKernel()->locateResource('@SocialSnackAdminBundle/Tests/Resources/test-upload-image.jpg'),'test-upload-image.jpg');

    $client->request(
        'POST',
        '/missisippi/ieBenefits/add',
        [
            'name' => 'New name',
            'description' => 'New description',
            'active' => 1,
            'category' => 'basic',
        ],
        [
            'imageFile' => $imageFile
        ]
    );
    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertRegexp('/^\/missisippi\/ieBenefits\/[0-9]+$/', $client->getResponse()->headers->get('location'));

    $crawler = $client->request(
        'GET',
        $client->getResponse()->headers->get('location')
    );
    $this->assertGreaterThan(
        0,
        $crawler->filter('html:contains("New name")')->count()
    );
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testBenefitsEditAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData',
    ]);

    $benefitId = IeBenefitsData::$benefits[0]->getId();
    $client->request(
        'GET',
        sprintf('/missisippi/ieBenefits/%d', $benefitId)
    );
    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   */
  public function testBenefitsEditAction_Post() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData',
    ]);

    $imageFile = new UploadedFile($client->getKernel()->locateResource('@SocialSnackAdminBundle/Tests/Resources/test-upload-image.jpg'),'test-upload-image.jpg');
    $benefitId = IeBenefitsData::$benefits[0]->getId();
    $route     = sprintf('/missisippi/ieBenefits/%d', $benefitId);

    $client->request(
        'POST',
        $route,
        [
            'name' => 'Updated name',
            'description' => 'Updated description',
            'active' => 0,
            'category' => 'gold',
        ],
        [
            'imageFile' => $imageFile
        ]
    );
    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame($route, $client->getResponse()->headers->get('location'));

    $crawler = $client->request(
        'GET',
        $client->getResponse()->headers->get('location')
    );
    $this->assertGreaterThan(
        0,
        $crawler->filter('html:contains("Updated name")')->count()
    );
  }


  /**
   * @group functional
   */
  public function testBenefitsDeleteAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT'], [
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
        '\SocialSnack\AdminBundle\Tests\Fixtures\Controller\IeBenefitsData',
    ]);

    $benefitId = IeBenefitsData::$benefits[0]->getId();
    $route     = sprintf('/missisippi/ieBenefits/%d', $benefitId);

    $client->request(
        'GET',
        $route . '/delete'
    );
    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/missisippi/ieBenefits', $client->getResponse()->headers->get('location'));

    $client->request(
        'GET',
        $route
    );
    $this->assertSame(404, $client->getResponse()->getStatusCode());
  }

}