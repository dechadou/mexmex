<?php

namespace SocialSnack\FrontBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\FrontBundle\Entity\User;

class LoadUserData implements FixtureInterface {

  static $userIe;
  static $userNoIe;
  static $userInvalidIe;

  public function load(ObjectManager $manager) {
    $faker = Faker\Factory::create();
    $users = [];

    for ($i = 0; $i < 3; $i++) {
      $user = new User();
      $user
          ->setFirstName($faker->firstName())
          ->setLastName($faker->lastName())
          ->setEmail($faker->email())
          ->setPassword($faker->md5())
      ;
      $manager->persist($user);
      $users[] = $user;
    }

    $users[0]->setIecode(111111111111);
    self::$userIe = $users[0];
    $users[1]->setIecode(999999999999);
    self::$userInvalidIe = $users[1];
    $users[2]->setIecode(NULL);
    self::$userNoIe = $users[2];

    $manager->flush();
  }

}