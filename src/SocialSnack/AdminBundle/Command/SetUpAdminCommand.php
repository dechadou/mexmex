<?php

namespace SocialSnack\AdminBundle\Command;

use Doctrine\DBAL\DBALException;
use SocialSnack\AdminBundle\Entity\Admin;
use SocialSnack\AdminBundle\Entity\AdminRole;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetUpAdminCommand extends ContainerAwareCommand {

  protected $em;

  public function configure() {
    $this
        ->setName('admin:setup')
        ->setDescription('Set up the first admin user and all the required admin roles.')
    ;
  }

  public function execute(InputInterface $input, OutputInterface $output) {
    $this->em = $this->getContainer()->get('doctrine')->getManager();

    // Set up roles.
    $output->write('Setup roles... ');
    $roles = $this->em->getRepository('SocialSnackAdminBundle:AdminRole')->findAll();
    $roles = $this->setUpRoles($roles);
    $output->writeln('<info>OK</info>');

    // This command can be used to create a new admin only if the admins table is empty.
    $admins = $this->em->getRepository('SocialSnackAdminBundle:Admin')->findAll();
    if (count($admins) !== 0) {
      $output->writeln('<error>ERROR: Admins table is not empty.</error>');
      return;
    }

    // Ask user data.
    $output->writeln('Please enter the login information for the new admin user.');
    $dialog = $this->getHelper('dialog');

    $validator = $this->getContainer()->get('validator');

    $userValidator = function($value) use ($validator) {
      $minLength = 8;
      $constraints = [
          new \Symfony\Component\Validator\Constraints\Length([
              'min' => $minLength,
          ]),
      ];
      $error = $validator->validateValue($value, $constraints);
      if (count($error) > 0) {
        throw new \Exception(sprintf('Username is too short. Must be at least %d chars long.', $minLength));
      }
      return $value;
    };

    $passValidator = function($value) use ($validator) {
      $constraints = [
          new \Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints\PasswordStrength([
              'minStrength' => 5,
          ]),
      ];
      $error = $validator->validateValue($value, $constraints);
      if (count($error) > 0) {
        throw new \Exception('Password is too weak.');
      }
      return $value;
    };

    $username = $dialog->askAndValidate(
        $output,
        'Username: ',
        $userValidator
    );
    $password = $dialog->askHiddenResponseAndValidate(
        $output,
        'Password: ',
        $passValidator
    );

    // Create new admin user.
    $output->write('Create admin... ');
    $this->setUpAdmin($username, $password, $roles);
    $output->writeln('<info>OK</info>');
    $output->writeln(sprintf('User %s successfully created.', $username));
  }


  protected function setUpRoles($roles) {
    $roles_def = [
        'ROLE_ADMIN'    => 'Admin',
        'ROLE_USER'     => 'Usuario',
        'ROLE_PROCESS'  => 'Procesos manuales',
        'ROLE_CONTENT'  => 'Contenidos',
        'ROLE_REPORTS'  => 'Reportes',
        'ROLE_CACHE'    => 'Cache',
        'ROLE_ADMINS'   => 'Administradores',
        'ROLE_IP_BLOCK' => 'Bloquear IPs',
        'ROLE_APPS'     => 'Apps',
    ];

    // Avoid duplicated roles.
    $existingRoles = array_map(function($role) { return $role->getRole(); }, $roles);
    foreach ($existingRoles as $id) {
      unset($roles_def[$id]);
    }

    foreach ($roles_def as $id => $name) {
      $role = new AdminRole();
      $role
          ->setName($name)
          ->setRole($id)
      ;

      try {
        $this->em->persist($role);
        $this->em->flush();
        $roles[] = $role;
      } catch (DBALException $e) {
      }
    }

    return $roles;
  }


  protected function setUpAdmin($username, $password, $roles) {
    $admin = new Admin();
    $admin
        ->setUser($username)
        ->setPlainPassword($password)
        ->setSalt(md5(uniqid()))
        ->setRoles($roles)
    ;

    $factory = $this->getContainer()->get('security.encoder_factory');
    $encoder = $factory->getEncoder($admin);
    $admin->setPassword($encoder->encodePassword($admin->getPlainPassword(), $admin->getSalt()));

    $this->em->persist($admin);
    $this->em->flush();

    return $admin;
  }
}