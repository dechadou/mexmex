<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php foreach ( $items as $item ) { ?>
<div class="mycinema-sessions-group" <?php echo FrontHelper::get_movie_sessions_block_data($item, $platinum); ?>>
  <div>
    <?php if (isset($item['premium']) AND $item['premium'] === TRUE) { ?>
    <span class="movie-info-attr">Premium</span>
    <?php } ?>

    <?php
    $sess_types = array(
        'cinemom' => 'CineMá',
    );
    foreach ($sess_types as $type => $label) {
    ?>
    <?php if (isset($item[$type]) AND $item[$type] === TRUE) { ?>
    <span class="movie-info-attr movie-attr-<?php echo $type; ?>"><?php echo $label; ?></span>
    <?php } ?>
    <?php
    }
    ?>

    <?php foreach (FrontHelper::sort_movie_attrs(FrontHelper::get_movie_attrs($item['movie']->getAttr(), TRUE, TRUE)) as $type => $label) {; ?>
    <span class="movie-info-attr movie-attr-<?php echo $type; ?>"><?php echo $label; ?></span>
    <?php } ?>
  </div>

  <div class="mycinema-sessions icon icon-dark icon-clock pad-left">
    <?php /* @var $session \SocialSnack\WsBundle\Entity\Session */ ?>

    <?php
    $title = "Comprar boletos";
    $class = NULL;

    if (isset($item['cinemom']) AND $item['cinemom'] === TRUE) :
        $title = "Función para Mamás y Papás con Bebés";
        $class = ' data-tooltip-class="tooltip-cinemom" ';
    elseif (isset($item['jumbo']) AND $item['jumbo'] === TRUE) :
        $title = "Jumbo Pantalla";
        $class = ' data-tooltip-class="tooltip-jumbo" ';
    elseif (isset($item['macro']) AND $item['macro'] === TRUE) :
        $title = "Macropantalla";
        $class = ' data-tooltip-class="tooltip-macro" ';
    endif;

    foreach ( $item['sessions'] as $session ) {
    ?>
    <a href="<?php echo $view['router']->generate( 'checkout', array( 'session_id' => $session->getId() ) ); ?><?php if (isset($checkout_ref)) echo '?ref=' . $checkout_ref; ?>" rel="noindex,nofollow" title="<?php echo $title; ?>" <?php echo $class; ?> data-time="<?php echo $session->getTime()->format('Hi'); ?>"><?php echo $session->getTime()->format( 'h:i A' ); ?></a>
    <?php } ?>
  </div>
</div>
<?php } ?>
