<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ConfigController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/backgrounds", name="admin_config_background")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function backgroundAction(Request $request) {
    $this->check_roles();
    
    $options = $this->get('dyn_options');
    $options->useCache(FALSE);
    $bgcolor = $options->get('bgcolor');
    
    $files = array(
        'bg_1920' => "bg-1920.jpg",
        'bg_1360' => "bg-1360.jpg",
        'bg_1280' => "bg-1280.jpg",
    );
    $base_path = '/' . $this->container->getParameter('cms_upload_path') . 'bgs/';
    $purge = array();
    $purge_files = array();
    $formb = $this->createFormBuilder();

    foreach ($files as $field => $filename) {
      $formb->add($field, 'file', array('required' => FALSE));
    }
    $formb->add('bgcolor', 'text', array(
        'data' => $bgcolor,
        'attr' => array('class' => 'form-control'),
        'required' => FALSE,
    ));
    $formb->add('save', 'submit');
    $form = $formb->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $cache_purger = $this->get('ss.cache_purger');
      /* @var $cache_purger \SocialSnack\FrontBundle\Service\CachePurger */

      foreach ($files as $field => $filename) {
        if ($file = $form[$field]->getData()) {
          $this->container->get('ss.front.utils')->upload_without_rezise($filename, file_get_contents($file->getPathName()), $base_path);
          $purge_files[] = $base_path . $filename;
        }
      }

      if (count($purge_files)) {
        $cache_purger->enqueue(array('type' => 'assets', 'files' => $purge_files));
      }

      $new_bgcolor = $form['bgcolor']->getData();
      if ($new_bgcolor != $bgcolor) {
        $options->set('bgcolor', $new_bgcolor);
        $this->get('ss.front.utils')->build_dyn_css();
        $cache_purger->enqueue(array(
            'type' => 'assets',
            'files' => array('/assets/css/dyn-styles.css')
        ));
      }
      
      $purge = $cache_purger->purge_cache();
    }

    return $this->render('SocialSnackAdminBundle:Config:background.html.php', array('form' => $form->createView(), 'purge' => $purge));
  }


  /**
   * @Route("/emailBanner", name="admin_config_emailBanner")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function emailBannerAction(Request $request) {
    $this->check_roles();

    $options = $this->get('dyn_options');
    $options->useCache(FALSE);
    $values = $options->get(['email_banner_path', 'email_banner_i']);

    $email_banner_path = isset($values['email_banner_path']) ? $values['email_banner_path'] : NULL;
    $builder = $this->createFormBuilder([
        'banner_path' => $email_banner_path
    ]);

    $builder->add('banner', 'file', [
        'label' => 'Banner',
        'image_preview_path' => 'banner_path',
        'image_preview_size' => '',
        'required' => !$email_banner_path,
    ]);

    if ($email_banner_path) {
      $builder->add('banner_remove', 'checkbox', array(
          'label' => 'Eliminar banner',
          'mapped' => FALSE,
          'required' => FALSE,
      ));
    }
    $builder->add('submit', 'submit', ['label' => 'Guardar']);

    $form = $builder->getForm();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $purge_files = [];
      $cache_purger = $this->get('ss.cache_purger');
      /* @var $cache_purger \SocialSnack\FrontBundle\Service\CachePurger */

      if ($file = $form['banner']->getData()) {
        $base_path = 'email/';
        $filename_i = isset($values['email_banner_i']) ? ++$values['email_banner_i'] : 1;
        $filename = 'email_banner_' . $filename_i . '.jpg';
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);

        // @todo Set multiple vars in a single query.
        $options->set('email_banner_path', $base_path . $filename);
        $options->set('email_banner_i', $filename_i);
        $purge_files[] = $base_path . $filename;
      }

      if (isset($form['banner_remove']) && $form['banner_remove']->getData()) {
        $options->set('email_banner_path', '');
      }

      $cache_purger->enqueue(array('type' => 'assets', 'files' => $purge_files));
      $cache_purger->purge_cache();

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios se guardaron exitosamente.');

      return $this->redirect($this->generateUrl('admin_config_emailBanner'));
    }

    return $this->render('SocialSnackAdminBundle:Config:emailBanner.html.php', [
        'form' => $form->createView(),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
    ]);
  }


  /**
   * @Route("/options", name="admin_config_options")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function optionsAction(Request $request) {
    $options = $this->get('dyn_options');
    $options->useCache(FALSE);
    $form = $this->createFormBuilder([
      'external_failure_notice' => $options->get('external_failure_notice'),
      'extra' => 1,
    ]);

    // For some reason the form is not processed if the only field is the checkbox and it's not checked, so we add this
    // hidden field as a workaround for that issue.
    $form->add('extra', 'hidden');
    $form->add('external_failure_notice', 'checkbox', [
        'label' => 'Aviso de falla externa',
        'required' => FALSE,
    ]);
    $form->add('submit', 'submit', ['label' => 'Guardar']);
    $form = $form->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
      $value = $form->get('external_failure_notice')->getData();
      $options->set('external_failure_notice', $value);

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios se guardaron exitosamente.');

      return $this->redirect($this->generateUrl('admin_config_options'));
    }

    return $this->render('SocialSnackAdminBundle:Config:options.html.php', [
        'form' => $form->createView(),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
    ]);
  }

}
