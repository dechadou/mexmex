<?php

namespace SocialSnack\WsBundle\Exception\MovieGroup;

/**
 * Class MovieAlreadyGroupedException
 * @package SocialSnack\WsBundle\Exception\MovieGroup
 * @author Guido Kritz
 */
class MovieAlreadyGroupedException extends \Exception {

} 