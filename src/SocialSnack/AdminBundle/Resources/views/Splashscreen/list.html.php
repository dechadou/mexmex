<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de SplashScreens - Cinemex' );
$view['slots']->start('body');
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de SplashScreens
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <div class="clearfix">
            <div class="btn-group">
              <a class="btn btn-default" href="<?php echo $view['router']->generate('admin_splashscreen_add'); ?>">
                Nuevo Splashscreen <i class="icon-plus"></i>
              </a>
            </div>
          </div>
          <table  class="display table table-bordered table-striped" id="modal">
            <thead>
            <tr>
              <td>Nombre</td>
              <th>File</th>
              <th>Deep Link</th>
              <th>Activo</th>
              <th>Editar</th>
              <th>Borrar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($splash)){
              foreach ($splash as $sp) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $sp->getName() ?></td>
                  <td><img src="<?php echo $view['assets']->getUrl($sp->getImageH(), 'CMS'); ?>" alt="<?php echo $sp->getName(); ?>" width='160' /></td>
                  <td><?php echo $sp->getDeeplink(); ?></td>
                  <td><?php echo $sp->getActive() ? 'Sí' : 'No'; ?></td>
                  <td><a href="<?php echo $view['router']->generate('admin_splashscreen_edit', ['splash_id' => $sp->getId()]); ?>">Editar</a></td>
                  <td><a href="<?php echo $view['router']->generate('admin_splashscreen_delete', ['splash_id' => $sp->getId()]); ?>"  onclick="return confirm('Estás seguro?')">Borrar</a></td>
                </tr>
              <?php
              }
            }
            ?>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#modal').dataTable();
  } );
</script>
<?php $view['slots']->stop(); ?>
