<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\RestBundle\Exception\NotFoundRestException;
use SocialSnack\WsBundle\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SessionsController extends BaseController {
	
	
	/**
	 * @Route("/{session_id}", name="rest_sessions_single")
	 * @Method({"GET"})
	 */
  public function singleAction($session_id) {
    $result = $this->get('rest.session.handler')->getSerialized($session_id);

    if (!$result) {
      throw new NotFoundRestException();
    }

    $response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(15 * 60);
		return $response;
	}
	
	
}