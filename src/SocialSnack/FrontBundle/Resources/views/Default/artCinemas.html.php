<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Casa de Arte - Cines - Cinemex' );
$view['slots']->start('body')
?>
        <?php
        echo $view->render(
            'SocialSnackFrontBundle:Sections:artMenu.html.php',
            array( 'route' => $route )
        );
        ?>

				<div class="clearfix items-grid row">
          
          <?php foreach ( $art_cinemas as $cinema ) { ?>
          <?php $cinema_link = $view['fronthelper']->get_cinema_link( $cinema ); ?>
          <div class="col col-xs-1-3 col-sm-1-4 col-md-1-5 cinemas-grid-item">
            <a href="<?php echo $cinema_link; ?>"><img src="<?php echo $view['assets']->getUrl('assets/img/no-photo-187x187.jpg', 'Static'); ?>" alt="<?php echo $cinema->getName(); ?>" /></a>
            <h2 class="cinemas-grid-title"><a href="<?php echo $cinema_link; ?>"><?php echo $cinema->getName(); ?></a></h2>
          </div>
          <?php } ?>
					
				</div>
<?php $view['slots']->stop(); ?>