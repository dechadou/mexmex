<?php

use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de Clientes de API - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de Clientes de API
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <div class="clearfix">
            <div class="btn-group">
              <a class="btn btn-default" id="editable-sample_new" href="<?php echo $view['router']->generate('social_snack_admin_clients_add'); ?>">
                Nuevo cliente <i class="icon-plus"></i>
              </a>
            </div>
          </div>
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Estado</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($clients as $client) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $client->getName(); ?></td>
                  <td><?php echo $client->getDescription(); ?></td>
                  <td><?php echo $client->getActive() ? 'Activo' : 'Inactivo'; ?></td>
                  <td>
                    <a href="<?php echo $view['router']->generate('social_snack_admin_clients_singleUsage', array('id' => $client->getId())); ?>">Uso</a>
                    <a href="<?php echo $view['router']->generate('social_snack_admin_clients_edit', array('client_id' => $client->getId())); ?>">Editar</a>
                  </td>
                </tr>
                <?php
              }
              ?>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[4, "desc"]]
    });
  });
</script>
<?php $view['slots']->stop(); ?>