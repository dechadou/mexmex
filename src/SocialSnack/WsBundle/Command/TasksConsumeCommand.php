<?php
namespace SocialSnack\WsBundle\Command;

use Doctrine\ORM\EntityManager;
use SocialSnack\WsBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

class TasksConsumeCommand extends LoopedCommandAbstract {

  protected $tasks_per_iteration = 25;

  protected $max_attempts = 3;

  protected $lock_id = 'ws:tasks:consume';

  protected $start_time = [];

  protected $tasks_stats = ['times' => [], 'max' => NULL];

  protected $iterations = 0;

  /**
   * @var \SocialSnack\WsBundle\Handler\LockHandler
   */
  protected $lock_handler;

  protected $logger;

  protected $observers = [];

  protected $em;


	protected function configure() {
		$this
        ->setName('ws:tasks:consume')
        ->setDescription('Process queued tasks')
        ->addOption('processes', 'ps', InputOption::VALUE_OPTIONAL, 'Output of ps command.', NULL)
        ->setHelp('Run with "ps aux | grep php | xargs -0 -I ps php app/console ws:tasks:consume --processes ps"')
    ;
	}


  /**
   * @todo Move the logic to a service.
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   * @return mixed
   */
	protected function execute(InputInterface $input, OutputInterface $output) {
    // @todo Set a time limit
    set_time_limit( 0 );

    // @todo Use DI.
    $this->setObservers();

    $this->lock_handler = $this->getContainer()->get('lock.handler');
    $this->logger       = $this->getContainer()->get('ws.logger');

    // Set lock (and check before)
    $processes = $input->getOption('processes');
    $this->lock_handler->setLock($this->lock_id, ['check' => TRUE, 'processes' => $processes], ['alert_time' => 80 * 60]);

		$this->startTimer();

    $doctrine   = $this->getContainer()->get( 'doctrine' );
    $em         = $doctrine->getManager();
    $report     = array();

    $this->setEntityManager($em);
    $this->logger->info('Begin tasks:consume process.');

    while(1) {
      $this->iterations++;

      $this->checkStatus();

      $tasks = $this->getTasks();

      if ( !$tasks )
        break;

      /* @var $task    \SocialSnack\WsBundle\Entity\Task            */
      /* @var $service \SocialSnack\WsBundle\Service\SessionUpdater */
      foreach ( $tasks as $task ) {
        $this->startTimer('task');

        if (!isset($report[$task->getService()])) {
          $report[$task->getService()] = array('success' => 0, 'error' => 0);
        }
        $service = $this->getContainer()->get($task->getService());
        $data    = unserialize($task->getData());
        try {
          $success = $service->execute($data);
        } catch (\Exception $e) {
          $success = FALSE;
        }

        $this->update($task, $success);

        if ( !$success ) {
          $task->incAttempts();
          $output->writeln(sprintf('Fail. Attempt #%d', $task->getAttempts()));
          if ( $this->max_attempts == $task->getAttempts() ) {
            $report[$task->getService()]['error']++;
            // Log.
            $this->logger->warning(sprintf('Task failed after %d attempts with error: %s. Task data: %s',
                $this->max_attempts,
                $service->getError(),
                $task->getData()
            ));
            $em->remove($task);
          }
        } else {
          $report[$task->getService()]['success']++;
          $em->remove($task);
          $output->writeln('Success!');
        }
        $em->flush();

        $this->trackTaskTime($task);

        unset($data);
        unset($service);
      }

      $em->clear();
      unset($tasks);
      $output->writeln( sprintf( 'Iteration #%d completed.', $this->iterations ) );
    } // while

    $output->writeln( 'Queue empty, yay!' );

    $execution_time = $this->getElapsedTime();
    $output->writeln( 'Total Execution Time: '. ($execution_time/60) . ' Mins' );

    $this->logger->info(sprintf('Finish tasks:consume process. Time: %s', $execution_time));
    foreach ( $report as $k => $v ) {
      $this->logger->info(sprintf('Tasks:consume %s: %d success/%d error', $k, $v['success'], $v['error']));
    }

    $this->terminate();
	}


  /**
   * @param EntityManager $manager
   * @return $this
   */
  public function setEntityManager(EntityManager $manager) {
    $this->em = $manager;

    return $this;
  }


  /**
   * @return EntityManager
   */
  public function getEntityManager() {
    return $this->em;
  }


  protected function terminate($normal = TRUE) {
    $this->lock_handler->releaseLock($this->lock_id);

    if (!$normal) {
      $this->logger->error(sprintf('Command %s terminated abnormally.', $this->getName()));
    }

    $this->logStats();

    die();
  }


  /**
   * @return array
   */
  protected function getTasks() {
    $doctrine   = $this->getContainer()->get( 'doctrine' );
    /* @var $task_repo \Doctrine\ORM\EntityRepository */
    $task_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Task' );

    $tasks = $task_repo->findBy(
        array('locked' => NULL),
        array(
            'priority'  => 'ASC', // 1 = highest priority
            'attempts'  => 'ASC', // leave failed requests for later
            'dateAdded' => 'ASC',
        ),
        $this->tasks_per_iteration
    );

    return $this->filterTasks($tasks);
  }


  protected function filterTasks(array $tasks) {
    // Remove duplicates (ie: same service and same data).
    $tasksOk     = [];
    $tasksRemove = [];
    $tmp         = [];
    foreach ($tasks as $i => $task) {
      $tmp[$i] = implode('|', [$task->getService(), $task->getData()]);
    }
    $tmp = array_unique($tmp);
    foreach ($tasks as $i => $task) {
      if (array_key_exists($i, $tmp)) {
        $tasksOk[] = $task;
      } else {
        $tasksRemove[] = $task;
      }
    }

    // Delete duplicated tasks.
    $em = $this->getEntityManager();
    foreach ($tasksRemove as $task) {
      $em->remove($task);
    }
    $em->flush();

    return $tasksOk;
  }


  protected function trackTaskTime(Task $task) {
    $task_time = $this->getElapsedTime('task');
    $this->clearTimer('task');

    $old_max = count($this->tasks_stats['times']) ? max($this->tasks_stats['times']) : 0;
    if ($task_time > $old_max) {
      $this->tasks_stats['max'] = [
          'service' => $task->getService(),
          'data'    => $task->getData(),
          'time'    => $task_time,
      ];
    }

    $this->tasks_stats['times'][] = $task_time;
  }


  protected function logStats() {
    $count = count($this->tasks_stats['times']);

    if (!$count) {
      return;
    }

    $sum = array_sum($this->tasks_stats['times']);
    $avg = $sum / $count;

    $this->logger->info(sprintf(
        '%d tasks consumed. Avg time per task: %d. Longest task: %s (%d): %s',
        $count,
        $avg,
        $this->tasks_stats['max']['service'],
        $this->tasks_stats['max']['time'],
        $this->tasks_stats['max']['data']
    ));
  }


  protected function getMemoryLimit() {
    return 67108864; // 64M
  }

  protected function getTimeLimit() {
    return 3600; // 1 hour
  }

  protected function getIterationsLimit() {
    return 50;
  }

  protected function getLogger() {
    return $this->logger;
  }


  protected function setObservers() {
    $this->observers[] = $this->getContainer()->get('ws.observer.sessionUpdater');
  }


  protected function update(Task $task, $success) {
    foreach ($this->observers as $observer) {
      $observer->update($task, $success, []);
    }
  }

}