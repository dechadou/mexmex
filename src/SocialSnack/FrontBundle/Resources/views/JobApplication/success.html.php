<?php
$view->extend('SocialSnackFrontBundle:JobApplication:base.html.php');
$view['slots']->set('title', 'Solicitud de Empleo - Cinemex');
$view['slots']->start('jobs_body');
?>

<h1 class="content-box-title">Forma de Solicitud de Empleo</h1>

<div class="entry-body">
  <p>¡¡La solicitud se ha enviado con éxito!!</p>

<?php if ($area_type === 'cafes') : ?>
  <p>Gracias por querer ser parte del grupo; el proceso de selección puede tardar alrededor de dos semanas, si requieres de mayor información, solicítala enviando un correo a
    <a href="mailto:&#99;&#97;&#102;&#101;&#99;&#101;&#110;&#116;&#114;&#97;&#108;&#103;&#111;&#117;&#114;&#109;&#101;&#116;&#64;&#99;&#105;&#110;&#101;&#109;&#101;&#120;&#46;&#110;&#101;&#116;">&#99;&#97;&#102;&#101;&#99;&#101;&#110;&#116;&#114;&#97;&#108;&#103;&#111;&#117;&#114;&#109;&#101;&#116;&#64;&#99;&#105;&#110;&#101;&#109;&#101;&#120;&#46;&#110;&#101;&#116;</a>.</p>
<?php else : ?>
  <p>Gracias por querer ser parte de la magia del cine; el proceso de selección
    puede tardar alrededor de dos semanas, si requieres de mayor información, solicítala
    en el Cinemex que elegiste o envía un correo a
    <a href="mailto:&#98;&#117;&#122;&#111;&#110;&#64;&#99;&#105;&#110;&#101;&#109;&#101;&#120;&#46;&#110;&#101;&#116;">&#98;&#117;&#122;&#111;&#110;&#64;&#99;&#105;&#110;&#101;&#109;&#101;&#120;&#46;&#110;&#101;&#116;</a>.</p>
<?php endif; ?>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('ga_extras_after'); ?>
ga('send', 'event', 'Jobs form', 'submit', '<?php echo $area_type . ' - ' . $venue; ?>', 1);
<?php $view['slots']->stop();
