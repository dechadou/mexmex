<?php
$view->extend('SocialSnackFrontBundle:JobApplication:base.html.php');
$view['slots']->set('title', 'Solicitud de Empleo - Cinemex');
?>

<?php $view['slots']->start('jobs_body'); ?>
<article class="entry-full">
  <h1 class="content-box-title">Nosotros</h1>

  <div class="entry-body">
    <p>Cinemex, la sexta cadena de cines más grande del mundo, ha llevado La Magia del Cine a millones de personas alrededor de México desde su fundación en agosto de 1995, fecha en la cual, abrió sus puertas al público con el complejo Cinemex Altavista.</p>
    <p>El concepto de Cinemex llegó a revolucionar la industria del cine en nuestro país al instalar salas múltiplex y butacas tipo estadio para poder satisfacer las necesidades únicas del mercado.</p>
    <p>En Cinemex, nuestros clientes son nuestros invitados y nuestra misión es ser los mejores en divertir a la gente. Con la idea de poderle llegar a más personas nos hemos expandido a múltiples ciudades dentro de México a lo largo del tiempo, teniendo un crecimiento del 35% anual en los últimos 6 años. Hoy en día contamos con más de 271 complejos y 2,361 pantallas alrededor del país y esperando crecer aún más.</p>
    <p>Para que nuestros invitados disfruten al máximo La Magia del Cine ofrecemos innovadores conceptos como Platino Cinemex y Premium Cinemex, cines de vanguardia equipados con lujosos asientos y servicio de alimentos a la sala; también el concepto CinemeXtremo, la cual posiciona a Cinemex como cadena líder en México en número de salas con la última tecnología en imagen y sonido Dolby ATMOS.</p>
    <p>Al igual contamos con Cinemex 3D, la experiencia X4D, Casa de Arte y Contenido Alternativo, siendo este último un espacio donde los invitados podrán disfrutar de partidos de la NFL, UEFA Champions League, temporadas de Ópera, Ballet Bolshoi, entre otros. Adicional, ofrecemos funciones CineMá, donde los invitados podrán asistir acompañados de sus bebés a una sala acondicionada especialmente para ellos.</p>
    <p>Hoy en día, podemos orgullosamente comunicar que somos una cadena de cines 100% digital donde la calidad de la imagen y sonido crea una experiencia inolvidable para nuestros invitados, cumpliendo nuestra misión de ser los mejores en divertir a la gente.</p>
    <p>El año pasado fuimos reconocidos como el “Exhibidor Internacional del Año” durante la Convención Anual de ShowEast 2013, premio que reconoce nuestro liderazgo, innovación y crecimiento constante en los últimos años.</p>
  </div>

</article>
<?php $view['slots']->stop(); ?>
