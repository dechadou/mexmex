<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class LandingRepository extends EntityRepository {

  protected function _findWithPages($key, $value) {
    $qb = $this->createQueryBuilder('q');
    $qb
        ->select('q', 'p')
        ->innerJoin('q.pages', 'p')
        ->andWhere('q.active = 1')
        ->andWhere('p.active = 1')
        ->orderBy('p.position', 'ASC')
    ;

    if ($key === 'id') {
      $qb->andWhere('q = :id');
    } else {
      $qb->andWhere(sprintf('q.%s = :id', $key));
    }

    $qb->setParameter('id', $value);

    $q = $qb->getQuery();
    $cache_id = __METHOD__ . ':' . $key . ':' . $value;
    $q->useResultCache(TRUE, 60 * 10, $cache_id);

    try {
      return $q->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
  }

  public function findWithPages($id) {
    return $this->_findWithPages('id', $id);
  }

  public function findWithPagesBySlug($slug) {
    return $this->_findWithPages('slug', $slug);
  }

}