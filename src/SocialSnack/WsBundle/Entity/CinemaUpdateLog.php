<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CinemaUpdateLog
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SocialSnack\WsBundle\Entity\CinemaUpdateLogRepository")
 */
class CinemaUpdateLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Cinema
     *
     * @ORM\ManyToOne(targetEntity="Cinema")
     */
    private $cinema;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=100)
     */
    private $source;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ran", type="datetime")
     */
    private $dateRan;

    /**
     * @var array
     *
     * @ORM\Column(name="result", type="json_array")
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string")
     */
    private $status;


    public function __construct() {
        $this->dateRan = new \DateTime('now', new \DateTimeZone('America/Mexico_City'));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cinema
     *
     * @param Cinema $cinema
     * @return CinemaUpdateLog
     */
    public function setCinema($cinema)
    {
        $this->cinema = $cinema;
    
        return $this;
    }

    /**
     * Get cinema
     *
     * @return Cinema
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return CinemaUpdateLog
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set dateRan
     *
     * @param \DateTime $dateRan
     * @return CinemaUpdateLog
     */
    public function setDateRan($dateRan)
    {
        $this->dateRan = $dateRan;
    
        return $this;
    }

    /**
     * Get dateRan
     *
     * @return \DateTime 
     */
    public function getDateRan()
    {
        return $this->dateRan;
    }

    /**
     * Set result
     *
     * @param array $result
     * @return CinemaUpdateLog
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return array 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CinemaUpdateLog
     */
    public function setStatus($status)
    {
      $this->status = $status;

      return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }
}
