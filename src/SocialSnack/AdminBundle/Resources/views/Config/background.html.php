<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Cambiar fondos del sitio' );
$view['slots']->start('body');
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Cambiar fondos del sitio
            </header>
            
            <div class="panel-body">
                <form class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Color de fondo</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['bgcolor']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Fondo 1920px</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['bg_1920']); ?>
                            <img src="<?php echo $view['assets']->getUrl('bgs/bg-1920.jpg', 'CMS'); ?>" width="250" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Fondo 1360px</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['bg_1360']); ?>
                            <img src="<?php echo $view['assets']->getUrl('bgs/bg-1360.jpg', 'CMS'); ?>" width="250" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Fondo 1280px</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['bg_1280']); ?>
                            <img src="<?php echo $view['assets']->getUrl('bgs/bg-1280.jpg', 'CMS'); ?>" width="250" />
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </section>
    </div>
</div>
<?php $view['slots']->stop(); ?>