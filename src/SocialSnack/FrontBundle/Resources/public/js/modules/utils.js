$.fn.scroll_to = function( offset, only_if_out_of_viewport ) {
	var	$t			=	$( this ),
			target	=	$t.data( 'target' ),
			$target;
	if ( '#' == target.substr( 0,1 ) ) {
		$target = $( target );
	} else if ( 'next' == target ) {
		$target = $t.closest( '.screen' ).nextAll( '.screen' );
	}
	$.scroll_to( $target, offset, only_if_out_of_viewport );
}

$.scroll_to = function( $target, offset, only_if_out_of_viewport ) {
	offset  = offset || 0;
	$target = $( $target );
	if ( !$target.length )
		return;
	
  var top = $target.offset().top;
  
  if ( only_if_out_of_viewport ) {
    var h = $target.outerHeight(),
        wh = $( window ).height(),
        st = $( document ).scrollTop();

    if ( top > st && top < st + wh )
      return;
  }
  
	scrolling = true;
	$( 'html, body' ).stop().animate( { scrollTop : $target.offset().top + offset },
		{
			speed			:	'slow',
			complete	:	function() {
				scrolling = false;
			}
		}
	);
}

$( '[rel=scroll-to]' ).bind( 'click', function( e ) {
	e.preventDefault();
	$( this ).scroll_to();
} );


email_verify = function( email ) {
		return /^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test( email );
};

function clog() {
	if ( !window.console )
		return;
	if ( !window.debug )
		return;
	
	for ( var i in arguments ) {
		console.log( arguments[i] );
	}
}

/////////////////////////////////
// BEGIN Credit card validation
///////////////////////////////

/**
 * Validate credit card number.
 * 
 * @param {type} ccnum
 * @returns {Boolean}
 */
function validate_ccnum( ccnum ) {
	var regexs = {
		visa   : /^4\d{3}[\s-]?\d{4}[\s-]?\d{4}[\s-]?\d{4}$/, // Visa: length 16, prefix 4, dashes optional.
		mc     : /^5[1-5]\d{2}[\s-]?\d{4}[\s-]?\d{4}[\s-]?\d{4}$/,// Mastercard: length 16, prefix 51-55, dashes optional.
		disc   : /^6011[\s-]?\d{4}[\s-]?\d{4}[\s-]?\d{4}$/, // Discover: length 16, prefix 6011, dashes optional.
		amex   : /^3[4,7]\d{13}$/, // American Express: length 15, prefix 34 or 37.
		diners : /^3[0,6,8]\d{12}$/ // Diners: length 14, prefix 30, 36, or 38.
	};
  
	regex_passed = false;
	for ( var i in regexs ) {
		if ( regexs[i].test( ccnum ) ) {
			regex_passed = true;
			break;
		}
	}
	if ( !regex_passed )
		return false;

	// Remove spaces and dashes for the checksum checks to eliminate negative numbers
	ccnum = ccnum.replace( /\s|-/g, '' );
	// Checksum ("Mod 10")
	// Add even digits in even length strings or odd digits in odd length strings.
	var checksum = 0;
	for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
		checksum += parseInt(ccnum.charAt(i-1));
	}
	// Analyze odd digits in even length strings or even digits in odd length strings.
	for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
		var digit = parseInt(ccnum.charAt(i-1)) * 2;
		if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
	}
	
	if ((checksum % 10) == 0)
		return true;
	else
		return false;
}


/**
 * Validate Card Security Code.
 * 
 * @param {type} csc
 * @returns {RegExp}
 */
function validate_csc( csc ) {
	return /^\d{3,4}$/.test( csc );
}


/**
 * Validate credit card expiry date.
 * 
 * @param {type} m Month
 * @param {type} y Year
 * @returns {Boolean}
 */
function validate_expire_date( m, y ) {
	var now = new Date(),
	    c_y = now.getFullYear(),
			c_m = now.getMonth() + 1
	;
	
	if ( !m || !y )
		return false;
	
	m *= 1;
	y *= 1;
	
	// If a two digit year entered assume is in the current centuty.
	if ( y < 100 ) {
		y += c_y - ( c_y % 100 );
	}
	
	if ( y < c_y )
		return false;
	
	if ( y == c_y && m < c_m )
		return false;
	
	return true;
}
///////////////////////////////
// END Credit card validation
///////////////////////////////


///////////////////
// BEGIN Prototypes 
///////////////////
Number.prototype.toRad = function() {
	return this * Math.PI / 180;
}
///////////////////
// END Prototypes 
///////////////////


/**
 * Calculate the distance between two coordinates.
 * 
 * @param {Object} pos_a
 * @param {Object} pos_b
 * @returns {Number}
 */
function haversine( pos_a, pos_b ) {
	var lat2 = pos_b.lat,
	    lon2 = pos_b.lng,
	    lat1 = pos_a.lat,
	    lon1 = pos_a.lng
	;

	var R    = 6371; // km 
	//has a problem with the .toRad() method below.
	var x1   = lat2-lat1;
	var dLat = x1.toRad();  
	var x2   = lon2-lon1;
	var dLon = x2.toRad();  
	var a    = Math.sin(dLat/2) * Math.sin(dLat/2) + 
						 Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
						 Math.sin(dLon/2) * Math.sin(dLon/2);
	var c    = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d    = R * c;
	
	return d;
}

/**
 * Calculate how similar are two strings.
 * 
 * @param {string} a
 * @param {string} b
 * @returns {float}
 */
function levenshteinenator( a, b ) {
	var cost,	
	    m = a.length,
	    n = b.length
	;
	
	// make sure a.length >= b.length to use O(min(n,m)) space, whatever that is
	if (m < n) {
		var c=a;a=b;b=c;
		var o=m;m=n;n=o;
	}
	
	var r = new Array();
	r[0] = new Array();
	for (var c = 0; c < n+1; c++) {
		r[0][c] = c;
	}
	
	for (var i = 1; i < m+1; i++) {
		r[i] = new Array();
		r[i][0] = i;
		for (var j = 1; j < n+1; j++) {
			cost = (a.charAt(i-1) == b.charAt(j-1))? 0: 1;
			r[i][j] = minimator(r[i-1][j]+1,r[i][j-1]+1,r[i-1][j-1]+cost);
		}
	}
	
	return r[m][n] / ( m > n ? m : n );
}


/**
 * Return the smallest of the three values passed in.
 * 
 * Used by levenshteinenator().
 * 
 * @param {type} x
 * @param {type} y
 * @param {type} z
 * @returns {unresolved}
 */
function minimator( x, y, z ) {
	if ( x < y && x < z ) return x;
	if ( y < x && y < z ) return y;
	return z;
}


$.fn.ctl_msg = function( msg, width ) {
  var $bt = $( this );
  var $m = $( '<div/>', { 'class' : 'msg msg-error balloon', text : msg } ).appendTo( $bt.parent() );
  if ( !width ) {
    width = $bt.outerWidth();
  }
  $m.css( {
    position : 'absolute',
    top :      $bt.position().top - $m.outerHeight() - 10,
    left :     $bt.position().left + ( ($bt.outerWidth() - $m.outerWidth()) / 2 ),
    zIndex :   1000,
    width :    width - ( $m.css( 'padding-left' ) * 2 )
  } );
  
   $( document ).bind( 'mousedown', close );
   
  function close() {
    $m.fadeOut( { complete : function() {
       $m.remove();
    } } );
  }
  
  return $m;
}



function set_h_scroll($el, $parent, bt_class, shadow_class) {
  if ( !$parent.length )
    return;
  
  var $p_parent = $parent.parent();
  
  $p_parent.find('.' + bt_class + ', .' + shadow_class).remove();
  $p_parent.css({ height : $parent.outerHeight(), overflow : 'hidden' });
      
  var s_w = ( $el.outerWidth() - $parent.width() );
  
  if ( s_w <= 0 )
    return;
          
  $( '<a/>',   { 'class' : bt_class + ' scroll-left'  } ).appendTo( $p_parent );
  $( '<a/>',   { 'class' : bt_class + ' scroll-right' } ).appendTo( $p_parent );
  $( '<div/>', { 'class' : shadow_class + ' shadow-left'  } ).appendTo( $p_parent );
  $( '<div/>', { 'class' : shadow_class + ' shadow-right' } ).appendTo( $p_parent );
  
  var cur_pos,
      $arrs = $p_parent.find( '.' + bt_class ),
      $arrl = $p_parent.find( '.' + bt_class + '.scroll-left, .'  + shadow_class + '.shadow-left' ),
      $arrr = $p_parent.find( '.' + bt_class + '.scroll-right, .' + shadow_class + '.shadow-right' )
  ;
  
  $parent.unbind('scroll');
  $parent.bind('scroll', function() {
    var sl = $parent.scrollLeft();
    $arrl[ (sl == 0)   ? 'hide' : 'show' ]();
    $arrr[ (sl == s_w) ? 'hide' : 'show' ]();
  }).trigger('scroll');
  
  $arrs.unbind( 'mouseup mousedown' );
  $arrs.bind( 'mousedown', function(e) {
    var diff = $(this).hasClass('scroll-left') ? -75 : 75;
    cur_pos = parseInt( $parent.scrollLeft() ) + diff;
    if ( cur_pos < 0 ) {
      cur_pos = 0;
    }
    if ( cur_pos > s_w ) {
      cur_pos = s_w;
    }
    $parent.stop().animate( { scrollLeft : cur_pos } );
  } );
}


////////////////////////////////////////////////////////////////////////////////
// +/- buttons in number inputs
////////////////////////////////////////////////////////////////////////////////


$( '.input-number-ctl' ).each( function() {
	$( '<a/>', { href : '#', 'class' : 'number-ctl icon-small icon-light icon-minusb', data : { target : this } } ).insertAfter( this );
	$( '<a/>', { href : '#', 'class' : 'number-ctl icon-small icon-light icon-plusb', data : { target : this } } ).insertAfter( this );
} );

$( '.qty-selection' ).delegate( '.number-ctl', 'click', function( e ) {
	e.preventDefault();
	var $t      = $( this ),
	    $target = $( $t.data( 'target' ) ),
			max     = parseInt( $target.attr( 'max' ) ),
			min     = parseInt( $target.attr( 'min' ) ),
			v       = parseInt( $target.val() * 1 ),
			inc     = $t.hasClass( 'icon-minusb' ) ? -1 : 1
	;

	v += inc;

	if ( v > max ) {
		v = max;
	} else if ( v < min ) {
		v = min;
	}

	$target.val( v ).trigger( 'change' );
} );


$.fn.sticky_sidebar = function() {
//  $(this).each(function(){
  var $t = $(this),
      $p = $t.parent(),
      $w = $(window),
      w_h = $w.height(),
      top = $t.parent().offset().top,
      left = $p.offset().left,
      offset_top = 120,
      bottom
  ;

  $p.css({ minHeight : 1 });
  $t.closest('.row').css({ position : 'relative' });

  update();

  $w.bind('scroll.sticky_sidebar resize.sticky_sidebar', function(e) {
    var st = $w.scrollTop();
    var css = {
      left       : 'auto',
      top        : 'auto',
      bottom     : 'auto',
      marginLeft : 'auto'
    };

    if ('resize' === e.type) {
      w_h = $w.height();
      $p.css({ minHeight : w_h - offset_top });
      css.height = w_h - offset_top;
    }

    if ( st + w_h > bottom ) {
      css.bottom   = 0;
      css.position = 'absolute';
    } else if ( st + offset_top > top ) {
      css.left       = '50%';
      css.marginLeft = '-510px';
      css.top        = offset_top;
      css.position   = 'fixed';
    } else {
      css.position = 'relative';
    }
    $t.css(css);
  });
  
  trigger();
//  });
  
  function update() {
    $t.css({ width  : $p.width() + 16 });
    bottom = $p.offset().top + $t.closest('.row').height();
  }
  
  function trigger() {
    $w.trigger('resize.sticky_sidebar');
  }
  
  return {
    update  : update,
    trigger : trigger
  };
};

$.fn.sticky_billboard_title = function(title_selector) {
  var $w          = $(window),
      $els        = $(this),
      refs        = [],
      title_h     = 48,
      offset      = 120,
      fake_offset = 30,
      w_h         = $w.height() - offset,
      col_h,
      $current    = $els.first();

  $els.data('sticky_billboard_title', {});
  
  function update_refs() {
    refs = [];
    
    $els.each(function() {
      var $t     = $(this),
          $title = $t.find(title_selector);
          
      refs.push({
        top    : $t.offset().top,
        bottom : $t.offset().top + $t.outerHeight(),
        $t     : $t,
        $title : $title,
        height : $t.outerHeight()
      });
    });
    
    col_h = $els.closest('.row').height();
  }
      
  function init_styles() {
    $els.each(function() {
      var $t     = $(this),
          $title = $t.find(title_selector);

      $title.css({
        width     : '760px',
        position  : 'absolute',
        zIndex    : 10,
        borderTopWidth : fake_offset,
        top       : -1 * fake_offset,
        height    : title_h + fake_offset
      });
      $title.addClass('sticky-title');
      $t.css({
        paddingTop : title_h,
        position   : 'relative'
      });
    });
  }
  
  init_styles();
  update_refs();
  
  $w.bind('resize.sticky_billboard_title', function() {
    w_h = $w.height() - offset;
  });
  
  $w.bind('scroll.sticky_billboard_title', function() {
    var st = $w.scrollTop() + offset;
    
    $.each(refs, function(i, ref) {
      if ( col_h > w_h && ref.height > w_h ) {
        if (ref.bottom - 200 < st) {
          ref.$title.css({
            position : 'absolute',
            top      : 'auto',
            bottom   : 148
          });
          return;
        } else if (ref.top < st) {
          ref.$title.css({
            position : 'fixed',
            top      : 90,
            bottom   : 'auto'
          });
          $current = ref.$t;
          return;
        }
      }
      ref.$title.css({
        position : 'absolute',
        top      : -30,
        bottom   : 'auto'
      });
    });
  });
  
  function trigger() {
    $w.trigger('scroll.sticky_billboard_title');
  }
  
  trigger();
  
  return {
    update_refs : update_refs,
    trigger : trigger,
    get_current : function() { return $current; },
    append : function($elements) {
      $els = $els.add($elements); init_styles();
    }
  };
};


////////////////////////////////////////////////////////////////////////////////
// Top announces.
////////////////////////////////////////////////////////////////////////////////

( function() {
  
  /**
   * Display announces on the top of the window.
   * 
   * @param {type} args
   * @returns {}
   */
  $.display_announce = function(args) {
    var $top = $('#site-top'),
        $an = $('<div class="top-announce"><div class="wrapper"><div class="ann-msg">' + args.content + '</div><div class="ann-btns"></div></div></div>'),
        namespace = 'da' + (new Date()).getTime(),
        $bth = $an.find('.ann-btns'),
        binded = [];

    // Append and display the announce.
    $top.prepend($an);
    $( 'body' ).animate( { paddingTop : $top.outerHeight() } );
    $an.hide().slideDown({
      complete : function() {
        $( 'body' ).animate( { paddingTop : $top.outerHeight() } );
      }
    });

    // Hide automatically when the page scrolls?
    if ( 'hide_on_scroll' in args && true === args.hide_on_scroll ) {
      $(window).bind('scroll.' + namespace, function() {
        if ( $(document).scrollTop() == 0 )
          return;
        $.close_announce($an);
        $(window).unbind('scroll.' + namespace);
      });
      binded.push($(window));
    }

    // Add buttons.
    if ( 'actions' in args && args.actions.length ) {
      $.each(args.actions, function(i, action) {
        var $bt = $('<a/>', {
          'class' : 'btn btn-small btn-midgrey',
          href    : '#',
          text    : action.caption
        });

        if ( 'href' in action ) {
          $bt.attr({ href : action.href });
        }

        if ( 'class' in action ) {
          $bt.addClass(action['class']);
        }

        if ( 'events' in action && action.events.length ) {
          $.each(action.events, function(i, event) {
            $bt.bind(event.event + '.' + namespace, event.callback);
          });
        }

        $bt.appendTo($bth);
        binded.push($bt);
      });

      // Add close button.
      if ( !('can_close' in args) || false !== args.can_close ) {
        var $close = $('<a/>', {
            'class' : 'ann-close',
            href    : '#',
            text    : 'Cerrar',
            title   : 'Cerrar'
          }).appendTo($an.find('.wrapper'));
          
        $close.bind('click.' + namespace, function(e) {
          e.preventDefault();
          $.close_announce($an);
        });
        binded.push($close);
      }
    }
    
    $an.data({
      binded    : binded,
      namespace : namespace
    });

    return $an;
  };


  /**
   * Hide the announce, unbind events and remove from DOM.
   * 
   * @param {type} $an
   * @returns {undefined}
   */
  $.close_announce = function($an) {
    $an.slideUp({
      complete : function() {
        // We need to unbind the event handlers before removing the elements
        // in order to actually free memory up.
        var binded    = $an.data('binded'),
            namespace = $an.data('namespace');
    
        $.each(binded, function(i, $el) {
          $el.unbind('.' + namespace);
        });
        
        // Now we can remove the element.
        $an.remove();
        $('body').animate({ paddingTop : $('#site-top').outerHeight() });
      }
    });
  }

} )();


/**
 * Check if the property {v} exists on the element {el}.
 * If no element provided, window is used instead.
 * 
 * @param {string} v
 * @param {object} el
 * @returns {Boolean}
 */
function isset(v, el) {
  if ( undefined === el ) {
    el = window;
  }

  if ( false === el ) {
    return false;
  }
  
  var _v = v.split('.');
  v = _v.shift();
  
  var _isset = v in el && el[v] !== null && el[v] !== undefined;
  if ( _isset && _v.length ) {
    return isset(_v.join('.'), el[v]);
  } else {
    return _isset;
  }
}


( function() {
  function ac_evt_keyup(args) {
    var q = args.$el.val();
    
    if ( q.length < args.min_length ) {
      args.$div.hide();
      return;
    }
    
    if ( 'last_q' in args && args.last_q == q )
      return;
    
    args.last_q = q;
    
    var results = args.query(q);
    ac_render_list(results, args);
  };
  
  function ac_evt_focus(args) {
    clearTimeout(args.timers.blur);
    args.timers.blur = null;
    if ( args.$div.children('a').length ) {
      args.$div.show();
    }
  }
  
  function ac_evt_blur(args) {
    args.timers.blur = setTimeout(function() {
      args.$div.hide();
    }, 500);
  }
  
  function ac_evt_keydown(e, args) {
    var $a = args.$div.children('a.hover');
    if ( !$a.length ) {
      return;
    }
    switch (e.which) {
      // ENTER
      case 13:
        ac_evt_click($a, args);
        break;
        
      // Down arrow
      case 40:
        var $next = $a.next();
        if ( !$next.length ) {
          $next = args.$div.children('a:first');
        }
        if ( !$next.length ) {
          return;
        }
        $a.removeClass('hover');
        $next.addClass('hover');
        ac_scroll($next, args);
        break;
        
      // Up arrow
      case 38:
        var $next = $a.prev();
        if ( !$next.length ) {
          $next = args.$div.children('a:last');
        }
        if ( !$next.length ) {
          return;
        }
        $a.removeClass('hover');
        $next.addClass('hover');
        ac_scroll($next, args);
        break;
    }
  }
  
  function ac_scroll($el, args) {
    if ($el.length && ($el.position().top + $el.outerHeight() > args.$div.height() || $el.position().top < 0)) {
      var off = (($el.position().top - args.$div.height()) + $el.outerHeight());
      args.$div.scrollTop(args.$div.scrollTop() + off);
    }
  }
  
  function ac_evt_click($el, args) {
    args.$el.val($el.text());
    args.callback($el);
    args.$div.hide();
  }
  
  function ac_find_in_array(q, data) {
    var re      = new RegExp(q, 'i'),
        results = [];

    for ( i in data ) {
      var row = data[i];
      if ( re.test(row.q) ) {
        results.push(row);
      }
    }
    
    return results;
  };
  
  function ac_render_list(options, args) {
    var $div = args.$div;
    
    $div.children().remove();
    $div.show();
    
    if ( !options || !options.length ) {
      $div.append('<p class="ac-no-results">No se encontraron resultados</p>');
      return;
    }
    
    for ( var i in options ) {
      var opt = options[i];
      $('<a/>', {
        text : opt.q,
        data : {
          value : opt.v
        }
      }).appendTo($div);
    }
    $div.children().first().addClass('hover');
  }
  
  $.fn.autocomplete = function(args) {
    args.$el        = $(this);
    args.$div       = $('<div/>');
    args.min_length = 3;
    args.timers     = {};
    
    args.$div
      .insertAfter(args.$el)
      .delegate('a', 'click', function(e) {
        ac_evt_click($(this), args);
      })
      .addClass(args.div_class)
    ;
    
    if ( 'autoposition' in args && args.autoposition === true ) {
      var $holder = $('<div/>');
      $holder.insertBefore(args.$el);
      $holder.css({ position : 'relative', zIndex : 20 });
      args.$el.appendTo($holder);
      args.$div.appendTo($holder);
      args.$div.css({
        width  : args.$el.outerWidth(),
        top    : args.$el.outerHeight() + args.$el.position().top,
        left   : args.$el.position().left,
        position : 'absolute'
      });
    }
    
    if ( 'data' in args ) {
      args.query = function(q) {
        return ac_find_in_array(q, args.data);
      };
    } else if ( 'data_source' in args ) {
      args.query = function(q) {
        args.data = args.data_source(q);
        return ac_find_in_array(q, args.data);
      };
    } else {
      throw new Error('Data source not specified.');
    }
    
    $(this)
      .unbind('.autocomplete')
      .bind('keyup.autocomplete', function() {
        ac_evt_keyup(args);
      })
      .bind('blur.autocomplete', function() {
        ac_evt_blur(args);
      })
      .bind('focus.autocomplete', function() {
        ac_evt_focus(args);
      })
      .bind('keydown.autocomplete', function(e) {
        ac_evt_keydown(e, args);
      })
    ;
    
    args.$div.delegate('a', 'mouseenter', function() {
      $(this).addClass('hover').siblings().removeClass('hover');
    });
  }
} )();


var Is = {
  Home: function() {
    return (isset('is_home', window) && window['is_home']);
  },
  iPad: function() {
    return (navigator.userAgent.match(/iPad/i) != null);
  },
  iOS: function() {
    return !this.WindowsPhone() && (navigator.userAgent.match(/iPhone|iPad|iPod/i) != null);
  },
  Android: function() {
    return !this.WindowsPhone() && (navigator.userAgent.match(/\bandroid\b/i) != null);
  },
  WindowsPhone: function() {
    return (navigator.userAgent.match(/\bWindows Phone\b/i) != null);
  }
};



/* Input prefix */
$.fn.inputPrefix = function() {
  var $t = $(this);
  $t
      .off('.inputPrefix')
      .on('keydown.inputPrefix', function(e) {
        var $self = $(this);
        var prefix = $self.data('prefix');
        $self.data('inter', setInterval(function() {
          if (!$self.val().match(new RegExp('^' + prefix))) {
            e.preventDefault();
            $self.val(prefix);
          }
        }, 50));
      })
      .on('keyup.inputPrefix', function(e) {
        clearInterval($(this).data('inter'));
      });

  $t.each(function() {
    $(this).val($(this).data('prefix'));
  });
};


/* Allow integers only */
$.fn.integersOnly = function() {
  $(this).on('keydown', function (e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
          // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
          // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
          // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
          // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });
};