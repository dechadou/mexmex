<?php

namespace SocialSnack\FrontBundle\Tests\Functional\Controller;

use SocialSnack\FrontBundle\Tests\Controller\WebTestCase;

class IeControllerTest extends WebTestCase {

  /**
   * @group functional
   * @group smoke
   */
  public function testBenefitsAction() {
    $client  = static::createClient();
    $crawler = $client->request(
        'GET',
        '/invitado-especial/beneficios'
    );

    $this->assertSame(200, $client->getResponse()->getStatusCode());
    $this->assertContains('Beneficios', $crawler->filterXPath('//title')->text());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testFaqAction() {
    $client  = static::createClient();
    $crawler = $client->request(
        'GET',
        '/invitado-especial/preguntas-frecuentes'
    );

    $this->assertSame(200, $client->getResponse()->getStatusCode());
    $this->assertContains('Preguntas Frecuentes', $crawler->filterXPath('//title')->text());
  }

}