<?php

namespace SocialSnack\AdminBundle\Tests\Controller;

use SocialSnack\AdminBundle\Entity\AdminRole;
use SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class WebTestCase
 * @package SocialSnack\AdminBundle\Tests\Controller
 * @author Guido Kritz
 */
class WebTestCase extends \Liip\FunctionalTestBundle\Test\WebTestCase {


  protected function getLoggedInClient(array $roles = [], array $fixtures = NULL) {
    $client = static::createClient();
    $session = $client->getContainer()->get('session');

    $fixtures = NULL !== $fixtures ? $fixtures : array(
        '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
    );

    $this->loadFixtures($fixtures);
    $user = LoadAdminData::$admin;

    foreach ($roles as $role_name) {
      $role = new AdminRole();
      $role
          ->setName($role_name)
          ->setRole($role_name)
      ;
      $user->addRole($role);
    }

    $firewall = 'admin';
    $token = new UsernamePasswordToken( $user, $user->getPassword(), $firewall, $user->getRoles());
    $session->set('_security_'.$firewall, serialize($token));
    $session->save();

    $cookie = new Cookie($session->getName(), $session->getId());
    $client->getCookieJar()->set($cookie);

    return $client;
  }

} 