/////////////////////////////
// Facebook related functions
/////////////////////////////

var loggin = false;
var $process = $( '#register-process' );

/**
 * Trigger a callback function which requires a logged in Facebook account.
 */
function fb_logged( args ) {
	var success =  args.success ? args.success : function() {};
	var success_args = args.success_args ? args.success_args : [];
	var failure =  args.failure ? args.failure : function() {};
	var failure_args = args.failure_args ? args.failure_args : [];
	var scope = args.scope ? args.scope : '';

//	FB.getLoginStatus( function( res ) {
//		if ( 'connected' === res.status ) {
//			success_args.unshift( res );
//			success.apply( window, success_args );
//		} else {
			FB.login( function( res ) {
				if ( 'connected' === res.status ) {
					success_args.unshift( res );
					success.apply( window, success_args );
				} else {
					failure.apply( window, failure_args );
				}
			}, {'scope' : scope} );
//		}
//	} );
}

/**
 * Retrieve user info from Facebook.
 * 
 * @param callback
 */
function get_fb_info( callback ) {
	FB.api( '/me', function( res ) {
		if ( !res.id ) {
			$.msg_popup( locale.facebook.connect_error, 'alert' );
			return;
		}
		
		if ( callback )
			callback( res );
	} );
}

///////////////////////////////////
// En of Facebook related functions
///////////////////////////////////


/////////////////////////
// User related functions
/////////////////////////

/**
 * Check if the user's Facebook account is registered in the app.
 * 
 * @param user_id
 * @param callback
 */
function check_fb_user( user_id, callback ) {
	function success( res ) {
		callback( null, !!res.registered );
	}
	function error() {
		callback( { msg : 'Ocurrió un error. Por favor, inténtalo nuevamente.' }, null );
	}
	
	$.ajax( {
			url				:	rest_url + 'checkFb'
		,	type			:	'post'
		,	dataType	:	'json'
		,	data			:	{ fbuid : user_id }
		,	success		:	success
		,	error			:	error
	} );
}


/**
 * Get friends from Facebook.
 */
function get_fb_friends( cb ) {
	var friends = [];
	
	function done( err, data ) {
		if ( cb )
			cb( err, data );
		$evts.trigger( 'get_fb_friends', err, data );
	}
	
	function get( path ) {
		FB.api( '/me/' + path, function( res ) {
			if ( !res.data )
				return done( { msg : locale.global.generic_error }, null );

			friends = friends.concat( res.data );
			if ( res.paging.next )
				get( res.paging.next.split( '/' ).pop() );
			else {
				friends.sort( sort_friends );
				done( null, friends );
			}
		} );
	}
	
	get( 'friends' );
}

/**
 * Sort friends by name.
 */
function sort_friends( a, b ) {
	if ( a.name > b.name )
		return 1;
	else if ( a.name < b.name )
		return -1;
	else
		return 0;
}
////////////////////////////////
// End of User related functions
////////////////////////////////


function fb_register_click( e ) {
  e.preventDefault();
  
  if ( loggin )
    return;
  
  var $bt = $( this );
  set_logging( true, $bt );
  
  fb_logged( {
    success : function( res ) {
      check_fb_user( res.authResponse.userID, function( err, registered ) {
        if ( err ) {
          set_logging( false, $bt );
          $.msg_popup( err.msg, 'msg-error', true );
          return;
        }

        if ( !registered ) {
          get_fb_info( function( info ) {
            // Prefill form.
            if ( info.first_name ) {
              $( '#register_first_name' ).val( info.first_name );
            }
            if ( info.last_name ) {
              $( '#register_last_name' ).val( info.last_name );
            }
            if ( info.email ) {
              $( '#register_email' ).val( info.email );
            }
            $( '#register_fbsigned_request' ).val( res.authResponse.signedRequest );
            
            // Go to next step.
            $( '#register-step-1' ).slideUp();
            $( '#register-step-2' ).slideDown();
            set_logging( false, $bt );
          } );
        } else {
          // Error, the user is already registered.
          $.msg_popup( 'Ya existe una cuenta de Cinemex asociada a tu cuenta de Facebook.', 'msg-error', true );
          set_logging( false, $bt );
        }
      } );
    },
    failure	: function() {
      set_logging( false, $bt );

      $.msg_popup( 'Ocurrió un error al conectar con tu cuenta de Facebook. Por favor, inténtalo nuevamente.', 'msg-error', true );
    },
    scope : 'email'
  } );
}

$( '#fb-register-bt' ).bind( 'click', fb_register_click );
$( '[rel=fb-register-skip]' ).bind( 'click', function( e ) {
  e.preventDefault();
  $( '#register-step-1' ).slideUp();
  $( '#register-step-2' ).slideDown();
} );

function set_logging( status, $bt ) {
  loggin = status;
  $process[ status ? 'blockui' : 'unblockui' ]();
  $bt[ status ? 'addClass' : 'removeClass' ]( 'btn-loading' );
}


$( '#register-form' ).bind( 'submit', function( e ) {
  e.preventDefault();
  var $form = $( this ),
      $bt   = $form.find( 'button[type=submit]' );
      
  if ( !$( '#register_first_name' ).run_validators() ) {
    $.msg_popup( 'Por favor, ingresa tu nombre.', 'msg-error', true );
    return;
  }
  if ( !$( '#register_last_name' ).run_validators() ) {
    $.msg_popup( 'Por favor, ingresa tu apellido.', 'msg-error', true );
    return;
  }
  if ( !$( '#register_email' ).run_validators() ) {
    $.msg_popup( 'Por favor, ingresa un email válido.', 'msg-error', true );
    return;
  }
  if ( !$( '#register_password' ).run_validators() ) {
    $.msg_popup( 'Por favor, ingresa una contraseña válida.', 'msg-error', true );
    return;
  }
  if ( !$( '#register_terms:checked' ).length ) {
    $.msg_popup( 'Debes aceptar los términos y condiciones para poder continuar', 'msg-error', true );
    return;
  }
  
  var data = $form.serialize();
  if ( $('#privacy-check:checked').length ) {
    data += '&privacy_check=1';
  }
  
  set_logging( true, $bt );
  
  $.ajax( {
    url       : rest_url + 'register',
    type      : 'post',
    dataType  : 'json',
    data      : data,
    success   : register_success,
    error     : function( res ) { register_error( res.responseJSON ); }
  } );


  function register_success( res ) {
		parse_login_res( res );
    set_logging( false, $bt );
    $( '#register-step-2' ).slideUp();
    $( '#register-step-3' ).slideDown();
  }


  function register_error( res ) {
    set_logging( false, $bt );

    var msg = 'Ocurrió un error. Por favor, inténtalo nuevamente.';
    
    if ( res ) {
      switch ( res.errorcode ) {
        case 'invalid-signed-request':
          msg = 'Ocurrió un error al conectar con tu cuenta de Facebook. Por favor, inténtalo nuevamente.';
          break;
        case 'duplicated-email':
          msg = 'Ya existe una cuenta registrada con el email ingresado.'
          break;
        case 'duplicated-fbuid':
          msg = 'Ya existe una cuenta registrada con tu cuenta de Facebook.'
          break;
        default:
          msg = res.msg || msg;
          break;
      }
    }

    $.msg_popup( msg, 'msg-error', true );
  }
} );

( function() {
  var $form   = $( '#register-ie-form, #link-ie-form' ),
      $input  = $( '#register_iecode' ),
      $bt     = $form.find( 'button[type=submit]' )
  ;

  $input.inputPrefix();
  
  function after_success() {
    if ( 'register-ie-form' == $form.attr( 'id' ) ) {
      document.location = $( '#register-end-bt' ).attr( 'href' );
    } else {
      document.location.reload();
    }
  }
  
  $form.bind( 'submit', function( e ) {
    e.preventDefault();
    
    if ( loggin )
      return;

    if ( !$input.run_validators() ) {
      $.msg_popup( 'Por favor, ingresa tu número de invitado especial.', 'msg-error', true );
      return;
    }

    set_logging( true, $bt );
    
    $.ajax( {
      url      : rest_url + 'me/iecode',
      type     : 'post',
      dataType : 'json',
      data     : { iecode : $.trim($input.val()) },
      success  : function( res ) {
        setCookie('fetch_cache', 1);
        if ( res.iecode ) {
          if ( user.info ) {
            var _info = user.info;
            _info.iecode = res.iecode;
            update_user( { info : _info }, function() {
              return after_success();
            } );
            return;
          } else {
            return after_success();
          }
        }
        return after_success();
      },
      error    : function( res ) {
        set_logging( false, $bt );
        var msg = 'Ocurrió un error. Por favor, inténtalo nuevamente.';
        if ( ( res = res.responseJSON ) && res.errorcode ) {
          switch ( res.errorcode ) {
            case '-10111':
              msg = 'El número de Invitado Especial ingresado no es válido.';
              break;
          }
        }
        $.msg_popup( msg, 'msg-error', true );
      }
    } );
  } );
  
  $( '#register-end-bt' ).bind( 'click', function( e ) {
    if ( $input.val() ) {
      e.preventDefault();
      $form.trigger( 'submit' );
    }
  } );
} )();


( function() {
  var working = false,
      $bt     = $( '#fb-login-bt' );

  $bt.bind( 'click', function( e ) {
    e.preventDefault();

    if ( working )
      return;
    
    set_working( true );

    fb_logged( {
      success : function( res ) {
        $.ajax( {
          url      : rest_url + 'login',
          type     : 'post',
          dataType : 'json',
          data     : { fbsigned_request : res.authResponse.signedRequest, from_frontend : true },
          success  : fb_login_success,
          error    : function( res ) { fb_login_error( res.responseJSON ); }
        } );
      },
      failure : function() {
        $.msg_popup( 'Ocurrió un error al conectar con tu cuenta de Facebook. Por favor, inténtalo nuevamente.', 'msg-error', true );  
      },
      scope : 'email'
    } );
  } );
  
  function set_working( status ) {
    working = status;
    $bt[ status ? 'addClass' : 'removeClass' ]( 'btn-loading' );
    if ( status ) {
      $( '#login-popup' ).blockui( 'no-bg' );
    } else {
      $( '#login-popup' ).unblockui();
    }
  }
  
  function fb_login_success( res ) {
    parse_login_res(res, function() {
      if ( window['redirect_after_login'] ) {
        var redirect_url = $('[name=_target_path]').val() || url_prefix;
        document.location = redirect_url;
        return;
      }
      $.close_popups();
    } );
  }
  
  function fb_login_error( res ) {
    set_working( false );
    
    var msg = 'Ocurrió un error. Por favor, inténtalo nuevamente.';
    
    if ( res ) {
      switch ( res.errorcode ) {
        case 'invalid-credentials':
          msg = 'No hay una cuenta asociada a tu cuenta de Facebook.'
          break;
        default:
          msg = res.msg || msg;
          break;
      }
    }
    
    $.msg_popup( msg, 'msg-error', true );
  }
} )();