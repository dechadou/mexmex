<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;

class StatesCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:states' )
			->setDescription( 'Fetch states from the Web Service' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		/* @var $req \SocialSnack\WsBundle\Request\Request */
		$doctrine    = $this->getContainer()->get( 'doctrine' );
		$em          = $doctrine->getManager();
		$req         = $this->getContainer()->get( 'cinemex_ws' );
		$state_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
		$area_repo   = $doctrine->getRepository( 'SocialSnackWsBundle:StateArea' );
		$states      = array();
		$areas       = array();
		
		$req->init( 'Consulta' );
		$res = $req->request( array(
			'TipoConsulta' => \SocialSnack\WsBundle\Request\Consulta::TIPOCONSULTA_ESTADOS_ZONAS
		) );
		
		foreach ( $res as $row ) {
			if ( isset( $states[ (int)$row->CODSTATE ] ) ) {
				$state = $states[ (int)$row->CODSTATE ];
			} else {
				$state = $state_repo->findOneBy( array( 'legacy_id' => $row->CODSTATE ) );
				if ( !$state ) {
					$state = new State();
					$em->persist( $state );
				}
				$states[ (int)$row->CODSTATE ] = &$state;
			}
			$state->setLegacyID( (int)$row->CODSTATE );
			$state->setLegacyName( (string)$row->STATENAME );
			if ( !$state->getId() ) { // No ID = new record = INSERT.
				$state->setName( ucwords( strtolower( (string)$row->STATENAME ) ) );
			}
			
			/* @todo Notify updates */
			
			$area = $area_repo->findOneBy( array( 'legacy_id' => $row->CODZONE, 'legacy_state_id' => $row->CODSTATE ) );
			if ( !$area ) {
				$area = new StateArea();
				$em->persist( $area );
			}
			$area->setLegacyID( (int)$row->CODZONE );
			$area->setLegacyStateID( (int)$row->CODSTATE );
			$area->setLegacyName( (string)$row->ZONENAME );
			$area->setState( $state );
			if ( !$area->getId() ) { // No ID = new record = INSERT.
				$area->setName( ucwords( strtolower( (string)$row->ZONENAME ) ) );
			}
		}
		
		$em->flush();
		
		$output->writeln( 'Done.' );
		return;
	}
	
}