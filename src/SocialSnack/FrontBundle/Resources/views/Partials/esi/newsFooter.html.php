<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div class="bottom-box col-sm-3-5 col-md-3-6 col">
  <span class="bottom-title">
    <span class="discicon icon-star icon-red"></span>
    Novedades
    <div class="bottom-box-top">
      <a href="<?php echo $view['router']->generate( 'news_archive' ); ?>">Ver todas &rarr;</a>
    </div>
  </span>

  <div class="row">
    <div class="col col-sm-1-2">
      <article class="bottom-box-article news-grid-item news-grid-detrasdelamagia">
        <a href="<?php echo $view['router']->generate('landing/detrasdelamagia'); ?>">
          <img src="<?php echo $view['assets']->getUrl('assets/img/banner-detrasdelamagia.jpg'); ?>" alt="Checa las entrevistas de Megan Fox, Daniel Radcliffe y más actores..." />
        </a>

        <h3><a href="<?php echo $view['router']->generate('landing/detrasdelamagia'); ?>">Checa las entrevistas de Megan Fox, Daniel Radcliffe y más actores...</a></h3>
      </article>
    </div>

    <div class="col col-sm-1-2">
      <?php foreach ( $articles as $_article ) { ?>
      <?php $link = $view['router']->generate( 'news_single', array('id' => $_article->getId(), 'slug' => FrontHelper::sanitize_for_url($_article->getTitle())) ); ?>
      <article class="bottom-box-article news-grid-item">
        <a href="<?php echo $link; ?>"><img src="<?php echo $view['fronthelper']->get_img_size($_article->getThumb(), '236x222', 'CMS'); ?>" alt="<?php echo $_article->getTitle(); ?>" /></a>
        <h3><a href="<?php echo $link; ?>"><?php echo FrontHelper::shorten($_article->getTitle(), 50 ); ?></a></h3>
      </article>
      <?php } ?>
    </div>

  </div>
</div>
