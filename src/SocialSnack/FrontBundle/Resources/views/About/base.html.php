<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Nosotros - Cinemex');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-header.jpg'); ?>">
</div>

<div class="section-nav">
  <ul>
    <li><a href="<?php echo $view['router']->generate('about_index'); ?>" <?php if ($current_page === 'index') : ?>class="selected"<?php endif; ?>>CINEMEX</a></li>
    <li><a href="<?php echo $view['router']->generate('about_concesiones'); ?>" <?php if ($current_page === 'concesiones') : ?>class="selected"<?php endif; ?>>Concesiones</a></li>
    <li><a href="<?php echo $view['router']->generate('about_conciencia'); ?>" <?php if ($current_page === 'conciencia') : ?>class="selected"<?php endif; ?>>Conciencia CINEMEX</a></li>
  </ul>
</div>

<div class="section-content">
<?php $view['slots']->output('section_body'); ?>
</div>
<?php $view['slots']->stop(); ?>
