<div class="form-group <?php if (isset($attr['row_class'])) echo $attr['row_class']; ?>">
  <?php echo $view['form']->label($form) ?>
  <div class="col-sm-10">
    <?php echo $view['form']->errors($form) ?>
    <?php echo $view['form']->widget($form) ?>
    <?php foreach ($attr as $attrname => $attrvalue) { ?>
      <?php if ($attrname === 'help') { ?>
        <span class="help-block"><?php echo $attrvalue; ?></span>
      <?php } ?>
    <?php } ?>
  </div>
</div>