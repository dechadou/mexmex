<?php

namespace SocialSnack\FrontBundle\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface, LogoutSuccessHandlerInterface {

	private $container;
	private $router;
	private $templating;
	private $front_utils;

	public function __construct( $container, Router $router, $templating, $front_utils ) {
		$this->container   = $container;
		$this->router      = $router;
		$this->templating  = $templating;
		$this->front_utils = $front_utils;
		$this->doctrine    = $this->container->get('doctrine');;
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
    $user = $token->getUser();
    $em   = $this->doctrine->getManager();
    $user->setLoginDate();
    $em->persist($user);
    $em->flush();
    
		if ($request->isXmlHttpRequest()) {
			// Handle XHR here
      
      $res  = $this->front_utils->get_user_login_data( $user );
      
			$response = new JsonResponse( array( 'success' => TRUE, 'data' => $res ) );
      $response->headers->setCookie( new \Symfony\Component\HttpFoundation\Cookie( 'fetch_cache', '0', 0, '/', null, false, false ) );
      
      $this->container->get('graphite')->publish('user.login.success.ajax', 1);
      
      return $response;
		} else {
			// If the user tried to access a protected resource and was forces to login
			// redirect him back to that resource
      if ( $targetPath = $request->get('_target_path') ) {
				$url = $targetPath;
      } elseif ( $targetPath = $request->getSession()->get( '_security.target_path' ) ) {
				$url = $targetPath;
      } elseif ( FrontHelper::is_mobile($request->headers->get("referer")) ) {
        $url = $this->router->generate('mobile_index');
			} else {
				$url = $this->router->generate('home');
			}
      
      $response = new RedirectResponse( $url );
      $response->headers->setCookie( new \Symfony\Component\HttpFoundation\Cookie( 'fetch_cache', '1', 0, '/', null, false, false ) );
      
      $this->container->get('graphite')->publish('user.login.success.post', 1);
      
			return $response;
		}
	}

	public function onAuthenticationFailure(Request $request, AuthenticationException $e) {
    $this->container->get('graphite')->publish('user.login.fail.wronglogin', 1);
		if ($request->isXmlHttpRequest()) {
			// Handle XHR here
			return new JsonResponse( array( 'success' => FALSE, 'error' => $e->getMessage() ) );
		} else {
			// Create a flash message with the authentication error message
//      $request->getSession()->getFlashBag()->add( 'error', $e->getCode() );
//      $request->getSession()->getFlashBag()->add( 'error', $e->getMessage() );
      $request->getSession()->getFlashBag()->add( 'error', 'Usuario o contraseña inválidos.' );
      
      if ( FrontHelper::is_mobile($request->headers->get("referer")) ) {
        $url = $this->router->generate('mobile_login');
			} else {
        $url = $this->router->generate('user_login');
      }
      
			return new RedirectResponse($url);
		}
	}
  
  public function onLogoutSuccess(Request $request) {
    $request->getSession()->invalidate();

    $targetPath = $request->get('redirect_to');
    if ( !$targetPath ) {
      $targetPath = $request->headers->get('referer');
    }
    if ( !$targetPath ) {
      $targetPath = '/';
    }
    return new RedirectResponse($targetPath);
  }

}