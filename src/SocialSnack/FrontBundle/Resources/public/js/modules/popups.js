jQuery.fn.blockui = function( classes ) {
	$( this ).each( function() {
		var $t     = $( this ),
		    $block = $( '<div/>', { 'class' : 'blockui' } );
		$t.css( { position : 'relative' } );
		if ( classes ) {
			$block.addClass( classes );
		}
		$t.append( $block );
	} );
};

jQuery.fn.unblockui = function( classes ) {
	$( this ).find( '.blockui' ).remove();
};

/////////////////
// Begin Popups 
///////////////

jQuery.close_popups = function() {
  if ( !$( '#popups .popup' ).not( '.hidden' ).length )
    return;
  
	$( '#popups' ).stop().animate( { opacity : 0 }, { complete : function() {
		$( this ).css( { display : 'none' } );
		$( this ).find( '.popup' ).addClass( 'hidden' );
		var st = parseInt( $( '#overflow-ctl' ).css( 'top' ) );
		$( '#overflow-ctl' ).removeClass( 'control' ).css( { top : 'auto' } );
		$( 'body, html' ).scrollTop( st * -1 );
	} } );
}

jQuery.fn.popup = function() {
	if ( !$( '#overflow-ctl' ).hasClass( 'control' ) ) {
		var st = $( document ).scrollTop();
		$( '#overflow-ctl' ).addClass( 'control' ).css( { top : st * -1 } );
	}
  $( 'body, html' ).scrollTop(0);
	$( '#popups .popup' ).not( '.hidden' ).addClass( 'hidden' );
	$( this ).removeClass( 'hidden' );
	$( '#popups' ).css( { display : 'table' } ).stop().animate( { opacity : 1 } );
}

jQuery.fn.close_popup = function() {
	if ( $( '#popups .popup' ).not( '.hidden' ).length <= 1 ) {
		$.close_popups();
	} else {
		$( this ).addClass( 'hidden' );
	}
}

jQuery.msg_popup = function( msg, classes, easy_close ) {
  if ( $msg_popup = $( '#msg-popup' ) ) {
    $msg_popup.remove();
  }
  
  $msg_popup = $( '<div/>', { 'class' : 'popup ' + classes, id : 'msg-popup' } );
  $( '<div/>', { html : msg, 'class' : 'msg ' + classes } ).appendTo( $msg_popup );
  $( '<a/>', { 'class' : 'popup-close icon-small icon-close no-txt', text : 'cerrar' } ).appendTo( $msg_popup );
  $msg_popup.appendTo( '#popups-center' ).popup();
  
  if ( easy_close ) {
    $( '#popups' ).css( { cursor : 'pointer' } ).bind( 'click', popup_easy_close );
  }
}

function popup_easy_close( e ) {
  e.preventDefault();
  $( this ).unbind( 'click', popup_easy_close ).css( { cursor : 'auto' } );
  $.close_popups();
}

$( '#popups' ).delegate( '.popup-close, [rel="popup-close"]', 'click', function( e ) {
	e.preventDefault();
	$( this ).closest( '.popup' ).close_popup();
} );

$( '[rel="popup-trigger"]' ).bind( 'click', function( e ) {
	e.preventDefault();
	$( $( this ).data( 'popup' ) ).popup();
} );

jQuery( document ).ready( function( $ ) {
  var $popup = $( '.auto-popup' );
  if ( $popup.length ) {
    $popup.popup();
  }
} );
/////////////
// End Popups 
/////////////