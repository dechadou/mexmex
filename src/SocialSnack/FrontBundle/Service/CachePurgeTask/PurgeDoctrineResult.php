<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

class PurgeDoctrineResult implements DoctrineTaskInterface {

  protected $resources;

  public function addDoctrineResource($ids, $env) {
    $this->resources[] = ['ids' => $ids, 'env' => $env];
  }

  public function getDoctrineResources() {
    return $this->resources;
  }

}