<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Borrar cache por url' );
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Borrar cache por url
        </header>

        <div class="panel-body">
          <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
          <?php echo $view['form']->form($form); ?>
        </div>
      </section>
    </div>
  </div>
<?php $view['slots']->stop(); ?>