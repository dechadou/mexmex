<?php
namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class MovieUpdater extends WorkerAbstract {

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
	protected $container;

	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;

  /**
   * @var \SocialSnack\WsBundle\Request\Request
   */
	protected $cinemex_ws;
  
  /**
   * @var integer
   */
  protected $new_movies = 0;


  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
   * @param \SocialSnack\WsBundle\Request\Request $cinemex_ws
   */
	public function __construct($container, $doctrine, $cinemex_ws) {
		$this->container  = $container;
		$this->doctrine   = $doctrine;
		$this->cinemex_ws = $cinemex_ws;
	}


  public function execute($data) {
		/* @var $movie  \SocialSnack\WsBundle\Entity\Movie */
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */

    $cinema_id = $data['cinema_id'];

		try {
			$req          = $this->cinemex_ws;
			$doctrine     = $this->doctrine;
      $em           = $doctrine->getManager();
      $movie_repo   = $doctrine->getRepository( 'SocialSnackWsBundle:Movie' );
      $cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
      $movies       = array();
      $new_movies   = 0;
			$cinema        = $cinema_repo->find($cinema_id);

      if ( !$cinema )
        return TRUE;

      $this->consoleInfo(sprintf( 'Cinema: %s (%s)', $cinema->getName(), $cinema->getLegacyId() ));

      try {
        $req->init( 'Consulta' );
        $res = $req->request( array(
            'TipoConsulta' => Consulta::TIPOCONSULTA_PELICULAS,
            'CodigoCine'   => $cinema->getLegacyId(),
            'CodigoEstado' => '',
            'CodigoZona'   => '',
        ) );
        $this->consoleInfo(sprintf( 'Movies: %s', sizeof( $res ) ));
      } catch ( RequestMsgException $e ) {
        $this->setError($e->getMessage());
        $this->consoleInfo($e->getMessage());
        return FALSE;
      }

      // Fetch movies from DB.
      $fetch_m_ids = array();
      foreach ( $res as $_movie ) {
        $fetch_m_ids[] = (int)$_movie->FILMCODE;
      }
      $qb = $doctrine->getManager()->createQueryBuilder();
      $qb->select('m')
          ->from('SocialSnackWsBundle:Movie', 'm')
          ->andWhere( $qb->expr()->in('m.legacy_id', $fetch_m_ids));
      $movies_result = $qb->getQuery()->execute();


      foreach ( $res as $_movie ) {

        // The WS returns the same movie repeated for each cinema playing it, so
        // we need to skip duplicates.
        if ( in_array( $_movie->FILMCODE, $movies ) )
          continue;

        // Does the movie already exist on the DB?
        $movie = WsHelper::get_from_result_by_legacy_id($movies_result, $_movie->FILMCODE);
        if ( $movie )
          continue;

        // Get detailed info.
        try {
          $req->init( 'ConsultaInfoPelicula' );
          $movie_info = $req->request( array(
              'Opcion'         => 12,
              'CodigoPelicula' => (string)$_movie->FILMCODE,
          ) );
        } catch ( RequestMsgException $e ) {
          $this->setError($e->getMessage());
          $this->consoleInfo($e->getMessage());
          return FALSE;
        }

        if ( !$movie ) {
          $movie = new Movie();
          $em->persist( $movie );
//          echo $_movie->MOVIENAME . "\r\n";
          $new_movies++;
          $attributes = new \stdClass();
        } else {
          $attributes = $movie->getAttr();
        }

        list( $title, $attrs ) = WsHelper::parse_attrs_and_clean_title( $movie_info->MOVIENAME );

        $movie->setLegacyId( (int)$_movie->FILMCODE );
        $movie->setLegacyIdBis( (string)$_movie->MOVIE ); // Dunno why it has two different sort of IDs
        $movie->setLegacyName( (string)$movie_info->MOVIENAME );
        $movie->setInfo( json_encode( $movie_info ) );
        $movie->setPremiere( (bool)(string)$movie_info->ESTRENO );
        if($movie_info->ESTRENO){
          $movie->setFixedPosition(500);
        }
        $movie->setGroupId( (int)$movie_info->CLAVEAGRUPACION );
        $movie->setActive( 0 );

        if ( !$movie->getId() ) { // No ID = new record = INSERT.
          $movie->setName( ucwords( strtolower( $title ) ) );
          $movie->setShortName( ucwords( strtolower( $movie_info->MOVIESHORTNAME ) ) );
          $movie->setPosterUrl( md5( gethostname() . time() . $_movie->FILMCODE ) . '.jpg' );
          $this->container->get('ss.ws.utils')->process_posters( $movie, $movie_info, (!isset($data['enqueue']) || TRUE === $data['enqueue']) );
        } else {
          /* @todo Notify updates. */
        }

        // Keep hand-setted attributes.
        $non_version_attrs = FrontHelper::get_attributes_collection(array('version' => FALSE));
        foreach ($attributes as $k => $v) {
          if (in_array($k, $non_version_attrs) && $v) {
            $attrs[$k] = $v;
          }
        }

        $movie->setAttributes( json_encode( (object)$attrs ) );

        $movies[] = (int)$_movie->FILMCODE;

      }

      $this->new_movies = $new_movies;
      $this->consoleInfo(sprintf( 'New movies: %d', $new_movies ));
      
      if ( !isset($data['propagate']) || TRUE === $data['propagate'] ) {
        if ( !isset($data['enqueue']) || TRUE === $data['enqueue'] ) {
          // Enqueue sessions updater.
          $task = new Task();
          $task->setService('cinemex.session_updater');
          $task->setData(serialize(array('cinema_id' => $cinema->getId())));
          $em->persist($task);
          $em->flush();
        } else {
          // Flush first. Otherwise the new movies won't be available to assign
          // their sessions.
          $em->flush();
          
          // Run sessions update right now.
          $updater = $this->container->get('cinemex.session_updater');
          $updater->execute(array(
              'cinema_id' => $cinema->getId(),
              'propagate' => TRUE,
              'enqueue'   => FALSE,
          ));
          $this->console_log = array_merge($this->console_log, $updater->getConsoleLog());
        }
      }
      

      // Free memory.
      unset($task);
      unset($movies_result);
      unset($fetch_m_ids);
      unset($movie);
      unset($movies);
      unset($cinema);
      unset($cinema_repo);
      unset($movie_repo);
      unset($session_repo);
      unset($movie_info);
      unset($req);
      unset($session);
      unset($_sess);
      unset($em);
      unset($doctrine);
		} catch ( \Exception $e ) {
			$this->setError($e->getMessage());
      $this->consoleInfo($e->getMessage());
			return FALSE;
		}

		return TRUE;
  }
  
  
  public function getNewMovies() {
    return $this->new_movies;
  }

}
