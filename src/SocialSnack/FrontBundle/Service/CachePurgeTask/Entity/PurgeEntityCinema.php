<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityCinema extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->frontUtils->get_permalink($this->entity),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('home'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('esi_js_stuff'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('esi_location_popup'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('cinemas_archive', ['id' => $this->entity->getState()->getId()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_cinemas_default'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_cinemas_single', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_cinemas_default'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_cinemas_single', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'CinemaRepository:find:' . $this->entity->getId(),
                'SocialSnack\WsBundle\Entity\StateRepository::findAllWithAreasAndCinemas',
                sprintf('SocialSnack\WsBundle\Entity\StateRepository::findWithAreasAndCinemas:%d_', $this->entity->getState()->getId())
            ],
            'env' => 'front'
        ]
    ];
  }

}