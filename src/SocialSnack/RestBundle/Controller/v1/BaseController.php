<?php

namespace SocialSnack\RestBundle\Controller\v1;

use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;

use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;

use SocialSnack\FrontBundle\Entity\MovieVote;

class BaseController extends Controller {

  protected $auth_method = NULL;
  
	
	/**
	 * Methods which require user authentication.
	 * @var array
	 */
	static $auth_methods = array(
			'movieSeen',
			'movieUnseen',
			'movieWish',
			'movieUnwish',
			'movieVote',
	);
	
	
	/**
	 * Authenticated user
	 * @var \SocialSnack\FrontBundle\Entity\User
	 */
	protected $user;

	
	/**
	 * Build credentials token.
	 * 
	 * @param \SocialSnack\FrontBundle\Entity\User $user
	 * @return string
	 */
	protected function build_token( $user ) {
		$app_config      = $this->container->getParameter( 'mobile_app' );
		$secret_key      = $app_config['secret_key'];
		$firewall        = 'secured_area';
		$token           = new UsernamePasswordToken( $user->getUsername(), $user->getPassword(), $firewall, array( 'ROLE_MOBILE' ) );
		$access_token    = $user->getId() . '.' . hash_hmac( 'sha256', serialize( $token ), $secret_key );
		
		return $access_token;
	}
	
	
	/**
	 * Check user authorization.
	 * 
	 * @todo Check user roles.
	 * @todo Allow two authorization methods: access_token for API use and standard
	 *       Symphony security for browser use.
	 */
	protected function authorize() {
    if ( $this->user = $this->getUser() ) {
      $this->auth_method = 'symfony';
      return TRUE;
    }
    
    $headers = $this->getRequest()->headers;
    if (!$headers->has('Authorization') && function_exists('apache_request_headers')) {
      $all = apache_request_headers();

      if (isset($all['Authorization'])) {
        $headers->set('Authorization', $all['Authorization']);
      } else if ( isset($all['Authorizationx']) ) {
        $headers->set('Authorization', $all['Authorizationx']);
      }
    }

    $header = $headers->get('Authorization');
		if ( !$header )
			return FALSE;
		
		$header        = explode( ' ', $header, 2 );
		if ( 'token' != strtolower( $header[0] ) )
			return FALSE;
		
		$access_token  = $header[1];
		if ( empty( $access_token ) )
			return FALSE;
		
		$repo          = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
		list($user_id) = explode( '.', $access_token, 2 );
		$user          = $repo->find( (int)$user_id );
		
		if ( !$user )
			return FALSE;
		
		if ( $access_token == $this->build_token( $user ) ) {
			$this->user = $user;
      $this->auth_method = 'api';
			return TRUE;
		}
		
		return FALSE;
	}
	
	
	protected function _authorize() {
		if ( !$this->authorize() ) {
      throw new RestException('not-authorized', 'Not authorized.', 401 );
		}
	}
	
  
  protected function get_fave_cinemas_ids( $user_id ) {
    $user_faves  = array();
    if ( !$this->authorize() )
      return $user_faves;
    
    $user = $this->user;
		if ( $user_id && $user && ($user->getId() == $user_id) ) {
			foreach ( $user->getCinemasFav() as $fav ) {
				$user_faves[] = $fav->getId();
			}
		}
    return $user_faves;
  }


  protected function success_auth_res($user, $from_frontend = FALSE) {
    return $this->get('rest.auth.handler')->buildAuthResponse(
        $user,
        $this->getRequest(),
        $from_frontend
    );
  }


  public function getApiVersion() {
    return 1;
  }

}
