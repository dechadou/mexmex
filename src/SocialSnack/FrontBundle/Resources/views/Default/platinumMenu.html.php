<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Platino - Menú - Cinemex' );
$view['slots']->start('body')
?>

<?php
echo $view->render('SocialSnackFrontBundle:Sections:platinumMenu.html.php', [
  'route' => $route
]);
?>

<div class="section-content">
  <div class="content-box">
    <h1 class="section-title">Menú Platino</h1>

    <div class="grid-row">
      <div class="grid-col-7">
        <div class="slider-container" data-slider>
          <div class="slider-slider">
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-01.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-02.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-03.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-04.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-05.jpg'); ?>">
            </div>
          </div><!-- slider-slider -->

          <div class="slider-dots grid-row">
            <a class="grid-col-3" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-01.jpg'); ?>"></a>
            <a class="grid-col-3" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-02.jpg'); ?>"></a>
            <a class="grid-col-3" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-03.jpg'); ?>"></a>
            <a class="grid-col-3" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-04.jpg'); ?>"></a>
            <a class="grid-col-3" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-05.jpg'); ?>"></a>
          </div>
        </div>
      </div>

      <div class="grid-col-5">
        <p>En Platino Cinemex® contamos con un servicio personalizado dentro de la sala, a tu llegada nuestro staff estará al pendiente de ti para ofrecerte nuestra selecta variedad de alimentos que podrás deleitar durante tu película.</p>
        <p>Y si vienes a Platino, no te puedes perder nuestras exclusivas Palomitas Gourmet, que por su sabor te harán regresar.</p>
      </div>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
