<?php

namespace SocialSnack\FrontBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\FrontBundle\Entity\User;
use SocialSnack\FrontBundle\Exception\CustomHttpException;
use SocialSnack\RestBundle\Entity\AppUserAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DialogController extends Controller {

  const MSG_BAD_CREDENTIALS = 1;


  protected function getApp($client_id, $redirect_uri) {
    if (!$client_id) {
      throw new CustomHttpException(400, 'Client ID missing.');
    }

    if (!$redirect_uri) {
      throw new CustomHttpException(400, 'Redirect URI missing.');
    }

    $app = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App')->findByAppId($client_id);

    if (!$app) {
      throw new CustomHttpException(400, 'Invalid Client ID');
    }

    // Check if the app is allowed to use this auth method.
    if (!$app->hasCap('oauth')) {
      throw new AccessDeniedHttpException();
    }

    // Check domain.
    if (!preg_match('/^https?:\/\/' . preg_quote($app->getSetting('domain')) . '(\/|$)/', $redirect_uri)) {
      throw new CustomHttpException(400, 'Redirect URI not allowed.');
    }

    return $app;
  }


  /**
   * @param string $redirect_uri
   * @return string
   */
  protected function buildCancelUri($redirect_uri) {
    return $redirect_uri
        . (strpos($redirect_uri, '?') === FALSE ? '?' : '&')
        . 'error_reason=user_denied&error=access_denied&error_description=The+user+denied+your+request.';
  }


  /**
   * @Route("/oauth", name="front_dialog_oauth")
   * @Method({"GET"})
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws CustomHttpException
   */
  public function oauthAction(Request $request) {
    $client_id    = $request->query->get('client_id');
    $redirect_uri = $request->query->get('redirect_uri');

    // If the user is already logged in redirect to the next screen.
    $user = $this->getUser();
    if ($user instanceof User) {
      return $this->redirect($this->generateUrl('front_dialog_appGrantAccess', [
          'client_id' => $client_id,
          'redirect_uri' => $redirect_uri,
      ]));
    }

    $this->getApp($client_id, $redirect_uri);

    return $this->render('SocialSnackFrontBundle:Dialog:oauth.html.php', [
        'client_id'    => $client_id,
        'redirect_uri' => $redirect_uri,
        'cancel_uri'   => $this->buildCancelUri($redirect_uri),
    ]);
  }


  /**
   * @Route("/oauth")
   * @Method({"POST"})
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Exception
   */
  public function oauthPostAction(Request $request) {
    $client_id    = $request->request->get('client_id');
    $redirect_uri = $request->request->get('redirect_uri');

    // Attempt to log the user in.
    /** @var \SocialSnack\RestBundle\Handler\AuthHandler $auth_handler */
    $auth_handler = $this->get('rest.auth.handler');
    $user = $auth_handler->auth($request);

    // If authentication failed return to the login form.
    if (!$user instanceof User) {
      return $this->redirect($this->generateUrl('front_dialog_oauth', [
          'client_id'    => $client_id,
          'redirect_uri' => $redirect_uri,
          'message'      => self::MSG_BAD_CREDENTIALS,
      ]));
    }

    return $this->redirect($this->generateUrl('front_dialog_appGrantAccess', [
        'client_id' => $client_id,
        'redirect_uri' => $redirect_uri,
    ]));
  }


  /**
   * @Route("/appGrantAccess", name="front_dialog_appGrantAccess")
   * @Method({"GET"})
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws AccessDeniedHttpException
   */
  public function appGrantAccessAction(Request $request) {
    $user = $this->getUser();
    if (!$user instanceof User) {
      throw new AccessDeniedHttpException();
    }

    $client_id    = $request->query->get('client_id');
    $redirect_uri = $request->query->get('redirect_uri');

    $app = $this->getApp($client_id, $redirect_uri);

    // Generate a token associated with this request (app ID and redirect URI).
    $token = md5(uniqid() . '.' . $client_id); // @todo Better token
    $mem = $this->get('memcached');
    $mem->set($token . '.' . $client_id, [ // @todo Add expiration time
        'redirect_uri' => $redirect_uri
    ]);

    // If the user already granted access to this app remember and skip this step.
    // @todo Check the scope too
    $access_repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:AppUserAccess');
    $access = $access_repo->findOneBy([
        'app'  => $app,
        'user' => $user
    ]);
    if ($access) {
      // Forwarded to a POST action. Copy GET params to POST bag.
      $request->request->set('client_id', $client_id);
      $request->request->set('redirect_uri', $redirect_uri);
      $request->request->set('token', $token);
      return $this->forward('SocialSnackFrontBundle:Dialog:appGrantAccessPost', [
          'request' => $request
      ]);
    }

    return $this->render('SocialSnackFrontBundle:Dialog:appGrantAccess.html.php', [
        'client_id'    => $client_id,
        'redirect_uri' => $redirect_uri,
        'cancel_uri'   => $this->buildCancelUri($redirect_uri),
        'token'        => $token,
        'client'       => $app,
    ]);
  }


  /**
   * @Route("/appGrantAccess")
   * @Method({"POST"})
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function appGrantAccessPostAction(Request $request) {
    $user = $this->getUser();
    if (!$user instanceof User) {
      throw new AccessDeniedHttpException();
    }

    $token        = $request->request->get('token');
    $client_id    = $request->request->get('client_id');
    $redirect_uri = $request->request->get('redirect_uri');
    $mem_key      = $token . '.' . $client_id;
    $auth_handler = $this->get('rest.auth.handler');
    $mem          = $this->get('memcached');
    $app          = $this->getApp($client_id, $redirect_uri);

    $access_repo = $this->getDoctrine()->getRepository('SocialSnackRestBundle:AppUserAccess');
    $access = $access_repo->findOneBy([
        'app'  => $app,
        'user' => $user
    ]);

    if (!$access) {
      $stored = $mem->get($mem_key);

      // The token should be valid only for one attempt.
      $mem->delete($mem_key);

      // Check token and parameters consistency.
      if ($stored['redirect_uri'] !== $redirect_uri) {
        throw new CustomHttpException(401, 'Invalid session. Please close this window and try again.');
      }

      // Remember the granted scope.
      $access = new AppUserAccess();
      $access
          ->setApp($app)
          ->setUser($user)
          ->setScope(['basic' => TRUE]);
      $em = $this->getDoctrine()->getManager();
      $em->persist($access);
      $em->flush();
    }

    // If authentication succeeded generate code and redirect.
    $time = time();
    $code = $auth_handler->generateOauthCode($user, $app, $time);

    // Associate the code with the user.
    $set = $mem->set($code . '.' . $client_id, [ // @todo Add expiration time
        'timestamp'    => $time,
        'user_id'      => $user->getId(),
        'redirect_uri' => $redirect_uri,
    ]);

    return $this->redirect($redirect_uri . (strpos($redirect_uri, '?') === FALSE ? '?' : '&') . 'code=' . $code);
  }

}