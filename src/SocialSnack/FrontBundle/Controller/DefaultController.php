<?php

namespace SocialSnack\FrontBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Endroid\QrCode\QrCode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SocialSnack\FrontBundle\Entity\Article;
use SocialSnack\FrontBundle\Entity\Contact;
use SocialSnack\FrontBundle\Exception\NoMoviesInHomeException;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\MovieComing;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class DefaultController extends BaseController {

  /**
   * @Route("/", name="home")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="1800")
   */
  public function indexAction(Request $request) {
    if ($request->query->has('theatreID')) {
      $response = new RedirectResponse($this->generateUrl('home'), 301);
      $response->setSharedMaxAge(60 * 60);
      return $response;
    }
    
    if ( !is_null($request->query->get('no_redirect')) ) {
      if ( 'mobile' === $request->query->get('switch') ) {
        $response = new RedirectResponse($this->generateUrl('mobile_index', array(), TRUE));
        $response->headers->clearCookie('force_desktop');
        return $response;
      }

      $response = new RedirectResponse($this->generateUrl('home'));
      $response->headers->setCookie(new Cookie('force_desktop', '1'));
      return $response;
    }

    $movie_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );
    $coming_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:MovieComing' );

    $movies        = $movie_repo->findAll();
    $_movies       = $this->_get_js_movies( $movies );

    // If there are no movies available there's probably something wrong. Log and alert.
    if (!sizeof($movies)) {
      $this->get('logger')->critical('No movies in home!!! :S');
      // We don't want a successful response so the previous response remains cached.
      throw new NoMoviesInHomeException();
    }

    // Filter pre-sale movies.
    $presales = [];
    foreach ($movies as $movie) {
      if (preg_match('/\bpresale\b/', $movie->getAttributes())) {
        $presales[] = $movie;
      }
    }

    // Filter coming movies.
    $_coming = $coming_repo->findAllFuture();
    $coming = [];
    $friday = [];
    foreach ($_coming as $movie) {
      if ($movie->getNextWeek()) {
        $friday[] = $movie;
      } else {
        $coming[] = $movie;
      }
    }
    unset($_coming);

    $promoa_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackFrontBundle:PromoA' );
    $promosa       = $promoa_repo->findActive();

    return array(
        'movies'   => $movies,
        '_movies'  => $_movies,
        'presales' => $presales,
        'coming'   => $coming,
        'friday'   => $friday,
        'promosa'  => $promosa,
    );
  }


  /**
   * @Route("/apps/")
   */
  public function appsAction() {
    return $this->forward('SocialSnackFrontBundle:Mobile:apps');
  }


  /**
   * @Route("/cinema", name="cinemom")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function cinemomAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/cinema/preguntas-frecuentes", name="cinemom_faq")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function cinemomFaqAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }

  /**
   * @Route("/cinema/terminos-y-condiciones", name="cinemom_terms")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function cinemomTermsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }

  /**
   * @Route("/cinema/cines", name="cinemom_cinemas")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="21600", maxage="7200")
   */
  public function cinemomCinemasAction() {
    $sessions = $this->get('ss.ws.utils')->cinemomCinemasQuery();

    return array(
        'route'    => $this->getRequest()->attributes->get('_route'),
        'sessions' => $sessions,
    );
  }


  /**
   * @Route("/platino", name="platinum")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function platinumAction() {
    $movie_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );
    $cinema_repo  = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Cinema' );

    $platinum     = $cinema_repo->findPlatinum( TRUE );
    $movies       = $movie_repo->findAllMoviesByCinemaByDate( $platinum, new \DateTime( 'now' ), 0 );
    $__movies     = array();

    foreach ( $movies as $movie ) {
      if ( !$parent = $movie->getParent() ) {
        $__movies[$movie->getId()] = $movie;
        continue;
      }

      if ( isset( $__movies[$parent->getId()] ) ) {
        continue;
      }

      $__movies[$parent->getId()] = $parent;
    }

    WsHelper::sortMoviesDefault($__movies);
    $_movies = $this->_get_js_movies( $__movies );

    return array(
        'route'      => $this->getRequest()->attributes->get('_route'),
        'movies'     => $__movies,
        '_movies'    => $_movies,
    );
  }


  /**
   * @Route("/platino/cines/{id}", name="platinum_cinemas")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function platinumCinemasAction( $id = NULL ) {
    $cinemas = $this->get('concept.helper.factory')->createHelper('Platinum')->getCinemasLegacyIds();

    return $this->_mapByAttr('platinum_cinemas', $id, $cinemas);
  }


  /**
   * @Route("/platino/instalaciones", name="platinum_facilities")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function platinumFacilitiesAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/platino/menu", name="platinum_menu")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function platinumMenuAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/casa-de-arte", name="art")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function artAction() {
    $movie_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );

    $movies        = $movie_repo->findByAttr( array( 'art' => true ) ); // cache id: movie_findbyattr_art=1
    $__movies      = array();

    foreach ( $movies as $movie ) {
      if ( !$parent = $movie->getParent() ) {
        $__movies[$movie->getId()] = $movie;
        continue;
      }

      if ( isset( $__movies[$parent->getId()] ) ) {
        continue;
      }

      $__movies[$parent->getId()] = $parent;
    }

    $_movies       = $this->_get_js_movies( $__movies );

    return array(
        'route'      => $this->getRequest()->attributes->get('_route'),
        'movies'     => $__movies,
        '_movies'    => $_movies,
    );
  }


  /**
   * @Route("/casa-de-arte/cines", name="art_cinemas")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function artCinemasAction() {
    $cinemas = $this->get('concept.helper.factory')->createHelper('Art')->getCinemas();

    return array(
        'route'       => $this->getRequest()->attributes->get('_route'),
        'art_cinemas' => $cinemas,
    );
  }


  /**
   * @Route(
   *   "/pelicula/{movie_id}/{slug}",
   *   name="movie_single",
   *   requirements={"movie_id" = "[0-9]+"},
   *   defaults={"slug" = ""}
   * )
   * @Template(engine="php")
   * @Cache(public=true, smaxage="21600", maxage="3600")
   */
  public function movieSingleAction( $movie_id, $slug ) {
    $movie_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );

    $movie         = $movie_repo->find( $movie_id );
    if ( !$movie ) {
      throw new NotFoundHttpException();
    }

    if ( $parent = $movie->getParent() ) {
      return $this->redirect( $this->container->get('ss.front.utils')->get_movie_link($parent), 301 );
    }

    if ($slug !== FrontHelper::sanitize_for_url($movie->getName())) {
      return $this->redirect( $this->container->get('ss.front.utils')->get_movie_link($movie), 301 );
    }

    return array(
        'movie' => $movie,
    );
  }


  /**
   * @Route("/cartelera/pelicula/{legacy_id}")
   * @Route("/cartelera/pelicula/{legacy_id}/{legacy_cinema_id}")
   */
  public function movieSingleOldAction($legacy_id) {
    $movie_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );
    $movie         = $movie_repo->findOneBy( array( 'legacy_id' => $legacy_id ) );
    if ( !$movie ) {
      return $this->redirect( $this->generateUrl('home'), 301 );
    }
    return $this->redirect( $this->generateUrl('movie_single', array('movie_id' => $movie->getId())), 301 );
  }


  /**
   * @Route(
   *   "/proximamente/{movie_id}/{slug}",
   *   name="moviecoming_single",
   *   requirements={"movie_id" = "[0-9]+"},
   *   defaults={"slug" = ""}
   * )
   * @Template(engine="php")
   * @Cache(public=true, smaxage="21600", maxage="3600")
   * @ParamConverter("movie",  class="SocialSnackWsBundle:MovieComing",  options={"id" = "movie_id"})
   */
  public function movieComingSingleAction(MovieComing $movie, $slug ) {
    if ($slug !== FrontHelper::sanitize_for_url($movie->getName())) {
      return $this->redirect( $this->container->get('ss.front.utils')->get_movie_link($movie, 'moviecoming_single'), 301 );
    }

    return array(
        'movie' => $movie,
    );
  }


  /**
   * @Route("/cines/{id}",
   *   name="cinemas_archive"
   * )
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function cinemasAction( $id = NULL ) {
    $default_id = $this->container->getParameter( 'default_cinema' );
    if ( !$id ) {
      $id = $default_id;
      $fallback = TRUE;
    } else {
      $fallback = FALSE;
    }
    $state_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:State' );
    $state        = $state_repo->findWithAreasAndCinemas( $id );

    if ( !$state ) {
      return $this->redirect( $this->generateUrl('cinemas_archive', array('id' => $default_id)) );
    }

    return $this->merge_js_stuff(
        array(
            'state'      => $state,
            'fallback'   => $fallback,
            'is_cinemas' => TRUE
        )
    );
  }


  /**
   * @Route("/cines/")
   */
  public function cinemasOldAction() {
    return $this->redirect($this->generateUrl('cinemas_archive'). 301);
  }


  /**
   * @Route(
   *   "/cine/{cinema_id}/{slug}",
   *   name="cinema_single",
   *   requirements={"cinema_id" = "[0-9]+"},
   *   defaults={"slug" = ""}
   * )
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function cinemaSingleAction( $cinema_id, $slug ) {
    $cinema_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Cinema' );
    $movie_repo     = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );

    $movies         = $movie_repo->findMoviesInCinema($cinema_id);
    $cinema         = $cinema_repo->find( $cinema_id );
    $closer_cinemas = $cinema_repo->findClosestCinemas($cinema_id, 5);

    if ( !$cinema ) {
      return $this->redirect($this->generateUrl('cinemas_archive'), 301);
    }

    return array(
        'movies'          => $movies,
        'cinema'          => $cinema,
        'closer_cinemas'  => $closer_cinemas,
    );
  }


  /**
   * @Route("/privacidadcompra", name="checkout_privacy_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function checkoutPrivacyAction() {
    return array(
        'route' => $this->getRequest()->attributes->get('_route'),
    );
  }


  /**
   * @Route("/imprimirCompra/{id}/{hash}", name="booking_print")
   * @Template(engine="php", template="SocialSnackFrontBundle::printBase.html.php")
   * @Cache(public=true, smaxage="7200", maxage="3600")
   */
  public function bookingConfirmationAction( $id, $hash ) {
    $trans_repo  = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:Transaction' );
    $transaction = $trans_repo->findOneBy( array( 'id' => $id, 'hash' => $hash ) );
    $user        = $this->getUser();

    if ( !$transaction )
      throw $this->createNotFoundException( 'Transacción inválida.' );

    $content = $this->renderView( 'SocialSnackFrontBundle:Default:bookingConfirmation.html.php', array(
        'show_print'  => FALSE,
        'code'        => $transaction->getCode(),
        'qr_url'      => $transaction->getQr(),
        'transaction' => $transaction,
        'session'     => $transaction->getSession(),
    ) );

    return array(
        'content' => $content
    );
  }


  /**
   * @Route("/promociones", name="promos_archive")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function promosAction() {
    $promoa_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackFrontBundle:PromoA' );
    $promosa       = $promoa_repo->findActive();
    $promos_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackFrontBundle:Promo' );
    $promos        = $promos_repo->findActive();

    return array (
        'promosa' => $promosa,
        'promos' => $promos
    );
  }


  /**
   * @Route("/novedades", name="news_archive")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function newsAction() {
    $repo     = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Article');
    $articles = $repo->findBy(array('status' => \SocialSnack\FrontBundle\Entity\Article::STATUS_PUBLISH), array('dateCreated' => 'DESC'));

    return array(
        'articles' => $articles
    );
  }


  /**
   * @Route("/novedades/{id}/{slug}", name="news_single",
   *   requirements={"id"="[0-9]+", "slug"=".+"},
   *   defaults={"slug"=""}
   * )
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function newsSingleAction(Article $article) {
    $repo    = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Article');
    $related = $repo->findRelated($article, array( 'id' => 'DESC' ), 2);

    return array(
        'article'  => $article,
        'related'  => $related,
        'this_url' => $this->get('ss.front.utils')->get_permalink($article, TRUE)
    );
  }


  /**
   * @Route("/corporativo", name="about_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function aboutAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/terminos", name="tos_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function tosAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/comunicados", name="announcements_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function announcementsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadcineusuario", name="privacy_page")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacy.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadinvitadoespecial")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadinvitadoespecial.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyIeAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadvisitantes", name="privacy_users")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadvisitantes.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyVisitorsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadproveedores", name="privacy_providers")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadproveedores.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyProvidersAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadpromociones", name="privacy_promos")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadpromociones.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyPromosAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadfacturacion", name="privacy_billing")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadfacturacion.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyBillingAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadexempleados", name="privacy_employees")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadexempleados.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyEmployeesAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadcandidatos", name="privacy_candidates")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadcandidatos.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyCandidatesAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/privacidadpayback", name="privacy_payback")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadpayback.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyPaybackAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/productos", name="products_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function productsAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/contacto", name="contact")
   * @Template(engine="php")
   * @Method({"GET"})
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function contactAction() {
    $doctrine = $this->getDoctrine();

    $cinemas  = $doctrine->getRepository('SocialSnackWsBundle:Cinema')
        ->findBy(['active' => TRUE], ['name' => 'ASC']);

    return array(
        'cinemas'    => $cinemas,
        'success'    => $this->getRequest()->get('success') == 1,
        'route'      => $this->getRequest()->attributes->get('_route'),
        'flash_msgs' => $this->get_flash_bag(),
    );
  }


  /**
   * @Route("/contacto", name="contact_post")
   * @Template(engine="php")
   * @Method({"POST"})
   */
  public function contactPostAction() {
    $request  = $this->getRequest();
    $em       = $this->getDoctrine()->getManager();
    $fields   = array( 'name', 'email', 'phone', 'ie', 'cinema', 'service', 'type', 'msg' );
    $required = array( 'name', 'email', 'msg' );
    $valid    = TRUE;
    $success  = FALSE;

    foreach ( $required as $field ) {
      if ( !$request->get( $field ) ) {
        $valid = FALSE;
        break;
      }
    }

    if ( !$valid )
      return new JsonResponse( array(), 400 );

    $contact = new Contact();
    $contact->setEmail(   $request->get( 'email' ) );
    $contact->setMsg(     $request->get( 'msg' ) );
    $contact->setName(    $request->get( 'name' ) );
    $contact->setCinema(  $request->get( 'cinema' )  ? $request->get( 'cinema' )  : NULL );
    $contact->setIe(      $request->get( 'ie' )      ? $request->get( 'ie' )      : NULL );
    $contact->setPhone(   $request->get( 'phone' )   ? $request->get( 'phone' )   : NULL );
    $contact->setService( $request->get( 'service' ) ? $request->get( 'service' ) : NULL );
    $contact->setType(    $request->get( 'type' )    ? $request->get( 'type' )    : NULL );

    try {
      $em->persist( $contact );
      $em->flush();
      $success = TRUE;
    } catch ( \Exception $e ) {}

    if ( !$success )
      return new JsonResponse( array(), 400 );

    // Deliver the email.
    $this->get('cinemex.mailer')->enqueue( array(
        'type' => 'contact',
        'contact_id' => $contact->getId()
    ) );

    return new JsonResponse();
  }


  /**
   * @Route("/privacidadatencion", name="contact_privacy_page")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function contactPrivacyAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/arco/formulario", name="arco_form")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:arcoForm.html.php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function arcoFormAction() {
    $request = $this->getRequest();
    $ws      = $this->container->get('cinemex_ws');
    $form    = $this->createForm(new \SocialSnack\FrontBundle\Form\Type\ArcoType($ws));

    $form->handleRequest($request);

    if ( $form->isSubmitted() ) {

      if ( $form->isValid() ) {
        $data = $form->getData();
        $this->container->get('ss.front.utils')->process_arco_form($form, $data);

        $entry = new \SocialSnack\FrontBundle\Entity\ArcoEntry();
        $entry->setData($data);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entry);
        $em->flush();

        // Deliver the email.
        $this->get('cinemex.mailer')->enqueue( array(
            'type' => 'arco',
            'entry_id' => $entry->getId()
        ) );

        $response = $this->render(
            'SocialSnackFrontBundle:Privacy:arcoSuccess.html.php',
            array(
                'route' => $this->getRequest()->attributes->get('_route'),
                'ref'   => $entry->getRef(),
            )
        );
        return $response;
      }
    }

    return array(
        'route' => $this->getRequest()->attributes->get('_route'),
        'form' => $form->createView()
    );
  }

  /**
   * nflAction function.
   *
   * @Route("/nfl", name="nfl")
   * @Template(engine="php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   *
   * @access public
   * @return void
   */
  public function nflAction() {
    $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
    $movies     = $movie_repo->findByAttr(array('nfl' => TRUE));

    return array(
        'movies' => $movies,
        'route'  => $this->getRequest()->attributes->get('_route')
    );
  }


  /**
   * @Route("/cartelera/{area_type}-{area_id}/{slug}/{arguments}",
   *   name="billboard",
   *   requirements={
   *     "area_type" = "[a-z]+",
   *     "area_id"   = "\d+",
   *     "slug"      = "[a-zA-Z0-9\-]*",
   *     "arguments" = ".*"
   *   },
   *   defaults={
   *     "area_type" = "zona",
   *     "area_id"   = "",
   *     "slug"      = "",
   *     "arguments" = ""
   *   }
   * )
   */
  public function billboardAction($area_type, $area_id, $slug, $arguments) {
    /** @var \SocialSnack\FrontBundle\Service\ConceptHelperFactory $concept_factory */
    $concept_factory = $this->get('concept.helper.factory');
    $doctrine        = $this->getDoctrine();
    $area_repo       = $doctrine->getRepository('SocialSnackWsBundle:StateArea');
    $movie_repo      = $doctrine->getRepository('SocialSnackWsBundle:Movie');
    $state_repo      = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
    $session_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:Session' );
    $is_state        = 'estado' === $area_type;
    $args            = FrontHelper::parse_url_arguments( $arguments );

    if (!$area_id) {
      $area_id = 8;
      $is_state = TRUE;
    }

    if ($is_state) {
      // By state.
      $area = $state_repo->find($area_id);
//      $available_dates = $session_repo->findAvailableDatesByState($area_id);
      $available_dates = $this->findAvailableDatesBy('state', $area_id);
      $date = FrontHelper::get_billboard_date($args, $area, $available_dates);
      $movies = $movie_repo->findAllMoviesByState($area_id, $date);

      $has_premium  = $concept_factory->createHelper('Premium')->stateHasConcept($area_id);
      $has_platinum = $concept_factory->createHelper('Platinum')->stateHasConcept($area_id);
      $has_x4d      = $concept_factory->createHelper('X4D')->stateHasConcept($area_id);
      $has_v3d      = $concept_factory->createHelper('3D')->stateHasConcept($area_id);
      $has_cx       = $concept_factory->createHelper('Xtreme')->stateHasConcept($area_id);
    } else {
      // By area.
      $area = $area_repo->findWithCinemas($area_id);
      $available_dates = $this->findAvailableDatesBy('area', $area_id);
//      $available_dates = $session_repo->findAvailableDatesByArea($area_id);
      $date = FrontHelper::get_billboard_date($args, $area, $available_dates);
      $movies = $movie_repo->findAllMoviesByArea($area_id, $date);

      $has_premium  = $concept_factory->createHelper('Premium')->areaHasConcept($area_id);
      $has_platinum = $concept_factory->createHelper('Platinum')->areaHasConcept($area_id);
      $has_x4d      = $concept_factory->createHelper('X4D')->areaHasConcept($area_id);
      $has_v3d      = $concept_factory->createHelper('3D')->areaHasConcept($area_id);
      $has_cx       = $concept_factory->createHelper('Xtreme')->areaHasConcept($area_id);
    }

    WsHelper::sortMoviesByName($movies);

    $min_time = $this->get('ss.ws.utils')->getMinSessionTime($area, $date);
    if ($min_time) {
      $min_time = ((int)str_replace(':', '', $min_time)) / 10000;
    }

    $data            = array();
    $states          = $state_repo->findAllWithAreas();
//    $date_limit_str  = date('Y-m-d 01:00:00', $date->getTimestamp() + 24*60*60);
//    $date_limit      = new \DateTime($date_limit_str, $date->getTimezone());

    if ( !empty($area_id) && $slug != FrontHelper::sanitize_for_url($area->getName()) ) {
      $url = $this->generateUrl('billboard', array(
          'area_type' => $is_state ? 'estado' : 'zona',
          'area_id'   => $area_id,
          'slug'      => FrontHelper::sanitize_for_url($area->getName()),
          'arguments' => ''
      ));
      return $this->redirect($url, 301);
    }

    if ( $is_state ) {
      $cinemas = array();
    } else {
      $cinemas = $area->getCinemas();
    }

    foreach ( $cinemas as &$cinema ) {
      $sessions = $movie_repo->findAllSessionsInCinemaByDate( $cinema->getId(), $date );
      if ( !sizeof($sessions) )
        continue;

      $data[] = array(
          'cinema'   => $cinema,
          'sessions' => $sessions,
      );
    }

    $response = $this->render('SocialSnackFrontBundle:Default:billboard.html.php', array(
        'args'     => $args,
        'date'     => $date,
        'min_time' => $min_time,
        'area'     => $area,
        'is_state' => $is_state,
        'data'     => $data,
        'states'   => $states,
        'movies'   => $movies,
        'available_dates' => $available_dates,
        'has_premium'  => $has_premium,
        'has_platinum' => $has_platinum,
        'has_x4d'      => $has_x4d,
        'has_v3d'      => $has_v3d,
        'has_cx'       => $has_cx,
    ));
    $response->setPublic();

    if ( !$is_state ) {
      $response->setMaxAge(15 * 60);
      $response->setSharedMaxAge(60 * 60);
    } else {
      $expire = new \DateTime('tomorrow');
      $expire->setTime(0, 0, 0);
      $response->setExpires($expire);
    }
    return $response;
  }


  /**
   * @Route("/captcha", name="captcha")
   */
  public function captchaAction() {
    $app_session = $this->getRequest()->getSession();
    $app_session->start();

    $captcha = new \SocialSnack\FrontBundle\Service\Captcha(array(
        'session' => $app_session,
        'expire'  => 60 * 10
    ));
    $captcha->wordsFile = '';
    $captcha->minWordLength = 4;
    $captcha->maxWordLength = 5;
    $captcha->width = 316;
    $captcha->height = 60;
    header("Cache-Control: no-store, no-cache, must-revalidate");
    $captcha->CreateImage();
    die();
  }


  /**
   * @Route("/privacidadcinefan", name="privacy_cinefan")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacidadcinefan.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function privacyCinefanAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }


  /**
   * @Route("/cache/purge/doctrine", name="cache_purge_doctrine")
   */
  public function purgeDoctrineAction() {
    $req = $this->getRequest();
    $ua = $req->headers->get('User-Agent');
    $cache_ids = $req->request->get('cache_ids');

    if ($ua != $this->container->getParameter('remote_cache_purge.user_agent')) {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

    if (!is_array($cache_ids)) {
      $cache_ids = array();
    }

    $purger = $this->get('ss.cache_purger');
    /* @var $purger \SocialSnack\FrontBundle\Service\CachePurger */

    if (!$cache_ids) {
      $purger->enqueue(array(
          'type' => 'method',
          'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_DOCTRINE_ALL,
      ));
    }

    foreach ($cache_ids as $cache_id) {
      $purger->enqueue(array(
          'type' => 'method',
          'method' => \SocialSnack\FrontBundle\Service\CachePurger::PURGE_DOCTRINE,
          'cache_id' => $cache_id,
      ));
    }

    $purge = $purger->purge_cache();

    return new JsonResponse($purge);
  }

  /**
   * @Route("/ventas-corporativas", name="corpsales_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:Landings:corpsales_index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function corpSalesIndexAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }

  /**
   * @Route("/ventas-corporativas/faq", name="corpsales_faq")
   * @Template(engine="php", template="SocialSnackFrontBundle:Landings:corpsales_faq.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function corpSalesFaqAction() {
    return array('route' => $this->getRequest()->attributes->get('_route'));
  }

  /**
   * @Route("/ventas-corporativas/informes", name="corpsales_form")
   * @Template(engine="php", template="SocialSnackFrontBundle:Landings:corpsales_form.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function corpSalesFormAction() {
    $request = $this->getRequest();
    $form    = $this->createFormBuilder();
    $success = FALSE;

    $event_type = [
      'group'        => 'Función de grupo',
      'private'      => 'Función privada',
      'rent'         => 'Renta de sala',
      'rent_private' => 'Renta de sala y función privada',
      'tickets'      => 'Boletos y Convenios',
      'etickets'     => 'Boletos Electrónicos'
    ];

    $event_email = [
      'group'        => 'anap@cinemex.net',
      'private'      => 'anap@cinemex.net',
      'rent'         => 'anap@cinemex.net',
      'rent_private' => 'anap@cinemex.net',
      'tickets'      => 'jennyl@cinemex.net',
      'etickets'     => 'lmunoz@cinemex.net'
    ];

    $form->add('name', NULL, [
        'label'    => 'Nombre completo',
        'required' => TRUE
      ])
      ->add('email', 'email', [
        'label'    => 'Email',
        'required' => TRUE
      ])
      ->add('company', NULL, [
        'label'    => 'Compañia',
        'required' => TRUE
      ])
      ->add('phone', NULL, [
        'label'    => 'Teléfono',
        'required' => TRUE
      ])
      ->add('event_type', 'choice', [
        'label'    => 'Tipo de evento',
        'required' => TRUE,
        'empty_value' => 'Seleccione una opción',
        'choices'  => $event_type
      ])
      ->add('cinema', 'entity', [
        'label'       => 'Cine',
        'empty_value' => 'Seleccione una opción',
        'required'    => TRUE,
        'class'    => 'SocialSnackWsBundle:Cinema',
        'property' => 'name',
        'query_builder' => function(EntityRepository $er){
          return $er->createQueryBuilder('s')
            ->where('s.active = 1')
            ->orderBy('s.name', 'ASC');
            }
      ])
      ->add('event_date', 'date', [
        'label'       => 'Fecha del evento',
        'required'    => TRUE,
        'format' => 'dd MM yyyy',
        'empty_value' => [
          'year'  => 'Año',
          'month' => 'Mes',
          'day'   => 'Día'
        ]
      ])
      ->add('event_time', 'time', [
        'label'    => 'Horario',
        'required' => TRUE
      ])
      ->add('capacity', NULL, [
        'label'    => 'Capacidad de la sala requerida',
        'required' => TRUE
      ])
      ->add('need_candy', 'choice', [
        'label'       => 'Requiere de productos de dulcería?',
        'required'    => TRUE,
        'empty_value' => 'Seleccione una opción',
        'choices'     => [
          'si' => 'Si',
          'no' => 'No'
        ]
      ])
      ->add('comments', 'textarea', [
        'label'    => 'Comentarios',
        'required' => FALSE
      ])
      ->add('captcha', 'hidden')
      ->add('submit', 'submit', [
        'label' => 'Enviar'
      ]);

    $form = $form->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) :
      $recived_captcha  = $request->get('captcha');
      $expected_captcha = $request->getSession()->get('captcha');

      if ($form->isValid() AND $recived_captcha === $expected_captcha) :
        $data      = $form->getData();
        $success   = TRUE;
        $recipient = $event_email[$data['event_type']];

        $this->get('cinemex.mailer')->enqueue([
          'type'      => 'corpsales',
          'data'      => $data,
          'recipient' => $recipient
        ]);
      else :
        $error = new FormError('Los datos ingresados no son válidos. Por favor, asegúrate de completar
          todos los campos requeridos e ingresar un código de verificación válido.');

        $form->get('captcha')->addError($error);
      endif;
    endif;

    return array(
      'route'   => $this->getRequest()->attributes->get('_route'),
      'form'    => $form->createView(),
      'success' => $success
    );
  }

  /**
   * Helper to get movies by attr
   *
   * @param array $attr
   * @return array
   */
  protected function _billboardByAttr($attr) {
    $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
    $movies     = $movie_repo->findByAttr((array) $attr);
    $__movies   = [];

    foreach ($movies as $movie) {
      if (!$parent = $movie->getParent()) {
        $__movies[$movie->getId()] = $movie;
        continue;
      }

      if (isset($__movies[$parent->getId()])) {
        continue;
      }

      $__movies[$parent->getId()] = $parent;
    }

    $_movies = $this->_get_js_movies($__movies);

    return array(
        '__movies' => $__movies,
        '_movies'  => $_movies
    );
  }


  protected function _mapByAttr($route, $state_id, $cinema_ids) {

    $default_id = $this->container->getParameter( 'default_cinema' );

    if ('null' === $state_id) {
      return $this->redirect($this->generateUrl($route, array('id' => $default_id)));
    }

    if (!$state_id || 'null' === $state_id) {
      $state_id = $default_id;
      $fallback = TRUE;
    } else {
      $fallback = FALSE;
    }

    $found = ($cinema_ids != NULL) ? TRUE : FALSE;

    $tree  = array();

    // Get the current state
    $state   = $this->getDoctrine()
      ->getRepository('SocialSnackWsBundle:State')
      ->find($state_id);

    if (!$state) {
      throw $this->createNotFoundException();
    }

    // Gets the states for this cinemas
    $qb = $this->getDoctrine()
      ->getRepository('SocialSnackWsBundle:Cinema')
      ->createQueryBuilder('c');

    $qb->select('s')
      ->innerJoin('SocialSnackWsBundle:State', 's', 'WITH', 's.id = c.state')
      ->andWhere('c.active = 1')
      ->andWhere('c.legacy_id IN (:ids)')
      ->addGroupBy('s')
      ->addOrderBy('s.order', 'DESC')
      ->addOrderBy('s.name',  'ASC')
      ->setParameter('ids', $cinema_ids);

    $query  = $qb->getQuery();
    $states = $query->getResult();

    // Gets cinemas in the current state that matches the cinemas list
    $qb = $this->getDoctrine()
      ->getRepository('SocialSnackWsBundle:Cinema')
      ->createQueryBuilder('c');

    $qb->addSelect('c')
      ->innerJoin('SocialSnackWsBundle:State', 's', 'WITH', 's.id = c.state')
      ->andWhere('c.active = 1')
      ->andWhere('c.state = :state_id')
      ->andWhere('c.legacy_id IN (:ids)')
      ->addGroupBy('c.id')
      ->addOrderBy('c.name', 'ASC')
      ->setParameter('ids', $cinema_ids)
      ->setParameter('state_id', $state_id);

    $query   = $qb->getQuery();
    $cinemas = $query->getResult();

    foreach ($cinemas as $cinema) {
      $area = $cinema->getArea();

      if (!isset($tree[$area->getId()])) {
        $tree[$area->getId()] = array(
            'area'    => $area,
            'cinemas' => array(),
        );
      }

      $tree[$area->getId()]['cinemas'][] = $cinema;
    }
    usort($tree, function($a, $b) { return $a['area']->getName() > $b['area']->getName(); });

    return array(
        'state'        => $state,
        'states'       => $states,
        'tree'         => $tree,
        'found'        => $found,
        'fallback'     => $fallback,
        'current_page' => 'cinemas',
        'route'        => $route,
        'found'        => $found
    );
  }


  /**
   * @Route("/nosotros/", name="about_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:About:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function aboutIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/nosotros/concesiones", name="about_concesiones")
   * @Template(engine="php", template="SocialSnackFrontBundle:About:concesiones.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function aboutConcesionesAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'concesiones'
    );
  }

  /**
   * @Route("/nosotros/conciencia", name="about_conciencia")
   * @Template(engine="php", template="SocialSnackFrontBundle:About:conciencia.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function aboutConcienciaAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'conciencia'
    );
  }

  /**
   * @Route("/premium", name="premium_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:Premium:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function premiumIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/premium/cartelera", name="premium_billboard")
   * @Template(engine="php", template="SocialSnackFrontBundle:Premium:billboard.html.php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function premiumBillboardAction() {
    $qb = $this->container->get('doctrine')->getManager()->createQueryBuilder();

    $qb->addSelect('m')
      ->from('SocialSnackWsBundle:Movie', 'm')
      ->innerJoin('SocialSnackWsBundle:Session', 's', 'WITH', 's.group_id = m.group_id')
      ->andWhere($qb->expr()->isNull('m.parent'))
      ->andWhere('s.active = 1')
      ->andWhere('s.premium = 1')
      ->addGroupBy('m.id')
      ->addOrderBy('m.premiere', 'DESC')
      ->addOrderBy('m.fixed_position', 'DESC')
      ->addOrderBy('m.position', 'DESC')
      ->addOrderBy('m.name', 'ASC');

    $query    = $qb->getQuery();
    $query->useResultCache(TRUE, 30 * 60, 'premiumMoviesQuery');

    $movies   = $query->getResult();
    $__movies = [];

    foreach ($movies as $movie) {
      if (!$parent = $movie->getParent()) {
        $__movies[$movie->getId()] = $movie;
        continue;
      }

      if (isset($__movies[$parent->getId()])) {
        continue;
      }

      $__movies[$parent->getId()] = $parent;
    }

    $_movies = $this->_get_js_movies($__movies);

    return array(
        'movies'   => $__movies,
        '_movies'  => $_movies,
        'current_page' => 'billboard'
    );
  }

  /**
   * @Route("/premium/cines/{id}", name="premium_cinemas")
   * @Template(engine="php", template="SocialSnackFrontBundle:Premium:cinemas.html.php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function premiumCinemasAction( $id = NULL ) {
    $cinemas = $this->get('concept.helper.factory')->createHelper('Premium')->getCinemasLegacyIds();

    return $this->_mapByAttr('premium_cinemas', $id, $cinemas);
  }

  /**
   * @Route("/xtremo", name="xtremo_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:Xtremo:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function xtremoIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/xtremo/cartelera", name="xtremo_billboard")
   * @Template(engine="php", template="SocialSnackFrontBundle:Xtremo:billboard.html.php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function xtremoBillboardAction() {
    $results = $this->_billboardByAttr(['cx' => TRUE]);

    return array(
      'movies'   => $results['__movies'],
      '_movies'  => $results['_movies'],
      'current_page' => 'billboard'
    );
  }

  /**
   * @Route("/xtremo/cines/{id}", name="xtremo_cinemas")
   * @Template(engine="php", template="SocialSnackFrontBundle:Xtremo:cinemas.html.php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function xtremoCinemasAction( $id = NULL ) {
    $cinemas = $this->get('concept.helper.factory')->createHelper('Xtreme')->getCinemasLegacyIds();

    return $this->_mapByAttr('xtremo_cinemas', $id, $cinemas);
  }


  /**
   * @Route("/3d", name="3d_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:3d:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function v3dIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/3d/cartelera", name="3d_billboard")
   * @Template(engine="php", template="SocialSnackFrontBundle:3d:billboard.html.php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function v3dBillboardAction() {
    $results = $this->_billboardByAttr(['v3d' => TRUE]);

    return array(
      'movies'   => $results['__movies'],
      '_movies'  => $results['_movies'],
      'current_page' => 'billboard'
    );
  }

  /**
   * @Route("/3d/cines/{id}", name="3d_cinemas")
   * @Template(template="SocialSnackFrontBundle:3d:cinemas.html.php", engine="php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function v3dCinemasAction($id = NULL) {
    $cinemas = $this->get('concept.helper.factory')->createHelper('3D')->getCinemasLegacyIds();

    return $this->_mapByAttr('3d_cinemas', $id, $cinemas);
  }


  /**
   * @Route("/x4d", name="x4d_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:X4d:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function x4dIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/x4d/cartelera", name="x4d_billboard")
   * @Template(engine="php", template="SocialSnackFrontBundle:X4d:billboard.html.php")
   * @Cache(public=true, smaxage="3600", maxage="1800")
   */
  public function x4dBillboardAction() {
    $results = $this->_billboardByAttr(['v4d' => TRUE]);

    return array(
        'x4d_empty'       => $this->render('SocialSnackFrontBundle:Partials:billboardEmpty.html.php')->getContent(),
        'x4d_unavailable' => $this->render('SocialSnackFrontBundle:Partials:billboardUnavailable.html.php')->getContent(),
        'movies'          => $results['__movies'],
        '_movies'         => $results['_movies'],
        'current_page'    => 'billboard'
    );
  }

  /**
   * @Route("/x4d/cines/{id}", name="x4d_cinemas")
   * @Template(engine="php", template="SocialSnackFrontBundle:X4d:cinemas.html.php")
   * @Cache(public=true, smaxage="86400", maxage="7200")
   */
  public function x4dCinemasAction( $id = NULL ) {
    $cinemas = $this->get('concept.helper.factory')->createHelper('X4D')->getCinemasLegacyIds();

    return $this->_mapByAttr('x4d_cinemas', $id, $cinemas);
  }


  /**
   * @Route("/espacio-alternativo", name="ea_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:Space:index.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function spaceIndexAction() {
    return array(
      'route'        => $this->getRequest()->attributes->get('_route'),
      'current_page' => 'index'
    );
  }

  /**
   * @Route("/privacidad/", name="privacy_index")
   * @Template(engine="php", template="SocialSnackFrontBundle:Privacy:privacy.html.php")
   * @Cache(public=true, smaxage="604800", maxage="7200")
   */
  public function allPrivacyAction() {
    return $this->redirect($this->generateUrl('privacy_users'), 301);
  }

  /**
   * @Route("/empleos/", name="jobs_index")
   */
  public function JobApplicationIndexAction() {
    $response = $this->render('SocialSnackFrontBundle:JobApplication:index.html.php', array(
      'route' => $this->getRequest()->attributes->get('_route'),
    ));

    return $response;
  }

  /**
   * @Route("/empleos/mision/", name="jobs_mission")
   */
  public function jobApplicationMissionAction() {
    $response = $this->render('SocialSnackFrontBundle:JobApplication:mission.html.php', array(
      'route' => $this->getRequest()->attributes->get('_route'),
    ));

    return $response;
  }

  /**
   * @Route("/empleos/nosotros/", name="jobs_about")
   */
  public function jobApplicationAboutNosotros() {
    $response = $this->render('SocialSnackFrontBundle:JobApplication:about.html.php', array(
      'route' => $this->getRequest()->attributes->get('_route'),
    ));

    return $response;
  }

  /**
   * @Route("/empleos/formulario/", name="jobs_form")
   */
  public function jobApplicationFormAction(Request $request) {
    $doctrine = $this->getDoctrine();

    /**
     * GET REQUIRED DATA FOR FORM!
     */

    // years for birthdate field
    $year  = (int) date('Y');
    $years = range($year - 16, $year - 100);

    // the cinemas list
    $repo    = $doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas = $repo->findAll();

    foreach ($cinemas as $cinema) {
      $choices[$cinema->getId()] = $cinema->getName();
    }

    $cafes = [
      'CAFÉ CENTRAL GOURMET (ITAM)'            => 'Café Ccentral Gourmet (ITAM)',
      'CAFÉ CENTRAL GOURMET SANTA FE'          => 'Café Central Gourmet Santa Fé',
      'CAFÉ CENTRAL GOURMET PARQUE REFORMA'    => 'Café Central Gourmet Parque Reforma',
      'CAFÉ CENTRAL GOURMET GALERÍAS MAZATLÁN' => 'Café Central Gourmet Galerías Mazatlán'
    ];

    // gets the states list
    $state_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
    $states        = $state_repo->findAllWithAreasAndCinemas();
    $states_select = [];

    foreach ($states as $state) {
      $areas = [];

      foreach ($state->getAreas() as $area) {
        $areas[$area->getId()] = $area->getName();
      }

      $states_select[$state->getName()] = $areas;
    }

    unset($repo, $cinemas, $cinema);

    // family members
    $family = $family = [
      'father'   => 'Padre',
      'mother'   => 'Madre',
      'partner'  => 'Conyuge',
      'brother1' => 'Hermano(s)',
      'brother2' => NULL,
      'child1'   => 'Hijo(s)',
      'child2'   => NULL
    ];

    // number of previous jobs
    $jobs_loop = 3;

    /**
     * GET THE FORM!
     */
    $form = $this->createForm(
        new \SocialSnack\FrontBundle\Form\Type\JobsType(),
        NULL,
        array(
            'years'     => $years,
            'choices'   => $choices,
            'family'    => $family,
            'jobs_loop' => $jobs_loop,
            'states'    => $states_select,
            'cafes'     => $cafes
        )
    );
    $form->handleRequest($request);

    $recived_captcha  = $request->get('captcha');
    $expected_captcha = $request->getSession()->get('captcha');

    if ($form->isSubmitted()) :
      if ($form->isValid() AND ($recived_captcha === $expected_captcha)) :
        $data = $form->getData();

        /**
         * Sends the email only if cinema has email address.
         */
        $this->get('cinemex.mailer')->enqueue(array(
          'type' => 'jobapplication',
          'data' => [
            'data'      => $data,
            'form'      => $form,
            'family'    => $family,
            'jobs_loop' => $jobs_loop
          ]
        ));

        $response = $this->render(
          'SocialSnackFrontBundle:JobApplication:success.html.php', array(
            'route'       => $this->getRequest()->attributes->get('_route'),
            'area_type'   => $data['area_type'],
            'venue'       => 'cafes' === $data['area_type'] ? $cafes[$data['area_cafe']] : $choices[$data['cinema']],
          )
        );

        return $response;
      else :
        $error = new FormError('Los datos ingresados no son válidos. Por favor, asegúrate de completar
          todos los campos requeridos e ingresar un código de verificación válido.');

        $form->get('captcha')->addError($error);
      endif;
    endif;

    $response = $this->render('SocialSnackFrontBundle:JobApplication:form.html.php', array(
      'route'     => $this->getRequest()->attributes->get('_route'),
      'form'      => $form->createView(),
      //'errors'    => $errors,
      'family'    => $family,
      'jobs_loop' => $jobs_loop
    ));

    $response->setPublic();
    $response->setSharedMaxAge(60 * 60 * 24);
    $response->setMaxAge(60 * 15);
    return $response;
  }


  protected function findAvailableDatesBy($area_type, $area_id) {
    if (!in_array($area_type, ['area', 'state'])) {
      throw new \Exception('Invalid area type.');
    }

    /** @var \Memcached $mem */
    $mem = $this->get('memcached');
    $mem_key = 'findAvailableDatesBy' . $area_type . ':' . $area_id;
    $dates = $mem->get($mem_key);

    if (!$dates || $mem->getResultCode() === \Memcached::RES_NOTFOUND) {
      $conn = $this->getDoctrine()->getConnection();
      $stmt = $conn->prepare(
          "SELECT DISTINCT date FROM DatesByArea WHERE {$area_type}_id = :area_id ORDER BY date ASC"
      );
      $stmt->bindValue('area_id', $area_id);
      $stmt->execute();
      $dates = $stmt->fetchAll();

      $mem->set($mem_key, $dates, 60 * 60);
    }

    foreach ($dates as &$date) {
      $date['date'] = new \DateTime($date['date']);
    }

    return $dates;
  }

}
