<?php
/** @var \SocialSnack\RestBundle\Entity\App $client */

$title = 'Obtener token en <strong>' . $client->getName() . '</strong>';
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', strip_tags($title) . ' - Cinemex');
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?php echo $title; ?>
        </header>

        <div class="panel-body">
          <?php if ($token) { ?>
            <div class="alert alert-<?php echo (strpos($token, 'BAD') !== FALSE || strpos($token, 'INVALID') !== FALSE) ? 'danger' : 'success'; ?>" role="alert"><?php echo $token; ?></div>
          <?php } ?>
          <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
          <?php echo $view['form']->form($form); ?>
        </div>
      </section>
    </div>
  </div>
<?php $view['slots']->stop(); ?>