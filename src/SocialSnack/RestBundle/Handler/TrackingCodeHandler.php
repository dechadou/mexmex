<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\RestBundle\Entity\TrackingCode;

class TrackingCodeHandler {

  protected $doctrine;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  /**
   * Returns the IDs of the groups which has this tracking code assigned.
   *
   * @param TrackingCode $code The Tracking Code.
   * @return array Group IDs.
   */
  public function getAssignedGroups(TrackingCode $code) {
    $conn = $this->doctrine->getConnection();
    $query = "SELECT group_id FROM trackingcode_moviegroup WHERE code_id = :code_id";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('code_id', $code->getId());
    $stmt->execute();
    return array_map('current', $stmt->fetchAll());
  }


  /**
   * Returns the Tracking Codes assigned to a group.
   *
   * @param $group_id string Group ID.
   * @return array Tracking Codes.
   */
  public function getGroupCodes($group_id) {
    $conn = $this->doctrine->getConnection();
    $query = "SELECT code_id FROM trackingcode_moviegroup WHERE group_id = :group_id";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('group_id', $group_id);
    $stmt->execute();
    $code_ids = array_map('current', $stmt->fetchAll());

    if (!sizeof($code_ids)) {
      return [];
    }

    $qb = $this->doctrine->getRepository('SocialSnackRestBundle:TrackingCode')
        ->createQueryBuilder('t');

    $qb->select('t')
        ->where($qb->expr()->in('t.id', $code_ids));

    $q = $qb->getQuery();
    $q->useResultCache(TRUE, 60 * 60, __METHOD__ . '::' . $group_id);

    return $q->getResult();
  }


  public function removeCodeAssociations(TrackingCode $code) {
    $conn = $this->doctrine->getConnection();
    $query = "DELETE FROM trackingcode_moviegroup WHERE code_id = :code_id";
    $stmt = $conn->prepare($query);
    $stmt->bindValue('code_id', $code->getId());
    $stmt->execute();
  }

}