<span class="location-popup-header"></span>

<p class="popup-title">Ayúdanos a encontrar las películas y horarios ideales para ti.</p>

<form id="location-form-side">
  <a href="#" class="btn btn-default btn100" rel="ask-location">Ingresar mi ubicación</a>
  
	<p class="popup-small">
		Usaremos tu ubicación para ofrecerte las mejores películas en los Cinemex más cercanos.
	</p>
</form><!-- #location-form -->