server "108.168.135.11", :app, :web, :primary => true
set :clear_controllers, false
set :parameters_file, "parameters_staging.yml"
#set :use_set_permissions, true
set :git_shallow_clone, 0
set :branch, "no-starwars"
