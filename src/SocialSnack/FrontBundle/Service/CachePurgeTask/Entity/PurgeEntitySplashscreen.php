<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntitySplashscreen extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('rest_splashscreen_default'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_splashscreen_single', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_splashscreen_default'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('rest_v1_splashscreen_single', ['id' => $this->entity->getId()]),
            'env'  => 'front'
        ],
    ];
  }

  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\SplashscreenRepository::findActive',
            ],
            'env' => 'front'
        ]
    ];
  }

}