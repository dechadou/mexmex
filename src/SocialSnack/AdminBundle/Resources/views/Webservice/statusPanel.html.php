<?php $view->extend('SocialSnackAdminBundle::base.html.php'); ?>
<?php $view['slots']->set('title', 'Estado de los servicios'); ?>
<?php $view['slots']->start('body'); ?>
<table class="table table-striped" id="status-table">
</table>
<script>
  var $table = $('#status-table');
  var checkInterval = 30;

  function queryStatus() {
    $.ajax({
      url : '<?php echo $view['router']->generate('socialsnack_admin_ws_statusData'); ?>',
      success : function(res) {
        var $tbody = $('<tbody/>');

        for (var key in res) {
          $('<tr><td>' + key + '</td><td>' + res[key] + '</td></tr>').appendTo($tbody);
        }

        $table.children().remove();
        $tbody.appendTo($table);

        setTimeout(queryStatus, checkInterval * 1000);
      }
    });
  }
  jQuery('document').ready(function() {
    queryStatus();
  });
</script>
<?php $view['slots']->stop(); ?>