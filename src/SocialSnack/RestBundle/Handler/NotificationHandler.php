<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Notification;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

/**
 * Class NotificationHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Rodrigo Catalano
 */
class NotificationHandler {

  protected $doctrine;


  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  public function getAll() {
    $repo = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Notification' );
    $result = $repo->getList(NULL, 30);
    return $result;
  }


  public function get($id) {
    $repo = $this->doctrine->getRepository('SocialSnackFrontBundle:Notification');
    return $repo->findOneBy(array('id' => $id));
  }


  public function serialize($items) {
    if ($items instanceof Notification) {
      return $this->serializeOne($items);
    }

    $result = [];
    foreach ($items as $item) {
      $result[] = $this->serializeOne($item);
    }

    return $result;
  }

  protected function serializeOne(Notification $notification) {
    return array(
        'id' => $notification->getId(),
        'date' => $notification->getDate()->format('c'),
        'title' => $notification->getTitle(),
        'content' => $notification->getContent(),
        'deep_link' => $notification->getDeeplink(),
    );
  }

} 