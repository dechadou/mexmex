<?php

namespace SocialSnack\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PromoAType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('title', 'text', array(
            'label' => 'Título',
        ))
        ->add('url', 'text', array(
            'label' => 'URL',
        ))
        ->add('linkTarget', 'choice', array(
            'label'   => 'Abrir en',
            'attr'    => ['class' => 'form-control'],
            'choices' => [
                '_self'  => 'En la misma ventana',
                '_blank' => 'En una nueva ventana',
            ],
        ))
        ->add('sequence', 'text', array(
            'label' => 'Orden',
        ))
        ->add('image', 'file', array(
            'mapped'   => FALSE,
            'required' => $builder->getData()->getId() === NULL,
            'label'    => 'Imagen',
            'image_preview_path' => 'image',
            'image_preview_size' => '1020x129',
            'constraints' => array(
                new Assert\Image(),
            ),
        ))
        ->add('status', 'checkbox', array(
            'required' => FALSE,
            'label'    => 'Activo',
        ))
    ;
  }

  public function getName() {
    return '';
  }

}