<?php $view->extend('SocialSnackFrontBundle::skeleton.html.php'); ?>
<?php $view['slots']->start('base'); ?>

<div id="sponsored-bg"></div>

<?php
echo $this['actions']->render(
    $view['router']->generate('esi_header', array()),
    array('strategy' => 'esi')
);
?>

<div class="wrapper" id="main-body">
  <?php $view['slots']->output('body'); ?>
</div><!-- #main-body -->

<div id="page-bottom">
  <div class="wrapper">

    <!-- Promos destacadas -->
    <?php if ( !$view['slots']->has('esi_featured_promos') ) { ?>
      <?php
      echo $view['actions']->render(
          $view['router']->generate('esi_important_promos', array('target' => 'home')),
          array('strategy' => 'esi')
      );
      ?>
    <?php } else { $view['slots']->output('esi_featured_promos'); } ?>

    <div class="clearfix row box-row">
      <?php
      echo $view['actions']->render(
          $view['router']->generate('esi_news_footer', array()),
          array('strategy' => 'esi')
      );
      ?>
      <?php
      echo $view['actions']->render(
          $view['router']->generate('esi_social_footer', array()),
          array('strategy' => 'esi')
      );
      ?>
    </div>

  </div>
</div><!-- #page-bottom -->

<footer id="site-footer">
  <div class="wrapper">
    <div id="footer-cols">
      <div id="footer-phones" class="footer-col">
        <span class="logo-footer logo-cinemex-phone">
        Línea Cinemex.
        5257-6969.
        01800-7108888.
        </span>
      </div>

      <div id="footer-internals" class="footer-col">
        <a href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/footer-phone.png'); ?>" alt="" /></a>
        <a href="<?php echo $view['router']->generate('landing/cartelerasms'); ?>" target="_blank"><img src="<?php echo $view['assets']->getUrl('assets/img/footer-sms.png'); ?>" alt="" /></a>
      </div>

      <div class="footer-col">
        <a href="http://cineguru.com.mx/" target="_blank" class="logo-footer logo-cineguru">CineGuru</a>
        <a href="http://www.fundaciongrupomexico.com/" target="_blank" class="logo-footer logo-conciencia">Conciencia Cinemex</a>
        <a href="http://www.canacine.org.mx/" target="_blank" class="logo-footer logo-canacine">Canacine</a>
      </div>

      <div id="footer-social-links" class="footer-col">
        <ul>
          <li><a href="http://www.facebook.com/pages/Cinemex/83988183288" target="_blank" class="discicon icon-light icon-facebook">Facebook</a></li>
          <li><a href="http://twitter.com/cinemex" target="_blank" class="discicon icon-light icon-twitter">Twitter</a></li>
          <li><a href="http://instagram.com/cinemex" target="_blank" class="discicon icon-light icon-instagram">Instagram</a></li>
          <li><a href="https://plus.google.com/u/0/118176719987899899686/posts" target="_blank" class="discicon icon-light icon-gplus">Google+</a></li>
        </ul>
      </div>

      <div id="footer-links" class="footer-col">
        <ul>
          <li><a href="<?php echo $view['router']->generate('about_page'); ?>">Nosotros</a></li>
          <li><a href="http://webportal.edicomgroup.com/customers/cinemex/consulta-ticket-cfdi-cinemex.html" target="_blank">Factura Electrónica</a></li>
          <li><a href="<?php echo $view['router']->generate('corpsales_index'); ?>">Ventas Corporativas</a></li>
          <li><a href="http://cinemex.bumeran.com.mx/" target="_blank">Bolsa de trabajo corporativo</a></li>
          <li><a href="<?php echo $view['router']->generate('tos_page'); ?>">Términos y condiciones</a></li>
          <li><a href="<?php echo $view['router']->generate('privacy_index'); ?>" title="Aviso de Privacidad">Aviso de Privacidad</a></li>
          <li class="featured"><a href="<?php echo $view['router']->generate('jobs_index'); ?>">Bolsa de trabajo cines</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div id="footer-bottom">
    <div class="wrapper">
      <div class="col">DERECHOS RESERVADOS &copy; CADENA MEXICANA DE EXHIBICIÓN S.A. DE C.V. 2013.
        SITIO DESARROLLADO POR <a href="http://socialsnack.com" target="_blank">SOCIALSNACK.COM</a>
      </div>

      <div class="col col-buttons">
        Descarga nuestra app
        <a href="https://play.google.com/store/apps/details?id=com.cinemex&amp;hl=es" target="_blank" title="Descarga la aplicación para Android en Google Play" class="logo-google-play logo-footer">Google Play</a>
        <a href="https://itunes.apple.com/mx/app/cinemex/id418163740" target="_blank" title="Descarga la aplicación para iPhone en iTunes" class="logo-itunes logo-footer">iTunes</a>
        <a href="https://www.microsoft.com/en-us/store/apps/cinemex/9nblggh375cf" target="_blank" title="Descarga la aplicación para Windows Phone en Windows Store" class="logo-windows logo-footer">Windows Store</a>
      </div>

      <div id="switch-to-mobile">
        <a href="<?php echo $view['router']->generate('home'); ?>?no_redirect=1&switch=mobile">Ver versión móvil</a>
      </div>
    </div><!-- #footer-bottom -->

</footer><!-- #site-footer -->

<?php $view['slots']->stop(); ?>
