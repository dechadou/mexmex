<?php
/** @var \SocialSnack\FrontBundle\Entity\Modal $modal*/

$title = $modal->getId()
		? 'Editar modal <strong>' . $modal->getTitle() . '</strong>'
		: 'Nueva modal';
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', strip_tags($title) . ' - Cinemex');
$view['slots']->start('body');
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				<?php echo $title; ?>
			</header>

			<div class="panel-body">
				<?php if (sizeof($submit_msg)) { ?>
					<div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
				<?php } ?>

				<?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
				<?php echo $view['form']->form($form); ?>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	<?php if ('swf' === $modal->getType()) { ?>
		$('#form_file').next('figure').remove();
		$('<div id="swf_content"></div>').insertAfter('#form_file');
  	swfobject.embedSWF("<?php echo $view['fronthelper']->get_img_size($modal->getFile(), '', 'CMS'); ?>", "swf_content", "<?php echo $modal->getWidth() ?>", "<?php echo $modal->getHeight() ?>", "9.0.0");
	<?php } ?>

	$(document).ready(function(){
		$('.hidden').hide().removeClass('hidden');
		var initialValue = '<?php echo $modal->getType(); ?>'
		if (initialValue == 'image'){
			$('.form-group.file').slideDown();
			$('.form-group.file').find('label').text('Imagen');
		} else if (initialValue == 'swf'){
			$('.form-group.file').slideDown();
			$('.form-group.swf_size').slideDown();
      $('.form-group.position').slideDown();
			$('.form-group.file').find('label').text('SWF');
		}


		$('#form_type').on('change',function(e){
			var val = $(this).val();
			$('.form-group.types:visible').slideUp();
			if (val == 'image'){
				$('.form-group.file').slideDown();
				$('.form-group.file').find('label').text('Imagen');
				$('.form-group.swf_size').slideUp();
				$('.form-group.position').slideUp();
			} else if (val == 'swf'){
				$('.form-group.file').slideDown();
				$('.form-group.file').find('label').text('SWF');
				$('.form-group.swf_size').slideDown();
				$('.form-group.position').slideDown();
			}
		})

    $('input[value="cinemas_archive"]').on('click',function(){
      $(".cinemas").slideToggle(this.checked);
      $('select#form_cinema').chosen({width: "100%",search_contains:true});

    })

    if ($('input[value="cinemas_archive"]').is(':checked')){
      $(".cinemas").slideToggle(this.checked);
      $('select#form_cinema').chosen({width: "100%",search_contains:true});
    }
	});
</script>
<?php $view['slots']->stop(); ?>
