<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
$current_url = $view['router']->generate('mobile_checkout', array('session_id' => $session->getId()));

$view->extend('SocialSnackFrontBundle:Mobile:base.html.php');
$view['slots']->set( 'title', 'Checkout - Cinemex' );
$view['slots']->start( 'body' );

$poster_url = $view['fronthelper']->get_movie_poster_url($session->getMovie(), '67x99');
?>
<div class="clearfix row">

	<div class="pad1 box1">
		<div class="side-box-content checkout-summary">
			<img src="<?php echo $poster_url; ?>" alt="<?php echo $session->getMovie()->getName(); ?>" width="67" height="99" class="side-box-cover" />

			<dl class="movie-details-info" id="order-info">
				<dt>Título</dt>
				<dd><?php echo $session->getMovie()->getName(); ?></dd>
        <?php $type = FrontHelper::get_movie_attr_str($session->getMovie()->getAttr(), ', '); ?>
        <?php if ( $type ) { ?>
				<dt>Versión</dt>
				<dd><?php echo $type; ?></dd>
        <?php } ?>
				<dt>Día</dt>
				<dd><?php echo FrontHelper::get_session_nice_date($session->getDate()); ?></dd>
				<dt>Horario</dt>
				<dd><?php echo $session->getTime()->format( 'h:i A' ); ?></dd>
				<dt>Cine</dt>
				<dd><?php echo $session->getCinema()->getName(); ?></dd>
			</dl>
		</div>
	</div>


	<div class="wide-main-col col">

    <?php if ( !$user ) { ?>
    <div class="box1 align-center">
      ¿Eres usuario registrado de Cinemex?
      <br/>
      <a href="<?php echo $view['router']->generate('mobile_login'); ?>?redirect_to=<?php echo $current_url; ?>" class="btn btn-default btn-icon icon icon-user btn100" style="margin-top: 6px;">Ingresa con tu usuario</a>
    </div>
    <?php } else { ?>
    <div class="box1">
      <div class="user-box">
          <figure id="header-user-avatar" class="avatar-48">
            <img src="<?php echo $view['fronthelper']->get_avatar_url( $user->getAvatar(), '64x64' ); ?>" alt="<?php echo $user->getFullName(); ?>" />
          </figure>
        <div>

          Estás logueado como <strong><?php echo $user->getFullName(); ?></strong>.
          <?php $iecode = $user->getIecode();?>
          <!--<br/>Tu número de Invitado Especial es <strong><?php echo $iecode; ?></strong>.-->
          <p class="small">
            <a href="<?php echo $view['router']->generate('mobile_login'); ?>?force-logout=1&redirect_to=<?php echo urlencode($current_url); ?>">Ingresar con otro usuario</a>
            |
            <a href="<?php echo $view['router']->generate('logout'); ?>?redirect_to=<?php echo urlencode($current_url); ?>">Cerrar sesión</a>
          </p>
        </div>
      </div>
    </div>
    <?php } ?>

    <div id="checkout-process">
      <?php
      $steps = array(
          'Seleccionar boletos',
          'Seleccionar asientos',
          'Detalles de pago',
          'Compra finalizada',
      );
      $seats_allocation = ( 'Y' == $session->getData( 'SEATALLOCATIONON' ) );
      if ( !$seats_allocation ) {
        array_splice( $steps, 1, 1 );
      }
      ?>

      <div class="checkout-steps">
        <div class="checkout-step content-box selected" id="checkout-step-1">
          <h2 class="checkout-step-title pad1 box1"><span>1</span> Seleccionar boletos</h2>

          <form action="" method="post" id="checkout-step-1-form" class="checkout-form" data-step="1">
            <input type="hidden" name="session_id" value="<?php echo $session->getId(); ?>" />

            <div class="qty-selection mar1">
              <?php $i = 0; foreach ( $session->getTickets() as $ticket ) { ?>
              <div class="qty-row">
                <label>
                  <span class="ticket-label"><?php echo $ticket->getDescription(); ?></span>
                  <span class="ticket-sep">|</span>
                  <span class="ticket-price"><?php echo FrontHelper::format_price( $ticket->getPrice() ); ?> c/u</span>
                </label>
                <input type="hidden" name="tickets[<?php echo $i; ?>][type]" value="<?php echo $ticket->getData( 'TICKETTYPECODE' ); ?>" class="ticket-type" />
                <input type="number" name="tickets[<?php echo $i; ?>][qty]" value="0" data-price="<?php echo $ticket->getPrice(); ?>" class="qty-q input-number-ctl" max="10" min="0" />
              </div>
              <?php $i++; } ?>
              <div class="qty-total-row">
                Total a pagar
                <span class="currency">$</span>
                <span class="number">0</span>
              </div>
            </div>

            <div class="btn-row">
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-1-form -->
        </div>

        <div id="checkout-timeout">
          <span class="small">Tiempo restante para<br> finalizar la compra:</span>
          <div id="timeout" class="countdown">00<span class="sep">:</span>00</div>
        </div>

        <?php if ( $seats_allocation ) { ?>
        <div class="checkout-step content-box" id="checkout-step-2">
          <h2 class="checkout-step-title box1"><span>2</span> Seleccionar asientos</h2>

          <form action="" method="post" id="checkout-step-2-form" class="checkout-form" data-step="2">

            <input type="hidden" name="session_id"     value="<?php echo $session->getId(); ?>" />
            <input type="hidden" name="transaction_id" value="" />

            <div class="box1">
              <div class="posrel">
                <div id="theater-layout">
                  <table id="seats-layout">
                  </table>
                </div>
              </div>
<!--              <div id="theater-layout">
                <table id="seats-layout">
                <tbody><tr><td class="row-name first">A</td><td class="seat-11-23" title=""><span class="seat seat-E"></span></td><td class="seat-11-22" title=""><span class="seat seat-E"></span></td><td class="seat-11-21" title=""><span class="seat seat-E"></span></td><td class="seat-11-20" title=""><span class="seat seat-E"></span></td><td class="seat-11-19" title=""><span class="seat seat-E"></span></td><td class="seat-11-18" title="Fila A, asiento 18"><span class="seat seat-1"></span></td><td class="seat-11-17" title="Fila A, asiento 17"><span class="seat seat-1"></span></td><td class="seat-11-16" title="Fila A, asiento 16"><span class="seat seat-1"></span></td><td class="seat-11-15" title="Fila A, asiento 15"><span class="seat seat-0"></span></td><td class="seat-11-14" title="Fila A, asiento 14"><span class="seat seat-0"></span></td><td class="seat-11-13" title="Fila A, asiento 13"><span class="seat seat-0"></span></td><td class="seat-11-12" title="Fila A, asiento 12"><span class="seat seat-0"></span></td><td class="seat-11-11" title="Fila A, asiento 11"><span class="seat seat-0"></span></td><td class="seat-11-10" title="Fila A, asiento 10"><span class="seat seat-0"></span></td><td class="seat-11-9" title="Fila A, asiento 9"><span class="seat seat-0"></span></td><td class="seat-11-8" title="Fila A, asiento 8"><span class="seat seat-0"></span></td><td class="seat-11-7" title="Fila A, asiento 7"><span class="seat seat-0"></span></td><td class="seat-11-6" title="Fila A, asiento 6"><span class="seat seat-0"></span></td><td class="seat-11-5" title="Fila A, asiento 5"><span class="seat seat-0"></span></td><td class="seat-11-4" title="Fila A, asiento 4"><span class="seat seat-0"></span></td><td class="seat-11-3" title="Fila A, asiento 3"><span class="seat seat-selected"></span></td><td class="seat-11-2" title=""><span class="seat seat-E"></span></td><td class="seat-11-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">A</td></tr><tr><td class="row-name first">B</td><td class="seat-10-23" title=""><span class="seat seat-E"></span></td><td class="seat-10-22" title=""><span class="seat seat-E"></span></td><td class="seat-10-21" title=""><span class="seat seat-E"></span></td><td class="seat-10-20" title=""><span class="seat seat-E"></span></td><td class="seat-10-19" title=""><span class="seat seat-E"></span></td><td class="seat-10-18" title="Fila B, asiento 18"><span class="seat seat-0"></span></td><td class="seat-10-17" title="Fila B, asiento 17"><span class="seat seat-0"></span></td><td class="seat-10-16" title="Fila B, asiento 16"><span class="seat seat-0"></span></td><td class="seat-10-15" title="Fila B, asiento 15"><span class="seat seat-0"></span></td><td class="seat-10-14" title="Fila B, asiento 14"><span class="seat seat-0"></span></td><td class="seat-10-13" title="Fila B, asiento 13"><span class="seat seat-0"></span></td><td class="seat-10-12" title="Fila B, asiento 12"><span class="seat seat-0"></span></td><td class="seat-10-11" title="Fila B, asiento 11"><span class="seat seat-0"></span></td><td class="seat-10-10" title="Fila B, asiento 10"><span class="seat seat-0"></span></td><td class="seat-10-9" title="Fila B, asiento 9"><span class="seat seat-0"></span></td><td class="seat-10-8" title="Fila B, asiento 8"><span class="seat seat-0"></span></td><td class="seat-10-7" title="Fila B, asiento 7"><span class="seat seat-0"></span></td><td class="seat-10-6" title="Fila B, asiento 6"><span class="seat seat-0"></span></td><td class="seat-10-5" title="Fila B, asiento 5"><span class="seat seat-0"></span></td><td class="seat-10-4" title="Fila B, asiento 4"><span class="seat seat-0"></span></td><td class="seat-10-3" title="Fila B, asiento 3"><span class="seat seat-0"></span></td><td class="seat-10-2" title=""><span class="seat seat-E"></span></td><td class="seat-10-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">B</td></tr><tr><td class="row-name first">C</td><td class="seat-9-23" title=""><span class="seat seat-E"></span></td><td class="seat-9-22" title=""><span class="seat seat-E"></span></td><td class="seat-9-21" title=""><span class="seat seat-E"></span></td><td class="seat-9-20" title=""><span class="seat seat-E"></span></td><td class="seat-9-19" title=""><span class="seat seat-E"></span></td><td class="seat-9-18" title="Fila C, asiento 18"><span class="seat seat-0"></span></td><td class="seat-9-17" title="Fila C, asiento 17"><span class="seat seat-0"></span></td><td class="seat-9-16" title="Fila C, asiento 16"><span class="seat seat-0"></span></td><td class="seat-9-15" title="Fila C, asiento 15"><span class="seat seat-0"></span></td><td class="seat-9-14" title="Fila C, asiento 14"><span class="seat seat-0"></span></td><td class="seat-9-13" title="Fila C, asiento 13"><span class="seat seat-0"></span></td><td class="seat-9-12" title="Fila C, asiento 12"><span class="seat seat-0"></span></td><td class="seat-9-11" title="Fila C, asiento 11"><span class="seat seat-0"></span></td><td class="seat-9-10" title="Fila C, asiento 10"><span class="seat seat-0"></span></td><td class="seat-9-9" title="Fila C, asiento 9"><span class="seat seat-0"></span></td><td class="seat-9-8" title="Fila C, asiento 8"><span class="seat seat-0"></span></td><td class="seat-9-7" title="Fila C, asiento 7"><span class="seat seat-0"></span></td><td class="seat-9-6" title="Fila C, asiento 6"><span class="seat seat-0"></span></td><td class="seat-9-5" title="Fila C, asiento 5"><span class="seat seat-0"></span></td><td class="seat-9-4" title="Fila C, asiento 4"><span class="seat seat-0"></span></td><td class="seat-9-3" title="Fila C, asiento 3"><span class="seat seat-0"></span></td><td class="seat-9-2" title=""><span class="seat seat-E"></span></td><td class="seat-9-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">C</td></tr><tr><td class="row-name first">D</td><td class="seat-8-23" title=""><span class="seat seat-E"></span></td><td class="seat-8-22" title=""><span class="seat seat-E"></span></td><td class="seat-8-21" title=""><span class="seat seat-E"></span></td><td class="seat-8-20" title=""><span class="seat seat-E"></span></td><td class="seat-8-19" title=""><span class="seat seat-E"></span></td><td class="seat-8-18" title="Fila D, asiento 18"><span class="seat seat-0"></span></td><td class="seat-8-17" title="Fila D, asiento 17"><span class="seat seat-0"></span></td><td class="seat-8-16" title="Fila D, asiento 16"><span class="seat seat-0"></span></td><td class="seat-8-15" title="Fila D, asiento 15"><span class="seat seat-0"></span></td><td class="seat-8-14" title="Fila D, asiento 14"><span class="seat seat-0"></span></td><td class="seat-8-13" title="Fila D, asiento 13"><span class="seat seat-0"></span></td><td class="seat-8-12" title="Fila D, asiento 12"><span class="seat seat-0"></span></td><td class="seat-8-11" title="Fila D, asiento 11"><span class="seat seat-0"></span></td><td class="seat-8-10" title="Fila D, asiento 10"><span class="seat seat-0"></span></td><td class="seat-8-9" title="Fila D, asiento 9"><span class="seat seat-0"></span></td><td class="seat-8-8" title="Fila D, asiento 8"><span class="seat seat-0"></span></td><td class="seat-8-7" title="Fila D, asiento 7"><span class="seat seat-0"></span></td><td class="seat-8-6" title="Fila D, asiento 6"><span class="seat seat-0"></span></td><td class="seat-8-5" title="Fila D, asiento 5"><span class="seat seat-0"></span></td><td class="seat-8-4" title="Fila D, asiento 4"><span class="seat seat-0"></span></td><td class="seat-8-3" title="Fila D, asiento 3"><span class="seat seat-0"></span></td><td class="seat-8-2" title=""><span class="seat seat-E"></span></td><td class="seat-8-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">D</td></tr><tr><td class="row-name first">E</td><td class="seat-7-23" title=""><span class="seat seat-E"></span></td><td class="seat-7-22" title=""><span class="seat seat-E"></span></td><td class="seat-7-21" title=""><span class="seat seat-E"></span></td><td class="seat-7-20" title=""><span class="seat seat-E"></span></td><td class="seat-7-19" title=""><span class="seat seat-E"></span></td><td class="seat-7-18" title="Fila E, asiento 18"><span class="seat seat-0"></span></td><td class="seat-7-17" title="Fila E, asiento 17"><span class="seat seat-0"></span></td><td class="seat-7-16" title="Fila E, asiento 16"><span class="seat seat-0"></span></td><td class="seat-7-15" title="Fila E, asiento 15"><span class="seat seat-0"></span></td><td class="seat-7-14" title="Fila E, asiento 14"><span class="seat seat-0"></span></td><td class="seat-7-13" title="Fila E, asiento 13"><span class="seat seat-0"></span></td><td class="seat-7-12" title="Fila E, asiento 12"><span class="seat seat-0"></span></td><td class="seat-7-11" title="Fila E, asiento 11"><span class="seat seat-0"></span></td><td class="seat-7-10" title="Fila E, asiento 10"><span class="seat seat-0"></span></td><td class="seat-7-9" title="Fila E, asiento 9"><span class="seat seat-0"></span></td><td class="seat-7-8" title="Fila E, asiento 8"><span class="seat seat-0"></span></td><td class="seat-7-7" title="Fila E, asiento 7"><span class="seat seat-0"></span></td><td class="seat-7-6" title="Fila E, asiento 6"><span class="seat seat-0"></span></td><td class="seat-7-5" title="Fila E, asiento 5"><span class="seat seat-0"></span></td><td class="seat-7-4" title="Fila E, asiento 4"><span class="seat seat-0"></span></td><td class="seat-7-3" title="Fila E, asiento 3"><span class="seat seat-0"></span></td><td class="seat-7-2" title=""><span class="seat seat-E"></span></td><td class="seat-7-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">E</td></tr><tr><td class="row-name first">F</td><td class="seat-6-23" title=""><span class="seat seat-E"></span></td><td class="seat-6-22" title=""><span class="seat seat-E"></span></td><td class="seat-6-21" title=""><span class="seat seat-E"></span></td><td class="seat-6-20" title=""><span class="seat seat-E"></span></td><td class="seat-6-19" title=""><span class="seat seat-E"></span></td><td class="seat-6-18" title="Fila F, asiento 18"><span class="seat seat-0"></span></td><td class="seat-6-17" title="Fila F, asiento 17"><span class="seat seat-0"></span></td><td class="seat-6-16" title="Fila F, asiento 16"><span class="seat seat-0"></span></td><td class="seat-6-15" title="Fila F, asiento 15"><span class="seat seat-0"></span></td><td class="seat-6-14" title="Fila F, asiento 14"><span class="seat seat-0"></span></td><td class="seat-6-13" title="Fila F, asiento 13"><span class="seat seat-0"></span></td><td class="seat-6-12" title="Fila F, asiento 12"><span class="seat seat-0"></span></td><td class="seat-6-11" title="Fila F, asiento 11"><span class="seat seat-0"></span></td><td class="seat-6-10" title="Fila F, asiento 10"><span class="seat seat-0"></span></td><td class="seat-6-9" title="Fila F, asiento 9"><span class="seat seat-0"></span></td><td class="seat-6-8" title="Fila F, asiento 8"><span class="seat seat-0"></span></td><td class="seat-6-7" title="Fila F, asiento 7"><span class="seat seat-0"></span></td><td class="seat-6-6" title="Fila F, asiento 6"><span class="seat seat-0"></span></td><td class="seat-6-5" title="Fila F, asiento 5"><span class="seat seat-0"></span></td><td class="seat-6-4" title="Fila F, asiento 4"><span class="seat seat-0"></span></td><td class="seat-6-3" title="Fila F, asiento 3"><span class="seat seat-0"></span></td><td class="seat-6-2" title=""><span class="seat seat-E"></span></td><td class="seat-6-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">F</td></tr><tr><td class="row-name first">G</td><td class="seat-5-23" title=""><span class="seat seat-E"></span></td><td class="seat-5-22" title=""><span class="seat seat-E"></span></td><td class="seat-5-21" title=""><span class="seat seat-E"></span></td><td class="seat-5-20" title=""><span class="seat seat-E"></span></td><td class="seat-5-19" title=""><span class="seat seat-E"></span></td><td class="seat-5-18" title="Fila G, asiento 18"><span class="seat seat-0"></span></td><td class="seat-5-17" title="Fila G, asiento 17"><span class="seat seat-0"></span></td><td class="seat-5-16" title="Fila G, asiento 16"><span class="seat seat-0"></span></td><td class="seat-5-15" title="Fila G, asiento 15"><span class="seat seat-0"></span></td><td class="seat-5-14" title="Fila G, asiento 14"><span class="seat seat-0"></span></td><td class="seat-5-13" title="Fila G, asiento 13"><span class="seat seat-0"></span></td><td class="seat-5-12" title="Fila G, asiento 12"><span class="seat seat-0"></span></td><td class="seat-5-11" title="Fila G, asiento 11"><span class="seat seat-0"></span></td><td class="seat-5-10" title="Fila G, asiento 10"><span class="seat seat-0"></span></td><td class="seat-5-9" title="Fila G, asiento 9"><span class="seat seat-0"></span></td><td class="seat-5-8" title="Fila G, asiento 8"><span class="seat seat-0"></span></td><td class="seat-5-7" title="Fila G, asiento 7"><span class="seat seat-0"></span></td><td class="seat-5-6" title="Fila G, asiento 6"><span class="seat seat-0"></span></td><td class="seat-5-5" title="Fila G, asiento 5"><span class="seat seat-0"></span></td><td class="seat-5-4" title="Fila G, asiento 4"><span class="seat seat-0"></span></td><td class="seat-5-3" title="Fila G, asiento 3"><span class="seat seat-0"></span></td><td class="seat-5-2" title=""><span class="seat seat-E"></span></td><td class="seat-5-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">G</td></tr><tr><td class="row-name first">H</td><td class="seat-4-23" title=""><span class="seat seat-E"></span></td><td class="seat-4-22" title=""><span class="seat seat-E"></span></td><td class="seat-4-21" title="Fila H, asiento 21"><span class="seat seat-0"></span></td><td class="seat-4-20" title="Fila H, asiento 20"><span class="seat seat-0"></span></td><td class="seat-4-19" title=""><span class="seat seat-E"></span></td><td class="seat-4-18" title="Fila H, asiento 18"><span class="seat seat-0"></span></td><td class="seat-4-17" title="Fila H, asiento 17"><span class="seat seat-0"></span></td><td class="seat-4-16" title="Fila H, asiento 16"><span class="seat seat-0"></span></td><td class="seat-4-15" title="Fila H, asiento 15"><span class="seat seat-0"></span></td><td class="seat-4-14" title="Fila H, asiento 14"><span class="seat seat-0"></span></td><td class="seat-4-13" title="Fila H, asiento 13"><span class="seat seat-0"></span></td><td class="seat-4-12" title="Fila H, asiento 12"><span class="seat seat-1"></span></td><td class="seat-4-11" title="Fila H, asiento 11"><span class="seat seat-1"></span></td><td class="seat-4-10" title="Fila H, asiento 10"><span class="seat seat-0"></span></td><td class="seat-4-9" title="Fila H, asiento 9"><span class="seat seat-0"></span></td><td class="seat-4-8" title="Fila H, asiento 8"><span class="seat seat-0"></span></td><td class="seat-4-7" title="Fila H, asiento 7"><span class="seat seat-0"></span></td><td class="seat-4-6" title="Fila H, asiento 6"><span class="seat seat-0"></span></td><td class="seat-4-5" title="Fila H, asiento 5"><span class="seat seat-0"></span></td><td class="seat-4-4" title="Fila H, asiento 4"><span class="seat seat-1"></span></td><td class="seat-4-3" title="Fila H, asiento 3"><span class="seat seat-1"></span></td><td class="seat-4-2" title=""><span class="seat seat-E"></span></td><td class="seat-4-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">H</td></tr><tr><td class="row-name first">I</td><td class="seat-3-23" title=""><span class="seat seat-E"></span></td><td class="seat-3-22" title=""><span class="seat seat-E"></span></td><td class="seat-3-21" title="Fila I, asiento 21"><span class="seat seat-0"></span></td><td class="seat-3-20" title="Fila I, asiento 20"><span class="seat seat-0"></span></td><td class="seat-3-19" title=""><span class="seat seat-E"></span></td><td class="seat-3-18" title="Fila I, asiento 18"><span class="seat seat-0"></span></td><td class="seat-3-17" title="Fila I, asiento 17"><span class="seat seat-0"></span></td><td class="seat-3-16" title="Fila I, asiento 16"><span class="seat seat-0"></span></td><td class="seat-3-15" title="Fila I, asiento 15"><span class="seat seat-0"></span></td><td class="seat-3-14" title="Fila I, asiento 14"><span class="seat seat-0"></span></td><td class="seat-3-13" title="Fila I, asiento 13"><span class="seat seat-0"></span></td><td class="seat-3-12" title="Fila I, asiento 12"><span class="seat seat-0"></span></td><td class="seat-3-11" title="Fila I, asiento 11"><span class="seat seat-0"></span></td><td class="seat-3-10" title="Fila I, asiento 10"><span class="seat seat-0"></span></td><td class="seat-3-9" title="Fila I, asiento 9"><span class="seat seat-0"></span></td><td class="seat-3-8" title="Fila I, asiento 8"><span class="seat seat-0"></span></td><td class="seat-3-7" title="Fila I, asiento 7"><span class="seat seat-0"></span></td><td class="seat-3-6" title="Fila I, asiento 6"><span class="seat seat-0"></span></td><td class="seat-3-5" title="Fila I, asiento 5"><span class="seat seat-0"></span></td><td class="seat-3-4" title="Fila I, asiento 4"><span class="seat seat-0"></span></td><td class="seat-3-3" title="Fila I, asiento 3"><span class="seat seat-0"></span></td><td class="seat-3-2" title=""><span class="seat seat-E"></span></td><td class="seat-3-1" title=""><span class="seat seat-E"></span></td><td class="row-name last">I</td></tr><tr><td class="row-name first">J</td><td class="seat-2-23" title="Fila J, asiento 23"><span class="seat seat-0"></span></td><td class="seat-2-22" title="Fila J, asiento 22"><span class="seat seat-0"></span></td><td class="seat-2-21" title="Fila J, asiento 21"><span class="seat seat-0"></span></td><td class="seat-2-20" title="Fila J, asiento 20"><span class="seat seat-0"></span></td><td class="seat-2-19" title="Fila J, asiento 19"><span class="seat seat-0"></span></td><td class="seat-2-18" title="Fila J, asiento 18"><span class="seat seat-0"></span></td><td class="seat-2-17" title="Fila J, asiento 17"><span class="seat seat-0"></span></td><td class="seat-2-16" title="Fila J, asiento 16"><span class="seat seat-0"></span></td><td class="seat-2-15" title="Fila J, asiento 15"><span class="seat seat-0"></span></td><td class="seat-2-14" title="Fila J, asiento 14"><span class="seat seat-0"></span></td><td class="seat-2-13" title="Fila J, asiento 13"><span class="seat seat-0"></span></td><td class="seat-2-12" title="Fila J, asiento 12"><span class="seat seat-0"></span></td><td class="seat-2-11" title="Fila J, asiento 11"><span class="seat seat-0"></span></td><td class="seat-2-10" title="Fila J, asiento 10"><span class="seat seat-0"></span></td><td class="seat-2-9" title="Fila J, asiento 9"><span class="seat seat-0"></span></td><td class="seat-2-8" title="Fila J, asiento 8"><span class="seat seat-0"></span></td><td class="seat-2-7" title="Fila J, asiento 7"><span class="seat seat-0"></span></td><td class="seat-2-6" title="Fila J, asiento 6"><span class="seat seat-0"></span></td><td class="seat-2-5" title="Fila J, asiento 5"><span class="seat seat-0"></span></td><td class="seat-2-4" title="Fila J, asiento 4"><span class="seat seat-0"></span></td><td class="seat-2-3" title="Fila J, asiento 3"><span class="seat seat-0"></span></td><td class="seat-2-2" title="Fila J, asiento 2"><span class="seat seat-0"></span></td><td class="seat-2-1" title="Fila J, asiento 1"><span class="seat seat-0"></span></td><td class="row-name last">J</td></tr><tr><td class="row-name first"></td><td class="seat-1-23" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-22" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-21" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-20" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-19" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-18" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-17" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-16" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-15" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-14" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-13" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-12" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-11" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-10" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-9" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-8" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-7" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-6" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-5" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-4" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-3" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-2" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-1" title=""><span class="seat seat-E seat-EE"></span></td><td class="seat-1-0" title=""><span class="seat seat-E seat-EE"></span></td><td class="row-name last"></td></tr></tbody></table>
              </div>-->
            </div>

            <div class="btn-row">
              <a href="#" class="no-btn" rel="checkout-back">Volver</a>
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-2-form -->
        </div><!-- #checkout-step-2 -->
        <?php } ?>

        <div class="checkout-step content-box" id="checkout-step-3">
          <h2 class="checkout-step-title box1"><span><?php echo ( sizeof( $steps ) == 4 ) ? '3' : '2'; ?></span> Detalles de pago</h2>

          <form action="" method="post" id="checkout-step-3-form" class="checkout-form" data-step="3" autocomplete="off">

            <input type="hidden" name="session_id"     value="<?php echo $session->getId(); ?>" />
            <input type="hidden" name="transaction_id" value="" />
            <input type="hidden" name="iecode"         value="" />
            <input type="hidden" name="app_id"         value="Y2VUfRew32ZbK7IiZxsB" />
            <input type="hidden" name="ref"            value="<?php echo $ref; ?>" />

            <div class="std-form box1">

              <div class="form-row">
                <label for="checkout-name">Nombre y apellido</label>
                <input type="text" name="name" id="checkout-name" placeholder="Nombre del titular de la tarjeta" data-validators="not_empty full_name" data-flyvalidate="1" />
              </div>

              <div class="form-row">
                <label for="checkout-email">Email</label>
                <input type="email" name="email" id="checkout-email" placeholder="Ingresa tu email" data-validators="not_empty email" data-flyvalidate="1" />
              </div>

              <div class="form-row">
                <label for="checkout-iecode">Invitado Especial (opcional)</label>
                <input type="text" name="iecode" id="checkout-iecode" placeholder="Número de Invitado Especial" value="<?php if ( isset($iecode) ) { echo $iecode; } ?>"/>
              </div>

              <div class="form-row">
                <label for="checkout-cc">Número de tarjeta</label>
                <input type="text" name="cc" id="checkout-cc" placeholder="Ingresa el número de tu tarjeta de crédito" data-validators="not_empty ccnum"  data-flyvalidate="1" />
              </div>

              <div class="form-row quarter">
                <label for="checkout-csc">Código de seguridad</label>
                <input type="text" name="csc" id="checkout-csc" placeholder="" data-validators="not_empty csc"  data-flyvalidate="1" />
              </div>

              <div class="form-row quarter">
                <label for="checkout-exp-month">Fecha de vencimiento</label>
                <select name="expire-month" id="checkout-exp-month" class="month-year month" data-validators="not_empty cc_expiry_month" data-flyvalidate="1">
                  <option value="">Mes</option>
                  <?php for ( $i = 1 ; $i <= 12 ; $i++ ) { ?>
                  <option value="<?php echo $i; ?>"><?php echo str_pad( $i, 2, '0', STR_PAD_LEFT ); ?></option>
                  <?php } ?>
                </select>
                <select name="expire-year" id="checkout-exp-year" class="month-year year" data-validators="not_empty cc_expiry_year" data-flyvalidate="1">
                  <option value="">Año</option>
                  <?php for ( $i = date( 'Y' ) ; $i <= date( 'Y' ) + 15 ; $i++ ) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>

              <!--<img src="<?php echo $view['assets']->getUrl( 'assets/img/cc-icons.png'); ?>" alt="" class="checkout-cc-icons" />-->
            </div>

            <div class="btn-row">
              <a href="#" class="no-btn" rel="checkout-back">Volver</a>
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-3-form -->
          <div class="disclaimer mar1">
            <p>Cinemex Desarrollos, S.A. de C.V. (en adelante "Cinemex"), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>
            <p>Los Datos Personales y Financieros que le son solicitados, serán tratados con las finalidades primarias de comprar y confirmar los boletos que adquiere.</p>
            <p><a href="<?php echo $view['router']->generate('mobile_checkout_privacy_page'); ?>" target="_blank">Aviso de privacidad</a></p>
          </div>
        </div><!-- #checkout-step-3 -->

        <div class="checkout-step content-box" id="checkout-step-4">
          <h2 class="checkout-step-title box1"><span><?php echo ( sizeof( $steps ) == 4 ) ? '4' : '3'; ?></span> Compra finalizada</h2>

          <div id="checkout-confirmation" class="box1">
          </div>
        </div><!-- #checkout-step-4 -->
      </div>

      <div class="disclaimer mar1">
        <p>
          Los boletos son válidos únicamente para el día y horario seleccionado. No se realizarán reembolsos ni cambios de horarios una vez realizada la compra.
        </p>
      </div>

    </div><!-- #checkout-process -->

    <div class="big-msg msg-timeout box0">
      <p class="big-msg-title">Tiempo</p>
      <p class="big-msg-msg">Has superado el tiempo necesario para poder realizar su compra.</p>
      <a href="<?php echo $view['router']->generate( 'mobile_checkout', array( 'session_id' => $session->getId() ) ); ?>" class="btn btn-default btn-icon icon icon-larr">Volver</a>
    </div>

	</div>
	<script>var seatallocationallowed = <?php echo ( 'N' == $session->getData( 'SEATALLOCATIONON' ) ) ? 'false' : 'true'; ?></script>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'js_after_vendor' ); ?>
<script>
$( document ).bind( 'user_init', function() {
  if ( user && user.info && user.info.iecode ) {
    $( '#checkout-ie .ie-banner' ).remove();
    $( '#checkout-ie .heading-title .txt' ).text( '¡Ya estás sumando beneficios como Invitado Especial!' );
//    $( '#checkout-step-3-form [name=iecode]' ).val( user.info.iecode );
  }
} );
var checkout_movie_id  = '<?php echo $session->getMovie()->getId(); ?>';
var checkout_cinema_id = '<?php echo $session->getCinema()->getId(); ?>';
var checkout_area_id   = '<?php echo $session->getCinema()->getArea()->getId(); ?>';
var checkout_state_id  = '<?php echo $session->getCinema()->getState()->getId(); ?>';

$( '.input-number-ctl' ).bind( 'focus', function( e ) {
  var self = this;
  setTimeout(function() {
    self.selectionStart = 0;
    self.selectionEnd = 999;
  }, 10);
} );
</script>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('ga_extras'); ?>
<!--_gaq.push(['_setCustomVar', 1, 'Checkout source', '<?php echo $ref; ?>', 3]);-->
ga('set', 'dimension1', '<?php echo $ref; ?>');
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('ga_extras_after'); ?>
ga('require', 'ecommerce');
<?php $view['slots']->stop(); ?>
