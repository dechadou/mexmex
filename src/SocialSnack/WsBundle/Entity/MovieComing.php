<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Movie coming soon
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="moviec_filter_idx", columns={"active", "release_date", "next_week"})
 * })
 * @ORM\Entity(repositoryClass="SocialSnack\WsBundle\Entity\MovieComingRepository")
 */
class MovieComing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = 1;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="release_date", type="date")
     */
    private $release_date;
    
		/**
		 * @var string
		 * 
		 * @ORM\Column(name="poster_url", type="string", length=255)
		 */
		private $poster_url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="attributes", type="text")
     */
    private $attributes;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="next_week", type="boolean")
     */
    private $next_week;
    		
		private $decoded_attrs = null;
		private $decoded_info  = null;
		
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->type = 'coming-soon';
        $this->attributes = '{}';
        $this->poster_url = '';
        $this->next_week = FALSE;
    }
    
		/**
		 * Get data from the WS movie object.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->getInfo() );
			}
      
			if ( !isset( $this->decoded_info->{ $key } ) )
				return NULL;
			
      return $this->decoded_info->{ $key };
		}
		
		
		/**
		 * Get attribute value.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getAttr( $key = NULL ) {
      if ( is_null( $this->decoded_attrs ) ) {
				$this->decoded_attrs = json_decode( $this->getAttributes() );
			}
      
      if ( !$key )
        return $this->decoded_attrs;
      
			if ( !isset( $this->decoded_attrs->{ $key } ) )
				return NULL;
      
			return $this->decoded_attrs->{ $key };
		}
		
		public function getSlug() {
			/** @todo Move the logic into a Helper in order to make it reusable */
			$slug = $this->name;
			$slug = strtolower( $slug );
			$slug = preg_replace( '/[^\da-z]/i', '-', $slug );
			$slug = preg_replace( '/\-+/i', '-', $slug );
			return $slug;
		}
		
		public function toArray( $populate_info = TRUE ) {
			$fields = array(
					'id',
					'name',
			);
			$result = array();
			foreach ( $fields as $field ) {
				$result[ $field ] = $this->{ $field };
			}
			$result[ 'cover' ] = $this->getPosterUrl();
			$result[ 'release_date' ] = $this->release_date->format('Y-m-d');

			if ( $populate_info ) {
				$result[ 'info' ] = array(
						'sinopsis'       => $this->getData( 'SINOPSIS' ) ?: '',
						'original_title' => $this->getData( 'NOMBREORIGINAL' ) ?: '',
						'director'       => $this->getData( 'DIRECTOR' ) ?: '',
						'cast'           => $this->getData( 'ACTORES' ) ?: '',
						'country'        => $this->getData( 'PAISORIGEN' ) ?: '',
						'genre'          => (array)$this->getData( 'GENERO' ) ?: [],
						'rating'         => $this->getData( 'RATING' ) ?: '',
						'year'           => $this->getData( 'ANIO' ) ?: '',
						'duration'       => $this->getData( 'DURACION' ) ?: '',
						'trailer'        => $this->getData( 'TRAILER' ) ?: '',
				);
			}
			
			return $result;
		}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MovieComing
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return MovieComing
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return MovieComing
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return MovieComing
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set poster_url
     *
     * @param string $posterUrl
     * @return MovieComing
     */
    public function setPosterUrl($posterUrl)
    {
        $this->poster_url = $posterUrl;
    
        return $this;
    }

    /**
     * Get poster_url
     *
     * @return string 
     */
    public function getPosterUrl()
    {
        return $this->poster_url;
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     * @return MovieComing
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    
        return $this;
    }

    /**
     * Get attributes
     *
     * @return string 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set release_date
     *
     * @param \DateTime $releaseDate
     * @return MovieComing
     */
    public function setReleaseDate($releaseDate)
    {
        $this->release_date = $releaseDate;
    
        return $this;
    }

    /**
     * Get release_date
     *
     * @return \DateTime 
     */
    public function getReleaseDate()
    {
        return $this->release_date;
    }

    /**
     * Set next_week
     *
     * @param boolean $nextWeek
     * @return MovieComing
     */
    public function setNextWeek($nextWeek)
    {
        $this->next_week = $nextWeek;
    
        return $this;
    }

    /**
     * Get next_week
     *
     * @return boolean 
     */
    public function getNextWeek()
    {
        return $this->next_week;
    }
    
    
    /**
     * Hardcoded method for compatibility with Movie class.
     * 
     * @return boolean
     */
    public function getPremiere() {
      return FALSE;
    }
    
}