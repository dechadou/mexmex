<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Aviso de privacidad integro para visitantes - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Aviso de privacidad integral para protección de datos
  personales conforme al Programa de Lealtad PAYBACK</h1>

<div class="entry-body align-justify">
  <h2>1. DISPOSICIONES GENERALES</h2>

  <p>Su privacidad y confianza son muy importantes para PAYBACK MÉXICO S. de R.L.
    de C.V. Por ello queremos dar a conocer al (los) Participante(s) (en lo
    sucesivo, (el) los “Participante(s)”) del Programa de Lealtad PAYBACK (en lo
    sucesivo: el “Programa PAYBACK®” y/o el “Programa de Lealtad” y/o el “Programa
    de Lealtad PAYBACK®”) cómo salvaguardamos la integridad, privacidad y protección
    de sus datos personales, en apego a la Ley Federal de Protección de Datos
    Personales en Posesión de los Particulares y su Reglamento y las características
    esenciales de su tratamiento, para que los Participantes tengan pleno control
    y decisión sobre éstos. Por ello, le recomendamos que lea atentamente la
    siguiente información.</p>

  <h2>2. IDENTIDAD Y DOMICILIO DE LOS RESPONSABLES</h2>

  <p>Los datos personales proporcionados por Usted para su inscripción dentro del
    Programa de Lealtad PAYBACK, serán tratados por PAYBACK MÉXICO, S. de R.L. de
    C.V (”PAYBACK” y/o “PAYBACK MÉXICO”) con domicilio en Av. Patriotismo No. 635,
    Col. Ciudad de los Deportes, México, Distrito Federal, C.P. 03710 y adicionalmente
    por alguno de los siguientes socios comerciales (“Socios Principales”),que
    forman  parte del Programa PAYBACK®, cuando Usted se registre con ellos o a
    través de PAYBACK, al Programa de Lealtad y adquiera la Tarjeta de Lealtad
    emitida por alguno de ellos, y que serán:: TIENDAS COMERCIAL MEXICANA, S.A.
    de C.V.(“COMERCIAL MEXICANA” y/o “TIENDAS COMERCIAL MEXICANA”) para conocer
    su domicilio ingrese a la página www.comercialmexicana.com.mx, ABC AEROLINEAS
    S.A. de C.V. (“INTERJET”) para conocer  su domicilio ingrese a la página de
    Internet www.interjet.com.mx, INNOVACIÓN Y CONVENIENCIA, S.A. de C.V. (“PETRO 7”
    y “7 ELEVEN”) para conocer su domicilio ingrese a la página de Internet
    www.7-eleven.com.mx, COMUNICACIONES NEXTEL DE MEXICO, S.A. de C.V. (“NEXTEL”)
    para conocer su domicilio ingrese a la página www.nextel.com.mx, ALQUILADORA
    DE VEHÍCULOS AUTOMOTORES S.A. DE C.V. (“HERTZ”) para conocer su domicilio
    ingrese a la página de Internet www.hertz.com.mx, AMERICAN EXPRESS COMPANY
    (MEXICO), S.A. de C.V. (“AMERICAN EXPRESS COMPANY”) para conocer su domicilio
    ingrese a la página www.americanexpress.com/mexico, AMERICAN EXPRESS BANK
    (MEXICO), S.A., INSTITUCION DE BANCA MULTIPLE (“AMERICAN EXPRESS BANK”), para
    conocer su domicilio ingrese a la página www.americanexpress.com/mexico y
    Cinemex Desarrollos, S.A. de C.V.  (“CINEMEX”) para conocer su domicilio
    ingrese a la página www.cinemex.com.mx, (en lo sucesivo los “Responsables”).</p>

  <h2>3. FINALIDADES PRIMARIAS Y SECUNDARIAS DEL TRATAMIENTO DE LOS DATOS PERSONALES.</h2>

  <p>Los  Responsables recabarán de usted, los datos personales (“Datos Personales”)
    que sean necesarios para la operación y funcionamiento del Programa PAYBACK®
    ya sea (i) directa o personalmente o bien (ii) a través de sus agentes. Dichos
    Datos Personales consistirán en: nombre completo (incluyendo nombre(s) y
    apellidos materno y paterno), fecha de nacimiento, Registro Federal de
    Contribuyentes (RFC), Correo Electrónico, Teléfono(s) de contacto (celular
    y/o fijo), Domicilio completo (Calle, Número, Colonia, Delegación,
    Ciudad/Municipio, Estado de la República, Código Postal) y género.</p>

  <p>Los Responsables no recabarán ni tratarán Datos Personales sensibles.</p>

  <p>Los Datos Personales que nos proporcione, serán utilizados para las siguientes
    finalidades primarias, necesarias para la operación y desarrollo del Programa
    de Lealtad que son: (i) Suscripción al Programa de Lealtad; (ii) Acumular
    puntos PAYBACK (los “Puntos” y/o “Puntos PAYBACK”); (iii) Intercambiar Puntos
    por bienes y/o servicios; (iv) canjear vales/cupones por bienes y/o servicios
    que ofrezcan los Socios Principales del Programa de Lealtad y PAYBACK; (v)
    realizar cualquier tipo de promoción en relación con el Programa de Lealtad
    y la emisión de las Tarjetas del Programa de Lealtad (“Tarjeta de Lealtad”
    y/o “Monedero PAYBACK”); (vi) , y/o llevar a cabo cualesquiera otras actividades
    suplementarias o auxiliares relacionadas con la operación del Programa de
    Lealtad PAYBACK.</p>

  <p>Además, si usted no se opone, los Responsables tratarán sus datos personales
    para las siguientes finalidades secundarias: (i) realizar actividades
    promocionales de mercadotecnia, incluyendo sorteos, previa autorización de la
    Secretaría de Gobernación; (ii) enviarle a través de PAYBACK información,
    promociones de los bienes y/o servicios que ofrezcan los Socios Principales
    a través de medios de comunicación electrónicos, y (iii) llevar a cabo estudios
    de mercadotecnia, estadísticos y de análisis del mercado, incluyendo la
    preparación y la distribución de información personalizada sobre los bienes
    y/o servicios de los Socios PAYBACK presentes y futuros.</p>

  <p>Si usted no desea que se traten sus datos personales para las mencionadas
    finalidades secundarias favor de seguir los siguientes pasos: (i) ingresa a
    a página www.payback.mx, (ii) acceda a su cuenta PAYBACK, (iii) acceda a la
    sección de “Mis Datos”, (iv) seleccione la opción “Cambiar Suscripción de
    Servicio” y (v) finalmente, a su elección, deshabilite los recuadros “Cupones
    y Promociones”, “Boletín PAYBACK” y “SMS PAYBACK” y de esa manera manifiesta
    su negativa.</p>

  <p>Además, puede comunicarse con nuestro Centro de Atención Telefónica PAYBACK
    de acuerdo a lo que se señala en el apartado 6 de éste aviso, o haciendo clic
    en el enlace para darse de baja de nuestra comunicación de correo electrónico.
    Una vez que hayamos recibido su solicitud, ni PAYBACK ni sus Socios Principales
    utilizarán más sus datos personales para las finalidades secundarias</p>

  <p>Los Responsables pueden usar con los Participantes, servicios basados en
    rastreo y localización como serían las herramientas para geo-localización.
    El uso de métodos que permiten la geo-localización puede involucrar para el
    Participante el intercambio de los datos de localización, identificadores de
    la red inalámbrica junto con dispositivo único o proveedor de servicios de red,
    identificadores con un servidor de ubicación. Dependiendo de las configuraciones
    del Participante de posicionamiento y el uso de servicios de ubicación de los
    demás proveedores de servicios el dispositivo se puede conectar a otros
    servidores de proveedores de servicios, que no están controlados u operados
    por los Responsables. Recomendamos que verifique las políticas de privacidad
    de dichos proveedores de servicios para entender cómo procesan los datos de
    ubicación. Si desea desactivar esta característica, por favor consulta las
    opciones de su dispositivo móvil.</p>

  <h2>4. TRANSFERENCIAS DE DATOS PERSONALES</h2>

  <p>Los Responsables requieren transferir sus Datos Personales en la medida que
    sea necesario para la operación y desarrollo del Programa PAYBACK® dentro e
    incluso fuera de la República Mexicana en los siguientes supuestos: i) a
    cualquiera de sus sociedades: controladoras, subsidiarias, afiliadas bajo el
    control común del responsable, a una sociedad matriz o a cualquier sociedad
    del mismo grupo del Responsable que opere bajo los mismos procesos y políticas
    internas; ii) cuando se  precisa para el cumplimiento de las finalidades del
    propio Programa de Lealtad, iii) o en aquéllos casos legalmente previstos y
    por los que no se requiera autorización del titular de los Datos Personales
    (los “Terceros”).</p>

  <p>Los Responsables se comprometen a velar porque se cumplan todos los principios
    legales de protección en torno a la transferencia de sus Datos Personales. De
    igual forma, manifiestan su compromiso para que se respete en todo momento,
    tanto por PAYBACK MÉXICO como por sus Socios Principales así como por  los
    Terceros a quienes se transfieran sus Datos Personales,  el presente Aviso
    de Privacidad.</p>

  <h2>5. PROCEDIMIENTO PARA EJERCER LOS DERECHOS ARCO Y/O REVOCACIÓN DEL
    CONSENTIMIENTO.</h2>

  <p>Para el ejercicio de los derechos de acceso, rectificación, cancelación,
    oposición y revocación del consentimiento del tratamiento de los Datos
    Personales (Derechos “ARCO”) que los Participantes han proporcionado a los
    “Responsables” para poder participar en el Programa de Lealtad PAYBACK,
    éstos últimos acuerdan que podrán ser hechos valer por el participante ya sea
    por sí o mediante representante legal, debiendo identificarse o acreditarse
    correctamente, llamando al Centro de atención a clientes PAYBACK 01 800 838 35 70
    (interior de la República) ó 53 26 25 70 (Ciudad de México) con una horario de
    atención de Lunes a Sábado de 9:00 am a 9:00 pm, describiendo de manera clara
    y precisa los datos personales respecto de los que se busca ejercer alguno
    de los Derechos ARCO.</p>

  <p>En la llamada para ejercer derechos ARCO deberá proporcionar una dirección
    de correo electrónico y/o número telefónico para poderle comunicar la respuesta
    a su solicitud o poderlo contactar en caso de requerir información adicional
    en relación con su petición. Así mismo, deberá enviarnos al correo electrónico
    <a href="mailto:DERECHOSARCOPAYBACK@aexp.com">DERECHOSARCOPAYBACK@aexp.com</a> copia de su identificación oficial vigente a
    efectos de acreditar su identidad (credencial de elector, pasaporte, cartilla
    militar, cédula profesional o forma migratoria (FM-3 ó el que en su caso
    sustituya a ésta última)  y  en su caso, de actuar a través de un representante
    legal, deberá adjuntar en forma adicional, los documentos que acrediten la
    identidad y facultades de representación de éste último (copia simple en formato
    impreso o electrónico de la carta poder simple con firma autógrafa del Participante,
    del mandatario o representante  y sus correspondientes identificaciones
    oficiales vigentes: credencial de elector, pasaporte, cartilla militar, cédula
    profesional  o forma migratoria ( FM-3 ó el que en su caso sustituya a ésta última).</p>

  <p>En el caso de las solicitudes de rectificación de datos personales, el
    Participante respectivo deberá también indicar las modificaciones a realizarse
    y aportar la documentación que sustente su petición.</p>

  <p>El Responsable de la Protección de datos personales responderá al Participante
    respectivo en un plazo máximo de 20 (veinte) días hábiles, contados desde la
    fecha en que se recibió la solicitud de acceso, rectificación, cancelación u
    oposición, la determinación adoptada, a efecto de que, si resulta procedente,
    se haga efectiva la misma dentro de los 15 (quince) días naturales siguientes
    a la fecha en que se le comunique la respuesta al Participante.</p>

  <h2>6. LIMITACIÓN DE USO O DIVULGACIÓN DE LOS DATOS PERSONALES.</h2>

  <p>El Participante puede limitar el uso o divulgación de sus datos para dejar
    de recibir promociones, ofertas y publicidad respecto de los productos y/o
    servicios de los Socios PAYBACK y/o PAYBACK llamando al  Centro de atención
    a clientes PAYBACK 01 800 838 35 70 (interior de la República) ó 53 26 25 38
    (Ciudad de México).</p>

  <p>En caso de que su solicitud resulte procedente, se registrará al Participante
    en el listado de exclusión con el objeto de que usted deje de recibir nuestras
    promociones.</p>

  <h2>7. USO DE COOKIES</h2>

  <p>Los diferentes sitios de Internet y servicios en línea de PAYBACK MÉXICO se
    basan en el manejo de Cookies y/o Web Beacons que permiten que PAYBACK MÉXICO
    recabe automáticamente datos personales. Al ingresar y continuar con el uso
    del Sitio de Internet, el Participante consiente que PAYBACK MÉXICO recabe y
    trate sus datos personales. El uso de estas tecnologías es necesario por
    motivos de operación, técnicos y de seguridad. Para más información favor de
    consultar los términos y condiciones.</p>

  <h2>8. CAMBIOS AL AVISO PRIVACIDAD</h2>

  <p>Cualquier cambio sustancial o total que se realice al presente Aviso de
    Privacidad, podrá ser realizado por PAYBACK México publicándose un aviso
    informando sobre el cambio en nuestra página  www.payback.mx o mediante correo
    electrónico a la dirección del Participante que para tales efectos se tenga
    registrado. Es responsabilidad del Participante revisar el contenido actualizado
    del Aviso de Privacidad disponible en la página de Internet www.payback.mx.
    PAYBACK MÉXICO asume que si el Participante del Programa PAYBACK no se opone
    a los cambios, significa que el Participante del Programa PAYBACK ha leído,
    entendido y consentido los términos ahí establecidos. Recomendamos que visite,
    de tiempo en tiempo, la página de Internet de PAYBACK MÉXICO www.payback.mx.
    para saber de cualquier modificación a éste Aviso de Privacidad.</p>

  <p>Fecha de última actualización: 5/Enero/2015</p>
</div>
<?php $view['slots']->stop(); ?>
