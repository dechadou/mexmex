<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

if (!isset($title)) {
  $title = 'Cinemex';
}

if (!isset($section_name)) {
  $section_name = 'landing-iframe';
}

if (!isset($height)) {
  $height = '4000px';
}

$view['slots']->set('title', $title);
$view['slots']->set('body_class', 'sponsored' . $section_name);
?>

<?php $view['slots']->start('body'); ?>
<div class="section-<?php echo $section_name; ?>">
  <iframe src="<?php echo $url; ?>" onload="scroll(0, 0);" width="100%" height="<?php echo $height; ?>"></iframe>
</div>
<?php $view['slots']->stop(); ?>
