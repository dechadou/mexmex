<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Task;

class PosterRetryCommand extends ContainerAwareCommand {
	
  
	protected function configure() {
		$this
			->setName( 'ws:movies:poster_retry' )
			->setDescription( 'Retry to download the movie poster' )
      ->addOption( 'movie_id', 'mid', InputOption::VALUE_OPTIONAL );
	}
	
  
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$doctrine    = $this->getContainer()->get( 'doctrine' );
		$em          = $doctrine->getManager();
		$movie_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Movie' );
    
    if ( $movie_id = $input->getOption( 'movie_id' ) ) {
      $movies = $movie_repo->findBy( array( 'id' => $movie_id ) );
    } else {
      $movies = $movie_repo->findAll();
    }
    
    
		/* @var $movie \SocialSnack\WsBundle\Entity\Movie   */
		/* @var $task   \SocialSnack\WsBundle\Entity\Task   */
		foreach ( $movies as $movie ) {
			$output->writeln( sprintf( 'Movie %s', $movie->getName() ) );
      $this->getContainer()->get('ss.ws.utils')->process_posters( $movie, json_decode($movie->getInfo()) );
		}

    $em->flush();
    
		$output->writeln( 'Didn\'t fail, yay!' );
		return;
		
	}
	
}