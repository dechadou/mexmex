<?php

namespace SocialSnack\RestBundle\Service;

use SocialSnack\WsBundle\Entity\Session;

class Helper {


  /**
   * Validates a FB access token with a call to FB API.
   *
   * @param $access_token
   * @param $app_id
   * @param $secret_key
   * @param $app_access_token
   * @param $redirect_url
   * @return array|bool
   */
  public static function validate_access_token( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url ) {
		$url = sprintf( "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s",
				$access_token,
				$app_access_token
		);
    
		$ch  = curl_init( $url );
		/** @todo SSL should be checked. */
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		
		$me = curl_exec( $ch );
		
		if ( $me ) {
			if ( $user_data = @json_decode( $me ) ) {
				if ( isset($user_data->data) && $user_data->data->is_valid ) {
					return (array)$user_data->data;
				}
			}
		}
		
		return FALSE;
	}


  /**
   * Validates a FB access token and throws an exception if the validation fails.
   *
   * @param $access_token
   * @param $app_id
   * @param $secret_key
   * @param $app_access_token
   * @param $redirect_url
   * @return array|bool
   * @throws \Exception
   */
  public static function validate_access_tokenX( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url ) {
		$data = self::validate_access_token( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url );
		
		if ( is_null( $data ) || FALSE === $data )
			throw new \Exception();
		
		return $data;
	}


  /**
   * Parses a FB signed request string.
   *
   * @param string $signed_request
   * @param string $secret_key
   * @return mixed|null
   */
  public static function parse_signed_request( $signed_request, $secret_key ) {
		list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

		// decode the data
		$sig = self::base64_url_decode($encoded_sig);
		$data = json_decode(self::base64_url_decode($payload), true);

		// confirm the signature
		$expected_sig = hash_hmac('sha256', $payload, $secret_key, $raw = true);
		if ($sig !== $expected_sig) {
			return null;
		}

		return $data;
	}


  /**
   * Parses a FB signed request string and throws an exception if the parse fails.
   *
   * @param string $signed_request
   * @param string $secret_key
   * @return mixed|null
   * @throws \Exception
   */
  public static function parse_signed_requestX( $signed_request, $secret_key ) {
		$data = self::parse_signed_request( $signed_request, $secret_key );
		if ( is_null( $data ) )
			throw new \Exception();
		return $data;
	}


  /**
   * Validates a FB signed request with a FB API call.
   *
   * @param $signed_request
   * @param $app_id
   * @param $secret_key
   * @param $app_access_token
   * @param $redirect_url
   * @return bool|mixed|null
   */
  public static function validate_signed_request( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url ) {
		$data = self::parse_signed_request( $signed_request, $secret_key );
		
		if ( is_null( $data ) )
			return NULL;
		
		if ( !$data['code'] )
			return FALSE;
		
		$exchange_url = sprintf( "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s",
				$app_id,
				urlencode( $redirect_url ),
				$secret_key,
				$data['code']
		);
		
		$ch  = curl_init( $exchange_url );
		/** @todo SSL should be checked. */
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		$res = curl_exec( $ch );
		if ( $res ) {
			$res = parse_str( $res, $exchange_data );
		}
		
		if ( !isset($exchange_data['access_token']) )
			return FALSE;
				
		$url = sprintf( "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s",
				$exchange_data['access_token'],
				$app_access_token
		);
		$ch  = curl_init( $url );
		/** @todo SSL should be checked. */
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		
		$me = curl_exec( $ch );
		
		if ( $me ) {
			if ( $user_data = @json_decode( $me ) ) {
				if ( isset($user_data->data) && $user_data->data->is_valid ) {
					return $data;
				}
			}
		}
		
		return FALSE;
	}


  /**
   * Validates a FB signed request and throws an exception if the validation fails.
   *
   * @param $signed_request
   * @param $app_id
   * @param $secret_key
   * @param $app_access_token
   * @param $redirect_url
   * @return bool|mixed|null
   * @throws \Exception
   */
  public static function validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url ) {
		$data = self::validate_signed_request( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
		
		if ( is_null( $data ) || FALSE === $data )
			throw new \Exception();
		
		return $data;
	}

	
	public static function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}
	
	
  /**
   * Build a numbers-only hash.
   * Not intended for uniqueness.
   * 
   * @param int    $num
   * @param string $salt
   * @param int    $len
   * @return int
   */
  public static function num_hash($num, $salt = '', $len = 10) {
    return substr(hexdec( substr(sha1($salt.$num), 0, 10) ), 0, $len);
  }


  /**
   * @param Session $session
   * @return string
   */
  public static function get_session_memcached_key(Session $session) {
    return md5('tmp_session_' . $session->getId() . '-' . $session->getLegacyId());
  }


  public static function buildBookingCode($branch, $booking) {
    return 'B' . str_pad($branch, 4, ' ', STR_PAD_RIGHT) . $booking;
  }

}