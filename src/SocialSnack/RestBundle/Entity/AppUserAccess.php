<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserAccess
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="uniq_appuseraccess",columns={"app_id", "user_id"}),
 *   }
 * )
 * @ORM\Entity
 */
class AppUserAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="App")
     */
    private $app;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="SocialSnack\FrontBundle\Entity\User")
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(name="scope", type="json_array")
     */
    private $scope;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scope
     *
     * @param array $scope
     * @return AppUserAccess
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
    
        return $this;
    }

    /**
     * Get scope
     *
     * @return array 
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * Set app
     *
     * @param \SocialSnack\RestBundle\Entity\App $app
     * @return AppUserAccess
     */
    public function setApp(\SocialSnack\RestBundle\Entity\App $app = null)
    {
        $this->app = $app;
    
        return $this;
    }

    /**
     * Get app
     *
     * @return \SocialSnack\RestBundle\Entity\App 
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set user
     *
     * @param \SocialSnack\FrontBundle\Entity\User $user
     * @return AppUserAccess
     */
    public function setUser(\SocialSnack\FrontBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialSnack\FrontBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}