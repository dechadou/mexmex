<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Modales - Cinemex' );
$view['slots']->start('body');
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de Modales
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <div class="clearfix">
            <div class="btn-group">
              <button class="btn green" id="editable-sample_new" onclick="window.location.href='<?php echo $view['router']->generate('social_snack_modal_add'); ?>'">
                Nuevo modal <i class="icon-plus"></i>
              </button>
            </div>
          </div>
          <table  class="display table table-bordered table-striped" id="modal">
            <thead>
            <tr>
              <th>File</th>
              <th>Título</th>
              <th>Seccion</th>
              <th>Cine</th>
              <th>Estado</th>
              <th>Creado</th>
              <th>Editar</th>
              <th>Borrar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($modals)){
              foreach ($modals as $modal) {
                if ($modal->getType() == 'image'){
                  $file = "<img src=".$view['fronthelper']->get_img_size($modal->getFile(), '', 'CMS')." alt=". $modal->getTitle()." width='236' height='222' />";
                } else if ($modal->getType() == 'swf'){
                  $file = 'swf';
                } else {
                  $file = 'otro';
                }
                ?>
                <tr class="gradeX">
                  <td><?php echo $file ?></td>
                  <td><?php echo $modal->getTitle(); ?></td>
                  <td>
                    <ul>
                      <?php foreach($modal->getSection() as $section){ ?>
                        <li>- <?php echo $routes[$section] ?></li>
                      <?php }; ?>
                    </ul>
                  </td>
                  <td><?php
                    foreach($modal->getTargets() as $targets){
                      echo $targets->getTargetName().'<br>';
                    };
                    ?></td>
                  <td><?php echo ('1' === $modal->getStatus()) ? 'Publicado' : 'Borrador'; ?></td>
                  <td><?php echo $modal->getDateCreated()->format('Y-m-d'); ?></td>
                  <td><a href="<?php echo $view['router']->generate('social_snack_modal', ['modal_id' => $modal->getId()]); ?>">Editar</a></td>
                  <td><a href="<?php echo $view['router']->generate('social_snack_modal_delete', ['modal_id' => $modal->getId()]); ?>"  onclick="return confirm('Estás seguro?')">Borrar</a></td>
                </tr>
              <?php
              }
            }
            ?>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#modal').dataTable();
  } );
</script>
<?php $view['slots']->stop(); ?>
