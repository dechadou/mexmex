<?php

namespace SocialSnack\WsBundle\Tests\Request;

use Buzz\Browser;
use SocialSnack\WsBundle\Request\ConsultaAbstract;

class ConsultaAbstractTest extends \PHPUnit_Framework_TestCase {
	
	
	public function testBuild_signature() {
		$query = new ConsultaTesteable([''], '', '713610', new Browser());
		$this->assertEquals('2571361017840225', $query->build_signature());
	}
	
	
	public function testSet_args() {
		$query = new ConsultaTesteable([''], '', '', new Browser());
		$args  = array(
				'foo'          => 'bar',
				'CodigoEstado' => 'test',
		);
		$query->set_args( $args );
		foreach ( $args as $k => $v ) {
			$this->assertEquals( $v, $query->get_arg( $k ) );
		}
	}
	
}


class ConsultaTesteable extends ConsultaAbstract {
	
	/**
	 * Return a fixed value to test the signature.
	 * The value used (25) is taken from the documentation. It should be used
	 * with $client = 713610 and the expected result is 2571361017840225.
	 * 
	 * @return int
	 */
	protected function get_date() {
		return 25;
	}
	
	public function build_signature() {
		return parent::build_signature();
	}

  protected function parse_res($xml) {
  }
}