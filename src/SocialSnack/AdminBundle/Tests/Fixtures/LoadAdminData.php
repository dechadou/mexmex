<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\AdminBundle\Entity\Admin;
use SocialSnack\AdminBundle\Entity\AdminRole;
use SocialSnack\FrontBundle\Entity\User;

/**
 * Class LoadAdminData
 * @package SocialSnack\AdminBundle\Tests\Fixtures
 * @author Guido Kritz
 */
class LoadAdminData implements FixtureInterface {

  static $admin;

  public function load(ObjectManager $manager) {
    $admin = new Admin();
    $admin
        ->setUser('admin')
        ->setPlainPassword('abc')
        ->setPassword('123')
    ;
    $manager->persist($admin);
    self::$admin = $admin;

    $user_role = new AdminRole();
    $user_role
        ->setName('Usuario')
        ->setRole('ROLE_USER')
    ;
    $manager->persist($user_role);

    $admin_role = new AdminRole();
    $admin_role
        ->setName('Admin')
        ->setRole('ROLE_ADMIN')
    ;
    $manager->persist($admin_role);

    $admin->addRole($user_role);
    $admin->addRole($admin_role);

    $manager->flush();
  }

} 