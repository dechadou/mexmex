<?php
$view->extend('SocialSnackAdminBundle::base.html.php');

$title = 'Listado de Códigos de Tracking';
if ($inactive) {
  $title .= ' inactivos';
}
$active_switch_url = $view['router']->generate('admin_movies_trackingCodes' . ($inactive ? '' : '_inactive'));
$active_switch_label = 'Ver ' . ($inactive ? 'activos' : 'inactivos');

$view['slots']->set('title', strip_tags($title) . ' - Cinemex');
$view['slots']->start('body')
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?php echo $title; ?>
          <div class="right">
            <a href="<?php echo $active_switch_url; ?>"><?php echo $active_switch_label; ?></a>
          </div>
        </header>

        <div class="panel-body">
          <div class="adv-table">
            <div class="clearfix">
              <div class="btn-group">
                <a class="btn btn-default" id="editable-sample_new" href="<?php echo $view['router']->generate('admin_movies_trackingCodes_add'); ?>">
                  Nuevo código <i class="icon-plus"></i>
                </a>
              </div>
            </div>

            <table class="display table table-bordered table-striped" id="example">
              <thead>
              <tr>
                <th>Nombre</th>
                <th>Activo</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($codes as $code) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $code->getName(); ?></td>
                  <td><?php echo $code->getActive() ? 'Sí' : 'No'; ?></td>
                  <td>
                    <a href="<?php echo $view['router']->generate('admin_movies_trackingCodes_edit', array('id' => $code->getId())); ?>">Editar</a>
                    <a href="<?php echo $view['router']->generate('admin_movies_trackingCodes_assign', array('id' => $code->getId())); ?>">Asignar</a>
                    <a href="<?php echo $view['router']->generate('admin_movies_trackingCodes_delete', array('id' => $code->getId())); ?>" onclick="return confirm('¿Estás seguro?');">Borrar</a>
                  </td>
                </tr>
              <?php
              }
              ?>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
      $('#example').dataTable({});
    });
  </script>
<?php $view['slots']->stop(); ?>