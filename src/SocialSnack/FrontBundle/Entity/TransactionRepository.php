<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class TransactionRepository extends EntityRepository {
  
  protected function _search($q, $c, $orderBy = array(), $limit = NULL, $offset = NULL, $select = 't') {
    $column = 't.'.$c;
    $qb = $this->createQueryBuilder('t');
    $qb->select($select)
        ->where($column.' LIKE :q')
        /*->where($qb->expr()->orX(
            $qb->expr()->like($column, ':q')---
            //$qb->expr()->like('t.code', ':q')
        ))*/
        ->setParameter('q', sprintf('%%%s%%', $q));
    
    foreach ($orderBy as $col => $dir) {
      $qb->addOrderBy('t.' . $col, $dir);
    }
    if ($limit) {
      $qb->setMaxResults($limit);
    }
    if ($offset) {
      $qb->setFirstResult($offset);
    }
    
    $query = $qb->getQuery();
    $query->execute();
    
    return $query->getResult();
  }
  
  public function search($q, $c = NULL, $orderBy = NULL, $limit = NULL, $offset = NULL) {
    return $this->_search($q, $c, $orderBy, $limit, $offset);
  }
  
  /**
   * @todo Parse the result more safely.
   * 
   * @param string $q
   * @return int
   */
  public function getSearchCount($q,$c) {
    return (int)$this->_search($q, $c, array(), NULL, NULL, 'count(t.id)')[0][1];
  }


  public function getAggregationDataSince($since_id, $limit) {
    $qb = $this->createQueryBuilder('t');
    $qb
        ->select(['t.id', 'm.id movie_id', 'm.group_id', 't.tickets', 't.tickets_count', 't.amount', 'm.attributes'])
        ->innerJoin('t.movie', 'm')
        ->orderBy('t.id', 'ASC')
        ->setMaxResults($limit)
    ;

    if ($since_id) {
      $qb
          ->where('t.id > :since_id')
          ->setParameter('since_id', $since_id)
      ;
    }

    $q = $qb->getQuery();
    return $q->getResult(Query::HYDRATE_ARRAY);
  }

}