( function() {
  var last_term = '',
      timer     = false,
      timer2    = false,
      ajax      = false,
      cache     = [],
      focused   = false,
      hover     = false,
      $form     = $( '#top-search' ),
      $input    = $( '#top-search-input' ),
      $bt       = $form.find( 'button' ),
      $res      = $( '#top-search-results' )
  ;

  function hide_res() {
    if ( focused || hover )
      return;
    
    timer2 = setTimeout( function() {
      $res.slideUp();
      $input.removeClass( 'open' );
    }, 1000 );
  }
  
  $form
    .bind( 'keypress mouseover focus', function() {
      hover = true;
      if ( timer2 ) {
        clearTimeout( timer2 );
        timer2 = false;
      }
    } )
    .bind( 'mouseleave', function() {
      hover = false;
      
      hide_res();
    } );
    
  $input
    .bind( 'focus', function( e ) {
      e.preventDefault();
      $( this ).addClass( 'open' );
      focused = true;
    } )
    .bind( 'blur', function( e ) {
      e.preventDefault();
      if ( !$( this ).val() ) {
        $( this ).removeClass( 'open' );
      }
      focused = false;
      
      hide_res();
    } )
    .bind( 'change keydown', function() {
      quick_search();
    } );


  $form.bind( 'submit', function( e ) {
    e.preventDefault();
    quick_search( true );
    if ( $res.find( 'li' ).length ) {
      $res.slideDown();
    }
  } );

  function quick_search( nowait ) {
    if ( timer ) {
      clearTimeout( timer );
      timer = false;
    }
    
    timer = setTimeout( function() {
      var term = $input.val();
      
      if ( term.length < 3 ) {
        hide_results();
        return false;
      }
      
      if ( term == last_term )
        return false;

      last_term = term;
      
      _quick_search( term );
    }, nowait ? 1 : 300 );
  }
  
  function _quick_search( term ) {
    var res = false;
    
    if ( term.length < 3 )
      return false;
    
    $bt.addClass( 'loading' );
    $.each( cache, function( i, entry ) {
      if ( entry.term == term ) {
        res = entry.res;
        return false;
      }
    } );
    
    if ( res )
      return quick_search_res( res );
    
    if ( ajax ) {
      ajax.abort();
    }
    
    $.ajax( {
      url      : rest_url + 'movies/search',
      type     : 'get',
      dataType : 'json',
      data     : { q : term, limit : 4 },
      success  : function( res ) {
        clog( $bt );
        $bt.removeClass( 'loading' );
        cache.push( {
          term : term,
          res  : res
        } );
        quick_search_res( res );
      }
    } );
  }
  
  function quick_search_res( res ) {
    var $ul  = $( '<ul/>' );

    $res.children().remove();
    
    if ( res.movies && res.movies.length ) {
      $.each( res.movies, function( i, movie ) {
        var $img = $( '<img/>', { src : movie.covers['34x45'], width : 34, height : 45 } ),
            $a   = $( '<a/>', { text : movie.name, href : movie.url } ),
            $li  = $( '<li/>' );

        $img.prependTo( $a );
        $a.appendTo( $li );
        $ul.append( $li );
      } );

      $res.append( $ul );
    } else {
      $( '<div/>', { 'class' : 'no-results', 'text' : 'No se encontraron resultados' } ).appendTo( $res );
    }
    $res.slideDown();
  }
  
  function hide_results() {
    $res.hide();
  }
  
} )();