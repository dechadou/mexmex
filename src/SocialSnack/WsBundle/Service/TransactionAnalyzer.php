<?php

namespace SocialSnack\WsBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;

class TransactionAnalyzer {

  protected $doctrine;


  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  protected function getHourlyCompareDates(\DateTime $startTime) {
    // Get transactions count in the previous hour and the same hour a week ago for comparison.
    $time_to = clone $startTime;
    $time_to->setTime($time_to->format('H'), 0, 0);
    $time_from = clone $time_to;
    $time_from->sub(new \DateInterval('PT1H'));
    $comp_time_to = clone $time_to;
    $comp_time_to->sub(new \DateInterval('P1W'));
    $comp_time_from = clone $time_from;
    $comp_time_from->sub(new \DateInterval('P1W'));

    return array($time_from, $time_to, $comp_time_from, $comp_time_to);
  }


  protected function getComparableTransactions($criteria) {
    $conn     = $this->doctrine->getConnection();

    switch ($criteria) {
      case 'cinema':
        $query = "SELECT cinema_id, amount, date FROM TransactionsByCinemaHourly
            WHERE date >= :date_from AND date < :date_to";
        break;

      case 'app':
        $query = "SELECT app_id, amount, date FROM TransactionsByAppHourly
            WHERE date >= :date_from AND date < :date_to";
        break;

      default:
        throw new \Exception('Invalid criteria.');
    }

    $stmt = $conn->prepare($query);

    list($time_from, $time_to, $comp_time_from, $comp_time_to) = $this->getHourlyCompareDates(new \DateTime('now'));

    // Get last hour transactions.
    $stmt->bindValue('date_to', $time_to->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_from', $time_from->format('Y-m-d H:i:s'));
    $stmt->execute();
    $today = $stmt->fetchAll();

    // Get previous week transactions.
    $stmt->bindValue('date_to', $comp_time_to->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_from', $comp_time_from->format('Y-m-d H:i:s'));
    $stmt->execute();
    $comp = $stmt->fetchAll();

    return array($today, $comp);
  }


  protected function analyzeTransactionsBy($criteria, $column, $collection, $idGetter, $alertMsg) {
    $alerts = [];

    list($_today, $_comp) = $this->getComparableTransactions($criteria);

    $today = [];
    foreach ($_today as $item) {
      $today[$item[$column]] = $item['amount'];
    }

    $comp = [];
    foreach ($_comp as $item) {
      $comp[$item[$column]] = $item['amount'];
    }

    // Check for every app.
    foreach ($collection as $item) {
      $count_today = isset($today[$item->{$idGetter}()]) ? $today[$item->{$idGetter}()] : 0;
      $count_comp  = isset($comp[$item->{$idGetter}()]) ? $comp[$item->{$idGetter}()] : 0;

      // If today's number is below the threshold trigger an alert.
      if ($count_comp > 10 && $count_today === 0) {
        $alerts[] = sprintf(
            $alertMsg,
            $item->getName(),
            $count_today,
            $count_comp
        );
      }
    }

    return $alerts;
  }


  /**
   * Find apps allowed to sell.
   *
   * @return mixed
   */
  protected function getApps() {
    return $this->doctrine
        ->getRepository('SocialSnackRestBundle:App')
        ->findByCap('purchase');
  }


  /**
   * Find active cinemas.
   *
   * @return array
   */
  protected function getCinemas() {
    return $this->doctrine
        ->getRepository('SocialSnackWsBundle:Cinema')
        ->findBy(['active' => 1]);
  }


  public function analyzeTransactionsByApp() {
    return $this->analyzeTransactionsBy(
        'app',
        'app_id',
        $this->getApps(),
        'getAppId',
        'El número de ventas del medio %s cayó por debajo del 50%% respecto a la semana anterior (%d/%d).'
    );
  }


  public function analyzeTransactionsByCinema() {
    return $this->analyzeTransactionsBy(
        'cinema',
        'cinema_id',
        $this->getCinemas(),
        'getId',
        'El número de ventas del complejo %s cayó por debajo del 50%% respecto a la semana anterior (%d/%d).'
    );
  }


  /**
   * Look for transactions anomalies.
   */
  public  function analyzeTransactions() {
    if ($this->getCurrentTime() < 900) {
      return [];
    }

    return array_merge(
        $this->analyzeTransactionsByApp(),
        $this->analyzeTransactionsByCinema()
    );
  }


  protected function getCurrentTime() {
    return (int)date('Hi');
  }

}