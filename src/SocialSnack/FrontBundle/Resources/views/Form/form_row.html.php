<?php
$whole      = isset($attr['class']) && preg_match('/\bwhole\b/', $attr['class']);
$subform    = isset($attr['subform']) && $attr['subform'];
$label_attr = array(
    'class' => ''
);

if ( $subform ) {
  $class = 'subform clearfix';
} else {
  $class = 'form-row';
  if ( $whole ) {
    $class .= ' whole';
  } else {
    $class .= ' half';
  } 
}

if ( isset($attr['row_class']) ) {
  $class .= ' ' . $attr['row_class'];
  unset($attr['row_class']);
}

if ( $subform ) {
  $label_attr['class'] .= ' subform-label';
}
?>
<div class="<?php echo trim($class); ?>">
	<?php echo $view['form']->label($form, $label, array('label_attr' => $label_attr)) ?>
	<?php echo $view['form']->errors($form) ?>
	<?php echo $view['form']->widget($form, $parameters) ?>
</div>