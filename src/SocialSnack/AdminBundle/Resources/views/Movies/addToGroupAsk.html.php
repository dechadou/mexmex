<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Agrupar película - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Agrupar película <strong><?php echo $movie->getName(); ?></strong>
      </header>
      <div class="panel-body">
        <a href="<?php echo $view['router']->generate('admin_movies_createGroupFromMovie', ['movie_id' => $movie->getId()]); ?>" class="btn btn-default">Crear nuevo grupo</a>
      </div>
      <div class="panel-body">
        <p>Añadir a un grupo existente</p>
        <div class="adv-table">
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>ID</th>
                <th>Group ID</th>
                <th>Título</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($groupers as $grouper) { ?>
              <tr>
                <td><?php echo $grouper->getId(); ?></td>
                <td><?php echo $grouper->getGroupId(); ?></td>
                <td><?php echo $grouper->getName(); ?></td>
                <td><a href="<?php echo $view['router']->generate('admin_movies_addMovieToGroup', ['movie_id' => $movie->getId(), 'group_id' => $grouper->getGroupId()]); ?>">Agrupar</a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<?php
$view['slots']->stop();