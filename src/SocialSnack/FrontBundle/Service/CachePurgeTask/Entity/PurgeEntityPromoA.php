<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityPromoA extends AbstractEntityTask implements UrlTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('esi_news_footer'),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('promos_archive'),
            'env'  => 'front'
        ],
        [
            'path' => $this->frontUtils->get_cms_url($this->entity->getImage(), '1020x129'),
            'env'  => 'static'
        ],
    ];
  }

}