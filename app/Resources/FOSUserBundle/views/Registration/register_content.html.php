<form action="<?php echo $view['router']->generate( 'fos_user_registration_register' ); ?>" {{ form_enctype(form) }} method="POST" class="fos_user_registration_register">
    <?php echo $view['form']->widget($form); ?>
    <div>
        <input type="submit" value="{{ 'registration.submit'|trans }}" />
    </div>
</form>
