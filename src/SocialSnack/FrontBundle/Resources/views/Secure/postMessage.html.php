<!DOCTYPE html>
<html><head>
<meta name="robots" content="noindex, nofollow" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>!window.jQuery && document.write('<script src="<?php echo $view['assets']->getUrl( 'assets/vendor/jquery/jquery-1.10.2.min.js' ); ?>"><\/script>')</script>
<?php foreach ($view['assetic']->javascripts(
  array(
      '@SocialSnackFrontBundle/Resources/public/vendor/artberri/jquery.html5storage.min.js',
      '@SocialSnackFrontBundle/Resources/public/vendor/jschannel/jschannel.js',),
  array()
) as $url): ?>
<script src="<?php echo $view->escape($url) ?>"></script>
<?php endforeach; ?>
<script>

var chan = Channel.build({window: window.parent, origin: "*", scope: "testScope"});
chan.bind('removeItem', function(trans, key) {
  return $.localStorage.removeItem(key);
});
chan.bind('getItem', function(trans, key) {
  return $.localStorage.getItem(key);
});
chan.bind('setItem', function(trans, params) {
  return $.localStorage.setItem(params.k, params.v);
});

</script>
</head>
</html>