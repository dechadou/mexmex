<?php

namespace SocialSnack\RestBundle\Command;

use SocialSnack\RestBundle\Entity\AppUsageEntry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AggregateClientsUsageCommand extends ContainerAwareCommand {

  public function configure() {
    $this->setName('rest:clients_usage:aggregate');
  }

  public function execute(InputInterface $input, OutputInterface $output) {
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
    $doctrine = $this->getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    /** @var \Memcached $memcached */
    $memcached = $this->getContainer()->get('memcached');

    $apps = $doctrine->getRepository('SocialSnackRestBundle:App')->findAll();

    foreach ($apps as $app) {
      $app_id = $app->getAppId();
      $key = 'api_client_hit:' . $app_id;
      $val = $memcached->get($key);
      if (!$val) {
        continue;
      }

      // Write the count to DB.
      $output->writeln(sprintf('%s hits: %d', $app_id, $val));
      $entry = new AppUsageEntry();
      $entry
          ->setApp($app)
          ->setMisses($val)
          ->setDate(new \DateTime())
      ;
      $em->persist($entry);
      $em->flush();

      // Reset counter (-= $val).
      // Can't reset to zero because another process could have altered the value since last read.
      $memcached->decrement($key, $val);
    }
  }

}