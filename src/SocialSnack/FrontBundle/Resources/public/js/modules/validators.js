var validator_fns = {
  not_empty: function( v, $t ) {
    if ( 'radio' == $t.attr('type') ) {
      var name = $t.attr('name');
      return !!$t.closest('form').find('[name="' + name + '"]:checked').length;
    } else if ( 'checkbox' == $t.attr('type') ) {
      return $t.is(':checked');
    } else {
      return !!v;
    }
  },

  full_name: function( v ) {
    return /[a-zA-Z][ \.]+[a-zA-Z][a-zA-Z \.]*/i.test( v );
  },

  ccnum: function( v ) {
    return validate_ccnum( v );
  },
  
  debit_card: function( v ) {
    var re = /^(429505|429522|446985|446986|449187|449700|449702|476684|476687|477261|508112|520116|520694|520698|524711|529028|529093|533609|550233|550962|551077|551081|551134|551240|551244|551253|551276|557561|557874|557875|558426|881021|203101|400195|400443|400820|400866|405063|405689|407848|408400|410128|412432|413098|413131|415269|418914|418928|419334|419335|421003|422299|422671|425983|425984|426188|426808|428464|430967|433454|435769|438099|444888|444889|445017|457249|457476|460068|460700|460766|462275|462278|465496|465497|465498|465762|469693|474174|474176|476588|477130|483029|483030|491089|491365|491566|491567|491571|491580|492143|498588|498589|498590|502275|506199|506201|506204|506205|506206|506213|506214|506215|506217|506218|506221|506229|506236|506245|506247|506249|506250|506251|506253|506254|506255|506257|506258|506259|506263|506264|506265|506266|506267|506269|506270|506273|506274|506275|506277|506278|506279|506280|506300|506302|506303|506304|506305|506306|506307|506308|506309|506311|506312|506314|506317|506318|506319|506320|506322|506323|506332|506333|506334|506340|506341|510586|511114|511265|512280|516685|524847|526424|526498|528074|528480|529506|529571|529575|530113|533987|534926|535943|537030|539975|539978|541099|545094|545290|545325|545730|551238|557551|557602|588771|588772|589617|601999|603681|627318|627535|627636|636379|639388|639484|639559|643200|643201|643202|643203|643204|643205|643206|643207|643208|643209|643212|643260|643270|643280|643281|506337|506313|400819|408340|408341|408343|409851|409852|410177|411808|416916|441312|441313|441712|446115|446116|446117|446118|451712|455509|455510|455511|455533|455537|493120|493121|505751|516415|526400|526404|537033|539944|546378|555924|589751|636937|4213|5579|4658|4213).+/;
    return (v + '').replace(/[^0-9]+/g, '').match(re);
  },
  
  csc: function( v ) {
    return validate_csc( v );
  },

  cc_expiry_month: function( v, $t ) {
    var $y = $t.parent().find( '.year' );
    if ( $y.length && !$y.val() )
      return true;

    if ( validate_expire_date( v, $y.val() ) ) {
      $y.removeClass( 'error' ).addClass( 'valid' );
      return true;
    } else {
      $y.removeClass( 'valid' ).addClass( 'error' );
      return false;
    }
  },

  cc_expiry_year: function( v, $t ) {
    var $m = $t.parent().find( '.month' );
    if ( $m.length && !$m.val() )
      return true;

    if ( validate_expire_date( $m.val(), v ) ) {
      $m.removeClass( 'error' ).addClass( 'valid' );
      return true;
    } else {
      $m.removeClass( 'valid' ).addClass( 'error' );
      return false;
    }
  },

  email: function( v ) {
    if ( !v )
      return true;
    return email_verify( v );
  },

  repeat: function(v, $t) {
    var $ref = $($t.data('repeat-ref'));
    return v === $ref.val();
  }
};

$.fn.setup_validators = function() {
  $( this ).each( function() {

    var $t    = $( this ),
        _fns  = $t.data( 'validators' ).split( ' ' ),
        fns   = [],
        evt   = 'change.formvalidation keyup.formvalidation blur.formvalidation'
    ;

    $.each( _fns, function( i, _fn ) {
      if ( !( fn = validator_fns[_fn] ) )
        return;

      fns.push( fn );
    } );

    $t.bind( evt, function( e ) {
      if ( !$t.data( 'init' ) ) {
        if ( 'blur' == e.type ) {
          $t.data( { init : true } );
        } else {
          return;
        }
      }

      valid = true;

      $.each( fns, function( i, fn ) {
        if ( !fn( $t.val(), $t ) ) {
          valid = false;
          return false;
        }
      } );

      var $v;
      if ( $t.hasClass( 'hasCustomSelect' ) ) {
        $v = $t.next();
      } else if ( 'radio' == $t.attr('type') ) {
        /**
         * @todo The .siblings() thing is kinda hardcode... it would be nice to
         *       code it in some way that allows more flexibility on the markup.
         */
        $v = $t.closest('label').siblings().andSelf();
      } else {
        $v = $t;
      }

      if ( !valid ) {
        $v.addClass( 'error' ).removeClass( 'valid' );
      } else {
        $v.removeClass( 'error' ).addClass( 'valid' );
      }
    } );
    $t.data( { 'validators-binded' : true } );
  } );
};

$.fn.disable_validators = function() {
  var $t = $(this);
  $t.unbind('.formvalidation')
      .removeData('init')
      .removeData('validators-binded');
  return $t;
}

$.fn.run_validators = function( lazy ) {
  valid = true;
  $( this ).each( function() {
    var $t     = $( this ),
        unbind = false;

    if ($t.parents('.form-row').is(':hidden')) {
      return;
    }

    if ( !$t.data( 'validators-binded' ) ) {
      $t.setup_validators();
    }
    $( this ).data( { init : true } ).trigger( 'change.formvalidation' );
    if ( $( this ).hasClass( 'error' ) ) {
      valid = false;
      if ( lazy )
        return false;
    }
  } );
  return valid;
};


$( '[data-flyvalidate]' ).setup_validators();

$( 'form[data-basicvalidate=1]' ).bind( 'submit.formvalidation', function( e ) {
  var $form = $( this ),
      valid = true;

  if ( $form.data( 'working' ) )
    return false;

  $form.find( '[data-validators]' ).each( function() {
    if ( !$( this ).run_validators() ) {
      valid = false;
      return false;
    }
  } );

  if ( !valid ) {
		$.msg_popup( 'Por favor, verifica los datos ingresados.', 'msg-error', true );
    return false;
  }

  $form.data( { working : true } );
  var $bt = $form.find( 'button[type="submit"]' ),
      $ui = $form.closest( '.content-box, .col, #main-body' ).first();
  if ( !$ui.length )
    $ui = $form;

  $bt.addClass( 'btn-loading' );
  $ui.blockui();

  if ( ( _fn = $form.data( 'validcb' ) ) && ( fn = window[_fn] ) ) {
    e.preventDefault();
    fn( $form, $ui, $bt );
  }
} );