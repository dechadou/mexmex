<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Error - Cinemex' );
$view['slots']->start('body')
?>
<div class="error-page-msg">
  <div class="big-msg">
    <p><strong>Estimado Invitado</strong></p>
    <p>
      Queremos informarte que por el momento la compra en línea para Cinemex Santa Fe no está disponible.
      Te invitamos a contactar a nuestro call center (<strong>5257-6969</strong> o marcando <strong>*CINE</strong> desde tu celular)
      para poder realizar tu compra o bien adquirir tus boletos directamente <strong>en la taquilla del cine</strong>.
    </p>
    <p>
      Estamos trabajando para restablecer a la brevedad el servicio y ofrecerte la mejor experiencia en tu compra
    </p>
    <a href="<?php echo $view['router']->generate('home'); ?>" class="btn btn-default btn-icon icon icon-larr">Volver</a>
  </div>
</div>
<?php $view['slots']->stop(); ?>