<span class="btn btn-sec btn-no-shadow score-number"><?php echo $movie->getScore() ? $movie->getScore() : 'N/A'; ?></span>
<span class="movie-score-label">Tu puntaje:</span>
<span class="movie-score-stars">
  <?php for ( $i = 1 ; $i <= 5 ; $i++ ) { ?>
  <?php $star_label = $i . ' estrella' . ( $i > 1 ? 's' : '' ); ?>
  <a href="#" rel="set-score" data-id="<?php echo $movie->getId(); ?>" data-score="<?php echo $i; ?>" class="<?php if ( $user_score >= $i ) { echo 'whole'; } elseif ( $user_score == $i - 0.5 ) { echo 'half'; } ?>" title="Puntuar con <?php echo $star_label; ?>"><?php echo $star_label; ?></a>
  <?php } ?>
</span>