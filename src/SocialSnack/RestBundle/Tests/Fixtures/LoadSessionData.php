<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\RestBundle\Entity\App;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;
use SocialSnack\WsBundle\Entity\Ticket;

class LoadSessionData implements FixtureInterface {

  static $sessions;
  static $session_without_seat_allocation;
  static $cinemas;
  static $movies;

  public function load(ObjectManager $manager) {
    static::$sessions = array();
    static::$cinemas  = array();
    static::$movies   = array();

    $state = new State();
    $state
        ->setName('State A')
        ->setLegacyName('STATE A')
        ->setLegacyId(1)
    ;
    $manager->persist($state);

    $area = new StateArea();
    $area
        ->setName('Area A')
        ->setLegacyName('AREA A')
        ->setLegacyId(1)
        ->setState($state)
        ->setLegacyStateId($state->getLegacyId())
    ;
    $manager->persist($area);

    $cinema = new Cinema();
    $cinema
        ->setName('Cinema A')
        ->setLegacyName('CINEMA A')
        ->setState($state)
        ->setArea($area)
        ->setInfo(json_encode(array()))
        ->setPlatinum(FALSE)
        ->setLat(0)
        ->setLng(0)
        ->setLegacyId(1)
    ;
    $manager->persist($cinema);
    static::$cinemas[] = $cinema;

    $movie = new Movie();
    $movie
        ->setGroupId('ng')
        ->setInfo(json_encode(array()))
        ->setName('Movie A')
        ->setShortName('M. A')
        ->setPremiere(FALSE)
        ->setActive(TRUE)
        ->setPosterUrl('')
    ;
    $manager->persist($movie);
    static::$movies[] = $movie;

    $session = new Session();
    $session
        ->setActive(TRUE)
        ->setDateTime(new \DateTime('tomorrow'))
        ->setTz('America/Tijuana')
        ->setLegacyId(1)
        ->setPlatinum(FALSE)
        ->setPremium(FALSE)
        ->setExtreme(FALSE)
        ->setLegacyCinemaId($cinema->getLegacyId())
        ->setCinema($cinema)
        ->setInfo(json_encode((object)['SEATALLOCATIONON' => 'Y', 'PRICECODE' => '0001']))
        ->setGroupId('ng')
        ->setMovie($movie)
    ;
    $manager->persist($session);
    static::$sessions[] = $session;

    $ticket = new Ticket();
    $ticket
        ->setCinema($cinema)
        ->setDescription('Ticket 1')
        ->setLegacyDescription('TICKET 1')
        ->setLegacyId('0001')
        ->setPrice(1000)
        ->setTypeId('0001')
        ->setInfo(json_encode(['TICKETTYPECODE' => '0001']))
        ->setLegacyCinemaId($cinema->getLegacyId())
    ;
    $manager->persist($ticket);

    $ticket = new Ticket();
    $ticket
        ->setCinema($cinema)
        ->setDescription('20 ANIV')
        ->setLegacyDescription('20 ANIV')
        ->setLegacyId('0001')
        ->setPrice(1000)
        ->setTypeId('0002')
        ->setInfo(json_encode(['TICKETTYPECODE' => '0002']))
        ->setLegacyCinemaId($cinema->getLegacyId())
    ;
    $manager->persist($ticket);

    $session = new Session();
    $session
        ->setActive(TRUE)
        ->setDateTime(new \DateTime('now'))
        ->setTz('America\Tijuana')
        ->setLegacyId(1)
        ->setPlatinum(FALSE)
        ->setPremium(FALSE)
        ->setExtreme(FALSE)
        ->setLegacyCinemaId($cinema->getLegacyId())
        ->setCinema($cinema)
        ->setInfo(json_encode((object)['SEATALLOCATIONON' => 'N']))
        ->setGroupId('ng')
        ->setMovie($movie)
    ;
    $manager->persist($session);
    static::$session_without_seat_allocation = $session;

    $manager->flush();
  }
} 