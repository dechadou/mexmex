<?php

namespace SocialSnack\RestBundle\Tests\Handler;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ContextHandlerTest extends WebTestCase {

  protected function _testNormalizeAreaIds($api_version, $area_id, $state_id) {
    $handler = $this->getContainer()->get('rest.context.handler');
    $handler->setApiVersion($api_version);
    return $handler->normalizeAreaIds($area_id, $state_id);
  }

  public function testNormalizeAreaIdsAreaV1() {
    $api_version = 1;
    $area_id = 1;
    $state_id = NULL;

    list($area_id,$state_id) = $this->_testNormalizeAreaIds($api_version, $area_id, $state_id);

    $this->assertEquals(1, $area_id);
    $this->assertNull($state_id);
  }

  public function testNormalizeAreaIdsAreaV2() {
    $api_version = 2;
    $area_id = 1008;
    $state_id = NULL;

    list($area_id,$state_id) = $this->_testNormalizeAreaIds($api_version, $area_id, $state_id);

    $this->assertEquals(1008, $area_id);
    $this->assertNull($state_id);
  }

  public function testNormalizeAreaIdsStateV1() {
    $api_version = 1;
    $area_id = 1008;
    $state_id = NULL;

    list($area_id,$state_id) = $this->_testNormalizeAreaIds($api_version, $area_id, $state_id);

    $this->assertNull($area_id);
    $this->assertEquals(8, $state_id);
  }

  public function testNormalizeAreaIdsStateV2() {
    $api_version = 1;
    $area_id = NULL;
    $state_id = 8;

    list($area_id,$state_id) = $this->_testNormalizeAreaIds($api_version, $area_id, $state_id);

    $this->assertNull($area_id);
    $this->assertEquals(8, $state_id);
  }

}