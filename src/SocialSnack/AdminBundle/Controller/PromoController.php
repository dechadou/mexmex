<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\Promo;

class PromoController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }
  

  public function indexAction() {
    $this->check_roles();
    
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Promo');
    $promos = $repo->findActive();

    return $this->render('SocialSnackAdminBundle:Promo:list.html.php', array('promos' => $promos));
  }

  public function editAction($promo_id, Request $request) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Promo');
    $promo = $repo->find($promo_id);
    $purge = NULL;

    $form = $this->createFormBuilder()
        ->add('title', 'text', array(
            'data' => (is_string($promo->getTitle())) ? $promo->getTitle() : '',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('subtitle', 'text', array(
            'data' => (is_string($promo->getSubtitle())) ? $promo->getSubtitle() : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('content', 'textarea', array(
            'data' => (is_string($promo->getContent())) ? $promo->getContent() : '',
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('url', 'text', array(
            'data' => (is_string($promo->getUrl())) ? $promo->getUrl() : '',
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('sequence', 'text', array(
            'data' => ($promo->getSequence()) ? $promo->getSequence() : '',
            'attr' => array('class' => 'form-control')
        ))
        ->add('status', 'checkbox', array(
            'required' => false,
            'data' => ($promo->getStatus() == 1) ? true : false
        ))
        ->add('thumb', 'file', array('required' => false))
        ->add('image', 'file', array('required' => false))
        ->add('save', 'submit')
        ->getForm();


    $form->handleRequest($request);

    if ($form->isValid()) {
      $em->persist($promo);
      $em->flush();
      
      $base_path = 'promos/';

      if ($file = $form["thumb"]->getData()) {
        $filename = $promo->getId() . "-promo-thumb.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 236, 222);
        $promo->setThumb($filename);
      }
      
      if ($file = $form["image"]->getData()) {
        $filename = $promo->getId() . "-promo-image.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 670, 320);
        $promo->setImage($filename);
      }

      $data = $form->getData();
      $promo->setTitle((string) $data['title']);
      $promo->setSubtitle((string) $data['subtitle']);
      $promo->setContent((string) $data['content']);
      $promo->setUrl((string) $data['url']);
      $promo->setStatus($data['status']);
      $promo->setSequence((int) $data['sequence']);

      $em->flush();

      $purger = $this->get('ss.cache_purger');
      $purge = $purger->purge_cache(array('entity' => $promo));
    }

    return $this->render('SocialSnackAdminBundle:Promo:edit.html.php', array(
        'promo' => $promo,
        'form'  => $form->createView(),
        'purge' => $purge,
    ));
  }

  public function addAction(Request $request) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Promo');
    $promo = new Promo();

    $form = $this->createFormBuilder()
        ->add('title', 'text', array(
            'attr' => array('class' => 'form-control')
        ))
        ->add('subtitle', 'text', array(
            'attr' => array('class' => 'form-control')
        ))
        ->add('content', 'textarea', array(
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('url', 'text', array(
            'required' => false,
            'attr' => array('class' => 'form-control')
        ))
        ->add('sequence', 'text', array(
            'attr' => array('class' => 'form-control')
        ))
        ->add('status', 'checkbox', array(
            'required' => false
        ))
        ->add('thumb', 'file')
        ->add('image', 'file', array('required' => false))
        ->add('save', 'submit')
        ->getForm();


    $form->handleRequest($request);

    if ($form->isValid()) {
      $base_path = 'promos/';

      $data = $form->getData();
      $promo->setTitle((string) $data['title']);
      $promo->setSubtitle((string) $data['subtitle']);
      $promo->setContent((string) $data['content']);
      $promo->setUrl((string) $data['url']);
      $promo->setSequence((int) $data['sequence']);
      if ($data['status']) {
        $active = 1;
      } else {
        $active = 0;
      }
      $promo->setStatus($active);
      $promo->setThumb("default.jpg");
      $promo->setImage("default.jpg");
      $em->persist($promo);
      $em->flush();

      $file = $form["thumb"]->getData();
      $filename = $promo->getId() . "-promo-thumb.jpg";
      $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 236, 222);
      $promo->setThumb($filename);

      if ($file = $form["image"]->getData()) {
        $filename = $promo->getId() . "-promo-image.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 670, 320);
        $promo->setImage($filename);
      }

      $em->persist($promo);
      $em->flush();

      $purger = $this->get('ss.cache_purger');
      $purge = $purger->purge_cache(array('entity' => $promo));
      
      return $this->redirect($this->generateUrl('social_snack_promo', array('promo_id' => $promo->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Promo:add.html.php', array('promo' => $promo, 'form' => $form->createView()));
  }

  public function deleteAction($promo_id) {
    $this->check_roles();
    
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Promo');
    $promo = $repo->find($promo_id);
    $em->remove($promo);
    $em->flush();
    return $this->redirect($this->generateUrl('social_snack_promo_list'));
  }

  public function inactiveAction() {
    $this->check_roles();

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Promo');
    $promos = $repo->findActive(0);

    return $this->render('SocialSnackAdminBundle:Promo:inactive_list.html.php', array('promos' => $promos));
  }

}
