<?php $view->extend('SocialSnackFrontBundle:Landings:corpsales.html.php'); ?>

<?php $view['slots']->start('corpsales_body'); ?>

<div class="content-box">
  <h1 class="content-box-title">Preguntas frecuentes</h1>

  <div class="entry-body">
    <h2 class="content-box-subtitle">Boletos empresariales</h2>

    <dl>
      <dt>¿Qué es un boleto empresarial?</dt>
      <dd>Es un boleto prepagado para el cine con descuento</dd>

      <dt>¿Cuál es el mínimo de boletos que debo comprar para obtener un descuento?</dt>
      <dd>100 boletos si los requieres físicos o  electronicos</dd>

      <dt>¿Qué vigencia tienen los boletos?</dt>
      <dd>3 meses</dd>

      <dt>¿Se puede extender la vigencia de los boletos?</dt>
      <dd>No, no se puede extender y es irrenovable.</dd>

      <dt>¿En cuánto tiempo me entregan los boletos?</dt>
      <dd>Una vez recibido el pago, tardamos de 1 a 2 días en el DF, en el interior de la república de 3 a 5 días hábiles.</dd>

      <dt>¿Entregan factura por la compra de boletos empresariales?</dt>
      <dd>Si, sólo necesitas enviarnos los documentos solicitados.</dd>

      <dt>¿Los boletos pueden traer un diseño especial?</dt>
      <dd>En la compra mínima de 10,000 boletos se puede personalizar.</dd>

      <dt>¿Cuál es el tiempo de entrega si los boletos se personalizan?</dt>
      <dd>Un mes, una vez aprobado el diseño.</dd>

      <dt>¿En dónde me entregan los boletos?</dt>
      <dd>En las oficinas corporativas ubicadas en Santa Fe o en cualquier cine que te quede cerca de tu domicilio.</dd>
    </dl>

    <h2 class="content-box-subtitle">Funciones de grupo, privadas y renta de la sala</h2>

    <dl>
      <dt>¿Qué es una función de grupo?</dt>
      <dd>Es un evento al que asisten varias personas para disfrutar de una misma función que se encuentra en cartelera y que queda abierta al público en general.</dd>

      <dt>¿Qué es una función privada?</dt>
      <dd>Es un evento al que asisten una o varias personas para disfrutar de una misma función que se encuentra en cartelera y sólo pueden ingresar  los invitados.</dd>

      <dt>¿Qué es una renta de sala?</dt>
      <dd>Es la renta del espacio de uno de nuestros auditorios para cualquier actividad o evento que requiera por ejemplo: capacitaciones, presentaciones de productos, etc.</dd>

      <dt>¿Puedo realizar una presentación y al finalizar proyectarles  una película a mis invitados?</dt>
      <dd>Si, si lo puedes realizar.</dd>

      <dt>¿Cuál es el mínimo que requiero para una función de grupo?</dt>
      <dd>50 personas</dd>

      <dt>¿Cuál es el mínimo que requiero para una función privada o renta de sala?</dt>
      <dd>No se tiene un mínimo pero por ser función privada o renta de sala  se cobra la capacidad total de la sala.</dd>

      <dt>¿Qué día puedo solicitar una función de grupo, función privada o renta de sala?</dt>
      <dd>Lunes y Martes en cualquier horario
      Jueves, Viernes, sábado y domingo  sólo en la primer función</dd>

      <dt>¿Qué costo tiene una función de grupo, función privada o renta de sala?</dt>
      <dd>El costo puede variar de acuerdo al cine, cantidad de personas, horario y en caso de renta de sala el tiempo requerido.</dd>

      <dt>¿Me hacen descuento en las funciones de grupo, función privada o renta de sala?</dt>
      <dd>Si, si realizamos descuento y varía de acuerdo al cine, el día, horario y cantidad de personas</dd>

      <dt>¿Hacen descuento en productos de dulcería?</dt>
      <dd>Si realizamos descuentos, la cual te podemos hacer llegar dentro de la cotización.</dd>

      <dt>¿Con cuántos días de anticipado puedo reservar?</dt>
      <dd>Puedes reservar con por lo menos 5 días hábiles de anticipación.</dd>

      <dt>¿Cómo le puedo hacer para reservar un evento?</dt>
      <dd>Primero tendrías que  solicitar tu cotización, una vez que estés de acuerdo firmarla y realizar el pago.</dd>

      <dt>¿Al contratar el evento, me entregan boletos o invitaciones?</dt>
      <dd>No, no entregamos boletos ni invitaciones, todo corre por cuenta del invitado.</dd>

      <dt>¿Cómo puedo realizar el pago?</dt>
      <dd>El pago lo puedes hacer por transferencia o depósito en cuenta con los datos  que se te indica en la cotización.</dd>

      <dt>¿Me realizan factura por el evento que quiero contratar?</dt>
      <dd>Si, si realizamos factura.</dd>

      <dt>¿Qué necesito para que me realicen la factura?</dt>
      <dd>Necesitas enviar el alta de hacienda y comprobante de domicilio que coincida con tus datos fiscales.</dd>
    </dl>
  </div>
</div>

<?php $view['slots']->stop(); ?>
