<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;

class PurgeEntityApp extends AbstractEntityTask implements DoctrineTaskInterface {

  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\RestBundle\Entity\AppRepository::findByAppId:' . $this->entity->getAppId(),
                'SocialSnack\RestBundle\Entity\AppRepository::findPublic',
            ],
            'env' => 'front'
        ]
    ];
  }

}