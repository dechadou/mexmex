(function($){
  $(':radio').on('change', function(){
    var $input = $(this)

    $input.parents('.answers')
      .find('.answers__label')
      .removeClass('answers__label--active')

    $input.closest('.answers__label')
      .addClass('answers__label--active')
  })

  function getTransitionEnd() {
    var transEndEventNames = {
      'WebkitAnimation' : 'webkitAnimationEnd',
      'OAnimation' : 'oAnimationEnd',
      'msAnimation' : 'MSAnimationEnd',
      'animation' : 'animationend'
    }

    return animEndEventName = transEndEventNames[Modernizr.prefixed('animation')]
  }

  function resetPage($outpage, $inpage) {
    $outpage.attr('class', $outpage.data('originalClassList'))
    $inpage.attr('class', $inpage.data('originalClassList') + ' pt-page-current')
  }

  function onEndAnimation($outpage, $inpage) {
    endCurrPage = false
    endNextPage = false
    resetPage($outpage, $inpage)
    isAnimating = false
  }

  function animateElements(currPage, nextPage, outClass, inClass){
    var $currPage = $(currPage),
        $nextPage = $(nextPage)

    $nextPage.addClass('pt-page-current')

    $currPage
      .addClass(outClass)
      .on(animEndEventName, function(){
        $currPage
          .off(animEndEventName)

        endCurrPage = true

        if (endNextPage) {
          onEndAnimation($currPage, $nextPage)
        }
      })

    $nextPage
      .addClass(inClass)
      .on(animEndEventName, function(){
        $nextPage.off(animEndEventName)

        endNextPage = true

        if (endCurrPage) {
          onEndAnimation($currPage, $nextPage)
        }
      })

    if (!Modernizr.cssanimations) {
      onEndAnimation($currPage, $nextPage)
    }
  }

  function nextPage(index){
    var $currPage = $pages.eq(current)

    if (!$currPage.find(':checked').length) {
      return
    }

    if (isAnimating) {
      return
    }

    isAnimating = true

    if (!index) {
      if (current < (pagesCount - 1)) {
        ++current
      } else {
        current = 0
      }
    } else {
      current = index
    }

    if (current === (pagesCount - 1)) {
      $.post(window.location.url, $('[data-poll-form]').serialize(), function(response){})
    }

    var $nextPage = $pages.eq(current),
      outClass = 'pt-page-scaleDown',
      inClass = 'pt-page-moveFromRight pt-page-ontop'

    animateElements($currPage, $nextPage, outClass, inClass)
  }

  function prevPage(index){
    if (current === 0 || (current === (pagesCount - 1))) {
      return
    }

    var $currPage = $pages.eq(current)

    if (!index) {
      if (current <= 0) {
        current = (pagesCount - 1)
      } else {
        --current
      }
    } else {
      current = index
    }

    if (isAnimating) {
      return
    }

    isAnimating = true

    var $nextPage = $pages.eq(current),
      outClass = 'pt-page-moveToRight pt-page-ontop',
      inClass = 'pt-page-scaleUp'

    animateElements($currPage, $nextPage, outClass, inClass)
  }

  var $pages = $('.pt-page'),
      current = 0,
      pagesCount = $pages.length,
      isAnimating = false,
      endCurrPage = false,
  		endNextPage = false,
      animEndEventName = getTransitionEnd(),
      keys = {
        LEFT: 37,
        RIGHT: 39
      },
      hammertime = new Hammer($('body')[0]);

  hammertime.on('swipeleft', function(ev){
    nextPage()
  })

  hammertime.on('swiperight', function(ev){
    prevPage()
  })

  $pages.each(function() {
    var $page = $(this)
    $page.data('originalClassList', $page.attr('class'))
  })

  $pages.eq(current).addClass('pt-page-current')

  $('body').on('keyup', function(ev){
    var key = ev.which

    if (key == keys.RIGHT) {
      nextPage()
    }

    if (key == keys.LEFT) {
      prevPage()
    }
  })

  $('[data-nav=jump]').on('click', function(ev){
    ev.preventDefault()

    var $link = $(this),
        index = $link.parent().index()

    if (index === current) {
      return
    }

    if (index > current) {
      nextPage(index)
    } else {
      prevPage(index)
    }
  })

  $('[data-nav=prev], [data-nav=next]').on('click', function(ev){
    ev.preventDefault()

    var $link = $(this),
        action = $link.data('nav')

    if (action === 'next') {
      nextPage()
    }

    if (action === 'prev') {
      prevPage()
    }
  })
})(jQuery)
