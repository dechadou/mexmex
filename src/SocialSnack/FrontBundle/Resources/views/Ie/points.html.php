<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Acumulación y redención de puntos PAYBACK - Cinemex');
$view['slots']->start('iebody');
?>
<h1 class="content-box-title"><span class="icon icon-light icon-crown no-txt"></span> Acumulación y redención de puntos PAYBACK</h1>

<div class="entry-body">
  <p>
    Con tu tarjeta Invitado Especial Cinemex PAYBACK podrás acumular Puntos PAYBACK por cada compra que realices en Cinemex®. Podrás utilizar tus puntos para comprar tus boletos o tus productos favoritos de la dulcería.
  </p>

  <div class="ie-points-txt">
    <div class="top">
      <p>
        <img src="<?php echo $view['assets']->getUrl('/assets/img/ie-points-ico1.png'); ?>" alt="PAYBACK" />
        <strong>Acumulación de Puntos PAYBACK:</strong> no olvides entregar al staff del cine tu tarjeta de Invitado Especial PAYBACK antes de realizar tu compra. De forma automática tus puntos se estarán cargando a tu cuenta.
      </p>
      <p>
        <img src="<?php echo $view['assets']->getUrl('/assets/img/ie-points-ico2.png'); ?>" alt="Invitado Especial" />
        <strong>Redención de Puntos PAYBACK:</strong> utilizar tus puntos es muy sencillo, entrega tu tarjeta de Invitado Especial en el punto de venta en donde quieras hacer uso de ellos e indica cuantos puntos quieres utilizar para pagar tus compras.
      </p>
    </div>

    <div class="bottom">
      <p>
        <img src="<?php echo $view['assets']->getUrl('/assets/img/ie-points-ico3.png'); ?>" alt="Información" />
        Para obtener un boleto de cine necesitarás tener el total en puntos del valor del boleto, mientras que para cualquier otro producto podrás utilizar el número de puntos que tengas disponibles o que desees, para pagar parte o la totalidad de tus consumos con puntos.
      </p>
    </div>
  </div>

  <p>
    Ahora Invitado Especial se une a PAYBACK, por lo cual se podrán utilizar los Puntos de cualquier monedero PAYBACK para comprar en Cinemex.
  </p>

  <h3>Valor de los Puntos PAYBACK</h3>
  <p>10 puntos PAYBACK equivalen a $1 peso para gastar en Cinemex </p>

  <p>
    <span class="payback-eq">10 puntos PAYBACK = $1 peso</span>
  </p>

  <h3>Vigencia de Puntos PAYBACK</h3>
  <p>
    Los puntos PAYBACK tienen una vigencia de 3 años siempre y cuando utilices tu tarjeta de Invitado Especial Cinemex PAYBACK, si no registras movimientos en tu tarjeta en el transcurso de un año vencerán tus puntos.
  </p>
</div>
<?php $view['slots']->stop(); ?>