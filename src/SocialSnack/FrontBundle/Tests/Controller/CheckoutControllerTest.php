<?php

namespace SocialSnack\FrontBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;

class CheckoutControllerTest extends WebTestCase {

  public function testCheckoutAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);

    $session = LoadSessionData::$sessions[0];
    $route = sprintf(
        '/checkout/%d/?ref=test',
        $session->getId()
    );
    $client = static::createClient();
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array(
            'HTTP_HOST' => $client->getContainer()->getParameter('domain'),
            'HTTP_X-Forwarded-Proto' => 'HTTPS'
        )
    );

    $this->assertSame(200, $client->getResponse()->getStatusCode());
  }


  public function testLegacyCheckoutAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);

    $session = LoadSessionData::$sessions[0];
    $route = sprintf(
        '/checkout/legacy/?session_id=%d&cinema_id=%d&ref=foobar&no-location=1',
        $session->getLegacyId(),
        $session->getLegacyCinemaId()
    );
    $client = static::createClient();
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array(
            'HTTP_HOST' => $client->getContainer()->getParameter('domain'),
            'HTTP_X-Forwarded-Proto' => 'HTTPS'
        )
    );

    $this->assertEquals(301, $client->getResponse()->getStatusCode());
    $this->assertRegexp('/^\/checkout\/' . $session->getId() . '\//', $client->getResponse()->headers->get('location'));
    $this->assertRegexp('/\bref=foobar\b/', $client->getResponse()->headers->get('location'));
    $this->assertRegexp('/#no-location$/', $client->getResponse()->headers->get('location'));
  }

}