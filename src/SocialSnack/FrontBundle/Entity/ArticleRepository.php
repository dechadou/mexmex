<?php
namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository {

  public function findRelated($article, $order = NULL, $limit = NULL) {

    $count = $this->createQueryBuilder('u')
        ->select('COUNT(u)')
        ->getQuery()
        ->getSingleScalarResult();

    $tags = explode(',',$article->getTags(true));
    $qb = $this->createQueryBuilder('a');


    if (!empty($tags)) {
      $or = $qb->expr()->orX();
      foreach($tags as $k => $v){
        $v = trim($v);
        $qb->setParameter('tags'.$k, "%$v%");
        $or->add($qb->expr()->like('a.tags', ':tags'.$k));
      }
      $qb->orWhere($or);
    }

    $qb->orwhere('a.id != :article_id')
        ->setParameter('article_id', $article->getId());
    if ( $limit ) {
      $qb->setMaxResults($limit);
    }
    $qb->setFirstResult(rand(0, $count - 1));
    $query = $qb->getQuery();

    $query->useResultCache( TRUE, 60 * 60 );
    return $query->execute();
  }

  public function findAdmin() {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('a')->from('SocialSnackFrontBundle:Article', 'a');
    $qb->orderBy('a.id', 'DESC');
    $query = $qb->getQuery();
    return $query->getResult();
  }

  public function findActive($active = 1){
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('a')->from('SocialSnackFrontBundle:Article','a');
    $qb->andWere('a.status = '.$active);
    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . (int)$active);
    return $query->getResult();
  }

  public function findManyBy( $ids, $criteria = array() ) {
    if ( 1 == sizeof( $ids ) ) {
      return $this->findBy( array_merge( array( 'id' => $ids[0] ), $criteria ) );
    }

    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect( 'm' )
        ->from( 'SocialSnackFrontBundle:Article', 'a' )
        ->where(
            $qb->expr()->in( 'a.id', $ids )
        );

    if ( $criteria ) {
      foreach ( $criteria as $k => $v ) {
        $qb->andWhere( 'a.' . $k . ' = :' . $k )
            ->setParameter( $k, $v );
      }
    }

    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
    return $query->getResult();
  }

  public function search($criteria)
  {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('a')
        ->from('SocialSnackFrontBundle:Article', 'a')
        ->andWhere('a.status = 1');
    $or = $qb->expr()->orX();
    $qb->setParameter(':criteria',"%$criteria%");
    $or->add($qb->expr()->like('a.content', ':criteria'));

    $qb->orWhere($or);

    // Apply where criterias.

    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60 );
    return $query->getResult();
  }

}
