<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:User:base.html.php');
$view['slots']->set( 'title', 'Invitado Especial - Cinemex' );
?>

<?php $view['slots']->start( 'section_banner' ); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/header-ie.jpg'); ?>" alt="Invitado Especial" />
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'user_body' ); ?>
<div class="content-box" id="user-content">
	<h1 class="content-box-title">Invitado Especial</h1>

  <?php
  echo $view->render(
      'SocialSnackFrontBundle::flashMsgs.html.php',
      array( 'flash_msgs' => $flash_msgs )
  );
  ?>

  <?php if (!$user->getIecode() || !$valid) { ?>
  <form action="" method="post" id="link-ie-form">
    <?php echo $this->render( 'SocialSnackFrontBundle:Partials:iecForm.html.php' ); ?>
  </form>
  <?php } else { ?>

  <?php if ( $info ) { ?>
  <dl class="movie-details-info">
    <dt>Invitado Especial N°</dt>
    <dd><?php echo $info->NUMIEC; ?> <a href="#" rel="ie-unlink">(desvincular)</a></dd>
    <dt>Nombre</dt>
    <dd><?php echo $info->PNOMBREIEC . ' ' . $info->SNOMBREIEC . ' ' . $info->APATERNOIEC . ' ' . $info->AMATERNOIEC; ?></dd>
    <dt>Puntos Invitado Especial</dt>
    <dd><?php echo $info->SALDO; ?></dd>
  </dl>
  <?php } ?>

  <?php if ( sizeof( $transactions ) ) { ?>
  <table class="std-table">
    <thead>
      <tr>
        <th>Fecha</th>
        <th>Complejo</th>
        <th>Origen</th>
        <th>Puntos</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ( $transactions as $t ) { ?>
      <tr>
        <td><?php echo $t->FECHA; ?></td>
        <td><?php echo $t->COMPLEJO; ?></td>
        <td><?php echo $t->ORIGEN; ?></td>
        <td class="align-right"><?php echo ( 'REDENCIONES' == $t->TIPOMOV ? '- ' : '' ) . $t->PUNTOS; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <?php } ?>

  <?php } ?>

  <div class="disclaimer">
    <p>La información mostrada considera únicamente las transacciones que has realizado
      con tu tarjeta en Cinemex, por lo que no se muestran las transacciones realizadas
      con los demás Socios de PAYBACK. Si deseas consultar el detalle de éstas, entra
      a <a href="http://www.payback.mx">payback.mx</a>.</p>
  </div>
</div>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start( 'js_after' ); ?>
<script>
$( '[rel="ie-unlink"]' ).bind( 'click', function( e ) {
  e.preventDefault();
  if ( confirm( '¿Deseas cancelar la asociación de tu cuenta con tu número de Invitado Especial?' ) ) {
    $( '#user-content' ).blockui();
    $.ajax( {
      url      : rest_url + 'me/iecode',
      type     : 'post',
      dataType : 'json',
      data     : { 's_method' : 'DELETE' },
      success  : function( res ) {
        setCookie('fetch_cache', 1);
        document.location.reload();
      },
      error    : function( res ) {
        alert( 'Ocurrió un error. Por favor, inténtalo nuevamente.' );
      }
    } )
  }
} );
</script>
<?php $view['slots']->stop(); ?>
