<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\SplashscreenType;
use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\Splashscreen;

/**
 * @Route("/splashscreen")
 */
class SplashscreenController extends BaseController
{
  protected function get_required_role()
  {
    return 'ROLE_CONTENT';
  }


  /**
   * @Route("/", name="admin_splashscreen_list")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction(){
    $this->check_roles();

    $repo_splash = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Splashscreen');
    $splash = $repo_splash->getList();

    return $this->render('SocialSnackAdminBundle:Splashscreen:list.html.php', array('splash' => $splash));
  }


  public function fileName(){
    $filePath = md5(rand() * time());
    return $filePath;
  }


  /**
   * @Route("/add", name="admin_splashscreen_add", defaults={"splash"=null})
   * @Route("/{splash_id}", name="admin_splashscreen_edit")
   * @ParamConverter("splash", options={"id"="splash_id"}, isOptional=true)
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function editAction(Splashscreen $splash = NULL, Request $request) {
    $this->check_roles();

    $session = $request->getSession();
    $em      = $this->get('doctrine')->getManager();

    if ('admin_splashscreen_add' === $request->attributes->get('_route')) {
      $splash = new Splashscreen();
    }

    $form = $this->createForm(new SplashscreenType(), $splash);
    $form->add('save', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      foreach (['v', 'h'] as $orientation) {
        $path = 'image_' . $orientation;
        $setter = 'setImage' . ucfirst($orientation);

        if ($file = $form->get($path)->getData()){
          $base_path = 'splashscreen/';
          $filename  = $this->fileName();
          $ext       = substr(strrchr($file->getClientOriginalName(), "."), 1);
          $this->container->get('ss.front.utils')->upload_cms_content($filename.".".$ext, file_get_contents($file->getPathName()), $base_path);
          $purge_files[] = '/' . $this->container->getParameter('cms_upload_path') . $base_path . $filename.".".$ext;
          $splash->{$setter}($base_path . $filename . '.' . $ext);
        }
      }

      $em->persist($splash);
      $em->flush();

      $cache_purger = $this->get('ss.cache_purger');
      $cache_purger->enqueue(array('entity' => $splash));
      $purge = $cache_purger->purge_cache();

      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);

      return $this->redirect($this->generateUrl('admin_splashscreen_edit', array('splash_id' => $splash->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Splashscreen:edit.html.php', array(
        'splash' => $splash,
        'form' => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }


  /**
   * @Route("/delete/{splash_id}", name="admin_splashscreen_delete")
   * @ParamConverter("splash", options={"id"="splash_id"})
   *
   * @param Splashscreen $splash
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteAction(Splashscreen $splash) {
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $em->remove($splash);
    $em->flush();

    return $this->redirect($this->generateUrl('admin_splashscreen_list'));
  }
}