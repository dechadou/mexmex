<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class SessionRepository extends EntityRepository {
	
  
  /**
   * 
   * @param int $cinema_id
   * @param int $area_id
   * @param int $state_id
   * @return type
   */
	protected function _findAvailableDates( $cinema_id = NULL, $area_id = NULL, $state_id = NULL ) {
		$cache_id = __METHOD__ . ':' . implode(',', [$cinema_id, $area_id, $state_id]);

		/* @todo Define the right timezone */
//		date_default_timezone_set("UTC");
		
		$date_from = new \DateTime( 'now' );
		
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb
				->addSelect( 's.date' )
				->from( 'SocialSnackWsBundle:Session', 's' )
				->where( 's.date_time >= :date_from' )
				->andWhere( 's.active = 1' )
				->setParameter( 'date_from', $date_from->format( 'Y-m-d H:i:s' ) );
		
		if ( $cinema_id ) {
			$qb->andWhere( 's.cinema = :cinema_id' )
					->setParameter( 'cinema_id', $cinema_id );
		}
    
    if ( $area_id ) {
			$qb->innerJoin( 's.cinema', 'c' )
          ->andWhere( 'c.area = :area_id' )
					->setParameter( 'area_id', $area_id );
    } elseif ( $state_id ) {
      $qb->innerJoin( 's.cinema', 'c' )
          ->andWhere( 'c.state = :state_id' )
					->setParameter( 'state_id', $state_id );
    }
		
		$qb
				->groupBy( 's.date' )
				->orderBy( 's.date' );
		
		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60,  $cache_id );
		return $query->getResult();
	}
	
	
	public function findAvailableDates() {
		return $this->_findAvailableDates();
	}
	
	
	public function findAvailableDatesByArea( $area_id ) {
		return $this->_findAvailableDates( NULL, $area_id );
	}
	
  
	public function findAvailableDatesByState( $state_id ) {
		return $this->_findAvailableDates( NULL, NULL, $state_id );
	}
	
  
	public function findAvailableDatesByCinema( $cinema_id ) {
		return $this->_findAvailableDates( $cinema_id );
	}
	
	
	public function findWithMovie( $id ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 's' )
				->addSelect( 'm' )
				->from( 'SocialSnackWsBundle:Session', 's' )
				->innerJoin( 's.movie', 'm' )
				->where( 's.id = :session_id' )
				->setParameter( 'session_id', $id );
		
		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 15 * 60, __METHOD__ . ':' . $id );

    try {
      return $query->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
	}
	
	
	
	public function findWithCinema( $id ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 's' )
				->addSelect( 'c' )
				->from( 'SocialSnackWsBundle:Session', 's' )
				->innerJoin( 's.cinema', 'c' )
				->where( 's.id = :session_id' )
				->setParameter( 'session_id', $id );
		
		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 10 * 60, __METHOD__ . ':' . $id );

    try {
      return $query->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
	}
	
	
	
	public function findWithMovieAndCinema( $id ) {
		$qb = $this->getEntityManager()->createQueryBuilder()
				->addSelect( 's' )
				->addSelect( 'm' )
				->addSelect( 'c' )
				->from( 'SocialSnackWsBundle:Session', 's' )
				->innerJoin( 's.movie', 'm' )
				->innerJoin( 's.cinema', 'c' )
				->where( 's.id = :session_id' )
				->setParameter( 'session_id', $id );
		
		$query = $qb->getQuery();
    $query->useResultCache( TRUE, 10 * 60, __METHOD__ . ':' . $id );

    try {
      return $query->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
	}
	
	
  /**
   * Find tickets for a session and assign them to the tickets property of the
   * Session entity.
   * 
   * @param \SocialSnack\WsBundle\Entity\Session $session
   */
	public function _populate_tickets( &$session ) {
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb
				->select('t')
				->from('SocialSnackWsBundle:Ticket', 't')
				->andWhere('t.cinema = :cinema')
				->andWhere('t.legacy_id = :price_code')
				->setParameter('cinema', $session->getCinema())
				->setParameter('price_code', $session->getData('PRICECODE'))
		;
		$q = $qb->getQuery();

		$q->useResultCache(TRUE, 5 * 60, __METHOD__ . ':' . $session->getCinema()->getId() . ':' . $session->getData('PRICECODE'));

		$session->setTickets($q->getResult());
	}
	
	
  /**
   * Find a session by ID and populate the tickets collection.
   * 
   * @param int $id
   * @return \SocialSnack\WsBundle\Entity\Session
   */
	public function findWithTickets( $id ) {
		$session = $this->find( $id );

    if (!$session) {
      return NULL;
    }

		$this->_populate_tickets( $session );
		
		return $session;
	}
	
	
  /**
   * Find a session by ID and populate the cinema relationship and the tickets
   * collection.
   * 
   * @param int $id
   * @return \SocialSnack\WsBundle\Entity\Session
   */
	public function findWithCinemaAndTickets( $id ) {
		$session = $this->findWithCinema( $id );

    if (!$session) {
      return NULL;
    }

    $this->_populate_tickets( $session );
		
		return $session;
	}
	
	
  /**
   * Find a session by ID and populate the cinema and movie relationships and
   * the tickets collection.
   * 
   * @param int $id
   * @return \SocialSnack\WsBundle\Entity\Session
   */
	public function findWithMovieAndCinemaAndTickets( $id ) {
		$session = $this->findWithMovieAndCinema( $id );

    if (!$session) {
      return NULL;
    }

    $this->_populate_tickets( $session );
		
		return $session;
	}

  
  public function findForMovieInCinema($movie_id, $cinema_id) {
    $qb = $this->createQueryBuilder('s');
    $qb->addSelect('s')
        ->andWhere('s.cinema = :cinema_id')->setParameter('cinema_id', $cinema_id)
        ->andWhere('s.movie  = :movie_id')->setParameter('movie_id', $movie_id)
        ->andWhere('s.active = 1')
        ->orderBy('s.date_time', 'ASC');
    
    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 15 * 60, 'findForMovieInCinema_' . $movie_id . '|' . $cinema_id);
    return $query->getResult();
    
  }
 
  
  /**
   * Find sessions matching a session type.
   * 
   * @param string $type
   * @param array $criteria
   * @param array|null $orderBy
   * @param int|null $limit
   * @param int|null $offset
   * @return array
   */
  public function findByType($type, array $criteria, array $orderBy = null, $limit = null, $offset = null) {
    $qb = $this->createQueryBuilder('s');
    $qb->andWhere($qb->expr()->like('s.types', ':type'))
        ->andWhere('s.active = 1')
        ->setParameter('type', '%"' . $type . '"%');
    
    /** @todo Implement this arguments */
    if ( !empty($criteria) || !is_null($orderBy) || !is_null($offset) ) {
      throw new \Exception('Unimplemented parameter :D');
    }
    
    if ( !is_null($limit) ) {
      $qb->setMaxResults($limit);
    }
    
    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 15 * 60, 'Session:findByType_' . $type);
    
    if ( 1 === $limit ) {
      try {
        return $query->getSingleResult();
      } catch (NoResultException $e) {
        return NULL;
      }
    } else {
      return $query->getResult();
    }
  }
  
  
}