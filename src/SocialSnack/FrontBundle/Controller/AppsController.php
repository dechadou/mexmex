<?php

namespace SocialSnack\FrontBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppsController extends Controller {

  /**
   * mobileAction function.
   *
   * @Route("/appios", name="appios")
   * @Template(engine="php")
   *
   * @access public
   * @param Request $request
   * @return Response
   */
  public function mobileIpadAction(Request $request) {
    $redirect_params = array();
    if ($request->query->has('redirect_url')) {
      $redirect_params['redirect_url'] = $request->query->get('redirect_url');
    }

    $response = $this->render('SocialSnackFrontBundle:Mobile:appIos.html.php',
        array(
            'route' => $request->attributes->get('_route'),
            'redirect_params' => $redirect_params,
        )
    );

    $response->setPublic();
    $response->setSharedMaxAge(24 * 60 * 60);
    $response->setMaxAge(60 * 60);
    return $response;
  }

  /**
   * mobileAction function.
   *
   * @Route("/appandroid", name="appandroid")
   * @Template(engine="php")
   *
   * @access public
   */
  public function mobileAndroidAction(Request $request) {
    $redirect_params = array();
    if ($request->query->has('redirect_url')) {
      $redirect_params['redirect_url'] = $request->query->get('redirect_url');
    }

    $response = $this->render('SocialSnackFrontBundle:Mobile:appAndroid.html.php',
        array(
            'route'  => $this->getRequest()->attributes->get('_route'),
            'redirect_params' => $redirect_params,
        )
    );

    $response->setPublic();
    $response->setSharedMaxAge(24 * 60 * 60);
    $response->setMaxAge(60 * 60);
    return $response;
  }

  /**
   * mobileAction function.
   *
   * @Route("/appwindows", name="appwindows")
   * @Template(engine="php")
   *
   * @access public
   */
  public function mobileWindowsAction(Request $request) {
    $redirect_params = array();
    if ($request->query->has('redirect_url')) {
      $redirect_params['redirect_url'] = $request->query->get('redirect_url');
    }

    $response = $this->render('SocialSnackFrontBundle:Mobile:appWindows.html.php',
        array(
            'route'  => $this->getRequest()->attributes->get('_route'),
            'redirect_params' => $redirect_params,
        )
    );

    $response->setPublic();
    $response->setSharedMaxAge(24 * 60 * 60);
    $response->setMaxAge(60 * 60);
    return $response;
  }

}