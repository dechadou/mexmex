$.cookie.defaults.path = '/';

/**
 *
 * @param {type} name
 * @param {type} value
 * @param {type} exdays
 * @returns {undefined}
 */
function setCookie(name, value, exdays) {
  return $.cookie(name, value, {
    expires : exdays,
    path    : '/'
  });
}


/**
 *
 * @param {type} name
 * @returns {undefined}
 */
function unsetCookie(name) {
  return $.removeCookie(name);
}


/**
 *
 * @param {type} name
 * @returns {mixed}
 */
function getCookie(name) {
  return $.cookie(name);
}
