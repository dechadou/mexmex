<?php

namespace SocialSnack\WsBundle\Tests\Handler;
use SocialSnack\WsBundle\Handler\LockHandler;

/**
 * Class LockHandlerTest
 * @package SocialSnack\WsBundle\Tests\Handler
 * @author Guido Kritz
 */
class LockHandlerTest extends \PHPUnit_Framework_TestCase {

  static $path = '/tmp/testlock/';

  static $handler;

  protected $logger;

  public function setUp() {
    self::$handler = $this->getHandler();
  }


  protected function tearDownLockTests() {
    $path = static::$path;
    if ($dh = @opendir($path)) {
      while ($file = readdir($dh)) {
        if ($file === '.' || $file === '..') {
          continue;
        }

        unlink($path . $file);
      }
      rmdir($path);
    }
  }


  protected function setUpLockTests() {
    $path = static::$path;
    $this->tearDownLockTests();
    mkdir($path);
  }


  protected function tearDown() {
    $this->tearDownLockTests();
    parent::tearDown();
  }


  protected function getHandler() {
    $this->logger = $this->getMockBuilder('\Monolog\Logger')
        ->disableOriginalConstructor()
        ->getMock();

    return new LockHandler(self::$path, $this->logger);
  }


  protected function getHandlerMock(array $methods = NULL) {
    return $this->getMockBuilder('\SocialSnack\WsBundle\Handler\LockHandler')
        ->setMethods($methods)
        ->setConstructorArgs([static::$path, $this->logger])
        ->getMock();
  }


  /**
   * @todo Testing multiple methods in a single test is a bad practice. Find a way to improve this...
   */
  public function testSetCheckReleaseLock() {
    $this->setUpLockTests();

    $lock_id = 'testlock';
    $this->assertFalse(static::$handler->isLocked($lock_id));
    static::$handler->setLock($lock_id);
    $this->assertTrue(static::$handler->isLocked($lock_id));
    static::$handler->releaseLock($lock_id);
    $this->assertFalse(static::$handler->isLocked($lock_id));
  }


  public function testCheckLockTimeOK() {
    $handler = $this->getHandlerMock([
        'getFileModificationTime',
        'getAlertTime'
    ]);

    $handler->expects($this->once())
        ->method('getFileModificationTime')
        ->willReturn(time() - 10);

    $handler->expects($this->once())
        ->method('getAlertTime')
        ->willReturn(3600);

    $output = $handler->checkLockTime('foobar');
    $this->assertTrue($output);
  }


  public function testCheckLockTimeAlert() {
    $this->logger->expects($this->once())
        ->method('critical');

    $handler = $this->getHandlerMock([
        'getFileModificationTime',
        'getAlertTime'
    ]);

    $handler->expects($this->once())
        ->method('getFileModificationTime')
        ->willReturn(time() - 4000);

    $handler->expects($this->once())
        ->method('getAlertTime')
        ->willReturn(3600);

    $output = $handler->checkLockTime('foobar');
    $this->assertFalse($output);
  }


  public function testSetLockException() {
    $this->setUpLockTests();

    $lock_id = 'testlock';
    static::$handler->setLock($lock_id);
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\ResourceLockedException');
    static::$handler->setLock($lock_id);
  }

}