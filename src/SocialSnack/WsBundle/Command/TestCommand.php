<?php

namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

use SocialSnack\WsBundle\Request\Arco\MuestraCatalogos;

class TestCommand extends ContainerAwareCommand {
  
  
	protected function configure() {
		$this
			->setName( 'ws:test' )
			->setDescription( 'Test' );
	}
	
  
	protected function execute( InputInterface $input, OutputInterface $output ) {
    $req = $this->getContainer()->get('cinemex_ws');
    $req->init('Arco\MuestraCatalogos');
    $res = $req->request(array(
        'catalogo' => MuestraCatalogos::CATALOGO_DERECHO_ACC
    ));
    foreach ($res as $r) {
      var_dump($r);
    }
  }
  
}