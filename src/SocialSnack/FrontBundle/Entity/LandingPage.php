<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LandingPage
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class LandingPage
{

    const TYPE_IMAGE   = 'image';
    const TYPE_HTML    = 'html';
    const TYPE_MOVIES  = 'movies';
    const TYPE_CINEMAS = 'cinemas';
    const TYPE_PROMOS  = 'promos';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var array
     *
     * @ORM\Column(name="content", type="json_array")
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var Landing
     *
     * @ORM\ManyToOne(targetEntity="Landing", inversedBy="pages")
     */
    private $landing;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;


    public function __construct()
    {
        $this->content = [];
        $this->position = 1;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return LandingPage
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LandingPage
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param array $content
     * @return LandingPage
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return LandingPage
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set landing
     *
     * @param \SocialSnack\FrontBundle\Entity\Landing $landing
     * @return LandingPage
     */
    public function setLanding(\SocialSnack\FrontBundle\Entity\Landing $landing = null)
    {
        $this->landing = $landing;
    
        return $this;
    }

    /**
     * Get landing
     *
     * @return \SocialSnack\FrontBundle\Entity\Landing 
     */
    public function getLanding()
    {
        return $this->landing;
    }

    public function setContentValue($key, $value)
    {
        $this->content[$key] = $value;
    }

    public function getContentValue($key)
    {
        if (!isset($this->content[$key]))
        {
            return NULL;
        }

        return $this->content[$key];
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return LandingPage
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}