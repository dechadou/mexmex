<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Forma de Derechos ARCO - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Forma de Derechos ARCO</h1>

<p class="msg">Su requerimiento con folio <?php echo $ref; ?> ha sido enviado. Gracias.</p>
<?php $view['slots']->stop(); ?>