<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div id="order-success">
  <h1>¡Ya tienes tus boletos!</h1>
  <?php if ( $view['fronthelper']->cinema_has_qr($transaction->getCinema()) ) { ?>
  <div class="order-w-qr clearfix">
    <div class="clearfix">
      <h3 class="left">
        Imprime tu código QR y pasa directamente al acceso de la sala y evita la taquilla.
      </h3>
      <?php if ( $show_print ) { ?>
      <div class="right">
        <a class="btn btn-default btn100 btn-icon icon icon-print" href="<?php echo $print_url; ?>" onclick="javascript:window.open(this.href,'cinemex_print','menubar=1,resizable=1,width=778,height=500');return false;">Imprimir</a>
      </div>
      <?php } ?>
    </div>
    <div class="sep">
      <h4 class="left line-01-top">
        Guarda el código QR o lleva una impresión contigo para presentarlo directamente en el acceso de tu sala.
      </h4>
      <div class="right order-qr-code">
        <?php if ( isset( $qr_url ) ) { ?>
        <img src="<?php echo $view['assets']->getUrl($qr_url, 'QR'); ?>" alt="" />
        <?php } ?>
      </div>
    </div>
    <h5 class="left line-01-top">
      También puedes pasar por la taquilla con tu tarjeta de crédito o con tu código de compra: <strong><?php if ( isset( $code ) ) echo $code; ?></strong>
    </h5>
  </div>
  
  <?php } else { ?>
  <div class="order-wo-qr">
    <p>
      Te hemos enviado también un correo electrónico con la confirmación y datos de tu compra.
    </p>
    <p>
      Puedes pasar por tus boletos a taquilla indicando el código de compra o con tarjeta con la que hiciste tu compra.
    </p>

    <div class="order-codes line-01-top">
      <div class="order-code">
        <span class="legend">Código de compra</span>
        <span class="code" id="order-code">
          <?php if ( isset( $code ) ) echo $code; ?>
        </span>
      </div>
      <div class="order-code line-01-left">
        <span class="legend">Código QR</span>
        <div id="order-qr-code">
          <?php if ( isset( $qr_url ) ) { ?>
          <img src="<?php echo $view['assets']->getUrl($qr_url, 'QR'); ?>" alt="" />
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

  <div class="order-summary line-01-top">
    <h2>Datos de la compra</h2>
    <img src="<?php echo $view['fronthelper']->get_poster_url($session->getMovie()->getPosterUrl(), '95x142'); ?>" alt="<?php echo $session->getMovie()->getName(); ?>" width="95" height="142" class="order-success-cover" />
    <dl class="movie-details-info">
      <dt>Título</dt>
      <dd><?php echo $session->getMovie()->getName(); ?></dd>
      
      <?php $type = FrontHelper::get_movie_attr_str( $session->getMovie()->getAttr(), ', ' ); ?>
      <?php if ( $type ) { ?>
      <dt>Versión</dt>
      <dd><?php echo $type; ?></dd>
      <?php } ?>
      
      <dt>Día</dt>
      <dd><?php echo FrontHelper::get_session_nice_date( $session->getDate() ); ?></dd>
      
      <dt>Horario</dt>
      <dd><?php echo $session->getTime()->format( 'h:i A' ); ?></dd>
      
      <dt>Cine</dt>
      <dd><?php echo $session->getCinema()->getName(); ?></dd>
    </dl>
    
    <dl class="movie-details-info" id="success-order-info">
      <dt>Sala</dt>
      <dd><?php echo FrontHelper::confirmation_get_screenname($transaction); ?></dd>
      
      <dt class="step-1 dt-row">Tus boletos</dt>
      <dd class="step-1"><?php echo FrontHelper::confirmation_get_ticketslist($transaction, '<br/>'); ?></dd>

      <?php if ( $transaction->getSeats() ) { ?>
      <dt class="step-2 dt-row">Asientos seleccionados</dt>
      <dd><?php echo FrontHelper::confirmation_get_seatslist($transaction, ' / '); ?></dd>
      <?php } ?>
      
      <dt>Total pagado</dt>
      <dd><?php echo FrontHelper::format_price( $transaction->getAmount() ); ?></dd>
    </dl>
  </div>

  <?php if ( $show_print ) { ?>
  <div class="line-01-top overline-btns">
    <a class="btn btn-icon icon icon-print" href="<?php echo $print_url; ?>" onclick="javascript:window.open(this.href,'cinemex_print','menubar=1,resizable=1,width=778,height=500');return false;">Imprimir</a>
  </div>
  <?php } ?>

</div>