<?php
namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ModalRepository extends EntityRepository {

  public function getList($order=null){
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('a')->from('SocialSnackFrontBundle:Modal', 'a');
    $qb->orderBy('a.id', 'DESC');
    $query = $qb->getQuery();
    return $query->getResult();
  }

  public function findActive($data) {

    $route = isset($data['route']) ? $data['route'] : NULL;

    if ($route != NULL){

      $state_id = isset($data['data']['state_id']) ? "stateid-".$data['data']['state_id'] : NULL;
      $area_id = isset($data['data']['area_id']) ? "areaid-".$data['data']['area_id'] : NULL;
      $cinema_id = isset($data['data']['cinema_id']) ? "cinemaid-".$data['data']['cinema_id'] : NULL;

      if ($route != 'cinema_single'){
        $sql = "SELECT m.* FROM Modal m WHERE";
      } else {
        $sql = "SELECT m.*, t.modal_id,t.target FROM Modal m, ModalTargets t WHERE t.modal_id = m.id";
      }

      switch($route){
        case 'cinemas_archive':
          $sql .= " m.section LIKE '%cinemas_map%'";
          break;
        case 'cinema_single':
          $sql .= " AND (t.target = '$state_id'";
          $sql .= " OR t.target = '$area_id'";
          $sql .= " OR t.target = '$cinema_id')";
          break;
        default:
          $sql .= " m.section LIKE '%$route%'";
      }

      $sql .= ' AND m.status = 1 GROUP BY m.id';

      $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();


    } else {
      return false;
    }
  }


}
