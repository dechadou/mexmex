<?php if ($required && empty($empty_value) && !$empty_value_in_choices): ?>
  <?php $required = false; ?>
<?php endif; ?>

<select <?php echo $view['form']->block($form,'widget_attributes');
  if ($multiple) : ?> multiple="multiple"<?php endif;
  if ($required): ?> data-validators="not_empty"<?php endif; ?>>

  <?php if (!empty($empty_value)): ?>
  <option value="" <?php
    if ($required && empty($value)) : ?> selected="selected"<?php endif; ?>><?php echo $empty_value; ?></option>
  <?php endif ?>

  <?php
  if (sizeof($preferred_choices) > 0) :
    $options = $preferred_choices;

    echo $view['form']->block($form,'choice_widget_options');
  ?>
    <?php if (sizeof($choices) > 0 && !empty($separator)): ?>
      <option disabled="disabled"><?php echo $separator; ?></option>
    <?php endif; ?>
  <?php
  endif;

  $options = $choices;
  echo $view['form']->block($form,'choice_widget_options');
  ?>
</select>
