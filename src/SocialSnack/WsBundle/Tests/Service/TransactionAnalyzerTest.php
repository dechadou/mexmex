<?php

namespace SocialSnack\WsBundle\Tests\Service;

use SocialSnack\RestBundle\Entity\App;
use SocialSnack\WsBundle\Service\TransactionAnalyzer;

class TransactionAnalyzerTest extends \PHPUnit_Framework_TestCase {


  protected function getService() {
    $doctrine = $this->getMockBuilder('\Doctrine\Bundle\DoctrineBundle\Registry')
        ->disableOriginalConstructor()
        ->getMock();
    return new TransactionAnalyzerTesteable($doctrine);
  }


  protected function getServiceMock(array $methods = []) {
    $service = $this->getMockBuilder('\SocialSnack\WsBundle\Tests\Service\TransactionAnalyzerTesteable')
        ->disableOriginalConstructor()
        ->setMethods($methods)
        ->getMock();
    return $service;
  }


  public function testGetHourlyCompareDates() {
    $startTime = new \DateTime('2015-01-08 13:10:01');
    $service = $this->getService();
    list($time_from, $time_to, $comp_time_from, $comp_time_to) = $service->getHourlyCompareDates($startTime);

    $this->assertEquals(new \DateTime('2015-01-08 12:00:00'), $time_from);
    $this->assertEquals(new \DateTime('2015-01-08 13:00:00'), $time_to);
    $this->assertEquals(new \DateTime('2015-01-01 12:00:00'), $comp_time_from);
    $this->assertEquals(new \DateTime('2015-01-01 13:00:00'), $comp_time_to);
  }


  protected function _testAnalyzeTransactionsByApp(array $transactions) {
    $service = $this->getServiceMock([
        'getApps',
        'getComparableTransactions',
        'getHourlyCompareDates',
    ]);

    $app_class = '\SocialSnack\RestBundle\Entity\App';

    $app1 = $this->getMock($app_class);
    $app1->method('getAppId')->willReturn('abc');

    $app2 = $this->getMock($app_class);
    $app2->method('getAppId')->willReturn('def');

    $app3 = $this->getMock($app_class);
    $app3->method('getAppId')->willReturn('ghi');

    $service->expects($this->once())
        ->method('getApps')
        ->willReturn([
            $app1,
            $app2,
            $app3,
        ]);

    $service->expects($this->once())
        ->method('getComparableTransactions')
        ->willReturn($transactions);

    return $service->analyzeTransactionsByApp();
  }


  public function testAnalyzeTransactionsByAppNoAlerts() {
    $transactions = [
        [
            ['app_id' => 'abc', 'date' => '2015-01-08 12:00:00', 'amount' => 0],
            ['app_id' => 'def', 'date' => '2015-01-08 12:00:00', 'amount' => 1],
            ['app_id' => 'ghi', 'date' => '2015-01-08 12:00:00', 'amount' => 2],
        ],
        [
            ['app_id' => 'abc', 'date' => '2015-01-01 12:00:00', 'amount' => 5],
            ['app_id' => 'def', 'date' => '2015-01-01 12:00:00', 'amount' => 21],
            ['app_id' => 'ghi', 'date' => '2015-01-01 12:00:00', 'amount' => 20],
        ]
    ];
    $output = $this->_testAnalyzeTransactionsByApp($transactions);

    $this->assertCount(0, $output);
  }


  public function testAnalyzeTransactionsByAppAlerts() {
    $transactions = [
        [
//            ['app_id' => 'abc', 'date' => '2015-01-08 12:00:00', 'amount' => 0],
            ['app_id' => 'def', 'date' => '2015-01-08 12:00:00', 'amount' => 1],
            ['app_id' => 'ghi', 'date' => '2015-01-08 12:00:00', 'amount' => 0],
        ],
        [
            ['app_id' => 'abc', 'date' => '2015-01-01 12:00:00', 'amount' => 19],
            ['app_id' => 'def', 'date' => '2015-01-01 12:00:00', 'amount' => 21],
            ['app_id' => 'ghi', 'date' => '2015-01-01 12:00:00', 'amount' => 20],
        ]
    ];
    $output = $this->_testAnalyzeTransactionsByApp($transactions);

    $this->assertCount(2, $output);
  }

  protected function _testAnalyzeTransactionsByCinema(array $transactions) {
    $service = $this->getServiceMock([
        'getCinemas',
        'getComparableTransactions',
        'getHourlyCompareDates',
    ]);

    $cinema_class = '\SocialSnack\WsBundle\Entity\Cinema';

    $cinema1 = $this->getMock($cinema_class);
    $cinema1->method('getId')->will($this->returnValue(1));

    $cinema2 = $this->getMock($cinema_class);
    $cinema2->method('getId')->willReturn(2);

    $cinema3 = $this->getMock($cinema_class);
    $cinema3->method('getId')->willReturn(3);

    $service->expects($this->once())
        ->method('getCinemas')
        ->willReturn([
            $cinema1,
            $cinema2,
            $cinema3,
        ]);

    $service->expects($this->once())
        ->method('getComparableTransactions')
        ->willReturn($transactions);

    return $service->analyzeTransactionsByCinema();
  }


  public function testAnalyzeTransactionsByCinemaNoAlerts() {
    $transactions = [
        [
            ['cinema_id' => 1, 'date' => '2015-01-08 12:00:00', 'amount' => 0],
            ['cinema_id' => 2, 'date' => '2015-01-08 12:00:00', 'amount' => 1],
            ['cinema_id' => 3, 'date' => '2015-01-08 12:00:00', 'amount' => 2],
        ],
        [
            ['cinema_id' => 1, 'date' => '2015-01-01 12:00:00', 'amount' => 5],
            ['cinema_id' => 2, 'date' => '2015-01-01 12:00:00', 'amount' => 21],
            ['cinema_id' => 3, 'date' => '2015-01-01 12:00:00', 'amount' => 20],
        ]
    ];
    $output = $this->_testAnalyzeTransactionsByCinema($transactions);

    $this->assertCount(0, $output);
  }


  public function testAnalyzeTransactionsByCinemaAlerts() {
    $transactions = [
        [
//            ['cinema_id' => 1, 'date' => '2015-01-08 12:00:00', 'amount' => 0],
            ['cinema_id' => 2, 'date' => '2015-01-08 12:00:00', 'amount' => 1],
            ['cinema_id' => 3, 'date' => '2015-01-08 12:00:00', 'amount' => 0],
        ],
        [
            ['cinema_id' => 1, 'date' => '2015-01-01 12:00:00', 'amount' => 19],
            ['cinema_id' => 2, 'date' => '2015-01-01 12:00:00', 'amount' => 21],
            ['cinema_id' => 3, 'date' => '2015-01-01 12:00:00', 'amount' => 20],
        ]
    ];
    $output = $this->_testAnalyzeTransactionsByCinema($transactions);

    $this->assertCount(2, $output);
  }


  public function testAnalyzeTransactions_RunAtDay() {
    $service = $this->getServiceMock([
        'analyzeTransactionsByApp',
        'analyzeTransactionsByCinema',
        'getCurrentTime'
    ]);

    $service->expects($this->once())
        ->method('getCurrentTime')
        ->willReturn(1300);

    $service->expects($this->once())
        ->method('analyzeTransactionsByApp')
        ->willReturn(['foo']);

    $service->expects($this->once())
        ->method('analyzeTransactionsByCinema')
        ->willReturn(['bar']);

    $output = $service->analyzeTransactions();

    $this->assertEquals($output, ['foo', 'bar']);
  }


  public function testAnalyzeTransactions_DontRunAtNight() {
    $service = $this->getServiceMock([
        'analyzeTransactionsByApp',
        'analyzeTransactionsByCinema',
        'getCurrentTime'
    ]);

    $service->expects($this->once())
        ->method('getCurrentTime')
        ->willReturn(200);

    $output = $service->analyzeTransactions();

    $this->assertEquals($output, []);
  }

}


class TransactionAnalyzerTesteable extends TransactionAnalyzer {

  public function getHourlyCompareDates(\DateTime $startTime) {
    return parent::getHourlyCompareDates($startTime);
  }

}