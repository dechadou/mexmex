<?php $view->extend('FOSUserBundle::layout.html.php'); ?>

{% trans_default_domain 'FOSUserBundle' %}

<?php $view['slots']->start( 'fos_user_content' ); ?>
<p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
{% if app.session is not empty %}
		{% set targetUrl = app.session.get('_security.' ~ app.security.token.providerKey ~ '.target_path') %}
		{% if targetUrl is not empty %}<p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>{% endif %}
{% endif %}
<?php $view['slots']->stop(); ?>