<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\State;

class LoadStatesData extends AbstractFixture implements OrderedFixtureInterface {

  static $states;

  public function load(ObjectManager $manager) {
    self::$states = [];

    $state1 = new State();
    $state1
        ->setName('D.F.')
        ->setLegacyName('DF')
        ->setLegacyId(1001)
        ->setActive(TRUE)
        ->setOrder(1)
        ->setTimezone('America/Mexico_City')
    ;
    $this->addReference('State-1', $state1);
    self::$states[] = $state1;
    $manager->persist($state1);

    $state2 = new State();
    $state2
        ->setName('Monterrey')
        ->setLegacyName('MONTERREY')
        ->setLegacyId(1002)
        ->setActive(TRUE)
        ->setOrder(2)
        ->setTimezone('America/Mexico_City')
    ;
    $this->addReference('State-2', $state2);
    self::$states[] = $state2;
    $manager->persist($state2);

    $manager->flush();
  }

  public function getOrder() {
    return 1;
  }

}