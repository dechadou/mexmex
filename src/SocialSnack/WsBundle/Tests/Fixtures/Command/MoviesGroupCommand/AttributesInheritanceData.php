<?php

namespace SocialSnack\WsBundle\Tests\Fixtures\Command\MoviesGroupCommand;

use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;

class AttributesInheritanceData extends AbstractLoadData {

  /**
   * Each movie version has different attributes.
   * One movie version has no sessions.
   * Grouper has version and non-version attributes.
   *
   * Expected test result: grouper should keep non-version attributes, inherit attributes from movies with sessions and
   * loose the attributes from the movie without sessions.
   *
   * @param ObjectManager   $om
   * @param Faker\Generator $faker
   */
  protected function setUpMovies(ObjectManager $om, Faker\Generator $faker) {
    $movies = [];

    for ($i = 0 ; $i < 4 ; $i++) {
      $date = $faker->dateTimeBetween('+1 days', '+1 years');

      $movie = new Movie();
      $movie
          ->setLegacyId($faker->randomNumber())
          ->setActive(TRUE)
          ->setFeatured(FALSE)
          ->setFixedPosition(0)
          ->setInfo('{}')
          ->setLegacyIdBis($faker->word())
          ->setLegacyName($faker->title())
          ->setName($faker->title())
          ->setParent(NULL)
          ->setPosition(0)
          ->setPosterUrl($faker->imageUrl())
          ->setShortName($faker->title())
          ->setPremiere(FALSE)
          ->setGroupId('foobar');
      $om->persist($movie);
      $movies[] = $movie;
    }

    for ($i = 0 ; $i < 2 ; $i++) {
      $session = new Session();
      $session
          ->setLegacyId($faker->randomNumber())
          ->setPlatinum(FALSE)
          ->setInfo('{}')
          ->setActive(TRUE)
          ->setCinema($this->cinema)
          ->setCinemom(FALSE)
          ->setDate($date)
          ->setDateTime($date)
          ->setExtreme(FALSE)
          ->setLegacyCinemaId($this->cinema->getLegacyId())
          ->setLegacyId($faker->randomNumber())
          ->setMovie($movies[$i])
          ->setGroupId($movies[$i]->getGroupId())
          ->setPremium(FALSE)
          ->setTime($date)
          ->setTz('America/Mexico_City')
          ->setUpdating(FALSE);
      $om->persist($session);
    }

    $movies[0]->setType('single');
    $movies[1]->setType('single');
    $movies[2]->setType('single');
    $movies[0]->setParent($movies[3]);
    $movies[1]->setParent($movies[3]);
    $movies[2]->setParent($movies[3]);
    $movies[0]->setAttr('v4d', TRUE);
    $movies[0]->setAttr('premium', TRUE);
    $movies[1]->setAttr('v3d', TRUE);
    $movies[2]->setAttr('platinum', TRUE);

    $movies[3]->setType('grouper');
    $movies[3]->setAttr('platinum', TRUE);
    $movies[3]->setAttr('premium', TRUE);
    $movies[3]->setAttr('v3d', TRUE);
    $movies[3]->setAttr('nfl', TRUE);
  }

}