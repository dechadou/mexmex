<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Encuesta</title>

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,400italic,700,900">

    <style>
    html {
      box-sizing:border-box;
    }

    *,
    *:before,
    *:after {
      box-sizing:inherit;
    }

    body {
      background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/bg.png'); ?>) no-repeat 100% 0;
      color:#333;
      font-family:roboto, helvetica, sans-serif;
      font-size:22px;
      line-height:1.5;
      margin:0;
    }

    h1,
    h2,
    h3,
    p,
    ul,
    ol {
      margin:0 0 1.25em;
    }

    h1 {
      font-size:54px;
      line-height:1.25;
      margin-bottom:.25em;
    }

    a {
      color:#b1132f;
      text-decoration:none;
    }

    img {
      display:inline-block;
      height:auto;
      vertical-align:middle;
      max-width:100%;
    }

    .input {
      background:#fff;
      border:1px solid #d9d9d9;
      border-radius:5px;
      color:inherit;
      font-family:inherit;
      font-size:18px;
      line-height:inherit;
      outline:0;
      padding:.5em .75em;
      transition:all .5s;
      width:100%;
    }

    .input:focus {
      border-color:#b1132f;
      box-shadow:0 0 8px rgba(177, 19, 47, .7);
    }

    .input::-moz-placeholder {
      color:#a7a6a5;
      opacity:1;
    }
    .input:-ms-input-placeholder { color:#a7a6a5; }
    .input::-webkit-input-placeholder  { color:#a7a6a5; }

    .btn {
      background:#252424;
      color:#fff;
      cursor:pointer;
      display:inline-block;
      font-size:18px;
      line-height:52px;
      padding:0 25px;
      text-decoration:none;
    }

    .page:after,
    .footer {
      height:52px;
    }

    .page {
      min-height:100vh;
      margin-bottom:-52px;
    }

    .page:before {
      background:#333;
      content:'';
      display:none;
      left:79%;
      overflow:auto;
      padding-bottom:28%;
      position:absolute;
      transform:rotate(12deg);
      top:-46%;
      width:25%;
    }

    .page:after {
      content:'';
      display:block;
    }

    .header {
      padding:30px 0 30px;
    }

    .header img {
      width:195px;
    }

    .footer {
      background:#252424;
      border-top:2px solid #b1132f;
      padding:10px 0;
      position:absolute;
      width:100%;
    }

    .footer img {
      display:block;
      float:right;
      height:30px;
    }

    .container {
      margin:0 auto;
      max-width:80%;
    }

    .step {
      display:none;
      opacity:0;
      visibility:hidden;
      transition:all .5s;
      width:100%;
    }

    .step-start .container {
      padding-left:40%;
    }

    .step-middle {
      height:92vh;
      display:table-cell;
      vertical-align:middle;
    }

    .step-start h1 {
      font-weight:900;
      text-transform:uppercase;
    }

    .step-start p {
      color:#5f5b58;
    }

    .step-thank-you {
      background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/thanks-bg.png'); ?>) no-repeat 50% 0;
      background-size:cover;
    }

    .step-thank-you .step-middle {
      padding-top:6em;
    }

    .step-thank-you p {
      font-size:45px;
      text-transform:uppercase;
    }

    .step-thank-you strong {
      display:inline-block;
      margin-bottom:.5em;
    }

    .steps-pagination,
    .step-next {
      margin:0;
    }

    .steps-pagination {
      font-size:15px;
      font-weight:700;
      list-style:none;
      margin:0 130px;
      padding:12px 0;
      text-align:center;
    }

    .steps-pagination li {
      display:inline-block;
    }

    .steps-pagination a {
      background:#f2f3f3;
      border-radius:50%;
      color:#f2f3f3;
      display:block;
      line-height:28px;
      width:28px;
    }

    .steps-pagination .active a {
      background:#929292;
      color:#fff;
    }

    .step-next {
      float:right;
    }

    .questions {
      display:none;
      padding-bottom:3em;
    }

    .question {
      margin-bottom:5em;
    }

    .question:last-child {
      margin-bottom:0;
    }

    .question-text {
      padding-left:3.5em;
      position:relative;
    }

    .question-text:before {
      background:#b1132f;
      border-radius:50%;
      color:#fff;
      content:attr(data-number);
      float:left;
      font-size:20px;
      margin-top:-1.5em;
      left:0;
      line-height:3em;
      position:absolute;
      text-align:center;
      top:50%;
      width:3em;
    }

    .question-answers {
      margin:0 -12px;
      list-style:none;
      padding:0;
    }

    .question-answers li {
      color:#fff;
      float:left;
      padding:0 12px;
      width:9.0909%;
    }

    .question-answers:after {
      content:'';
      clear:both;
      display:block;
    }

    .answer-item {
      cursor:pointer;
      display:inline-block;
      position:relative;
      width:100%;
    }

    .answer-input {
      -webkit-user-select:none;
      cursor:pointer;
      height:15px;
      margin:0;
      left:50%;
      opacity:0;
      padding:0;
      position:absolute;
      top:50%;
      width:15px;
    }

    .answer-text {
      background:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/star-inactive.png'); ?>) no-repeat;
      background-size:cover;
      font-weight:700;
      height:0;
      display:block;
      padding-bottom:100%;
      position:relative;
      transition:all .5s;
      width:100%;
    }

    .answer-text span {
      left:50%;
      position:absolute;
      transform:translate(-50%, -50%);
      top:56%;
    }

    .answer-help {
      color:#222;
      font-size:12px;
      font-weight:700;
      line-height:1.25;
      left:50%;
      position:absolute;
      text-align:center;
      text-transform:uppercase;
      transform:translate(-50%, 0);
      top:120%;
    }

    :checked + .answer-text {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/star-active.png'); ?>);
    }

    .step-content {
      background:#f2f3f3;
      margin-bottom:3em;
      padding:8% 14%;
    }

    .active {
      display:block;
    }

    .fade-in {
      opacity:1;
      visibility:visible;
    }

    .step-image-top,
    .step-image-bottom {
      position:relative;
    }

    .step-image {
      background-size:cover;
      bottom:-3.75em;
      position:absolute;
      z-index:1;
    }

    .step-image-top .step-image {
      right:0;
    }

    .step-image-bottom .step-image {
      left:0;
    }

    .step-image-bottom-0 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step01-left.png'); ?>);
      height:753px;
      width:250px;
    }

    .step-image-top-1 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step02-right.png'); ?>);
      height:263px;
      width:237px;
    }

    .step-image-bottom-1 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step02-left.png'); ?>);
      height:230px;
      width:104px;
    }

    .step-image-top-2 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step03-right.png'); ?>);
      height:227px;
      width:155px;
    }

    .step-image-bottom-2 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step03-left.png'); ?>);
      height:296px;
      width:234px;
    }

    .step-image-top-3 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step04-right.png'); ?>);
      height:169px;
      width:252px;
    }

    .step-image-bottom-3 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step04-left.png'); ?>);
      height:219px;
      width:207px;
    }

    .step-image-bottom-4 {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step05-left.png'); ?>);
      height:471px;
      width:232px;
    }

    .step-start .step-image-bottom,
    .step-thank-you .step-image-bottom {
      position:static;
    }

    .step-start .step-image,
    .step-thank-you .step-image {
      bottom:auto;
      transform:translate(0, -50%);
      top:50%;
      z-index:0;
    }

    .step-start .step-image {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step-start.png'); ?>);
      height:0;
      padding-bottom:62.188%;
      width:40.625%;
    }

    .step-thank-you .step-image {
      background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/step-thanks.png'); ?>);
      height:0;
      left:auto;
      right:0;
      padding-bottom:54.688%;
      width:33.984375%;
    }
    </style>
  </head>
  <body>
    <div class="page">
      <div class="step step-start active fade-in">
        <div class="container">
          <header class="step-middle">
            <h1>Cuentanos<br> tu experiencia<br> en Cinemex</h1>
            <p>Ayúdanos a superar tus espectativas<br> con base en tu utima visita.</p>

            <p><a class="btn" href="#step0" data-action="go-to">Comenzar</a></p>
          </header>

          <div class="step-image-bottom">
            <div class="step-image"></div>
          </div>
        </div>
      </div>

      <div class="questions">
        <div class="container">
          <div class="header">
            <img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/logo-cinemex-gray.png'); ?>">
          </div>
        </div>

        <?php
        $question_number = 0;
        $groups_count = (count($questions_groups) - 1);

        foreach ($questions_groups as $group_id => $questions) :
        ?>
          <div id="step<?php echo $group_id; ?>" class="step">
            <div class="step-image-top">
              <div class="step-image step-image-top-<?php echo $group_id; ?>"></div>
            </div>

            <div class="container">
              <div class="step-content">
                <?php if ($group_id === 0) : ?>
                <h2>Queremos conocer cuál fue tu experiencia en Cinemex Altavista el xx/xx/xxxx</h2>
                <?php endif; ?>

                <?php foreach ($questions as $question_id => $question) : ?>
                <div class="question">
                  <p class="question-text" data-number="<?php echo ++$question_number; ?>"><?php echo $question['text']; ?></p>

                  <?php if ($question['type'] === 'range') : ?>
                  <ul class="question-answers">
                    <?php for ($i = 0; $i <= 10; ++$i) : ?>
                    <li>
                      <label class="answer-item">
                        <input class="answer-input" type="radio" name="q<?php echo $question_id; ?>" value="<?php echo $i; ?>">
                        <span class="answer-text"><span><?php echo $i; ?></span></span>

                        <?php if ($i === 0) : ?>
                        <span class="answer-help">Nada satisfecho</span>
                        <?php endif; ?>

                        <?php if ($i === 10) : ?>
                        <span class="answer-help">Altamente satisfecho</span>
                        <?php endif; ?>
                      </label>
                    </li>
                    <?php endfor; ?>
                  </ul>
                  <?php endif; ?>

                  <?php if ($question['type'] === 'textarea') : ?>
                  <textarea class="input" name="1<?php echo $question_id; ?>" cols="30" rows="6" placeholder="Escribe tu respuesta aquí"></textarea>
                  <?php endif; ?>
                </div><!-- question -->
                <?php endforeach; ?>
              </div><!-- step-content -->

              <?php if ($group_id === $groups_count) : ?>
              <p class="step-next"><a class="btn" href="#thank-you" data-action="go-to">Continuar</a>
              <?php else : ?>
              <p class="step-next"><a class="btn" href="#step<?php echo ($group_id + 1); ?>" data-action="go-to">Continuar</a>
              <?php endif; ?>

              <ol class="steps-pagination">
              <?php foreach ($questions_groups as $group_idx => $questions) : ?>
                <li <?php if ($group_idx === $group_id) : ?>class="active"<?php endif; ?>>
                  <a href="#step<?php echo $group_idx; ?>" data-action="go-to"><?php echo ($group_idx + 1); ?></a>
                </li>
              <?php endforeach; ?>
              </ol>
            </div>

            <div class="step-image-bottom">
              <div class="step-image step-image-bottom-<?php echo $group_id; ?>"></div>
            </div>
          </div><!-- step -->
        <?php endforeach; ?>
      </div><!-- questions -->

      <div id="thank-you" class="step step-thank-you">
        <div class="container">
          <div class="step-middle">
            <p><strong>Gracias,</strong><br>
              tu optinion es lo<br>
              más importante</p>

            <p><a href="#" class="btn">Salir</a></p>
          </div>

          <div class="step-image-bottom">
            <div class="step-image"></div>
          </div>
        </div>
      </div>
    </div><!-- page -->

    <div class="footer">
      <div class="container">
        <img src="<?php echo $view['assets']->getUrl('assets/img/landings/encuesta-ie/logo-cinemex-white.png'); ?>" alt="Cinemex">
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
    (function($){
      $('html')
        .removeClass('no-js')
        .addClass('js');

      $('body').css('overflow', 'hidden');

      $('[data-action=go-to]').on('click', function(ev){
        ev.preventDefault();

        $('body').css('overflow', 'visible');

        var $trigger = $(this),
            $target = $($trigger.attr('href')),
            $questions = $target.closest('.questions');

        if (!$target.size()) {
          return;
        }

        if ($target.attr('id') === 'thank-you') {
          $('.questions').removeClass('active');
        }

        if ($questions.size()) {
          $questions.addClass('active');
        } else {
          $questions.removeClass('active');
        }

        $trigger.closest('.step').removeClass('active fade-in');

        $target.addClass('active');
        $target[0].offsetWidth;
        $target.addClass('fade-in');
      });
    })(jQuery)
    </script>
  </body>
</html>
