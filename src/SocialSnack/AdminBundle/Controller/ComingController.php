<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use SocialSnack\WsBundle\Entity\MovieComing;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class ComingController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }
  

  public function indexAction() {
    $this->check_roles();
    
    $coming_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:MovieComing');
    $movies = $coming_repo->findAllForAdmin();

    return $this->render('SocialSnackAdminBundle:Coming:list.html.php', array('movies' => $movies));
  }

  public function editAction($movie_id = NULL, Request $request) {
    $this->check_roles();
    
    $session = $request->getSession();
    $purge = NULL;
    $em = $this->get('doctrine')->getManager();
    $coming_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:MovieComing');
    $movie_attributes = array_map(function($a) { return $a['display_name']; }, FrontHelper::get_attributes_collection());
    $attributes_data = array();
    
    if (!is_null($movie_id)) {
      $movie = $coming_repo->find($movie_id);
      $attributes = $movie->getAttr();
      if ($attributes != NULL && !empty($attributes)) {
        foreach ($attributes as $key => $value) {
          if ($value == 1) {
            $attributes_data[] = $key;
          }
        }
      }
    } else {
      $movie = new MovieComing();
    }


    $form = $this->createFormBuilder()
            ->add('name', 'text', array(
                'data' => (is_string($movie->getName())) ? $movie->getName() : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('NOMBREORIGINAL', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('NOMBREORIGINAL'))) ? $movie->getData('NOMBREORIGINAL') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('active', 'checkbox', array(
                'required' => false,
                'data' => ($movie->getActive() == 1) ? true : false
            ))
            ->add('next_week', 'checkbox', array(
                'required' => false,
                'data' => ($movie->getNextWeek() == 1) ? true : false
            ))
            ->add('release_date', 'text', array(
                'data' => $movie->getReleaseDate() ? $movie->getReleaseDate()->format('Y-m-d') : NULL,
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('attribute', 'choice', array(
                'choices' => $movie_attributes,
                'multiple' => true,
                'data' => $attributes_data,
                'expanded' => true
            ))
            ->add('SINOPSIS', 'textarea', array(
                'required' => false,
                'data' => (is_string($movie->getData('SINOPSIS'))) ? $movie->getData('SINOPSIS') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('RATING', 'text', array(
                'data' => (is_string($movie->getData('RATING'))) ? $movie->getData('RATING') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('DIRECTOR', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('DIRECTOR'))) ? $movie->getData('DIRECTOR') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('ACTORES', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('ACTORES'))) ? $movie->getData('ACTORES') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('DURACION', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('DURACION'))) ? $movie->getData('DURACION') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('GENERO', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('GENERO'))) ? $movie->getData('GENERO') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('TRAILER', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('TRAILER'))) ? $movie->getData('TRAILER') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('PAISORIGEN', 'text', array(
                'required' => false,
                'data' => (is_string($movie->getData('PAISORIGEN'))) ? $movie->getData('PAISORIGEN') : '',
                'attr' => array(
                    'class' => 'form-control')
            ))
            ->add('poster', 'file')
            ->add('save', 'submit')
            ->getForm();


    $form->handleRequest($request);

    if ($form->isValid()) {

      if (!$_FILES["movie_poster"]["error"] &&
              (($_FILES["movie_poster"]["type"] == "image/gif") || ($_FILES["movie_poster"]["type"] == "image/jpeg") || ($_FILES["movie_poster"]["type"] == "image/jpg") || ($_FILES["movie_poster"]["type"] == "image/pjpeg") || ($_FILES["movie_poster"]["type"] == "image/x-png") || ($_FILES["movie_poster"]["type"] == "image/png"))) {


        $bytes = file_get_contents($_FILES["movie_poster"]["tmp_name"]);
        $poster = $this->container->get('ss.front.utils')->name_movie_poster(15);

        $this->container->get('ss.front.utils')->upload_movie_poster($poster, $bytes);
        $movie->setPosterUrl($poster);
      }

      $data = $form->getData();
      $movie->setName((string) $data['name']);

      if ($data['active']) {
        $active = 1;
      } else {
        $active = 0;
      }
      $movie->setActive($active);
      $movie->setReleaseDate(date_create_from_format("Y-m-d", $data['release_date']));

      if ($data['next_week']) {
        $next_week = 1;
      } else {
        $next_week = 0;
      }
      $movie->setNextWeek($next_week);

      $movie_info = json_decode($movie->getInfo());
      $movie_info->SINOPSIS       = $data['SINOPSIS'];
      $movie_info->DIRECTOR       = $data['DIRECTOR'];
      $movie_info->ACTORES        = $data['ACTORES'];
      $movie_info->GENERO         = $data['GENERO'];
      $movie_info->DURACION       = $data['DURACION'];
      $movie_info->GENERO         = $data['GENERO'];
      $movie_info->ESTRENO        = $data['ESTRENO'];
      $movie_info->TRAILER        = $data['TRAILER'];
      $movie_info->PAISORIGEN     = $data['PAISORIGEN'];
      $movie_info->NOMBREORIGINAL = $data['NOMBREORIGINAL'];
      $movie_info->RATING         = $data['RATING'];
      $movie->setInfo(json_encode($movie_info));
      
      // Set the attributes for the movie.
      if (isset($data['attribute']) && is_array($data['attribute'])) {
        $save_attribute = array();
        foreach ($data['attribute'] as $attribute) {
          $save_attribute[$attribute] = TRUE;
        }
        $movie->setAttributes(json_encode($save_attribute));
      }
      
      $em->persist($movie);
      $em->flush();
      
      $cache_purger = $this->get('ss.cache_purger');
      $purge = $cache_purger->purge_cache(array('entity' => $movie));
      
      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);
      return $this->redirect($this->generateUrl('social_snack_coming_movie', array('movie_id' => $movie->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Coming:edit.html.php', array(
        'movie' => $movie,
        'form'  => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }

  public function inactiveAction() {
    $this->check_roles();

    $coming_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:MovieComing');
    $movies = $coming_repo->findAllForAdmin(0);

    return $this->render('SocialSnackAdminBundle:Coming:inactive_list.html.php', array('movies' => $movies));
  }

}
