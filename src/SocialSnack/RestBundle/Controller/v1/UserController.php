<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @todo Secure resources
 */
class UserController extends BaseController {

	
	/**
	 * @Route("/")
   * @Method({"GET"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function defaultAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:default', ['request' => $request]);
	}
  
  
  /**
   * @Route("/")
   * @Method({"PUT"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function updateAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:update', ['request' => $request]);
  }


	/**
	 * @Route("/iecode")
	 * @Method({"GET"})
	 */
	public function iecodeAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:iecode', ['request' => $request]);
	}
	
	
	/**
	 * @Route("/iecode/validate")
	 * @Method({"POST"})
	 */
	public function iecodeValidateAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:iecodeValidate', ['request' => $request]);
	}
	
	
	/**
	 * Link Invitado Especial account with user account.
	 * 
	 * Requires $_REQUEST['iecode'] string Invitado Especial code.
	 * 
	 * @Route("/iecode")
	 * @Method({"POST"})
	 */
	public function iecodePostAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:iecodePost', ['request' => $request]);
	}
	
	
	/**
	 * Unlink Invitado Especial account from user account.
	 * 
	 * @Route("/iecode")
	 * @Method({"DELETE"})
	 */
	public function iecodeDeleteAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:iecodeDelete', ['request' => $request]);
	}
	
	
	/**
	 * @Route("/fbuid")
	 * @Method({"GET"})
	 */
	public function fbuidAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:fbuid', ['request' => $request]);
	}
	
	
	/**
	 * Link FB account with user account.
	 * 
	 * Requires $_REQUEST['fbsigned_request'] or $_REQUEST['fbaccess_token'] string Facebook User ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/fbuid")
	 * @Method({"POST"})
	 */
	public function fbuidPostAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:fbuidPost', ['request' => $request]);
	}


	/**
	 * Unlink FB account from user account.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 *
	 * @Route("/fbuid")
	 * @Method({"DELETE"})
	 */
	public function fbuidDeleteAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:fbuidDelete', ['request' => $request]);
	}
	
	
	/**
	 * @Route("/cinemas")
	 * @Method({"GET"})
	 */
	public function cinemasAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:cinemas', ['request' => $request]);
	}


	/**
	 * @Route("/cinemas/movies")
	 * @Method({"GET"})
	 */
	public function cinemasMoviesAction(Request $request) {
		/** @var \SocialSnack\RestBundle\Handler\ContextHandler $context_handler */
		$context_handler = $this->get('rest.context.handler');

		if ($request->query->has('include_other_versions')) {
			$other = $request->query->get('include_other_versions');
			list($area_id, $state_id) = $context_handler->normalizeAreaIds($other);
			if (!is_null($area_id)) {
				$other = 'area-' . $area_id;
			} elseif (!is_null($state_id)) {
				$other = 'state-' . $state_id;
			}
			$request->query->set('include_other_versions', $other);
		}

		return $this->forward('SocialSnackRestBundle:User:cinemasMovies', ['request' => $request]);
	}
	
	
	/**
	 * Add cinema to favorites.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/cinemas")
	 * @Method({"POST"})
	 */
	public function cinemasPostAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:cinemasPost', ['request' => $request]);
	}
	
	
	/**
	 * Remove cinema from favorites.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/cinemas")
	 * @Method({"DELETE"})
	 */
	public function cinemasDeleteAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:cinemasDelete', ['request' => $request]);
	}
	
	
	/**
	 * Set user's preferred cinema.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/preferredCinema")
	 * @Method({"POST"})
	 */
	public function preferredCinema(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:preferred', ['request' => $request]);
	}
  
 
  /**
   * @Route("/loginData")
   */
  public function loginDataAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:loginData', ['request' => $request]);
  }
  
  
  /**
   * @Route("/purchaseHistory")
   */
  public function purchaseHistoryAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:purchaseHistory', ['request' => $request]);
  }
  
  
  /**
   * @Route("/ie/details")
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function ieDetailsAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:ieDetails', ['request' => $request]);
  }


  /**
   * @Route("/registerGCM")
   * @Method({"POST"})
   */
  public function registerGCMAction(Request $request) {
		return $this->forward('SocialSnackRestBundle:User:registerGCM', ['request' => $request]);
  }
  	
}