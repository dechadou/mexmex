<div class="std-form">
  <div class="form-row ie-box">
    <label for="register_iecode">Ingresa tu número de Invitado Especial</label>
    <div class="ie-input-box">
      <div class="ie-input-wrap">
        <input type="text" name="iecode" id="register_iecode" value="" data-validators="not_empty" data-prefix="308681 " />
        <a class="ie-input-help lightbox-img" href="<?php echo $view['assets']->getUrl('assets/img/ie-example.png'); ?>">?</a>
      </div>
      <button type="submit" class="btn btn-icon icon icon-tick btn-icon-right btn-right">Aplicar</button>
    </div>
  </div>
</div>
<div class="ie-no-box">
  <a href="<?php echo $view['router']->generate('ie_program'); ?>">¿Qué es Invitado Especial Cinemex?</a>
  <a href="<?php echo $view['router']->generate('ie_benefits'); ?>">Conoce los beneficios</a>
</div>
