<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;

interface EntityTaskInterface extends CachePurgeTaskInterface {

  public function setEntity($entity);

}