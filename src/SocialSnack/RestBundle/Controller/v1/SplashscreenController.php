<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use SocialSnack\FrontBundle\Entity\Splashscreen;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

class SplashscreenController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_v1_splashscreen_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Splashscreen:default', ['request' => $request]);
	}
		
	
	/**
	 * Return detailed info for splashscreen(s).
	 * 
	 * @Route("/{id}", name="rest_v1_splashscreen_single", requirements={"id"="[0-9]+"})
	 * @Method({"GET"})
	 */
	public function detailsAction($id, Request $request) {
    return $this->forward('SocialSnackRestBundle:Splashscreen:details', ['id' => $id, 'request' => $request]);
	}

}