<?php

namespace SocialSnack\WsBundle\Service;


use SocialSnack\WsBundle\Command\MoviesGroupCommand;
use SocialSnack\WsBundle\Command\MoviesOrderCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GroupAndOrder extends WorkerAbstract {

  protected $container;

  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }


  /**
   * @todo Accept a parameter to limit the action to a single cinema/area/state.
   *
   * @param $data
   * @return bool
   */
  public function execute($data) {
    $input   = new ArrayInput(array());
    $output  = new NullOutput();

    $this->consoleInfo('Group movies...');
    $command = new MoviesGroupCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);

    $this->consoleInfo('Order movies...');
    $command = new MoviesOrderCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);

    $this->consoleInfo('Done!');

    return TRUE;
  }

} 