<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['cinemom']);
$view['slots']->set('title', 'CineMá - Cinemex');
$view['slots']->start('body');


echo $view->render(
    'SocialSnackFrontBundle:Sections:cinemomMenu.html.php',
    array('route' => $route)
);
?>

<div class="cinemom-cinemas-grid items-grid row clearfix">
<?php
$formatter = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC');
$formatter->setPattern('EEEE');

foreach ($sessions as $session) :
    $cinema = $session->getCinema();
    $movie  = $session->getMovie();

    if ( $movie->getParent() && $movie->getParent()->getId() ) {
        $poster_url = $movie->getParent()->getPosterUrl();
    } else {
        $poster_url = $movie->getPosterUrl();
    }

    $poster_url = $view['fronthelper']->get_poster_url($poster_url, '187x276');
?>
    <div class="col col-xs-1-3 col-sm-1-4 col-md-1-5 cinemas-grid-item">
        <a href="<?php echo $view['router']->generate('checkout', array('session_id' => $session->getId())); ?>">
            <div class="movie-poster-bt buy-tickets-bt">
                <?php echo $formatter->format($session->getDate()); ?><br>
                <?php echo $session->getTime()->format('h:i A'); ?>
            </div>

            <img src="<?php echo $poster_url; ?>" alt="<?php echo $movie->getName(); ?>" />
        </a>

        <h2 class="cinemas-grid-title"><a href="<?php echo $view['router']->generate('checkout', array('session_id' => $session->getId())); ?>"><?php echo $cinema->getName(); ?></a></h2>
    </div>
<?php endforeach; ?>
</div>

<?php $view['slots']->stop(); ?>
