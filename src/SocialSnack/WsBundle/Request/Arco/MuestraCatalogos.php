<?php

namespace SocialSnack\WsBundle\Request\Arco;

use SocialSnack\WsBundle\Request\ConsultaArcoAbstract;

class MuestraCatalogos extends ConsultaArcoAbstract {

  const     CATALOGO_ESTADOS        = 1;
  const     CATALOGO_COMPLEJOS      = 3;
  const     CATALOGO_IDENTIFICACION = 4;
  const     CATALOGO_DERECHO_ACC    = 5;
  const     CATALOGO_TIPO_RELACION  = 6;
  const     CATALOGO_ETIQUETAS      = 7;
	
	protected $method       = 'MuestraCatalogo';
	
	protected $default_args = array(
      'catalogo' => NULL,
  );

	
	/**
	 * Parse the request and return only the relevant nodes.
	 * 
	 * @param \stdClass $res;
   * @return \SimpleXMLElement
	 */
	public function parse_res($res) {
    if ( !$res->MuestraCatalogoResult ) {
      throw new \Exception( 'Invalid response 0: ' . json_encode( $res ) );
    }
    
    $xml = simplexml_load_string( $res->MuestraCatalogoResult, null, LIBXML_NOCDATA );
    
    if ( !isset($xml->RESPUESTA) || !isset($xml->RESPUESTA->CATALOGO) ) {
      throw new \Exception( 'Invalid response 1: ' . json_encode( $xml ) );
    }
    
		return $xml->RESPUESTA->CATALOGO;
	}
  
}