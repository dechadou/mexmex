<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Form\Type\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\Article;

class ArticleController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }
  

  public function indexAction() {
    $this->check_roles();
    
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Article');
    $articles = $repo->findAdmin();

    return $this->render('SocialSnackAdminBundle:Article:list.html.php', array('articles' => $articles));
  }

  public function editAction($article_id = NULL, Request $request) {
    $this->check_roles();

    $session = $request->getSession();
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Article');

    if (is_null($article_id)) {
      $article = new Article();
    } else {
      $article = $repo->find($article_id);
    }

    $form = $this->createForm(new ArticleType(), $article);
    $form->add('save', 'submit', ['label' => 'Guardar']);
    $form->handleRequest($request);

    if ($form->isValid()) {
      $cache_purger = $this->get('ss.cache_purger');
      $base_path = 'news/';
      $purge_files = NULL;

      // The ID is used to name the image files so we need to persist the entity first.
      // @todo Use another way to name the file and avoid the extra DB write.
      if (!$article->getId()) {
        $em->persist($article);
        $em->flush();
      }

      if ($file = $form["cover"]->getData()) {
        $filename = $article->getId() . "-Large.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 733, 286);
        $purge_files[] = '/' . $this->container->getParameter('cms_upload_path') . $base_path . $filename;
        $article->setCover($base_path . $filename);
      }

      if ($file = $form["thumb"]->getData()) {
        $filename = $article->getId() . "-Thumbnail.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 236, 222);
        $purge_files[] = '/' . $this->container->getParameter('cms_upload_path') . $base_path . $filename;
        $article->setThumb($base_path . $filename);
      }

      $em->flush();

      if ($purge_files != NULL){
        $cache_purger->enqueue(array(
            'type' => 'assets',
            'files' => $purge_files
        ));
      }
      $cache_purger->enqueue(array('entity' => $article));
      $purge = $cache_purger->purge_cache();

      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);
      return $this->redirect($this->generateUrl('social_snack_article', array('article_id' => $article->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Article:edit.html.php', array(
        'article' => $article,
        'form' => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }

}
