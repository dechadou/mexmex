<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovieVote
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MovieVote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;
		
		/**
		 * @ORM\ManyToOne(targetEntity="User", inversedBy="votes")
		 */
		private $user;
		
		/**
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Movie", inversedBy="votes")
		 */
		private $movie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \SocialSnack\FrontBundle\Entity\User $user
     * @return MovieVote
     */
    public function setUser(\SocialSnack\FrontBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialSnack\FrontBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set movie
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $movie
     * @return MovieVote
     */
    public function setMovie(\SocialSnack\WsBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;
    
        return $this;
    }

    /**
     * Get movie
     *
     * @return \SocialSnack\WsBundle\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return MovieVote
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }
}