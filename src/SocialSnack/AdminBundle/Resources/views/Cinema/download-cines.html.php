<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Descargar listado de cines - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de cines
      </header>
      <div class="panel-body">
        <form action="<?php echo $view['router']->generate('social_snack_admin_cinema_download'); ?>" method="post">
          <h5>Descargar el CSV con el listado de cines.</h5>
          <input type="hidden" name="descargar" value="1">
          <div class="row">
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Descargar</button>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[4, "desc"]]
    });
  });
</script>
<?php $view['slots']->stop(); ?>