<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\User;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\WsBundle\Exception\InvalidIECodeException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class UserHandler {

  protected $container;

  protected $doctrine;

  protected $frontUtils;

  public function __construct(Registry $doctrine, ContainerInterface $container, FrontUtils $frontUtils) {
    $this->doctrine   = $doctrine;
    $this->container  = $container;
    $this->frontUtils = $frontUtils;
  }

  /**
   * @param int $user_id
   * @return User
   */
  public function get($user_id) {
    return $this->doctrine->getRepository('SocialSnackFrontBundle:User')->find($user_id);
  }


  /**
   * @param int $user_id
   * @param int $count
   * @param int $since_id
   * @param int $before_id
   * @return array
   */
  public function getTransactionHistory($user_id, $count = NULL, $since_id = NULL, $before_id = NULL) {
    $user = $this->get($user_id);

    // Set default number of results.
    if (!$count) {
      $count = 25;
    }

    $qb = $this->doctrine
        ->getRepository('SocialSnackFrontBundle:Transaction')
        ->createQueryBuilder('t');

    $qb
        ->select('t')
        ->where('t.user = :user')
        ->setParameter('user', $user)
        ->orderBy('t.purchase_date', 'DESC')
        ->setMaxResults($count)
    ;

    // Set pagination offset.
    if ($since_id) {
      $qb->andWhere('t.id > :since_id')
          ->setParameter('since_id', $since_id);
    } elseif ($before_id) {
      $qb->andWhere('t.id < :before_id')
          ->setParameter('before_id', $before_id);
    }

    $transactions = $qb->getQuery()->getResult();

    $apps       = $this->doctrine->getRepository('SocialSnackRestBundle:App')->findPublic();
    $apps_by_id = array_reduce($apps, function($carry, $item) {
      $carry[$item->getAppId()] = $item;
      return $carry;
    }, array());

    $output = array();
    foreach ( $transactions as $t ) {
      $_t = $t->toArray();
      $_t['qr'] = $this->container->get('templating.helper.assets')->getUrl($_t['qr'], 'QR');
      $_t['movie']['label'] = FrontHelper::get_movie_attr_str($t->getMovie()->getAttr());
      $_t['movie']['cover'] = $this->frontUtils->get_poster_url($_t['movie']['cover'], '200x295');
      $_t['auditorium'] = FrontHelper::confirmation_get_screenname($t);
      $_t['app'] = isset($apps_by_id[$t->getAppId()]) ? $apps_by_id[$t->getAppId()]->getName() : NULL;

      $output[] = $_t;
    }

    return $output;
  }


  /**
   * @param $iecode
   * @return bool
   * @throws \SocialSnack\RestBundle\Exception\RestException
   * @throws \Exception
   */
  public function validateIECode($iecode) {
    /** @var \SocialSnack\RestBundle\Handler\IEHandler $ie_handler */
    $ie_handler = $this->container->get('rest.ie.handler');

    try {
      $ie_handler->verifyIECode($iecode);
      return TRUE;

    } catch (\Exception $e) {
      if ($e instanceof InvalidIECodeException) {
        throw new RestException('-10111', 'Invalid IE code.');
      } elseif ($e instanceof UnexpectedWsResponseException) {
        throw new RestException('', '', 500);
      }

      throw $e;
    }
  }


  /**
   * @param User $user
   * @return array
   */
  public function serializeOne(User $user) {
    $_user = $user->toArray();
    $_user['avatar_medium'] = $this->frontUtils->get_avatar_url($_user['avatar'], '360x360');
    $_user['avatar'] = $this->frontUtils->get_avatar_url($_user['avatar'], '64x64');
    return $_user;
  }


  /**
   * @param User $user
   * @return array
   */
  public function serializeUserFavCinemas(User $user) {
    $cinemas = array();
    foreach ($user->getCinemasFav() as $cinema) {
      if ($cinema->getActive()) {
        $cinemas[] = $cinema->toArray();
      }
    }

    return $cinemas;
  }

} 