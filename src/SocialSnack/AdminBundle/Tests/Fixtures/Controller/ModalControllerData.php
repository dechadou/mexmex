<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures\Controller;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Modal;

class ModalControllerData implements FixtureInterface {

  static $cinemas;

  static $modals;

  public function load(ObjectManager $manager) {
    self::$modals = [];

    $faker = \Faker\Factory::create();

    $modal = new Modal();
    $modal
        ->setTitle($faker->title())
        ->setType(Modal::TYPE_IMAGE)
        ->setUrl($faker->url())
        ->setStatus(Modal::STATUS_PUBLISH)
    ;
    $manager->persist($modal);
    self::$modals[] = $modal;

    $manager->flush();
  }

}