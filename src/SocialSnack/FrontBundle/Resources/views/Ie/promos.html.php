<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Promociones - Cinemex');
$view['slots']->start('iebody');
?>
<h1 class="content-box-title">Promociones</h1>

<div class="entry-body">
  <p>Forma parte del mejor programa de recompensas que Premia tu Diversión, adquiere tu tarjeta y empieza a disfrutar de las exclusivas promociones.</p>

  <?php echo $this->render('SocialSnackFrontBundle:Ie:levelsTabs.html.php'); ?>

  <?php foreach ($promos as $level => $items) : ?>
    <div id="ie-levels-<?php echo $level; ?>" class="ie-levels-block ie-promos-grid">
      <?php if (empty($items)) : ?>
        <p class="msg">No hay promociones disponibles actualmente.</p>
      <?php else : ?>
        <?php foreach ($items as $promo) : ?>
          <a href="<?php echo $view['assets']->getUrl($promo->getImage(), 'CMS'); ?>">
            <img src="<?php echo $view['fronthelper']->get_cms_url($promo->getThumb(), '300x393'); ?>">
          </a>
        <?php endforeach; ?>
      <?php endif; ?>

      <div class="clear"></div>
    </div>
  <?php endforeach; ?>
</div>
<?php $view['slots']->stop(); ?>
