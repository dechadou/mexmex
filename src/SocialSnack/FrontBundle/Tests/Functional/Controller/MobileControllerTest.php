<?php

namespace SocialSnack\FrontBundle\Tests\Functional\Controller;

use SocialSnack\FrontBundle\Tests\Controller\WebTestCase;

class MobileControllerTest extends WebTestCase{

  public function testResetPassAction_NoCode() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/restablecer-password/',
        [],
        [],
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertSame($client->getResponse()->getStatusCode(), 400);
  }


  public function testResetPassAction_CodeOk() {
    $authHandlerMock = $this->getServiceMockBuilder('rest.auth.handler')
        ->setMethods(['getResetPassCode'])
        ->getMock();
    $code = $this->getMock('SocialSnack\FrontBundle\Entity\ResetCode');

    $authHandlerMock->expects($this->once())
        ->method('getResetPassCode')
        ->with('foobar')
        ->willReturn($code);

    $client = static::createClient();
    $client->getContainer()->set('rest.auth.handler', $authHandlerMock);
    $client->request(
        'GET',
        '/restablecer-password/?code=foobar',
        [],
        [],
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertSame($client->getResponse()->getStatusCode(), 200);
  }

}