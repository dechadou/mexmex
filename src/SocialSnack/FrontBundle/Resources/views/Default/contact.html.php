<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Buzón Cinemex - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('contact'); ?>">Contáctanos</a></li>
</ul>

<div class="clearfix row">
  <div class="main-col col">
    <div class="content-box">
      <h1 class="content-box-title">Contáctanos</h1>
      <p class="checkout-legend">
        Tu opinión es muy importante para nosotros, envíanos tus opiniones y comentarios acerca de nuestro servicio para poder mejorar cada día.
      </p>

      <form action="" method="post" id="contact-form" data-basicvalidate="1" data-validcb="contact_submit">
        <div class="std-form clearfix">
          <div class="form-row half">
            <label for="name">Nombre *</label>
            <input type="text" name="name" id="contact_name" value="" data-validators="not_empty" data-flyvalidate="1" />
          </div>
          <div class="form-row half">
            <label for="email">Email *</label>
            <input type="email" name="email" id="contact_email" value="" data-validators="not_empty email" data-flyvalidate="1" />
          </div>
          <div class="form-row half">
            <label for="ie">N° de Invitado Especial</label>
            <input type="text" name="ie" id="contact_ie" value="" />
          </div>
          <div class="form-row half">
            <label for="phone">Teléfono</label>
            <input type="text" name="phone" id="contact_phone" value="" />
          </div>
          <div class="form-row half">
            <label for="cinema">Cine</label>
            <select name="cinema" id="contact_cinema">
              <option value="">Selecciona un cine</option>
              <?php foreach ( $cinemas as $cinema ) { ?>
              <option value="<?php echo $cinema->getId(); ?>"><?php echo $cinema->getName(); ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-row half">
            <label for="service">Servicio</label>
            <select name="service" id="contact_service">
              <option value="">Selecciona una opción</option>
              <option value="Dulcería">Dulcería</option>
              <option value="General">General</option>
              <option value="Invitado Especial">Invitado Especial</option>
              <option value="Internet">Internet</option>
              <option value="Proyección">Proyección</option>
              <option value="Taquilla">Taquilla</option>
            </select>
          </div>
          <div class="form-row half">
            <label for="type">Tipo de mensaje</label>
            <select name="type" id="contact_type">
              <option value="">Selecciona una opción</option>
              <option value="Felicitación">Felicitación</option>
              <option value="Información">Información</option>
              <option value="Queja">Queja</option>
              <option value="Sugerencia">Sugerencia</option>
            </select>
          </div>
          <div class="form-row half">
            <label for="msg">Mensaje *</label>
            <textarea name="msg" id="contact_msg" data-validators="not_empty" data-flyvalidate="1"></textarea>
          </div>
        </div>

        <div class="btn-row">
          <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Enviar</button>
        </div>
      </form>
    </div>
  </div>
  <div class="side-col col">
    <img src="<?php echo $view['assets']->getUrl( 'assets/img/contact-img.jpg' ); ?>" alt="" class="d-block" />

    <div class="disclaimer">
      <p>Cinemex Desarrollos, S.A. de C.V. (en adelante "Cinemex"), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente: </p>
      <p>Los Datos Personales que le son solicitados, serán tratados con las finalidades primarias de atender y darle seguimiento a sus dudas, quejas, comentarios o sugerencias.</p>
      <p><a href="<?php echo $view['router']->generate('contact_privacy_page'); ?>">Aviso de Privacidad</a></p>
      <p>Fecha de última actualización: 5/Enero/2015</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
