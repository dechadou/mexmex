<?php
/** @var \SocialSnack\FrontBundle\Entity\Landing $landing */

$title = $landing->getId()
    ? sprintf('Editar landing %s', $landing->getName())
    : 'Nueva landing';

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', $title . ' - Cinemex');
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <strong><?php echo $title; ?></strong>
          <a href="<?php echo $view['fronthelper']->get_permalink($landing); ?>">(ver en front)</a>
        </header>

        <div class="panel-body">
          <?php if (sizeof($submit_msg)) { ?>
            <div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
          <?php } ?>

          <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
          <?php echo $view['form']->form($form); ?>
        </div>
      </section>

      <?php if ($landing->getId()) { ?>
        <section class="panel">
          <header class="panel-heading">
            <strong>Secciones</strong>
          </header>

          <div class="panel-body">
            <div class="btn-group">
              <a class="btn btn-default" id="editable-sample_new" href="<?php echo $view['router']->generate('admin_landingPage_add', ['id' => $landing->getId()]); ?>">
                Nueva sección <i class="icon-plus"></i>
              </a>
            </div>
          </div>

          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>Nombre</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($landing->getPages() as $page) { ?>
                <tr>
                  <td><?php echo $page->getName(); ?></td>
                  <td><a href="<?php echo $view['router']->generate('admin_landingPage_edit', ['id' => $page->getId()]); ?>">Editar</a></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </section>
      <?php } ?>
    </div>
  </div>
<?php $view['slots']->stop(); ?>