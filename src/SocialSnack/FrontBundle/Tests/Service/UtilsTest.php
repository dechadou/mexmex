<?php

namespace SocialSnack\FrontBundle\Tests\Service;


use SocialSnack\FrontBundle\Entity\User;
use SocialSnack\FrontBundle\Service\Utils;
use SocialSnack\WsBundle\Entity\Cinema;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UtilsTest extends WebTestCase {

  protected $container;

  public function setUp() {
    $this->container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
        ->disableOriginalConstructor()
        ->getMock();
  }

  protected function getService() {
    return new Utils($this->container);
  }


  protected function _testCinemaHasQr($cinema_id) {
    // Has QR
    $cinema_w_qr = $this->getMock('\SocialSnack\WsBundle\Entity\Cinema');
    $cinema_w_qr->expects($this->once())
        ->method('getLegacyId')
        ->willReturn($cinema_id);

    $utils = $this->getService();
    $output = $utils->cinema_has_qr($cinema_w_qr);

    return $output;
  }


  public function testCinemaHasQr_Has() {
    // Has QR
    $this->assertTrue(
        $this->_testCinemaHasQr(1032)
    );
  }


  public function testCinemaHasQr_HasNot() {
    // Doesn't have QR
    $this->assertFalse(
        $this->_testCinemaHasQr(666)
    );
  }


  public function testUpload_avatar() {
    $service = $this->getMockBuilder('\SocialSnack\FrontBundle\Service\Utils')
        ->disableOriginalConstructor()
        ->setMethods(['get_statics_fss', 'resize_n_upload_to_cdn', 'delete_from_cdn'])
        ->getMock();

    $file = tempnam(sys_get_temp_dir(), 'avatar');
    imagejpeg(imagecreatetruecolor(10, 10), $file);
    $user = new User();
    $upload = new UploadedFile($file, 'avatar.jpg');
    $user->setFile($upload);

    $service->expects($this->once())
        ->method('resize_n_upload_to_cdn')
        ->with(
            $this->anything(),
            $this->anything(),
            $this->anything(),
            $this->callback(function($sizes) {
              $valid = TRUE;
              foreach ($sizes as $size) {
                $valid &= ($size === []) || isset($size['w'], $size['h'], $size['ext']);
              }

              return $valid;
            })
        );

    $service->upload_avatar($user);
  }


  protected function _testGetMobileOS($ua, $expected) {
    $service = $this->getService();
    $output = $service->getMobileOS($ua);
    $this->assertSame($expected, $output);
  }


  public function testGetMobileOS_iOS_iPhone() {
    $this->_testGetMobileOS(
        'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3',
        'ios'
    );
  }


  public function testGetMobileOS_iOS_iPad() {
    $this->_testGetMobileOS(
        'Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10',
        'ios'
    );
  }


  public function testGetMobileOS_Android() {
    $this->_testGetMobileOS(
        'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>',
        'android'
    );
  }


  public function testGetMobileOS_Windows() {
    $this->_testGetMobileOS(
        'Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 930) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537',
        'windows'
    );
  }

} 