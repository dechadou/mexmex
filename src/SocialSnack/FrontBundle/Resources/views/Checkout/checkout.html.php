<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
?>
<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set('body_class', ['checkout', 'movie-' . $session->getGroupId()]); ?>
<?php $view['slots']->set('title', 'Checkout - Cinemex'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>
<?php $view['slots']->stop(); ?>

<?php
$view['slots']->start('body');
$poster_url = $view['fronthelper']->get_movie_poster_url($session->getMovie(), '164x245');
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['fronthelper']->get_movie_link( $session->getMovie() ); ?>"><?php echo $session->getMovie()->getName(); ?></a></li>
  <li><a href="<?php echo $view['router']->generate( 'checkout', array( 'session_id' => $session->getId() ) ); ?>">Checkout</a></li>
</ul>

<div class="clearfix row">

	<div class="narrow-side-col col">
		<span href="#" class="btn btn-default btn-icon icon icon-movie side-head">Detalle de compra</span>

		<div class="side-box-content">
			<img src="<?php echo $poster_url; ?>" alt="<?php echo $session->getMovie()->getName(); ?>" class="side-box-cover" />

			<dl class="movie-details-info" id="order-info">
				<dt>Título</dt>
				    <dd><?php echo $session->getMovie()->getName(); ?></dd>
				<dt>Clasificación</dt>
				    <dd><?php echo $session->getMovie()->getData('RATING'); ?></dd>

        <?php $type = FrontHelper::get_movie_attr_str($session->getMovie()->getAttr(), ', '); ?>
        <?php if ( $type ) { ?>
				<dt>Versión</dt>
				    <dd><?php echo $type; ?></dd>
        <?php } ?>

				<dt>Día</dt>
				    <dd><?php echo FrontHelper::get_session_nice_date($session->getDate()); ?></dd>
				<dt>Horario</dt>
				    <dd><?php echo $session->getTime()->format( 'h:i A' ); ?></dd>
				<dt>Cine</dt>
				    <dd><?php echo $session->getCinema()->getName(); ?></dd>
        <dt>Sala</dt>
            <dd><?php echo WsHelper::get_session_screen_number($session); ?></dd>
			</dl>
		</div>

		<div class="btn btn-sec btn-icon icon icon-money" id="side-total">
			<span class="small">Total a pagar:</span>
			<span class="number"></span>
		</div>
	</div>


	<div class="wide-main-col col">

    <div id="checkout-process">
      <?php
      $steps = array(
          'Seleccionar boletos',
          'Seleccionar asientos',
          'Detalles de pago',
          'Compra finalizada',
      );
      $seats_allocation = ( 'Y' == $session->getData( 'SEATALLOCATIONON' ) );
      if ( !$seats_allocation ) {
        array_splice( $steps, 1, 1 );
      }
      ?>
      <ol id="checkout-trail">
        <?php foreach ( $steps as $i => $step ) { ?>
        <li class="checkout-trail-step <?php if ( $i == 0 ) echo 'selected'; ?>"><span><?php echo $step; ?></span></li>
        <?php } ?>
      </ol>

      <div class="checkout-steps">
        <div class="checkout-step content-box selected" id="checkout-step-1">
          <h2 class="content-box-title">1. Seleccionar boletos</h2>

          <p class="checkout-legend">Selecciona tipo y cantidad de boletos.</p>

          <form action="" method="post" id="checkout-step-1-form" class="checkout-form" data-step="1">
            <input type="hidden" name="session_id" value="<?php echo $session->getId(); ?>" />

            <div class="qty-selection">
              <?php $i = 0; foreach ( $session->getTickets() as $ticket ) { ?>
                <div class="qty-row">
                  <label>
                    <span class="ticket-label"><?php echo $ticket->getDescription(); ?></span>
                    <span class="ticket-sep">|</span>
                    <span class="ticket-price"><?php echo FrontHelper::format_price( $ticket->getPrice() ); ?> c/u</span>
                  </label>
                  <input type="hidden" name="tickets[<?php echo $i; ?>][type]" value="<?php echo $ticket->getData( 'TICKETTYPECODE' ); ?>" class="ticket-type" />
                  <input type="number" name="tickets[<?php echo $i; ?>][qty]" value="0" data-price="<?php echo $ticket->getPrice(); ?>" class="qty-q input-number-ctl" max="10" min="0" />
                </div>
              <?php $i++; } ?>
              <div class="qty-total-row">
                Total a pagar
                <span class="currency">$</span>
                <span class="number">0</span>
              </div>
            </div>

            <div id="checkout-ie">
              <span class="heading-title">
                <span class="discicon icon-star icon-red"></span>
                <span class="txt">¿Ya eres Invitado Especial Cinemex?</span>
              </span>
              <div class="ie-banner row">
                <div class="col col-md-3-5">
                  <a href="<?php echo $view['router']->generate( 'user_ie' ); ?>" class="ie-cto">
                    Ingresa tu número de Invitado Especial para sumar puntos
                    y disfrutar de muchos beneficios.*
                  </a>
                </div>
                <div class="ie-links col col-md-2-5">
                  <a href="<?php echo $view['router']->generate('ie_program'); ?>" class="ie-link ie-link-01">¿Qué es Invitado Especial?</a>
                  <a href="<?php echo $view['router']->generate('ie_benefits'); ?>" class="ie-link ie-link-02">Conoce los beneficios.</a>
                </div>
              </div>

              <div class="disclaimer">
                <p>* Los puntos y visita generados en esta compra se verán reflejados en 24 horas.</p>
              </div>
            </div>

            <div class="btn-row">
              <a href="javascript:history.back();" class="btn btn-transparent btn-icon btn-light icon icon-larr">Volver</a>
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-1-form -->
        </div>

        <div id="checkout-timeout">
          <span class="small">Tiempo Restante Para Realizar La Compra</span>
          <div id="timeout" class="countdown">00<span class="sep">:</span>00</div>
        </div>

        <?php if ( $seats_allocation ) { ?>
        <div class="checkout-step content-box" id="checkout-step-2">
          <h2 class="content-box-title">2. Seleccionar asientos</h2>

          <div class="seats-ref">
            <span class="seat seat-selected">Tu butaca</span>
            <span class="seat seat-0">Disponible</span>
            <span class="seat seat-1">No disponible</span>
          </div>

          <form action="" method="post" id="checkout-step-2-form" class="checkout-form" data-step="2">

            <input type="hidden" name="session_id"     value="<?php echo $session->getId(); ?>" />
            <input type="hidden" name="transaction_id" value="" />

            <div class="posrel">
              <div id="theater-layout">
                <table id="seats-layout">
                </table>
              </div>
            </div>

            <div class="btn-row">
              <a href="#" class="btn btn-transparent btn-icon btn-light icon icon-larr" rel="checkout-back">Volver</a>
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-2-form -->
        </div><!-- #checkout-step-2 -->
        <?php } ?>

        <div class="checkout-step content-box" id="checkout-step-3">
          <h2 class="content-box-title"><?php echo ( sizeof( $steps ) == 4 ) ? '3' : '2'; ?>. Detalles de pago</h2>

          <form action="" method="post" id="checkout-step-3-form" class="checkout-form" data-step="3" autocomplete="off">

            <input type="hidden" name="session_id"     value="<?php echo $session->getId(); ?>" />
            <input type="hidden" name="transaction_id" value="" />
            <input type="hidden" name="iecode"         value="" />
            <input type="hidden" name="app_id"         value="XXQha7vz4kdvoMSdixhN" />
            <input type="hidden" name="ref"            value="<?php echo $ref; ?>" />

            <div class="std-form clearfix">
              <div class="form-row half">
                <label for="checkout-name">Nombre y apellido</label>
                <input type="text" name="name" id="checkout-name" placeholder="Nombre del titular de la tarjeta" data-validators="not_empty full_name" data-flyvalidate="1" />
              </div>

              <div class="form-row half">
                <label for="checkout-email">Email</label>
                <input type="email" name="email" id="checkout-email" placeholder="Ingresa tu email" data-validators="not_empty email" data-flyvalidate="1" />
              </div>

              <div class="form-row half">
                <label for="checkout-cc">Número de tarjeta</label>
                <input type="text" name="cc" id="checkout-cc" placeholder="Ingresa el número de tu tarjeta de crédito" data-validators="not_empty ccnum"  data-flyvalidate="1" />
              </div>

              <div class="form-row quarter">
                <label for="checkout-csc">Código de seguridad</label>
                <input type="text" name="csc" id="checkout-csc" placeholder="" data-validators="not_empty csc"  data-flyvalidate="1" />
              </div>

              <div class="form-row quarter">
                <label for="checkout-exp-month">Fecha de vencimiento</label>
                <select name="expire-month" id="checkout-exp-month" class="month-year month" data-validators="not_empty cc_expiry_month" data-flyvalidate="1">
                  <option value="">Mes</option>
                  <?php for ( $i = 1 ; $i <= 12 ; $i++ ) { ?>
                  <option value="<?php echo $i; ?>"><?php echo str_pad( $i, 2, '0', STR_PAD_LEFT ); ?></option>
                  <?php } ?>
                </select>
                <select name="expire-year" id="checkout-exp-year" class="month-year year" data-validators="not_empty cc_expiry_year" data-flyvalidate="1">
                  <option value="">Año</option>
                  <?php for ( $i = date( 'Y' ) ; $i <= date( 'Y' ) + 15 ; $i++ ) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>
              <!--<img src="<?php echo $view['assets']->getUrl( 'assets/img/cc-icons.png'); ?>" alt="" class="checkout-cc-icons" />-->
            </div>

            <div id="checkout-captcha-row" <?php if ($captcha) echo 'data-captcha-load="1"'; ?>>
              <label for="checkout-captcha-input">Ingresa el código de verificación:</label>
              <input type="text" name="captcha" id="checkout-captcha-input" value="" class="std-field" placeholder="Ingresa el código..."/>
              <figure id="checkout-captcha-fig">
                <a class="captcha-reload icon icon-reload no-txt" title="Cargar otro código">Cargar otro código</a>
              </figure>
            </div>

            <!-- =SMS -->
            <div class="std-form clearfix checkout-sms">
              <div class="checkout-sms-hr"></div>

              <div class="form-row">
                <label>
                  <input class="checkout-sms-edit" type="checkbox" name="sms" value="1">
                  <span class="checkout-sms-strong">Recibir información de compra por SMS</span>
                </label>
              </div>

              <div class="clearfix checkout-sms-fields <?php if ($user !== NULL) : ?>checkout-sms-hidden<?php endif; ?>">
                <div class="form-row half">
                  <label for="checkout-sms-number">Número de celular <small class="helper">10 dígitos</small></label>
                  <input id="checkout-sms-number" type="text" name="mobile_number" value="<?php echo ($user !== NULL) ? $user->getData('mobile_number') : NULL; ?>" placeholder="55-1234-1234" />
                </div>

                <div class="form-row half">
                  <label for="checkout-sms-carrier">Operadora</label>
                  <select id="checkout-sms-carrier" name="mobile_carrier">
                    <option value="">Operadora</option>

                  <?php foreach ($carriers as $carrier) : ?>
                    <option value="<?php echo $carrier; ?>" <?php echo ($user !== NULL AND $user->getData('mobile_carrier') === $carrier) ? 'selected' : NULL; ?>><?php echo $carrier; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <!-- =SMS -->

            <div class="btn-row">
              <a href="#" class="btn btn-transparent btn-icon btn-light icon icon-larr" rel="checkout-back">Volver</a>
              <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Continuar</button>
            </div>
          </form><!-- #checkout-step-3-form -->
          <div class="disclaimer">
            <p>Cinemex Desarrollos, S.A. de C.V. (en adelante "Cinemex"), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>
            <p>Los Datos Personales y Financieros que le son solicitados, serán tratados con las finalidades primarias de comprar y confirmar los boletos que adquiere.</p>
            <p><a href="<?php echo $view['router']->generate('checkout_privacy_page'); ?>" target="_blank">Aviso de privacidad</a></p>
            <p>Fecha de última actualización: 5/Enero/2015</p>
          </div>
        </div><!-- #checkout-step-3 -->

        <div class="checkout-step content-box" id="checkout-step-4">
          <h2 class="content-box-title"><?php echo ( sizeof( $steps ) == 4 ) ? '4' : '3'; ?>. Compra finalizada</h2>

          <div id="checkout-confirmation"></div>
        </div><!-- #checkout-step-4 -->
      </div>

      <div class="disclaimer">
        <p>
          Los boletos son válidos únicamente para el día y horario seleccionado. No se realizarán reembolsos ni cambios de horarios una vez realizada la compra.
        </p>
      </div>

      <?php
      switch (strtolower($session->getMovie()->getData('RATING'))) {
        case 'c': ?>
        <div class="disclaimer alert alert-danger">
          <p><strong>AVISO IMPORTANTE:</strong> Cinemex informa al público en general que la clasificación emitida por la autoridad correspondiente para este título es “C” −clasificación de índole restrictivo para adultos únicamente,- art. 25 (Ley Federal de Cinematografía) −, por ningún motivo no se permitirá el acceso a menores de edad. Te pedimos tomar en cuenta lo anterior antes de realizar tu compra ya que contiene lenguaje procaz, sexo implícito y alto grado de violencia. Cinemex no lleva a cabo la solicitud, registro y emisión de la clasificación de exhibición comercial o preventa de esta película.</p>
        </div>
        <?php
          break;
        case 'd':
          ?>
        <div class="disclaimer alert alert-danger">
          <p><strong>AVISO IMPORTANTE:</strong> Cinemex informa al público en general que la clasificación emitida por la autoridad correspondiente para este título es “D” −clasificación de índole restrictivo para adultos únicamente,- art. 25 (Ley Federal de Cinematografía) −, por ningún motivo no se permitirá el acceso a menores de edad. Te pedimos tomar en cuenta lo anterior antes de realizar tu compra ya que contiene lenguaje procaz, sexo implícito y alto grado de violencia. Cinemex no lleva a cabo la solicitud, registro y emisión de la clasificación de exhibición comercial o preventa de esta película.</p>
        </div>
        <?php
          break;
        case 'b15':
            ?>
            <div class="disclaimer alert alert-danger">
              <p><strong>AVISO IMPORTANTE:</strong> Cinemex informa al público en general que la clasificación emitida por la autoridad correspondiente para este título es “B15” (No recomendada para menores de 15 años),- art. 25 (Ley Federal de Cinematografía), por ningún motivo no se permitirá el acceso a menores de 15 años. Te pedimos tomar en cuenta lo anterior antes de realizar tu compra ya que contiene ESCENAS DE VIOLENCIA EXPLICITA. Cinemex no lleva a cabo la solicitud, registro y emisión de la clasificación de exhibición comercial o preventa de esta película.</p>
            </div>
          <?php
          break;
        ?>
      <?php } ?>
    </div><!-- #checkout-process -->


    <div class="big-msg msg-timeout">
      <p class="big-msg-title">Tiempo</p>
      <p class="big-msg-msg">Has superado el tiempo necesario para poder realizar su compra.</p>
      <a href="<?php echo $view['router']->generate( 'checkout', array( 'session_id' => $session->getId() ) ); ?>" id="checkout-reload-bt" class="btn btn-default btn-icon icon icon-larr">Volver</a>
    </div>

	</div>
	<script>var seatallocationallowed = <?php echo ( 'N' == $session->getData( 'SEATALLOCATIONON' ) ) ? 'false' : 'true'; ?></script>
</div>

<?php if ($view['fronthelper']->getOptions('external_failure_notice')) { ?>
  <div id="external-failure">
    <p>
      <span class="big">Por problemas<br/>ajenos a Cinemex</span>
      no podemos completar la<br/>
      transacción temporalmente.<br/>
      Por favor intenta más tarde.
    </p>
    <div>
      <a href="javascript:history.back();" class="btn btn-default btn-icon icon icon-larr">Volver</a>
    </div>
  </div>
<?php } ?>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'popups' ); ?>
<div class="popup" id="pre-checkout-popup">
  <div class="popup-panel popup-top">
    <div class="popup-title line-01-bottom">
      <span class="icon icon-edit icon-dark no-txt"></span> Regístrate y ahorra tiempo</span>
    </div>
    <ul class="register-benefits">
      <li>Compra tus boletos en unos pocos clics.</li>
      <li>Fácil acceso al historial de compras.</li>
      <li>Recibe recomendaciones basadas en los gustos y calificaciones de tus amigos.</li>
      <li>Participa de promociones especiales, beneficios exclusivos y muchos beneficios más.</li>
    </ul>
    <div class="popup-btns">
      <a href="<?php echo $view['router']->generate( 'user_login' ); ?>" class="btn btn-default btn-icon icon icon-user" rel="popup-trigger" data-popup="#login-popup">Ingresar</a>
      <a href="<?php echo $view['router']->generate( 'user_register' ); ?>" class="btn btn-icon icon icon-edit">Registrarme</a>
    </div>
  </div>
  <div class="popup-panel popup-title popup-bottom">
    <span class="icon icon-user icon-dark no-txt"></span> Comprar sin registrarme</span>
    <a href="#" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right" rel="popup-close">Continuar</a>
  </div>

  <a href="#" class="popup-close icon-small icon-close no-txt">Cerrar</a>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'js_after_vendor' ); ?>
<script>
$( document ).bind( 'user_init', function() {
  if ( user && user.info && user.info.iecode ) {
    $( '#checkout-ie .ie-banner' ).remove();
    $( '#checkout-ie .heading-title .txt' ).text( '¡Ya estás sumando beneficios como Invitado Especial!' );
//    $( '#checkout-step-3-form [name=iecode]' ).val( user.info.iecode );
  }
} );
var checkout_movie_id  = '<?php echo $session->getMovie()->getId(); ?>';
var checkout_cinema_id = '<?php echo $session->getCinema()->getId(); ?>';
var checkout_area_id   = '<?php echo $session->getCinema()->getArea()->getId(); ?>';
var checkout_state_id  = '<?php echo $session->getCinema()->getState()->getId(); ?>';
</script>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('ga_extras'); ?>
ga('set', 'dimension1', '<?php echo $ref; ?>');
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('ga_extras_after'); ?>
ga('require', 'ecommerce');
<?php $view['slots']->stop(); ?>
