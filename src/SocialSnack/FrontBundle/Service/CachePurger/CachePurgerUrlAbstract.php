<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use Buzz\Browser;
use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

abstract class CachePurgerUrlAbstract implements CachePurgerInterface {

  protected $browser;

  protected $environments = [];

  public function __construct(Browser $browser) {
    $this->browser = $browser;
  }


  public function handle(UrlTaskInterface $task) {
    $output = [];

    foreach ($task->getUrlResources() as $urlResource) {
      $env = $this->environments[$urlResource['env']];

      $output = array_merge(
          $output,
          $this->process(
              $urlResource['path'],
              $env['hostname'],
              $env['servers']
          )
      );
    }

    return $output;
  }


  public function canHandle(CachePurgeTaskInterface $task) {
    return $task instanceof UrlTaskInterface;
  }


  public function setEnv($name, $config) {
    $this->environments[$name] = $config;
  }


  protected function process($path, $host, array $servers) {
    $output = [];

    foreach ($servers as $server) {
      $output[] = $this->purge($path, $host, $server);
    }

    return $output;
  }


  abstract protected function purge($path, $host, $server);

}