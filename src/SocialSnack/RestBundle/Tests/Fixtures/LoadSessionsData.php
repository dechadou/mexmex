<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\Session;

class LoadSessionsData extends AbstractFixture implements OrderedFixtureInterface {

  static $sessions;

  public function load(ObjectManager $manager) {
    self::$sessions = [];

    $tomorrow = new \DateTime('tomorrow');
    $movie = $this->getReference('Movie-1');
    $cinema = $this->getReference('Cinema-1');

    $session = new Session();
    $session
        ->setInfo(json_encode(['SEATALLOCATIONON' => 'Y', 'SCREENNAME' => '4']))
        ->setActive(TRUE)
        ->setCinema($cinema)
        ->setDate($tomorrow)
        ->setDateTime($tomorrow)
        ->setGroupId($movie->getGroupId())
        ->setLegacyCinemaId($cinema->getLegacyId())
        ->setLegacyId(1001)
        ->setMovie($movie)
        ->setTime($tomorrow)
        ->setTz('America/Mexico_City')
        ->setPlatinum(FALSE)
        ->setExtreme(FALSE)
    ;
    self::$sessions[] = $session;
    $manager->persist($session);
    $this->setReference('Session-1', $session);

    $manager->flush();
  }

  public function getOrder() {
    return 5;
  }

}