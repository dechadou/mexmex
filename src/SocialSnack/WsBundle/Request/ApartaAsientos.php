<?php
namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class ApartaAsientos extends ConsultaAbstract {
  
	protected $method = 'ApartadoAsientos';
	
  const OPTION_FREEZE  = 1;
  const OPTION_RELEASE = 2;
  
	protected $default_args = array(
			'Opcion'         => 0,
			'Session'        => 0,
			'CadenaAsientos' => '',
			'CadenaBoletos'  => '',
			'CodigoCine'     => 0,
			'TranTemp'       => '',
	);
	
  protected function __parse_res( $res ) {
		try {
			$xml = simplexml_load_string( trim( $res ) );
		} catch ( \Exception $e ) {
			throw $e;
		}
		
    return $xml;
  }


  /**
   * @param \SimpleXMLElement $xml
   * @return bool|string
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
   */
  public function parse_res($xml) {
    // Check for successful response.
		if (isset($xml->ERROR) && '0' == $xml->ERROR) {
      if ($this->args['Opcion'] === self::OPTION_FREEZE && isset($xml->CLAVETMP)) {
        return (string)$xml->CLAVETMP;
      } elseif ($this->args['Opcion'] === self::OPTION_RELEASE) {
        return TRUE;
      }
    }
    
    if (isset($xml->ERRORDESC)) {
			throw new RequestMsgException( $xml->ERRORDESC );
    } else {
			throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
    }
  }
  
}
