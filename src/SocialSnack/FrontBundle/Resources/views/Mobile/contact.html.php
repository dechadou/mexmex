<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>

<?php $view['slots']->start('body'); ?>
<div class="box1">
  <div class="content-box">
    <article class="entry-full">
      <h1 class="content-box-title">Contáctanos</h1>

      <div class="content-text">
        <p class="checkout-legend">Tu opinión es muy importante para nosotros, envíanos
          tus opiniones y comentarios acerca de nuestro servicio para poder mejorar cada día.</p>

        <form action="" method="post" id="contact-form" data-basicvalidate="1" data-validcb="contact_submit">
          <div class="form-row">
            <label for="name">Nombre *</label>

            <input type="text" name="name" id="name" value="" data-validators="not_empty" data-flyvalidate="1" />
          </div>

          <div class="form-row">
            <label for="email">Email *</label>

            <input type="email" name="email" id="email" value="" data-validators="not_empty email" data-flyvalidate="1" />
          </div>

          <div class="form-row">
            <label for="ie">N° de Invitado Especial</label>

            <input type="text" name="ie" id="ie" value="" />
          </div>

          <div class="form-row">
            <label for="phone">Teléfono</label>

            <input type="text" name="phone" id="phone" value="" />
          </div>

          <div class="form-row half">
            <label for="cinema">Cine</label>

            <select name="cinema" id="cinema">
              <option value="">Selecciona un cine</option>

              <?php foreach ($cinemas as $cinema) : ?>
              <option value="<?php echo $cinema->getId(); ?>"><?php echo $cinema->getName(); ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="form-row half">
            <label for="service">Servicio</label>

            <select name="service" id="service">
              <option value="">Selecciona una opción</option>
              <option value="Dulcería">Dulcería</option>
              <option value="General">General</option>
              <option value="Invitado Especial">Invitado Especial</option>
              <option value="Internet">Internet</option>
              <option value="Proyección">Proyección</option>
              <option value="Taquilla">Taquilla</option>
            </select>
          </div>

          <div class="form-row half">
            <label for="type">Tipo de mensaje</label>

            <select name="type" id="type">
              <option value="">Selecciona una opción</option>
              <option value="Felicitación">Felicitación</option>
              <option value="Información">Información</option>
              <option value="Queja">Queja</option>
              <option value="Sugerencia">Sugerencia</option>
            </select>
          </div>

          <div class="form-row half">
            <label for="msg">Mensaje *</label>
            <textarea name="msg" id="contact_msg" data-validators="not_empty" data-flyvalidate="1"></textarea>
          </div>

          <div class="disclaimer">
            <p>Cinemex Desarrollos, S.A. de C.V. (en adelante "Cinemex"), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente: </p>
            <p>Los Datos Personales que le son solicitados, serán tratados con las finalidades primarias de atender y darle seguimiento a sus dudas, quejas, comentarios o sugerencias.</p>
            <p><a href="<?php echo $view['router']->generate('contact_privacy_page'); ?>">Aviso de Privacidad</a></p>
          </div>

          <div class="btn-row">
            <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Enviar</button>
          </div>
        </form>
      </div>
    </article>
  </div>
</div>
<?php $view['slots']->stop(); ?>
