<?php

namespace SocialSnack\FrontBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;

class DefaultControllerTest extends WebTestCase {


  /**
   * @group functional
   * @group smoke
   */
  public function testAnnouncementsAction() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/comunicados',
        array(),
        array(),
        array('HTTP_HOST' => $client->getContainer()->getParameter('domain'))
    );

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testArtCinemasAction() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/casa-de-arte/cines',
        array(),
        array(),
        array('HTTP_HOST' => $client->getContainer()->getParameter('domain'))
    );

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testCinemaSingleAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);
    $cinema_id = LoadSessionData::$cinemas[0]->getId();
    $client = static::createClient();
    $client->request(
        'GET',
        '/cine/' . $cinema_id,
        array(),
        array(),
        array('HTTP_HOST' => $client->getContainer()->getParameter('domain'))
    );

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testNewsSingleAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadArticlesData'
    ]);
    $cinema_id = LoadArticlesData::$artices[0]->getId();
    $client = static::createClient();
    $client->request(
        'GET',
        '/novedades/' . $cinema_id,
        array(),
        array(),
        array('HTTP_HOST' => $client->getContainer()->getParameter('domain'))
    );

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }



}
