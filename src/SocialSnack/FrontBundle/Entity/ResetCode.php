<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResetCode
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_code",columns={"code"})
 *   }
 * )
 * @ORM\Entity
 */
class ResetCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;
    
    /**
     * @var SocialSnack\FrontBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="SocialSnack\FrontBundle\Entity\User")
     */
    private $user;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    
    public function __construct() {
      $this->dateCreated = new \DateTime('now');
      $this->code = md5(uniqid(null, true));
      $this->active = TRUE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ResetCode
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return ResetCode
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set user
     *
     * @param \SocialSnack\FrontBundle\Entity\User $user
     * @return ResetCode
     */
    public function setUser(\SocialSnack\FrontBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialSnack\FrontBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ResetCode
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
}