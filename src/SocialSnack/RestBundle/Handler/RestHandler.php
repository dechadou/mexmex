<?php

namespace SocialSnack\RestBundle\Handler;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RestHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class RestHandler {

  protected $container;

  protected $doctrine;

  public function __construct(Registry $doctrine, ContainerInterface $container) {
    $this->doctrine = $doctrine;
    $this->container = $container;
  }

  /**
   * @param $device
   * @return \SocialSnack\RestBundle\Entity\AppVersion
   */
  public function getAppVersion($device) {
    $version = $this->doctrine->getRepository('SocialSnackRestBundle:AppVersion')->findBy(
        array(
            'device' => $device,
        ),
        array(
            'id' => 'DESC'
        ),
        1
    );
    if (!$version || !sizeof($version)) {
      return NULL;
    }

    return $version[0];
  }

} 