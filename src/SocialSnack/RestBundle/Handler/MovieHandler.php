<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Service\Helper as WsHelper;

/**
 * Class MovieHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class MovieHandler {

  protected $doctrine;

  protected $front_utils;

  public function __construct(Registry $doctrine, FrontUtils $front_utils) {
    $this->doctrine    = $doctrine;
    $this->front_utils = $front_utils;
  }


  public function getAll($area_type, $area_id, array $include_other_versions = NULL) {
    switch ($area_type) {
      case 'area':
        $state_id = NULL;
        $cinema_ids = NULL;
        break;

      case 'state':
        $state_id = $area_id;
        $area_id = NULL;
        $cinema_ids = NULL;
        break;

      case 'cinema':
        $cinema_ids = $area_id;
        $state_id = NULL;
        $area_id = NULL;
        break;

      default:
        $state_id = NULL;
        $area_id = NULL;
        $cinema_ids = NULL;
        break;
    }

    /* @var $movie_repo \SocialSnack\WsBundle\Entity\MovieRepository */
    $movie_repo  = $this->doctrine->getRepository( 'SocialSnackWsBundle:Movie' );

    $non_version_attrs = array_keys(FrontHelper::get_attributes_collection(array('version' => FALSE)));
    $non_version_attrs[] = 'premiere'; // Premiere is not actually an attribute but is present in the "type"
    $non_version_attrs[] = 'ESTRENO';  // property so... let's hardcode this :$

    $result = [];

    if (!is_null($area_id)) {
      $movies   = $this->findMoviesIn('area', $area_id);
      $versions = $this->findMoviesVersionsIn('area', $area_id);
    } elseif (!is_null($state_id)) {
      $movies   = $this->findMoviesIn('state', $state_id);
      $versions = $this->findMoviesVersionsIn('state', $state_id);
    } elseif (!is_null($cinema_ids)) {
      $movies   = $this->findMoviesIn('cinema', $cinema_ids);
      $versions = $this->findMoviesVersionsIn('cinema', $cinema_ids);
    } else {
      $movies   = $movie_repo->findAll();
      $versions = $this->findMoviesVersionsIn('area', $area_id);
    }

    $versions = $this->parseVersionsResult($versions, $include_other_versions ? ['user_fav' => 1] : []);

    if ($include_other_versions) {
      // Find all versions in area.
      $otherVersions = $this->findMoviesVersionsIn(
          $include_other_versions['area_type'],
          $include_other_versions['area_id']
      );
      $otherVersions = $this->parseVersionsResult($otherVersions, ['user_fav' => 0]);

      // Merge versions from favorite cinemas with versions in other cinemas.
      foreach ($versions as $groupId => $item) {
        if (isset($otherVersions[$groupId])) {
          $versions[$groupId] = $item + $otherVersions[$groupId];
        }
      }
    }

    foreach ( $movies as $movie ) {
      $movie_types = [];
      $_movie      = $this->serializeOne($movie, TRUE, FALSE, FALSE);

      $_movie['versions'] = array_values($versions[$movie->getGroupId()]);
      foreach ($_movie['versions'] as &$version) {
        $version['label'] = FrontHelper::get_movie_attr_str(json_decode($version['attributes']));
        if (!$version['label']) {
          $version['label'] = 'Tradicional';
        }
        $version['type'] = array_keys((array)json_decode($version['attributes']));
        unset($version['attributes']);

        $movie_types = array_merge($movie_types, $version['type']);
      }

      // The grouper inherits the type attributes from his children because only from those with active sessions in the
      // queried area should be present.
      if ('grouper' === $movie->getType()) {
        $_movie['type'] = array_values(array_unique(array_merge(
            array_intersect($_movie['type'], $non_version_attrs),
            array_values($movie_types)
        )));
      }

      $_movie['label'] = FrontHelper::get_movie_attr_str($_movie['type']);

      $result[] = $_movie;
    }

    return $result;
  }


  protected function findMoviesIn($area_type, $area_id) {
    if (!in_array($area_type, ['area', 'state', 'cinema'])) {
      throw new \Exception('Invalid area type.');
    }

    if (!is_array($area_id)) {
      $area_id = [$area_id];
    }

    $conn = $this->doctrine->getConnection();
    $stmt = $conn->executeQuery(
        "SELECT DISTINCT movie_id FROM MoviesByArea WHERE {$area_type}_id IN (?)",
        array($area_id),
        array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
    );
    $stmt->execute();
    $ids = $stmt->fetchAll();
    $ids = array_map('current', $ids);

    $movies = $this->doctrine->getRepository('SocialSnackWsBundle:Movie')->findManyBy($ids);
    WsHelper::sortMoviesDefault($movies);

    return $movies;
  }


  protected function findMoviesVersionsIn($area_type, $area_id) {
    if (!in_array($area_type, ['area', 'state', 'cinema'])) {
      throw new \Exception('Invalid area type.');
    }

    if (!is_array($area_id)) {
      $area_id = [$area_id];
    }

    // @todo Implement some cache.
//    $cache_id = md5(__METHOD__ . '::' . $area_type . '|' . implode(',', $area_id));
//
//    $cached = $this->memcached->get($cache_id);
//    if ($cached && $cached['time'] + (60 * 60) < time()) {
//      return $cached['result'];
//    }

    $conn = $this->doctrine->getConnection();
    $stmt = $conn->executeQuery(
        "SELECT DISTINCT movie_id, group_id, attributes FROM MovieVersionsByCinema WHERE {$area_type}_id IN (?)",
        array($area_id),
        array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
    );
    $stmt->execute();
    return $stmt->fetchAll();
  }


  public function getCinemasWithMovieSessions($movie_id, $cinemas) {
    $result = array();

    foreach ($cinemas as $cinema) {
      $cinema_info = $cinema->toArray();
      $cinema_info['sessions'] = array();

      $sessions = $this->querySessionsInCinema($movie_id, $cinema->getId());
      foreach ($sessions as $session) {
        $date_wtz = new \DateTime($session['date_time'], new \DateTimeZone($session['tz']));
        $cinema_info['sessions'][] = array(
            'id'        => $session['id'],
            'date'      => $date_wtz->getTimestamp(),
            'tz_offset' => $date_wtz->getOffset(),
            'cinema_id' => $cinema->getId(),
        );
      }
      $result[] = $cinema_info;
    }

    return $result;
  }

  /**
   * @param $movie_id
   * @param $cinema_id
   * @return array
   */
  protected function querySessionsInCinema($movie_id, $cinema_id) {
    $conn  = $this->doctrine->getConnection();
    $query = "SELECT s.id, s.date_time, s.cinema_id, s.tz FROM Session s
      WHERE s.movie_id = :movie_id AND s.cinema_id = :cinema_id AND s.active = 1";
    $stmt  = $conn->prepare($query);

    $stmt->bindValue('movie_id', $movie_id);
    $stmt->bindValue('cinema_id', $cinema_id);
    $stmt->execute();

    return $stmt->fetchAll();
  }


  /**
   * @param Movie $movie
   * @param bool  $populate_info
   * @param bool  $populate_sessions
   * @param bool  $include_versions
   * @return array
   */
  public function serializeOne(Movie $movie, $populate_info = TRUE, $populate_sessions = FALSE, $include_versions = TRUE) {
    $_movie                  = $movie->toArray($populate_info, $populate_sessions, $include_versions);
    $_movie['poster_small']  = $this->front_utils->get_poster_url($_movie['cover'], 'small');
    $_movie['poster_medium'] = $this->front_utils->get_poster_url($_movie['cover'], 'medium');
    $_movie['poster_big']    = $this->front_utils->get_poster_url($_movie['cover'], 'big');
    $_movie['cover']         = $this->front_utils->get_poster_url($_movie['cover'], '200x295');
    $_movie['url']           = $this->front_utils->get_permalink($movie);
    $_movie['featured']      = $movie->getFeatured();
    $_movie['ribbons']       = $this->getMovieRibbons($movie);
    return $_movie;
  }


  /**
   * @param Movie $movie
   * @return array
   */
  protected function getMovieRibbons(Movie $movie) {
    $ribbons = [];

    if ($movie->getPremiere()) {
      $ribbons[] = [
          'label'     => 'Estreno',
          'bgcolor'   => '#ee174f',
          'textcolor' => '#FFFFFF',
      ];
    }

    $attr_ribbons = [
        'exclusive'     => [
            'label'     => 'Exclusiva',
            'bgcolor'   => '#ff7a00',
            'textcolor' => '#FFFFFF',
        ],
        'preview'       => [
            'label'     => 'Pre estreno',
            'bgcolor'   => '#bd0b2f',
            'textcolor' => '#FFFFFF',
        ],
        'presale'       => [
            'label'     => 'Preventa',
            'bgcolor'   => '#959595',
            'textcolor' => '#FFFFFF',
        ],
        'rerelease'     => [
            'label'     => 'Re-estreno',
            'bgcolor'   => '#ee174f',
            'textcolor' => '#FFFFFF',
        ],
        'tour'          => [
            'label'     => 'Tur de cine',
            'bgcolor'   => '#ff7a00',
            'textcolor' => '#FFFFFF',
        ],
        'fest'          => [
            'label'     => 'Festivales',
            'bgcolor'   => '#ff7a00',
            'textcolor' => '#FFFFFF',
        ],
        'oscar_nominee' => [
            'label'     => 'Nominada',
            'bgcolor'   => '#bf9d1b',
            'textcolor' => '#000000',
        ],
        'oscar_winner'  => [
            'label'     => 'Ganadora',
            'bgcolor'   => '#bf9d1b',
            'textcolor' => '#000000',
        ],
        'v3d'           => [
            'label'     => '3D',
            'bgcolor'   => '#282828',
            'textcolor' => '#FFFFFF',
        ],
        'v4d'           => [
            'label'     => '4D',
            'bgcolor'   => '#282828',
            'textcolor' => '#FFFFFF',
        ],
        'classics'      => [
            'label'     => 'Clásicos',
            'bgcolor'   => '#bf9d1b',
            'textcolor' => '#FFFFFF',
        ]
    ];

    foreach ($attr_ribbons as $attr => $ribbon) {
      if ($movie->getAttr($attr)) {
        $ribbons[] = $ribbon;
      }
    }

    return $ribbons;
  }


  protected function parseVersionsResult($versions, array $extra = []) {
    return array_reduce($versions, function($carry, $item) use ($extra) {
      $group_id = $item['group_id'];
      $movie_id = (int)$item['movie_id'];

      if (!isset($carry[$group_id])) {
        $carry[$group_id] = [];
      }

      $carry[$group_id][$movie_id] = [
          'id' => $movie_id,
          'attributes' => $item['attributes'],
      ] + $extra;

      return $carry;
    }, []);
  }

} 