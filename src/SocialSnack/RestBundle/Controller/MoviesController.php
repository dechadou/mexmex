<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\FrontBundle\Entity\MovieVote;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\WsBundle\Entity\MovieComing;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MoviesController extends BaseController {
	
	
	/**
	 * @Route("/", name="rest_movies_default")
	 * @Route("/area/{area_id}")
	 * @Route("/state/{state_id}")
	 * @Method({"GET"})
	 */
	public function defaultAction($area_id = NULL, $state_id = NULL, Request $request) {
		// If IDs specified, return the detailed info.
		if ($ids = $request->query->get('ids')) {
			$res = $this->forward('SocialSnackRestBundle:Movies:details', array('id' => $ids));
			return $res;
		}

    $handler = $this->get('rest.movie.handler');

    if (!is_null($area_id)) {
      $result = $handler->getAll('area', $area_id);
    } else {
      $result = $handler->getAll('state', $state_id);
    }

		$response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 45 * 60 );
    
    return $response;
	}
	
	
	/**
	 * @Route("/search", name="rest_movies_search")
	 * @Method({"GET"})
	 */
	public function searchAction(Request $request) {
    $term    = $request->get( 'q' );
    $limit   = $request->get( 'limit' );
    $_movies = array();
    
    if ( strlen( $term ) > 2 ) {
      /* @var $qb \Doctrine\ORM\QueryBuilder */
      $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
      $qb->addSelect( 'm' )
          ->from( 'SocialSnackWsBundle:Movie', 'm' )
          ->where( $qb->expr()->orX(
              $qb->expr()->like( 'm.name', ':term' ),
              $qb->expr()->like( 'm.info', ':term' )
          ) )
          ->andWhere( $qb->expr()->isNull( 'm.parent' ) )
          ->andWhere( 'm.position > 0' )
          ->setParameter( 'term', sprintf( '%%%s%%', $term ) );
      
      // Filters...
      if ( $request->get('premiere') ) {
        $qb->andWhere('m.premiere = 1');
      }
      if ( $request->get('art') ) {
        WsHelper::dql_apply_movie_attrs($qb, array('art' => TRUE));
      }
      if ( $request->get('v4d') ) {
        WsHelper::dql_apply_movie_attrs($qb, array('v4d' => TRUE));
      }
      if ( $request->get('v3d') ) {
        WsHelper::dql_apply_movie_attrs($qb, array('v3d' => TRUE));
      }
      if ( $request->get('platinum') ) {
        $qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm.id = s.id OR m.group_id = s.group_id' )
            ->innerJoin( 's.cinema', 'c' )
            ->andWhere( 's.active = 1' )
            ->andWhere('c.platinum = 1')
            ->groupBy('m.id');
      }
      if ( $request->get('premium') ) {
        $qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm.id = s.id OR m.group_id = s.group_id' )
            ->andWhere( 's.active = 1' )
            ->andWhere('s.premium = 1')
            ->groupBy('m.id');
      }
      if ( $request->get('extreme') ) {
        $qb->innerJoin( 'SocialSnackWsBundle:Session', 's', 'WITH', 'm.id = s.id OR m.group_id = s.group_id' )
            ->andWhere( 's.active = 1' )
            ->andWhere('s.extreme = 1')
            ->groupBy('m.id');
      }
      
      // Default limit = 10. Max limit = 25.
      if ( !$limit ) {
        $limit = 10;
      } else if ( $limit > 25 ) {
        $limit = 25;
      }
      $qb->setMaxResults( $limit );
      
      $query = $qb->getQuery();
      $query->useResultCache( TRUE, 60 * 60 );
      
      $movies = $query->execute();
      
      foreach ( $movies as $movie ) {
        $_movie = $movie->toArray();
        $_movie['url'] = $this->generateUrl( 'movie_single', array( 'movie_id' => $movie->getId(), 'slug' => $movie->getSlug() ) );
        $_movie['covers'] = array(
            '34x45' => $this->container->get( 'ss.front.utils' )->get_poster_url( $_movie['cover'], '34x45' )
        );
        $_movie['cover'] = $this->container->get( 'ss.front.utils' )->get_poster_url( $_movie['cover'], '200x295' );
        $_movie['label'] = FrontHelper::get_movie_attr_str($movie->getAttr());
        
        foreach ( $_movie['versions'] as &$version ) {
          $version['label'] = FrontHelper::get_movie_attr_str(array_flip($version['type']), ' ', FALSE);
        }
        
        $_movies[] = $_movie;
      }
    }
    
    $response = new JsonResponse( array( 'movies' => $_movies ) );
    $response->setPublic();
    $response->setMaxAge( 30 * 60 );
    $response->setSharedMaxAge( 60 * 60 );
    
    return $response;
	}
		
	
	/**
	 * Return detailed info for movie(s).
	 * 
	 * @Route("/{id}", name="rest_movies_single", requirements={"id"="[0-9]+"})
	 * @Method({"GET"})
	 */
	public function detailsAction($id) {
		$ids = explode( ',', $id );
		$ids = array_map( 'trim', $ids );

    $handler    = $this->get('rest.movie.handler');
		$movie_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movies     = $movie_repo->findManyBy( $ids, array( 'active' => 1 ) );
		$result     = array();
		
		if ( !$movies  )
			return new JsonResponse( array(), 404 );
		
		foreach ( $movies as $movie ) {
      $_movie = $handler->serializeOne($movie);
      $_movie['label'] = FrontHelper::get_movie_attr_str($movie->getAttr());

      foreach ( $_movie['versions'] as &$version ) {
        $version['label'] = FrontHelper::get_movie_attr_str(array_flip($version['type']), ' ', FALSE);
      }
			$result[] = $_movie;
		}
		
		if ( 1 == sizeof( $result ) ) {
			$result = $result[0];
		}
		
		$response = new JsonResponse( $result, 200 );
    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 60 * 60 );
    
    return $response;
	}
	
	
	/**
	 * @Route("/{id}/cinemas", name="rest_movies_cinemas")
	 * @Method({"GET"})
	 */
	public function cinemasAction( $id ) {
		/** @todo Everything :D */
		return new JsonResponse( array( 'error' => 'not-implemented' ), 501 );
	}


  /**
   * $_GET['date']       Optional.
   * $_GET['cinema_id']  Optional.
   *
   * @Route("/{id}/sessions",                  name="rest_movies_sessions")
   * @Route("/{id}/sessions/area/{area_id}",   name="rest_movies_sessions_area")
   * @Route("/{id}/sessions/state/{state_id}", name="rest_movies_sessions_state")
   * @Route("/{id}/sessions/favcinemas",       name="rest_movies_sessions_fav")
   * @Method({"GET"})
   *
   * @param integer $id
   * @param integer $area_id
   * @param integer $state_id
   * @param Request $request
   * @return JsonResponse
   * @throws RestException
   */
	public function sessionsAction($id, $area_id = NULL, $state_id = NULL, Request $request) {
    /** @var \SocialSnack\RestBundle\Handler\MovieHandler $handler */
    $handler = $this->get('rest.movie.handler');

    /** @var \SocialSnack\RestBundle\Handler\ContextHandler $context_handler */
    $context_handler = $this->get('rest.context.handler');

    $api_v1      = $context_handler->getApiVersion() < 2;
    $user_faves  = [];
    $route       = $request->attributes->get('_route');
		$cinema_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
		$cinemas     = array();

		$response = new JsonResponse();
    $response->setPublic();
    $response->setSharedMaxAge( 60 * 60 );
    $response->setMaxAge( 15 * 60 );
    
    if ('rest_movies_sessions_fav' === $route || 'rest_v1_movies_sessions_fav' === $route) {
      $this->_authorize($request);

      // Cache specifically for this user.
      if ($api_v1) {
        $response->setVary('Authorization,Authorizationx');
      } else {
        $request->attributes->set('vary', 'Authorization,Authorizationx');
      }
      $user_faves = $this->get_fave_cinemas_ids($this->user->getId());
      $etag       = md5(implode('|', $user_faves));
      $response->setSharedMaxAge(0);
      $response->setMaxAge(0);
      $response->setEtag($etag);

      if ( $response->isNotModified($this->getRequest()) ) {
        // If ETag matches return 304 code.
        return $response;
      }

      if ($user_faves) {
        $cinemas = $cinema_repo->findPlayingMovieInCinemas($id, $user_faves);
      }
    } elseif ( !is_null($area_id) ) {
      $cinemas = $cinema_repo->findPlayingMovieInArea($id, $area_id);
    } elseif ( !is_null($state_id) ) {
      $cinemas = $cinema_repo->findPlayingMovieInState($id, $state_id);
    } elseif ($cinema_ids_str = $request->query->get('cinema_ids')) {
      $cinema_ids = array_map('trim', explode(',', $cinema_ids_str));
      sort($cinema_ids);
      if (implode(',', $cinema_ids) !== $cinema_ids_str) {
        throw new RestException('query-format', 'Please, format query string properly :)');
      }
      $cinemas = $cinema_repo->findPlayingMovieInCinemas($id, $cinema_ids);
    } else {
      // Deprecated since V2.
      if ($api_v1) {
        $this->get('logger')->warning('Deprecated /rest/movies/{id}/sessions call.');
        $cinemas = $cinema_repo->findPlayingMovieInArea( $id, $area_id );
      } else {
        throw new RestException('deprecated', 'This method was deprecated since v2.', 410);
      }
    }

    $result = $handler->getCinemasWithMovieSessions($id, $cinemas);

    // Deprecated since V2.
    if ($api_v1) {
      // Add the user_fav flag.
      foreach ($result as &$cinema) {
        $cinema['user_fav'] = $user_faves
            ? ((in_array($cinema['id'], $user_faves)) ? 1 : 0)
            : 0;
      }
    }

		$response->setData($result);
    return $response;
	}
  
  
  /**
   * $_POST['value'] Required.
   * 
   * @Route("/{id}/vote", name="rest_movies_vote")
   * @Method({"POST", "GET"})
   */
  public function voteAction($id, Request $request) {
    $this->_authorize();

    $value = (int)$request->get('value');
    $em    = $this->getDoctrine()->getManager();
		
		// Check required fields.
		if ( empty( $value ) ) {
			return new JsonResponse( array( 'error' => 'Missing value.' ), 400 );
    }
    
    // Make sure nobody is trying to cheat...
    if ( $value < 1 || $value > 5 ) {
      return new JsonResponse( array( 'error' => 'Invalid value.' ), 503 );
    }
    
		// Find Movie.
    /** @var \SocialSnack\WsBundle\Entity\Movie $movie */
		$movie_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movie      = $movie_repo->find( $id );
		
		if ( !$movie )
			return new JsonResponse( array( 'error' => 'Invalid movie ID.' ), 400 );
		
		// Check if the user has already rated this movie.
		$vote_repo  = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:MovieVote' );
		$vote       = $vote_repo->findOneBy( array( 'movie' => $movie->getId(), 'user' => $this->user->getId() ) );
		
		if ( $vote ) {
			if ( $vote->getScore() == $value )
				return new JsonResponse( array( 'response' => 'No changes' ), 200 );
		} else {
			$vote = new MovieVote();
			$vote->setMovie( $movie );
			$vote->setUser( $this->user );
			$em->persist( $vote );
		}
		
		// Write vote.
		$vote->setScore( $value );
		$em->flush();
		
		// @todo Move this piece of code somewhere else
		// @todo Don't hardcode the table name
		// Recalculate the score.
		$query = $em->getConnection()->prepare( 'SELECT AVG(score) AS avg_score FROM MovieVote WHERE movie_id = :movie_id' );
		$query->bindValue( 'movie_id', $movie->getId() );
		$query->execute();
		$score = round( $query->fetchColumn(), 2 );
		if ($movie->getScore() != $score) {
			$movie->setScore($score);

      foreach ($movie->getChildren() as $movie) {
        $movie->setScore($score);
      }

			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array( 'score' => $movie->getScore() ), 200 );
		
  }


  /**
   * @Route("/coming", name="rest_movies_coming")
   * @param Request $request
   * @return JsonResponse
   */
  public function comingAction(Request $request) {
    /** @var $handler \SocialSnack\RestBundle\Handler\MovieComingHandler */
    $handler = $this->container->get('rest.moviecoming.handler');

    // Pagination vars.
    $page = $request->query->get('page');
    $limit = $request->query->get('count');
    list($page, $offset, $limit) = $handler->getOffsetLimit($page, $limit);

    // Return response.
    $res = new JsonResponse(array(
        'movies' => $handler->getAll($offset, $limit),
        'pagination' => $handler->getAllPagination($request, $page, $limit),
    ));
    $res->setSharedMaxAge(60 * 60);
    $res->setMaxAge(10 * 60);
    return $res;
  }


  /**
   * @Route("/coming/{id}", name="rest_movies_comingSingle")
   * @Method({"GET"})
   *
   * @param MovieComing $movie
   * @param Request     $request
   * @return JsonResponse
   */
  public function comingSingleAction(MovieComing $movie, Request $request) {
    /** @var $handler \SocialSnack\RestBundle\Handler\MovieComingHandler */
    $handler = $this->container->get('rest.moviecoming.handler');

    $res = new JsonResponse($handler->serializeOne($movie));
    $res->setSharedMaxAge(60 * 60);
    $res->setMaxAge(15 * 60);
    return $res;
  }

}