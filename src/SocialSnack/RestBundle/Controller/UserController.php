<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @todo Secure resources
 */
class UserController extends BaseController {
	
	
	/**
	 * Endpoints which require user authorization.
	 * @var array
	 */
	static $auth_methods = array(
			'iecode',
			'iecodePost',
			'iecodeDelete',
	);
	
	
	/**
	 * @Route("/")
   * @Method({"GET"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function defaultAction(Request $request) {
		$this->_authorize($request);
		$handler = $this->get('rest.user.handler');
		return new JsonResponse($handler->serializeOne($this->user));
	}
  
  
  /**
   * @Route("/")
   * @Method({"PUT"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function updateAction(Request $request) {
		$this->_authorize($request);
    $user     = $this->user;
    $pass_set = FALSE;

    foreach ( array( 'first_name', 'last_name', 'email' ) as $field ) {
      if ( !$value = $request->request->get($field) )
        continue;
      $method = 'set' . \Symfony\Component\DependencyInjection\Container::camelize($field);
      $user->{$method}($value);
    }
    
    if ( $plain_password = $request->request->get('password') ) {
      // If a new password was supplied then store the hash for that password.
      $factory = $this->get('security.encoder_factory');
      $encoder = $factory->getEncoder($user);
      $password = $encoder->encodePassword($plain_password, $user->getSalt());
      $user->setPassword($password);
      $pass_set = TRUE;
    }
    
    $em = $this->getDoctrine()->getManager();
    $em->flush();
    
    if ( !$pass_set ) {
      return $this->forward('SocialSnackRestBundle:User:default');
    } else {
      return $this->success_auth_res($user);
    }
  }
	
	
	/**
	 * @Route("/iecode")
	 * @Method({"GET"})
	 */
	public function iecodeAction(Request $request) {
		$this->_authorize($request);
		
		return new JsonResponse( array( 'iecode' => $this->user->getIecode() ), 200 );
	}
	
	
	/**
	 * @Route("/iecode/validate")
	 * @Method({"POST"})
	 */
	public function iecodeValidateAction(Request $request) {
		$this->_authorize($request);
		
		$iecode = $request->request->get('iecode');

    // Check required fields.
    if (empty($iecode)) {
      throw new MissingFieldsException();
    }

    // Validate code. If no exception thrown, the code is valid.
    $this->get('rest.user.handler')->validateIECode($iecode);

    return new JsonResponse();
	}
	
	
	/**
	 * Link Invitado Especial account with user account.
	 * 
	 * Requires $_REQUEST['iecode'] string Invitado Especial code.
	 * 
	 * @Route("/iecode")
	 * @Method({"POST"})
	 */
	public function iecodePostAction(Request $request) {
		$this->_authorize($request);

		$iecode = $request->request->get('iecode');
		$iecode = str_replace(' ', '', $iecode);
		$em     = $this->getDoctrine()->getManager();
		
    // There was some issue with Varnish or something using _method so this is
    // a workaround:
    if ( 'DELETE' == $request->get('s_method') ) {
      return $this->forward('SocialSnackRestBundle:User:iecodeDelete');
    }

    // Check required fields.
    if (empty($iecode)) {
      throw new MissingFieldsException();
    }

    // Validate code. If no exception thrown, the code is valid.
    $this->get('rest.user.handler')->validateIECode($iecode);

    // Valid code, link to user and persist in DB.
    $this->user->setIecode($iecode);
    $em->flush();

    // Return success code.
    return new JsonResponse(array('iecode' => $this->user->getIecode()), 200);
	}
	
	
	/**
	 * Unlink Invitado Especial account from user account.
	 * 
	 * @Route("/iecode")
	 * @Method({"DELETE"})
	 */
	public function iecodeDeleteAction(Request $request) {
		$this->_authorize($request);
		
		$em = $this->getDoctrine()->getManager();
		
		$this->user->setIecode( NULL );
		$em->flush();
		
		// Return success code.
		return new JsonResponse( array( 'iecode' => $this->user->getIecode() ), 200 );
	}
	
	
	/**
	 * @Route("/fbuid")
	 * @Method({"GET"})
	 */
	public function fbuidAction(Request $request) {
		$this->_authorize($request);
		
		return new JsonResponse( array( 'fbuid' => $this->user->getFbuid() ), 200 );
	}
	
	
	/**
	 * Link FB account with user account.
	 * 
	 * Requires $_REQUEST['fbsigned_request'] or $_REQUEST['fbaccess_token'] string Facebook User ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/fbuid")
	 * @Method({"POST"})
	 */
	public function fbuidPostAction(Request $request) {
		$this->_authorize($request);

		$fbuid        = NULL;
		$signed_req   = $request->request->get( 'fbsigned_request' );
		$access_token = $request->request->get( 'fbaccess_token' );
		$em           = $this->getDoctrine()->getManager();
		
		// Check required fields.
		if ( empty($signed_req) && empty($access_token) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		/** @todo Process fbsigned_request */
		
		if ( !$fbuid )
			return new JsonResponse( array( 'error' => 'Failed to get user.' ), 400 );
		
		$this->user->setFbuid( $fbuid );
		$em->flush();
		
		// Return success code.
		return new JsonResponse( array( 'fbuid' => $this->user->getFbuid() ), 200 );
	}
	
	
	/**
	 * Unlink FB account from user account.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 *
	 * @Route("/fbuid")
	 * @Method({"DELETE"})
	 */
	public function fbuidDeleteAction(Request $request) {
		$this->_authorize($request);
		
		$em = $this->getDoctrine()->getManager();
		
		$this->user->setFbuid( NULL );
		$em->flush();
		
		// Return success code.
		return new JsonResponse( array( 'fbuid' => $this->user->getFbuid() ), 200 );
	}
	
	
	/**
	 * @Route("/cinemas")
	 * @Method({"GET"})
	 */
	public function cinemasAction(Request $request) {
		$this->_authorize($request);
		$cinemas = $this->get('rest.user.handler')->serializeUserFavCinemas($this->user);
		return new JsonResponse($cinemas);
	}


	/**
	 * @Route("/cinemas/movies")
	 * @Method({"GET"})
	 *
	 * @param Request $request
	 * @return JsonResponse
	 * @throws RestException
	 */
	public function cinemasMoviesAction(Request $request) {
		$this->_authorize($request);

		$cinema_ids = $this->user->getCinemasFav()->map(function($a) { return $a->getId(); })->toArray();

		if (!sizeof($cinema_ids)) {
			return new JsonResponse([]);
		}

		if ($request->query->has('include_other_versions')) {
			$other = explode('-', $request->query->get('include_other_versions'));
			$includeOthers = [
					'area_type' => $other[0],
					'area_id' => $other[1],
			];

			if (!in_array($includeOthers['area_type'], ['area', 'state']) || !is_numeric($includeOthers['area_id'])) {
				throw new RestException();
			}
		} else {
			$includeOthers = NULL;
		}

		/** @var \SocialSnack\RestBundle\Handler\MovieHandler $handler */
		$handler = $this->get('rest.movie.handler');
		$result = $handler->getAll('cinema', $cinema_ids, $includeOthers);

		$response = new JsonResponse( $result, 200 );
//		$response->setPublic();
//		$response->setMaxAge( 15 * 60 );
//		$response->setSharedMaxAge( 45 * 60 );

		return $response;
	}
	
	
	/**
	 * Add cinema to favorites.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/cinemas")
	 * @Method({"POST"})
	 */
	public function cinemasPostAction(Request $request) {
		$this->_authorize($request);

		$cinema_id  = $request->get('cinema_id');
		
		// Check required fields.
		if ( empty( $cinema_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Cinema.
		$em         = $this->getDoctrine()->getManager();
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
		$cinema     = $repo->find( $cinema_id );
		if ( !$cinema )
			return new JsonResponse( array( 'error' => 'Invalid cinema ID.' ), 400 );
		
		// Add relationship.
		if ( !$this->user->getCinemasFav()->contains( $cinema ) ) {
			$this->user->getCinemasFav()->add( $cinema );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
	
	
	/**
	 * Remove cinema from favorites.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/cinemas")
	 * @Method({"DELETE"})
	 */
	public function cinemasDeleteAction(Request $request) {
		$this->_authorize($request);

		$cinema_id  = $request->get('cinema_id');
		$em         = $this->getDoctrine()->getManager();

		// Check required fields.
		if ( empty( $cinema_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Cinema.
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
		$cinema     = $repo->find( $cinema_id );
		
		if ( !$cinema )
			return new JsonResponse( array( 'error' => 'Invalid cinema ID.' ), 400 );
		
		// Remove relationship.
		if ( $this->user->getCinemasFav()->contains( $cinema ) ) {
			$this->user->getCinemasFav()->removeElement( $cinema );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
	
	
	/**
	 * Set user's preferred cinema.
	 * 
	 * Requires $_REQUEST['cinema_id'] int Cinema ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * 
	 * @Route("/preferredCinema")
	 * @Method({"POST"})
	 */
	public function preferredCinema(Request $request) {
		$this->_authorize($request);

		$cinema_id  = $request->request->get( 'cinema_id' );
		$em         = $this->getDoctrine()->getManager();
		
		// Check required fields.
		if ( empty( $cinema_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Cinema.
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
		$cinema     = $repo->find( $cinema_id );
		
		if ( !$cinema )
			return new JsonResponse( array( 'error' => 'Invalid cinema ID.' ), 400 );
		
		// Set relationship.
		$this->user->setCinema( $cinema );
		$em->flush();
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
  
 
  /**
   * @Route("/loginData")
   */
  public function loginDataAction(Request $request) {
		$this->_authorize($request);

    $res = $this->get( 'ss.front.utils' )->get_user_login_data( $this->user );

    return new JsonResponse( array( 'data' => $res ) );
  }
  
  
  /**
   * @Route("/purchaseHistory")
   * @todo Calculate eTag using MAX(id) WHERE user = $user.
   */
  public function purchaseHistoryAction(Request $request) {
    $this->_authorize($request);

		$since  = $request->query->get('since');
		$before = $request->query->get('before');
		$count  = $request->query->get('count');
		if (!$count || $count > 50) {
			$count = NULL;
		}

		/** @var \SocialSnack\RestBundle\Handler\UserHandler $user_handler */
		$user_handler = $this->get('rest.user.handler');
    $output = $user_handler->getTransactionHistory(
				$this->user->getId(),
				$count,
				$since,
				$before
		);

    if (!is_array($output)) {
      /** @todo Replace with RestException */
      throw new \Exception();
    }

		if ($this->get('rest.context.handler')->getApiVersion() >= 2) {
			$output = ['data' => $output];
		}

    return new JsonResponse($output);
  }
  
  
  /**
   * @Route("/ie/details")
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws RestException
   */
	public function ieDetailsAction(Request $request) {
		$this->_authorize($request);

		$user = $this->user;
		$iecode = $user->getIecode();

		if (!$iecode) {
			throw new RestException('no-ie-code', 'User account doesn\'t have a IE code linked.');
		}

		/** @var \SocialSnack\RestBundle\Handler\IEHandler $ie_handler */
		$ie_handler = $this->get('rest.ie.handler');
		$info = $ie_handler->getIEInfo($iecode);
		$transactions = $ie_handler->getIETransactions($iecode);

		if (isset($transactions->ERROR)) {
			$history = array(array(
					'action' => 'ERROR',
					'cinema' => $transactions->ERROR->ERRORDESC->__toString(),
					'date'   => date('d/m/Y h:i:s') . ' ' . substr(date('a'), 0, 1) . '. m.',
					'points' => 0,
			));
		} else {
			$history = array_map( function($a) {
				return array(
						'action' => $a->ORIGEN->__toString(),
						'cinema' => $a->COMPLEJO->__toString(),
						'date'   => $a->FECHA->__toString(),
						'points' => (float)(($a->TIPOMOV->__toString() === 'REDENCIONES' ? '-' : '') . $a->PUNTOS->__toString())
				);
			}, $transactions);
		}

		$output = array(
				'iecode'  => $info->NUMIEC->__toString(),
				'name'    => trim($info->PNOMBREIEC . ' ' . $info->SNOMBREIEC . ' ' . $info->APATERNOIEC . ' ' . $info->AMATERNOIEC),
				'points'  => (float)$info->SALDO->__toString(),
				'history' => $history,
		);

		$response = new JsonResponse($output);
		return $response;
	}
  

  /**
   * @Route("/registerGCM")
   * @Method({"POST"})
   */
  public function registerGCMAction(Request $request) {
    $this->_authorize($request);

    $uuid  = $request->request->get('uuid');
    $gcmid = $request->request->get('gcmid');
    $type  = $request->request->get('type');
    
    if (!isset($uuid, $gcmid, $type)) {
      return new JsonResponse(array('error' => 'missing-fields'), 400);
    }
    
    $gcm = $this->container->get('ss.rest.utils')->setGCM($uuid, $gcmid, $type, $this->user);
    
    if (!$gcm) {
      return new JsonResponse(array('error' => 'unexpected-error'), 400);
    }
    
    return new JsonResponse(array('success' => TRUE), 200);
  }
  
}