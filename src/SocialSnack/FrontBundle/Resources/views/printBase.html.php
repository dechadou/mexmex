<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Cinemex</title>
		<link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/style.css' ); ?>" />
		<link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/print.css' ); ?>" media="print" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,400italic,700|Roboto+Condensed:700' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="<?php echo $view['assets']->getUrl( 'assets/img/favicon.png' ); ?>" />
	</head>
	<body>
    <?php if ( isset( $content ) ) { echo $content; } ?>
  </body>
  <script>
    if ( '#print' == document.location.hash ) {
      window.print();
      window.onfocus = function() { window.close(); };
    }
  </script>
</html>