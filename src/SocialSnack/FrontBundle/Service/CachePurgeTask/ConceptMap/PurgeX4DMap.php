<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

class PurgeX4DMap extends AbstractConceptMap {

  protected  function getRouteName() {
    return 'x4d_cinemas';
  }


  protected function getConceptName() {
    return 'x4d';
  }

}