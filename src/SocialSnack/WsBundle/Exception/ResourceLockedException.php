<?php

namespace SocialSnack\WsBundle\Exception;

/**
 * Class ResourceLockedException
 * @package SocialSnack\WsBundle\Exception
 * @author Guido Kritz
 */
class ResourceLockedException extends \Exception {

} 