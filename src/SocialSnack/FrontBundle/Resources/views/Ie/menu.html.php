<?php
$menu = array(
    array(
        'label' => 'Nuestro programa',
        'route' => array( 'ie_program' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'Niveles',
        'route' => array( 'ie_levels' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'Beneficios',
        'route' => array( 'ie_benefits' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'Puntos',
        'route' => array( 'ie_points' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'Promociones',
        'route' => array( 'ie_promos' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'Preguntas frecuentes',
        'route' => array( 'ie_faq' ),
        'class' => 'icon-rarr',
    ),
    array(
        'label' => 'PAYBACK',
        'route' => array( 'ie_payback' ),
        'class' => 'icon-payback',
        'default_class' => '',
        'active_class'  => 'btn-payback'
    )
);
?>

<ul class="panel-menu">
	<?php
	foreach ( $menu as $item ) {
		if ( !is_array( $item['route'] ) ) {
			$item['route'] = array( $item['route'] );
		}
		$current = ( in_array( $route, $item['route'] ) );
    $default_class = isset($item['default_class']) ? $item['default_class'] : 'btn-light';
    $active_class  = isset($item['active_class'])  ? $item['active_class']  : 'btn-default';
	?>
	<li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? $active_class : $default_class; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
	<?php
	}
	?>
</ul>
