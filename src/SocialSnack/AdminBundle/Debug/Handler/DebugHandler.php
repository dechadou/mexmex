<?php

namespace SocialSnack\AdminBundle\Debug\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\WsBundle\Observer\WsDebugger;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DebugHandler
 * @package SocialSnack\AdminBundle\Debug\Handler
 * @author Guido Kritz
 */
class DebugHandler {

  protected $container;

  protected $doctrine;

  protected $ws;

  protected $ws_observer;

  public function __construct(ContainerInterface $container, Registry $doctrine, WsRequest $ws, WsDebugger $ws_observer) {
    $this->container   = $container;
    $this->doctrine    = $doctrine;
    $this->ws          = $ws;
    $this->ws_observer = $ws_observer;
  }


  /**
   * @param $session_id
   * @return \SocialSnack\AdminBundle\Debug\Result\ResultAbstract
   */
  public function debugSession($session_id) {
    $handler = new SessionHandler(
        $this->doctrine,
        $this->ws,
        $this->ws_observer,
        $this->container->get('rest.buy.handler'),
        $this->container->get('session')
    );

    return $handler->debug($session_id);
  }

}