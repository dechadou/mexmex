<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div class="row xs-clear-on-3 sm-clear-on-<?php echo ($movies_cols - 1); ?> md-clear-on-<?php echo ($movies_cols); ?> clearfix movies-grid" data-movies-per-row="<?php echo $movies_cols; ?>" data-movies-per-page="<?php echo $movies_per_page; ?>">
	<?php /* @var $movie \SocialSnack\WsBundle\Entity\Movie */ ?>
	<?php foreach ( $movies as $movie ) { ?>
  <?php
  $attributes = array();
  $data = array();
  if ( $movie->getPremiere() )                             { $attributes[] = 'new'; }
  if ( 'DIGITAL 3D' == $movie->getData( 'FORMATOVIDEO' ) ) { $attributes[] = '3d'; }
  foreach ( $attributes as $attr ) {
    $data[] = 'data-' . $attr;
  }
  $link_to_single = isset($link_to_single) ? $link_to_single : TRUE;
  ?>
	<?php $movie_url = $view['fronthelper']->get_movie_link($movie); ?>
	<div class="col col-xs-1-3 col-sm-1-<?php echo ($movies_cols - 1); ?> col-md-1-<?php echo $movies_cols; ?> movies-grid-item <?php if (!$link_to_single) echo 'hover1'; ?>" <?php echo implode( ' ', $data ); ?>>
		<a href="<?php echo $movie_url; ?>" class="movies-grid-cover">
			<img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '154x230'); ?>" alt="<?php echo $movie->getName(); ?>" />
		</a>
		<a href="<?php echo $movie_url; ?>"  class="movies-grid-title"><?php echo $movie->getName(); ?></a>

		<?php
    echo $this->render('SocialSnackFrontBundle:Partials:movieRibbons.html.php', array(
        'movie' => $movie
    ));
    ?>

		<?php if ($link_to_single) { ?>
    <a href="<?php echo $movie_url . (isset($link_filter) ? $link_filter : ''); ?>" class="movie-poster-bt buy-tickets-bt">Consulta<br/>horarios</a>
    <?php } ?>

<?php
$trailer = $movie->getData('TRAILER');

if (gettype($trailer) === 'string' AND !empty($trailer)) :
?>
		<a href="#" data-youtube-id="<?php echo FrontHelper::get_youtube_id_from_url($trailer); ?>" class="movie-poster-bt watch-trailer-bt">Ver<br/>tráiler</a>
<?php endif; ?>
	</div>
	<?php } ?>
</div>

<?php if ( isset( $_movies ) && ( sizeof( $_movies ) > $movies_per_page ) ) { ?>
<div class="end-body-bt">
  <a href="#" class="btn btn-default btn-icon icon icon-reload" rel="grid-load-more">Ver más</a>
</div>
<?php } ?>

<?php if ( isset( $_movies ) ) { ?>
<script>
  var movies = <?php echo json_encode( $_movies ); ?>;
</script>
<?php } ?>
