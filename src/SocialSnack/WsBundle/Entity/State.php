<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * State
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="st_active_idx", columns={"active"})
 * })
 * @ORM\Entity(repositoryClass="SocialSnack\WsBundle\Entity\StateRepository")
 */
class State {

  /**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var integer
	 * 
	 * @ORM\Column(type="integer")
	 */
	private $legacy_id;
	
	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", length=255)
	 */
	private $name = '';
	
	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", length=255)
	 */
	private $legacy_name;
  
  /**
   * @var string
   *
   * @ORM\Column(type="string", length=100)
   */
  private $timezone;
  
  /**
   * @var integer 
   *
   * @ORM\Column(name="position", type="integer")
   */
  private $order;
	
	/**
	 * @ORM\OneToMany(targetEntity="StateArea", mappedBy="state")
	 */	 
	private $areas;
	
	/**
	 * @var boolean
	 * 
	 * @ORM\Column(type="boolean")
	 */
	private $active = 1;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return State
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name ? $this->name : $this->legacy_name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return State
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->timezone = 'America/Mexico_City';
        $this->areas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->order = 0;
    }
    
    /**
     * Add areas
     *
     * @param \SocialSnack\WsBundle\Entity\StateArea $areas
     * @return State
     */
    public function addArea(\SocialSnack\WsBundle\Entity\StateArea $areas)
    {
        $this->areas[] = $areas;
    
        return $this;
    }

    /**
     * Remove areas
     *
     * @param \SocialSnack\WsBundle\Entity\StateArea $areas
     */
    public function removeArea(\SocialSnack\WsBundle\Entity\StateArea $areas)
    {
        $this->areas->removeElement($areas);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAreas()
    {
        return $this->areas;
    }

    /**
     * Set legacy_name
     *
     * @param string $legacyName
     * @return State
     */
    public function setLegacyName($legacyName)
    {
        $this->legacy_name = $legacyName;
    
        return $this;
    }

    /**
     * Get legacy_name
     *
     * @return string 
     */
    public function getLegacyName()
    {
        return $this->legacy_name;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return State
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }
		
		public function toArray( $include_areas = FALSE, $include_cinemas = FALSE )
    {
        $output = array(
            'id'   => $this->id,
            'name' => $this->name,
        );
				
				if ( $include_areas ) {
					$output['areas'] = array();
					foreach ( $this->getAreas() as $area ) {
						$output['areas'][] = $area->toArray( $include_cinemas );
					}
				}
				
				return $output;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return State
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return State
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    
        return $this;
    }

    /**
     * Get timezone
     *
     * @return string 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
}