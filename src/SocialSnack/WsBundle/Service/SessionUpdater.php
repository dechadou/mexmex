<?php

namespace SocialSnack\WsBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Monolog\Logger;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\CinemaUpdateLog;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Exception\SessionsUpdate\MainMovieNotFoundException;
use SocialSnack\WsBundle\Exception\SessionsUpdate\MovieNotFoundException;
use SocialSnack\WsBundle\Exception\SessionsUpdate\MovieVersionNotFoundException;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SessionUpdater extends WorkerAbstract {

  protected $container;

  protected $doctrine;

  protected $ws;

  protected $ticketUpdater;

  protected $logger;

  protected $updatedSessions;

  public function __construct(ContainerInterface $container, Registry $doctrine, WsRequest $ws, TicketUpdater $ticketUpdater, Logger $logger) {
    $this->container     = $container;
    $this->doctrine      = $doctrine;
    $this->ws            = $ws;
    $this->ticketUpdater = $ticketUpdater;
    $this->logger        = $logger;
  }


  /**
   * @param array $data
   * @return bool
   */
  public function execute($data) {
    if (!$this->updateTickets($data)) {
      return FALSE;
    }

    if (!$this->updateSessions($data)) {
      return FALSE;
    }
    return TRUE;
  }


  /**
   * Execute the TicketUpdater service.
   *
   * @param array $data
   * @return bool
   */
  protected function updateTickets($data) {
    return $this->ticketUpdater->execute($data);
  }


  /**
   * Sessions update process.
   *
   * @param array $data
   * @return bool
   * @throws \Exception
   */
  protected function updateSessions($data) {
    $this->updatedSessions = [];
    $cinema                = $this->getCinema($data);
    $sessionData           = $this->getSessionsFromWS($cinema);
    $sessionCount          = $sessionData->count();
    $activeSessionIds      = $this->getActiveSessionIds($cinema);
    $sessions              = $this->getSessions($cinema, $this->extractIdsFromWsResponse($sessionData, 'SESSIONID'));
    $movies                = $this->getMovies($this->extractIdsFromWsResponse($sessionData, 'MOVIE'));
    $tickets               = $this->getTickets($cinema);

    if (!sizeof($sessionData) || (sizeof($sessionData) === 1 && !$sessionData[0]->SESSIONID->__toString())) {
      $sessionData = [];
      $sessionCount = 0;
      $this->alertNoSessions($cinema);
    }

    foreach ($sessionData as $s) {
      $session = $this->getSession($s->SESSIONID->__toString(), $sessions, TRUE, TRUE);
      if (!$session) {
        continue;
      }

      $this->setSessionData($session, $s, $cinema);
      $this->setSessionTypes($session, $s, $tickets);

      $movie = $this->getMovieForSession($movies, $session, $s);
      if (!$movie) {
        continue;
      }

      $this->setSessionMovie($session, $movie);

      $this->updatedSessions[] = $session;
      $this->doctrine->getManager()->persist($session);
    }

    $this->doctrine->getManager()->flush();

    // Disable old sessions not present in this update.
    $updatedSessionIds = array_map(function($s) { return $s->getId(); }, $this->updatedSessions);
    $oldSessionIds     = array_values(array_diff($activeSessionIds, $updatedSessionIds));
    $this->disableOldSessions($oldSessionIds);

    $this->logCinemaUpdate($cinema, isset($data['source']) ? $data['source'] : '', $sessionCount, 'SUCCESS');

    // Detach the sessions after the task is completed in order to ensure the entities are no longer cached and are
    // refreshed from the DB in case this task is ran again in the same tasks batch.
    $this->detach($sessions);

    return TRUE;
  }


  /**
   * @param array $data
   * @return Cinema
   * @throws \Exception
   */
  protected function getCinema(array $data) {
    $cinema_repo  = $this->doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );

    if (isset($data['cinema_id'])) {
      $cinema = $cinema_repo->find($data['cinema_id']);
    } elseif (isset($data['cinema_legacy_id'])) {
      $cinema = $cinema_repo->findOneBy(array('legacy_id' => $data['cinema_legacy_id']));
    } else {
      throw new \InvalidArgumentException('No key defined.');
    }

    if (!$cinema) {
      throw new \Exception('Cinema not found.');
    }

    return $cinema;
  }


  /**
   * Fetch sessions from DB.
   *
   * @param Cinema $cinema
   * @param array  $sessionLegacyIds
   * @return mixed
   */
  protected function getSessions(Cinema $cinema, $sessionLegacyIds) {
    $qb = $this->doctrine->getManager()->createQueryBuilder();
    $qb
        ->select('s')
        ->from('SocialSnackWsBundle:Session', 's')
        ->andWhere($qb->expr()->in('s.legacy_id', $sessionLegacyIds))
        ->andWhere('s.cinema = :cinema')
        ->setParameter('cinema', $cinema)
    ;

    return $qb->getQuery()->execute();
  }


  protected function getActiveSessionIds(Cinema $cinema) {
    $qb = $this->doctrine->getManager()->createQueryBuilder();
    $qb
        ->select('s.id')
        ->from('SocialSnackWsBundle:Session', 's')
        ->andWhere('s.active = 1')
        ->andWhere('s.cinema = :cinema')
        ->setParameter('cinema', $cinema)
    ;

    $res = $qb->getQuery()->execute();
    if (!$res) {
      return [];
    }

    return array_map(function($a) { return $a['id']; }, $res);
  }


  /**
   * Fetch movies from DB.
   *
   * @param array $legacyMovieIds
   * @return mixed
   */
  protected function getMovies($legacyMovieIds) {
    $qb = $this->doctrine->getManager()->createQueryBuilder();
    $qb
        ->select('m')
        ->from('SocialSnackWsBundle:Movie', 'm')
        ->andWhere( $qb->expr()->in('m.legacy_id_bis', $legacyMovieIds))
    ;

    return $qb->getQuery()->execute();
  }


  /**
   * @param Cinema $cinema
   * @return \SimpleXMLElement
   */
  protected function getSessionsFromWS(Cinema $cinema) {
    $req = $this->ws;
    $req->init('Consulta');
    $res = $req->request( array(
        'TipoConsulta' => Consulta::TIPOCONSULTA_SESIONES,
        'CodigoCine'   => $cinema->getLegacyId(),
    ) );

    return $res;
  }


  /**
   * @param integer $legacyId
   * @param array   $sessions
   * @param boolean $createIfNotFound
   * @param boolean $removeFromResult
   * @return Session|null
   */
  protected function getSession($legacyId, &$sessions, $createIfNotFound, $removeFromResult) {
    $session = NULL;

    $session = current(array_filter(
        $sessions,
        function($a) use($legacyId) { return $a->getLegacyId() == $legacyId; }
    ));

    if (!$session && $createIfNotFound) {
      $session = $this->createNewSession();
    }
    return $session;
  }


  /**
   * @param string $priceCode
   * @param array  $tickets
   * @return array
   */
  protected function getTicketsByPriceCode($priceCode, array $tickets) {
    $result = array_filter($tickets, function($ticket) use ($priceCode) {
      return $ticket['priceCode'] == $priceCode;
    });

    return $result;
  }


  /**
   * @param array             $movies
   * @param Session           $session
   * @param \SimpleXMLElement $sessionData
   * @return null|Movie
   * @throws \Exception
   */
  protected function getMovieForSession(array &$movies, Session $session, \SimpleXMLElement $sessionData) {
    $movieLegacyIdBis = $sessionData->MOVIE->__toString();

    try {
      $movie = $this->getMovie($movieLegacyIdBis, $movies, $session->getTypes());
    } catch (\Exception $e) {

      if ($e instanceof MovieVersionNotFoundException) {
        $new_version = $this->createNewVersion($e->getMainMovie(), $session);
        $this->doctrine->getManager()->persist($new_version);
        $movies[] = $new_version;
        $movie    = $new_version;

      } elseif ($e instanceof MovieNotFoundException) {
        $this->alertMovieNotFound($movieLegacyIdBis);
        return NULL;

      } else {
        throw $e;
      }
    }

    return $movie;
  }


  /**
   * @param string $legacyIdBis
   * @param array  $movies
   * @param array  $attributes
   * @return null|Movie
   * @throws MainMovieNotFoundException
   * @throws MovieNotFoundException
   * @throws MovieVersionNotFoundException
   */
  protected function getMovie($legacyIdBis, $movies, $attributes) {
    $filtered = array_filter($movies, function($a) use($legacyIdBis, $attributes) {
      return ($a->getLegacyIdBis() == $legacyIdBis);
    });

    if (!sizeof($filtered)) {
      throw new MovieNotFoundException(sprintf('Movie ID %s for sessions update not found.', $legacyIdBis));
    }

    $main = array_reduce($filtered, function($carry, $item) {
      if ($item->getLegacyId()) {
        return $item;
      } else {
        return $carry;
      }
    });

    if (!$main) {
      throw new MainMovieNotFoundException(sprintf('Main movie ID %s for sessions update not found.', $legacyIdBis));
    }

    $main_attr = $this->getAttributesHash($main, $attributes);

    $version = NULL;
    foreach ($filtered as $item) {
      $v_attr = $this->getAttributesHash($item);
      if ($main_attr === $v_attr) {
        $version = $item;
        break;
      }
    }

    if (!$version) {
      throw new MovieVersionNotFoundException(
          $main,
          sprintf('Version of movie ID %s for sessions update not found.', $legacyIdBis)
      );
    }

    return $version;
  }


  /**
   * @return Session
   */
  protected function createNewSession() {
    return new Session();
  }


  /**
   * @param Session           $session
   * @param \SimpleXMLElement $sessionData
   * @param Cinema            $cinema
   */
  protected function setSessionData(Session $session, \SimpleXMLElement $sessionData, Cinema $cinema) {
    $date = new \DateTime(preg_replace( '/([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2})\:([0-9]{2})\:([0-9]{2}) ([ap])\.\s?m\./', '$3-$2-$1 $4:$5:$6 $7m.', $sessionData->SESSIONDATE ));

    // Date needs to be checked manually. Otherwise Doctrine will think there's some change to be persisted even if the date remains the same.
    if ($date != $session->getDateTime()) {
      $session->setDateTime($date);
    }

    $session->setLegacyId((int)$sessionData->SESSIONID);
    $session->setCinema($cinema);
    $session->setLegacyCinemaId((int)$sessionData->CINE);
    $session->setPlatinum((boolean)$sessionData->PLATINO);
    $session->setInfo(json_encode($sessionData));
    $session->setActive(TRUE);
    $session->setUpdating(FALSE);
    $session->setTz($cinema->getState()->getTimezone());
  }


  protected function disableOldSessions(array $sessionIds) {
    if (!sizeof($sessionIds)) {
      return;
    }

    $conn = $this->doctrine->getConnection();

    $update_query = "UPDATE Session SET active = 0 WHERE id IN (?)";

    $stmt = $conn->executeQuery($update_query,
        [$sessionIds],
        array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
    );

    return $stmt->execute();
  }


  /**
   * @param Cinema $cinema
   * @param string $source
   * @param string $result
   * @param string $status
   */
  protected function logCinemaUpdate(Cinema $cinema, $source, $result, $status) {
    $em = $this->doctrine->getManager();

    $qb = $em->createQueryBuilder();
    $qb
        ->delete('SocialSnackWsBundle:CinemaUpdateLog', 'l')
        ->where('l.cinema = :cinema')
        ->andWhere('l.source = :source')
        ->setParameter('cinema', $cinema)
        ->setParameter('source', $source)
    ;

    $q = $qb->getQuery();
    $q->execute();

    $entry = new CinemaUpdateLog();
    $entry
        ->setCinema($cinema)
        ->setSource($source)
        ->setResult($result)
        ->setStatus($status)
        ->setDateRan(new \DateTime('now', new \DateTimeZone('America/Mexico_City')))
    ;
    $em->persist($entry);
    $em->flush();
  }


  /**
   * @param \SimpleXMLElement $response
   * @param string            $key
   * @return array
   */
  protected function extractIdsFromWsResponse(\SimpleXMLElement $response, $key) {
    $ids = [];
    foreach ($response as $s) {
      $ids[] = $s->{$key}->__toString();
    }
    $ids = array_unique($ids);

    return $ids;
  }


  /**
   * Normalize attributes list into a string which can be used to compare against other attributes collections.
   *
   * @param Movie $movie
   * @param array $extra_attributes
   * @return string
   */
  protected function getAttributesHash(Movie $movie, $extra_attributes = NULL) {
    $main_attr = array_keys(array_filter((array)$movie->getAttr(), function($a) {
      return !!$a;
    }));

    // Hack to deal with the cx/extreme ambiguity issue.
    // @todo Remove this hack and SessionUpdaterTest::testUpdateSessions_CX() once the main issue is solved.
    if (in_array('cx', $main_attr) && !in_array('extreme', $main_attr)) {
      $main_attr[] = 'extreme';
    }

    if ($extra_attributes) {
      $main_attr = array_merge($extra_attributes, $main_attr);
      $main_attr = array_unique($main_attr);
    }

    $main_attr = array_filter($main_attr); // Remove empty elements.
    sort($main_attr);
    $main_attr = implode(',', $main_attr);
    return $main_attr;
  }


  /**
   * Create a new movie version from $mainMovie matching the attributes of $session.
   *
   * @param Movie   $mainMovie
   * @param Session $session
   * @return Movie
   */
  protected function createNewVersion(Movie $mainMovie, Session $session) {
    $this->logger->info(sprintf(
        'New version created from movie %d for session %d (%s) with types %s',
        $mainMovie->getLegacyId(),
        $session->getLegacyId(),
        $session->getId() ? 'update' : 'new',
        implode(' / ', $session->getTypes())
    ));
    $newVersion = clone $mainMovie;
    $newVersion->setLegacyId(NULL);
    foreach ($session->getTypes() as $type) {
      $newVersion->setAttr($type, TRUE);
    }
    return $newVersion;
  }


  /**
   * @param string $movieLegacyIdBis
   */
  protected function alertMovieNotFound($movieLegacyIdBis) {
    $this->logger->info(sprintf('Sessions found for %s but movie not imported yet.', $movieLegacyIdBis));
  }


  /**
   * @param Cinema $cinema
   */
  protected function alertNoSessions(Cinema $cinema) {
    $this->logger->info(sprintf('No active sessions in cinema %s (%d).', $cinema->getName(), $cinema->getId()));
  }


  /**
   * Detach entities from the entity manager.
   */
  protected function detach() {
    $em = $this->doctrine->getManager();

    foreach (func_get_args() as $arg) {
      foreach ($arg as $item) {
        $em->detach($item);
      }
    }
  }


  /**
   * @return array
   */
  public function getUpdatedSessions() {
    return $this->updatedSessions;
  }


  /**
   * @param Session           $session
   * @param \SimpleXMLElement $session_data
   */
  protected function setSessionTypes(Session $session, \SimpleXMLElement $session_data, array $tickets) {
    // Clear all previously set types.
    $session->setTypes(array());

    $sessionTickets = $this->getTicketsByPriceCode($session_data->PRICECODE->__toString(), $tickets);
    // Set flag for CineMá sessions...
    foreach ($sessionTickets as $ticket) {
      if (preg_match('/\bCINEM[AÁá]\b/iu', $ticket['name'])) {
        $session->setCinemom();
      }
    }


    if (WsHelper::session_is_jumbo($session)) {
      $session->setType('jumbo');
    }
    if (WsHelper::session_is_macro($session)) {
      $session->setType('macro');
    }


    if (isset($session_data->SCREENTYPE)) {
      switch ($session_data->SCREENTYPE) {
        case 'X4D':
          $session->setType('v4d');
          break;
        case 'Premium':
          $session->setPremium(TRUE);
          $session->setType('premium');
          break;
        case 'Platino':
          $session->setType('platinum');
          break;
        case 'CX':
          $session->setExtreme(TRUE);
          $session->setType('extreme');
          $session->setType('cx');
          break;
        case 'Convencional':
          break;
      }
    }
  }


  /**
   * @param Cinema $cinema
   * @return array
   */
  protected function getTickets(Cinema $cinema) {
    $qb = $this->doctrine->getManager()->createQueryBuilder();
    $qb
        ->select('t.legacy_id priceCode, t.legacy_description name')
        ->from('SocialSnackWsBundle:Ticket', 't')
        ->andWhere('t.cinema = :cinema')
        ->setParameter('cinema', $cinema)
    ;

      $q = $qb->getQuery();

      return $q->getResult();
  }


  /**
   * @param Session $session
   * @param Movie   $movie
   */
  protected function setSessionMovie(Session $session, Movie $movie) {
    $session->setMovie($movie);
    $session->setGroupId((string)$movie->getGroupId());
  }

}