<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\FrontBundle\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $new = !($builder->getData() instanceof Article && $builder->getData()->getId());

    $builder
        ->add('title', 'text', array(
            'label' => 'Título',
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('featured', 'checkbox', array(
            'label' => 'Destacado',
            'required' => false,
        ))
        ->add('content', 'textarea', array(
            'label' => 'Contenido',
            'attr' => array(
                'class' => 'form-control ckeditor'),
            'required' => false
        ))
        ->add('status', 'choice', array(
            'label' => 'Estado',
            'choices' => array(
                \SocialSnack\FrontBundle\Entity\Article::STATUS_PUBLISH => 'Publicado',
                \SocialSnack\FrontBundle\Entity\Article::STATUS_DRAFT   => 'Borrador',
            ),
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('tags', 'text', array(
            'label' => 'Tags',
            'required' => FALSE,
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('cover', 'file', array(
            'label' => 'Imagen',
            'mapped' => FALSE,
            'required' => $new ? TRUE : FALSE,
            'image_preview_path' => 'cover',
            'image_preview_size' => '733x286',
        ))
        ->add('thumb', 'file', array(
            'label' => 'Thumb',
            'mapped' => FALSE,
            'required' => $new ? TRUE : FALSE,
            'image_preview_path' => 'thumb',
            'image_preview_size' => '236x222',
        ))
    ;
  }

  public function getName() {
    return '';
  }
}