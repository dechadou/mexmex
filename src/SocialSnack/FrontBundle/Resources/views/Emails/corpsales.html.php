<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start( 'body' );
?>
<!-- BODY -->
<table class="body-wrap" bgcolor="#ebebeb" style="width: 100%; padding-bottom: 30px;">
  <tr>
    <td></td>
    <td class="container" align="" bgcolor="#ebebeb" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">
      <!-- content -->
      <div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
        <table>
          <tr>
            <td>
              <p>
                <strong>Nombre completo:</strong> <?php echo $data['name']; ?><br/>
                <strong>Email:</strong> <?php echo $data['email']; ?><br/>
                <strong>Compañia:</strong> <?php echo $data['company']; ?><br/>
                <strong>Teléfono:</strong> <?php echo $data['phone']; ?><br/>
                <strong>Tipo de evento:</strong> <?php echo $data['event_type']; ?><br/>

<?php if (!in_array($data['event_type'], ['tickets', 'etickets'])) : ?>
                <strong>Cine:</strong> <?php echo $data['cinema']->getName(); ?><br/>
                <strong>Fecha del evento:</strong> <?php echo $data['event_date']->format('d F Y'); ?><br/>
                <strong>Horario del evento:</strong> <?php echo $data['event_time']->format('G:i'); ?><br/>
                <strong>Capacidad de la sala requerida:</strong> <?php echo $data['capacity']; ?><br/>
                <strong>Requiere de productos de dulcería:</strong> <?php echo $data['need_candy']; ?><br/>
<?php endif; ?>
              </p>

              <p>
                <strong>Comentarios:</strong><br/>
                <?php echo str_replace("\n", '<br/>', $data['comments']); ?>
              </p>
            </td>
          </tr>
        </table>
      </div><!-- /content -->
    </td>
    <td></td>
  </tr>
</table><!-- /BODY -->

<?php $view['slots']->stop(); ?>
