<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\EntityRepository;

use SocialSnack\WsBundle\Service\Helper as WsHelper;

class MovieComingRepository extends EntityRepository {


  protected function queryAllFuture($offset = NULL, $limit = NULL) {
    $qb = $this->createQueryBuilder('m');
    $qb
        ->andWhere('m.active = 1')
        ->andWhere('m.release_date > :today')
        ->setParameter('today', new \DateTime('now'))
        ->orderBy('m.release_date', 'ASC')
    ;

    if ($offset) {
      $qb->setFirstResult($offset);
    }
    if ($limit) {
      $qb->setMaxResults($limit);
    }

    return $qb;
  }


  public function findAllFuture($offset = NULL, $limit = NULL) {
    $qb = $this->queryAllFuture($offset, $limit);
    $qb->addSelect('m');

    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60, 'MovieComingRepository:findAllFuture:' . ($offset ?: 0) . '|' . ($limit ?: 0) );
    return $query->getResult();
  }


  public function countAllFuture() {
    $qb = $this->queryAllFuture();
    $qb->addSelect('COUNT(m.id)');

    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60, 'MovieComingRepository:countAllFuture' );
    $res = $query->getResult();
    if (!$res) {
      throw new \Exception('Error counting MovieComing');
    }

    return $res[0][1];
  }


  public function findNextFriday() {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('m')->from('SocialSnackWsBundle:MovieComing', 'm');
    $qb->andWhere('m.active = 1');
    $qb->andWhere('m.next_week = 1')
        ->andWhere('m.release_date > :today')
        ->setParameter('today', new \DateTime('now'));
    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60, 'MovieComingRepository:findNextFriday' );
    return $query->getResult();
  }
  
  
  public function findFurther() {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('m')->from('SocialSnackWsBundle:MovieComing', 'm');
    $qb->andWhere('m.active = 1');
    $qb->andWhere('m.next_week = 0')
        ->andWhere('m.release_date > :today')
        ->setParameter('today', new \DateTime('now'))
        ->orderBy('m.release_date', 'ASC');
    $query = $qb->getQuery();
    $query->useResultCache( TRUE, 60 * 60, 'MovieComingRepository:findFurther' );
    return $query->getResult();
  }
  
  
  public function findAllForAdmin($active=1) {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('m')->from('SocialSnackWsBundle:MovieComing', 'm');
    $qb->andWhere('m.active = '.$active);
    $qb->orderBy('m.id', 'DESC');
    $query = $qb->getQuery();
    return $query->getResult();
  }
  
}
