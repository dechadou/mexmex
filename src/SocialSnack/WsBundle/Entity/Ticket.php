<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="ticket_session_idx", columns={"legacy_id","cinema_id"})
 * })
 * @ORM\Entity
 */
class Ticket
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="legacy_id", type="string", length=10)
     */
    private $legacy_id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="legacy_description", type="string", length=255)
     */
    private $legacy_description;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer")
     */
    private $type_id;
		
    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

		/**
		 * @ORM\ManyToOne(targetEntity="Cinema", inversedBy="tickets")
		 * @ORM\JoinColumn(name="cinema_id", referencedColumnName="id")
		 */
		private $cinema;
		
		/**
		 * @var integer
		 *
		 * @ORM\Column(name="legacy_cinema_id", type="integer")
		 */
		private $legacy_cinema_id;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="is_new", type="boolean")
     */
    private $is_new;
		
		private $decoded_info = null;

    
    public function __construct() {
      $this->is_new = TRUE;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return Ticket
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Ticket
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set legacy_description
     *
     * @param string $legacyDescription
     * @return Ticket
     */
    public function setLegacyDescription($legacyDescription)
    {
        $this->legacy_description = $legacyDescription;
    
        return $this;
    }

    /**
     * Get legacy_description
     *
     * @return string 
     */
    public function getLegacyDescription()
    {
        return $this->legacy_description;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Ticket
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Ticket
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinema
     * @return Ticket
     */
    public function setCinema(\SocialSnack\WsBundle\Entity\Cinema $cinema = null)
    {
        $this->cinema = $cinema;
    
        return $this;
    }

    /**
     * Get cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema 
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Set type_id
     *
     * @param integer $typeId
     * @return Ticket
     */
    public function setTypeId($typeId)
    {
        $this->type_id = $typeId;
    
        return $this;
    }

    /**
     * Get type_id
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->type_id;
    }
		
		
		/**
		 * Get data from the WS movie object.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->getInfo() );
			}
			return $this->decoded_info->{ $key };
		}

    /**
     * Set legacy_cinema_id
     *
     * @param integer $legacyCinemaId
     * @return Ticket
     */
    public function setLegacyCinemaId($legacyCinemaId)
    {
        $this->legacy_cinema_id = $legacyCinemaId;
    
        return $this;
    }

    /**
     * Get legacy_cinema_id
     *
     * @return integer 
     */
    public function getLegacyCinemaId()
    {
        return $this->legacy_cinema_id;
    }

    /**
     * Set is_new
     *
     * @param boolean $isNew
     * @return Ticket
     */
    public function setIsNew($isNew)
    {
        $this->is_new = $isNew;
    
        return $this;
    }

    /**
     * Get is_new
     *
     * @return boolean 
     */
    public function getIsNew()
    {
        return $this->is_new;
    }
}