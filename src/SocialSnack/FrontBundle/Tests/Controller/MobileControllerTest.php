<?php

namespace SocialSnack\FrontBundle\Tests\Controller;


use SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData;
use Symfony\Component\BrowserKit\Cookie;

class MobileControllerTest extends WebTestCase {

  /**
   * @group functional
   */
  public function testIndexAction_AskLocation() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/ubicacion', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testIndexAction_RememberCinema() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $cookie = new Cookie('m_cinema_id', 1);
    $client->getCookieJar()->set($cookie);
    $client->request(
        'GET',
        '/',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/cartelera/1', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testIndexAction_iOS() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appios', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testIndexAction_Android() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appandroid', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testIndexAction_Windows() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 930) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appwindows', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testAppsAction_iOS() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/apps/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543 Safari/419.3',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appios', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testAppsAction_Android() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/apps/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appandroid', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testAppsAction_Windows() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/apps/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 930) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/appwindows', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   */
  public function testAppsAction_NoMatch() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/apps/',
        array(),
        array(),
        array(
            'HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'),
            'HTTP_USER_AGENT' => 'Foobar',
        )
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testMobileIpadAction() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/appios',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testMobileAndroidAction() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/appandroid',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testMobileWindowsAction() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/appwindows',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testLocationAction() {
    $this->loadFixtures([]);

    $client = static::createClient();
    $client->request(
        'GET',
        '/ubicacion',
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testCinemaSingleAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);

    $cinema_id = LoadSessionData::$cinemas[0]->getId();
    $client = static::createClient();
    $client->request(
        'GET',
        '/cine/' . $cinema_id,
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testMovieSingleAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);

    $movie_id = LoadSessionData::$movies[0]->getId();
    $client = static::createClient();
    $client->request(
        'GET',
        '/pelicula/' . $movie_id,
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testMovieSingleAction_WithCinema() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData'
    ]);

    $movie_id  = LoadSessionData::$movies[0]->getId();
    $cinema_id = LoadSessionData::$cinemas[0]->getId();
    $client = static::createClient();
    $client->request(
        'GET',
        '/pelicula/' . $movie_id . '/' . $cinema_id,
        array(),
        array(),
        array('HTTP_HOST' => 'm.' . $client->getContainer()->getParameter('domain'))
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

}