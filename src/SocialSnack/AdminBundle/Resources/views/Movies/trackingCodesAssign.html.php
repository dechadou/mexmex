<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Asignar Código de Tracking - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Asignar Código de Tracking <strong><?php echo $code->getName(); ?></strong>
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>Group ID</th>
                <th>Título</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($groups as $group) { ?>
              <tr>
                <td><?php echo $group['group_id']; ?></td>
                <td><?php echo $group['name']; ?></td>
                <td>
                  <?php if (!in_array($group['group_id'], $assigned)) { ?>
                    <a href="<?php echo $view['router']->generate('admin_movies_trackingCodes_assign', ['id' => $code->getId(), 'group_id' => $group['group_id']]); ?>">Asignar</a>
                  <?php } else { ?>
                    <a href="<?php echo $view['router']->generate('admin_movies_trackingCodes_assign', ['id' => $code->getId(), 'group_id' => $group['group_id'], 'action' => 'remove']); ?>">Desasignar</a>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<?php
$view['slots']->stop();