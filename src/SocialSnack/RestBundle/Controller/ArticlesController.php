<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticlesController extends BaseController {

  /**
   * @Route("/", name="rest_articles_default")
   * @Method({"GET"})
   */
  public function defaultAction() {
    $handler = $result = $this->get('rest.article.handler');
    $context = $result = $this->get('rest.context.handler');
    $request = $this->getRequest();
    $limit   = (int)$request->get('limit');
    $offset  = (int)$request->get('offset');

    $result = $handler->getAll($limit,$offset);

    if ($context->getApiVersion() >= 2) {
      $result = ['articles' => $result];
    }

    $response = new JsonResponse($result, 200);
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(45 * 60);
    
    return $response;
  }
  
  
  /**
   * @Route("/search", name="rest_articles_search")
   * @Method({"GET"})
   */
  public function searchAction(Request $request) {
    $handler = $result = $this->get('rest.article.handler');
    $term    = $request->get('q');

    $result = $handler->search($term);

    $response = new JsonResponse(array('articles' => $result));
    $response->setPublic();
    $response->setMaxAge(30 * 60);
    $response->setSharedMaxAge(60 * 60);
    
    return $response;
  }
    
  
  /**
   * Return detailed info for article(s).
   * 
   * @Route("/{id}", name="rest_articles_single", requirements={"id"="[0-9]+"})
   * @Method({"GET"})
   */
  public function detailsAction($id, Request $request) {
    $handler      = $result = $this->get('rest.article.handler');
    $context      = $result = $this->get('rest.context.handler');
    $article_repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Article');
    $articles     = $article_repo->findBy(array('id' => $id));

    if (!$articles) {
      return new JsonResponse(array(), 404);
    }

    $result = $handler->generateArray($articles);

    if ($context->getApiVersion() >= 2) {
      $result = $result[0];
    }

    $response = new JsonResponse($result, 200);
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(60 * 60);

    return $response;
  }

}