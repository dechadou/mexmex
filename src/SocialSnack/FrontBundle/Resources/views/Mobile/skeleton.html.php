<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Component\HttpKernel\Controller\ControllerReference;

$description = 'Cadena de cines en México. Presenta información relevante sobre cartelera, salas, películas en exhibición, concursos y mucho más.';
?>
<!DOCTYPE html>
<html>
	<head>
    <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
		<meta charset="UTF-8" />
		<title><?php $view['slots']->output( 'title', 'Cinemex' ); ?></title>
		<link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/mobile.css' ); ?>" />
		<link href='//fonts.googleapis.com/css?family=Roboto:300,400,500,400italic,700|Roboto+Condensed:700' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="<?php echo $view['assets']->getUrl( '/favicon.ico' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta name="description" content="<?php echo $description; ?>" />
    <meta property="fb:app_id" content="<?php echo $view['fronthelper']->get_fb_app_id(); ?>" />
    <meta property="og:image" content="https:<?php echo $view['assets']->getUrl('assets/img/share-img.jpg'); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="<?php echo $description; ?>" />
    <?php $view['slots']->output('head_meta_tags'); ?>
    <script>
      var is_mobile = true;
    </script>
	</head>
	<body>

    <?php $view['slots']->output('base'); ?>

    <div id="site-footer">
      <div id="footer-bottom">
        DERECHOS RESERVADOS &copy; CADENA MEXICANA DE EXHIBICIÓN S.A. DE C.V. 2013 | <a href="<?php echo $view['router']->generate('mobile_contact'); ?>">CONTACTO</a>
        <br/>
        SITIO DESARROLLADO POR <a href="http://socialsnack.com" target="_blank">SOCIALSNACK.COM</a>
      </div>

      <div id="switch-to-mobile">
        <a href="<?php echo $view['router']->generate('home'); ?>?no_redirect=1">Ver versión completa</a>
      </div>
    </div>

    <div id="goto-app-banner">
      <span class="goto-app-ico"></span>
      ¡Usa gratis la app de Cinemex!
      <span class="goto-app-btn btn">Usar</span>
      <span class="goto-app-close">Cerrar</span>
    </div>

    <?php
    echo $view['actions']->render(
        $view['router']->generate('esi_js_stuff', array()),
        array('strategy' => 'esi')
    );
    ?>

    <iframe id="sessionbridge" src="<?php echo $view['router']->generate('post_message_iframe'); ?>" style="display:none;"></iframe>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>!window.jQuery && document.write('<script src="<?php echo $view['assets']->getUrl( 'assets/vendor/jquery/jquery-1.10.2.min.js' ); ?>"><\/script>')</script>

      <?php foreach ($view['assetic']->javascripts(
        array(
//            '@SocialSnackFrontBundle/Resources/public/vendor/artberri/jquery.html5storage.min.js',
            '@SocialSnackFrontBundle/Resources/public/vendor/estebanav/geoPosition.js',
            '@SocialSnackFrontBundle/Resources/public/vendor/carhartl/jquery.cookie.js',
//            '@SocialSnackFrontBundle/Resources/public/vendor/adamcoulombe/jquery.customSelect.js',
            '@SocialSnackFrontBundle/Resources/public/vendor/rgrove/lazyload-min.js',
//            '@SocialSnackFrontBundle/Resources/public/vendor/suprb/jquery.nested.js',
//            '@SocialSnackFrontBundle/Resources/public/vendor/magnific-popup/jquery.magnific-popup.min.js',
            '@SocialSnackFrontBundle/Resources/public/vendor/jschannel/jschannel.js',
        ),
        array(),
        array('output' => 'js/compiled/mobile-vendor.js')
    ) as $url): ?>
        <script src="<?php echo $view->escape($url) ?>"></script>
    <?php endforeach; ?>

    <?php $view['slots']->output( 'js_after_vendor' ); ?>

    <?php foreach ($view['assetic']->javascripts(
        array(
            '@SocialSnackFrontBundle/Resources/public/js/modules/storagebridge.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/utils.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/popups.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/cookies.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/validators.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/slider.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/location.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/checkout.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/map.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/facebook.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/tour.js',
            '@SocialSnackFrontBundle/Resources/public/js/modules/apisdk.js',
            '@SocialSnackFrontBundle/Resources/public/js/mobile-app.js',
        ),
        array(),
        array('output' => 'js/compiled/mobile-app.js')
    ) as $url): ?>
        <script src="<?php echo $view->escape($url) ?>"></script>
    <?php endforeach; ?>

    <?php $view['slots']->output( 'js_after' ); ?>

    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '<?php echo $view['fronthelper']->get_analytics_id(); ?>', 'auto');
      <?php $view['slots']->output('ga_extras'); ?>
      ga('require', 'displayfeatures');
      ga('send', 'pageview');
      <?php $view['slots']->output('ga_extras_after'); ?>
    </script>

    <script type="text/javascript">
      var _sf_async_config = { uid: 52715, domain: 'cinemex.com', useCanonical: true };
      (function() {
        function loadChartbeat() {
          window._sf_endpt = (new Date()).getTime();
          var e = document.createElement('script');
          e.setAttribute('language', 'javascript');
          e.setAttribute('type', 'text/javascript');
          e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
          document.body.appendChild(e);
        };
        var oldonload = window.onload;
        window.onload = (typeof window.onload != 'function') ?
          loadChartbeat : function() { oldonload(); loadChartbeat(); };
      })();
    </script>
	</body>
</html>
