<?php

namespace SocialSnack\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SocialSnack\AdminBundle\Form\Type\AdminType;
use SocialSnack\AdminBundle\Entity\Admin;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_ADMINS';
  }


  /**
   * @Route("/admin_list", name="social_snack_admin_admin_list")
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function indexAction() {
    $this->check_roles();
    
    $repo = $this->get('doctrine')->getRepository('SocialSnackAdminBundle:Admin');
    $admins = $repo->findAll();

    return $this->render('SocialSnackAdminBundle:Admin:list.html.php', array('admins' => $admins));
  }


  /**
   * @Route("/admin/{admin_id}", name="social_snack_admin_admin_edit")
   * @Route("/admin_add", name="social_snack_admin_admin_add", defaults={"admin_id"=null})
   * @param null    $admin_id
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function editAction($admin_id = NULL, Request $request) {
    $this->check_roles();

    $session = $request->getSession();
    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackAdminBundle:Admin');

    if (is_null($admin_id)) {
      $admin = new Admin();
    } else {
      $admin = $repo->find($admin_id);
    }

    $form = $this->createForm(new AdminType(), $admin);

    $form->add('save', 'submit', ['label' => 'Guardar']);

    $form->handleRequest($request);

    if ($form->isValid()) {
      $plainPassword = $form->get('password')->getData();

      if ($plainPassword) {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($admin);
        $admin->setPassword($encoder->encodePassword($plainPassword, $admin->getSalt()));
      }

      $em->persist($admin);
      $em->flush();

      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');

      return $this->redirect($this->generateUrl('social_snack_admin_admin_edit', array('admin_id' => $admin->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Admin:edit.html.php', array(
        'admin' => $admin,
        'form' => $form->createView(),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    ));
  }

}
