<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;
use Symfony\Component\Routing\Router;

abstract class AbstractConceptMap implements UrlTaskInterface, DoctrineTaskInterface {

  protected $router;

  protected $doctrine;

  public function __construct(Router $router, Registry $doctrine) {
    $this->router   = $router;
    $this->doctrine = $doctrine;
  }


  abstract protected function getRouteName();


  abstract protected function getConceptName();


  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate($this->getRouteName()),
            'env'  => 'front'
        ]
    ];
  }


  public function getDoctrineResources() {
    $attribute = $this->getAttribute();

    return [
        [
            'ids' => [
                'SocialSnack\WsBundle\Entity\CinemaRepository::findLegacyIdsByAttribute:' . $attribute->getId()
            ],
            'env' => 'front'
        ]
    ];
  }


  protected function getAttribute() {
    return $this->doctrine
        ->getRepository('SocialSnackFrontBundle:Attribute')
        ->findOneByValue($this->getConceptName());
  }

}