<div id="login-popup-fb">
  <p class="popup-title">Ingresa ahora con tu cuenta de Facebook</p>

  <a href="#" class="btn btn-icon btn-facebook icon icon-facebook" id="fb-login-bt">Conectarme</a>
</div>

<div id="login-popup-std">
  <p class="popup-title">Ingresa con tu correo electrónico</p>

  <form id="login-form" action="<?php echo $view['router']->generate( 'login_check' ); ?>" class="std-form compact-form no-cols" method="post">
    <?php
    //              echo $view['actions']->render(
    //                  new ControllerReference('SocialSnackFrontBundle:Partials:esiCsrf', array(
    //                      'token' => 'authenticate',
    //                      'name'  => '_csrf_token'
    //                  )),
    //                  array('strategy' => 'esi')
    //              );
    ?>
    <div class="form-row">
      <input type="text" name="_username" id="login-user" placeholder="Email" data-validators="not_empty email" data-flyvalidate="1" />
    </div>
    <div class="form-row">
      <input type="password" name="_password" id="login-pass" placeholder="Contraseña" data-validators="not_empty" data-flyvalidate="1" />
    </div>
    <a href="<?php echo $view['router']->generate('user_forgot_pass'); ?>" class="small forgot-password">¿Olvidaste tu contraseña?</a>
    <button type="submit" class="btn btn-icon btn-default icon icon-user">Ingresar</button>
  </form><!-- #login-form -->

  <a href="<?php echo $view['router']->generate( 'user_register' ); ?>" id="login-popup-reg" class="popup-bottom icon pad-right icon-rarr icon-dark">
    ¿No tienes una cuenta?<br/>
    Regístrate aquí
  </a>
</div>
