<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', '#CineOpinión - Cinemex');
$view['slots']->set('body_class', 'sponsored-cineopinion');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-cineopinion">
  <iframe src="//promocinemex.com/landing/cineopinion" onload="scroll(0, 0);" width="100%" height="3000px"></iframe>
</div>
<?php $view['slots']->stop(); ?>
