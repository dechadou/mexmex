<?php

namespace SocialSnack\FrontBundle\Tests\Service\CachePurger;

class CachePurgerUrlAbstractTest extends \PHPUnit_Framework_TestCase {

  protected $buzz;

  public function setUp() {
    $this->buzz = $this->getMockBuilder('Buzz\Browser')->disableOriginalConstructor()->getMock();
  }

  protected function getServiceMock(array $methods = []) {
    return $this->getMockBuilder('SocialSnack\FrontBundle\Service\CachePurger\CachePurgerUrlAbstract')
        ->setConstructorArgs([$this->buzz])
        ->setMethods($methods)
        ->getMockForAbstractClass();
  }

  public function testHandle() {
    $task = $this->getMock('SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface');
    $task->expects($this->once())->method('getUrlResources')->willReturn([
        ['path' => '/esi/foo', 'env' => 'front'],
        ['path' => '/esi/bar', 'env' => 'front'],
        ['path' => '/foo.bar', 'env' => 'static'],
    ]);

    $service = $this->getServiceMock(['purge']);
    $service->setEnv('front', ['hostname' => 'foobar.front', 'servers' => ['10.1.1.1', '10.1.1.2']]);
    $service->setEnv('static', ['hostname' => 'foobar.static', 'servers' => ['10.1.1.3', '10.1.1.4']]);

    $service->expects($this->exactly(6))->method('purge')->will($this->returnCallback(function() {
      return implode(',', func_get_args());
    }));

    $output = $service->handle($task);
    $expected = [
        '/esi/foo,foobar.front,10.1.1.1',
        '/esi/foo,foobar.front,10.1.1.2',
        '/esi/bar,foobar.front,10.1.1.1',
        '/esi/bar,foobar.front,10.1.1.2',
        '/foo.bar,foobar.static,10.1.1.3',
        '/foo.bar,foobar.static,10.1.1.4',
    ];
    $this->assertEquals($expected, $output);
  }

}