<?php

namespace SocialSnack\RestBundle\Tests\Controller;
use SocialSnack\RestBundle\Tests\Fixtures\LoadPurchasesData;

/**
 * Class PurchasesControllerTest
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class PurchasesControllerTest extends WebTestCase {

  public function testIndexAction() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadPurchasesData',
    ));

    // This is used to test pagination.
    $count = 3;
    $since = LoadPurchasesData::$transactions[4]->getId();

    $route = sprintf('/rest/v2/purchases/?count=%d&before=%d', $count, $since);
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());

    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertObjectHasAttribute('items', $data);
    $this->assertObjectHasAttribute('pagination', $data);

    // Test pagination.
    $this->assertEquals($count, sizeof($data->items));
    $this->assertObjectHasAttribute('next', $data->pagination);
    $this->assertObjectHasAttribute('prev', $data->pagination);
  }

  public function testIndexActionUsersOnly() {
    $client = static::createClient();
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadPurchasesData',
    ));

    $route = '/rest/v2/purchases/?users_only=1';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());

    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertEquals(5, sizeof($data->items));

    $non_null = 1;
    foreach ($data->items as $item) {
      $non_null &= !is_null($item);
    }
    $this->assertEquals(1, $non_null);
  }
} 