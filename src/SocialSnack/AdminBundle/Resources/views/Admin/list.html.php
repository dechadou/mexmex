<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de administradores - Cinemex' );
$view['slots']->start('body')

?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Listado de Administradores
            </header>
            <div class="panel-body">
                  <div class="adv-table">
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Roles</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
<?php
foreach ($admins as $admin) { 
?>
                        <tr class="gradeX">
                            <td><?php echo $admin->getId(); ?></td>                       
                            <td><?php echo $admin->getUser(); ?></td>
                            <td><?php echo implode(', ', array_map(function($a){return $a->getName();}, $admin->getRoles())); ?></td> 
                            <td><a href="admin/<?php echo $admin->getId(); ?>" class="edit">Editar</a></td>
                        </tr>
<?php
}
?>
                      </table>
                  </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#example').dataTable( {
            "aaSorting": [[ 4, "desc" ]]
        } );
    } );
</script>
<?php $view['slots']->stop(); ?>