<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Routing\Router;

class PurgeAllStatesCinemasMap implements UrlTaskInterface, DoctrineTaskInterface {

  protected $router;

  protected $doctrine;

  public function __construct(Router $router, Registry $doctrine) {
    $this->router   = $router;
    $this->doctrine = $doctrine;
  }


  public function getUrlResources() {
    $resources = [];

    $states = $this->getActiveStates();

    foreach ($states as $state) {
      $resources[] = [
          'path' => $this->router->generate('cinemas_archive', ['id' => $state['id']]),
          'env'  => 'front'
      ];
    }

    return $resources;
  }


  public function getDoctrineResources() {
    $ids = [];

    $states = $this->getActiveStates();

    foreach ($states as $state) {
      $ids[] = sprintf('SocialSnack\WsBundle\Entity\StateRepository::findWithAreasAndCinemas:%d_', $state['id']);
    }

    return [
        [
            'ids' => $ids,
            'env' => 'front'
        ],
    ];
  }


  protected function getActiveStates() {
    $qb = $this->doctrine->getRepository('SocialSnackWsBundle:State')->createQueryBuilder('q');
    $qb
        ->select('q.id')
        ->where('q.active = 1')
    ;

    $q = $qb->getQuery();
    return $q->getResult();
  }

}