<?php $view->extend('SocialSnackFrontBundle:About:base.html.php'); ?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Conciencia Cinemex</h1>

  <div class="quote lead">
    <p>En Cinemex siempre hemos estado ligados a la responsabilidad social y estamos comprometidos a ayudar y regresar algo a la sociedad de lo mucho que nos aporta. En  conjunto con Fundación Grupo México, promovemos actividades de soporte y ayuda a diferentes Organizaciones para impulsar el desarrollo de un mejor país.</p>
  </div>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-grupomexico.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Conciencia Cinemex</h2>
      <p>Impulsamos la labor social de diversas Organizaciones difundiendo su trabajo a través de nuestras pantallas para dar a conocer a quienes trabajan por los que más lo necesitan.</p>

      <h2>Apoyo al cine Mexicano</h2>
      <p>En Cinemex, en conjunto con Fundación Grupo México, estamos comprometidos con el talento mexicano y buscamos apoyar las artes y cultura de nuestro país,  por lo cual promovemos e impulsamos la difusión de películas mexicanas a través de alfombras rojas en nuestros complejos.</p>
      <p>Al igual, se realizan premieres a beneficio, donde los ingresos de la asistencia de las salas se destinan a  Organizaciones sin fines de lucro.</p>

      <h2>Campañas de marketing social</h2>
      <p>Recaudamos fondos y colaboramos para proyectos específicos de Organizaciones sin fines de lucro, con el objetivo de dar a conocer la labor social de la Organización y al mismo tiempo brindar información a quienes requieran del apoyo.</p>
      <p>Hemos apoyado a Organizaciones como:</p>

      <ul>
        <li>CMR</li>
        <li>Angelitos de Cristal</li>
        <li>Fundación entre tus Manos</li>
        <li>Dr. Sonrisas</li>
        <li>Teletón</li>
        <li>Instituto Nuevo Amanecer</li>
        <li>Huracán Alex</li>
        <li>Michou y Mau</li>
      </ul>
    </div>
  </div>

  <hr>

  <div class="grid-row">
    <div class="grid-col-4">
      <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-logo-carreracinemex.png'); ?>">
    </div>

    <div class="grid-col-8">
      <h2>Carrera Cinemex</h2>

      <p>Desde hace 5 años, la Magia del Cine sale a las calles con la Carrera Cinemex, reuniendo a más  de 5,000 personas para recorrer las calles de la ciudad de México apoyando una causa social.</p>
      <p>Cada año buscamos una causa y encaminamos la carrera para apoyarla; el hecho de realizar un evento a través de una actividad que promueva la sana convivencia en familia es lo que nos impulsa a realizar la carrera Cinemex.</p>
      <p>Te invitamos a que conozcas más de nuestra labor social en conjunto con Fundación Grupo México <a href="http://www.fundaciongrupomexico.com" target="_blank">aquí</a>.</p>

      <div class="grid-row">
        <div class="grid-col-6">
          <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-carrera-cmx1.jpg'); ?>">
        </div>

        <div class="grid-col-6">
          <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-carrera-cmx2.jpg'); ?>">
        </div>
      </div>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
