<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:User:base.html.php');
$view['slots']->set( 'title', 'Mis datos - Cinemex' );
$view['slots']->start( 'user_body' );
?>
<div class="content-box">
	<h1 class="content-box-title">Mis datos</h1>

	<?php
	echo $view->render(
			'SocialSnackFrontBundle::flashMsgs.html.php',
			array( 'flash_msgs' => $flash_msgs )
	);
	?>

  <?php $view['form']->setTheme( $form, array( 'SocialSnackFrontBundle:Form' ) ); ?>
  <?php echo $view['form']->start($form, array( 'action' => $view['router']->generate( 'user_profile_edit' ), 'attr' => array( 'novalidate' => 'novalidate'))) ?>
		<div class="clearfix">
      <?php echo $view['form']->errors($form) ?>

      <?php echo $view['form']->row($form['first_name']) ?>
      <?php echo $view['form']->row($form['last_name']) ?>
      <?php echo $view['form']->row($form['email']) ?>
      <?php echo $view['form']->row($form['plainPassword']) ?>
      <?php echo $view['form']->row($form['info_phone_mobile']) ?>
      <?php echo $view['form']->row($form['info_phone_carrier']) ?>
      <?php echo $view['form']->row($form['file']) ?>

      <div class="form-row btn-row">
        <button type="submit" class="btn btn-default btn-icon icon icon-rarr">Guardar</button>
      </div>
		</div>
	<!--</form>-->
  <?php echo $view['form']->end($form) ?>
</div>
<?php
$view['slots']->stop();
?>
