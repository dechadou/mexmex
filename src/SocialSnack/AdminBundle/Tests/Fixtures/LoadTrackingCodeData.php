<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use SocialSnack\RestBundle\Entity\TrackingCode;

class LoadTrackingCodeData implements FixtureInterface {

  static $codes;

  public function load(ObjectManager $manager) {
    self::$codes = [];

    $faker = Faker\Factory::create();

    $code = new TrackingCode();
    $code
        ->setName($faker->name())
        ->setDescription($faker->text())
        ->setActive(TRUE)
        ->setCode('<script>alert("foobar");</script>')
    ;
    $manager->persist($code);
    self::$codes[] = $code;

    $manager->flush();
  }

}