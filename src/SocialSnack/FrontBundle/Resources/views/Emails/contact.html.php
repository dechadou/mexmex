<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Emails:base.html.php');
$view['slots']->start( 'body' );
?>
<!-- BODY -->
<table class="body-wrap" bgcolor="#ebebeb" style="width: 100%; padding-bottom: 30px;">
	<tr>
		<td></td>
		<td class="container" align="" bgcolor="#ebebeb" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">
			
			<!-- content -->
			<div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
				<table>
					<tr>
						<td>
							
							<p>
								Hemos recibido un nuevo contacto en el Buzón Cinemex.
							</p>
							<p>
                <?php /* @var $contact \SocialSnack\FrontBundle\Entity\Contact */ ?>
                <strong>Nombre:</strong> <?php echo $contact->getName(); ?><br/>
                <strong>Email:</strong> <?php echo $contact->getEmail(); ?><br/>
                <?php if ( $value = $contact->getPhone() ) { ?>
                <strong>Teléfono:</strong> <?php echo $value; ?><br/>
                <?php } ?>
                <?php if ( $value = $contact->getIe() ) { ?>
                <strong>Invitado N°:</strong> <?php echo $value; ?><br/>
                <?php } ?>
                <?php if ( $cinema ) { ?>
                <strong>Cine:</strong> <?php echo $cinema->getName(); ?><br/>
                <?php } ?>
                <?php if ( $value = $contact->getService() ) { ?>
                <strong>Servicio:</strong> <?php echo $value; ?><br/>
                <?php } ?>
                <?php if ( $value = $contact->getType() ) { ?>
                <strong>Tipo de mensaje:</strong> <?php echo $value; ?><br/>
                <?php } ?>
							</p>
              <p>
                <strong>Mensaje:</strong><br/>
                <?php echo str_replace( "\n", '<br/>', $contact->getMsg() ); ?>
              </p>
              
						</td>
					</tr>
				</table>
			</div><!-- /content -->
      
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->
<?php
$view['slots']->stop();
?>