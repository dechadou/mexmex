<?php

namespace SocialSnack\FrontBundle\Service;

class TwigString implements \Twig_LoaderInterface {

  protected $twig;

  public function __construct(\Twig_Environment $twig) {
    $this->twig = $twig;
  }

  public function getSource($string) {
    return $string;
  }

  public function getCacheKey($string) {
    return 'TwigString:' . md5($string);
  }

  public function isFresh($name, $time) {
    return !($this->getCurrentTimestamp() - $time > 60 * 60);
  }

  protected function getCurrentTimestamp() {
    return time();
  }

  public function render($string, $variables) {
    $this->twig->setLoader($this);
    return $this->twig->render($string, $variables);
  }

}