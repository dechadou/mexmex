<?php
$view->extend('SocialSnackFrontBundle:3d:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Experiencia 3D</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-03.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-04.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-03.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-01.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-02.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/3d-04.jpg'); ?>"></a>
        </div>
      </div>
    </div>

    <div class="grid-col-5">
      <p class="lead">¡Vive, disfruta y siente La Magia del Cine en tercera dimensión!</p>
      <p>En Cinemex®, nos caracterizamos por contar con los mejores sistemas 3D, como el increíble Real D.</p>
      <p>¿Te Imaginas ver el brazo de Hulk en tamaño real? o ¿Ver el vuelo de Iron Man lo más cerca de ti? Disfruta los mejores estrenos en 3D en cualquiera de nuestras salas: Premium, Platino, X4D y CinemeXtermo.</p>
      <p>Conoce la experiencia Digital 3D y descubre una nueva forma de ver el cine.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
