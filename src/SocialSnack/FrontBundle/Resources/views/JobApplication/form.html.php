<?php
$view->extend('SocialSnackFrontBundle:JobApplication:base.html.php');
$view['slots']->set('title', 'Solicitud de Empleo - Cinemex');
$view['slots']->start('jobs_body');
?>

<style>
.jobs-form {
    margin-bottom:40px;
}

.jobs-disclaimer {
    margin-left:40px;
    margin-top:40px;
}

.jobs-disclaimer h1 {
    font-size:14px;
}

.table th,
.table td {
    border-top:1px solid #d8d8d8;
    padding:7px 5px;
}

.table th {
    font-weight:200;
}

.table thead th {
    border-bottom:2px solid #d8d8d8;
    border-top:0;
    font-weight:200;
    vertical-align:middle;
}

.area-selector-target {
    display:none;
}

.content-box .form-row.half {
  height:53px;
}

.addr-tooltip {
  position: absolute;
  border: 1px solid #b7b7b7;
  padding: 10px 12px;
  font-size: 12px;
  color: #7E7E7E;
  top: 118%;
  box-shadow: 2px 2px 0 #eee;
  left: 0;
  width: 100%;
  background: #F7F7F7;

  opacity:0;
  visibility:hidden;
  -webkit-transition:all .3s;
  transition:all .3s;
  -webkit-transform:translate(0, -50%);
  -ms-transform:translate(0, -50%);
  transform:translate(0, -50%);
}

.addr-tooltip:before {
  content: '';
  width: 11px;
  height: 11px;
  position: absolute;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
  top: -5px;
  background: #f7f7f7;
  box-shadow: -1px -1px 0 #b7b7b7;
}

.addr-tooltip.active {
  opacity:1;
  visibility:visible;
  -webkit-transform:translate(0, 0);
  -ms-transform:translate(0, 0);
  transform:translate(0, 0);
}
</style>

<h1 class="content-box-title">Forma de Solicitud de Empleo</h1>

<form class="std-form sym-form" method="post" data-basicvalidate="1" novalidate>
    <div class="form-errors form-row wide">
      <?php echo $view['form']->errors($form); ?>
    </div>

    <div class="subform clearfix">
        <label class="subform-label">Información general</label>

        <?php echo $view['form']->row($form['general_name']); ?>
        <?php echo $view['form']->row($form['general_address']); ?>
        <?php echo $view['form']->row($form['general_neighborhood']); ?>
        <?php echo $view['form']->row($form['general_postal_code']); ?>
        <?php echo $view['form']->row($form['general_state']); ?>
        <?php echo $view['form']->row($form['general_city']); ?>
        <?php echo $view['form']->row($form['general_phone_mobile']); ?>
        <?php echo $view['form']->row($form['general_phone_home']); ?>
        <?php echo $view['form']->row($form['general_email']); ?>
        <?php echo $view['form']->row($form['general_birthdate']); ?>
        <?php echo $view['form']->row($form['general_marital_status']); ?>
        <?php echo $view['form']->row($form['general_have_childrens']); ?>
        <?php echo $view['form']->row($form['general_number_imss']); ?>
        <?php echo $view['form']->row($form['general_number_rfc']); ?>
        <?php echo $view['form']->row($form['general_number_curp']); ?>
    </div>


    <div class="subform clearfix">
        <label class="subform-label">Área de interés</label>

        <?php echo $view['form']->row($form['area_available_trips']); ?>
        <?php echo $view['form']->row($form['area_available_move']); ?>

        <div class="area-selector clearfix">
            <?php echo $view['form']->row($form['area_type']); ?>
        </div>

        <div class="area-selector-target area-selector-cinemas clearfix">
            <div class="state-area-selection">
              <?php echo $view['form']->row($form['area_state']); ?>
              <?php echo $view['form']->row($form['cinema']); ?>
            </div>

            <?php echo $view['form']->row($form['area_name']); ?>
        </div>

        <div class="area-selector-target area-selector-cafes clearfix">
            <?php echo $view['form']->row($form['area_cafe']); ?>
        </div>
    </div>

    <div class="subform clearfix">
        <label class="subform-label">Escolaridad</label>

        <?php echo $view['form']->row($form['school_grade']); ?>
        <?php echo $view['form']->row($form['school_actual']); ?>
    </div>

    <div class="subform clearfix">
        <label class="subform-label">Trabajos anteriores</label>

        <div class="form-row wide">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre de la empresa</th>
                        <th>Nombre del puesto</th>
                        <th>Periodo</th>
                        <th>Sueldo</th>
                        <th>Motivo de salida</th>
                    </tr>
                </thead>

                <tbody>
    <?php for ($i = 1; $i <= $jobs_loop; ++$i) : ?>
                    <tr>
                        <td><?php echo $view['form']->widget($form["experience_{$i}company"]); ?></td>
                        <td><?php echo $view['form']->widget($form["experience_{$i}position"]); ?></td>
                        <td><?php echo $view['form']->widget($form["experience_{$i}period"]); ?></td>
                        <td><?php echo $view['form']->widget($form["experience_{$i}salary"]); ?></td>
                        <td><?php echo $view['form']->widget($form["experience_{$i}exit_reason"]); ?></td>
                    </tr>
    <?php endfor; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="subform clearfix">
        <label class="subform-label">Datos familiares</label>

        <div class="form-row wide">
            <table class="table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Nombre</th>
                        <th>Edad</th>
                        <th>Ocupacion</th>
                        <th>Empresa</th>
                    </tr>
                </thead>

                <tbody>
    <?php
    foreach ($family as $field => $label) : ?>
                    <tr>
                        <th><?php echo $label; ?></th>
                        <td><?php echo $view['form']->widget($form["family_{$field}_name"]); ?></td>
                        <td><?php echo $view['form']->widget($form["family_{$field}_age"]); ?></td>
                        <td><?php echo $view['form']->widget($form["family_{$field}_job"]); ?></td>
                        <td><?php echo $view['form']->widget($form["family_{$field}_company"]); ?></td>
                    </tr>
    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="family-cinema-selector clearfix">
        <?php echo $view['form']->row($form['family_cinemex']); ?>
        </div>

        <div class="family-cinema-selector-target clearfix">
            <?php echo $view['form']->row($form['family_state']); ?>
            <?php echo $view['form']->row($form['family_cinema']); ?>
        </div>
    </div>

    <div class="subform clearfix">
        <div class="form-row wide">
            <div id="checkout-captcha-row" style="display:block;">
                <label for="checkout-captcha-input">Ingresa el código de verificación:</label>

                <input type="text" name="captcha" id="checkout-captcha-input" value="" class="std-field" placeholder="Ingresa el código..."/>

                <figure id="checkout-captcha-fig">
                    <img src="<?php echo $view['router']->generate('captcha'); ?>">
                    <a class="captcha-reload icon icon-reload no-txt" title="Cargar otro código">Cargar otro código</a>
                </figure>
            </div>
        </div>
    </div>

    <section class="disclaimer jobs-disclaimer">
      <h1>AVISO DE PRIVACIDAD SIMPLIFICADO SOLICITANTES Y CANDIDATOS</h1>

      <p>Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>
      <p>Los datos personales, financieros, patrimoniales y sensibles, (en adelante y conjuntamente los “Datos Personales”), que les solicitamos son tratados con la finalidad primaria de llevar a cabo el proceso de Reclutamiento y Selección. Podrá conocer el Aviso de Privacidad Integro en la siguiente página de internet: www.cinemex.com/privacidadcandidatos</p>
      <p>Al enviar esta solicitud manifiesto mi consentimiento para el tratamiento y transferencia de mis Datos Personales, de conformidad con lo previsto en el presente Aviso de Privacidad.</p>

      <p>Fecha de última actualización: 5/Enero/2015</p>
    </section>

    <div class="subform clearfix">
        <?php echo $view['form']->row($form['submit']); ?>
    </div>
</form>

<?php
// echo $view['form']->form($form, array(
//     'attr' => array('class' => 'jobs-form')
// ));
?>


<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('js_after'); ?>
<script>
(function($){
  $('#family_cinemex')
  .on('change', function() {
    var $inputs = $('.family-cinema-selector-target :input');

    if ($(this).val() === 'si') {
      $('.family-cinema-selector-target').show();
      $inputs.removeProp('disabled');
    } else {
      $('.family-cinema-selector-target').hide();
      $inputs.prop('disabled', true);
    }
  })
  .change();

  $('#general_have_childrens').bind('change', function() {
    var $inputs = $('[name^=family_child]');
    if ('no' === $(this).val()) {
      $inputs.closest('tr').hide();
      $inputs.prop('disabled', true);
    } else {
      $inputs.closest('tr').show();
      $inputs.removeProp('disabled');
    }
  });

  $('#area_type').bind('change', function(){
    var field   = $(this),
        options = $('.area-selector-target'),
        target  = null;

    if (field.val() === 'cafes') {
        target = '.area-selector-cafes';
    } else if (field.val() === 'cinemas') {
        target = '.area-selector-cinemas';

        if (user && user.preferred_cinema) {
          $('#area_state')
            .val(user.preferred_cinema.area.id)
            .change();

          $('#cinema')
            .val(user.preferred_cinema.id)
            .change();
        }
    }

    options
        .filter(target)
        .fadeIn()
        .find(':input')
        .removeProp('disabled')
        .attr('required', 'required');

    options
        .not(target)
        .hide()
        .find(':input')
        .prop('disabled', true)
        .removeAttr('required');
  });

  $('body').on('change', '#cinema', function(ev){
    var field     = $(this),
        container = field.closest('.form-row'),
        selected  = field.val(),
        tooltip   = $('.addr-tooltip'),
        cinema;

    if (tooltip.size() === 0) {
      tooltip = $('<div class="addr-tooltip" />');

      container.append(tooltip);
    }

    tooltip.removeClass('active');

    var x = states.length;

    while (x--) {
      var state = states[x],
          y = state.areas.length;

      while (y--) {
        var area = state.areas[y],
            z = area.cinemas.length;

        while (z--) {
          var item = area.cinemas[z];

          if (item.id == parseInt(selected)) {
            cinema = item;

            break;
          }
        }
      }
    }

    if (cinema !== undefined) {
      tooltip
        .html(cinema.info.address)
        .addClass('active');
    }
  });

  show_lightbox(['<?php echo $view['assets']->getUrl('assets/img/jobs-index.jpg'); ?>']);
})(jQuery);
</script>
</script>
<?php $view['slots']->stop(); ?>
