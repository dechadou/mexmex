<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Http\CsvResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ReportController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_REPORTS';
  }
  
  
  public function transactionsListAction() {
    $this->check_roles();
    
    return $this->render(
        'SocialSnackAdminBundle:Report:transactionsList.html.php'
    );
  }

  
  public function transactionsListAjaxAction(Request $request) {
    $this->check_roles();

    // Get query params.
    $start   = $request->query->get('start');
    $length  = $request->query->get('length');
    $search  = $request->query->get('search')['value'];
    $column  = $request->query->get('columns');
    $sort_col = $request->query->get('order')[0]['column'];
    $sort_dir = $request->query->get('order')[0]['dir'];


    $columna = false;
    foreach($column as $_c){
      if ($_c['search']['value'] != ''){
        $columna = $_c;
        break;
      }
    }
    $columns = array(
      0 => 'code',
      1 => 'info'
    );

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Transaction');
    $total = $this->get('ss.front.utils')->getTransactionsTotal();
    // Find transactions.

    if (!$columna){
      $transactions = $repo->findBy(array(), array('purchase_date' => $sort_dir), $length, $start);
      $matches_count = $total;
    } else {
      $transactions  = $repo->search($columna['search']['value'],$columns[$columna['data']], array('purchase_date' => $sort_dir), $length, $start);
      $matches_count = 0;
      //$matches_count = $repo->getSearchCount($columna['search']['value'],$columns[$columna['data']]);

    }


    /*if (!$search) {
      $transactions = $repo->findBy(array(), array('purchase_date' => $sort_dir), $length, $start);
      $matches_count = $total;
    } else {
      $transactions  = $repo->search($search, array('purchase_date' => $sort_dir), $length, $start);
      $matches_count = $repo->getSearchCount($search);
    }*/
    
    // Format results array.

    $result = array();
    foreach ($transactions as $t) {
      if ($matches_count == 0){
        $matches_count = $matches_count+1;
      }
      $result[] = array(
          $t->getPurchaseDate()->format('Y-m-d'),
          $t->getCode(),
          $t->getSession()->getDateTime()->format('Y-m-d H:i'),
          $t->getMovie()->getName(),
          $t->getCinema()->getName(),
          (string)$t->getData('TDC'),
          sprintf('<a href="%s">Ver</a>', $this->generateUrl('social_snack_admin_transactions_view', array('transaction_id' => $t->getId()))),
          (string)$t->getData('ClientIP'),
          !$t->getData('ClientIP') ? '' : sprintf('<a href="%s" class="block-ip" data-post=\'{"ip":"%s"}\'>Bloquear IP</a>', $this->generateUrl('social_snack_admin_block_ip'), $t->getData('ClientIP')),
      );
    }
    
    return new \Symfony\Component\HttpFoundation\JsonResponse(array(
        'data' => $result,
        'draw' => $request->query->get('draw'),
        'recordsFiltered' => $matches_count,
        'recordsTotal' => $total,
    ));
  }

  
  public function transactionsViewAction($transaction_id) {
    $this->check_roles();

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Transaction');
    $transaction = $repo->find($transaction_id);
    return $this->render(
        'SocialSnackAdminBundle:Report:transactionsView.html.php',
        array('t' => $transaction)
    );
  }
  
  
  /**
   * @Template(engine="php")
   */
  public function reportsAction() {
    $this->check_roles();
    $repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
    //$movies = $repo->findBy(array(),array(),null,null,null,false);
    $movies = $repo->findAll();
    return array('movies'=>$movies);
  }

  public function transactionsByMovieAction(Request $request){
    $this->check_roles();
    /** @var \SocialSnack\AdminBundle\Handler\ReportHandler $handler */
    $handler = $this->get('admin.report.handler');
    $data['movie']   = $request->request->get('movie');
    $data['year']    = $request->request->get('year');
    $data['month']   = $request->request->get('month');
    $data['detail']  = $request->request->get('type');

    $result    = $handler->ticketsByMovie($data);

    if ($result == false){
      echo '<script>alert("No se encuentran datos para la pelicula y rango de fechas seleccionado")</script>';
      return $this->forward('SocialSnackAdminBundle:Report:reports');
    }

    $res = new CsvResponse($result);
    //$filename = trim($data['movie'] . '-' . $data['year'] . '-' . $data['month'].'-'.date('YmdHis')).'.csv';
    //$res->saveFile($result,$filename);
    $res->setForExcel(TRUE);
    $res->setFilename('ventas-por-pelicula-'. trim($data['movie'] . '-' . $data['year'] . '-' . $data['month'].'-'.date('YmdHis')) . '.csv');

    return $res;
  }
  
  
  public function ticketsByCinemaByAppAction(Request $request) {
    $this->check_roles();

    /** @var \SocialSnack\AdminBundle\Handler\ReportHandler $handler */
    $handler = $this->get('admin.report.handler');
    $year    = $request->request->get('year');
    $month   = $request->request->get('month');
    $data    = $handler->ticketsByCinemaByApp($year, $month);

    $res = new CsvResponse($data);
    $res->setForExcel(TRUE);
    $res->setFilename('boletos-por-cine-por-app-' . trim($year . '-' . $month, '-') . '.csv');

    return $res;
  }
  
  
  /**
   * @Template(engine="php")
   */
  public function monitorAction(Request $request) {
    $this->check_roles();
    
    /* @var $mem \Memcached */
    $mem = $this->container->get('memcached');
    
    return array(
        'item' => $mem->get('cineplus_block_cc'),
    );
  }
  
  public function monitorStartAction(Request $request) {
    $this->check_roles();
    
    /* @var $blocker \SocialSnack\RestBundle\Service\CineMasBlocker */
    $blocker = $this->get('ss.cineplus_blocker');
    /* @var $mem \Memcached */
    $mem = $this->container->get('memcached');
    
    $item = $mem->get('cineplus_block_cc');
    
    if (!$item || 1 == $request->query->get('force')) {
      $ccs = $blocker->buildCreditCards();
      $item = array(
          'ccs' => $ccs,
          'time' => time()
      );
      $mem->set('cineplus_block_cc', $item, 10 * 60);
    }
    
    return new \Symfony\Component\HttpFoundation\JsonResponse($item);
  }
  
  public function monitorStopAction(Request $request) {
    $this->check_roles();
    
    /* @var $mem \Memcached */
    $mem = $this->container->get('memcached');
    
    $mem->delete('cineplus_block_cc');
    $result = array();
    $cap = $mem->get('cineplus_block_cap');
    if (isset($cap['session_id'])) {
      $session = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Session')->find($cap['session_id']);
    }
    if (isset($session)) {
      $result['session'] = array(
          'id' => $session->getId(),
          'date' => $session->getDateTime()->format('Y-m-d H:i:s'),
          'cinema' => $session->getCinema()->getName(),
          'movie' => $session->getMovie()->getName(),
      );
    }
    if (isset($cap['client_ip'])) {
      $result['client_ip'] = $cap['client_ip'];
    }
    
    return new \Symfony\Component\HttpFoundation\JsonResponse(array(
        'cap' => $result
    ));
  }
  
  
  public function salesByFormatAction(Request $request) {
    set_time_limit(120);

    $this->check_roles();
    
    $year = $request->request->get('year');
    $path = $this->get('kernel')->getRootDir() . '/reports/txfxa/';
    $file = sprintf('%d.csv', $year);
    $filepath = $path . $file;

    if (file_exists($filepath)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/csv');
      header('Content-Disposition: attachment; filename=boletos-por-concepto-por-app-' . basename($file));
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($filepath));
      readfile($filepath);
      die();
    }
  }


  public function transactionsByDateAction(Request $request) {
    $this->check_roles();

    /** @var \SocialSnack\AdminBundle\Handler\ReportHandler $handler */
    $handler = $this->get('admin.report.handler');
    $date    = new \DateTime($request->request->get('date'));
    $data    = $handler->transactionsByDate($date);

    $res = new CsvResponse($data);
    $res->setForExcel(TRUE);
    $res->setFilename('transacciones-' . $date->format('Y-m-d') . '.csv');
    $res->setSharedMaxAge(15 * 60);
    $res->setMaxAge(15 * 60);

    return $res;
  }


  /**
   * @Route("/graphs", name="social_snack_admin_transactions_graphs")
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function graphsAction(Request $request) {
    $this->check_roles();

    /** @var \SocialSnack\AdminBundle\Services\Analytics\TransactionsByHour $service */
    $service = $this->get('analytics.transactionsByHour');
    $dates = $service->getComparableLast24hsDates();

    // Get the values from the last 24 hours.
    $current_from = $dates['current'][0];
    $current_to   = $dates['current'][1];
    $last24       = $service->getTransactionsBetween($current_from, $current_to);
    $transactions = $last24['transactions'];
    $tickets      = array_sum($last24['tickets']);
    $count        = array_sum($transactions);

    // Get the values from a week ago for comparison.
    $compare_from   = $dates['compare'][0];
    $compare_to     = $dates['compare'][1];
    $comp24         = $service->getTransactionsBetween($compare_from, $compare_to);
    $transactions7  = $comp24['transactions'];
    $tickets7       = array_sum($comp24['tickets']);
    $count7         = array_sum($transactions7);

    if ($count7 > 0) {
      $delta = ($count - $count7) / $count7 * 100;
    } else {
      $delta = 100;
    }

    $labels = $service->getRangeLabels($compare_from, $compare_to);

    $trans_by_app = $service->getTransactionsByAppBetween($current_from, $current_to);
    $_apps = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App')->findPublic();
    $app_by_id = array_reduce($_apps, function($carry, $item) {
      $carry[$item->getAppId()] = $item;
      return $carry;
    }, []);

    // Get today values.
    $dates = $service->getComparableTodayDates();
    $today_trans = $service->getTransactionsBetween($dates['current'][0], $dates['current'][1]);
    $comp_trans  = $service->getTransactionsBetween($dates['compare'][0], $dates['compare'][1]->setTime(
        $dates['current'][1]->format('H'),
        $dates['current'][1]->format('i'),
        $dates['current'][1]->format('s')
    )->sub(new \DateInterval('P1D')));
    $today_trans_count   = array_sum($today_trans['transactions']);
    $today_tickets_count = array_sum($today_trans['tickets']);
    $comp_trans_count    = array_sum($comp_trans['transactions']);
    $comp_tickets_count  = array_sum($comp_trans['tickets']);
    if ($comp_trans_count > 0) {
      $today_delta = ($today_trans_count - $comp_trans_count) / $comp_trans_count * 100;
    } else {
      $today_delta = 100;
    }

    return $this->render('SocialSnackAdminBundle:Report:graphs.html.php', [
        'labels'         => $labels,
        'transactions'   => $transactions,
        'transactions_7' => $transactions7,
        'trans_by_app'   => $trans_by_app,
        'apps'           => $app_by_id,
        'last24'         => [
            'transactions' => [
                'count'      => $count,
                'comp_count' => $count7,
                'delta'      => $delta,
            ],
            'tickets'      => [
                'count'      => $tickets,
                'comp_count' => $tickets7,
            ],
        ],
        'today'          => [
            'transactions' => [
                'count'      => $today_trans_count,
                'comp_count' => $comp_trans_count,
                'delta'      => $today_delta,
            ],
            'tickets'      => [
                'count'      => $today_tickets_count,
                'comp_count' => $comp_tickets_count,
            ],
        ],
    ]);
  }

}