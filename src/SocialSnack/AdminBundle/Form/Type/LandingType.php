<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class LandingType extends AbstractType {


  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder->add('name', 'text', ['label' => 'Nombre']);
    $builder->add('active', 'checkbox', [
        'label'    => 'Activa',
        'required' => FALSE,
    ]);
    $builder->add('header', 'file', [
        'label'    => 'Imagen de header',
        'mapped'   => FALSE,
        'required' => FALSE,
        'image_preview_path' => 'config[header]',
        'image_preview_size' => '',
    ]);
    $builder->add('bgimg', 'file', [
        'label'    => 'Imagen de fondo',
        'mapped'   => FALSE,
        'required' => FALSE,
        'image_preview_path' => 'config[bgimg]',
        'image_preview_size' => '',
    ]);
    $builder->add('marker', 'file', [
        'label'    => 'Marcador de mapa',
        'mapped'   => FALSE,
        'required' => FALSE,
        'image_preview_path' => 'config[marker]',
        'image_preview_size' => '',
    ]);
    $builder->add('bgcolor', 'text', [
        'label'  => 'Fondo #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['bgcolor'] : ''
    ]);
    $builder->add('bgcolorcontainer', 'text', [
        'label'  => 'Fondo de contenedor #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['bgcolorcontainer'] : ''
    ]);
    $builder->add('titlecolor', 'text', [
        'label'  => 'Color de título #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['titlecolor'] : ''
    ]);
    $builder->add('tabscolor', 'text', [
        'label'  => 'Color de tabs #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['tabscolor'] : ''
    ]);
    $builder->add('tabscoloron', 'text', [
        'label'  => 'Color de tab activo #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['tabscoloron'] : ''
    ]);
    $builder->add('btcolor', 'text', [
        'label'  => 'Color de botón #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['btcolor'] : ''
    ]);
    $builder->add('btcolorsec', 'text', [
        'label'  => 'Color de botón 2 #',
        'mapped' => FALSE,
        'data'   => $builder->getData() ? $builder->getData()->getConfig()['btcolorsec'] : ''
    ]);
    $builder->add('htmlhead', 'textarea', [
        'label'    => 'HTML en head',
        'mapped'   => FALSE,
        'required' => FALSE,
        'data'     => $builder->getData() ? $builder->getData()->getConfig()['htmlhead'] : '',
        'attr'     => ['class' => 'form-control-code'],
    ]);
    $builder->add('htmlbody', 'textarea', [
        'label'    => 'HTML en body',
        'mapped'   => FALSE,
        'required' => FALSE,
        'data'     => $builder->getData() ? $builder->getData()->getConfig()['htmlbody'] : '',
        'attr'     => ['class' => 'form-control-code'],
    ]);

    $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'postSubmit']);
  }


  public function postSubmit(FormEvent $event) {
    $form = $event->getForm();
    $landing = $event->getData();

    $landing->setConfigValue('bgcolor', $form->get('bgcolor')->getData());
    $landing->setConfigValue('bgcolorcontainer', $form->get('bgcolorcontainer')->getData());
    $landing->setConfigValue('titlecolor', $form->get('titlecolor')->getData());
    $landing->setConfigValue('tabscolor', $form->get('tabscolor')->getData());
    $landing->setConfigValue('tabscoloron', $form->get('tabscoloron')->getData());
    $landing->setConfigValue('btcolor', $form->get('btcolor')->getData());
    $landing->setConfigValue('btcolorsec', $form->get('btcolorsec')->getData());
    $landing->setConfigValue('htmlhead', $form->get('htmlhead')->getData());
    $landing->setConfigValue('htmlbody', $form->get('htmlbody')->getData());

    $landing->setSlug(FrontHelper::sanitize_for_url($landing->getName()));
  }


  public function getName() {
    return '';
  }

}