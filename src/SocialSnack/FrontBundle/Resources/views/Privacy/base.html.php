<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->start('body');
  $menu = array(
    array(
        'label' => 'Privacidad Visitantes',
        'route' => array('privacy_users'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Atención',
        'route' => array('contact_privacy_page'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Cineusuario',
        'route' => array('privacy_page'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Compra',
        'route' => array('checkout_privacy_page'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Payback',
        'route' => array('privacy_payback'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Facturación',
        'route' => array('privacy_billing'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Proveedores',
        'route' => array('privacy_providers'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Candidatos',
        'route' => array('privacy_candidates'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Ex Empleados',
        'route' => array('privacy_employees'),
        'class' => 'icon-file',
    ),
    array(
        'label' => 'Privacidad Promociones',
        'route' => array('privacy_promos'),
        'class' => 'icon-file',
    )
  );
?>
<div class="clearfix row">
  <div class="narrow-side-col col">
    <ul class="panel-menu">
<?php
foreach ($menu as $item) {
  if (!is_array($item['route'])) {
    $item['route'] = array($item['route']);
  }

  $current = (in_array($route, $item['route']));
  ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?>"><?php echo $item['label']; ?></a></li>
<?php } ?>
    </ul>
  </div>
  <div class="wide-main-col col"><div class="content-box">
    <article class="entry-full">
      <?php $view['slots']->output('privacy_body'); ?>
    </article>
  </div></div>
</div>
<?php $view['slots']->stop(); ?>
