<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Atributos - Cinemex' );
$view['slots']->start('body')
?>

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Listado de Atributos
        </header>
        <div class="panel-body">
          <div class="adv-table">
            <div class="clearfix">
              <div class="btn-group">
                <button class="btn green" id="editable-sample_new" onclick="window.location.href='attribute_add'">
                  Nuevo atributo <i class="icon-plus"></i>
                </button>
              </div>
            </div>
            <table  class="display table table-bordered table-striped" id="example">
              <thead>
              <tr>
                <th>ID</th>
                <th>Tipo</th>
                <th>Nombre</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($attributes as $attribute) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $attribute->getId() ?></td>
                  <td><?php echo $attribute->getType(); ?></td>
                  <td><?php echo $attribute->getName(); ?></td>
                  <td><a href="attribute/<?php echo $attribute->getId(); ?>">Editar</a></td>
                  <td><a href="attribute_delete/<?php echo $attribute->getId(); ?>">Eliminar</a></td>
                </tr>
              <?php
              }
              ?>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#example').dataTable( {
        "aaSorting": [[ 4, "desc" ]]
      } );
    } );
  </script>
<?php $view['slots']->stop(); ?>