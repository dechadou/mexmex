<?php

use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', $movie->getId()
    ? 'Editar película ' . $movie->getName() . ' - Cinemex'
    : 'Nueva próxima película'
);
$view['slots']->start('body');
?>
<script>
  $(function() {
    $('#form_release_date').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd ',
      yearRange: "-0:+1"
    });
  });
</script>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <?php if ($movie->getId()) { ?>
        Editar <strong><?php echo $movie->getName(); ?></strong>
        <a href="<?php echo $view['fronthelper']->get_permalink($movie); ?>">(ver en front)</a>
        <?php } else { ?>
        Nuevo pre-estreno
        <?php } ?>
      </header>

      <div class="panel-body">
        <?php if (sizeof($submit_msg)) { ?>
        <div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
        <?php } ?>

        <?php $view['form']->setTheme($form, array('SocialSnackAdminBundle:Form')); ?>
        <form class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">

          <?php echo $view['form']->row($form['name'],           array('label' => 'Título')); ?>
          <?php echo $view['form']->row($form['active'],         array('label' => 'Activo')); ?>
          <?php echo $view['form']->row($form['next_week'],      array('label' => 'Semana que viene')); ?>
          <?php echo $view['form']->row($form['release_date'],   array('label' => 'Fecha de estreno')); ?>
          <?php echo $view['form']->row($form['attribute'],      array('label' => 'Atributos')); ?>
          <?php echo $view['form']->row($form['NOMBREORIGINAL'], array('label' => 'Título Original')); ?>
          <?php echo $view['form']->row($form['DIRECTOR'],       array('label' => 'Director')); ?>
          <?php echo $view['form']->row($form['ACTORES'],        array('label' => 'Actores')); ?>
          <?php echo $view['form']->row($form['GENERO'],         array('label' => 'Género')); ?>
          <?php echo $view['form']->row($form['PAISORIGEN'],     array('label' => 'País')); ?>
          <?php echo $view['form']->row($form['SINOPSIS'],       array('label' => 'Sinopsis')); ?>
          <?php echo $view['form']->row($form['RATING'],         array('label' => 'Clasificación')); ?>
          <?php echo $view['form']->row($form['DURACION'],       array('label' => 'Duración')); ?>

          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Poster</label>
            <div class="col-sm-10">
              <input type="file" value="" name="movie_poster">
              <?php if ($movie->getPosterUrl()) { ?>
              <br />
              <img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '200x295'); ?>" alt="<?php echo $movie->getName(); ?>" width="154" height="230" />
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Trailer</label>
            <div class="col-sm-10">
              <?php echo $view['form']->widget($form['TRAILER']); ?>
              <br />
              <?php if ($youtube_id = FrontHelper::get_youtube_id_from_url($movie->getData('TRAILER'))) { ?>
              <iframe width="300px" height="200px" style="width: 300px; height: 200px;" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
              <?php } ?>
            </div>
          </div>

          <button type="button" class="btn btn-default" onclick="window.location.href = '../coming_list';">Cancelar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </section>
  </div>
</div>
<?php $view['slots']->stop(); ?>