<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use SocialSnack\FrontBundle\Entity\Attribute;

class AttributeController extends BaseController {


  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }


  public function indexAction() {
    $this->check_roles();

    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Attribute');
    $attributes = $repo->findAll();

    return $this->render('SocialSnackAdminBundle:Attribute:list.html.php', array('attributes' => $attributes));
  }

  public function editAction($attribute_id, Request $request) {
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Attribute');
    $attribute = $repo->find($attribute_id);
    $purge = NULL;


    $form = $this->createFormBuilder()
        ->add('name', 'text', array(
            'data' => $attribute->getName(),
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('value', 'text', array(
            'data' => $attribute->getValue(),
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('type', 'choice', array(
            'data' => $attribute->getType(),
            'choices' => array(
                'cine' => 'Cines'
            ),
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('save', 'submit')
        ->getForm();


    $form->handleRequest($request);

    if ($form->isValid()) {
      $helper = $this->get('ss.front.utils');


      $data = $form->getData();
      $attribute->setType((string) $data['type']);
      $attribute->setName((string) $data['name']);
      $attribute->setValue((string) $data['value']);

      $em->persist($attribute);
      $em->flush();

    }

    return $this->render('SocialSnackAdminBundle:Attribute:edit.html.php', array(
        'attribute' => $attribute,
        'form' => $form->createView()
    ));
  }

  public function addAction(Request $request) {
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Attribute');
    $attribute = new Attribute();

    $form = $this->createFormBuilder()
        ->add('name', 'text', array(
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('value', 'text', array(
            'attr' => array(
                'class' => 'form-control')
        ))
        ->add('type', 'choice', array(
            'choices' => array(
                'cine' => 'Cines'
            ),
            'attr' => array(
                'class' => 'form-control')
        ))

        ->add('save', 'submit')
        ->getForm();


    $form->handleRequest($request);

    if ($form->isValid()) {
      $data = $form->getData();
      $attribute->setType((string) $data['type']);
      $attribute->setName((string) $data['name']);
      $attribute->setValue((string) $data['value']);

      $em->persist($attribute);
      $em->flush();

      $cache_purger = $this->get('ss.cache_purger');

      $cache_purger->enqueue(array('entity' => $attribute));
      $cache_purger->purge_cache();

      return $this->redirect($this->generateUrl('social_snack_admin_attribute_edit', array('attribute_id' => $attribute->getId())));
    }

    return $this->render('SocialSnackAdminBundle:Attribute:add.html.php', array('attribute' => $attribute, 'form' => $form->createView()));
  }

  public function deleteAction($attribute_id){
    $this->check_roles();

    $em = $this->get('doctrine')->getManager();
    $repo = $this->get('doctrine')->getRepository('SocialSnackFrontBundle:Attribute');
    $promo = $repo->find($attribute_id);
    $em->remove($promo);
    $em->flush();
    return $this->redirect($this->generateUrl('social_snack_admin_attribute'));
  }

}
