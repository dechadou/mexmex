<?php

namespace SocialSnack\FrontBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Option;
use SocialSnack\FrontBundle\Service\CachePurgeTask\PurgeDoctrineResult;

class DynOptions {

  protected $doctrine;

  protected $cachePurger;

  protected $useCache = TRUE;

  public function __construct(Registry $doctrine, CachePurger $cachePurger) {
    $this->doctrine    = $doctrine;
    $this->cachePurger = $cachePurger;
  }


  public function useCache($useCache) {
    $this->useCache = $useCache;
  }
  
  public function get($key, $default = NULL) {
    if (!is_array($key)) {
      $key = [$key];
    }

    $qb = $this->doctrine->getRepository('SocialSnackFrontBundle:Option')->createQueryBuilder('o');
    $qb
        ->addSelect('o.name')
        ->addSelect('o.value')
    ;

    $or = $qb->expr()->orX();

    foreach ($key as $i => $k) {
      $or->add($qb->expr()->eq('o.name', ':key' . $i));
      $qb->setParameter('key' . $i, $k);
    }

    $qb->andWhere($or);

    $query = $qb->getQuery();

    if ($this->useCache) {
      $query->useResultCache(TRUE, 60 * 60, $this->getCacheId($key));
    }

    $result = $query->getArrayResult();

    if (!$result) {
      return $default;
    }

    try {
      if (count($key) === 1) {
        return unserialize($result[0]['value']);
      } else {
        return array_reduce($result, function ($carry, $item) {
          $carry[$item['name']] = unserialize($item['value']);

          return $carry;
        }, []);
      }
    } catch (\Exception $e) {
      return FALSE;
    }
  }
  
  
  public function set($key, $value) {
    $em   = $this->doctrine->getManager();
    $repo = $this->doctrine->getRepository('SocialSnackFrontBundle:Option');
    $opt  = $repo->findOneBy(array('name' => $key));

    if (!$opt) {
      $opt = new Option();
      $opt->setName($key);
      $opt->setValue(serialize($value));
      $em->persist($opt);
    } else {
      $old_value = unserialize($opt->getValue());
      if ($old_value === $value) {
        return TRUE;
      }
      
      $opt->setValue(serialize($value));
    }

    // Purge cache.
    $task = new PurgeDoctrineResult();
    $task->addDoctrineResource($this->getCacheId($key), CachePurger::ENV_FRONT);
    $this->cachePurger->enqueue($task);
    $this->cachePurger->purge_cache();

    return $em->flush();
  }
  
  
  public function remove($key) {
    
  }


  protected function getCacheId($key) {
    $key = (array)$key;
    sort($key);

    return __METHOD__ . '::' . implode(',', $key);
  }
  
}