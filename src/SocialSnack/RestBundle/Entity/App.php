<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App
 *
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="app_filter_idx", columns={"active","public"}),
 *     @ORM\Index(name="app_app_id_idx", columns={"app_id"})
 *   }
 * )
 * @ORM\Entity(repositoryClass="AppRepository")
 */
class App
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="app_id", type="string", length=100)
     */
    private $appId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="secret_key", type="string", length=255)
     */
    private $secretKey;
    
    /**
     * @var array
     *
     * @ORM\Column(name="caps", type="json_array", length=255)
     */
    private $caps;
    
    /**
     * @var array
     *
     * @ORM\Column(name="settings", type="json_array")
     */
    private $settings;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="public", type="boolean")
     */
    private $public;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    
    public function __construct() {
      $this->caps = [];
      $this->settings = [];
      $this->public = TRUE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return App
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return App
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return App
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    
        return $this;
    }

    /**
     * Get appId
     *
     * @return string 
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return App
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set secretKey
     *
     * @param string $secretKey
     * @return App
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    
        return $this;
    }

    /**
     * Get secretKey
     *
     * @return string 
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }
    
    /**
     * Get capabilities
     *
     * @return string 
     */
    public function getCaps()
    {
        return $this->caps;
    }
    
    
    /**
     * Check if the app has certain capability.
     * 
     * @param string $cap Capability.
     * @return boolean
     */
    public function hasCap($cap) {
      return in_array($cap, $this->caps);
    }
    
    
    /**
     * Set/unset capability.
     * 
     * @param string  $cap
     * @param boolean $set
     * @return boolean
     */
    public function setCap($cap, $set = TRUE) {
      if ($this->hasCap($cap) === $set) {
        // No changes needed.
        return $set;
      }

      if ($set) { // Set
        $this->caps[] = $cap;
      } else { // Unset
        $this->caps = array_diff($this->caps, [$cap]);
      }

      return $this;
    }

    /**
     * Set caps
     *
     * @param string $caps
     * @return App
     */
    public function setCaps($caps)
    {
        $this->caps = $caps;
    
        return $this;
    }

    /**
     * Set settings
     *
     * @param string $settings
     * @return App
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    
        return $this;
    }

    /**
     * Get settings
     *
     * @return string
     */
    public function getSettings()
    {
        return $this->settings;
    }
    
    
    public function getSetting($key) {
      if (!$this->settings || !isset($this->settings[$key])) {
        return NULL;
      }
      
      return $this->settings[$key];
    }
    
    
    public function setSetting($key, $value) {
      $this->settings[$key] = $value;

      return $this;
    }
    
    
    public function unsetSetting($key) {
      unset($this->settings[$key]);

      return $this;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return App
     */
    public function setPublic($public)
    {
        $this->public = $public;
    
        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }
}