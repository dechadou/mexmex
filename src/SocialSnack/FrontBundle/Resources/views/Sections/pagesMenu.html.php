<?php
if (!isset($menu)) {
    $menu = array(
        array(
            'label' => 'Acerca de Cinemex',
            'route' => array( 'about_page' ),
            'class' => 'icon-cinemex',
        ),
        array(
            'label' => 'Términos y Condiciones',
            'route' => array( 'tos_page' ),
            'class' => 'icon-file',
        ),
        array(
            'label' => 'Comunicados',
            'route' => array( 'announcements_page' ),
            'class' => 'icon-file',
        )
    );
}
?>

<ul class="panel-menu">
	<?php
	foreach ( $menu as $item ) {
		if ( !is_array( $item['route'] ) ) {
			$item['route'] = array( $item['route'] );
		}
		$current = ( in_array( $route, $item['route'] ) );
	?>
	<li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
	<?php
	}
	?>
</ul>
