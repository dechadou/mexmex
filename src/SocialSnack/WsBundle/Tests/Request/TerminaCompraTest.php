<?php

namespace SocialSnack\WsBundle\Tests\Request;

/**
 * Class TerminaCompraTest
 * @package SocialSnack\WsBundle\Tests\Request
 * @author Guido Kritz
 */
class TerminaCompraTest extends WebTestCase {

  protected function get_ws_method() {
    return 'TerminaCompra';
  }

  public function testSuccess() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;0&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;2232979&lt;/TRANSACCION&gt;&lt;BOOKING&gt;146316&lt;/BOOKING&gt;&lt;BRANCH&gt;39  &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;UNI&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;521911|0|0||0000000001|2|0001|2|10|G|9|sala 11|521912|0|0||0000000001|2|0003|2|9|G|8|sala 11|521913|0|0||0000000001|2|0002|2|8|G|7|sala 11|521914|0|0||0000000001|2|0002|2|7|G|6|sala 11|521915|0|0||0000000001|2|0002|2|6|G|5|sala 11&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;0&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;0&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Venta exitosa&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');

    $res = self::$ws->request();

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    $this->assertObjectHasAttribute('BOOKING', $res);
    $this->assertObjectHasAttribute('BRANCH', $res);
  }

  public function testPaymentExceptionRechazarTarjeta() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;279468&lt;/TRANSACCION&gt;&lt;BOOKING&gt;2003&lt;/BOOKING&gt;&lt;BRANCH&gt;167 &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;CVI&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;4&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;30&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Ocurrio un error con la aplicación de taquilla|Comando:PAYMENTREQUEST|ERROR_CODE:06,ERROR_DESCRIPTION:RECHAZAR TARJETA&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\PaymentException');
    self::$ws->request();
  }

  public function testPaymentExceptionRetenerTarjeta() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;512390&lt;/TRANSACCION&gt;&lt;BOOKING&gt;84216&lt;/BOOKING&gt;&lt;BRANCH&gt;42  &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;CCO&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;4&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;30&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Ocurrio un error con la aplicación de taquilla|Comando:PAYMENTREQUEST|ERROR_CODE:04,ERROR_DESCRIPTION:RETENER TARJETA&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\PaymentException');
    self::$ws->request();
  }

  public function testPaymentExceptionTarjetaInvalida() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;512387&lt;/TRANSACCION&gt;&lt;BOOKING&gt;84213&lt;/BOOKING&gt;&lt;BRANCH&gt;42  &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;CCO&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;4&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;30&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Ocurrio un error con la aplicación de taquilla|Comando:PAYMENTREQUEST|ERROR_CODE:14,ERROR_DESCRIPTION:TARJETA INVALIDA&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\PaymentException');
    self::$ws->request();
  }

  public function testIEPaymentException() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;-1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;0&lt;/TRANSACCION&gt;&lt;BOOKING&gt;&lt;/BOOKING&gt;&lt;BRANCH&gt;&lt;/BRANCH&gt;&lt;CODIGOCINE&gt;&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;1&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;35&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;El invitado no existe o está inactivo&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\IEPaymentException');
    self::$ws->request();
  }

  public function testInactiveIE() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;-1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;0&lt;/TRANSACCION&gt;&lt;BOOKING&gt;&lt;/BOOKING&gt;&lt;BRANCH&gt;&lt;/BRANCH&gt;&lt;CODIGOCINE&gt;&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;1&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;35&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;El invitado no existe o está inactivo&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\IEPaymentException');
    self::$ws->request();
  }

  public function testInvalidIE() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;-1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;0&lt;/TRANSACCION&gt;&lt;BOOKING&gt;&lt;/BOOKING&gt;&lt;BRANCH&gt;&lt;/BRANCH&gt;&lt;CODIGOCINE&gt;&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;1&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;34&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;La longitud de número de IE debe ser igual a 9&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\IEPaymentException');
    self::$ws->request();
  }

  public function testNotEnoughSeats() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;CMXXML&gt;&lt;RESPUESTA&gt;&lt;RESULTADO&gt;1&lt;/RESULTADO&gt;&lt;TRANSACCION&gt;0&lt;/TRANSACCION&gt;&lt;BOOKING&gt;0&lt;/BOOKING&gt;&lt;BRANCH&gt;217 &lt;/BRANCH&gt;&lt;CODIGOCINE&gt;PPR&lt;/CODIGOCINE&gt;&lt;CADENAASIENTOS&gt;&lt;/CADENAASIENTOS&gt;&lt;TIPOERROR&gt;4&lt;/TIPOERROR&gt;&lt;CLAVEERROR&gt;29&lt;/CLAVEERROR&gt;&lt;MENSAJE&gt;Boletos solicitados mayor a cantidad de boletos disponibles&lt;/MENSAJE&gt;&lt;/RESPUESTA&gt;&lt;/CMXXML&gt;</string>');
    $this->setExpectedException('\SocialSnack\WsBundle\Exception\NotEnoughSeatsException');
    self::$ws->request();
  }

} 