<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;

/**
 * Class UserControllerTest
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class UsersControllerTest extends WebTestCase {

  public function testDefaultAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = static::createClient();
    $route = '/rest/v2/users/';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertObjectHasAttribute('users', $data);
    $this->assertObjectHasAttribute('pagination', $data);

    /** @todo Test Pagination */
  }

  public function testUserAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = static::createClient();
    $route = sprintf(
        '/rest/v2/users/%d',
        LoadUserData::$users[0]->getId()
    );
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertObjectHasAttribute('id', $data);
  }

  public function testUserActionNotFound() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = array_pop(LoadUserData::$users);
    $client = static::createClient();
    $route = sprintf(
        '/rest/v2/users/%d',
        $user->getId() + 100
    );
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(404, $client->getResponse()->getStatusCode());
  }

  public function testCinemasAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = static::createClient();
    $route = sprintf(
        '/rest/v2/users/%d/cinemas',
        LoadUserData::$users[0]->getId()
    );
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertTrue(is_array($data));
  }

  public function testPurchaseHistoryAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = static::createClient();
    $route = sprintf(
        '/rest/v2/users/%d/purchaseHistory',
        LoadUserData::$users[0]->getId()
    );
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'allcaps')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertTrue(is_array($data));
  }

}