<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

class GCMRepository extends EntityRepository {


  /**
   * Queries the number of record by device type.
   *
   * @return array
   */
  public function getCount() {
    $qb = $this->createQueryBuilder('q');
    $qb->select('COUNT(q.id) AS cnt')
        ->addSelect('q.device')
        ->groupBy('q.device');

    $q = $qb->getQuery();
    return $q->getResult(Query::HYDRATE_ARRAY);
  }


  /**
   * @param string $uuid
   * @param string $gcmid
   * @param string $device
   * @return GCM|null
   * @throws \Doctrine\ORM\NonUniqueResultException
   */
  public function findByUuidOrGcmid($uuid, $gcmid, $device) {
    $qb = $this->createQueryBuilder('q');
    $qb
        ->select('q')
        ->where($qb->expr()->orX(
            $qb->expr()->andX(
                $qb->expr()->eq('q.uuid', ':uuid'),
                $qb->expr()->eq('q.device', ':device')
            ),
            $qb->expr()->andX(
                $qb->expr()->eq('q.gcmid', ':gcmid'),
                $qb->expr()->eq('q.device', ':device')
            )
        ))
        ->setParameter('uuid', $uuid)
        ->setParameter('gcmid', $gcmid)
        ->setParameter('device', $device)
        ->setMaxResults(1)
    ;

    try {
      return $qb->getQuery()->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
  }


  /**
   * @param array   $devices
   * @param array   $orderBy
   * @param integer $limit
   * @param integer $offset
   * @return array
   */
  public function findByDevice(array $devices, array $orderBy = NULL, $limit = NULL, $offset = NULL) {
    $qb = $this->createQueryBuilder('q');
    $qb->select('q');

    $orX = $qb->expr()->orX();

    for ($i = 0 ; $i < count($devices) ; $i++) {
      $orX->add($qb->expr()->eq('q.device', ':device' . $i));
      $qb->setParameter('device' . $i, $devices[$i]);
    }

    $qb->andWhere($orX);
    $qb->andWhere('q.active = 1');

    if ($orderBy) {
      foreach ($orderBy as $key => $sort) {
        $qb->addOrderBy('q.' . $key, $sort);
      }
    }

    if (!is_null($limit)) {
      $qb->setMaxResults($limit);
    }

    if (!is_null($offset)) {
      $qb->setFirstResult($offset);
    }

    $q = $qb->getQuery();
    return $q->getResult();
  }


  /**
   * @param array   $devices
   * @param integer $sinceID First ID expected in result (ie: last ID from previous query + 1)
   * @param integer $limit
   * @return array
   */
  public function findByDeviceSince(array $devices, $sinceID, $limit = NULL) {
    $qb = $this->createQueryBuilder('q');
    $qb->select('q');

    $orX = $qb->expr()->orX();

    for ($i = 0 ; $i < count($devices) ; $i++) {
      $orX->add($qb->expr()->eq('q.device', ':device' . $i));
      $qb->setParameter('device' . $i, $devices[$i]);
    }

    $qb
        ->andWhere($orX)
        ->andWhere('q.active = 1')
        ->andWhere('q.id >= :since_id')
        ->setParameter('since_id', $sinceID)
        ->addOrderBy('q.id', 'ASC');

    if (!is_null($limit)) {
      $qb->setMaxResults($limit);
    }

    $q = $qb->getQuery();
    return $q->getResult();
  }

}