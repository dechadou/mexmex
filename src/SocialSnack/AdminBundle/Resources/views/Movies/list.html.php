<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Películas - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Listado de películas activas
              <div class="right">
                <a href="<?php echo $view['router']->generate( 'social_snack_movies_inactive' );?>">Ver inactivos</a>
              </div>
            </header>

            <div class="panel-body">
                  <div class="adv-table">
<!--                        <div class="clearfix">
                            <div class="btn-group">
                                <button class="btn green" id="editable-sample_new" onclick="if (confirm('El proceso se realizará en una nueva ventana. No la cierre hasta que el proceso se complete. Puede seguir trabajando o ver el proceso de la importación en la opción Películas - En aprobación')) { window.open('<?php echo $view['router']->generate( 'social_snack_webservice_moviesenqueue' );?>', 'movie_process', 'width=300,height=150,scrollbars=no,menubar=no,location=no,resizable=no'); } return false;">
                                    Importar películas <i class="icon-plus"></i>
                                </button>
                            </div>
                        </div>-->
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                        <tr>
                            <th>Poster</th>
                            <th>Nombre</th>
                            <th>Versión</th>
                            <th>Estreno</th>
                            <th>Editar</th>
                            <th>Ver</th>
                            <!--<th>Deshabilitar</th>-->
                        </tr>
                        </thead>
                        <tbody>
<?php
foreach ($movies as $movie) {
?>
                        <tr class="gradeX">
                            <td><img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '200x295'); ?>" alt="<?php echo $movie->getName(); ?>" width="154" height="230" /></td>
                            <td><?php echo $movie->getName(); ?></td>
                            <td><?php echo $movie->getLegacyName(); ?></td>
                            <td><?php echo $movie->getPremiere() ? 'Sí' : 'No'; ?></td>
                            <td><a href="<?php echo $view['router']->generate('social_snack_admin_movie', array('movie_id' => $movie->getId())); ?>">Editar</a></td>
                            <td><a href="<?php echo $view['fronthelper']->get_permalink($movie); ?>">Ver</a></td>
                            <!--<th><a href="<?php echo $view['router']->generate('social_snack_admin_movie', array('movie_id' => $movie->getId())); ?>?disbale=true">Deshabilitar</a></th>-->
                        </tr>
<?php
}
?>
                      </table>
                  </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#example').dataTable( {
            "aaSorting": [[ 4, "desc" ]]
        } );
    } );
</script>
<?php $view['slots']->stop(); ?>