<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\FrontBundle\Entity\Modal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ModalType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('title', 'text', array(
            'label' => 'Título',
        ))
        ->add('type', 'choice', array(
            'label'   => 'Tipo',
            'choices' => array(
                0                 => '-- Seleccionar --',
                Modal::TYPE_IMAGE => 'Imagen',
                Modal::TYPE_SWF   => 'SWF',
              //Modal::TYPE_VIDEO   => 'Video',
            ),
            'attr'    => array('class' => 'form-control'),
        ))
        ->add('section', 'choice', array(
            'label'    => 'Sección',
            'choices'  => $options['routes'],
            'multiple' => TRUE,
            'expanded' => TRUE,
        ))
        ->add('cinema', 'choice', array(
            'label'    => 'Cine',
            'data'     => $options['targets'],
            'choices'  => $options['zones'],
            'multiple' => TRUE,
            'required' => FALSE,
            'mapped'   => FALSE,
            'attr'     => ['row_class' => 'cinemas hidden']
        ))
        ->add('dias', 'choice', array(
            'label'    => 'Días',
            'choices'  => array(
                0 => 'Domingo',
                1 => 'Lunes',
                2 => 'Martes',
                3 => 'Miercoles',
                4 => 'Jueves',
                5 => 'Viernes',
                6 => 'Sabado'
            ),
            'multiple' => TRUE,
            'expanded' => TRUE,
            'attr'     => ['row_class' => 'url']
        ))
        ->add('url', 'text', array(
            'label'    => 'URL',
            'required' => FALSE,
            'attr'     => ['row_class' => 'url']
        ))
        ->add('file', 'file', array(
            'label'              => 'Imagen',
            'required'           => FALSE,
            'mapped'             => FALSE,
            'attr'               => ['row_class' => 'hidden file'],
            'image_preview_path' => 'file',
        ))
        ->add('width', 'number', array(
            'label'    => 'Ancho SWF',
            'required' => FALSE,
            'attr'     => ['row_class' => 'hidden swf_size'],
        ))
        ->add('height', 'number', array(
            'label'    => 'Alto SWF',
            'required' => FALSE,
            'attr'     => ['row_class' => 'hidden swf_size']
        ))
        ->add('position', 'choice', array(
            'label'    => 'Posición',
            'choices'  => array(
                'top'    => 'Arriba',
                'center' => 'Centrado'
            ),
            'required' => FALSE,
            'attr'     => [
                'class'     => 'form-control',
                'row_class' => 'hidden position'
            ]
        ))
        ->add('clicktag', 'text', array(
            'label'    => 'ClickTAG',
            'required' => FALSE,
            'attr'     => ['row_class' => 'hidden swf_size']
        ))
        ->add('delay', 'number', array(
            'label' => 'Duracion del Modal (segundos)',
        ))
        ->add('only_once', 'choice', array(
            'label'    => 'Mostrar solo una vez?',
            'choices'  => array(
                0 => 'No',
                1 => 'Si'
            ),
            'attr'     => array('class' => 'form-control'),
            'required' => FALSE
        ))
        ->add('status', 'choice', array(
            'label'   => 'Estado',
            'choices' => array(
                \SocialSnack\FrontBundle\Entity\Article::STATUS_PUBLISH => 'Publicado',
                \SocialSnack\FrontBundle\Entity\Article::STATUS_DRAFT   => 'Borrador',
            ),
            'attr'    => array('class' => 'form-control')
        ))
      ;
  }

  public function getName() {
    return 'form';
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults([
        'routes'   => [],
        'zones'    => [],
        'targets'  => [],
    ]);
  }

}