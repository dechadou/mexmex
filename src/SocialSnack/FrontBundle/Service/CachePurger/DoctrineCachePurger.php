<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use Buzz\Browser;
use Buzz\Message\Form\FormRequest;
use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use Symfony\Component\Routing\Router;

class DoctrineCachePurger implements CachePurgerInterface {

  protected $browser;

  protected $router;

  protected $userAgent;

  protected $environments = [];

  public function __construct(Browser $browser, Router $router, $userAgent) {
    $this->browser   = $browser;
    $this->router    = $router;
    $this->userAgent = $userAgent;
  }


  public function handle(DoctrineTaskInterface $task) {
    $output = [];

    foreach ($task->getDoctrineResources() as $resource) {
      $output = array_merge(
          $output,
          $this->process($resource['ids'], $resource['env'])
      );
    }

    return $output;
  }


  public function canHandle(CachePurgeTaskInterface $task) {
    return $task instanceof DoctrineTaskInterface;
  }


  protected function process($cacheIds, $environment) {
    $output = [];
    $env    = $this->environments[$environment];

    foreach ($env['servers'] as $server) {
      $output = array_merge(
          $output,
          $this->purge($cacheIds, $env['hostname'], $server)
      );
    }

    return $output;
  }

  protected function purge($cacheIds, $hostname, $server) {
    $_output = sprintf('%s: ', $server);
    $output = [];
    $path = $this->router->generate('cache_purge_doctrine');

    // In case protocol relative URL is returned instead of domain relative URL, strip the domain.
    // It seems like this is the case when called from a console command instead of a web request.
    if (preg_match('/^\/\/' . $hostname . '\//', $path)) {
      $path = substr($path, strlen('//' . $hostname));
    }
    
    $req = new FormRequest(FormRequest::METHOD_POST, $path, $server);
    $req->setField('cache_ids', $cacheIds);
    $req->addHeaders([
        sprintf('Host: %s', $hostname),
        sprintf('User-Agent: %s', $this->userAgent)
    ]);

    try {
      $res = $this->browser->send($req);
      foreach (json_decode($res->getContent()) as $row) {
        $output[] = $_output . $row;
      }
    } catch (\Exception $e) {
      $output[] = $_output . $e->getMessage();
    }

    return $output;
  }


  public function setEnv($name, $config) {
    $this->environments[$name] = $config;
  }

}