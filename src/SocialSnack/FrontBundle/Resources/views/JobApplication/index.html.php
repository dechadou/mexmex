<?php
$view->extend('SocialSnackFrontBundle:JobApplication:base.html.php');
$view['slots']->set('title', 'Solicitud de Empleo - Cinemex');
?>

<?php $view['slots']->start('jobs_body'); ?>
  <style>
  .content-box {
    border:0;
    padding:0;
  }
  </style>

  <img src="<?php echo $view['assets']->getUrl('assets/img/jobs-index.jpg'); ?>">
<?php $view['slots']->stop(); ?>
