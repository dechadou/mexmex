<?php

namespace SocialSnack\WsBundle\Tests\Handler;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Handler\MovieGroupHandler;
use SocialSnack\WsBundle\Tests\Fixtures\LoadMovieGroupData;

/**
 * Class MovieGroupHandlerTest
 * @package SocialSnack\WsBundle\Tests\Handler
 * @author Guido Kritz
 */
class MovieGroupHandlerTest extends WebTestCase {

  protected $handler;

  protected $manager;


  public function setUp() {
    $this->manager = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')
        ->disableOriginalConstructor()
        ->getMock();

    $this->manager->expects($this->any())
        ->method('getRepository')
        ->willReturn($this->getContainer()->get('doctrine')->getRepository('SocialSnackWsBundle:Movie'));

    $this->handler = $this->getHandler();

    $this->loadFixtures(array(
        '\SocialSnack\WsBundle\Tests\Fixtures\LoadMovieGroupData',
    ));
  }


  protected function loadFixtures(array $classNames, $omName = null, $registryName = 'doctrine', $purgeMode = null) {
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
    $result = parent::loadFixtures($classNames, $omName, $registryName, $purgeMode);
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));
    return $result;
  }


  protected function getHandler(array $setMethods = []) {
    $handler = $this->getMockBuilder('\SocialSnack\WsBundle\Handler\MovieGroupHandler')
        ->setMethods(array_unique(array_merge(['updateSessionsGroupIds'], $setMethods)))
        ->setConstructorArgs([$this->getContainer(), $this->manager])
        ->getMock();

    return $handler;
  }

  protected function getRepository() {
    return $this->getContainer()->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
  }


  public function testCreateGrouper() {
    $movie = LoadMovieGroupData::$ungrouped[0];

    $grouper = $this->handler->createGrouper($movie);

    $this->assertInstanceOf('\SocialSnack\WsBundle\Entity\Movie', $grouper);
    $this->assertNull($grouper->getLegacyId());
    $this->assertNull($grouper->getLegacyIdBis());
    $this->assertNotNull($grouper->getGroupId());
    $this->assertEquals('grouper', $grouper->getType());
  }


  public function testCreateGrouperFromGrouper() {
    $movie = LoadMovieGroupData::$groupers[0];

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\MovieGroup\GrouperFromGrouperException');

    $this->handler->createGrouper($movie);
  }


  public function testCreateGrouperWithoutGroupID() {
    $movie = LoadMovieGroupData::$ungrouped[0];
    $movie->setGroupId(NULL);

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\MovieGroup\NoGroupIdException');

    $this->handler->createGrouper($movie);
  }


  public function testCreateGroupFromMovie() {
    $this->manager->expects($this->once())
        ->method('persist');
    $this->manager->expects($this->once())
        ->method('flush');

    $movie = LoadMovieGroupData::$ungrouped[0];

    // Create group from movie
    $this->handler->createGroupFromMovie($movie);

    // Check movie now has parent
    $this->assertNotNull($movie->getParent());
    $this->assertInstanceOf('\SocialSnack\WsBundle\Entity\Movie', $movie->getParent());

    // @todo Find a way to test DB persistence.
    // Check no duplicated groupers for a group ID.
//    $this->assertCount(1, $this->getRepository()->findBy(['group_id' => $movie->getGroupId(), 'type' => 'grouper']));

    // Check only this version belongs to the new group
//    $this->assertCount(1, $movie->getParent()->getChildren());
  }


  public function testCreateGroupFromMovieAlreadyGrouped() {
    $movie = LoadMovieGroupData::$grouped[0];

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\MovieGroup\MovieAlreadyGroupedException');

    // Attempt to create a new group form an already grouped movie.
    $this->handler->createGroupFromMovie($movie);
  }


  public function testCreateGroupFromMovieGroupIdConflict() {
    $movie = LoadMovieGroupData::$ungrouped[0];

    $handler = $this->getHandler(['getMoviesInGroup']);
    // Mock the method to return a result and trigger the expected exception.
    $handler->expects($this->once())->method('getMoviesInGroup')
        ->willReturn(['foobar']);

    $this->manager = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')
        ->disableOriginalConstructor()
        ->getMock();

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\MovieGroup\PreviousGroupFoundException');

    // Attempt to create a new group form movie with a group ID conflict.
    $handler->createGroupFromMovie($movie);
  }


  public function testAddMovieToGroup() {
    $movie = LoadMovieGroupData::$ungrouped[0];
    $group_id = LoadMovieGroupData::$grouped[0]->getGroupId();

    $this->manager->expects($this->once())
        ->method('flush');

    // Add movie to group
    $this->handler->addMovieToGroup($movie, $group_id);

    // Check movie now belongs to the group
    $this->assertNotNull($movie->getParent());
    $this->assertInstanceOf('\SocialSnack\WsBundle\Entity\Movie', $movie->getParent());
    $this->assertNotNull($movie->getGroupId());
    $this->assertEquals($movie->getGroupId(), $movie->getParent()->getGroupId());
  }


  public function testUngroupMovie() {
    $movie = LoadMovieGroupData::$grouped[0];

    $this->manager->expects($this->once())
        ->method('flush');

    // Remove movie from group
    $this->handler->ungroupMovie($movie, FALSE);

    // Check movie no longer is grouped
    $this->assertNull($movie->getParent());
  }


  public function testUngroupMovieAndDissolve() {
    $movie = LoadMovieGroupData::$lonely[0];
    $group_id = $movie->getGroupId();

    $this->manager->expects($this->once())
        ->method('flush');

    // Since the method dissolveGroup() is tested in another test, let's just mock the method
    // and assert only that the method is called.
    $handler = $this->getHandler(['dissolveGroup', 'updateSessionsGroupIds', 'getMoviesInGroup']);

    // Return an empty set to trigger the dissolveGroup method.
    $handler->method('getMoviesInGroup')
        ->willReturn([]);

    // Assert that the method is called.
    $handler->expects($this->once())
        ->method('dissolveGroup');

    // Remove movie from group
    $handler->ungroupMovie($movie);

    // @todo Functional test: Check the group no longer exists
//    $this->assertCount(0, $this->getRepository()->findBy(['group_id' => $group_id, 'type' => 'grouper']));
  }


  public function testDissolveGroup() {
    $grouper = LoadMovieGroupData::$groupers[0];
    $group_id = $grouper->getGroupId();

    $this->manager = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')
        ->disableOriginalConstructor()
        ->getMock();
    $this->manager->expects($this->once())
        ->method('remove');
    $this->manager->expects($this->once())
        ->method('flush');

    $handler = $this->getHandler(['getMoviesInGroup']);
    // Mock the method to return the expected value to test this.
    $handler->expects($this->once())->method('getMoviesInGroup')
        ->willReturn([$grouper]);

    // Dissolve group
    $handler->dissolveGroup($group_id);

    // Check group no longer exists and no movie belongs to that group
//    $this->assertCount(0, $this->getRepository()->findBy(['group_id' => $group_id]));
  }


  protected function tearDown(){
    parent::tearDown();
    $this->getContainer()->get('doctrine')->getManager()->close();
  }

} 