<?php /* @var $user \SocialSnack\FrontBundle\Entity\User */ ?>
<div id="header-user-info">
	<figure id="header-user-avatar" class="avatar-64">
    <a href="<?php echo $view['router']->generate( 'user_profile' ); ?>">
      <img src="<?php echo $view['fronthelper']->get_avatar_url( $user->getAvatar(), '64x64' ); ?>" alt="<?php echo $user->getFullName(); ?>" />
    </a>
	</figure>
  <a href="<?php echo $view['router']->generate( 'user_profile' ); ?>" class="user-name">
    <?php echo $user->getFullName(); ?>
  </a>
  <div id="header-user-bt">
    <a href="<?php echo $view['router']->generate( 'user_profile_edit' ); ?>" class="icon icon-white icon-settings no-txt" title="Opciones">Opciones</a>
  </div>
  <div id="header-user-menu">
    <a href="<?php echo $view['router']->generate( 'user_profile_edit' ); ?>" class="icon-small icon-grey icon-user">Mi perfil</a>
    <a href="<?php echo $view['router']->generate( 'user_ie' ); ?>"           class="icon-small icon-grey icon-crown">Invitado especial</a>
    <a href="<?php echo $view['router']->generate( 'logout' ); ?>"            class="icon-small icon-grey icon-close">Cerrar sesión</a>
  </div>
</div>