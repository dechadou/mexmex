<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'NFL - Cinemex');
$view['slots']->set('body_class', 'sponsored-nfl-single');

$view['slots']->start('body')
?>

<div class="section-nfl">
    <div class="nfl-banner">
        <div class="nfl-top">
            <a class="btn btn-default btn-icon icon icon-ticket" href="/pelicula/848/super-bowl-xlviii" title="Comprar boletos">Comprar boletos</a>
        </div>

        <div class="nfl-bottom">
            <div class="nfl-player">
                <iframe src="//youtube.com/embed/PRDNA2NKcNg"></iframe>
            </div>
        </div>

        <div class="nfl-copy"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>