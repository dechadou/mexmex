<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * Movie
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="movie_active_idx", columns={"active"}),
 *   @ORM\Index(name="movie_group_idx",  columns={"group_id"}),
 *   @ORM\Index(name="movie_legacy_idx", columns={"legacy_id"})
 * })
 * @ORM\Entity(repositoryClass="SocialSnack\WsBundle\Entity\MovieRepository")
 */
class Movie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="legacy_id", type="integer", nullable=true)
     */
    private $legacy_id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="legacy_name", type="string", length=255)
     */
    private $legacy_name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=255)
     */
    private $short_name;

    /**
     * @var string
     *
     * @ORM\Column(name="legacy_id_bis", type="string", length=20, nullable=true)
     */
    private $legacy_id_bis;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text")
     */
    private $info;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = 1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="featured", type="boolean")
     */
    private $featured = FALSE;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="premiere", type="boolean")
     */
    private $premiere;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="fixed_position", type="integer", nullable=true)
     */
    private $fixed_position;
		
		/**
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\FrontBundle\Entity\MovieVote", inversedBy="movie")
		 */
		private $votes;
		
		/**
		 * @var float
		 * 
		 * @ORM\Column(name="score", type="float")
		 */
		private $score;
		
		/**
		 * @var string
		 * 
		 * @ORM\Column(name="poster_url", type="string", length=255)
		 */
		private $poster_url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="attributes", type="text")
     */
    private $attributes;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="type", type="string")
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="group_id", type="string", length=20)
     */
    private $group_id;
    
    /**
     * @ORM\OneToMany(targetEntity="Movie", mappedBy="parent")
     */
    private $children;
    
    /**
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="parent")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
		
		/**
		 * @ORM\OneToMany(targetEntity="\SocialSnack\WsBundle\Entity\Session", mappedBy="movie")
		 */
		private $sessions;
		
		private $decoded_attrs = null;
		private $decoded_info  = null;
		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return Movie
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Movie
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set legacy_name
     *
     * @param string $legacyName
     * @return Movie
     */
    public function setLegacyName($legacyName)
    {
        $this->legacy_name = $legacyName;
    
        return $this;
    }

    /**
     * Get legacy_name
     *
     * @return string 
     */
    public function getLegacyName()
    {
        return $this->legacy_name;
    }

    /**
     * Set short_name
     *
     * @param string $shortName
     * @return Movie
     */
    public function setShortName($shortName)
    {
        $this->short_name = $shortName;
    
        return $this;
    }

    /**
     * Get short_name
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->short_name;
    }

    /**
     * Set legacy_id_bis
     *
     * @param string $legacyIdBis
     * @return Movie
     */
    public function setLegacyIdBis($legacyIdBis)
    {
        $this->legacy_id_bis = $legacyIdBis;
    
        return $this;
    }

    /**
     * Get legacy_id_bis
     *
     * @return string 
     */
    public function getLegacyIdBis()
    {
        return $this->legacy_id_bis;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Movie
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Movie
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set movie_info
     *
     * @param \SocialSnack\WsBundle\Entity\MovieInfo $movieInfo
     * @return Movie
     */
    public function setMovieInfo(\SocialSnack\WsBundle\Entity\MovieInfo $movieInfo = null)
    {
        $this->movie_info = $movieInfo;
    
        return $this;
    }

    /**
     * Get movie_info
     *
     * @return \SocialSnack\WsBundle\Entity\MovieInfo 
     */
    public function getMovieInfo()
    {
        return $this->movie_info;
    }
    
    
    /**
     * Constructor
     */
    public function __construct() {
      $this->sessions = new \Doctrine\Common\Collections\ArrayCollection();
      $this->children = new \Doctrine\Common\Collections\ArrayCollection();
      $this->legacy_name = '';
      $this->legacy_short_name = '';
      $this->type = 'single';
      $this->attributes = '{}';
      $this->fixed_position = 0;
      $this->position = 0;
      $this->setScore( 0 );
    }
    
    
    public function __toString() {
      return (string)$this->id;
    }
    
    
    public function __clone() {
      $this->decoded_attrs = NULL;
      $this->decoded_info = NULL;
    }
    
    /**
     * Add sessions
     *
     * @param \SocialSnack\WsBundle\Entity\Session $sessions
     * @return Movie
     */
    public function addSession(\SocialSnack\WsBundle\Entity\Session $sessions)
    {
        $this->sessions[] = $sessions;
    
        return $this;
    }

    /**
     * Remove sessions
     *
     * @param \SocialSnack\WsBundle\Entity\Session $sessions
     */
    public function removeSession(\SocialSnack\WsBundle\Entity\Session $sessions)
    {
        $this->sessions->removeElement($sessions);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSessions() {
      $criteria = Criteria::create();
      $criteria->where(Criteria::expr()->eq('active', 1));
      return $this->sessions->matching($criteria);
    }
    
    
    public function getSessionsInCinema($cinema_id) {
      $criteria = Criteria::create();
      $criteria->where(Criteria::expr()->eq('active', 1))
          ->andWhere(Criteria::expr()->eq('cinema', $cinema_id));
      return $this->sessions->matching($criteria);
    }
		
		
		/**
		 * Get data from the WS movie object.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->info );
			}
      
			if ( !isset( $this->decoded_info->{ $key } ) )
				return NULL;
			
      return $this->decoded_info->{ $key };
		}
		
		
		/**
		 * Get attribute value.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getAttr( $key = NULL ) {
      if ( is_null( $this->decoded_attrs ) ) {
				$this->decoded_attrs = json_decode( $this->getAttributes() );

        if ($this->decoded_attrs === []) {
          $this->decoded_attrs = new \stdClass();
        }
			}
      
      if ( !$key )
        return $this->decoded_attrs;
      
			if ( !isset( $this->decoded_attrs->{ $key } ) )
				return NULL;
      
			return $this->decoded_attrs->{ $key };
		}
    
    
    /**
     * Set attribute value.
     * 
     * @param string $key
     * @param string $value
     */
    public function setAttr( $key, $value ) {
      if ( is_null( $this->decoded_attrs ) ) {
				$this->decoded_attrs = json_decode( $this->getAttributes() );

        if ($this->decoded_attrs === []) {
          $this->decoded_attrs = new \stdClass();
        }
			}
      
      $this->decoded_attrs->{$key} = $value;
      $this->attributes = json_encode( $this->decoded_attrs );
    }

    
    /**
     * Unset attribute.
     * 
     * @param string $key
     */
    public function unsetAttr( $key ) {
      if ( is_null( $this->decoded_attrs ) ) {
				$this->decoded_attrs = json_decode( $this->getAttributes() );
			}
      
      if ( isset($this->decoded_attrs->{$key}) ) {
        unset($this->decoded_attrs->{$key});
      }
      
      $this->attributes = json_encode( $this->decoded_attrs );
    }
    
    /**
     * Set score
     *
     * @param float $score
     * @return Movie
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return float 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set votes
     *
     * @param \SocialSnack\FrontBundle\Entity\MovieVote $votes
     * @return Movie
     */
    public function setVotes(\SocialSnack\FrontBundle\Entity\MovieVote $votes = null)
    {
        $this->votes = $votes;
    
        return $this;
    }

    /**
     * Get votes
     *
     * @return \SocialSnack\FrontBundle\Entity\MovieVote 
     */
    public function getVotes()
    {
        return $this->votes;
    }
		

    /**
     * Set poster_url
     *
     * @param string $posterUrl
     * @return Movie
     */
    public function setPosterUrl($posterUrl)
    {
        $this->poster_url = $posterUrl;
    
        return $this;
    }

    /**
     * Get poster_url
     *
     * @return string 
     */
    public function getPosterUrl()
    {
        return $this->poster_url;
    }
		
		
		public function getSlug() {
			/** @todo Move the logic into a Helper in order to make it reusable */
			$slug = $this->name;
			$slug = strtolower( $slug );
			$slug = preg_replace( '/[^\da-z]/i', '-', $slug );
			$slug = preg_replace( '/\-+/i', '-', $slug );
			return $slug;
		}
		
		public function toArray( $populate_info = TRUE, $populate_sessions = FALSE, $include_versions = TRUE ) {
			$fields = array(
					'id',
					'name',
					'score',
			);
			$result = array();
			foreach ( $fields as $field ) {
				$result[ $field ] = $this->{ $field };
			}
			$result[ 'cover' ]      = $this->getPosterUrl();
			$result[ 'type' ]       = @array_keys(get_object_vars($this->getAttr()));
      if (is_null($result[ 'type' ])) {
        $result[ 'type' ] = array();
      }
			if ( $this->getPremiere() ) {
				$result[ 'type' ][] = 'premiere';
				$result[ 'type' ][] = 'ESTRENO';
			}
			
      if ( $include_versions ) {
        $result[ 'versions' ] = array();
        if ( 'grouper' == $this->getType() ) {
          foreach ( $this->getChildren() as $child ) {
            $result['versions'][] = array(
                'id'   => $child->getId(),
                'type' => array_keys(get_object_vars($child->getAttr()))
            );
          }
        } elseif ( $this->getParent() && $this->getParent()->getId() ) {
          foreach ( $this->getParent()->getChildren() as $child ) {
            $result['versions'][] = array(
                'id' => $child->getId(),
                'type' => array_keys(get_object_vars($child->getAttr()))
            );
          }
        } else {
          $result['versions'][] = array(
              'id' => $this->getId(),
              'type' => array_keys(get_object_vars($this->getAttr()))
          );
        }
      }
			if ( $populate_info ) {
        if ( $this->getParent() && $this->getParent()->getId()) {
          $info_source = $this->getParent();
        } else {
          $info_source = $this;
        }
				$result[ 'info' ] = array(
						'sinopsis'       => is_string($info_source->getData( 'SINOPSIS' ))       ? $info_source->getData( 'SINOPSIS' ) : '',
						'original_title' => is_string($info_source->getData( 'NOMBREORIGINAL' )) ? $info_source->getData( 'NOMBREORIGINAL' ) : '',
						'director'       => is_string($info_source->getData( 'DIRECTOR' ))       ? $info_source->getData( 'DIRECTOR' ) : '',
						'cast'           => is_string($info_source->getData( 'ACTORES' ))        ? $info_source->getData( 'ACTORES' ) : '',
						'country'        => is_string($info_source->getData( 'PAISORIGEN' ))     ? $info_source->getData( 'PAISORIGEN' ) : '',
						'genre'          => (array)$info_source->getData( 'GENERO' ),
						'rating'         => is_string($info_source->getData( 'RATING' ))         ? $info_source->getData( 'RATING' ) : '',
						'year'           => is_string($info_source->getData( 'ANIO' ))           ? $info_source->getData( 'ANIO' ) : '',
						'duration'       => is_string($info_source->getData( 'DURACION' ))       ? $info_source->getData( 'DURACION' ) : '',
						'trailer'        => is_string($info_source->getData( 'TRAILER' ))        ? $info_source->getData( 'TRAILER' ) : '',
				);
				$result[ 'attributes' ] = array_keys(get_object_vars($this->getAttr()));
			}
			
			if ( $populate_sessions ) {
				$result[ 'sessions' ] = array();
        if ( TRUE === $populate_sessions ) {
          $sessions = $this->getSessions();
        } elseif ( is_numeric($populate_sessions) ) {
          $sessions = $this->getSessionsInCinema($populate_sessions);
        } else {
          $sessions = $populate_sessions;
        }
				foreach ( $sessions as $session ) {
          $_sess = $session->toArray();
          unset($_sess['tickets']);
          $result[ 'sessions' ][] = $_sess;
        }
			}
				
			return $result;
		}

    /**
     * Set premiere
     *
     * @param boolean $premiere
     * @return Movie
     */
    public function setPremiere($premiere)
    {
        $this->premiere = $premiere;
    
        return $this;
    }

    /**
     * Get premiere
     *
     * @return boolean 
     */
    public function getPremiere()
    {
        return $this->premiere;
    }

    /**
     * Set position
     *
     * @param integer $order
     * @return Movie
     */
    public function setPosition($order)
    {
        $this->position = $order;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Add children
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $children
     * @return Movie
     */
    public function addChildren(\SocialSnack\WsBundle\Entity\Movie $children)
    {
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $children
     */
    public function removeChildren(\SocialSnack\WsBundle\Entity\Movie $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $parent
     * @return Movie
     */
    public function setParent(\SocialSnack\WsBundle\Entity\Movie $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \SocialSnack\WsBundle\Entity\Movie 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     * @return Movie
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        $this->decoded_attrs = NULL;
    
        return $this;
    }

    /**
     * Get attributes
     *
     * @return string 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set group_id
     *
     * @param string $groupId
     * @return Movie
     */
    public function setGroupId($groupId)
    {
        $this->group_id = $groupId;
    
        return $this;
    }

    /**
     * Get group_id
     *
     * @return string
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Movie
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fixed_position
     *
     * @param integer $fixedPosition
     * @return Movie
     */
    public function setFixedPosition($fixedPosition)
    {
        $this->fixed_position = $fixedPosition;
    
        return $this;
    }

    /**
     * Get fixed_position
     *
     * @return integer 
     */
    public function getFixedPosition()
    {
        return $this->fixed_position;
    }

    /**
     * Set featured
     *
     * @param integer $featured
     * @return Movie
     */
    public function setFeatured($featured)
    {
      $this->featured = $featured;

      return $this;
    }

    /**
     * Get featured
     *
     * @return integer
     */
    public function getFeatured()
    {
      return $this->featured;
    }
}