<?php
namespace SocialSnack\WsBundle\Service;

use Buzz\Message\RequestInterface;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class SMS {

    protected $container;
    protected $doctrine;

    public function __construct($container, $doctrine) {
        $this->container = $container;
        $this->doctrine  = $doctrine;
    }

    public function enqueue($message) {
        $this->execute((object) $message);
    }

    public function execute($message) {
        switch ($message->type) {
            case 'purchase':
                $text = $this->_smsPurchase($message);
                break;
        }

        if ($text === NULL) {
            return FALSE;
        }

        $browser      = $this->container->get('gremo_buzz');
        $api_url      = $this->container->getParameter('sms_api_url');
        $api_code     = $this->container->getParameter('sms_api_code');
        $phone_number = implode('', explode('-', $message->phone_number));

        $fields = array(
            'sms'      => $text,
            'msisdn'   => $phone_number,
            'operador' => $message->phone_carrier,
            'token'    => $api_code
        );

        // This API request does not have response,
        // we assume that everything went well.
        $browser->submit($api_url, $fields, RequestInterface::METHOD_GET);

        return TRUE;
    }

    protected function _smsPurchase($message) {
        $text    = $this->container->getParameter('sms_message_purchase');
        $tickets = 0;
        $session = $message->session->getTime()->format('h:i A');
        $qr_url  = 'http:' . $message->qr_url;

        foreach ($message->transaction->getTickets() as $ticket) {
            $tickets += (int) $ticket->qty;
        }

        // $query = http_build_query(array(
        //     'longUrl'      => $qr_url,
        //     'access_token' => $this->container->getParameter('bitly_token'),
        //     'domain'       => $this->container->getParameter('bitly_domain')
        // ));

        // $url = $this->container->getParameter('bitly_url');
        // $url = $url . '?' . $query;

        // $ch = curl_init($url);

        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        // $response = json_decode(curl_exec($ch));

        // curl_close($ch);

        // if ($response->status_code !== 200) {
        //     return NULL;
        // }

        $qr_url     = $response->data->url;
        $movie_name = $message->movie->getName();

        $text = str_replace('{{TRANSACTION}}', $message->transaction->getCode(), $text);
        $text = str_replace('{{TICKETS}}',     $tickets, $text);
        $text = str_replace('{{SESSION}}',     $message->session->getTime()->format('h:iA'), $text);
        $text = str_replace('{{CINEMA}}',      $message->cinema->getName(), $text);
        $text = str_replace('{{MOVIE}}',       $movie_name, $text);

        $text = substr($text, 0, 160);
        $text = FrontHelper::transliterate_to_ascii($text);
        $text = str_replace('~!@#$%^&*()_+={}|[]\\/<>?', '', $text);

        //$text = substr($text, 0, (160 - (strlen($qr_url) + 1)));
        //$text = $text . ' ' . $qr_url;

        return trim($text);
    }

}
