CMX.init({
  app_id : 'Y2VUfRew32ZbK7IiZxsB'
});

LazyLoad.js( '//connect.facebook.net/es_ES/all.js#xfbml=1&appId=' + config.fb.app_id );

setup_area_w_cinema();

$(window).bind('load resize', sessions_scroll);

function sessions_scroll() {
  $s = $('.sessions-scroller');

  $(window)
    .unbind('load resize',sessions_scroll);

  if ( !$s.length )
    return;

  $(window)
    .bind('resize',sessions_scroll);

  $s.each(function() {
    var $parent   = $(this),
        $children = $parent.children(),
        $last     = $children.children().last();

    $parent.scrollLeft(0);
    $children.css({ width : $last.offset().left + $last.outerWidth()});
    set_h_scroll($children, $parent, 'sessions-scroll-bt', 'sessions-shadow');
  });

}


$( '#billboard-date' ).bind( 'change', function( e ) {
  e.preventDefault();
  var re = /^(.+\/cartelera\/[0-9]+)/;
  var base_url = document.location.href.match(re)[0];
  document.location = base_url + '/date-' + $(this).val();
  $('body').blockui();
} );

$( '#movie-date' ).bind( 'change', function( e ) {
  e.preventDefault();
  var re = /^(.+\/pelicula\/[0-9]+\/[0-9]+)/;
  var base_url = document.location.href.match(re)[0];
  document.location = base_url + '/date-' + $(this).val();
  $('body').blockui();
} );


function parse_login_res( res, cb ) {
  $('body').blockui();
  cb();
}

/**
 * Contact form submit function.
 *
 * @param {type} $form
 * @param {type} $ui
 * @param {type} $bt
 * @returns {undefined}
 */
function contact_submit( $form, $ui, $bt ) {
  $.ajax({
    url      : $form.attr('action'),
    type     : 'post',
    dataType : 'json',
    data     : $form.serialize(),
    success  : function(res) {
      unset_working();
      var $msg = $( '<div/>', { text : '\u00a1Gracias! Ya hemos recibido tus comentarios y consultas, te responderemos a la brevedad.', 'class' : 'msg' } );
      $form.replaceWith( $msg );
      $.scroll_to( $ui, -20 );
    },
    error    : function(res) {
      unset_working();

      var msg = 'Ocurrió un error. Por favor, verifica los datos ingresados e inténtalo nuevamente.';

      $.msg_popup( msg, 'msg-error', true );
    }
  });

  function unset_working() {
    $form.data('working', false);
    $ui.unblockui();
    $bt.removeClass('btn-loading');
  }
}


$(document).ready(function($) {
  if ((!Is.iOS() && !Is.Android() && !Is.WindowsPhone()) || getCookie('hideAppBanner')) {
    return;
  }
  var $banner = $('#goto-app-banner');
  var re = new RegExp('^' + url_prefix + '([a-z\-]+)\/([0-9]+)(\/.*|$)');
  var target = 'cinemex://com.cinemex/home';
  var match = document.location.pathname.match(re);
  if (match) {
    switch (match[1]) {
      case 'pelicula':
        target = 'cinemex://com.cinemex/movie/' + match[2];
        break;

      case 'cine':
        target = 'cinemex://com.cinemex/cinema/' + match[2];
        break;
    }
  }

  $banner.find('.goto-app-close').on('click', function(e) {
    e.stopPropagation();
    setCookie('hideAppBanner', 1, 1);
    $banner.fadeOut();
  });

  $banner.data('target', target);
  $banner.on('click', function(e) {
    if (Is.iOS()) {
      setTimeout(function () { window.location = "https://itunes.apple.com/ar/app/cinemex/id418163740"; }, 25);
    } else if (Is.Android()) {
      setTimeout(function () { window.location = "https://play.google.com/store/apps/details?id=com.cinemex&hl=es"; }, 25);
    } else if (Is.WindowsPhone()) {
      setTimeout(function () { window.location = "http://www.windowsphone.com/s?appid=cef7d94f-2ee9-4ec2-85c0-18d6de33c80c#"; }, 25);
    }

    window.location = $(this).data('target');
  });
  $banner.show();
});