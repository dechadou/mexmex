<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;

/**
 * Class Platinum
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class Platinum extends ConceptHelperAbstract {

  protected function getConceptName() {
    return 'Platinum';
  }

} 