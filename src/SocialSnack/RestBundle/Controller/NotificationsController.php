<?php

namespace SocialSnack\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NotificationsController extends BaseController {

	/**
	 * 
	 * @Route("/", name="rest_notification_default")
	 * @Method({"GET"})
	 */
	public function defaultAction(Request $request) {
    $handler = $result = $this->get('rest.notification.handler');
    $result = $handler->getAll();

    $response = new JsonResponse([
        'notifications' => $handler->serialize($result)
    ]);

    $response->setPublic();
    $response->setMaxAge( 15 * 60 );
    $response->setSharedMaxAge( 45 * 60 );

    return $response;
	}


  /**
   * @Route("/{id}", name="rest_notificaation_detail")
   * @Method({"GET"})
   */
  public function targetAction($id, Request $request) {
    $handler = $result = $this->get('rest.notification.handler');
    $notification = $handler->get($id);

    if (!$notification){
      return new JsonResponse(array('error' => 'ID not found'), 404);
    }

    $response = new JsonResponse($handler->serialize($notification));
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(45 * 60);

    return $response;
  }


}