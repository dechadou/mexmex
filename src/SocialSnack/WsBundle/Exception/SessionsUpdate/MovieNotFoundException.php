<?php

namespace SocialSnack\WsBundle\Exception\SessionsUpdate;

/**
 * Class MovieNotFoundException
 * @package SocialSnack\WsBundle\Exception\SessionsUpdate
 * @author Guido Kritz
 */
class MovieNotFoundException extends \Exception {

} 