<?php

namespace SocialSnack\RestBundle\Tests\Controller;

class BaseControllerTest extends WebTestCase {
	
	
	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;
	
	/**
	 * @var \SocialSnack\FrontBundle\Entity\user
	 */
	protected $user;
	
	/**
	 * @var \SocialSnack\WsBundle\Entity\Cinema
	 */
	protected $cinema;
	
	/**
	 * @var \SocialSnack\WsBundle\Entity\Movie
	 */
	protected $movie;
	
  protected $default_host   = 'cinemex.dev';
	
	protected $access_token   = '00000000000000000001.1c59aacfd26a05379e12a242c54ebed826ab1038';
  

	
	/**
	 * {@inheritDoc}
	 */
	public function setUp() {
		static::$kernel = static::createKernel();
		static::$kernel->boot();
	}	
	
	/**
	 * @return Client A Client instance
	 */
	protected function getClient($host = NULL) {
		return static::createClient( array(), array( 'HTTP_HOST' => !is_null($host) ? $host : $this->default_host ) );
	}
  
  
  /**
   * @todo ALL! Hardcode an user and a known working token and test the results
   *       of BaseController:build_token() agains it?
   */
  public function testBuild_token() {}
  
  
  public function testAuthorize() {}
  
  
  public function test_authorize() {}
  

  
//	\Doctrine\Common\Util\Debug::dump( $client->getResponse()->getContent() );
//	\Doctrine\Common\Util\Debug::dump( $crawler->filter( 'h1' )->first()->extract(array('_text') ) );
}
