<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use Symfony\Component\HttpKernel\Controller\ControllerReference;

$description = 'Cadena de cines en México. Presenta información relevante sobre cartelera, salas, películas en exhibición, concursos y mucho más.';

$body_class = $view['slots']->get('body_class', NULL);
$body_class = array_merge(array('sponsored'), (array) $body_class);
$body_class = join(' ', $body_class);
$request = $this->container->get('request');
$routeName = $request->get('_route');
?>
<!DOCTYPE html>
<html>
<head>
  <?php if (!$app->getDebug()) { ?>
  <script type="text/javascript">
    window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o?o:e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({QJf3ax:[function(t,e){function n(t){function e(e,n,a){t&&t(e,n,a),a||(a={});for(var c=s(e),f=c.length,u=i(a,o,r),d=0;f>d;d++)c[d].apply(u,n);return u}function a(t,e){f[t]=s(t).concat(e)}function s(t){return f[t]||[]}function c(){return n(e)}var f={};return{on:a,emit:e,create:c,listeners:s,_events:f}}function r(){return{}}var o="nr@context",i=t("gos");e.exports=n()},{gos:"7eSDFh"}],ee:[function(t,e){e.exports=t("QJf3ax")},{}],3:[function(t){function e(t){try{i.console&&console.log(t)}catch(e){}}var n,r=t("ee"),o=t(1),i={};try{n=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(i.console=!0,-1!==n.indexOf("dev")&&(i.dev=!0),-1!==n.indexOf("nr_dev")&&(i.nrDev=!0))}catch(a){}i.nrDev&&r.on("internal-error",function(t){e(t.stack)}),i.dev&&r.on("fn-err",function(t,n,r){e(r.stack)}),i.dev&&(e("NR AGENT IN DEVELOPMENT MODE"),e("flags: "+o(i,function(t){return t}).join(", ")))},{1:22,ee:"QJf3ax"}],4:[function(t){function e(t,e,n,i,s){try{c?c-=1:r("err",[s||new UncaughtException(t,e,n)])}catch(f){try{r("ierr",[f,(new Date).getTime(),!0])}catch(u){}}return"function"==typeof a?a.apply(this,o(arguments)):!1}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function n(t){r("err",[t,(new Date).getTime()])}var r=t("handle"),o=t(6),i=t("ee"),a=window.onerror,s=!1,c=0;t("loader").features.err=!0,t(5),window.onerror=e;try{throw new Error}catch(f){"stack"in f&&(t(1),t(2),"addEventListener"in window&&t(3),window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent)&&t(4),s=!0)}i.on("fn-start",function(){s&&(c+=1)}),i.on("fn-err",function(t,e,r){s&&(this.thrown=!0,n(r))}),i.on("fn-end",function(){s&&!this.thrown&&c>0&&(c-=1)}),i.on("internal-error",function(t){r("ierr",[t,(new Date).getTime(),!0])})},{1:9,2:8,3:6,4:10,5:3,6:23,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],5:[function(t){function e(){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var n=t("ee"),r=t("handle"),o=t(1),i=t(2);t("loader").features.stn=!0,t(3),n.on("fn-start",function(t){var e=t[0];e instanceof Event&&(this.bstStart=Date.now())}),n.on("fn-end",function(t,e){var n=t[0];n instanceof Event&&r("bst",[n,e,this.bstStart,Date.now()])}),o.on("fn-start",function(t,e,n){this.bstStart=Date.now(),this.bstType=n}),o.on("fn-end",function(t,e){r("bstTimer",[e,this.bstStart,Date.now(),this.bstType])}),i.on("fn-start",function(){this.bstStart=Date.now()}),i.on("fn-end",function(t,e){r("bstTimer",[e,this.bstStart,Date.now(),"requestAnimationFrame"])}),n.on("pushState-start",function(){this.time=Date.now(),this.startPath=location.pathname+location.hash}),n.on("pushState-end",function(){r("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),"addEventListener"in window.performance&&(window.performance.addEventListener("webkitresourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.webkitClearResourceTimings()},!1),window.performance.addEventListener("resourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.clearResourceTimings()},!1)),document.addEventListener("scroll",e,!1),document.addEventListener("keypress",e,!1),document.addEventListener("click",e,!1)}},{1:9,2:8,3:7,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],6:[function(t,e){function n(t){i.inPlace(t,["addEventListener","removeEventListener"],"-",r)}function r(t){return t[1]}var o=(t(1),t("ee").create()),i=t(2)(o),a=t("gos");if(e.exports=o,n(window),"getPrototypeOf"in Object){for(var s=document;s&&!s.hasOwnProperty("addEventListener");)s=Object.getPrototypeOf(s);s&&n(s);for(var c=XMLHttpRequest.prototype;c&&!c.hasOwnProperty("addEventListener");)c=Object.getPrototypeOf(c);c&&n(c)}else XMLHttpRequest.prototype.hasOwnProperty("addEventListener")&&n(XMLHttpRequest.prototype);o.on("addEventListener-start",function(t){if(t[1]){var e=t[1];"function"==typeof e?this.wrapped=t[1]=a(e,"nr@wrapped",function(){return i(e,"fn-",null,e.name||"anonymous")}):"function"==typeof e.handleEvent&&i.inPlace(e,["handleEvent"],"fn-")}}),o.on("removeEventListener-start",function(t){var e=this.wrapped;e&&(t[1]=e)})},{1:23,2:24,ee:"QJf3ax",gos:"7eSDFh"}],7:[function(t,e){var n=(t(2),t("ee").create()),r=t(1)(n);e.exports=n,r.inPlace(window.history,["pushState"],"-")},{1:24,2:23,ee:"QJf3ax"}],8:[function(t,e){var n=(t(2),t("ee").create()),r=t(1)(n);e.exports=n,r.inPlace(window,["requestAnimationFrame","mozRequestAnimationFrame","webkitRequestAnimationFrame","msRequestAnimationFrame"],"raf-"),n.on("raf-start",function(t){t[0]=r(t[0],"fn-")})},{1:24,2:23,ee:"QJf3ax"}],9:[function(t,e){function n(t,e,n){t[0]=o(t[0],"fn-",null,n)}var r=(t(2),t("ee").create()),o=t(1)(r);e.exports=r,o.inPlace(window,["setTimeout","setInterval","setImmediate"],"setTimer-"),r.on("setTimer-start",n)},{1:24,2:23,ee:"QJf3ax"}],10:[function(t,e){function n(){f.inPlace(this,p,"fn-")}function r(t,e){f.inPlace(e,["onreadystatechange"],"fn-")}function o(t,e){return e}function i(t,e){for(var n in t)e[n]=t[n];return e}var a=t("ee").create(),s=t(1),c=t(2),f=c(a),u=c(s),d=window.XMLHttpRequest,p=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"];e.exports=a,window.XMLHttpRequest=function(t){var e=new d(t);try{a.emit("new-xhr",[],e),u.inPlace(e,["addEventListener","removeEventListener"],"-",o),e.addEventListener("readystatechange",n,!1)}catch(r){try{a.emit("internal-error",[r])}catch(i){}}return e},i(d,XMLHttpRequest),XMLHttpRequest.prototype=d.prototype,f.inPlace(XMLHttpRequest.prototype,["open","send"],"-xhr-",o),a.on("send-xhr-start",r),a.on("open-xhr-start",r)},{1:6,2:24,ee:"QJf3ax"}],11:[function(t){function e(t){var e=this.params,r=this.metrics;if(!this.ended){this.ended=!0;for(var i=0;c>i;i++)t.removeEventListener(s[i],this.listener,!1);if(!e.aborted){if(r.duration=(new Date).getTime()-this.startTime,4===t.readyState){e.status=t.status;var a=t.responseType,f="arraybuffer"===a||"blob"===a||"json"===a?t.response:t.responseText,u=n(f);if(u&&(r.rxSize=u),this.sameOrigin){var d=t.getResponseHeader("X-NewRelic-App-Data");d&&(e.cat=d.split(", ").pop())}}else e.status=0;r.cbTime=this.cbTime,o("xhr",[e,r,this.startTime])}}}function n(t){if("string"==typeof t&&t.length)return t.length;if("object"!=typeof t)return void 0;if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if("undefined"!=typeof FormData&&t instanceof FormData)return void 0;try{return JSON.stringify(t).length}catch(e){return void 0}}function r(t,e){var n=i(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}if(window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent)){t("loader").features.xhr=!0;var o=t("handle"),i=t(2),a=t("ee"),s=["load","error","abort","timeout"],c=s.length,f=t(1);t(4),t(3),a.on("new-xhr",function(){this.totalCbs=0,this.called=0,this.cbTime=0,this.end=e,this.ended=!1,this.xhrGuids={}}),a.on("open-xhr-start",function(t){this.params={method:t[0]},r(this,t[1]),this.metrics={}}),a.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),a.on("send-xhr-start",function(t,e){var r=this.metrics,o=t[0],i=this;if(r&&o){var f=n(o);f&&(r.txSize=f)}this.startTime=(new Date).getTime(),this.listener=function(t){try{"abort"===t.type&&(i.params.aborted=!0),("load"!==t.type||i.called===i.totalCbs&&(i.onloadCalled||"function"!=typeof e.onload))&&i.end(e)}catch(n){try{a.emit("internal-error",[n])}catch(r){}}};for(var u=0;c>u;u++)e.addEventListener(s[u],this.listener,!1)}),a.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),a.on("xhr-load-added",function(t,e){var n=""+f(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),a.on("xhr-load-removed",function(t,e){var n=""+f(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),a.on("addEventListener-end",function(t,e){e instanceof XMLHttpRequest&&"load"===t[0]&&a.emit("xhr-load-added",[t[1],t[2]],e)}),a.on("removeEventListener-end",function(t,e){e instanceof XMLHttpRequest&&"load"===t[0]&&a.emit("xhr-load-removed",[t[1],t[2]],e)}),a.on("fn-start",function(t,e,n){e instanceof XMLHttpRequest&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=(new Date).getTime()))}),a.on("fn-end",function(t,e){this.xhrCbStart&&a.emit("xhr-cb-time",[(new Date).getTime()-this.xhrCbStart,this.onload,e],e)})}},{1:"XL7HBI",2:12,3:10,4:6,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],12:[function(t,e){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");return!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname),r.sameOrigin=!e.hostname||e.hostname===document.domain&&e.port===n.port&&e.protocol===n.protocol,r}},{}],13:[function(t,e){function n(t){return function(){r(t,[(new Date).getTime()].concat(i(arguments)))}}var r=t("handle"),o=t(1),i=t(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(t,e){window.NREUM[e]=n("api-"+e)}),e.exports=window.NREUM},{1:22,2:23,handle:"D5DuLP"}],"7eSDFh":[function(t,e){function n(t,e,n){if(r.call(t,e))return t[e];var o=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return t[e]=o,o}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],gos:[function(t,e){e.exports=t("7eSDFh")},{}],handle:[function(t,e){e.exports=t("D5DuLP")},{}],D5DuLP:[function(t,e){function n(t,e,n){return r.listeners(t).length?r.emit(t,e,n):(o[t]||(o[t]=[]),void o[t].push(e))}var r=t("ee").create(),o={};e.exports=n,n.ee=r,r.q=o},{ee:"QJf3ax"}],id:[function(t,e){e.exports=t("XL7HBI")},{}],XL7HBI:[function(t,e){function n(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:i(t,o,function(){return r++})}var r=1,o="nr@id",i=t("gos");e.exports=n},{gos:"7eSDFh"}],G9z0Bl:[function(t,e){function n(){var t=p.info=NREUM.info,e=f.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){s(d,function(e,n){e in t||(t[e]=n)});var n="https"===u.split(":")[0]||t.sslForHttp;p.proto=n?"https://":"http://",a("mark",["onload",i()]);var r=f.createElement("script");r.src=p.proto+t.agent,e.parentNode.insertBefore(r,e)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=t("handle"),s=t(1),c=(t(2),window),f=c.document,u=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-632.min.js"},p=e.exports={offset:i(),origin:u,features:{}};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),c.addEventListener("load",n,!1)):(f.attachEvent("onreadystatechange",r),c.attachEvent("onload",n)),a("mark",["firstbyte",i()])},{1:22,2:13,handle:"D5DuLP"}],loader:[function(t,e){e.exports=t("G9z0Bl")},{}],22:[function(t,e){function n(t,e){var n=[],o="",i=0;for(o in t)r.call(t,o)&&(n[i]=e(o,t[o]),i+=1);return n}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],23:[function(t,e){function n(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(0>o?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=n},{}],24:[function(t,e){function n(t){return!(t&&"function"==typeof t&&t.apply&&!t[i])}var r=t("ee"),o=t(1),i="nr@wrapper",a=Object.prototype.hasOwnProperty;e.exports=function(t){function e(t,e,r,a){function nrWrapper(){var n,i,s,f;try{i=this,n=o(arguments),s=r&&r(n,i)||{}}catch(d){u([d,"",[n,i,a],s])}c(e+"start",[n,i,a],s);try{return f=t.apply(i,n)}catch(p){throw c(e+"err",[n,i,p],s),p}finally{c(e+"end",[n,i,f],s)}}return n(t)?t:(e||(e=""),nrWrapper[i]=!0,f(t,nrWrapper),nrWrapper)}function s(t,r,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<r.length;c++)s=r[c],a=t[s],n(a)||(t[s]=e(a,f?s+o:o,i,s))}function c(e,n,r){try{t.emit(e,n,r)}catch(o){u([o,e,n,r])}}function f(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){u([r])}for(var o in t)a.call(t,o)&&(e[o]=t[o]);return e}function u(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=r),e.inPlace=s,e.flag=i,e}},{1:23,ee:"QJf3ax"}]},{},["G9z0Bl",4,11,5]);
    ;NREUM.info={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",licenseKey:"06982be375",applicationID:"6043210",sa:1,agent:"js-agent.newrelic.com/nr-632.min.js"}
  </script>
  <?php } ?>
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
  <meta charset="UTF-8" />
  <title><?php $view['slots']->output( 'title', 'Cinemex' ); ?></title>
  <link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/style.css' ); ?>?v3.2" />
  <link rel="stylesheet" href="<?php echo $view['assets']->getUrl( 'assets/css/dyn-styles.css' ); ?>" />
  <!--[if lt IE 9]><script src="<?php echo $view['assets']->getUrl( 'assets/vendor/html5shiv.js' ); ?>"></script><![endif]-->
  <link href='//fonts.googleapis.com/css?family=Roboto:300,300italic,400,500,400italic,700|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl( '/favicon.ico' ); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php $view['slots']->output('head_meta_tags'); ?>
  <meta property="fb:app_id" content="<?php echo $view['fronthelper']->get_fb_app_id(); ?>" />
  <?php if ( !$view['slots']->has('og_tags') ) { ?>
    <meta property="og:image" content="https:<?php echo $view['assets']->getUrl('assets/img/share-img.jpg'); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="<?php echo $description; ?>" />
  <?php } else { ?>
    <?php $view['slots']->output('og_tags'); ?>
  <?php } ?>
  <script>
    var is_mobile = false;
  </script>
  <?php $view['slots']->output('extra_styles'); ?>
</head>
<body class="<?php echo $body_class; ?>">
<div id="overflow-ctl">
  <?php $view['slots']->output('base'); ?>
</div><!-- #overflow-ctl -->

<div id="popups">
  <div id="popups-center">

    <div id="location-popup" class="popup hidden location-popup">
      <?php
      echo $view['actions']->render(
          $view['router']->generate('esi_location_popup', array()),
          array('strategy' => 'esi')
      );
      ?>
    </div><!-- #location-popup -->

    <div id="login-popup" class="popup hidden">
      <?php echo $this->render('SocialSnackFrontBundle:Partials:loginDialog.html.php'); ?>
      <a href="#" class="popup-close icon-small icon-close no-txt">Cerrar</a>
    </div><!-- #login-popup -->

    <?php $view['slots']->output( 'popups' ); ?>

    <!--[if lt IE 9]>
    <div class="popup auto-popup" style="width: 560px; text-align: center; line-height: 1.4em;">
      <div class="top" style="padding: 30px; border-bottom: 1px solid rgb(227, 227, 227);">
        <p class="popup-title" style="font-size: 26px; font-weight: 500; margin-bottom: 20px; color: red;">Aviso</p>
        <p>Para tener una mayor seguridad y una mejor experiencia en Cinemex.com y en otros sitios, te recomendamos actualizar tu navegador.</p>
      </div>
      <div class="bottom" style="padding: 30px; background: none repeat scroll 0% 0% rgb(227, 227, 227); border-top: 1px solid rgb(255, 255, 255);">
        <p>Haz clic en el navegador de tu preferencia para obtener la versión mas actual.</p>
        <p style="margin-top:30px">
          <a href="http://www.google.com/chrome/"><img src="<?php echo $view['assets']->getUrl('assets/img/ie-alert-chrome.jpg'); ?>" alt="Google Chrome" /></a>
          <a href="http://www.mozilla.org/firefox/" style="margin:0 50px"><img src="<?php echo $view['assets']->getUrl('assets/img/ie-alert-firefox.jpg'); ?>" alt="Firefox" /></a>
          <a href="http://windows.microsoft.com/ie"><img src="<?php echo $view['assets']->getUrl('assets/img/ie-alert-ie.jpg'); ?>" alt="Internet Explorer" /></a>
        </p>
      </div>
      <a href="#" class="popup-close icon-small icon-close no-txt">Cerrar</a>
    </div>
    <![endif]-->
  </div><!-- #popups-center -->
</div><!-- #popups -->

<?php
echo $view['actions']->render(
    $view['router']->generate('esi_js_stuff', array()),
    array('strategy' => 'esi')
);
?>

<iframe id="sessionbridge" src="<?php echo $view['router']->generate('post_message_iframe'); ?>" style="display:none;"></iframe>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>!window.jQuery && document.write('<script src="<?php echo $view['assets']->getUrl( 'assets/vendor/jquery/jquery-1.10.2.min.js' ); ?>"><\/script>')</script>

<?php foreach ($view['assetic']->javascripts(
    array(
//            '@SocialSnackFrontBundle/Resources/public/vendor/artberri/jquery.html5storage.min.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/modernizr/modernizr.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/estebanav/geoPosition.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/carhartl/jquery.cookie.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/adamcoulombe/jquery.customSelect.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/rgrove/lazyload-min.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/suprb/jquery.nested.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/magnific-popup/jquery.magnific-popup.min.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/jschannel/jschannel.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/nouislider/jquery.nouislider.min.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/teamdf/jquery.visible.min.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/handlebars/handlebars.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/detectmobilebrowser/detectmobilebrowser.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/swfobject/swfobject.js',
        '@SocialSnackFrontBundle/Resources/public/vendor/igorescobar/jquery.mask.js'
    ),
    array('?yui_js'),
    array('output' => 'js/compiled/vendor.js')
) as $url): ?>
  <script src="<?php echo $view->escape($url) ?>"></script>
<?php endforeach; ?>

<?php $view['slots']->output( 'js_after_vendor' ); ?>

<?php foreach ($view['assetic']->javascripts(
    array(
        '@SocialSnackFrontBundle/Resources/public/js/modules/storagebridge.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/utils.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/popups.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/cookies.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/validators.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/slider.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/location.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/checkout.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/map.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/facebook.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/tour.js',
        '@SocialSnackFrontBundle/Resources/public/js/modules/apisdk.js',
        '@SocialSnackFrontBundle/Resources/public/js/app.js',
    ),
    array('?yui_js'),
    array('output' => 'js/compiled/app.js')
) as $url): ?>
  <script src="<?php echo $view->escape($url) ?>?v3"></script>
<?php endforeach; ?>

<?php $view['slots']->output( 'js_after' ); ?>

<?php if (!isset($is_home) AND isset($_GET['ref']) AND strtolower($_GET['ref']) === 'mx') : ?>
  <script>
    ;(function($) {
      $(window).load(function(){
        $.magnificPopup.open({
          items: {
            src: "<?php echo $view['assets']->getUrl('/assets/img/banner-new-domain.jpg'); ?>"
          },
          type: 'image'
        }, 0);
      });
    })(jQuery);
  </script>
<?php endif; ?>

<?php
$modal_data = $view['slots']->get('modal_params');
echo $view['actions']->render(
    $view['router']->generate(
        'modal',
        array(
            'modal_params' => array(
                'data'=>$modal_data,
                'route'=>$routeName
            )
        )
    ), array('strategy' => 'esi')
);
?>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  //ga('create', '<?php echo $view['fronthelper']->get_analytics_id(); ?>', 'auto');
  ga('create', '<?php echo $view['fronthelper']->get_analytics_id(); ?>', {
    'cookieDomain': 'none'
  });
  <?php $view['slots']->output('ga_extras'); ?>
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
  <?php $view['slots']->output('ga_extras_after'); ?>
</script>


<script type="text/javascript">
  var _sf_async_config = { uid: 52715, domain: 'cinemex.com', useCanonical: true };
  (function() {
    function loadChartbeat() {
      window._sf_endpt = (new Date()).getTime();
      var e = document.createElement('script');
      e.setAttribute('language', 'javascript');
      e.setAttribute('type', 'text/javascript');
      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
      document.body.appendChild(e);
    };
    var oldonload = window.onload;
    window.onload = (typeof window.onload != 'function') ?
        loadChartbeat : function() { oldonload(); loadChartbeat(); };
  })();
</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-JDF5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-JDF5');</script>
<!-- End Google Tag Manager -->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Cinemex_Q1_2016_homeCinemex
URL of the webpage where the tag is expected to be placed: http://cinemex.com/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 01/25/2016
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<img src="https://ad.doubleclick.net/ddm/activity/src=4251971;type=cinem0;cat=cinem00;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1;num=' + a + '?" width="1" height="1" alt=""/>');
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=4251971;type=cinem0;cat=cinem00;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1;num=1?" width="1" height="1" alt=""/>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1661656000721516');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1661656000721516&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1632585817003495');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1632585817003495&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<img src='//pixel.mathtag.com/event/img?mt_id=925688&mt_adid=140050&v1=&v2=&v3=&s1=&s2=&s3=' width='1' height='1' />
</body>
</html>
