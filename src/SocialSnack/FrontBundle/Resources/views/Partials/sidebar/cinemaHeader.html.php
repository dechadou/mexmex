<div id="side-tooltip">
  Selecciona un horario para comprar
</div>

<?php if ( !$single ) { ?>
<div class="sidebar-heading">
  <a href="<?php echo $view['fronthelper']->get_cinema_link( $cinema ); ?>" class="icon icon-star pad-left sidebar-title"><?php echo $cinema->getName(); ?></a>
  <a href="#" class="btn btn-default-alt switch-cinema btn-icon icon icon-rarr btn-icon-right" rel="ask_location">Selecciona otro cine</a>
</div>
<?php } ?>

<div class="sidebar-filters">
    <select name="date" class="sidebar-filter icon-calendar">
    <?php
    $today = new \DateTime( 'today' );
    $tomorrow = new \DateTime( 'tomorrow' );

    foreach ( $available_dates as $_date ) {
        $_date    = $_date['date'];
        $date_lbl = '';

        if ( $today->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
            $date_lbl = 'Hoy, ';
        } else if ( $tomorrow->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) {
            $date_lbl = 'Mañana, ';
        }

        $formatter = new \IntlDateFormatter( \Locale::getDefault(), \IntlDateFormatter::NONE, \IntlDateFormatter::NONE, 'UTC' );
        $formatter->setPattern( 'EEEE dd \'de\' MMMM' );
        $date_lbl .= $formatter->format( $_date );
        $date_lbl = ucfirst( $formatter->format( $_date ) );
    ?>
        <option value="<?php echo $_date->format( 'Ymd' ); ?>"<?php if ( $date->format( 'Y-m-d' ) == $_date->format( 'Y-m-d' ) ) echo ' selected="selected"'; ?>><?php echo $date_lbl; ?></option>
    <?php } ?>

    <?php if ( !$available_dates ) { ?>
        <option value="">Aún no hay fechas para esta película</option>
    <?php } ?>
    </select>

    <select name="type" class="sidebar-filter icon-darr">
        <option value="" selected="selected" disabled="disabled">Ver películas por tipo de sala</option>
        <option value="">Mostrar todas las películas</option>
    </select>

    <?php if (isset($cinema_platinum) AND $cinema_platinum !== NULL) : ?>
    <p class="sidebar-cinema-link"><a class="btn btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_platinum); ?>"><i class="icon icon-platinum"></i> Ver Horarios en Platino</a></p>
    <?php endif; ?>

    <?php if (isset($cinema_standard) AND $cinema_standard !== NULL) : ?>
    <?php if (TRUE === $cinema_standard->getData('premium_only')): ?>
    <p class="sidebar-cinema-link"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_standard); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Premium</a></p>
    <?php else: ?>
    <p class="sidebar-cinema-link"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right" href="<?php echo $view['fronthelper']->get_cinema_link($cinema_standard); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Tradicional</a></p>
    <?php endif; ?>
    <?php endif; ?>
</div>