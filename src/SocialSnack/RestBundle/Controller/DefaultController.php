<?php

namespace SocialSnack\RestBundle\Controller;

use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use SocialSnack\FrontBundle\Entity\MovieVote;
use SocialSnack\RestBundle\Service\Helper as RestHelper;

class DefaultController extends BaseController {
  
  
  /**
   * This route hsa no functionality.
   * Is defined just to be able to get the base route of the API.
   * 
   * @Route("/", name="social_snack_rest")
   */
  public function defaultAction() {
    return new JsonResponse( array( 
        'error' => 'Endpoint missing.'
    ), 404 );
  }
  
  
  /**
   * @Route("/version", name="social_snack_rest_v")
   */
  public function versionAction() {
    return new JsonResponse(array( 
        'version' => '2'
    ));
  }
  

  /**
   * Check if there's already an user registered with FB account with UID = fbuid.
   * 
   * Requires $_POST['fbuid']
   * 
   * @Route("/checkFb")
   * @Method({"POST"})
   */
  public function checkFbAction(Request $request) {
    $fbuid = $request->request->get('fbuid');

    if (!$fbuid) {
      throw new MissingFieldsException();
    }

    $repo  = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
    $user  = $repo->findBy( array( 'fbuid' => $fbuid ) );
    return new JsonResponse( array( 'registered' => $user ? true : false ) );
  }
  
  
	/**
	 * Login.
	 * 
	 * For manual login:
	 * Requires $_POST['username'] OR $_POST['email']
	 * Requires $_POST['password']
	 * 
	 * For FB login:
	 * Requires $_POST['fbsigned_request']
	 * 
	 * @todo Make sure this is the best way to authenticate the user.
	 * 
   * @Route("/login")
   * @Method({"POST"})
   *
   * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function loginAction(Request $request) {
    $auth_handler  = $this->get('rest.auth.handler');
    $user          = $auth_handler->auth($request);
    $from_frontend = $request->request->get('from_frontend');

		return $this->success_auth_res($user, $from_frontend);
	}


	/**
	 * User registration.
	 * 
	 * Requires $_POST['email']
	 * Requires $_POST['first_name']
	 * Requires $_POST['last_name']
	 * Requires $_POST['password']
   * 
	 * Optional $_POST['fbsigned_request']
	 * Optional $_POST['from_frontend']
	 * 
	 * @todo The registration must be handled by the framework.
	 * 
   * @Route("/register")
   * @Method({"POST"})
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function registerAction() {
		/* @var $user \SocialSnack\FrontBundle\Entity\User */
		
		$req = $this->getRequest();
    
		// POST variables.
		$username         = $req->get( 'email' );
		$email            = $req->get( 'email' );
		$first_name       = $req->get( 'first_name' );
		$last_name        = $req->get( 'last_name' );
		$password         = $req->get( 'password' );
		$signed_request   = $req->get( 'fbsigned_request' );
		$access_token     = $req->get( 'fbaccess_token' );
		$from_frontend    = $req->get( 'from_frontend' );
		$privacy_check    = !!$req->get( 'privacy_check' );
		
		// Variables to verify the access token.
    /** @todo Use client IDs instead of this. */
    if ( $from_frontend ) {
      $fb_config      = $this->container->getParameter( 'fb' );
    } else {
      $mobile_config  = $this->container->getParameter( 'mobile_app' );
      $fb_config      = $mobile_config['fb'];
    }
		$secret_key       = $fb_config['secret_key'];
		$app_id           = $fb_config['app_id'];
		$app_access_token = $fb_config['app_access_token'];
		$redirect_url     = '';
    
		$required = array(
				'email',
				'first_name',
				'last_name',
				'password',
		);

		if (
				$first_name !== filter_var($first_name, FILTER_SANITIZE_STRING)
				||
				$last_name  !== filter_var($last_name, FILTER_SANITIZE_STRING)
		) {
			return new JsonResponse( array( 'error' => 'Invalid data', 'errorcode' => 'invalid-data' ), 400 );
		}

		// Check required fields
		foreach ( $required as $field ) {
			if ( empty( ${ $field } ) )
				return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		}

		$em   = $this->getDoctrine()->getManager();
		$repo = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:User' );
    
    if ( $repo->findBy( array( 'email' => $email ) ) ) {
      return new JsonResponse( array( 'error' => 'Duplicated email', 'errorcode' => 'duplicated-email' ), 400 );
    }
		$user = new \SocialSnack\FrontBundle\Entity\User();
		
		// Authenticate FB login.
		if ( $signed_request || $access_token ) {
			try {
        if ($signed_request) {
          $fbuser = RestHelper::validate_signed_requestX( $signed_request, $app_id, $secret_key, $app_access_token, $redirect_url );
        } else {
          $fbuser = RestHelper::validate_access_tokenX( $access_token, $app_id, $secret_key, $app_access_token, $redirect_url );
        }
				$user->setFbuid( $fbuser['user_id'] );
        $user->setAvatar( '//graph.facebook.com/' . $fbuser['user_id'] . '/picture' );
        
        /**
         * @todo Download avatar.
         */
			} catch ( \Exception $e ) {
				return new JsonResponse( array( 'error' => 'Invalid signed request.', 'errorcode' => 'invalid-signed-request' ), 400 );
			}
		}
		
		$encoder_service = $this->get('security.encoder_factory');
		$encoder         = $encoder_service->getEncoder($user);
		$encoded_pass    = $encoder->encodePassword( $password, $user->getSalt() );
			
		$user->setEmail( $email );
		$user->setFirstName( $first_name );
		$user->setLastName( $last_name );
		$user->setPassword( $encoded_pass );
    $user->setPrivacyCheck( $privacy_check );
		
		$em->persist( $user );
		
		try {
			$em->flush();
		} catch ( \Exception $e ) {
      if ( $e->getPrevious() && $e->getPrevious()->getCode() == 23000 ) {
        if ( preg_match( '/Duplicate entry.+unique_email/', $e->getMessage() ) ) {
          return new JsonResponse( array( 'error' => 'Duplicated email', 'errorcode' => 'duplicated-email' ), 400 );
        } elseif ( preg_match( '/Duplicate entry.+unique_fbuid/', $e->getMessage() ) ) {
          return new JsonResponse( array( 'error' => 'Duplicated fbuid', 'errorcode' => 'duplicated-fbuid' ), 400 );
        }
      }
			return new JsonResponse( array( 'error' => 'error', 'd' => $e->getMessage() ), 400 );
		}

		$access_token = $this->build_token( $user );
    
    // Deliver the welcome email.
		try {
			$this->get('cinemex.mailer')->enqueue( array(
					'type' => 'welcome',
					'user_id' => $user->getId()
			) );
		} catch ( \Exception $e ) {}

    $this->container->get('graphite')->publish('user.signup.success', 1);

    
    // If GCM fields are present, let's link the GCM to the user's account.
    if ($req->request->get('uuid') && $req->request->get('gcmid') && $req->request->get('type')) {
      $gcm = $this->container->get('ss.rest.utils')->setGCM(
          $req->request->get('uuid'),
          $req->request->get('gcmid'),
          $req->request->get('type'),
          $user
      );
    }
    

		return $this->success_auth_res( $user, $from_frontend );
	}
	
	
	/**
	 * Flag movie as seen.
	 * 
	 * Requires $_REQUEST['movie_id'] int Movie ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	protected function movieSeenAction() {
		$req        = $this->getRequest();
		$movie_id  = $req->get( 'movie_id' );
		
		// Check required fields.
		if ( empty( $movie_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Movie.
		$em         = $this->getDoctrine()->getManager();
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movie      = $repo->find( $movie_id );
		if ( !$movie )
			return new JsonResponse( array( 'error' => 'Invalid movie ID.' ), 400 );
		
		// Add relationship.
		if ( !$this->user->getMoviesSeen()->contains( $movie ) ) {
			$this->user->getMoviesSeen()->add( $movie );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
	
	
	/**
	 * Remove movie seen flag.
	 * 
	 * Requires $_REQUEST['movie_id'] int Movie ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	protected function movieUnseenAction() {
		$req        = $this->getRequest();
		$movie_id   = $req->get( 'movie_id' );
		$em         = $this->getDoctrine()->getManager();
		
		// Check required fields.
		if ( empty( $movie_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Movie.
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movie      = $repo->find( $movie_id );
		
		if ( !$movie )
			return new JsonResponse( array( 'error' => 'Invalid movie ID.' ), 400 );
		
		// Remove relationship.
		if ( $this->user->getMoviesSeen()->contains( $movie ) ) {
			$this->user->getMoviesSeen()->removeElement( $movie );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
	
	
	/**
	 * Add movie to whish list.
	 * 
	 * Requires $_REQUEST['movie_id'] int Movie ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	protected function movieWishAction() {
		$req        = $this->getRequest();
		$movie_id  = $req->get( 'movie_id' );
		
		// Check required fields.
		if ( empty( $movie_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Movie.
		$em         = $this->getDoctrine()->getManager();
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movie      = $repo->find( $movie_id );
		if ( !$movie )
			return new JsonResponse( array( 'error' => 'Invalid movie ID.' ), 400 );
		
		// Add relationship.
		if ( !$this->user->getMoviesWish()->contains( $movie ) ) {
			$this->user->getMoviesWish()->add( $movie );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}
	
	
	/**
	 * Remove movie from whish list.
	 * 
	 * Requires $_REQUEST['movie_id'] int Movie ID.
	 * 
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	protected function movieUnwishAction() {
		$req        = $this->getRequest();
		$movie_id   = $req->get( 'movie_id' );
		$em         = $this->getDoctrine()->getManager();
		
		// Check required fields.
		if ( empty( $movie_id ) )
			return new JsonResponse( array( 'error' => 'Missing fields.' ), 400 );
		
		// Find Movie.
		$repo       = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
		$movie      = $repo->find( $movie_id );
		
		if ( !$movie )
			return new JsonResponse( array( 'error' => 'Invalid movie ID.' ), 400 );
		
		// Remove relationship.
		if ( $this->user->getMoviesWish()->contains( $movie ) ) {
			$this->user->getMoviesWish()->removeElement( $movie );
			$em->flush();
		}
		
		// Return success code.
		return new JsonResponse( array(), 200 );
	}

  
  /**
   * Requires $_POST['email']
   * 
   * @Route("/recoverPass")
   * @Method({"POST"})
   */
  public function recoverPassAction(Request $request) {
    $doctrine  = $this->getDoctrine();
    $conn      = $doctrine->getConnection();
    $em        = $doctrine->getManager();
    $user_repo = $doctrine->getRepository('SocialSnackFrontBundle:User');
    $email     = $request->request->get('email');
    
    if ( !$email )
      return new JsonResponse( array( 'error' => 'Missing fields' ), 400 );
    
    $user = $user_repo->findOneBy(array('email' => $email));
    if ( !$user )
      return new JsonResponse( array( 'error' => 'No user with that email', 'errorcode' => 'no-user-found' ), 400 );
    
    // Invalidate previous reset codes for this user.
    $query = "UPDATE ResetCode SET active = 0 WHERE user_id = :user_id";
    $stmt  = $conn->prepare($query);
    $stmt->bindValue('user_id', $user->getId());
    $stmt->execute();
    
    // Generate a new reset code.
    $code = new \SocialSnack\FrontBundle\Entity\ResetCode();
    $code->setUser($user);
    $em->persist($code);
    $retry = 0;
    
    do {
      /** @todo Would be nice to measure this loop on Graphite. */
      try {
        $em->flush();
      } catch ( \Exception $e ) {
        if ( $e->getPrevious() && $e->getPrevious()->getCode() == 23000 ) {
          if ( preg_match( '/Duplicate entry.+unique_code/', $e->getMessage() ) ) {
            $retry++;
            continue;
          }
        }
        break;
      }
    } while ( !$code->getId() && $retry < 3 );
    
    if ( !$code->getId() )
      return new JsonResponse( array( 'error' => 'Error', 'errorcode' => 'error' ), 400 );
    
    // Deliver the reset password email.
		try {
			$this->get('cinemex.mailer')->enqueue( array(
					'type' => 'reset-pass',
					'code_id' => $code->getId()
      ) );
		} catch ( \Exception $e ) {}
    
    $this->container->get('graphite')->publish('user.resetpass.req', 1);
    
    return new JsonResponse( array() );
  }
	

  /**
   * @Route("/registerGCM")
   * @Method({"POST"})
   */
  public function registerGCMAction(Request $request) {
    $uuid  = $request->request->get('uuid');
    $gcmid = $request->request->get('gcmid');
    $type  = $request->request->get('type');
    
    if (!isset($uuid, $gcmid, $type)) {
      return new JsonResponse(array('error' => 'missing-fields'), 400);
    }
    
    $gcm = $this->container->get('ss.rest.utils')->setGCM($uuid, $gcmid, $type, $this->user);
    
    if (!$gcm) {
      return new JsonResponse(array('error' => 'unexpected-error'), 400);
    }
    
    return new JsonResponse(array('success' => TRUE), 200);
  }


  /**
   * @Route("/appVersions/{device}", name="rest_app_versions")
	 * @Method({"GET"})
   */
  public function appVersionsAction($device) {
    /** @var \SocialSnack\RestBundle\Handler\RestHandler $handler */
    $handler = $this->get('rest.handler');
    $version = $handler->getAppVersion($device);
    if (is_null($version)) {
      throw $this->createNotFoundException();
    }

    $res = new JsonResponse(array(
        'version' => $version->getVersion(),
    ));
    $res->setSharedMaxAge(60 * 60);
    $res->setMaxAge(60 * 60);

    return $res;
  }


	/**
	 * @Route("/oauth/access_token")
	 * @param Request $request
	 * @return JsonResponse
	 * @throws RestException
	 */
	public function oauthAccessTokenAction(Request $request) {
		if ($request->getScheme() !== 'https') {
			throw new RestException('https-required', 'This endpoint requires HTTPS.');
		}

		$client_id     = $request->query->get('client_id');
		$client_secret = $request->query->get('client_secret');
		$redirect_uri  = $request->query->get('redirect_uri');
		$code          = $request->query->get('code');
		$mem_key       = $code . '.' . $client_id;
		$mem           = $this->get('memcached');
		$auth_handler  = $this->get('rest.auth.handler');
		$user_handler  = $this->get('rest.user.handler');
		$stored        = $mem->get($mem_key);

		if (!$stored) {
			throw new RestException('invalid-code', 'Invalid code.', 401);
		}

		$mem->delete($mem_key);

		$time = $stored['timestamp'];
		$app  = $this->getDoctrine()->getRepository('SocialSnackRestBundle:App')->findByAppId($client_id);
		$user = $user_handler->get($stored['user_id']);

		// Check code consistency.
		if ($code !== $auth_handler->generateOauthCode($user, $app, $time) || $stored['redirect_uri'] !== $redirect_uri || $app->getSecretKey() !== $client_secret) {
			throw new RestException('invalid-code', 'Invalid code.', 401);
		}

		return new JsonResponse([
				'access_token' => $auth_handler->buildToken($user, $app),
				'token_type'   => 'cinemex_token_v1',
				'expires_in'   => 0,
		]);
	}


	/**
	 * Please note this is a very rudimentary method not intended for any sensitive transaction.
	 *
	 * @Route("/login/app")
	 * @Method({"POST"})
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function loginAppAction(Request $request) {
		if (!$this->client || $this->client->getAppId() !== $request->request->get('username') || $this->client->getSetting('appPass') !== $request->request->get('password')) {
			return new JsonResponse(['errorcode' => 'invalid-credentials'], 401);
		}

		return new JsonResponse();
	}

}
