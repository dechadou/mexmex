<?php

namespace SocialSnack\AdminBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CinemexDeepLinkValidator extends ConstraintValidator {

  public function validate($value, Constraint $constraint) {
    if (!preg_match('/^cinemex\:\/\/com\.cinemex\/.+/', $value)) {
      $this->context->addViolation(
          $constraint->message,
          ['%string%' => $value]
      );
    }
  }
}