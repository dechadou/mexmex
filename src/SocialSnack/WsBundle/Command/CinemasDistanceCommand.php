<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;
use SocialSnack\WsBundle\Entity\Cinema;

class CinemasDistanceCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:cinemas:distance' )
			->setDescription( 'Calculate distance between cinemas' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		$updater = $this->getContainer()->get('cinemex.cinema_updater');
    /* @var $updater \SocialSnack\WsBundle\Service\CinemaUpdater */
    return $updater->updateCloser();
	}
	
}