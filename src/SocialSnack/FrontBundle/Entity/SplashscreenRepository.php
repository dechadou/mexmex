<?php
namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class SplashscreenRepository extends EntityRepository {

  private $useCache = FALSE;

  public function useCache($useCache = TRUE) {
    $this->useCache = $useCache;
  }

  public function getList($order=null){
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->addSelect('a')->from('SocialSnackFrontBundle:Splashscreen', 'a');
    $qb->orderBy('a.id', 'DESC');
    $query = $qb->getQuery();
    return $query->getResult();
  }

  public function findActive($args = NULL) {
    $single = FALSE;

    $qb = $this->createQueryBuilder('s');
    $qb->select('s')
        ->andWhere('s.active = 1');

    if (is_numeric($args)) {
      $single = TRUE;
      $qb->andWhere('s.id = :id')
          ->setParameter('id', $args);

    } elseif (!is_null($args)) {
      throw new \Exception('$args argument not implemented yet.');
    }

    $q = $qb->getQuery();
    if ($this->useCache) {
      $q->useResultCache(TRUE, 60 * 60, __METHOD__);
    }

    if (!$single) {
      return $q->getResult();
    }

    try {
      return $q->getSingleResult();
    } catch (NoResultException $e) {
      return NULL;
    }
  }

}
