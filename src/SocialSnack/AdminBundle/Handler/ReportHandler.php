<?php

namespace SocialSnack\AdminBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\RestBundle\Service\Helper as RestHelper;

/**
 * Class ReportHandler
 * @package SocialSnack\AdminBundle\Handler
 * @author Guido Kritz
 */
class ReportHandler {

  const MOVIE_SALES_REPORT_FULL       = 1;
  const MOVIE_SALES_REPORT_BY_TICKET  = 2;
  const MOVIE_SALES_REPORT_BY_CONCEPT = 3;
  const MOVIE_SALES_REPORT_BY_VERSION = 4;

  protected $doctrine;

  static $apps_cache;

  public function __construct(Registry $doctrine) {
    $this->doctrine = $doctrine;
  }


  /**
   * Generates the transactions by date report.
   *
   * @param \DateTime $date
   * @return array
   * @throws \Exception
   */
  public function transactionsByDate(\DateTime $date) {
    $today = new \DateTime('today');
    if ($date->format('Y-m-d') === $today->format('Y-m-d')) {
      throw new \Exception('This method can be used only with dates before the current day.');
    }

    $res = $this->queryTransactionsByDate($date);

    $output = [[
        'Cine', 'Medio', 'Boletos', 'Monto', 'TDC', 'Tipo de boleto', 'Película', 'IE', 'ID', 'Booking', 'Fecha',
    ]];
    foreach ($res as $t) {
      /** @var \SocialSnack\FrontBundle\Entity\Transaction $t */
      $output[] = $this->serializeTransactionsByDateRow($t);
    }

    return $output;
  }


  /**
   * @param Transaction $row
   * @return array
   */
  protected function serializeTransactionsByDateRow(Transaction $row) {
    return [
        $row->getCinema()->getName(),
        $this->getAppName($row->getAppId()),
        $row->getTicketsCount(),
        number_format($row->getAmount()/100, 2),
        (string)$row->getData('TDC'),
        implode(', ', array_map(function($a) {
          return $a->qty . ' ' . $a->name;
        }, $row->getTickets())),
        $row->getMovie()->getName(),
        $row->getData('NUMIE'),
        $row->getData('TRANSACCION'),
        $row->getCode(),
        $row->getPurchaseDate()->format('Y-m-d H:i:s'),
    ];
  }


  /**
   * Queries transactions by date.
   *
   * @param \DateTime $date
   * @return array
   */
  protected function queryTransactionsByDate(\DateTime $date) {
    /** @var \Doctrine\ORM\QueryBuilder $qb */
    $qb = $this->doctrine
        ->getRepository('SocialSnackFrontBundle:Transaction')
        ->createQueryBuilder('t');

    $qb
        ->select('t')
        ->andWhere('t.purchase_date >= :date_from')
        ->andWhere('t.purchase_date <= :date_to')
        ->setParameter('date_from', $date->format('Y-m-d') . ' 00:00:00')
        ->setParameter('date_to',   $date->format('Y-m-d') . ' 23:59:59')
    ;

    $q = $qb->getQuery();
    $q->useResultCache(TRUE, 60 * 60, __CLASS__ . ':' . __METHOD__ . ':' . $date->format('Y-m-d'));
    return $q->getResult();
  }


  /**
   * Returns an associative array with all the available apps using the app ID as the element key.
   *
   * @return array
   */
  protected function getAppsById() {
    $apps = $this->doctrine->getRepository('SocialSnackRestBundle:App')->findAll();

    return array_reduce($apps, function($carry, $item) {
      $carry[$item->getAppId()] = $item;
      return $carry;
    }, array());
  }


  /**
   * Returns the app name from an app ID.
   *
   * @param  string $app_id App ID.
   * @return string|null    App Name
   */
  protected function getAppName($app_id) {
    if (!isset(self::$apps_cache)) {
      self::$apps_cache = $this->getAppsById();
    }

    if (isset(self::$apps_cache[$app_id])) {
      return self::$apps_cache[$app_id]->getName();
    } else {
      return NULL;
    }
  }

  protected function getCinemasNamesById() {
    $cinemas_qb = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $cinemas_qb->select('c.id', 'c.name')->where('c.active = 1');

    $cinemas_q = $cinemas_qb->getQuery();
    $cinemas = $cinemas_q->getResult();

    return array_reduce($cinemas, function($carry, $item) {
      $carry[$item['id']] = $item['name'];
      return $carry;
    }, array());
  }


  protected function getYearMonthRange($year, $month = NULL) {
    if ($year > (int)date('Y') || $year < 2013 || ($year == date('Y') && $month > (int)date('m'))) {
      throw new \Exception('Invalid date.');
    }

    if (!$month) {
      $from_month = 1;
    } else {
      $from_month = $month;
    }

    $date_from = new \DateTimeImmutable($year . '-' . $from_month . '-1 00:00:00');

    if (!$month) {
      $date_to = $date_from->add(new \DateInterval('P1Y'));
    } else {
      $date_to = $date_from->add(new \DateInterval('P1M'));
    }

    return [$date_from, $date_to];
  }


  public function ticketsByCinemaByApp($year, $month = NULL) {
    list($date_from, $date_to) = $this->getYearMonthRange($year, $month);

    $doctrine      = $this->doctrine;
    $apps_by_id    = $this->getAppsById();
    $cinemas_by_id = $this->getCinemasNamesById();

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare(
        'SELECT t.cinema_id AS cid, SUM(t.tickets_count) AS tickets, t.app_id
        FROM `Transaction` t
        WHERE t.purchase_date >= :date_from
        AND t.purchase_date < :date_to
        GROUP BY t.cinema_id, t.app_id;'
    );
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to',   $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();
    $result = $stmt->fetchAll();

    $cinemas_stats = array();
    foreach ($result as $row) {
      $cid = $row['cid'];
      $cinema = $cinemas_by_id[$cid];
      if (!$cinema) {
        continue;
      }

      if (!isset($cinemas_stats[$cid])) {
        $cinemas_stats[$cid] = array(
            'cinema'  => $cinema,
            'total'   => 0,
        );
      }
      if (!array_key_exists($row['app_id'], $apps_by_id)) {
        continue;
      }

      $cinemas_stats[$cid][$row['app_id']] = $row['tickets'];
      $cinemas_stats[$cid]['total'] += $row['tickets'];
    }

    usort($cinemas_stats, function($a, $b) {
      if ($a['total'] > $b['total']) {
        return -1;
      } elseif ($a['total'] < $b['total']) {
        return 1;
      } else {
        return 0;
      }
    });

    $row = ['Complejo'];
    foreach ($apps_by_id as $app_id => $app) {
      if (!$app->hasCap('purchase')) {
        continue;
      }

      $row[] = $app->getName();
    }
    $row[] = 'Total';
    $output[] = $row;
    foreach($cinemas_stats as $item) {
      $row = [$item['cinema']];
      foreach ($apps_by_id as $app_id => $app) {
        if (!$app->hasCap('purchase')) {
          continue;
        }

        $row[] = isset($item[$app_id]) ? $item[$app_id] : 0;
      }
      $row[] = $item['total'];
      $output[] = $row;
    }

    return $output;
  }

  public function ticketsByMovie($data){
    switch($data['detail']){
      case self::MOVIE_SALES_REPORT_FULL:
        return $this->ticketsByMovieFULL($data);
        break;

      case self::MOVIE_SALES_REPORT_BY_TICKET:
        return $this->ticketsByMovieByTicketType($data);
        break;

      case self::MOVIE_SALES_REPORT_BY_CONCEPT:
        return $this->ticketsByMovieByConcept($data);
        break;

      case self::MOVIE_SALES_REPORT_BY_VERSION:
        return $this->ticketsByMovieByVersion($data);
        break;
    }
  }

  public function ticketsByMovieByVersion($data){
    list($date_from, $date_to) = $this->getYearMonthRange($data['year'], $data['month']);
    $movie = $data['movie'];
    $doctrine = $this->doctrine;

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare("SELECT id, name, attributes, group_id FROM Movie WHERE parent_id = :parent_id");
    $stmt->bindValue('parent_id', $movie);
    $stmt->execute();
    $movies = $stmt->fetchAll();
    $group_id = $movies[0]['group_id'];
    $movies_by_id = array_reduce($movies, function($carry, $item) {
      $carry[$item['id']] = $item;
      return $carry;
    }, []);

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare("
        SELECT td.movie_id, SUM(td.amount)/100 AS Total, SUM(td.qty) as Cantidad
        FROM Transaction t
        INNER JOIN TransactionDataAttr td ON t.id = td.transaction_id
        WHERE
            t.purchase_date >= :date_from
            AND t.purchase_date < :date_to
            AND td.group_id = :group_id
        GROUP BY td.movie_id;
    ");
    $stmt->bindValue('group_id',  $group_id);
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to',   $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();
    $result = $stmt->fetchAll();

    if (empty($result)){
      return false;
      exit();
    }

    $movie_stats[] = [
        'Pelicula',
        'Versión',
        'Boletos',
        'Ingreso'
    ];

    $total_recaudado = 0;
    $total_entradas  = 0;
    foreach($result as $row){
      $total_recaudado += $row['Total'];
      $total_entradas  += $row['Cantidad'];
      $movie_stats[] = array(
          $movies[0]['name'],
          implode(' ', FrontHelper::get_movie_attrs(json_decode($movies_by_id[$row['movie_id']]['attributes']))),
          $row['Cantidad'],
          number_format($row['Total'], 2, ',', '.')
      );
    }

    $movie_stats[] = array(
        'Totales',
        '',
        $total_entradas,
        '$'.number_format($total_recaudado, 2, ',', '.')
    );
    return $movie_stats;
  }

  public function ticketsByMovieByConcept($data){
    list($date_from, $date_to) = $this->getYearMonthRange($data['year'], $data['month']);
    $movie = $data['movie'];
    $doctrine      = $this->doctrine;

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare("SELECT name,group_id FROM Movie WHERE parent_id = '$movie' LIMIT 1");
    $stmt->execute();
    $movies = $stmt->fetchAll();
    $group = $movies[0]['group_id'];


    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare(
        "SELECT t.id, t.movie_id, t.purchase_date, SUM(td.amount)/100 AS Total, SUM(td.qty) as Cantidad, td.attr, td.transaction_id
         FROM Transaction t, TransactionDataAttr td
         WHERE t.purchase_date >= :date_from
         AND t.purchase_date < :date_to
         AND t.id = td.transaction_id
         AND td.group_id = '$group'
         GROUP BY td.attr"
    );
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to',   $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();
    $result = $stmt->fetchAll();

    if (empty($result)){
      return false;
      exit();
    }

    $movie_stats['columns'] = [
        'Pelicula',
        'Concepto',
        'Cantidad de Tickets',
        'Total de la Venta'
    ];

    $total_recaudado = 0;
    $total_entradas  = 0;
    foreach($result as $row){
      $total_recaudado = $total_recaudado+$row['Total'];
      $total_entradas  = $total_entradas+$row['Cantidad'];
      $movie_stats[] = array(
          $movies[0]['name'],
          $row['attr'],
          $row['Cantidad'],
          number_format($row['Total'], 2, ',', '.')
      );
    }

    $movie_stats[] = array(
        'Totales',
      '',
        $total_entradas,
        '$'.number_format($total_recaudado, 2, ',', '.')

    );
    return $movie_stats;
  }

  public function ticketsByMovieByTicketType($data){
    list($date_from, $date_to) = $this->getYearMonthRange($data['year'], $data['month']);
    $movie = $data['movie'];
    $doctrine      = $this->doctrine;

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare("SELECT name,group_id FROM Movie WHERE parent_id = '$movie' LIMIT 1");
    $stmt->execute();
    $movies = $stmt->fetchAll();
    $group = $movies[0]['group_id'];

    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare(
        "SELECT t.id, t.movie_id, t.purchase_date, SUM(td.amount)/100 AS Total, SUM(td.qty) as Cantidad, td.ticket, td.transaction_id
         FROM Transaction t, TransactionDataTickets td
         WHERE t.purchase_date >= :date_from
         AND t.purchase_date < :date_to
         AND t.id = td.transaction_id
         AND td.group_id = '$group'
         GROUP BY td.ticket"
    );
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to',   $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();
    $result = $stmt->fetchAll();

    if (empty($result)){
      return false;
      exit();
    }

    $movie_stats['columns'] = [
      'Pelicula',
        'Tipo de Ticket',
        'Cantidad de Tickets',
        'Total de la Venta'
    ];

    $total_recaudado = 0;
    $total_entradas  = 0;
    foreach($result as $row){
      $total_recaudado = $total_recaudado+$row['Total'];
      $total_entradas  = $total_entradas+$row['Cantidad'];
      $movie_stats[] = array(
        $movies[0]['name'],
          $row['ticket'],
          $row['Cantidad'],
          number_format($row['Total'], 2, ',', '.')
      );
    }

    $movie_stats[] = array(
        'Totales',
        '',
        $total_entradas,
        '$'.number_format($total_recaudado, 2, ',', '.')

    );
    return $movie_stats;
  }

  public function ticketsByMovieFULL($data){
    list($date_from, $date_to) = $this->getYearMonthRange($data['year'], $data['month']);
    $movie = $data['movie'];
    $doctrine      = $this->doctrine;
    $conn = $doctrine->getConnection();
    $stmt = $conn->prepare(
        "SELECT t.cinema_id AS cid,
                t.tickets AS tickets,
                t.app_id,
                a.name AS app_name,
                t.id as transaction_id,
                SUM(t.tickets_count) AS tickets_count,
                t.movie_id,
                t.purchase_date as dia_compra,
                SUM(t.amount)/100 AS Total,
                m.id,
                m.parent_id,
                m.name
        FROM `Transaction` t, App a, Movie m
        WHERE t.purchase_date >= :date_from
        AND t.purchase_date < :date_to
        AND t.movie_id = m.id
        AND m.parent_id = '$movie'
        AND t.app_id = a.app_id
        GROUP BY t.id"
    );
    $stmt->bindValue('date_from', $date_from->format('Y-m-d H:i:s'));
    $stmt->bindValue('date_to',   $date_to->format('Y-m-d H:i:s'));
    $stmt->execute();
    $result = $stmt->fetchAll();


    if (empty($result)){
      return false;
      exit();
    }

    $cinemas_by_id  = $this->getCinemasNamesById();

    $movie_stats = [[
        'Dia de Compra',
        'Pelicula',
        'Cine',
        'Medio',
        'Descripcion',
        'Precio',
        'Cantidad de Tickets',
        'Total de la Transaccion'
    ]];
    $total_recaudado = 0;
    $total_entradas = 0;
    foreach($result as $row){

      $cid = $row['cid'];
      $cinema = $cinemas_by_id[$cid];
      if (!$cinema) {
        continue;
      }

      if (!isset($movie_stats[$row['transaction_id']])) {
        $tickets = json_decode($row['tickets'],true);
        $total_entradas = $total_entradas+$row['tickets_count'];
        $total_recaudado = $total_recaudado+$row['Total'];
        $movie_stats[] = array(
            $row['dia_compra'],
            $row['name'],
            $cinema,
            $row['app_name'],
            $tickets[0]['name'],
            $tickets[0]['price'],
            $row['tickets_count'],
            number_format($row['Total'], 2, ',', '.')
        );
      }
    }

    $movie_stats[] = array(
        'Totales',
        '',
        '',
        '',
        '',
        '',
        $total_entradas,
        '$'.number_format($total_recaudado, 2, ',', '.')
    );
    return $movie_stats;
  }

} 