<ul class="ie-levels-tabs clearfix row">
  <li class="col col-sm-1-3">
    <div class="tab" data-target="#ie-levels-basic">
      <p>Básico</p>
      <div class="hr"></div>
      <div class="content">
        <img src="<?php echo $view['assets']->getUrl('assets/img/card-basica.png'); ?>" alt="" />
      </div>
    </div>
  </li>
  <li class="col col-sm-1-3">
    <div class="tab" data-target="#ie-levels-gold">
      <p>Oro</p>
      <div class="hr"></div>
      <div class="content">
        <img src="<?php echo $view['assets']->getUrl('assets/img/card-oro.png'); ?>" alt="" />
      </div>
    </div>
  </li>
  <li class="col col-sm-1-3">
    <div class="tab" data-target="#ie-levels-premium">
      <p>Premium</p>
      <div class="hr"></div>
      <div class="content">
        <img src="<?php echo $view['assets']->getUrl('assets/img/card-premium.png'); ?>" alt="" />
      </div>
    </div>
  </li>
</ul>