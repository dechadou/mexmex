<?php
/** @var \SocialSnack\RestBundle\Entity\AppVersion $version */

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de Versiones de Apps - Cinemex');
$view['slots']->start('body')
?>

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Listado de Versiones de Apps
        </header>
        <div class="panel-body">
          <?php if (sizeof($submit_msg)) { ?>
            <div class="alert alert-info"><?php echo implode('<br>', $submit_msg); ?></div>
          <?php } ?>

          <div class="adv-table">
            <div class="clearfix">
              <div class="btn-group">
                <a class="btn btn-default" id="editable-sample_new" href="<?php echo $view['router']->generate('admin_appversions_add'); ?>">
                  Nueva versión <i class="icon-plus"></i>
                </a>
              </div>
            </div>
            <table  class="display table table-bordered table-striped" id="example">
              <thead>
              <tr>
                <th>ID</th>
                <th>Dispositivo</th>
                <th>Versión</th>
                <th>Editar</th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($versions as $version) {
                ?>
                <tr class="gradeX">
                  <td><?php echo $version->getId(); ?></td>
                  <td><?php echo $version->getDevice(); ?></td>
                  <td><?php echo $version->getVersion(); ?></td>
                  <td>
                    <a href="<?php echo $view['router']->generate('admin_appversions_edit', array('id' => $version->getId())); ?>">Editar</a>
                    <a href="<?php echo $view['router']->generate('admin_appversions_remove', array('id' => $version->getId())); ?>" onclick="return confirm('Esta acción no se puede deshacer. Deseas continuar?');">Eliminar</a>
                  </td>
                </tr>
              <?php
              }
              ?>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#example').dataTable({
        "aaSorting": [[0, "desc"]]
      });
    });
  </script>
<?php $view['slots']->stop(); ?>