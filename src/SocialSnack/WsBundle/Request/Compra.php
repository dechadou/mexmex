<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\IEPaymentException;
use SocialSnack\WsBundle\Exception\NotEnoughSeatsException;
use SocialSnack\WsBundle\Exception\PaymentException;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class Compra extends ConsultaAbstract {

	protected $method       = 'Compra';
	
	protected $default_args = array(
			'ClaveExterna'      => 0,
			'CodigoCine'        => 0,
			'Session'           => 0,
			'CadenaBoletos'     => '',
			'TDC'               => 0,
			'Vigencia'          => 0,
			'DV'                => 0,
			'CadenaPlatino'     => '',
			'IE'                => '',
			'NombreCliente'     => 0,
	);
	
	
	public function request($exec = TRUE) {
		if ( 0 == $this->args[ 'ClaveExterna' ] ) {
			/* @todo Replace this with an unique ID */
			$this->args[ 'ClaveExterna' ] = rand( 111111, 999999 );
		}
		
		return parent::request($exec);
	}
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
   * @throws PaymentException
	 * @param \SimpleXMLElement $xml;
   * @return \SimpleXMLElement
	 */
	public function parse_res( $xml ) {
		// Check for successful response.
		if (
				isset( $xml->RESULTADO, $xml->TIPOERROR, $xml->CLAVEERROR, $xml->TRANSACCION, $xml->BOOKING, $xml->BRANCH )
				&& $xml->RESULTADO->__toString() === '0'
				&& $xml->TRANSACCION->__toString() !== ''
		) {
			return $xml;
    }

		if (!isset($xml->MENSAJE)) {
      throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
    }

    if (preg_match('/\bERROR_DESCRIPTION\:(RECHAZAR TARJETA|RETENER TARJETA|FONDOS INSUFICIENTES|TARJETA INVALIDA|FONDOS INSUFICIENTES)\b/', $xml->MENSAJE)) {
      throw new PaymentException($xml->MENSAJE);
    }

    if ('El invitado no existe o está inactivo' === $xml->MENSAJE->__toString()) {
			throw new IEPaymentException($xml->MENSAJE);
		}

		if ('La longitud de número de IE debe ser igual a 9' === $xml->MENSAJE->__toString()) {
      throw new IEPaymentException($xml->MENSAJE);
    }

    if ('Boletos solicitados mayor a cantidad de boletos disponibles' === $xml->MENSAJE->__toString()) {
			throw new NotEnoughSeatsException($xml->MENSAJE);
		}

    throw new RequestMsgException($xml->MENSAJE);
	}


  /**
   * @inherit
   */
  public function getSuccessStatsKeys(\Exception $e = NULL) {
    if ($e instanceof PaymentException) {
      return ['ws_fails_cc'];
    }

		if ($e instanceof IEPaymentException) {
			return ['ws_fails_ie'];
		}

		if ($e instanceof NotEnoughSeatsException) {
			return ['ws_fails_neseats'];
		}

    $keys = parent::getSuccessStatsKeys($e);
    $keys[] = 'ws_fails_buy';
    $keys[] = 'ws_fails_buy_' . $this->args['CodigoCine'];

    return $keys;
  }
	
}