<?php $view->extend('SocialSnackFrontBundle:3d:base.html.php'); ?>

<?php $view['slots']->start('section_body'); ?>
  <div id="billboard-movies">
  <?php
  $movies_per_page = 18;
  $movies_cols     = 6;

  echo $view->render('SocialSnackFrontBundle:Partials:bodyMovies.html.php', [
    'movies'          => array_slice( $movies, 0, $movies_per_page ),
    '_movies'         => $_movies,
    'movies_per_page' => $movies_per_page,
    'movies_cols'     => $movies_cols,
  ]);
  ?>
  </div><!-- #billboard-movies -->
<?php $view['slots']->stop(); ?>
