<?php

namespace SocialSnack\WsBundle\Exception\MovieGroup;

/**
 * Class GrouperNotFoundException
 * @package SocialSnack\WsBundle\Exception\MovieGroup
 * @author Guido Kritz
 */
class GrouperNotFoundException extends \Exception {

} 