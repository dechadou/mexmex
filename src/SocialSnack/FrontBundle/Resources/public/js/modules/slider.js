jQuery.fn.slider = function( args ) {
	var	$slider							=	$( this ),
      obj;

   if ( ( obj = $slider.data( '_slider' ) ) && ( 'string' == typeof args ) ) {
     obj[args]();
     return;
   }

	if ( !args )
		args = {};

	var $window							=	$slider.find( '.slider-slider' )
		,	$slides							=	$window.children( '.slide' )
		,	$nav								=	$slider.find( '.slider-nav a' )
		,	$navdots						=	$slider.find( '.slider-dots' )
		,	count								=	$slides.length
		,	current							=	1
		,	slide_width					=	args.slide_width || $slides.outerWidth()
		,	total_width					= slide_width * count
		,	visible_width				=	$slider.width()
		,	timer								=	false
		,	interval						=	args.interval || 4000
		,	right_offset				=	args.right_offset || 0
		,	rotate							=	'undefined' != typeof args.rotate ? args.rotate : true
		,	stop_on_mouse_over	=	true
		,	ui_enabled					=	true;


	if ( rotate && stop_on_mouse_over ) {
		$slider.bind( 'mouseenter', function( e ) {
			stop_timer();
		} ).bind( 'mouseleave', function( e ) {
			start_timer();
		} );
	}

//	$( window ).load( function() {
//		$slides.each( function() {
//			var	$div	=	$( this ).find( '.title > div' )
//				,	$a		=	$div.find( 'a' );
//			if ( $a.width() > $div.width() ) {
//				$div.css( { maxWidth : $a.width() } );
//			}
//		} );
//	} );

	var max_height = 0;
	$slides.each( function() {
		var $t	= $( this ),
				h		=	$t.outerHeight();

		$t.css( { left : ($t.index() * 100) + '%' } );
		if ( h > max_height )
			max_height = h;
	} );

	$window.css( { /*width : total_width,*/ height : max_height, position : 'relative', zIndex : 0 } );
	$slides.css( { position : 'absolute' } );

	function slide( d, freeze ) {
		if ( !ui_enabled )
			return;

		ui_enabled = false;

		current += d;
		if ( current < 1 )
			current = count;
		if ( current > count )
			current = 1;

//		$window.stop().animate( { 'marginLeft' : ( current - 1 ) * slide_width * -1 } );

		var visible = [], $hide, visible_count = right_offset + 1, last_left;

		visible.push( $slides.eq( current - 1 ) );
		while ( visible.length < visible_count && count >= visible_count ) {
			var s = visible[visible.length-1].next();
			if ( !s.length ) {
				s = $slides.eq( 0 );
			}
			visible.push( s );
		}

		if ( d > 0 ) {
			visible[visible.length-1].css( { left : '100%' } );

			$hide = visible[0].prev();
			if ( !$hide.length ) {
				$hide = $slides.last();
			}
			visible.unshift( $hide );
			last_left = 0;
			d = 1;
		} else {
			visible[0].css( { left : '-100%' } );

			$hide = visible[visible.length-1].next();
			if ( !$hide.length ) {
				$hide = $slides.first();
			}
			visible.push( $hide );
			last_left = '-100%';
			d = -1;
		}

		$.each( visible, function( i, $slide ) {
			$slide.stop().animate( { left : (last_left + ( 100 * -1 * d )) + '%' }, { complete : i == 0 ? complete_cb : null } );
			last_left += 100;
		} );

		highlight_dots();

    if ( args.onslide ) {
      args.onslide( $(''), $slides.eq( current - 1 ) );
    }

		if ( !freeze )
			start_timer();
	}

	function complete_cb() {
		ui_enabled = true;
	}

	function stop_timer() {
		clearTimeout( timer );
		timer = false;
	}

	function start_timer() {
		if ( timer )
			stop_timer();
		if ( !rotate )
			return;
		timer = setTimeout( function() {
			slide( 1 );
		}, interval );
	}

	if ( count <= right_offset + 1 )
		$nav.remove();

	$nav.bind( 'click', function( e ) {
		e.preventDefault();
		if ( $( this ).hasClass( 'arrow-left' ) ) {
			slide( -1 );
		} else {
			slide( 1 );
		}
	} );


	if ( $navdots.length && count > 1 ) {
		if ($navdots.find('a').size() === 0) {
			for ( var i = 1 ; i <= count ; i++ ) {
				$( '<a/>', { href : '#', text : i } ).appendTo( $navdots );
			}
		}

		$navdots.delegate( 'a', 'click', function( e ) {
			e.preventDefault();

			if ( !ui_enabled )
				return;
			if ($(this).hasClass('selected')) {
				return;
			}

			var old = current;
			current = $( this ).index() + 1;
			if ( current > old ) {
				$slides.eq( old - 1 ).stop().animate( { left : '-100%' } );
				$slides.eq( current - 1 ).stop().css( { left : '100%' } ).animate( { left : 0 } );
			} else {
				$slides.eq( old - 1 ).stop().animate( { left : '100%' } );
				$slides.eq( current - 1 ).stop().css( { left : '-100%' } ).animate( { left : 0 } );
			}

			highlight_dots();

      if ( args.onslide ) {
        args.onslide( $slides.eq( old - 1 ), $slides.eq( current - 1 ) );
      }
		} );

		$navdots.children().eq( current - 1 ).addClass( 'selected' );
	}

	function highlight_dots() {
			$navdots.children().removeClass( 'selected' );
			$navdots.children().eq( current - 1 ).addClass( 'selected' );
	}

	if ( rotate )
		start_timer();


  $slider.data( '_slider', {
    stop  : function() {
      rotate = false;
      stop_timer();
    },
    start : function() {
      rotate = true;
      start_timer();
    }
  } );
};
