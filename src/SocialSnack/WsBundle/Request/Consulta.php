<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class Consulta extends ConsultaAbstract {

	const     TIPOCONSULTA_PELICULAS          = 1;
	const     TIPOCONSULTA_SESIONES_OLD       = 2;
	const     TIPOCONSULTA_PRECIOS            = 3;
	const     TIPOCONSULTA_CINES              = 4;
	const     TIPOCONSULTA_ESTADOS_ZONAS      = 5;
	const     TIPOCONSULTA_PUBLICIDAD         = 6;
	const     TIPOCONSULTA_FECHAS_DISPONIBLES = 7;
	const     TIPOCONSULTA_FECHAS_SESIONES    = 8;
	const     TIPOCONSULTA_SESIONES           = 9;
	
	protected $method       = 'Consulta';
	
	protected $default_args = array(
			'CodigoCine'        => 0,
			'CodigoEstado'      => 0,
			'CodigoZona'        => 0,
			'Dia'               => 0,
	);
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws \Exception
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
	 * @param \SimpleXMLElement $xml;
   * @return \SimpleXMLElement
   */
	public function parse_res( $xml ) {
		$node = '';
		switch ( $this->args[ 'TipoConsulta' ] ) {
			case self::TIPOCONSULTA_PELICULAS:
				$node = 'PELICULA';
				break;
			case self::TIPOCONSULTA_SESIONES:
				$node = 'SESION';
				break;
			case self::TIPOCONSULTA_PRECIOS:
				$node = 'PRECIO';
				break;
			case self::TIPOCONSULTA_ESTADOS_ZONAS:
				$node = 'ESTADOSYZONAS';
				break;
		}
		
		if ( !$node ) {
			throw new \Exception( 'Invalid parameter TipoConsulta.' );
		}
		
		if ( !isset( $xml->{$node} ) ) {
			if ( $xml->MENSAJE ) {
				throw new RequestMsgException( $xml->MENSAJE );
      } else {
				throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
      }
    }
		
		return $xml->{$node};
	}
}