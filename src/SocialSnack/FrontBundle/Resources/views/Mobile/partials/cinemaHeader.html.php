<div class="sidebar-heading">
  <?php if ($cinema) { ?>
  <a href="<?php echo $view['router']->generate('mobile_cinema', array('cinema_id' => $cinema->getId())); ?>" class="icon icon-star pad-left sidebar-title"><?php echo $cinema->getName(); ?></a>
  <?php } else { ?>
  <a href="<?php echo $view['router']->generate('mobile_location'); ?>" class="icon icon-star pad-left sidebar-title">Selecciona un cine</a>
  <?php } ?>
  <a href="<?php echo $view['router']->generate('mobile_location'); ?>" class="switch-cinema icon icon-darr no-txt" rel="ask_location">Selecciona otro cine</a>
</div>