<?php if ( !$single ) { ?>
<div class="sidebar-heading">
  <a href="<?php echo $view['fronthelper']->get_cinema_link( $cinema ); ?>" class="icon icon-star pad-left sidebar-title"><?php echo $cinema->getName(); ?></a>
  <a href="#" class="btn btn-default-alt switch-cinema btn-icon icon icon-rarr btn-icon-right" rel="ask_location">Selecciona otro cine</a>
</div>
<?php } ?>
<div class="sidebar-filters">
  <div class="msg">
    <p><strong>Estimado Invitado</strong></p>
    <p>
      Queremos informarte que por el momento la compra en línea para Cinemex Santa Fe no está disponible.
      Te invitamos a contactar a nuestro call center (<strong>5257-6969</strong> o marcando <strong>*CINE</strong> desde tu celular)
      para poder realizar tu compra o bien adquirir tus boletos directamente <strong>en la taquilla del cine</strong>.
    </p>
    <p>
      Estamos trabajando para restablecer a la brevedad el servicio y ofrecerte la mejor experiencia en tu compra
    </p>
  </div>
</div>