<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Aviso de privacidad integro para promociones, trivias y concursos - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Aviso de privacidad integro para promociones, trivias y concursos</h1>

<div class="entry-body align-justify">
	<p>Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>

	<h3>I. DATOS Y SU FINALIDAD:</h3>

	<p>Los Datos Personales que podrán solicitarle para la promoción, son: Nombre completo, teléfonos de contacto, dirección de correo electrónico, dirección de su domicilio y usuarios de redes sociales. La finalidad primaria de contar con estos datos es  la siguiente:</p>

	<p>•  Participación en promoción, trivia o concurso, y en su caso, entrega de premios y publicación de ganadores; </p>

	<p>Asimismo, podremos tratar sus Datos Personales con las siguientes finalidades secundarias:</p>

	<ul>
		<li>Proporcionarle información de eventos, boletines informativos y/o cartelera “Cinemex” por medios electrónicos, auditivos y/o mensajes de texto; </li>
		<li>Participación en trivias, y en su caso, entrega de premios y publicación de ganadores; </li>
		<li>Informarle y ofrecerle promociones y publicidad de “Cinemex”, de patrocinadores y socios comerciales;</li>
		<li>Llenado de encuestas; y</li>
		<li>Contactarlo en fechas especiales.</li>
	</ul>
		
	<p>En caso de no estar de acuerdo con nuestras finalidades secundarias, será necesario que nos los haga saber por medio de la Forma de Derechos ARCO que está en la siguiente página de internet: </p>
	<p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>

	<h3>II. LÍMITE AL USO O DIVULGACIÓN DE LOS DATOS:</h3>

	<p>Los Datos Personales proporcionados a “Cinemex”, serán resguardados y administrados en términos de lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad (en adelante y conjuntamente “la Legislación”), en tal sentido, nos comprometemos a salvaguardar tal información bajo los principios de lealtad y responsabilidad. La información obtenida se encuentra protegida por medidas de seguridad físicas, tecnológicas y administrativas, apegadas a normas, que buscan salvaguardar los datos en términos de lo dispuesto en “la Legislación”.</p>

	<p>Los Datos Personales que nos ha proporcionado, serán conservados por un periodo de 1 año en medios físicos o electrónicos (tomando en consideración aspectos administrativos, contables, fiscales y jurídicos), posteriormente serán descartados.</p>

	<h3>III. MEDIOS PARA EJERCER LOS DERECHOS “ARCO” Y REVOCACIÓN DEL CONSENTIMIENTO:</h3>

	<p>Podrá ejercer sus derechos de acceso, rectificación, cancelación y oposición contenidos en “la Legislación”, únicamente mediante el llenado del formato correspondiente que podrá obtener de la página de internet:</p>
	<p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>

	<p>Una vez presentada su solicitud en el formato prestablecido, “Cinemex” podrá solicitarle en un periodo no mayor a 5 días hábiles, la información y/o documentación necesaria para su seguimiento, así como para la acreditación de su identidad, de acuerdo a los términos que marca la Legislación. Por lo que contará con 10 días hábiles posteriores a su recepción, para atender este requerimiento. De lo contrario su solicitud se tendrá por no presentada.</p>

	<p>Asimismo, en un plazo posterior de 20 días hábiles “Cinemex” emitirá una resolución, la cual le será notificada por los medios de contacto que haya establecido en su solicitud. Una vez emitida la resolución y en caso de que la misma sea procedente (parcial o totalmente), “Cinemex” contará con 15 días hábiles para adoptar dicha resolución. </p>

	<p>Los términos y plazos indicados en los párrafos anteriores, podrán ser ampliados una sola vez en caso de ser necesario y se le deberá notificar a través de los medios de contacto que haya establecido. </p>

	<p>La revocación y el ejercicio de los Derechos ARCO serán gratuitos, debiendo usted cubrir únicamente los gastos justificados de envío, o el costo de reproducción en copias u otros formatos establecidos en su solicitud.</p>

	<p>Asimismo, le informamos que le asiste su derecho para acudir ante el Instituto Federal de Acceso a la Información y Protección de Datos en caso de considerar que su derecho a la protección de Datos Personales ha sido vulnerado. Para mayor información visite www.ifai.org.mx</p>

	<h3>IV. TRANSFERENCIA DE DATOS</h3>

	<p>“Cinemex”, se abstendrá de vender, arrendar o alquilar sus Datos Personales con un tercero, pero por razones de operación  y servicio serán transferidos únicamente con Empresas:</p>

	<p>•  Asociadas a la promoción, trivia o concurso de la que es participe; y </p>
	<p>•  De investigaciones de mercado para hacer evaluaciones.</p>

	<p>Estas empresas se encuentran obligadas a mantener la confidencialidad de la información proporcionada  y a no utilizarla de manera diversa a la establecida en el presente Aviso de Privacidad. Se entiende que consientes la transferencia de tus datos.</p>

	<h3>V. CAMBIOS AL AVISO DE PRIVACIDAD Y VULNERACIÓN DE SEGURIDAD:</h3>

	<p>“Cinemex” tiene el derecho de efectuar cualquier cambio o modificación al Aviso de Privacidad. Tales modificaciones podrán ser consultadas en:</p>
	<p>www.cinemex.com/privacidadpromociones</p>

	<p>En caso de que Cinemex se percate de alguna vulneración de seguridad en cualquier fase del tratamiento de sus Datos Personales, “Cinemex” a través de la figura del responsable lo hará del conocimiento general, a través de la siguiente página de internet:</p>
	<p> www.cinemex.com/privacidadpromociones, para que tome las medidas correspondientes a fin de resguardar sus derechos.</p>

	<p>Fecha de última actualización: 5/Enero/2015</p>
</div>
<?php $view['slots']->stop(); ?>