<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
  <link rel="alternate" href="cinemex://com.cinemex/cinema/<?php echo $cinema->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('body'); ?>
<?php
echo $this->render( 'SocialSnackFrontBundle:Mobile:partials/cinemaHeader.html.php', array(
    'cinema'          => $cinema,
) );
?>
<div id="gmap" style="width:100%;height:240px" data-zoom="15">
</div>
<?php
$address = $cinema->getData('DIRECCION');
$phone   = $cinema->getData('TELEFONOS');
?>
<?php if ( $address || $phone ) { ?>
<div class="box1 content-text">
  <?php if ( $address && is_string($address) ) { ?>
  <p>
    <strong>Dirección:</strong> <?php echo $address; ?>
  </p>
  <?php } ?>
  <?php if ( $phone && is_string($phone) ) { ?>
  <p>
    <strong>Teléfono:</strong> <?php echo $phone; ?>
  </p>
  <?php } ?>
</div>
<?php } ?>

<div class="mar1">
  <a href="<?php echo $view['router']->generate('mobile_billboard', array('cinema_id' => $cinema->getId())); ?>" class="btn btn-default btn100 align-center">Ver cartelera</a>
</div>

<?php if (isset($cinema_platinum) AND $cinema_platinum !== NULL) : ?>
<p class="sidebar-cinema-link mar1"><a class="btn btn-icon icon icon-rarr btn-icon-right btn100 align-center" href="<?php echo $view['router']->generate('mobile_cinema', array('cinema_id' => $cinema_platinum->getId())); ?>"><i class="icon icon-platinum"></i> Ver Horarios en Platino</a></p>
<?php endif; ?>

<?php if (isset($cinema_standard) AND $cinema_standard !== NULL) : ?>
<p class="sidebar-cinema-link mar1"><a class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn100 align-center" href="<?php echo $view['router']->generate('mobile_cinema', array('cinema_id' => $cinema_standard->getId())); ?>"><i class="icon icon-cinemex"></i> Ver Horarios en Tradicional</a></p>
<?php endif; ?>

<script>
	var places = [{id : <?php echo $cinema->getId(); ?>, lat : <?php echo $cinema->getLat(); ?>, lng : <?php echo $cinema->getLng(); ?>}];
  var marker_img = '<?php echo $view['assets']->getUrl('assets/img/gmaps-marker.png'); ?>';
</script>
<?php $view['slots']->stop(); ?>