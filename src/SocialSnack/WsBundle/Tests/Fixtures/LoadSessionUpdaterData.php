<?php

namespace SocialSnack\WsBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\RestBundle\Entity\App;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;

class LoadSessionUpdaterData implements FixtureInterface {

  static $sessions;
  static $movies;

  public function load(ObjectManager $manager) {
    static::$sessions = [];

    $state = new State();
    $state
        ->setName('State A')
        ->setLegacyName('STATE A')
        ->setLegacyId(1)
    ;
    $manager->persist($state);

    $area = new StateArea();
    $area
        ->setName('Area A')
        ->setLegacyName('AREA A')
        ->setLegacyId(1)
        ->setState($state)
        ->setLegacyStateId($state->getLegacyId())
    ;
    $manager->persist($area);

    $cinema = new Cinema();
    $cinema
        ->setName('Cinema A')
        ->setLegacyName('CINEMA A')
        ->setState($state)
        ->setArea($area)
        ->setInfo(json_encode(array()))
        ->setPlatinum(FALSE)
        ->setLat(0)
        ->setLng(0)
        ->setLegacyId(1)
    ;
    $manager->persist($cinema);

    $movie = new Movie();
    $movie
        ->setGroupId('ng')
        ->setInfo(json_encode(array()))
        ->setName('Movie A')
        ->setShortName('M. A')
        ->setPremiere(FALSE)
        ->setActive(TRUE)
        ->setPosterUrl('')
        ->setLegacyIdBis('TEST1')
        ->setLegacyId(1)
    ;
    $manager->persist($movie);
    static::$movies[] = $movie;

    $movie = clone $movie;
    $movie
        ->setLegacyId(NULL)
        ->setAttr('cx', TRUE)
    ;
    static::$movies[] = $movie;

    $movie = new Movie();
    $movie
        ->setGroupId('ng')
        ->setInfo(json_encode(array()))
        ->setName('Movie A')
        ->setShortName('M. A')
        ->setPremiere(FALSE)
        ->setActive(TRUE)
        ->setPosterUrl('')
        ->setLegacyIdBis('TEST2')
    ;
    $manager->persist($movie);
    static::$movies[] = $movie;

    $movie = new Movie();
    $movie
        ->setGroupId('ng')
        ->setInfo(json_encode(array()))
        ->setName('Movie A')
        ->setShortName('M. A')
        ->setPremiere(FALSE)
        ->setActive(TRUE)
        ->setPosterUrl('')
        ->setLegacyIdBis('TEST3')
    ;
    $manager->persist($movie);
    static::$movies[] = $movie;

    $session = new Session();
    $session
        ->setActive(TRUE)
        ->setDateTime(new \DateTime('now'))
        ->setTz('America\Tijuana')
        ->setLegacyId(1)
        ->setPlatinum(FALSE)
        ->setPremium(FALSE)
        ->setExtreme(FALSE)
        ->setLegacyCinemaId($cinema->getLegacyId())
        ->setCinema($cinema)
        ->setInfo(json_encode(array()))
        ->setGroupId('ng')
        ->setMovie($movie)
    ;
    $manager->persist($session);
    static::$sessions[] = $session;

    $manager->flush();
  }
} 