<?php

namespace SocialSnack\WsBundle\Request;

use Buzz\Browser;
use SocialSnack\WsBundle\Exception\InvalidIECodeException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class ConsultaIEC extends ConsultaIeAbstract {
	
	
	const     TIPOCONSULTA_INFO         = 1;
	const     TIPOCONSULTA_TRANSACTIONS = 2;
	
	protected $method       = 'ConsultaIEC';
	
	protected $default_args = array(
      'TipoConsulta' => NULL,
      'ClaveIEC'     => NULL,
			'CP'           => '',
			'Calle'        => '',
			'Colonia'      => '',
			'Municipio'    => '',
			'Ciudad'       => '',
			'Estado'       => '',
			'TelCasa'      => '',
			'TelCelular'   => '',
			'Sexo'         => '',
			'Email'        => '',
			'FechaNac'     => '',
			'CodigoNetMX'  => '',
	);


	protected function _parse_res( $xml ) {
		if ( !$xml || !isset( $xml->XMLFORMAT ) )
			throw new \Exception( 'Invalid response 0: ' . json_encode( $xml ) );

    if ( 'LMSERROR' == $xml->XMLFORMAT && isset( $xml->RESPUESTA->ERROR ) && (
            '-10111' == $xml->RESPUESTA->ERROR->ERRORCODIGO
            ||
            '-50100' == $xml->RESPUESTA->ERROR->ERRORCODIGO
        ) ) {
      throw new InvalidIECodeException();
    }

    switch ($this->args['TipoConsulta']) {
      case self::TIPOCONSULTA_INFO:
        if ('ConsultaIEC' == $xml->XMLFORMAT && isset($xml->RESPUESTA)) {
          return $this->parse_res( $xml->RESPUESTA );
        }
        break;

      case self::TIPOCONSULTA_TRANSACTIONS:
        if ('MovimientosIEC' == $xml->XMLFORMAT && isset($xml->RESPUESTA) && isset($xml->RESPUESTA->TRANSACCION) ) {
          return $this->parse_res( $xml->RESPUESTA->xpath('TRANSACCION') );
        }
        break;
    }

		throw new UnexpectedWsResponseException();
	}
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 * 
	 * @param \SimpleXMLElement $xml;
   * @return \SimpleXMLElement
	 */
	public function parse_res( $xml ) {
		return $xml;
	}
}
