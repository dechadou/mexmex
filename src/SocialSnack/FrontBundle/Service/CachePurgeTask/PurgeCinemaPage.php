<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use SocialSnack\FrontBundle\Service\Utils as FrontUtils;
use SocialSnack\WsBundle\Entity\Cinema;
use Symfony\Component\Routing\Router;

class PurgeCinemaPage implements UrlTaskInterface, DoctrineTaskInterface {

  protected $router;

  protected $frontUtils;

  protected $cinema;

  public function __construct(Router $router, FrontUtils $frontUtils) {
    $this->router     = $router;
    $this->frontUtils = $frontUtils;
  }


  public function setCinema(Cinema $cinema) {
    $this->cinema = $cinema;

    return $this;
  }


  public function getUrlResources() {
    return [
        [
            'path' => $this->frontUtils->get_permalink($this->cinema),
            'env'  => 'front'
        ],
        [
            'path' => $this->router->generate('sidebar_cinema', ['cinema_id' => $this->cinema->getId()]),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\WsBundle\Entity\MovieRepository::findMoviesInCinema:' . $this->cinema->getId(),
                'SocialSnack\WsBundle\Entity\SessionRepository::_findAvailableDates:' . $this->cinema->getId() . ',,',
            ],
            'env' => 'front'
        ]
    ];
  }

}