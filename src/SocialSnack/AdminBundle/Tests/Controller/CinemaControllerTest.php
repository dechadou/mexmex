<?php

namespace SocialSnack\AdminBundle\Tests\Controller;

/**
 * Class CinemaControllerTest
 * @package SocialSnack\AdminBundle\Tests\Controller
 * @author Guido Kritz
 */
class CinemaControllerTest extends WebTestCase {

  public function testDownloadIdsCsvAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'POST',
        '/missisippi/cinema_list_download',
        ['descargar' => 1]
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertEquals('text/csv; charset=UTF-8', $client->getResponse()->headers->get('Content-type'));
  }

} 