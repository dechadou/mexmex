<?php
namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;


class CinemaDistanceUpdater extends WorkerAbstract {
  
  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
	protected $container;
	
	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	protected $doctrine;


  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
   */
	public function __construct($container, $doctrine) {
		$this->container = $container;
		$this->doctrine  = $doctrine;
	}
	
  
  public function execute($data) {
    try {
      $doctrine     = $this->doctrine;
      $em           = $doctrine->getManager();
      $count        = 0;

      $args = $data;

      $cinemas = $this->getCinemas($args);
      $this->removeRelationships($args, $cinemas);

      /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
      foreach ($cinemas as $cinema) {
        $this->consoleInfo(sprintf(
            'Cinema: %s (%s)',
            $cinema->getName(),
            $cinema->getLegacyId()
        ));

        $res = $this->getCloserCinemas($cinema);

        foreach ( $res as $row ) {
          $to_cinema = $em->getReference('\SocialSnack\WsBundle\Entity\Cinema', $row['id']);
          $this->addRelationship($cinema, $to_cinema, $row['distance']);
          $count++;
        }

        $em->flush();
      }

      $this->consoleInfo(sprintf('%d relationship(s) created.', $count));
      $this->consoleInfo('Done.');

    } catch ( \Exception $e ) {
			$this->setError($e->getMessage());
      $this->consoleInfo($e->getMessage());
			return FALSE;
		}
		
		return TRUE;
  }


  /**
   * Get all the cinemas involved in the operation.
   *
   * @param array $args
   * @return array
   */
  protected function getCinemas($args) {
    extract($args);

    $cinema_repo  = $this->doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );

    if (isset($cinema_id)) {
      // Update for a specific cinema: find close cinemas ids, remove relations for
      // those cinemas. Then recalculate for all those cinemas.
      $cinema     = $cinema_repo->find($cinema_id);
      $cinemas    = $cinema->getClosestCinemas();
      $cinemas[]  = $cinema;

    } else {
      // If no specific cinema provided, then empty the table and recalculate for
      // every cinema.
      $cinemas    = $cinema_repo->findAll(FALSE, FALSE);
    }

    return $cinemas;
  }


  /**
   * Remove existing distance relationships.
   *
   * @param array $args
   * @param array $cinemas
   * @return integer mixed
   */
  protected function removeRelationships($args, $cinemas) {
    extract($args);

    $conn        = $this->doctrine->getConnection();
    $empty_query = "DELETE FROM CinemaDistance";

    if (isset($cinema_id)) {
      // Single cinema.
      $cinemas_ids = array_map(function ($a) { return $a->getId(); }, $cinemas);
      $empty_query .= " WHERE from_cinema_id IN (" . implode(',', $cinemas_ids) . ")";
      $this->consoleInfo('Remove previous relationships from cinemas: ' . implode(', ', $cinemas_ids));

    } else {
      // All cinemas.
      $this->consoleInfo('Remove all previous relationships.');
    }

    $deleted = $conn->executeUpdate($empty_query);
    $this->consoleInfo(sprintf('%d records deleted', $deleted));

    return $deleted;
  }


  /**
   * Find closer cinemas for a given cinema.
   *
   * @param Cinema $cinema
   * @return array
   */
  protected function getCloserCinemas(Cinema $cinema) {
    $conn  = $this->doctrine->getConnection();
    $query = "SELECT id, distance FROM
        (
            SELECT
                id,
                area_id,
                ( 3959 * acos( cos( radians( :lat ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( :lng ) ) + sin( radians( :lat ) ) * sin( radians( lat ) ) ) ) AS distance
            FROM Cinema c
            ORDER BY distance ASC
        ) d
        WHERE distance < 10 OR area_id = :area_id
        ORDER BY distance;";
    $stmt  = $conn->prepare($query);

    $stmt->bindValue('lat', $cinema->getLat());
    $stmt->bindValue('lng', $cinema->getLng());
    $stmt->bindValue('area_id', $cinema->getArea()->getId());
    $stmt->execute();

    return $stmt->fetchAll();
  }


  /**
   * Create a new distance relationship between two cinemas.
   *
   * @param Cinema  $from_cinema
   * @param Cinema  $to_cinema
   * @param integer $distance
   * @return bool
   */
  protected function addRelationship(Cinema $from_cinema, Cinema $to_cinema, $distance) {
    // Do not create a relationship with the cinema itself.
    if ($from_cinema->getId() === $to_cinema->getId()) {
      return FALSE;
    }

    // Create new relationship.
    $d = new \SocialSnack\WsBundle\Entity\CinemaDistance();
    $d->setFromCinema($from_cinema);
    $d->setToCinema($to_cinema);
    $d->setDistance($distance);
    $this->doctrine->getManager()->persist($d);

    return TRUE;
  }
}