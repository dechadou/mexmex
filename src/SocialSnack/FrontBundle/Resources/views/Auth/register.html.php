<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Regístrate - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('user_register'); ?>">Registro</a></li>
</ul>

<div class="clearfix row">
	
	<div class="side-col col">
    <div class="content-box content-box-bg">
      <ul class="register-benefits">
        <li>Compra tus boletos en unos pocos clics.</li>
        <li>Fácil acceso al historial de compras.</li>
        <li>Recibe recomendaciones basadas en los gustos y calificaciones de tus amigos.</li>
        <li>Participa de promociones especiales, beneficios exclusivos y muchos beneficios más.</li>
      </ul>
    </div>
	</div>
	
	<div class="main-col col">
    <div id="register-process">
      <div class="content-box register-step selected" id="register-step-1">
        <h1 class="content-box-title">Ingresa con Facebook</h1>
        <p>
          Haz click en <strong>"Conectarme"</strong> para utilizar tu cuenta de Facebook para acceder a Cinemex.
        </p>
        <p class="align-center">
          <a href="#" class="btn btn-facebook btn-icon icon icon-facebook" id="fb-register-bt">Conectarme</a>
        </p>
        <p class="small">
          ¿No tienes cuenta de Facebook? <a href="#" rel="fb-register-skip">Haz click aquí para saltar este paso y crear tu cuenta de Cinemex manualmente</a>.
        </p>
      </div><!-- #register-step-1 -->
      
      <div class="content-box register-step" id="register-step-2">
        <h1 class="content-box-title">Completa tu registro</h1>
        <form action="" method="post" novalidate="novalidate" id="register-form">
          <div class="std-form clearfix">
            <div class="form-row half">
              <label for="register_first_name">Nombre</label>
              <input type="text" name="first_name" id="register_first_name" data-flyvalidate="1" data-validators="not_empty" />
            </div>
            <div class="form-row half">
              <label for="register_last_name">Apellido</label>
              <input type="text" name="last_name" id="register_last_name" data-flyvalidate="1" data-validators="not_empty" />
            </div>
            <div class="form-row half">
              <label for="register_email">Email</label>
              <input type="email" name="email" id="register_email" data-flyvalidate="1" data-validators="not_empty email" />
            </div>
            <div class="form-row half">
              <label for="register_password">Contraseña</label>
              <input type="password" name="password" id="register_password" data-flyvalidate="1" data-validators="not_empty" />
            </div>
            <div class="form-row half">
              <label for="register_terms">
                <input type="checkbox" name="terms" id="register_terms" data-validators="checked" />
                Acepto los <a href="<?php echo $view['router']->generate('tos_page'); ?>" target="_blank">términos y condiciones</a>
              </label>
            </div>
          </div>
          <div class="btn-row">
            <input type="hidden" name="fbsigned_request" id="register_fbsigned_request" />
            <input type="hidden" name="from_frontend" value="1" />
            <button type="submit" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right">Continuar</button>
          </div>
        </form>
      </div><!-- #register-step-2 -->
      
      <div class="content-box register-step" id="register-step-3">
        <h1 class="content-box-title">Invitado especial</h1>
        <span class="heading-title">
          <span class="discicon icon-star icon-red"></span>
          ¿Ya eres Invitado Especial Cinemex?
        </span>
        
        <form action="" method="post" id="register-ie-form">
          <?php echo $this->render( 'SocialSnackFrontBundle:Partials:iecForm.html.php' ); ?>
        </form>
        
        <div class="btn-row">
          <a href="<?php echo $end_url; ?>" class="btn btn-default btn-icon icon icon-rarr btn-icon-right btn-right" id="register-end-bt">Continuar</a>
        </div>
      </div><!-- #register-step-3 -->
      
    </div><!-- #register-process -->
    
    <div class="disclaimer">
      <p>Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>

      <p>Los Datos Personales que le son solicitados, serán tratados con las siguientes finalidades primarias:</p>

      <ul>
        <li>Realizar la Compra de boletos de cine por internet o a través de nuestra aplicación.</li>
        <li>Recibir la cartelera en el correo electrónico que nos proporciona;</li>
        <li>Invitarlo a participar en nuestros eventos; y </li>
        <li>Enviarle promociones de “Cinemex”.</li>
      </ul>

      <p>Asimismo, podremos tratar sus Datos Personales con las siguientes finalidades:</p>

      <ul>
        <li>Enviarle promociones de nuestros socios comerciales; y </li>
        <li>Envío de noticias relacionadas con el mundo del cine.</li>
      </ul>

      <p>En caso de no estar de acuerdo con nuestras finalidades secundarias, marque con una “X” en el siguiente recuadro: <input type="checkbox" id="privacy-check" /></p>

      <p><a href="<?php echo $view['router']->generate('privacy_page'); ?>">Aviso de Privacidad Integro</a></p>

      <p>Manifiesto mi consentimiento para el tratamiento de mis Datos Personales, de conformidad con lo previsto en el presente Aviso de Privacidad.</p>

      <p>Fecha de última actualización: 5/Enero/2015</p>

    </div>
	</div>
</div>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start( 'js_after' ); ?>
<script>
  var no_location = true;
  var redirect_after_login = true;
  var match = document.location.hash.match(/\bredirect_uri=\/(.+)/);
  if (match) {
    $('#register-end-bt').attr('href', match[1]);
  }
</script>
<?php $view['slots']->stop(); ?>