<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadSplashesData;

class SplashscreenControllerTest extends WebTestCase {

  protected function getEntityModel() {
    return [
        'id'        => 'integer',
        'url_image' => 'string',
        'deep_link' => 'string',
    ];
  }


  protected function _test($url, $model) {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSplashesData',
    ]);

    $client = static::createClient();
    $client->request(
        'GET',
        is_string($url) ? $url : $url()
    );

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api
   */
  public function testDefault() {
    $model = [
        [
            'type' => 'array',
            'each' => $this->getEntityModel()
        ]
    ];
    $this->_test('/rest/splashscreen/', $model);
  }


  /**
   * @group functional
   * @group api
   */
  public function testDetails() {
    $this->_test(
        function() { return sprintf('/rest/splashscreen/%d', LoadSplashesData::$splashes[0]->getId()); },
        $this->getEntityModel()
    );
  }

}