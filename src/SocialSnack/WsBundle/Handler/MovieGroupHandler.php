<?php

namespace SocialSnack\WsBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use SocialSnack\RestBundle\Entity\TrackingCode;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Exception\MovieGroup\GrouperFromGrouperException;
use SocialSnack\WsBundle\Exception\MovieGroup\GrouperNotFoundException;
use SocialSnack\WsBundle\Exception\MovieGroup\MovieAlreadyGroupedException;
use SocialSnack\WsBundle\Exception\MovieGroup\NoGroupIdException;
use SocialSnack\WsBundle\Exception\MovieGroup\PreviousGroupFoundException;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MovieGroupHandler
 * @package SocialSnack\WsBundle\Handler
 * @author Guido Kritz
 */
class MovieGroupHandler {

  protected $container;

  protected $manager;

  protected $repository;


  public function __construct(ContainerInterface $container, ObjectManager $manager) {
    $this->container = $container;
    $this->manager = $manager;
  }


  protected function getRepository() {
    return $this->manager->getRepository('SocialSnackWsBundle:Movie');
  }


  protected function getManager() {
    return $this->manager;
  }


  public function createGrouper($movies) {
    if (!is_array($movies)) {
      $movies = [$movies];
    }

    if (!sizeof($movies)) {
      throw new \Exception('Invalid argument $movies provided.');
    }

    $name = '';
    $active = 0;
    $premiere = 0;

    $grouper = new Movie;

    /** @var \SocialSnack\WsBundle\Entity\Movie $movie */
    foreach ($movies as $movie) {
      if ('grouper' === $movie->getType()) {
        throw new GrouperFromGrouperException();
      }

      if (is_null($movie->getGroupId())) {
        throw new NoGroupIdException();
      }

      // Get the movie title from its children's titles.
      if (!$name) {
        $name = $movie->getName();
      } else {
        $name = WsHelper::get_longest_common_subsequence($name, $movie->getName());
      }

      // Assign attributes.
      foreach ($movie->getAttr() as $k => $v) {
        $grouper->setAttr($k, $v);
      }

      // If at least one child is active, make the grouper active.
      $active  |= $movie->getActive();
      $premiere|= $movie->getPremiere();
    }

    $grouper
        ->setActive((bool)$active)
        ->setGroupId($movie->getGroupId())
        ->setInfo($movie->getInfo())
        ->setLegacyId(NULL)
        ->setLegacyIdBis(NULL)
        ->setName(trim($name))
        ->setPosterUrl($movie->getPosterUrl())
        ->setPremiere((bool)$premiere)
        ->setShortName(trim($name))
        ->setType('grouper')
    ;

    return $grouper;
  }


  /**
   * Creates a new group from a Movie.
   * Creates the grouper entity and sets the associations.
   *
   * @param Movie $movie
   * @throws \SocialSnack\WsBundle\Exception\MovieGroup\PreviousGroupFoundException
   * @throws \SocialSnack\WsBundle\Exception\MovieGroup\MovieAlreadyGroupedException
   */
  public function createGroupFromMovie(Movie $movie) {
    $repo = $this->getRepository();
    $em = $this->getManager();

    // Make sure isn't already grouped.
    if ($movie->getParent()) {
      throw new MovieAlreadyGroupedException();
    }

    // Generate the new group ID and make sure it is available.
    $group_id = 'hg' . $movie->getId();
    $grouped = $this->getMoviesInGroup($group_id);
    if (sizeof($grouped)) {
      throw new PreviousGroupFoundException();
    }
    $movie->setGroupId($group_id);

    // Create grouper
    $grouper = $this->createGrouper($movie);

    // Set Movie parent
    $movie->setParent($grouper);

    // Persist, flush, etc.
    $em->persist($grouper);
    $em->flush();

    // Should always be called after flush!
    $this->updateSessionsGroupIds($movie);
  }


  /**
   * Adds a movie to a group.
   *
   * @param Movie $movie
   * @param $group_id
   * @throws \SocialSnack\WsBundle\Exception\MovieGroup\GrouperNotFoundException
   */
  public function addMovieToGroup(Movie $movie, $group_id) {
    $repo = $this->getRepository();
    $em = $this->getManager();

    // Find grouper
    $grouper = $this->getMoviesInGroup($group_id, 'grouper');
    if (!sizeof($grouper)) {
      throw new GrouperNotFoundException();
    }
    $grouper = $grouper[0];

    // Set Movie parent
    $movie
        ->setParent($grouper)
        ->setGroupId($grouper->getGroupId())
    ;

    // Persist, flush, etc.
    $em->persist($movie);
    $em->flush();

    // Should always be called after flush!
    $this->updateSessionsGroupIds($movie);
  }


  /**
   * Removes a movie from a group.
   * If no more movies belong to that group and $force_dissolve is set to TRUE (default)
   * then dissolve the group as well.
   *
   * @param Movie $movie
   * @param bool $force_dissolve
   */
  public function ungroupMovie(Movie $movie, $force_dissolve = TRUE) {
    $repo = $this->getRepository();
    $em = $this->getManager();

    $old_group_id = $movie->getGroupId();
    $group_id = 'ng' . $movie->getId();
    $movie
        ->setGroupId($group_id)
        ->setParent(NULL)
    ;

    // Persist, flush, etc.
    $em->persist($movie);
    $em->flush();

    $versions = $this->getMoviesInGroup($old_group_id, 'single');
    if (!sizeof($versions) && $force_dissolve) {
      $this->dissolveGroup($old_group_id);
    } else {
      // Should always be called after flush!
      $this->updateSessionsGroupIds($movie); // This is done by dissolveGroup() too, hence the else statement.
    }
  }


  /**
   * Dissolves a group.
   * Ungroups every movie version and removes the grouper entity.
   *
   * @param $group_id
   * @throws \SocialSnack\WsBundle\Exception\MovieGroup\GrouperNotFoundException
   */
  public function dissolveGroup($group_id) {
    $em = $this->getManager();

    $grouper_removed = FALSE;

    $movies = $this->getMoviesInGroup($group_id);
    foreach ($movies as $movie) {
      // Remove groupers.
      if ('grouper' === $movie->getType()) {
        $em->remove($movie);
        $grouper_removed = TRUE;
        continue;
      }

      // Ungroup versions.
      $movie
          ->setGroupId('ng' . $movie->getId())
          ->setParent(NULL)
      ;
    }

    if (!$grouper_removed) {
      throw new GrouperNotFoundException();
    }

    $em->flush();

    // Should always be called after flush!
    $this->updateSessionsGroupIds($movies);
  }


  /**
   * @param array $movies
   * @return bool
   */
  public function updateSessionsGroupIds($movies) {
    if (!is_array($movies)) {
      $movies = [$movies];
    }

    $movie_ids = array_filter(array_map(function($a) { return $a->getId(); }, $movies));
    $query = "UPDATE Session s INNER JOIN Movie m ON s.movie_id = m.id SET s.group_id = m.group_id WHERE m.id IN (" . implode(',', $movie_ids) . ")";
    /** @var \Doctrine\DBAL\Connection $conn */
    $conn = $this->getManager()->getConnection();
    $stmt = $conn->prepare($query);
    return $stmt->execute();
  }


  /**
   * @param string $group_id
   * @param null $type
   * @return array
   */
  protected function getMoviesInGroup($group_id, $type = NULL) {
    $qb = $this->getRepository()->createQueryBuilder('m');
    $qb
        ->select('m')
        ->andWhere('m.group_id = :group_id')
        ->setParameter('group_id', $group_id)
    ;

    if (!is_null($type)) {
      $qb
          ->andWhere('m.type = :type')
          ->setParameter('type', $type)
      ;
    }

    $q = $qb->getQuery();
    return $q->getResult();
  }


  public function assignTrackingCode(TrackingCode $code, $group_id) {
    $conn  = $this->manager->getConnection();
    $query = "INSERT INTO trackingcode_moviegroup (code_id, group_id) VALUES (:code_id, :group_id);";
    $stmt  = $conn->prepare($query);
    $stmt->bindValue('code_id',  $code->getId());
    $stmt->bindValue('group_id', $group_id);
    $stmt->execute();
  }


  public function removeTrackingCode(TrackingCode $code, $group_id) {
    $conn  = $this->manager->getConnection();
    $query = "DELETE FROM trackingcode_moviegroup WHERE code_id = :code_id AND group_id = :group_id;";
    $stmt  = $conn->prepare($query);
    $stmt->bindValue('code_id',  $code->getId());
    $stmt->bindValue('group_id', $group_id);
    $stmt->execute();
  }

} 