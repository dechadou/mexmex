<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\FrontBundle\Entity\User;

/**
 * Class LoadPurchasesData
 * @package SocialSnack\RestBundle\Tests\Fixtures
 * @author Guido Kritz
 */
class LoadPurchasesData implements FixtureInterface {

  static $transactions;

  public function load(ObjectManager $manager) {
    self::$transactions = [];

    $session = LoadSessionData::$sessions[0];
    $manager->persist($session);
    $manager->persist($session->getCinema());
    $manager->persist($session->getCinema()->getState());
    $manager->persist($session->getCinema()->getArea());
    $manager->persist($session->getMovie());

    $user = new User();
    $user
        ->setEmail('test@test.com')
        ->setFirstName('Homer')
        ->setLastName('Simpson')
        ->setPassword('123')
    ;
    $manager->persist($user);

    $t = new Transaction();
    $t
        ->setSession($session)
        ->setAmount(100)
        ->setCinema($session->getCinema())
        ->setCode('FOO BAR')
        ->setHash('foobar')
        ->setInfo(json_encode(array()))
        ->setMovie($session->getMovie())
        ->setPurchaseDate(new \DateTime())
        ->setTickets('a1')
        ->setSeats('A1')
        ->setRealSeats('A1')
        ->setQr('foobar.jpg')
        ->setTicketsCount(1)
    ;

    for ($i = 1 ; $i <= 10 ; $i++) {
      $t2 = clone $t;
      $t2->setUser($i % 2 ? $user : NULL);
      $manager->persist($t2);
      self::$transactions[] = $t2;
    }

    $manager->flush();
  }
} 