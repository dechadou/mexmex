<?php
$view->extend('SocialSnackFrontBundle:Ie:base.html.php');
$view['slots']->set('title', 'Invitado Especial - Nuestro Programa - Cinemex');
$view['slots']->start('iebody')
?>
<h1 class="content-box-title"><span class="icon icon-light icon-crown no-txt"></span> Nuestro Programa</h1>

<div class="entry-body">
  <p>
    Invitado Especial es el Programa de Lealtad de Cinemex que premia tu diversión, ya que te recompensa de manera inmediata.
    Para ofrecerte mayores beneficios, Invitado Especial se ha unido a PAYBACK, el programa de lealtad más grande de México.
  </p>

  <div class="row clearfix">
    <div class="special-box col col-sm-1-2 ie-program-benefits">
      <h2 class="special-box-title">Al inscribirte a Invitado Especial obtendrás:</h2>

      <div class="special-box-hr"></div>

      <div class="special-box-content" style="min-height: 400px;">
        <ul>
          <li class="two-lines"><i class="icon icon-red icon-tick2"></i> Acumulación en puntos<br>por tus consumos</li>
          <li><i class="red">M</i> Martes 2x1</li>
          <li><i class="icon icon-red icon-king"></i> Invitaciones a premieres</li>
          <li><i class="red">2x1</i> Boletos 2x1 y a precio especial</li>
          <li><i class="icon icon-red icon-gift"></i> Regalo el día de tu cumpleaños</li>
          <li><i class="icon icon-red icon-star"></i> Promociones exclusivas</li>
          <li><i class="icon icon-red icon-plus"></i> ¡Y mucho más!</li>
        </ul>
      </div>
    </div>

    <div class="special-box col col-sm-1-2 ie-program-how">
      <h2 class="special-box-title">Inscribirte es muy fácil<br>y es GRATIS</h2>

      <div class="special-box-hr"></div>

      <div class="special-box-content" style="min-height: 400px;">
        <p>Podrás hacerlo a través de:</p>
        <ul>
          <li class="icon icon-red icon-rarr">Centro de Atención al Invitado de tu complejo Cinemex favorito</li>
          <li class="icon icon-red icon-rarr"><a href="http://www.payback.mx" target="_blank" rel="nofollow">payback.mx</a></li>
        </ul>

        <p class="color-brand-main">Hazlo ahora y te regalaremos un boleto al 2x1 para que empieces a disfrutar de los beneficios que Invitado Especial Cinemex Payback trae para ti!</p>
      </div>
    </div>
  </div>

  <p class="ie-program-tos icon icon-dark icon-screen"><a href="<?php echo $view['router']->generate('ie_tos'); ?>">Términos y condiciones del Programa Invitado Especial Cinemex</a></p>
</div>
<?php $view['slots']->stop(); ?>
