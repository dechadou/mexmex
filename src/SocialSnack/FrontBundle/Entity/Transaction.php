<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="trans_date_idx", columns={"purchase_date"})
 * })
 * @ORM\Entity(repositoryClass="TransactionRepository")
 */
class Transaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
		
		/**
		 * @ORM\Column(name="purchase_date", type="datetime")
		 */
		private $purchase_date;
		
		/**
     * @var \SocialSnack\FrontBundle\Entity\User
     *
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\FrontBundle\Entity\User")
		 */
		private $user;
		
		/**
     * @var \SocialSnack\WsBundle\Entity\Session
     *
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Session")
		 */
		private $session;
		
		/**
     * @var \SocialSnack\WsBundle\Entity\Movie
     * 
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Movie")
		 */
		private $movie;
		
		/**
     * @var \SocialSnack\WsBundle\Entity\Cinema
     *
		 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Cinema")
		 */
		private $cinema;
		
		/**
		 * @ORM\Column(name="tickets", type="text")
		 */
		private $tickets;
    
    /**
     * @ORM\Column(name="seats", type="text")
     */
    private $seats;
    
    /**
     * @ORM\Column(name="real_seats", type="string", length=100)
     */
    private $real_seats;
		
		/**
		 * @ORM\Column(name="info", type="text")
		 */
		private $info;
    
    /**
     * @ORM\Column(name="code", type="text")
     */
    private $code;
    
    /**
     * @ORM\Column(name="qr", type="text", length=40)
     */
    private $qr;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tickets_count", type="integer")
     */
    private $tickets_count;
    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="text", length=255)
     */
    private $hash;
    
    /**
     * @var string
     *
     * @ORM\Column(name="app_id", type="string", length=100, nullable=true)
     */
    private $appId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="text", length=50, nullable=true)
     */
    private $ref;
    
		private $decoded_info  = null;
    
		
		public function __construct() {
      $this->hash = md5(uniqid(null, true));
			if ( !$this->purchase_date ) {
				$this->purchase_date = new \DateTime( 'now' );
			}
		}
		
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purchase_date
     *
     * @param \DateTime $purchaseDate
     * @return Transaction
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchase_date = $purchaseDate;
    
        return $this;
    }

    /**
     * Get purchase_date
     *
     * @return \DateTime 
     */
    public function getPurchaseDate()
    {
        return $this->purchase_date;
    }

    /**
     * Set tickets
     *
     * @param string $tickets
     * @return Transaction
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
    
        return $this;
    }

    /**
     * Get tickets
     *
     * @return array
     */
    public function getTickets()
    {
        if ( is_string( $this->tickets ) )
          return json_decode( $this->tickets );
        
        return $this->tickets;
    }

    /**
     * Set user
     *
     * @param \SocialSnack\FrontBundle\Entity\User $user
     * @return Transaction
     */
    public function setUser(\SocialSnack\FrontBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialSnack\FrontBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set session
     *
     * @param \SocialSnack\WsBundle\Entity\Session $session
     * @return Transaction
     */
    public function setSession(\SocialSnack\WsBundle\Entity\Session $session = null)
    {
        $this->session = $session;
    
        return $this;
    }

    /**
     * Get session
     *
     * @return \SocialSnack\WsBundle\Entity\Session 
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set movie
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $movie
     * @return Transaction
     */
    public function setMovie(\SocialSnack\WsBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;
    
        return $this;
    }

    /**
     * Get movie
     *
     * @return \SocialSnack\WsBundle\Entity\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinema
     * @return Transaction
     */
    public function setCinema(\SocialSnack\WsBundle\Entity\Cinema $cinema = null)
    {
        $this->cinema = $cinema;
    
        return $this;
    }

    /**
     * Get cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema 
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Transaction
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

		
		/**
		 * Get data from the WS movie object.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->info );
			}
      
			if ( !isset( $this->decoded_info->{ $key } ) )
				return NULL;
			
      return $this->decoded_info->{ $key };
		}
		
    
    /**
     * Set code
     *
     * @param string $code
     * @return Transaction
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set qr
     *
     * @param string $qr
     * @return Transaction
     */
    public function setQr($qr)
    {
        $this->qr = $qr;
    
        return $this;
    }

    /**
     * Get qr
     *
     * @return string 
     */
    public function getQr()
    {
        return $this->qr;
    }

    /**
     * Set seats
     *
     * @param string $seats
     * @return Transaction
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    
        return $this;
    }

    /**
     * Get seats
     *
     * @return string 
     */
    public function getSeats()
    {
        if ( is_string( $this->seats ) )
          return json_decode( $this->seats );
        
        return $this->seats;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Transaction
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set tickets_count
     *
     * @param integer $ticketsCount
     * @return Transaction
     */
    public function setTicketsCount($ticketsCount)
    {
        $this->tickets_count = $ticketsCount;
    
        return $this;
    }

    /**
     * Get tickets_count
     *
     * @return integer 
     */
    public function getTicketsCount()
    {
        return $this->tickets_count;
    }
    
    public function toArray($args = array()) {
      $iecode = $this->getData('NUMIE');

      $sessionDateWTZ = new \DateTime(
          $this->session->getDateTime()->format('Y-m-d H:i:s'),
          new \DateTimeZone($this->session->getTz())
      );

      $result = array(
          'id' => $this->id,
          'purchase_date' => $this->purchase_date->getTimestamp(),
          'confirmation_code' => $this->code,
          'movie' => array(
              'id' => $this->movie->getId(),
              'name' => $this->movie->getName(),
              'score' => $this->movie->getScore(),
              'cover' => $this->movie->getPosterUrl(),
              'info' => array(
                  'rating' => is_string($this->movie->getData( 'RATING' )) ? $this->movie->getData( 'RATING' ) : '',
              ),
          ),
          'cinema' => array(
              'id'   => $this->cinema->getId(),
              'name' => $this->cinema->getName(),
              'lat'  => $this->cinema->getLat(),
              'lng'  => $this->cinema->getLng(),
          ),
          'session' => array(
              'id'        => $this->session->getId(),
              'date'      => $sessionDateWTZ->getTimestamp(),
              'tz_offset' => $sessionDateWTZ->getOffset(),
          ),
          'tickets' => json_decode($this->tickets),
          'qr' => $this->qr,
          'total' => $this->amount,
          'seats' => explode(',', $this->real_seats),
          'iecode' => $iecode,
      );
      
      if (isset($args['user_id']) && $args['user_id']) {
        $result['user_id'] = $this->getUser()
            ? $this->getUser()->getId()
            : NULL
        ;
      }
      
      return $result;
    }
    

    /**
     * Set real_seats
     *
     * @param string $realSeats
     * @return Transaction
     */
    public function setRealSeats($realSeats)
    {
        $this->real_seats = $realSeats;
    
        return $this;
    }

    /**
     * Get real_seats
     *
     * @return string 
     */
    public function getRealSeats()
    {
        return $this->real_seats;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return Transaction
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    
        return $this;
    }

    /**
     * Get appId
     *
     * @return string 
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Transaction
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }
}