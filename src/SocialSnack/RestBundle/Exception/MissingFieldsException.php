<?php

namespace SocialSnack\RestBundle\Exception;

/**
 * Class MissingFieldsException
 * @package SocialSnack\RestBundle\Exception
 * @author Guido Kritz
 */
class MissingFieldsException extends RestException {

  /**
   * @param string $message
   * @param int $code
   * @param \Exception $previous
   */
  public function __construct($message = '', $code = 0, $previous = NULL) {
    parent::__construct('missing-fields', $message, 400, $code, $previous);
  }

} 