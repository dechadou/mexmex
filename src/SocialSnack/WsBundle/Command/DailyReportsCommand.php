<?php

namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class DailyReportsCommand extends ContainerAwareCommand {

  protected function configure() {
    $this
        ->setName('reports:daily:generate')
        ->setDescription('Generate daily reports')
        ->addOption('year', 'y', InputArgument::OPTIONAL, 'Year', date('Y'))
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $doctrine = $this->getContainer()->get('doctrine');
    $year = (int)$input->getOption('year');
    $start_year = 2013;
    $current_year = (int)date('Y');

    if ($year < $start_year || $year > $current_year) {
      throw new \Exception(sprintf('Invalid year %d', $year));
    }

    /* @var $qb \Doctrine\ORM\QueryBuilder */
    $qb = $doctrine->getManager()->createQueryBuilder();
    $qb->select('SUM(t.tickets_count) AS cnt', 't.appId')
        ->from('SocialSnackFrontBundle:Transaction', 't')
        ->groupBy('t.appId');

    if ($year) {
      $qb->andWhere('t.purchase_date >= :date_from')
          ->andWhere('t.purchase_date < :date_to')
          ->setParameter('date_from', $year . '-01-01 00:00:00')
          ->setParameter('date_to', ($year + 1) . '-01-01 00:00:00');
    }

    $qb_platinum = clone $qb;
    $qb_platinum->innerJoin('t.cinema', 'c')
        ->andWhere('c.platinum = 1');
    $q = $qb_platinum->getQuery();
    $res_platinum = $q->getResult();
    unset($qb_platinum);
    unset($q);

    $qb_premium = clone $qb;
    $qb_premium->innerJoin('t.session', 's')
        ->andWhere($qb->expr()->like('s.types', ':like'))
        ->setParameter('like', '%"premium"%');
    $q = $qb_premium->getQuery();
    $res_premium = $q->getResult();
    unset($qb_premium);
    unset($q);

    $qb_3d = clone $qb;
    $qb_3d->innerJoin('t.movie', 'm')
        ->andWhere($qb->expr()->like('m.attributes', ':like'))
        ->setParameter('like', '%"v3d":true%');
    $q = $qb_3d->getQuery();
    $res_3d = $q->getResult();
    unset($qb_3d);
    unset($q);

    $qb_4d = clone $qb;
    $qb_4d->innerJoin('t.movie', 'm')
        ->andWhere($qb->expr()->like('m.attributes', ':like'))
        ->setParameter('like', '%"v4d":true%');
    $q = $qb_4d->getQuery();
    $res_4d = $q->getResult();
    unset($qb_4d);
    unset($q);

    $qb_cx = clone $qb;
    $qb_cx->innerJoin('t.movie', 'm')
        ->andWhere($qb->expr()->like('m.attributes', ':like'))
        ->setParameter('like', '%"cx":true%');
    $q = $qb_cx->getQuery();
    $res_cx = $q->getResult();
    unset($qb_cx);
    unset($q);

    $qb_trad = clone $qb;
    $qb_trad->innerJoin('t.movie', 'm')
        ->innerJoin('t.cinema', 'c')
        ->innerJoin('t.session', 's')
        ->andWhere($qb->expr()->not($qb->expr()->like('m.attributes', ':like_3d')))
        ->setParameter('like_3d', '%"v3d":true%')
        ->andWhere($qb->expr()->not($qb->expr()->like('m.attributes', ':like_4d')))
        ->setParameter('like_4d', '%"v4d":true%')
        ->andWhere($qb->expr()->not($qb->expr()->like('m.attributes', ':like_cx')))
        ->setParameter('like_cx', '%"cx":true%')
        ->andWhere($qb->expr()->not($qb->expr()->like('s.types', ':like_premium')))
        ->setParameter('like_premium', '%"premium"%')
        ->andWhere('c.platinum = 0');
    $q = $qb_trad->getQuery();
    $res_trad = $q->getResult();
    unset($qb_trad);
    unset($q);

    $apps = $doctrine->getRepository('SocialSnackRestBundle:App')->findAll();

    $reduce = function($carry, $item) {
      $carry[$item['appId']] = $item['cnt'];
      return $carry;
    };

    $cnt_platinum = array_reduce($res_platinum, $reduce, array());
    $cnt_premium  = array_reduce($res_premium, $reduce, array());
    $cnt_3d       = array_reduce($res_3d, $reduce, array());
    $cnt_4d       = array_reduce($res_4d, $reduce, array());
    $cnt_cx       = array_reduce($res_cx, $reduce, array());
    $cnt_trad     = array_reduce($res_trad, $reduce, array());

    $totals = array(
        'trad'     => 0,
        'platinum' => 0,
        'premium'  => 0,
        '3d'       => 0,
        '4d'       => 0,
        'cx'       => 0,
    );

    $fs = new Filesystem();
    $path = $this->getContainer()->get('kernel')->getRootDir() . '/reports/txfxa/'; // Tickets by Format by App
    $fs->mkdir($path);
    $file = sprintf('%d.csv', $year);
    $fh = fopen($path . $file, 'w');
    fputcsv($fh, array('App','Tradicional','Platino','Premium','3D','X4D','CinemeXtremo','Total'));
    foreach ($apps as $app) {
      $values = array(
          isset($cnt_trad[$app->getAppId()])     ? $cnt_trad[$app->getAppId()]     : 0,
          isset($cnt_platinum[$app->getAppId()]) ? $cnt_platinum[$app->getAppId()] : 0,
          isset($cnt_premium[$app->getAppId()])  ? $cnt_premium[$app->getAppId()]  : 0,
          isset($cnt_3d[$app->getAppId()])       ? $cnt_3d[$app->getAppId()]       : 0,
          isset($cnt_4d[$app->getAppId()])       ? $cnt_4d[$app->getAppId()]       : 0,
          isset($cnt_cx[$app->getAppId()])       ? $cnt_cx[$app->getAppId()]       : 0,
      );
      $total = array_sum($values);

      $totals['trad']     += $values[0];
      $totals['platinum'] += $values[1];
      $totals['premium']  += $values[2];
      $totals['3d']       += $values[3];
      $totals['4d']       += $values[4];
      $totals['cx']       += $values[5];

      array_splice($values, 0, 0, array($app->getName()));
      $values[] = $total;

      fputcsv($fh, $values);
    }
    fputcsv($fh, array(
        'Total',
        $totals['trad'],
        $totals['platinum'],
        $totals['premium'],
        $totals['3d'],
        $totals['4d'],
        $totals['cx'],
    ));

    fclose($fh);
    $output->writeln(sprintf('Reporte generado: %s', $file));
  }

} 