<?php

namespace SocialSnack\WsBundle\Request;

use Buzz\Browser;

abstract class ConsultaIeAbstract extends ConsultaAbstract {
  
  const PATH = '/wsInvitadoEspecial/wsInvitadoEspecial.asmx';


  protected function requiresAuth() {
    return FALSE;
  }


}