<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Nuevo atributo - Cinemex' );
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Nuevo atributo</strong>
        </header>

        <div class="panel-body">
          <form class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Tipo</label>
              <div class="col-sm-10">
                <?php echo $view['form']->widget($form['type']); ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
              <div class="col-sm-10">
                <?php echo $view['form']->widget($form['name']); ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Valor</label>
              <div class="col-sm-10">
                <?php echo $view['form']->widget($form['value']); ?>
              </div>
            </div>


            <button type="button" class="btn btn-default" onclick="window.location.href='../attribute_list';">Cancelar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </form>
        </div>
      </section>
    </div>
  </div>
<?php $view['slots']->stop(); ?>