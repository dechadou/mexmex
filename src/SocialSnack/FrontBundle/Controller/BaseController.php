<?php

namespace SocialSnack\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

use Endroid\QrCode\QrCode;

class BaseController extends Controller {
	
  
  protected function get_flash_bag() {
    $app_session = $this->getRequest()->getSession();
		$flash_bag   = $app_session->getFlashBag();
		$flash_msgs  = array();
		foreach ( array( 'notice', 'warking', 'error' ) as $k ) {
			$flash_msgs[$k] = $flash_bag->get( $k );
		}
		$flash_bag->clear();
		return $flash_msgs;
  }
	
  
	/**
	 * @todo Get rid of this awful method and do something better instead.
	 */
	protected function _get_js_stuff() {
    $doctrine      = $this->get( 'doctrine' );
		$state_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
		$states        = $state_repo->findAllWithAreasAndCinemas();
		$_states       = array();
    
		foreach ( $states as $state ) {
			$_states[] = $state->toArray( TRUE, TRUE );
		}
    
		return array(
				'states'    => $states,
				'_states'   => $_states,
        'is_dev'    => in_array($this->get('kernel')->getEnvironment(), array('test', 'dev')),
		);
	}
  
  
  protected function _get_js_movies( $movies ) {
    $_movies       = array();
    
    foreach ( $movies as $movie ) {
      $_movie      = $movie->toArray(FALSE, FALSE, FALSE);
      $attributes  = array();
      $data        = array();
      if ( $movie->getPremiere() )                             { $attributes[] = 'new'; }
      if ( 'DIGITAL 3D' == $movie->getData( 'FORMATOVIDEO' ) ) { $attributes[] = '3d'; }
      foreach ( $attributes as $attr ) {
        $data[] = 'data-' . $attr;
      }
      $_movie['data']       = implode( ' ', $data );
      $_movie['cover']      = $this->container->get('templating.helper.assets')->getUrl( $_movie['cover'], 'MoviePosters' );
      $_movie['url']        = $this->generateUrl( 'movie_single', array( 'movie_id' => $movie->getId(), 'slug' => FrontHelper::sanitize_for_url( $movie->getName() ) ) );
      $_movie['youtube_id'] = FrontHelper::get_youtube_id_from_url( $movie->getData( 'TRAILER' ) );
      unset($_movie['info']);
      unset($_movie['score']);
      unset($_movie['versions']);
      $_movies[] = $_movie;
    }
		
    return $_movies;
  }
  
  protected function merge_js_stuff( $res ) {
    return array_merge( $res, $this->_get_js_stuff() );
  }
  
  
  protected function _check_cinema_disabled($cinema) {
    $disabled_cinemas = $this->container->getParameter('disabled_cinemas');
    if ( in_array($cinema->getId(), $disabled_cinemas) ) {
      $response = $this->render('SocialSnackFrontBundle:Default:disabledCinema.html.php', array( 'cinema' => $cinema ));
      $response->send();
      die();
    }
  }
		
}
