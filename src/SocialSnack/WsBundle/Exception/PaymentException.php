<?php

namespace SocialSnack\WsBundle\Exception;

/**
 * Class PaymentException
 * @package SocialSnack\WsBundle\Exception
 * @author Guido Kritz
 */
class PaymentException extends RequestMsgException {

} 