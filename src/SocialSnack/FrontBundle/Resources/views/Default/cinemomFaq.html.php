<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['cinemom']);
$view['slots']->set('title', 'CineMá - Cinemex');
$view['slots']->start('body')
?>
        <?php
        echo $view->render(
            'SocialSnackFrontBundle:Sections:cinemomMenu.html.php',
            array( 'route' => $route )
        );
        ?>

				<div class="cinemom-content">
          <h1 class="content-box-title">Preguntas Frecuentes</h1>
          
          <dl>
            <dt>¿Quiénes pueden asistir a las funciones de CineMá?</dt>
            <dd>Mamás y papás acompañados de un bebé no mayor a 2 años. Para poder entrar a las funciones de CineMá es necesario que vengan acompañados de un bebé. Se les negará la entrada a adultos que asistan solos.</dd>
            <dt>¿Cuáles son los días y horarios en los que hay funciones de CineMá? </dt>
            <dd>Las funciones destinadas a CineMá son las primeras funciones de los jueves y domingos en los complejos Cinemex participantes.</dd>
            <dt>¿Qué películas se proyectan en CineMá?</dt>
            <dd>Se proyecta el estreno de la semana, que no necesariamente es una película infantil. Las películas de clasificación C no son proyectadas.</dd>
            <dt>¿Cómo se adecúa la sala para que los bebés y papás estén cómodos?</dt>
            <dd>En la sala se cuida que el volumen no sea demasiado alto, que el aire acondicionado se mantenga bajo y que la luz no se apague por completo para que los invitados puedan atender cualquier necesidad que su bebé pueda tener. Asimismo, contamos con servicio de dulcería a la sala.</dd>
            <dt>¿Dónde se colocan las carriolas?</dt>
            <dd>Deben colocarse debajo de la pantalla dentro de la sala, esta ubicación está pensada para no obstruir los pasillos en caso de una emergencia.</dd>
            <dt>¿Cuáles son los cines participantes?</dt>
            <dd><a href="<?php echo $view['router']->generate('cinemom_cinemas'); ?>">Haz click aquí para ver el listado</a>.</dd>
          </dl>

				</div>
<?php $view['slots']->stop(); ?>