<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

interface DoctrineTaskInterface extends CachePurgeTaskInterface {

  public function getDoctrineResources();

}