<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class ConsultaActualizaciones extends ConsultaAbstract {

	protected $method       = 'ConsultaActualizaciones';

	protected $default_args = array(
			'Tipo'        => 1,
			'Codigo'      => '',
	);


  protected function __parse_res( $res ) {
    return simplexml_load_string( trim( $res ) );
  }

  protected function _parse_res( $xml ) {
    if ((int) $this->args['Tipo'] === 2) {
      return $xml;
    }

    if ( !$xml || !isset( $xml->RESPUESTA ) )
      throw new \Exception( 'Invalid response 0: ' . json_encode( $xml ) );

    return $this->parse_res( $xml->RESPUESTA );
  }

	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
	 * @param \SimpleXMLElement $xml
	 * @return \SimpleXMLElement
	 */
	public function parse_res( $xml ) {
    if ((int) $this->args['Tipo'] === 2) {
      return $xml;
    }

		if (!isset($xml->CodigoMax)) {
			if (isset($xml->MENSAJE)) {
				throw new RequestMsgException($xml->MENSAJE);
      } else {
				throw new UnexpectedWsResponseException('Invalid response 1:' . json_encode($xml));
      }
		}

		return $xml;
	}
}
