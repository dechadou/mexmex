<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Gracias cine por tanta magia');
$view['slots']->set('body_class', 'sponsored-campana-corporativa');
?>

<?php $view['slots']->start('body'); ?>
<style>
.sponsored-campana-corporativa {
  background-color:#c21f30;
}

.sponsored-campana-corporativa .landing-header {
  display:block;
  height:auto;
  margin-bottom:18px;
  max-width:100%;
}

.sponsored-campana-corporativa,
.sponsored-campana-corporativa #sponsored-bg {
  background:#f11a2c no-repeat 50% 0;
}

@media screen and (min-width: 1279px) {
  .sponsored-campana-corporativa #sponsored-bg {
    background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/campana-corporativa/BG-1280.jpg'); ?>);
  }
}

@media screen and (min-width: 1439px) {
  .sponsored-campana-corporativa #sponsored-bg {
    background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/campana-corporativa/BG-1440-1336.jpg'); ?>);
  }
}

@media screen and (min-width: 1919px) {
  .sponsored-campana-corporativa #sponsored-bg {
    background-image:url(<?php echo $view['assets']->getUrl('assets/img/landings/campana-corporativa/BG-1920-1680.jpg'); ?>);
  }
}
</style>

<img class="landing-header" src="<?php echo $view['assets']->getUrl('assets/img/landings/campana-corporativa/promo-a_harryP.jpg'); ?>">

<div class="embed-responsive embed-responsive-16by9">
  <iframe src="https://www.youtube.com/embed/4D4FRk1WEMA" frameborder="0" allowfullscreen></iframe>
</div>
<?php $view['slots']->stop(); ?>
