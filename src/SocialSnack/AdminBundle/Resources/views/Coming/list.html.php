<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de próximas películas - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de próximas películas
        <div class="right">
          <a href="<?php echo $view['router']->generate( 'social_snack_coming_inactive' );?>">Ver inactivos</a>
        </div>
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <div class="clearfix">
            <div class="btn-group">
              <button class="btn green" id="editable-sample_new" onclick="window.location.href = 'movie_coming_add'">
                Nueva película <i class="icon-plus"></i>
              </button>
            </div>
          </div>
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>Poster</th>
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Prox. Semana</th>
                <th>Editar</th>
                <!--<th>Deshabilitar</th>-->
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($movies as $movie) {
              ?>
              <tr class="gradeX">
                <td><img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '200x295'); ?>" alt="<?php echo $movie->getName(); ?>" width="154" height="230" /></td>
                <td><?php echo $movie->getName(); ?></td>
                <td><?php echo $movie->getReleaseDate()->format('Y-m-d'); ?></td>
                <td><?php echo $movie->getNextWeek() ? 'Sí' : 'No'; ?></td>
                <td><a href="movie_coming/<?php echo $movie->getId(); ?>" class="edit">Editar</a></td>
                <!--<th><a href="<?php echo $view['router']->generate('social_snack_admin_movie', array('movie_id' => $movie->getId())); ?>?disbale=true">Deshabilitar</a></th>-->
              </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[4, "desc"]]
    });
  });
</script>
<?php $view['slots']->stop(); ?>