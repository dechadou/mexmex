<?php

namespace SocialSnack\FrontBundle\EventListener;

use Psr\Log\LoggerInterface;
use SocialSnack\FrontBundle\Exception\CustomHttpException;
use SocialSnack\RestBundle\Exception\RestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Exception\DisabledSessionException;

class ExceptionListener {

  protected $container;
  protected $logger;
  
  
  public function __construct(ContainerInterface $container, LoggerInterface $logger) {
    $this->container = $container;
    $this->logger    = $logger;
  }
  
  
  public function onKernelException(GetResponseForExceptionEvent $event) {
    // You get the exception object from the received event
    $exception = $event->getException();

    $uri = $event->getRequest()->getUri();
    $mobile = FrontHelper::is_mobile($uri);

    // Customize your response object to display the exception details
    if ($exception instanceof CustomHttpException) {
      $args = array(
          'title' => '',
          'message' => $exception->getMessage(),
          'text' => $exception->getText(),
          'pageTitle' => $exception->getPageTitle(),
      );
    } elseif ($exception instanceof NotFoundHttpException) {
      $args = array(
          'title'   => 'Ups!',
          'message' => 'No hemos encontrado la página solicitada.',
      );
    } elseif ($exception instanceof AccessDeniedHttpException) {
      $args = array(
          'title'   => 'Ups!',
          'message' => 'Lo sentimos, no tienes acceso a la página solicitada.',
      );
    } elseif ($exception instanceof DisabledSessionException) {
      $args = array(
          'title'   => 'Ups!',
          'message' => 'La función solicitada no se encuentra disponible.',
          'text' => sprintf('<a href="%s">Haz click aquí para buscar otra función para <strong>%s</strong></a>.', 
              $this->container->get('ss.front.utils')->get_movie_link($exception->getSession()->getMovie()),
              $exception->getSession()->getMovie()->getName()),
      );
    } else {
      $method   = $event->getRequest()->getMethod();
      $error_id = preg_replace('/[^0-9]+/', '', gethostname()) . date('Ymd') . '-' . md5(gethostname() . ' ' . $method . ' ' . $uri . ' ' . time());
      $args = array(
          'title'    => 'Ups!',
          'message'  => 'Lo sentimos, ocurrió un error.',
          'error_id' => $error_id,
      );
      $this->logger->error(sprintf(
          '%s: %s: %s (uncaught exception) at %s line %s in %s %s',
          get_class($exception),
          $error_id,
          $exception->getMessage(),
          $exception->getFile(),
          $exception->getLine(),
          $method,
          $uri
      ));
    }

    // API error response.
    // $is_rest = $exception instanceof RestException || preg_match('/^(social_snack_rest_?|rest_)/', $event->getRequest()->attributes->get('_route'));
    if ($exception instanceof RestException) {
      $data = array_merge(
          array(
              'errorcode' => $exception->getErrorCode(),
              'error' => $exception->getMessage(),
          ),
          $exception->getData()
      );

      if ($event->getRequest()->attributes->get('captcha')) {
        $data['captcha'] = TRUE;
      }
      $response = new JsonResponse(
          $data,
          $exception->getStatusCode()
      );
      $event->setResponse($response);
      return;
    }


    $content = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:' . ($mobile ? 'Mobile' : 'Default') . ':error.html.php',
        array_merge( $args, array(
            'states'   => array(),
            '_states'  => array(),
            '_areas'   => array(),
            '_cinemas' => array(),
        ) )
    );
    $response = new Response();
    $response->setPublic();
    $response->setSharedMaxAge( 2 * 60 );
    $response->setMaxAge( 2 * 60 );
    $response->setContent($content);
    
    if ($exception instanceof HttpExceptionInterface) {
      $response->setStatusCode($exception->getStatusCode());
      $response->headers->replace($exception->getHeaders());
    } else {
      $response->setStatusCode(500);
    }

    // Send the modified response object to the event
    $event->setResponse($response);
  }

}
