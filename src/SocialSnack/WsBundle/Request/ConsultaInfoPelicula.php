<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class ConsultaInfoPelicula extends ConsultaAbstract {

	protected $method       = 'ConsultaInfoPelicula';
	
	protected $default_args = array(
			'Opcion'            => 0,
			'CodigoPelicula'    => 0,
			'CodigoCine'        => 0,
			'CodigoEstado'      => 0,
			'CodigoZona'        => 0,
			'Filtro'            => '',
			'Dia'               => 0,
	);
  
	
	/**
	 * Sadly the WS response doesn't follow the XML specifications and returns
	 * an invalid document. Some ugly modifications should be applied to the
	 * string before decoding into an XML object.
	 * 
	 * @param  string $xml
	 * @return string
	 */
	protected function additional_format( $xml ) {
		$xml = preg_replace( '/\<SINOPSIS\>(.*?)\<\/SINOPSIS\>/s', '<SINOPSIS><![CDATA[$1]]></SINOPSIS>', $xml );
		$xml = preg_replace( '/\<DIRECCION\>(.*?)\<\/DIRECCION\>/s', '<DIRECCION><![CDATA[$1]]></DIRECCION>', $xml );
		return $xml;
	}
  
	
	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
   * @param \SimpleXMLElement $xml
   * @return \SimpleXMLElement
	 */
	public function parse_res( $xml ) {
    $node = 'PELICULA';

		if ( !isset( $xml->{$node} ) ) {
			if ( $xml->MENSAJE ) {
				throw new RequestMsgException( $xml->MENSAJE );
      } else {
				throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
      }
		}
		
		return $xml->{$node};
	}
	
}