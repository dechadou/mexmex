<?php

namespace SocialSnack\AdminBundle\Http;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Class CsvResponse
 * @package SocialSnack\AdminBundle\Http
 * @author Guido Kritz
 */
class CsvResponse extends Response {

  protected $data;

  protected $filename = 'export.csv';

  protected $for_excel = FALSE;

  public function __construct($data = array(), $status = 200, $headers = array()) {
    parent::__construct('', $status, $headers);

    $this->setData($data);
  }

  public function saveFile(array $data, $filename){
    $this->filename = $filename;

    $output = fopen("php://temp", "w");
    foreach($data as $row){
      fputcsv($output, $row);
    }

    rewind($output);
    $this->data = '';
    while ($line = fgets($output)) {
      $this->data .= $line;
    }

    $this->data .= fgets($output);

    $fs = new \Symfony\Component\Filesystem\Filesystem();
    $folder = sys_get_temp_dir();
    try {
      $fs->dumpFile($folder.$this->filename, $this->data);
      $response = new Response();
      $response->headers->set('Cache-Control', 'private');
      $response->headers->set('Content-type', mime_content_type($folder.$this->filename));
      $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($folder.$this->filename) . '";');
      $response->headers->set('Content-length', filesize($folder.$this->filename));
      $response->sendHeaders();
      return $response->setContent(readfile($folder.$this->filename));
    }
    catch(IOException $e) {
      echo "An error occurred while uploading file to directory  ".$folder;
    }

  }

  public function setData(array $data) {
    $memory = 10 * 1024 * 1024; // 10 MB
    $output = fopen("php://temp/maxmemory:$memory", "r+");

    foreach ($data as $row) {
      fputcsv($output, $row);
    }

    rewind($output);
    $this->data = '';
    while ($line = fgets($output)) {
      $this->data .= $line;
    }

    $this->data .= fgets($output);

    return $this->update();
  }

  public function getFilename() {
    return $this->filename;
  }

  public function setFilename($filename) {
    $this->filename = $filename;
    return $this->update();
  }

  public function getForExcel() {
    return $this->for_excel;
  }

  public function setForExcel($for_excel) {
    $this->for_excel = $for_excel;
    return $this->update();
  }

  protected function update() {
    $this->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $this->filename));

    if (!$this->headers->has('Content-Type')) {
      $this->headers->set('Content-Type', 'text/csv');
    }

    if ($this->for_excel) {
      return $this->setContent(chr(255) . chr(254) . mb_convert_encoding('sep=,' . "\n" . $this->data, 'UTF-16LE', 'UTF-8'));
    } else {
      return $this->setContent($this->data);
    }
  }

} 