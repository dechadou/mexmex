<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

interface MemcachedTaskInterface extends CachePurgeTaskInterface {

  public function getMemcachedResources();

}