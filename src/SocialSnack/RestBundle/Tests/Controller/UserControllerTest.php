<?php

namespace SocialSnack\RestBundle\Tests\Controller;

use SocialSnack\RestBundle\Tests\Fixtures\LoadPurchaseHistoryData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;
use SocialSnack\WsBundle\Exception\InvalidIECodeException;

/**
 * Class UserControllerTest
 * @package SocialSnack\RestBundle\Tests\Controller
 * @author Guido Kritz
 */
class UserControllerTest extends WebTestCase {


  /**
   * @group functional
   * @group api
   */
  public function testDefaultAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
//        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $user_id = $user->getId();
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $model = [
        'id'         => [
            'type'   => 'integer',
            'equals' => $user_id,
        ],
        'first_name'    => 'string',
        'last_name'     => 'string',
        'email'         => 'string',
        'avatar'        => 'string',
        'avatar_medium' => 'string',
        'fbuid'         => 'string',
        'iecode'        => 'string',
        'cinema_id'     => 'null', //@todo Test a user with a favorite cinema.
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api
   */
  public function testUpdateAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/';
    $client->request(
        'PUT',
        $route,
        array(['first_name' => 'Another name']),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  /**
   * @group functional
   * @group api
   */
  public function testIecodeAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/iecode';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse(), ['iecode' => ['type' => 'string', 'equals' => (string)$user->getIecode()]]);
  }

  protected function _testIecodeValidateAction($ie_handler_mock, $params, $status_code) {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = $this->get_logged_in_client(LoadUserData::$users[0]);
    $client->getContainer()->set('rest.ie.handler', $ie_handler_mock);
    $route = '/rest/v2/me/iecode/validate';
    $client->request(
        'POST',
        $route,
        $params,
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );
    $this->assertEquals($status_code, $client->getResponse()->getStatusCode());
  }


  /**
   * @group functional
   * @group api
   */
  public function testIecodeValidateActionSuccess() {
    // Mock IEHandler
    $ie_handler_mock = $this->getServiceMockBuilder('rest.ie.handler')->getMock();
    $ie_handler_mock
        ->method('verifyIECode')
        ->willReturn(TRUE)
    ;

    $this->_testIecodeValidateAction($ie_handler_mock, array('iecode' => '1234567890123456'), 200);
  }


  /**
   * @group functional
   * @group api
   */
  public function testIecodeValidateActionInvalidCode() {
    // Mock IEHandler
    $ie_handler_mock = $this->getServiceMockBuilder('rest.ie.handler')->getMock();
    $ie_handler_mock
        ->method('verifyIECode')
        ->will($this->throwException(new InvalidIECodeException()))
    ;

    $this->_testIecodeValidateAction($ie_handler_mock, array('iecode' => '0000000000000000'), 400);
  }


  /**
   * @group functional
   * @group api
   */
  public function testIecodePostAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $iecode = '9999999999666666';
    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);

    $mock = $this->getServiceMockBuilder('rest.user.handler')
        ->getMock();

    $client->getContainer()->set('rest.user.handler', $mock);

    $route  = '/rest/v2/me/iecode';
    $client->request(
        'POST',
        $route,
        array('iecode' => $iecode),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse(), ['iecode' => ['type' => 'string', 'equals' => $iecode]]);
  }


  /**
   * @group functional
   * @group api
   */
  public function testIecodeDeleteAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/iecode';
    $client->request(
        'DELETE',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse(), ['iecode' => 'null']);
  }


  /**
   * @group functional
   * @group api
   */
  public function testFbuidAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/fbuid';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse(), ['fbuid' => ['type' => 'string', 'equals' => (string)$user->getFbuid()]]);
  }


  /**
   * @group functional
   * @group api
   */
  public function testFbuidPostAction() {
    // Actually this method isn't properly implmented yet.
//    $this->loadFixtures(array(
//        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
//        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
//    ));
//
//    $fbuid = '999999999';
//    $user = LoadUserData::$users_w_fb[0];
//    $client = $this->get_logged_in_client($user);
//    $route  = '/rest/v2/me/fbuid';
//    $client->request(
//        'POST',
//        $route,
//        array(['fbuid' => $fbuid]),
//        array(),
//        array('HTTP_X-API-Consumer-Key' => 'abc')
//    );
//
//    $this->assertJsonResponse($client->getResponse(), ['fbuid' => ['type' => 'string', 'equals' => (string)$user->getFbuid()]]);
  }


  /**
   * @group functional
   * @group api
   */
  public function testFbuidDeleteAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/fbuid';
    $client->request(
        'DELETE',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse(), ['fbuid' => 'null']);
  }


  /**
   * @group functional
   * @group api
   */
  public function testCinemasAction() {

  }


  /**
   * @group functional
   * @group api
   */
  public function testCinemasMoviesAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/cinemas/movies';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $this->assertJsonResponse($client->getResponse());
  }


  /**
   * @group functional
   * @group api
   */
  public function testCinemasPostAction() {

  }


  /**
   * @group functional
   * @group api
   */
  public function testCinemasDeleteAction() {

  }


  /**
   * @group functional
   * @group api
   */
  public function testPreferredCinemaAction() {

  }


  /**
   * @group functional
   * @group api
   */
  public function testLoginDataAction() {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
//        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $user = LoadUserData::$users_w_fb[0];
    $user_id = $user->getId();
    $client = $this->get_logged_in_client($user);
    $route  = '/rest/v2/me/loginData';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $model = [
        'data' => [
            'info' => [
                'id'         => [
                    'type'   => 'integer',
                    'equals' => $user_id,
                ],
                'first_name' => 'string',
                'last_name'  => 'string',
                'avatar'     => 'string',
                'fbuid'      => 'string',
                'iecode'     => 'string',
            ]
        ]
    ];
    $this->assertJsonResponse($client->getResponse(), $model);
  }

  protected function _testPurchaseHistoryAction($query = NULL) {
    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadPurchaseHistoryData',
    ));

    if (is_callable($query)) {
      $query = $query();
    } elseif (is_null($query)) {
      $query = [];
    }

    $client = $this->get_logged_in_client(LoadPurchaseHistoryData::$users[0]);
    $route = '/rest/v2/me/purchaseHistory';
    $client->request(
        'GET',
        $route,
        $query,
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );

    $json = $this->assertJsonResponse($client->getResponse(), ['data' => 'array']);

    return $json;
  }


  /**
   * @group functional
   * @group api
   */
  public function testPurchaseHistoryAction_DefaultArguments() {
    $json = $this->_testPurchaseHistoryAction();
    $this->assertCount(25, $json->data);
  }


  /**
   * @group functional
   * @group api
   */
  public function testPurchaseHistoryAction_CustomCount() {
    $json = $this->_testPurchaseHistoryAction([
        'count' => 10
    ]);
    $this->assertCount(10, $json->data);
  }


  /**
   * @group functional
   * @group api
   */
  public function testPurchaseHistoryAction_Since() {
    $json = $this->_testPurchaseHistoryAction(function() {
      return ['since' => LoadPurchaseHistoryData::$transactions[10]->getId()];
    });

    $this->assertTrue($json->data[0]->id > LoadPurchaseHistoryData::$transactions[10]->getId());
  }


  /**
   * @group functional
   * @group api
   */
  public function testPurchaseHistoryAction_Before() {
    $json = $this->_testPurchaseHistoryAction(function() {
      return ['before' => LoadPurchaseHistoryData::$transactions[25]->getId()];
    });

    $this->assertTrue($json->data[count($json) - 1]->id < LoadPurchaseHistoryData::$transactions[25]->getId());
  }


  /**
   * @group functional
   * @group api
   */
  public function testIeDetailsAction() {
    // Mock IEHandler
    $ie_handler_mock = $this->getServiceMockBuilder('rest.ie.handler')->getMock();

    $info_xml = new \SimpleXMLElement('<root/>');
    $info_xml->addChild('NUMIEC', '1234567890123456');
    $info_xml->addChild('PNOMBREIEC', 'Charles');
    $info_xml->addChild('SNOMBREIEC', 'Montgomery');
    $info_xml->addChild('APATERNOIEC', 'Burns');
    $info_xml->addChild('AMATERNOIEC', '');
    $info_xml->addChild('SALDO', '0.00');

    $trans_xml = new \SimpleXMLElement('<root/>');
    $trans_xml->addChild('NUMIEC', '1234567890123456');
    $trans_xml->addChild('ORIGEN', 'Candybar');
    $trans_xml->addChild('COMPLEJO', 'Cinemex Apruabas');
    $trans_xml->addChild('FECHA', '01/01/1970 00:00:00');
    $trans_xml->addChild('TIPOMOV', 'REDENCIONES');
    $trans_xml->addChild('PUNTOS', '100.00');

    $ie_handler_mock
        ->method('getIEInfo')
        ->willReturn($info_xml)
    ;
    $ie_handler_mock
        ->method('getIETransactions')
        ->willReturn([$trans_xml])
    ;

    $this->loadFixtures(array(
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ));

    $client = $this->get_logged_in_client(LoadUserData::$users[0]); // User must have IE code.
    $client->getContainer()->set('rest.ie.handler', $ie_handler_mock);
    $route = '/rest/v2/me/ie/details';
    $client->request(
        'GET',
        $route,
        array(),
        array(),
        array('HTTP_X-API-Consumer-Key' => 'abc')
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    // Test valid response.
    $data = @json_decode($client->getResponse()->getContent());
    $this->assertInstanceOf('stdClass', $data);
    $this->assertEquals('1234567890123456', $data->iecode);
    $this->assertEquals('Charles Montgomery Burns', $data->name);
    $this->assertEquals(0.0, $data->points);
    $this->assertInternalType('array', $data->history);
    foreach ($data->history as $item) {
      $this->assertEquals('Candybar',            $item->action);
      $this->assertEquals('Cinemex Apruabas',    $item->cinema);
      $this->assertEquals('01/01/1970 00:00:00', $item->date);
      $this->assertEquals(-100.00,               $item->points);
      break; // Test first element only.
    }
  }


  /**
   * @group functional
   * @group api
   */
  public function testRegisterGCMAction() {

  }

}