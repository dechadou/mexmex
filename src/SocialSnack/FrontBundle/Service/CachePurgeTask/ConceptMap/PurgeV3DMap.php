<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\ConceptMap;

class PurgeV3DMap extends AbstractConceptMap {

  protected  function getRouteName() {
    return '3d_cinemas';
  }


  protected function getConceptName() {
    return '3d';
  }

}