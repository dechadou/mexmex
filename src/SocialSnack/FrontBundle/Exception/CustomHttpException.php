<?php

namespace SocialSnack\FrontBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomHttpException extends HttpException {

  protected $text;
  protected $pageTitle;


  public function setPageTitle($pageTitle) {
    $this->pageTitle = $pageTitle;

    return $this;
  }

  public function getPageTitle() {
    return $this->pageTitle;
  }

  public function setText($text) {
    $this->text = $text;

    return $this;
  }

  public function getText() {
    return $this->text;
  }

}