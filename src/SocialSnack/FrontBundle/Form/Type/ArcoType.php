<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

use SocialSnack\WsBundle\Request\Arco\MuestraCatalogos;

class ArcoType extends ArcoAbstractType {
  
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder->add('owner', new ArcoOwnerType($this->ws), array('label' => 'Datos del titular', 'attr' => array('subform' => TRUE)));
    $builder->add('rep', new ArcoRepType(), array('label' => 'Representante del titular', 'attr' => array('subform' => TRUE)));
    $builder->add('rep_attach', new ArcoRepAttachType(), array('label' => 'Para acreditar el poder de representación deberá adjuntar la siguiente documentación (Art. 29, Art. 35 de la LFPDPPP)', 'attr' => array('subform' => TRUE)));
    
    $builder->add('Tipo_RelacionCMX', 'choice', array(
        'label' => 'Indique por favor el tipo de relación que tiene usted con Cinemex',
        'attr' => array( 'class' => 'whole' ),
        'choices' => $this->getWsChoices(MuestraCatalogos::CATALOGO_TIPO_RELACION, array('' => 'Seleccione una opción')),
    ));
    $builder->add('NumEmpleado', null, array(
        'label' => 'Indique su número de empleado',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' )
    ));
    $builder->add('NumIE', null, array(
        'label' => 'Indique su número de Invitado Especial',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' )
    ));
    $builder->add('rel_provider', null, array(
        'label' => 'Compañía',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' )
    ));
    $builder->add('SolicitaTipo', 'choice', array(
        'label' => 'Tipo de puesto',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' ),
        'choices' => array(
            '' => 'Seleccione una opción',
            'corporativo' => 'Corporativo',
            'complejo'    => 'Complejo',
        ),
        'constraints' => array(
            new NotBlank(array('groups' => array('rel_applicant')))
        ),
    ));
    $cinemas = $this->getWsChoices(MuestraCatalogos::CATALOGO_COMPLEJOS);
    asort($cinemas);
    array_splice($cinemas, 0, 0, array('' => 'Selecciona el cine'));
    $builder->add('SolicitaComplejo', 'choice', array(
        'label' => 'Selecciona el cine',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-type-input' ),
        'choices' => $cinemas,
        'constraints' => array(
            new NotBlank(array('groups' => array('rel_applicant_cinema')))
        ),
    ));
    $builder->add('SolicitaComplejoCheck', 'checkbox', array(
        'label' => 'Si ya envió su solicitud de empleo, marque el siguiente recuadro:',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' )
    ));
    $builder->add('RelacionOtro', null, array(
        'label' => 'Especifique cuál',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' )
    ));
    $builder->add('RFC', null, array(
        'label' => 'RFC',
        'required' => FALSE,
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-rel-input' ),
    ));
    
    $builder->add('CveDerechoAccDat', 'choice', array(
        'label' => 'En el ejercicio del derecho de acceso sobre mis datos de carácter personal, solicito a Producciones Expreso Astral S.A. de C.V. en adelante Cinemex lo siguiente (seleccionar una opción)',
        'attr' => array( 'class' => 'whole' ),
        'expanded' => TRUE,
        'choices' => $this->getWsChoices(MuestraCatalogos::CATALOGO_DERECHO_ACC),
        'constraints' => array(
            new NotBlank()
        ),
    ));
    
    $builder->add('Comentario', null, array(
        'label' => 'Comentarios',
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new NotBlank(array('groups' => array('req_acc', 'req_can', 'req_opo'))),
        ),
    ));
    
    $builder->add('DatoModificar', null, array(
        'label' => 'Dato que quiere modificar',
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new NotBlank(array('groups' => array('req_rec'))),
        ),
    ));
    $builder->add('DatoActual', null, array(
        'label' => 'Dato actual',
        'attr' => array( 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new NotBlank(array('groups' => array('req_rec'))),
        ),
    ));
    $builder->add('DatoNuevo', null, array(
        'label' => 'Actualizar por:',
        'attr' => array( 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new NotBlank(array('groups' => array('req_rec'))),
        ),
    ));
    $builder->add('attach_doc', 'file', array(
        'label' => 'Documentación que acredite la modificación del dato',
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new NotBlank(array('groups' => array('req_rec'))),
            new File(array(
                'maxSize' => '6144k',
                'maxSizeMessage' => 'El tamaño del archivo supera el límite permitido.',
                'uploadErrorMessage' => 'No se pudo subir el archivo.'
            )),
        )
    ));
    $builder->add('attach_doc2', 'file', array(
        'label' => 'Documentación adicional (opcional)',
        'attr' => array( 'class' => 'whole', 'row_class' => 'hidden arco-comment-input' ),
        'required' => FALSE,
        'constraints' => array(
            new File(array(
                'maxSize' => '6144k',
                'maxSizeMessage' => 'El tamaño del archivo supera el límite permitido.',
                'uploadErrorMessage' => 'No se pudo subir el archivo.'
            )),
        )
    ));
    
    $builder->add('submit', 'submit', array('label' => 'Enviar'));
  }
  
  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
      'validation_groups' => function(FormInterface $form) {
        $groups = array('Default');
        $data  = $form->getData();
        $rep   = $data['rep'];

        if ( $rep['first_name'] || $rep['last_name'] || $rep['email'] ) {
          $groups[] = 'rep';
        }
        
        switch ($data['CveDerechoAccDat']) {
          case '1':
            $groups[] = 'req_acc';
            break;
          case '2':
            $groups[] = 'req_rec';
            break;
          case '5':
            $groups[] = 'req_can';
            break;
          case '6':
            $groups[] = 'req_opo';
            break;
        }

        return $groups;
      }
    ));
  }
  
  
  public function getName() {
      return 'arco';
  }
  
}