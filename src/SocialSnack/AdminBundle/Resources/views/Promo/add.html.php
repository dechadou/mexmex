<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Nueva Promo - Cinemex' );
$view['slots']->start('body');
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Nueva Promo</strong>
            </header>
            
            <div class="panel-body">
                <form class="form-horizontal tasi-form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Título</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['title']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Subtítulo</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['subtitle']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Contenido</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['content']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">URL</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['url']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Orden</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['sequence']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Activo</label>
                        <div class="col-sm-10">
                            <?php echo $view['form']->widget($form['status']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Thumb</label>
                        <div class="col-sm-10">
                            Thumb 236x222px <?php echo $view['form']->widget($form['thumb']); ?><br />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Imagen grande</label>
                        <div class="col-sm-10">
                            Imagen 670x320px <?php echo $view['form']->widget($form['image']); ?><br />
                        </div>
                    </div>
                    
                    <button type="button" class="btn btn-default" onclick="window.location.href='../promo_list';">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </section>
    </div>
</div>
<?php $view['slots']->stop(); ?>