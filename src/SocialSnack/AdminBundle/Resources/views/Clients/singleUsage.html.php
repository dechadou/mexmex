<?php
$title = sprintf('Uso del cliente <strong>%s</strong>', $client->getName());

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', strip_tags($title) . ' - Cinemex');
$view['slots']->start('body');
?>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <?php echo $title; ?>
        </header>

        <div class="panel-body text-center">
          <canvas id="myChart" width="600px" height="300px"></canvas>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" language="javascript" src="<?php echo $view['fronthelper']->get_static_url('admin/assets/chart-master/Chart.min.js') ?>"></script>
  <script>
    var ctx = $("#myChart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx);

    var data = {
      labels: <?php echo json_encode(array_map(function($a) { return (string)$a; }, $data['labels'])); ?>,
      datasets: [
        {
          data: <?php echo json_encode($data['values']); ?>,
          fillColor: "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor: "rgba(151,187,205,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)"
        }
      ]
    };
    var myLineChart = new Chart(ctx).Line(data, {
      bezierCurve : false
    });
  </script>
<?php $view['slots']->stop(); ?>