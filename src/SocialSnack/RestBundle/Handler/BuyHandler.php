<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Entity\Transaction;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\RestBundle\Exception\MissingFieldsException;
use SocialSnack\RestBundle\Exception\RestException;
use SocialSnack\RestBundle\Service\Helper as RestHelper;
use SocialSnack\RestBundle\Service\Utils as RestUtils;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Exception\IEPaymentException;
use SocialSnack\WsBundle\Exception\PaymentException;
use SocialSnack\WsBundle\Request\ApartaAsientos;
use SocialSnack\WsBundle\Request\DetalleSession;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Templating\DelegatingEngine;
use Symfony\Component\Templating\Helper\AssetsHelper;

/**
 * Class BuyHandler
 * @package SocialSnack\RestBundle\Handler
 * @author Guido Kritz
 */
class BuyHandler {

  const SIMULATED_PURCHASE_SUCCESS = 1;
  const SIMULATED_PURCHASE_FAIL    = 2;

  protected $container;

  protected $doctrine;

  protected $ws;

  protected $memcached;

  protected $rest_utils;

  protected $timeout_time;

  protected $simulated_purchase;


  public function __construct(ContainerInterface $container, Registry $doctrine, WsRequest $ws, \Memcached $memcached, RestUtils $rest_utils, $timeout_time, $simulated_purchase) {
    $this->container = $container;
    $this->doctrine = $doctrine;
    $this->ws = $ws;
    $this->memcached = $memcached;
    $this->rest_utils = $rest_utils;
    $this->timeout_time = $timeout_time;
    $this->simulated_purchase = $simulated_purchase;
  }


  protected function getRepository($persistentObjectName, $persistentManagerName = NULL) {
    return $this->doctrine->getRepository($persistentObjectName, $persistentManagerName);
  }


  public function select_tickets(Request $request) {
    $doctrine     = $this->doctrine;
    $mem          = $this->memcached;
    $timeout_time = $this->timeout_time;
    $app_session  = $request->getSession();
    $session_id   = $request->request->get( 'session_id' );
    $tickets      = $request->request->get( 'tickets' );

    $app_session->start();

    // If the user has a previously started transaction, kill it.
    $this->cleanupUserTransactions($app_session);

    // Make sure all the POST data is present.
    if ( !$session_id || !$tickets ) {
      throw new MissingFieldsException();
    }

    $session_repo  = $doctrine->getRepository('SocialSnackWsBundle:Session');
    $session       = $session_repo->findWithMovieAndCinema($session_id);

    if (!$session) {
      throw new RestException('invalid-session', 'Invalid Session.');
    }

    $tickets_num = $this->parseTicketsQty($tickets);

    // Make sure at least one ticket was requested.
    if ( $tickets_num < 1 ) {
      throw new RestException('no-tickets-selected');
    }

    $this->validateAvailableSeatsForSession($session, $tickets_num);

    if (!WsHelper::session_allows_seat_allocation($session)) {
      // If session doesn't require seat allocation stop here.
      return array(
          'success'        => TRUE,
          'transaction_id' => '',
          'timeout'        => 0,
          'timeout_time'   => 0,
          'layout'         => '',
          'seats'          => '',
          'timestamp'      => time(),
      );
    }

    // Populate tickets for the Session.
    $session_repo->_populate_tickets($session);

    // Seats allocation
    $seats_info   = $this->getSeatsLayout($session);

    if (!$seats_info) {
      throw new RestException('no-layout');
    }

    $cat_code     = $seats_info['cat_code'];
    $cat_num      = $seats_info['cat_num'];
    $seats_layout = $seats_info['layout'];

    // Preallocate seats.
    $allocated_seats = $this->allocateSeats($seats_layout, $tickets_num);

    // Freeze selected seats
    $seats_str      = $this->buildSeatsString($allocated_seats, $cat_code, $cat_num);
    $tickets_parsed = $this->parseReqTickets($session, $tickets);
    $tickets_str    = $this->buildTicketsString($tickets_parsed);
    $transaction    = $this->freezeSeats($session, $seats_str, $tickets_str);

    $trans_id = $this->buildTmpTransactionId($session);

    // The timeout times. Set this after all WS requests have been completed.
    $rel_timeout = $timeout_time * 60;
    $trans_timeout = time() + $rel_timeout;

    // Store transaction and all associated info in Memcached.
    $transaction['id']         = $trans_id;
    $transaction['tickets']    = $tickets;
    $transaction['seats']      = $allocated_seats;
    $transaction['cat_code']   = $cat_code;
    $transaction['cat_num']    = $cat_num;
    $transaction['session_id'] = $session->getId();
    $transaction['timeout']    = $trans_timeout;

    $mem->add( $trans_id, $transaction, $rel_timeout );
    $app_session->set( 'transaction_id', $trans_id );

    // Return successful response.
    return array(
        'success'        => TRUE,
        'transaction_id' => $trans_id,
        'timeout'        => $trans_timeout,
        'timeout_time'   => $timeout_time,
        'layout'         => $seats_layout,
        'seats'          => $allocated_seats,
        'timestamp'      => time(),
    );
  }


  public function select_seats(Request $request) {
    $doctrine     = $this->doctrine;
    $mem          = $this->memcached;
    $app_session  = $request->getSession();
    $timeout_time = $this->timeout_time;
    $session_id   = $request->request->get('session_id');
    $req_seats    = $request->request->get('seats');

    $app_session->start();

    $mem->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);

    $trans_id = $request->request->get( 'transaction_id' );
    $transaction = $this->getTmpTransaction($trans_id);

    // Make sure all the POST data is present.
    if ( !$session_id || !$req_seats || !$trans_id ) {
      throw new MissingFieldsException();
    }

    $session_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Session' );
    $session       = $session_repo->findWithMovieAndCinema( $session_id );

    if (!$session) {
      throw new RestException('invalid-session', 'Invalid Session.');
    }

    $session_repo->_populate_tickets($session);

    // Fetch seats layout.
    $seats_info   = $this->getSeatsLayout($session);
    $seats_layout = $seats_info['layout'];

    // Make sure requested seats are still available.
    $simplified_seats = array_map(function($a) {
      return $a['row'] . '-' . $a['seat'];
    }, $transaction['seats']);
    $non_available = FALSE;
    foreach ( $req_seats as $seat ) {
      if ( $seats_layout[$seat['row']]['seats'][$seat['seat']] != 0 && !in_array($seat['row'] . '-' . $seat['seat'], $simplified_seats)) {
        $non_available = TRUE;
        break;
      }
    }

    // Selected seats non available.
    if ( $non_available ) {
      $e = new RestException('seats-non-available');
      $e->setData(array(
          'layout' => $seats_layout,
          'seats'  => $transaction['seats'],
      ));
      throw $e;
    }

    /** @todo Can't leave one empty seat alone */

    // Freeze selected seats (and release previous seats selection).
    $seats_str      = $this->buildSeatsString($req_seats, $transaction['cat_code'], $transaction['cat_num']);
    $tickets_parsed = $this->parseReqTickets($session, $transaction['tickets']);
    $tickets_str    = $this->buildTicketsString($tickets_parsed);
    $transaction    = $this->freezeSeats($session, $seats_str, $tickets_str, $transaction, TRUE);

    // Update transaction.
    $rel_timeout = $timeout_time * 60;
    $trans_timeout = time() + $rel_timeout;
    $transaction['timeout'] = $trans_timeout;
    $transaction['seats'] = $req_seats;
    $trans_saved = $mem->set( $trans_id, $transaction, $rel_timeout );

    // Update Session transactions expiration time.
    $tmp_id = RestHelper::get_session_memcached_key($session);
    $mem->touch($tmp_id, $rel_timeout * 2);

    if (!$trans_saved) {
      throw new RestException('error save mem');
    }

    // Return successful response.
    return array(
        'success'        => TRUE,
        'transaction_id' => $trans_id,
        'layout'         => $seats_layout,
        'seats'          => $req_seats,
        'timestamp'      => time(),
    );
  }


  public function complete_transaction(Request $request, $user, $client = NULL) {
    $doctrine      = $this->doctrine;
    $mem           = $this->memcached;
    $rest_utils    = $this->rest_utils;
    $app_session   = $request->getSession();
    $ip            = FrontHelper::get_user_ip();

    // Get POST data
    $session_id    = $request->request->get('session_id');
    $trans_id      = $request->request->get('transaction_id');
    $tickets       = $request->request->get('tickets');
    $seats         = $request->request->get('seats');
    $email         = $request->request->get('email');
    $name          = $request->request->get('name');
    $cc            = $request->request->get('cc');
    $csc           = $request->request->get('csc');
    $expire_month  = $request->request->get('expire-month');
    $expire_year   = $request->request->get('expire-year');
    $app_id        = $request->request->get('app_id', '');
    $ref           = $request->request->get('ref', '');

    $app_session->start();

    if (is_numeric($email)) {
      throw new RestException('', '', 500);
    }

    $security_rules = $rest_utils->process_security_enforcement( $ip, 'purchase', $request, $client );
    if (in_array('captcha_failed', $security_rules)) {
      $rest_utils->set_ip_trans_failed( $ip );
      throw new RestException('captcha-failed');
    }

    // Make sure all the POST data is present.
    $required = array(
        'session_id',
        'tickets',
        'name',
        'email',
        'cc',
        'csc',
        'expire_month',
        'expire_year',
    );

    foreach ( $required as $field ) {
      if ( empty( ${$field} ) ) {
//        $this->container->get('graphite')->publish('purchase.error.validation', 1);
        throw new MissingFieldsException();
      }
    }

    $session_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Session' );
    $session       = $session_repo->findWithCinemaAndTickets( $session_id );

    if (!$session) {
      throw new RestException('invalid-session', 'Invalid Session.');
    }

    // Build tickets string.
    $tickets_ref   = $this->parseReqTickets($session, $tickets);
    $tickets_str   = $this->buildTicketsString($tickets_ref);
    $tickets_num   = sizeof($tickets_ref);
    $tickets_total = $this->parseTicketsQty($tickets_ref);
    $total         = $this->parseTotalAmount($tickets_ref);

    // Make sure at least one ticket was requested.
    if ( $tickets_num < 1 ) {
      throw new RestException('no-tickets-selected');
    }

    // Session allows seat allocation? Build seats string.
    $seat_allocation = WsHelper::session_allows_seat_allocation($session);
    if ( $seat_allocation ) {

      $transaction = $this->getTmpTransaction($trans_id);

      $seats_num = sizeof( $seats );
      $seats_str = $this->buildSeatsString($seats, $transaction['cat_code'], $transaction['cat_num']);

      // Make sure the amount of tickets requests is the same as seats allocated.
      if ( $tickets_total != $seats_num ) {
//        $this->container->get('graphite')->publish('purchase.error.validation', 1);
        throw new RestException('tickets-seats-dont-match');
      }
    } else {

      // No seat allocation allowed.
      $seats_str = '';
      $transaction = [];
    }

    // See if there's a logged in user and he has an IE code.
    $iecode = '';
    if ( $user ) {
      if ( $user->getIecode() ) {
        $iecode = $user->getIecode();
      }
    }
    if ( !$iecode ) {
      $iecode = $request->get( 'iecode' );
    }

    $args = array(
        'ClaveExterna'      => 0, // @todo Generate some unique numeric identifier.
        'CodigoCine'        => $session->getCinema()->getLegacyId(),
        'Session'           => $session->getLegacyId(),
        'CadenaBoletos'     => $tickets_str,
        'TDC'               => preg_replace( '/\s|-/', '', $cc ),
        'Vigencia'          => $expire_year . str_pad( $expire_month, 2, '0', STR_PAD_LEFT ),
        'DV'                => $csc,
        'CadenaPlatino'     => $seats_str,
        'IE'                => $iecode ? $iecode : '',
        'NombreCliente'     => $name,
    );

    $res = $this->perform_purchase($session, $args, $transaction);

    // Set success transaction counter for this IP.
    $rest_utils->set_ip_trans_success( $ip );


    // Parse seat numbers string
    $real_seats = $this->parse_seats_labels($res);

    // Build the booking code and the QR code.
    $code    = RestHelper::buildBookingCode($res->BRANCH, $res->BOOKING);
    $qr_file = $rest_utils->build_qr( $code, $session->getId() );
    // @todo Make sure this returns the URL of the static host.

    // Cleanup Credit Card information before storing.
    $res->TDC = '----' . substr($args['TDC'], -4);
    $res->NombreCliente = mb_convert_encoding($args['NombreCliente'], 'UTF-8', 'UTF-8');
    $res->Email = $email;
    $res->ClientIP = $ip;
    $res->IEC = $iecode;

    $info = @json_encode($res);
    if (!$info) {
      $this->container->get('logger')->critical(sprintf(
        'Error saving transaction information: %s',
        serialize($res)
      ));
    }

    // Store the transaction for history records.
    $history = new Transaction();
    if ( $user ) {
      $history->setUser( $user );
    }
    $history->setSession( $session );
    $history->setCinema( $session->getCinema() );
    $history->setMovie( $session->getMovie() );
    $history->setTickets( json_encode( $tickets_ref ) );
    $history->setSeats( json_encode( $seats ) );
    $history->setRealSeats( implode(',', $real_seats) );
    $history->setInfo( $info );
    $history->setCode( $code );
    $history->setQr( $qr_file );
    $history->setAmount( $total );
    $history->setTicketsCount( $tickets_total );
    $history->setRef($ref);
    if ( $app_id ) {
      $history->setAppId($app_id);
    }

    $em = $doctrine->getManager();
    $em->persist( $history );
    $em->flush();

    // Remove transaction from user session and from Memcached.
    if ( $trans_id ) {
      $app_session->remove( 'transaction_id' );
      $mem->delete( $trans_id );
    }

    // Send the confirmation email.
    $recipients = array();
    $to_name = $user ? $user->getFullName() : $name;
    $recipients[$email] = $to_name;
    // If the provided email is not the same as the account's email, send a copy
    // to both addresses.
    if ( $user && $email != $user->getEmail() ) {
      $recipients[$user->getEmail()] = $to_name;
    }

    // Enqueue the email.
    $this->container->get('cinemex.mailer')->enqueue(array(
        'type'       => 'purchase',
        'trans_id'   => $history->getId(),
        'recipients' => $recipients,
    ));

    $assets_helper = $this->container->get('templating.helper.assets');

    // Passbook
    if ($request->request->get('passbook')) {
      $passbook = $this->rest_utils->build_passbook($history);
      $passbook = $assets_helper->getUrl($passbook, 'Static');
    } else {
      $passbook = NULL;
    }

    if ( $request->request->get( 'sms' ) ) :
      $phone_number  = $request->get( 'mobile_number' );
      $phone_carrier = $request->get( 'mobile_carrier' );

      // Enqueue the SMS
      $carriers = $this->container->getParameter('sms_carriers');

      if ( isset($phone_carrier, $carriers) ) :
        // Update user phone
        if ( $user ) {
          $user->addInfo('mobile_numer',   $phone_number);
          $user->addInfo('mobile_carrier', $phone_carrier);

          $em = $doctrine->getManager();
          $user->setUpdateDate();
          $em->persist( $user );
          $em->flush();
        }

        try {
          $this->container->get('cinemex.sms')->enqueue(array(
            'type'          => 'purchase',
            'transaction'   => $history,
            'session'       => $session,
            'cinema'        => $session->getCinema(),
            'movie'         => $session->getMovie(),
            'qr_url'        => $assets_helper->getUrl($history->getQr(), 'QR'),
            'phone_number'  => $phone_number,
            'phone_carrier' => $phone_carrier
          ));
        } catch (\Exception $e) {}
      endif;
    endif;

    return array(
        'success'      => TRUE,
        'id'           => RestHelper::num_hash($history->getId()),
        'booking_code' => $history->getCode(),
        'qr_url'       => $assets_helper->getUrl($history->getQr(), 'QR'),
        'html'         => $this->build_thankyou_page($history),
        'seats'        => $real_seats,
        'total'        => $total,
        'tickets'      => $tickets_ref,
        'auditorium'   => FrontHelper::confirmation_get_screenname($history),
        'iecode'       => $iecode ?: NULL,
        'passbook_url' => $passbook,
    );
  }


  protected function perform_purchase(Session $session, array $args, $transaction) {
    $rest_utils = $this->rest_utils;

    try {
      if ($this->simulated_purchase) {
        // Simulated purchase for testing purposes.
        return $this->simulatedPurchase();

      } else {
        // Real purchase
        return $this->actualPurchase($session, $args, $transaction);
      }

    } catch (\Exception $e) {

      // Issue with IE.
      if ($e instanceof IEPaymentException) {
        return $this->retryWithoutIE($session, $args, $transaction);
      }

      // Credit card rejected.
      if ($e instanceof PaymentException) {
        $rest_utils->set_ip_trans_failed(FrontHelper::get_user_ip());
        throw new RestException('cc-rejected');
      }

      throw new RestException('payment-error', $e->getMessage());
    }
  }


  protected function retryWithoutIE(Session $session, array $args, $transaction) {
    if (empty($args['IE'])) {
      throw new \Exception('Unexpected method call');
    }

    $this->container->get('ws.logger')->info('Retry without IE');

    // Remove IEC from request args and retry.
    unset($args['IE']);
    return $this->perform_purchase($session, $args, $transaction);
  }


  /**
   * Simulated purchase for testing purposes.
   *
   * @todo Maybe allow this method only in dev mode.
   *
   * @return \stdClass
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function simulatedPurchase() {
    $res = new \stdClass();
    $res->BOOKING = rand(90000, 99999);
    $res->BRANCH  = rand(100,999);
    $res->CADENAASIENTOS = '19652|0|0||0000000002|1|0001|8|8|A|A4|';

    if (static::SIMULATED_PURCHASE_FAIL === $this->simulated_purchase) {
      throw new RestException('simul-fail');
    }

    return $res;
  }


  /**
   * Perform a purchase request to the WS.
   *
   * @param Session $session
   * @param array   $args
   * @param array   $transaction
   * @return \SocialSnack\WsBundle\Request\SimpleXMLElement
   */
  protected function actualPurchase(Session $session, array $args, $transaction) {
    $ws = $this->ws;
    $seat_allocation = WsHelper::session_allows_seat_allocation($session);

    // @todo Move this value to a parameter.
//    $ws->setTimeout(30);

    if ($seat_allocation) {
      $ws->init('TerminaCompra');
      $args['TranTemp'] = $transaction['ws_trans_id'];
    } else {
      $ws->init('Compra');
    }
    $res = $ws->request($args);

    return $res;
  }


  /**
   * Freeze seats selection.
   * @todo If an existing transaction is provided, un freeze those seats first.
   *
   * @param Session $session
   * @param string $seats_str
   * @param string $tickets_str
   * @param array $transaction
   * @param boolean $release_seats If $transaction is provided you can set $release_seats = TRUE to release previously selected seats in the WS.
   * @return array
   */
  protected function freezeSeats(Session $session, $seats_str, $tickets_str, array $transaction = NULL, $release_seats = FALSE) {
    $ws = $this->ws;

    if (is_null($transaction)) {
      $transaction = array();
    }

    // It would be better to release the seats after the new selection, to make sure the new seats aren't occupied
    // by another user in the middle of the process leaving the user without any seat.
    // However there would be a conflict if any of the new seats are the same as any of the previous selection.
    // This issue is inherent to the way the WS works. Not sure if it can be avoided.
    if ($release_seats) {
      $this->releaseSeats($session, $transaction['seats_str'], $transaction['tickets_str'], $transaction['ws_trans_id']);
    }

    // Call WS ApartaAsientos method.
    $ws->init('ApartaAsientos');
    $ws_trans_id = $ws->request(array(
        'Opcion'         => ApartaAsientos::OPTION_FREEZE,
        'Session'        => $session->getLegacyId(),
        'CadenaAsientos' => $seats_str,
        'CadenaBoletos'  => $tickets_str,
        'CodigoCine'     => $session->getCinema()->getLegacyId(),
    ));

    if (!$ws_trans_id) {
      return FALSE;
    }

    $transaction['ws_trans_id'] = $ws_trans_id;
    $transaction['seats_str'] = $seats_str;
    $transaction['tickets_str'] = $tickets_str;

    return $transaction;
  }


  protected function releaseSeats(Session $session, $seats_str, $tickets_str, $ws_trans_id) {
    $ws = $this->ws;

    // Call WS ApartaAsientos method.
    $ws->init('ApartaAsientos');
    return $ws->request(array(
        'Opcion'         => ApartaAsientos::OPTION_RELEASE,
        'Session'        => $session->getLegacyId(),
        'CadenaAsientos' => $seats_str,
        'CadenaBoletos'  => $tickets_str,
        'CodigoCine'     => $session->getCinema()->getLegacyId(),
        'TranTemp'       => $ws_trans_id,
    ));
  }


  /**
   * If a user has a purchase transaction started, let's kill it.
   * Release the seats in the WS and remove the transaction from Memcached and the user's session variables.
   *
   * @param SessionInterface $app_session
   */
  public function cleanupUserTransactions(SessionInterface $app_session) {
    $trans_id = $app_session->get('transaction_id');
    if (!$trans_id) {
      return;
    }

    $this->invalidateTransaction($trans_id);

    $app_session->remove('transaction_id');
  }


  protected function getSeatsLayout(Session $session) {
    $ws = $this->ws;

    try {
      $ws->init('DetalleSession');
      $res = $ws->request( array(
          'Tipo'       => DetalleSession::TIPOCONSULTA_UBICACIONES,
          'Session'    => $session->getLegacyId(),
          'CodigoCine' => $session->getCinema()->getLegacyId(),
      ) );

    } catch ( \Exception $e ) {
      $res = NULL;
    }

    if ( !$res ) {
      return FALSE;
    }

    return array(
        'layout'   => $res['seats'],
        'cat_code' => $res['cat_code'],
        'cat_num'  => $res['cat_num'],
    );
  }


  /**
   * Get available seats number for a Session.
   *
   * @param Session $session
   * @return int Available seats number.
   */
  protected function getAvailableSeats(Session $session) {
    $ws = $this->ws;

    try {
      $ws->init( 'DetalleSession' );
      $available_seats = $ws->request( array(
          'Tipo'              => DetalleSession::TIPOCONSULTA_DISPONIBILIDAD,
          'Session'           => $session->getLegacyId(),
          'CodigoCine'        => $session->getCinema()->getLegacyId(),
      ) );

      // Success.
      return (int)$available_seats;

    } catch ( \Exception $e ) {}

    return FALSE;
  }


  protected function parse_seats_labels($res) {
    $real_seats = array();
    if (!isset($res->CADENAASIENTOS)) {
      return $real_seats;
    }
    $seats_res = explode('|', $res->CADENAASIENTOS);
    while(sizeof($seats_res) >= 12) {
      $_seat = array_splice($seats_res,0,12);
      $real_seats[] = $_seat[9] . $_seat[10];
    }
    sort($real_seats);

    return $real_seats;
  }


  protected function get_tracking_codes(Transaction $history) {
    $html     = '';
    $session  = $history->getSession();
    $group_id = $session->getGroupId();

    $handler = $this->container->get('rest.trackingcode.handler');
    $codes = $handler->getGroupCodes($group_id);

    if (!$codes) {
      return $html;
    }

    $twig = $this->container->get('twigstring');

    // Find tracking codes
    foreach ($codes as $code) {
      $html .= $twig->render(
          $code->getCode(),
          array('transaction' => $history)
      );
    }

    return $html;
  }


  protected function build_thankyou_page(Transaction $history) {
    $html = $this->container->get('templating')->render( 'SocialSnackFrontBundle:Default:bookingConfirmation.html.php', array(
        'show_print'  => TRUE,
        'print_url'   => $this->container->get('router')->generate( 'booking_print', array( 'id' => $history->getId(), 'hash' => $history->getHash() ) ) . '#print',
        'code'        => $history->getCode(),
        'qr_url'      => $history->getQr(),
        'transaction' => $history,
        'session'     => $history->getSession(),
    ) );

    // Tracking codes.
    $html .= $this->get_tracking_codes($history);

    return $html;
  }


  public function invalidateTransaction($transaction_id, $matchSessionId = FALSE) {
    $memcached = $this->memcached;
    $transaction = $memcached->get($transaction_id);

    if ( $memcached->getResultCode() === \Memcached::RES_NOTFOUND ) {
      // Transaction doesn't exists. Nothing to do here!
      return;
    }

    if ($matchSessionId && (int)$matchSessionId !== (int)$transaction['session_id']) {
      throw new \Exception('Transaction doesn\'t match session.');
    }

    if (isset($transaction['ws_trans_id'])) {
      try {
        $session = $this->getRepository('SocialSnackWsBundle:Session')->findWithCinema($transaction['session_id']);
        $this->releaseSeats($session, $transaction['seats_str'], $transaction['tickets_str'], $transaction['ws_trans_id']);
      } catch (\Exception $e) {
        /** @todo Do something here! Maybe log this error or something... */
      }
    }

    $memcached->delete($transaction_id);
  }


  /**
   * Checks if there are enough available seats for a Session.
   *
   * @param Session $session
   * @param int     $req_seats
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function validateAvailableSeatsForSession(Session $session, $req_seats) {
    $available_seats = $this->getAvailableSeats($session);

    // Unexpected error.
    if ($available_seats === NULL || $available_seats === FALSE ) {
      throw new RestException('no-tickets-err');
    }

    // Session sold out.
    if ( 0 == $available_seats ) {
      throw new RestException('no-tickets', 'No seats available (session sold out)');
    }

    // Check seats availability vs requested tickets number.
    if ( $req_seats > $available_seats ) {
      throw new RestException('no-tickets', 'Not enough seats available');
    }
  }


  protected function buildTmpTransactionId(Session $session) {
    $mem           = $this->memcached;
    $rel_timeout   = $this->timeout_time * 60;
    $attempts_loop = 0;

    do {
      // Get temporary sessions.
      $tmp_id = RestHelper::get_session_memcached_key($session);
      $tmp_session = $mem->get($tmp_id, NULL, $tmp_cas);
      if ( $mem->getResultCode() == \Memcached::RES_NOTFOUND ) {
        $tmp_session = array(
            'ids' => array(),
        );
      }

      // If we have no $tmp_session this far, there may be some issue with Memcached.
      if ( !$tmp_session ) {
        throw new RestException('mmcchd');
      }

      // Generate transaction ID.
      while ( in_array( ( $trans_id = md5( uniqid() ) ), $tmp_session['ids'] ) ) {}

      $tmp_session['ids'][] = $trans_id;

      // Write transactions to Memcached.
      // Use CAS to make sure no one wrote to the session during the process.
      if ( $tmp_cas ) {
        $tmp_saved = $mem->cas( $tmp_cas, $tmp_id, $tmp_session );
      } else {
        $tmp_saved = $mem->add( $tmp_id, $tmp_session, $rel_timeout * 2 );
      }

      if ( !$tmp_saved ) {
        $attempts_loop++;

        if ($attempts_loop < 6) {
          $this->container->get('logger')->error(sprintf(
              'BuyHandler:buildTmpTransactionId Memcached save failed. Attempt %d',
              $attempts_loop
          ));
        } else {
          $this->container->get('logger')->critical(sprintf(
              'BuyHandler:buildTmpTransactionId Memcached save failed. Attempt %d. Move on :S',
              $attempts_loop
          ));
          throw new \Exception();
        }
      }
    } while ( !$tmp_saved ); // If there was a problem saving the session (ie: CAS conflict) iterate over again.

    return $trans_id;
  }


  protected function parseReqTickets($session, $tickets) {
    $tickets_ref = array();

    foreach ( $tickets as $ticket ) {
      if ( !$ticket['qty'] ) {
        continue;
      }

      foreach ( $session->getTickets() as $_ticket ) {
        if ( $_ticket->getData( 'TICKETTYPECODE' ) != $ticket['type'] ) {
          continue;
        }

        $tickets_ref[] = array(
            'name'  => $_ticket->getDescription(),
            'qty'   => $ticket['qty'],
            'price' => $_ticket->getPrice(),
            'type'  => $ticket['type'],
        );
      }
    }

    return $tickets_ref;
  }


  protected function buildSeatsString($seats, $cat_code, $cat_num) {
    $seats_num = sizeof( $seats );
    $seats_str = '|' . $seats_num . '|';

    foreach ( $seats as $seat ) {
      $formatted_seat_num = str_pad( $seat['seat'], 2, '0', STR_PAD_LEFT );
      $seats_str .= $cat_code . '|' . $cat_num . '|' . $seat['row'] . '|' . $formatted_seat_num . '|';
    }

    return $seats_str;
  }


  protected function buildTicketsString($tickets) {
    $tickets_str = '|' . sizeof($tickets) . '|';

    foreach ( $tickets as $ticket ) {
      $tickets_str .= $ticket['type'] . '|' . $ticket['qty'] . '|' . $ticket['price'] . '|';
    }

    return $tickets_str;
  }


  /**
   * Automatically allocate seats in the layout.
   *
   * @param array   $seats_layout Seats layout.
   * @param integer $req_seats    Number of requested seats.
   * @return array
   */
  protected function allocateSeats($seats_layout, $req_seats) {
    $new_seats    = array();
    $seats_str    = str_repeat( '0', $req_seats );
    $seats_parts  = array( $seats_str );

    while ( sizeof( $seats_parts ) ) {
      $seats_str = array_shift( $seats_parts );
      $allocated = FALSE;

      foreach ( $seats_layout as $row_num => $row ) {
        $row_str   = implode( '', $row['seats'] );

        // If no room in this row, skip to the next one.
        if ( FALSE === ( $pos = strpos( $row_str, $seats_str ) ) ) {
          continue;
        }

        // Allocate the seats.
        $seats = array_slice( $seats_layout[$row_num]['seats'], $pos, strlen( $seats_str ), TRUE );
        foreach ( $seats as $seat_num => $status ) {
          $seats_layout[$row_num]['seats'][$seat_num] = 2;
          $new_seats[] = array(
              'row' => $row_num,
              'seat' => $seat_num,
          );
        }

        $allocated = TRUE;
        break;
      }

      // If there're no enough contiguous seats, split the string and try again.
      if ( !$allocated ) {
        $new_len   = ceil( strlen( $seats_str ) / 2 ) * -1;
        $new_part  = substr( $seats_str, $new_len );
        $seats_str = substr( $seats_str, 0, $new_len );
        if (!strlen($seats_str) || !strlen($new_part)) {
          return FALSE;
        }
        $seats_parts[] = $seats_str;
        $seats_parts[] = $new_part;
      }
    }

    if (sizeof($new_seats) === $req_seats) {
      return $new_seats;
    } else {
      return FALSE;
    }
  }


  /**
   * Get temporary transaction from Memcached.
   *
   * @param $trans_id
   * @return mixed
   * @throws \SocialSnack\RestBundle\Exception\RestException
   */
  protected function getTmpTransaction($trans_id) {
    $mem = $this->memcached;
    $transaction = $mem->get( $trans_id );

    if (!$transaction || $mem->getResultCode() == \Memcached::RES_NOTFOUND) {
      // Transaction expired, remove it from session.
      $app_session = $this->container->get('session');
      $app_session->remove('transaction_id');
      $app_session->save();
      throw new RestException('transaction-not-found');
    }

    return $transaction;
  }


  protected function parseTicketsQty($tickets) {
    return array_reduce($tickets, function($carry, $item) {
      return $carry + $item['qty'];
    }, 0);
  }


  protected function parseTotalAmount($tickets) {
    return array_reduce($tickets, function($carry, $item) {
      return $carry + ($item['qty'] * $item['price']);
    }, 0);
  }

}
