<?php

namespace SocialSnack\FrontBundle\Tests\Functional\Controller;

use SocialSnack\FrontBundle\Tests\Controller\WebTestCase;
use SocialSnack\FrontBundle\Tests\Fixtures\LoadUserData;

class UserControllerTest extends WebTestCase {

  public function testIeAction_NoIe() {
    $client = $this->getLoggedInClient(NULL, function() {
      return LoadUserData::$userNoIe;
    });
    $client->request(
        'GET',
        '/usuario/invitado-especial'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

  public function testIeAction_ValidIe() {
    $res1 = $this->getMock('Buzz\Message\Response');
    $res1->method('getContent')->willReturn('<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;ConsultaIEC&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;NUMIEC&gt;999999999&lt;/NUMIEC&gt;&lt;NUMINVITADO&gt;999&lt;/NUMINVITADO&gt;&lt;FECHAINSCRIPCION&gt;06/11/2014&lt;/FECHAINSCRIPCION&gt;&lt;PNOMBREIEC&gt;HOMERO&lt;/PNOMBREIEC&gt;&lt;SNOMBREIEC&gt;J&lt;/SNOMBREIEC&gt;&lt;APATERNOIEC&gt;SIMPSON&lt;/APATERNOIEC&gt;&lt;AMATERNOIEC&gt;&lt;/AMATERNOIEC&gt;&lt;TELEFONOCASA&gt;999999999&lt;/TELEFONOCASA&gt;&lt;CELULAR&gt;999999999&lt;/CELULAR&gt;&lt;EMAIL&gt;homero@simpson.com&lt;/EMAIL&gt;&lt;FECHANACIMIENTO&gt;22/03/1983&lt;/FECHANACIMIENTO&gt;&lt;NIVEL&gt;CORPORATIVA&lt;/NIVEL&gt;&lt;VIGENCIANIVEL&gt;31/12/2099&lt;/VIGENCIANIVEL&gt;&lt;VISITASORO&gt;0&lt;/VISITASORO&gt;&lt;VISITASBLACK&gt;0&lt;/VISITASBLACK&gt;&lt;SALDO&gt;43.00&lt;/SALDO&gt;&lt;FECHASALDOVALIDO&gt;05/08/2015&lt;/FECHASALDOVALIDO&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>');

    $res2 = $this->getMock('Buzz\Message\Response');
    $res2->method('getContent')->willReturn('<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;MovimientosIEC&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;TRANSACCION&gt;&lt;NUMIEC&gt;3086812059318963&lt;/NUMIEC&gt;&lt;FECHA&gt;05/08/2015 02:46:03 p. m.&lt;/FECHA&gt;&lt;COMPLEJO&gt;CINEMEX PARK PLAZA&lt;/COMPLEJO&gt;&lt;ORIGEN&gt;PUNTO DE VENTA (DULCERIA)&lt;/ORIGEN&gt;&lt;TIPOMOV&gt;ACUMULACIONES&lt;/TIPOMOV&gt;&lt;PUNTOS&gt;8.90&lt;/PUNTOS&gt;&lt;/TRANSACCION&gt;&lt;TRANSACCION&gt;&lt;NUMIEC&gt;3086812059318963&lt;/NUMIEC&gt;&lt;FECHA&gt;21/07/2015 02:36:23 p. m.&lt;/FECHA&gt;&lt;COMPLEJO&gt;CINEMEX PARK PLAZA&lt;/COMPLEJO&gt;&lt;ORIGEN&gt;PUNTO DE VENTA (DULCERIA)&lt;/ORIGEN&gt;&lt;TIPOMOV&gt;ACUMULACIONES&lt;/TIPOMOV&gt;&lt;PUNTOS&gt;7.30&lt;/PUNTOS&gt;&lt;/TRANSACCION&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>');

    $buzz = $this->getServiceMockBuilder('gremo_buzz')->getMock();
    $buzz->method('post')
        ->will($this->returnCallback(function() use ($res1, $res2) {
          $qs = func_get_arg(2);

          if (preg_match('/\bTipoConsulta=1\b/', $qs)) {
            return $res1;
          } elseif (preg_match('/\bTipoConsulta=2\b/', $qs)) {
            return $res2;
          }
        }));

    $client = $this->getLoggedInClient(NULL, function() {
      return LoadUserData::$userIe;
    });
    $client->getContainer()->set('gremo_buzz', $buzz);
    $client->request(
        'GET',
        '/usuario/invitado-especial'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
    $this->assertContains('HOMERO J SIMPSON', $client->getResponse()->getContent());
  }

  public function testIeAction_InvalidIe() {
    $res = $this->getMock('Buzz\Message\Response');
    $res->method('getContent')->willReturn('<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;LMSERROR&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;ERROR&gt;&lt;ERRORCODIGO&gt;-50100&lt;/ERRORCODIGO&gt;&lt;ERRORDESC&gt;Número de Invitado Especial No Existe.&lt;/ERRORDESC&gt;&lt;/ERROR&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>');

    $buzz = $this->getServiceMockBuilder('gremo_buzz')->getMock();
    $buzz->method('post')
        ->willReturn($res);

    $client = $this->getLoggedInClient(NULL, function() {
      return LoadUserData::$userInvalidIe;
    });
    $client->getContainer()->set('gremo_buzz', $buzz);
    $client->request(
        'GET',
        '/usuario/invitado-especial'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
    $this->assertContains('ingresa un nuevo código', $client->getResponse()->getContent());
  }

}