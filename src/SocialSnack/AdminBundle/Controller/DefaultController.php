<?php

namespace SocialSnack\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

  public function indexAction(Request $request) {
    return $this->render('SocialSnackAdminBundle:Default:index.html.php');
  }

}