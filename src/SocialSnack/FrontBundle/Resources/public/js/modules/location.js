( function() {
	var zipcodes = [
		{ from  : 00000, to    : 16999, state : 'Distrito Federal' },
		{ from  : 20000, to    : 20999, state : 'Aguascalientes' },
		{ from  : 21000, to    : 22999, state : 'Baja California' },
		{ from  : 23000, to    : 23999, state : 'Baja California Sur' },
		{ from  : 24000, to    : 24999, state : 'Campeche' },
		{ from  : 29000, to    : 30999, state : 'Chiapas' },
		{ from  : 31000, to    : 33999, state : 'Chihuahua' },
		{ from  : 25000, to    : 27999, state : 'Coahuila' },
		{ from  : 28000, to    : 28999, state : 'Colima' },
		{ from  : 34000, to    : 35999, state : 'Durango' },
		{ from  : 36000, to    : 38999, state : 'Guanajuato' },
		{ from  : 39000, to    : 41999, state : 'Guerrero' },
		{ from  : 42000, to    : 43999, state : 'Hidalgo' },
		{ from  : 44000, to    : 49999, state : 'Jalisco' },
		{ from  : 50000, to    : 57999, state : 'México' },
		{ from  : 58000, to    : 61999, state : 'Michoacán' },
		{ from  : 62000, to    : 62999, state : 'Morelos' },
		{ from  : 63000, to    : 63999, state : 'Nayarit' },
		{ from  : 64000, to    : 67999, state : 'Nuevo León' },
		{ from  : 68000, to    : 71999, state : 'Oaxaca' },
		{ from  : 72000, to    : 75999, state : 'Puebla' },
		{ from  : 76000, to    : 76999, state : 'Querétaro' },
		{ from  : 77000, to    : 77999, state : 'Quintana Roo' },
		{ from  : 78000, to    : 79999, state : 'San Luis Potosí' },
		{ from  : 80000, to    : 82999, state : 'Sinaloa' },
		{ from  : 83000, to    : 85999, state : 'Sonora' },
		{ from  : 86000, to    : 86999, state : 'Tabasco' },
		{ from  : 87000, to    : 89999, state : 'Tamaulipas' },
		{ from  : 90000, to    : 90999, state : 'Tlaxcala' },
		{ from  : 91000, to    : 96999, state : 'Veracruz' },
		{ from  : 97000, to    : 97999, state : 'Yucatán' },
		{ from  : 98000, to    : 99999, state : 'Zacatecas' },
	];


	var $popup      = $( '#location-popup' ),
			$form       = $( '#location-form' ),
			$api        = $( '#location-api' ),
			$input      = $( '#location-input' ),
			$results    = $( '#location-results' ),
			$results_ul = $results.find( 'ul' ),
			$error      = $( '#location-error' ),
			$bt         = $form.find( 'button[type=submit]'),
			working     = false,
			waiting_api = false
	;


	/**
	 * Location form submit.
	 */
	$form.bind( 'submit', function( e ) {
		e.preventDefault();
	} );
  
  
  var ac_data = [];
  $.each(cinemas, function(i, cinema) {
    ac_data.push({
      q : cinema.name,
      v : cinema.id
    });
  });
  $('#location-input').autocomplete({
    data      : ac_data,
    callback  : function($a){
      set_cinema( get_cinema_by_id( $a.data('value') ) );
    },
    div_class : 'location-input-ac'
  });


	/**
	 * Dislpay the dialog asking for user's location.
	 * Ask for browser's geolocation API if available.
	 */
	function ask_location( api, args ) {
    if ( !args ) {
      args = {};
    }
    
    // Hide close button?
    if (isset('close_bt', args) && false === args.close_bt) {
      $popup.find('.popup-close').hide();
    } else {
      $popup.find('.popup-close').show();
    }
    
    // Pre-select state.
    var selected_area_id;
    if ( 'area_id' in args ) {
      selected_area_id = args.area_id;
    } else if ( typeof user != 'undefined' && user.preferred_cinema ) {
      selected_area_id = user.preferred_cinema.area.id;
    } else if ( getCookie('m_cinema_id') ) {
      selected_area_id = get_cinema_by_id( getCookie('m_cinema_id') ).area.id
    }
    if ( selected_area_id ) {
      $form.find( '[name=area]' ).val(selected_area_id).trigger( 'change' );
    }

    // Clean up everything and set to the initial status.
		$input.val( '' );
		$error.hide();
		$results.hide();
		$form.show();
    $api.hide();

		// Use browser's geolocation API.
    if ( geoPosition.init() ) {
      $popup.find( '.api-enabled' ).show();

      if ( api ) {
        $( '.location-popup-arrow' ).show();
        waiting_api = true;
        $form.hide();
        $api.show();
        geoPosition.getCurrentPosition( geo_success, geo_error );
      }
    }

		$popup.popup();
	}


	$( '[rel="ask-location"]' ).bind( 'click', function( e ) {
		e.preventDefault();
		ask_location( $( this ).data( 'api' ) );
	} );


	/**
	 * Callback function on browser's geolocation success.
	 *
	 * @param {type} p Geolocation object.
	 * @returns {undefined}
	 */
	function geo_success( p ) {
    $( '.location-popup-arrow' ).hide();

		// Find the closest cinema.
		if ( p.coords.latitude && p.coords.longitude ) {
			var latlng = {
				lat : p.coords.latitude,
				lng : p.coords.longitude
			};
			find_by_latlng( latlng, 5, found );
			latlng.source = 'browser_api';
      if ( !is_mobile ) {
        update_user( { location : latlng } )
      }
		}
    $api.slideUp();
	}


	/**
	 * Callback function on browser's geolocation success.
	 *
	 * @param {type} p Error object.
	 * @returns {undefined}
	 */
	function geo_error( p ) {
    $( '.location-popup-arrow' ).hide();

    $api.slideUp();
    var msg = false;

    if ( p.code ) {
      switch ( p.code ) {
        case 3:
          msg = 'No se pudo obtener tu ubicación. Superó el tiempo de espera.';
          break;
          break;
        case 2:
          msg = 'Lo sentimos, tu navegador no permitió acceder a tu ubicación.';
          break;
        case 2:
        default:
          msg = 'Lo sentimos, no es posible obtener tu ubicación automáticamente.';
          break;
      }
    }
    if ( msg ) {
      msg += '<br/><a href="#" rel="ask_location">Ingresa tu ubicación manualmente</a>.';
    }
		error( msg );
    $error.find('a').bind('click',function(){ ask_location(); });
    $error.find('a').trigger('click');
	}


	/**
	 * Parse the location string submitted by the user and determine which kind
	 * of location it is and call the search function for that kind.
	 *
	 * @param {string} str
	 */
	function parse_location( str ) {
		var re_cinema   = /.*cinemex.*/i,
				re_zip      = /^[0-9]+$/,
				re_address  = /[a-zA-Z]+ [0-9]+/
		;

		// str is Cinema name.
		if ( re_cinema.test( str ) && ( cinema = match_cinema( str ) ) ) {
			found( [ cinema ] );
			return;
		}

		// str is a Zip Code.
		if ( re_zip.test( str ) && ( state = match_zipcode( str ) ) ) {
			find_by_state( state, found );
			return;
		}

		// str is an address.
		if ( re_address.test( str ) ) {
			find_by_address( str, found );
			return;
		}

		// str is a State name.
		if ( state = match_state( str ) ) {
			find_by_state( state, found );
			return;
		}

		// str matches any Cinema name?
//		if ( ( cinema = match_cinema( str ) ) || ( cinema = match_cinema( 'cinemex ' + str ) ) ) {
//			found( [ cinema ] );
//			return;
//		}

		// If no other criteria could be matched to str, lets Google Maps handle it.
		find_by_address( str, found );
		return;
	}
window.parse_location=parse_location;

	/**
	 * Return an State for the ZIP code.
	 *
	 * @param {integer} zip
	 * @returns {area|Boolean}
	 */
	function match_zipcode( zip ) {
		var match = null;

		if ( isNaN( zip ) )
			return false;

		zip = parseInt( zip );

		$.each( zipcodes, function( i, zipcode ) {
			if ( zip >= zipcode.from && zip <= zipcode.to )
				match = match_state( zipcode.state );
		} );

		return match;
	}

	/**
	 * Find a Cinema matching a string.
	 *
	 * @param {string} str
	 * @returns {area}
	 */
	function match_cinema( str ) {
		var match     = null,
				match_val = 0.5  // The default value for this var sets the minimum match threshold value.
		;

		str = str.toLowerCase();

		$.each( cinemas, function( i, cinema ) {
			if ( str == cinema.name.toLowerCase() ) {
				match     = cinema;
				match_val = 0;
			} else if ( ( _match_val = levenshteinenator( cinema.name.toLowerCase(), str ) ) < match_val ) {
				match     = cinema;
				match_val = _match_val;
			}
		} );

		return match;
	}


	/**
	 * Find a State matching a string.
	 *
	 * @param {String} str
	 * @returns {State}
	 */
	function match_state( str ) {
		var match = null;

		$.each( states, function( i, state ) {
			if ( str.toLowerCase() == state.name.toLowerCase() )
				match = state;
		} );

		/* @todo Use state aliases. Ie. DF, Distrito Federal, D.F., etc */
		return match;
	}


	/**
	 * Find all the cinemas in a State.
	 *
	 * @param {State} state
	 * @param {Function} cb
	 */
	function find_by_state( state, cb ) {
		results = [];
		$.each( cinemas, function( i, cinema ) {
			if ( state.id == cinema.state_id )
				results.push( cinema );
		} );

		cb( results );
	}


	/**
	 * Find the closest cinema to an address
	 *
	 * @param {String} address
	 * @param {Function} cb
	 */
	function find_by_address( address, cb ) {
		clog( 'find by address' );
		$bt.addClass( 'btn-loading' );
		working = true;

		if ( !/me|éxico/i.test( address ) )
			address += ', México';

		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { 'address': address }, function(results, status) {
			$bt.removeClass( 'btn-loading' );
			working = false;

			clog( arguments );

			// Error.
			if ( status != google.maps.GeocoderStatus.OK )
				return error();

			// No results.
			if ( !results || !results.length )
				return error_no_found();

			// Just one result, yay! Find the closest cinema.
			if ( 1 == results.length ) {
				if ( 'Mexico' == results[0].formatted_address )
					return error_no_found();

				find_by_latlng( {
					lat : results[0].geometry.location.lat(),
					lng : results[0].geometry.location.lng()
				}, 3, cb );
				return;
			}

			// Multiple results. Ask the user to pick the best match.
			var $ul = $results_ul;
			$ul.children().remove();
			$.each( results, function( i, result ) {
				$( '<li/>', {
					text : result.formatted_address,
					data : {
						lat : result.geometry.location.lat(),
						lng : result.geometry.location.lng(),
					},
          'class' : 'icon icon-marker icon-dark pad-left'
				} ).appendTo( $ul );
			} );
			$form.hide();
			$results.fadeIn();

			$results_ul.undelegate().delegate( 'li', 'click', function() {
				find_by_latlng( $( this ).data(), 3, found );
			} );
		} );
	}


	/**
	 * Find the closest cinema to a point.
	 *
	 * @param {type} pos
	 * @param {integer} count
	 * @param {Function} cb
	 * @returns {undefined}
	 */
	function find_by_latlng( pos, count, cb ) {
		var results    = [],
				distances  = []
		;

		if ( !count )
			count = 3;

		$.each( cinemas, function( i, cinema ) {
			distances.push( {
				d      : haversine( pos, cinema ),
				cinema : cinema
			} );
		} );

		if ( !distances.length )
			return cb();

		distances.sort( sort_by_distance );

		for ( var i = 0 ; i < count ; i++ ) {
			if ( i >= distances.length )
				break;
			results.push( distances[i].cinema );
		}
		cb( results );
	}
  window.find_by_latlng = find_by_latlng;

	/**
	 * Sort function to sort an array based on distance (objects with "d" property).
	 *
	 * @param {Object} a { d : int }
	 * @param {Object} b { d : int }
	 * @returns {Number}
	 */
	function sort_by_distance( a, b ) {
		if ( a.d == b.d )
			return 0;
		else if ( a.d < b.d )
			return -1;
		else
			return 1;
	}


	/**
	 * Display an error.
	 *
	 * @todo Display the error :P
	 *
	 * @param {string} msg Error message
	 * @returns {undefined}
	 */
	function error( msg ) {
		msg = msg || 'Ocurrió un error. Por favor, inténtalo nuevamente.';
		$error.html( msg ).slideDown();
	}


	/**
	 * No results found.
	 *
	 * @todo Display some message.
	 *
	 * @returns {undefined}
	 */
	function error_no_found() {
		error( 'No se encontró ningún resulta.<br/>Por favor, intenta con otra búsqueda.' );

		$form.show();
		$results.hide();
	}


	/**
	 * Receives the results from the search.
	 * If multiple results returned, display a list and ask the user to pick one.
	 * Set the default cinema if only one result is returned.
	 *
	 * @param {type} results
	 * @returns {unresolved}
	 */
	function found( results ) {
		if ( null == results )
			return error_no_found();

		if ( results.length > 1 )
			return multiple_results( results );

		set_cinema( results[0] );
	}


	/**
	 * Build a list of cinemas and ask the user to choose one.
	 *
	 * @todo It would be nice to show a map instead of a list.
	 *
	 * @param {array} results
	 * @returns {undefined}
	 */
	function multiple_results( results ) {
		var $ul = $results_ul;
		$ul.children().remove();
		$.each( results, function( i, result ) {
			$( '<li/>', {
				text : result.name,
				data : { id : result.id },
        'class' : 'icon icon-marker icon-dark pad-left'
			} ).appendTo( $ul );
		} );
		$form.hide();
		$results.fadeIn();

		$results_ul.undelegate().delegate( 'li', 'click', function() {
			set_cinema( get_cinema_by_id( $( this ).data( 'id' ) ) );
		} );
	}


	/**
	 * Set the user's preferred cinema.
	 *
	 * @todo Get the sidebar for the new preferred cinema (if changed).
	 *
	 * @param {Cinema} cinema
	 * @returns {undefined}
	 */
	function set_cinema( cinema ) {
    if ( is_mobile ) {
      $('#location-helper-id').val(cinema.id);
      $('#location-form-helper').submit();
      return;
    }

		waiting_api = false;

    if ( user.preferred_cinema && user.preferred_cinema.id == cinema.id )
      return;

		update_user({
      preferred_cinema : cinema,
      selected_area_id : cinema.area.id
    });
		if ( user.registered ) {
			// Persist new selection on DB.
			$.ajax( {
				url      : rest_url + 'me/preferredCinema',
				type     : 'post',
				dataType : 'json',
				data     : { cinema_id : cinema.id },
        success  : function() {}
			} );
		}
		$.event.trigger( {
			type   : 'change_location',
      cinema : cinema
		} );
		$.close_popups();
	}

	window.set_cinema = set_cinema;
	window.ask_location = ask_location;

	$( '#location-form-side' ).bind( 'submit', function( e ) {
		e.preventDefault();
		$input.val( $( '#location-input-side' ).val() );
		$form.show();
		$popup.popup();
		$form.trigger( 'submit' );
	} );

  $( '#location-popup .popup-close' ).bind( 'click', function( e ) {
    e.preventDefault();
    setCookie( 'cinemex_location_ask', 1, 1 );
  } );

//  if ( !getCookie( 'cinemex_tour_shown' ) ) {
//    $( '#location-popup .popup-close' ).bind( 'click.tour_show', function( e ) {
//      e.preventDefault();
//      $( this ).unbind( 'click.tour_show' );
//      start_tour( false );
//    } );
//  }
} )();


/**
 * Return Cinema matching id.
 *
 * @param {integer} id
 * @returns {area}
 */
function get_cinema_by_id( id ) {
  var result = null;

  $.each( cinemas, function( i, cinema ) {
    if ( id == cinema.id ) {
      result = cinema;
      return false; // Break loop.
    }
  } );

  return result;
}


/**
 * Return State matching id.
 *
 * @param {integer} id
 * @returns {area}
 */
function get_state_by_id( id ) {
  var result = null;

  $.each( states, function( i, state ) {
    if ( id == state.id ) {
      result = state;
      return false; // Break loop.
    }
  } );

  return result;
}


/**
 * Return Area matching id.
 *
 * @param {integer} id
 * @returns {area}
 */
function get_area_by_id( id ) {
  var result = null;

  $.each( areas, function( i, area ) {
    if ( id == area.id ) {
      result = area;
      return false; // Break loop.
    }
  } );

  return result;
}




function setup_area_w_cinema() {
  $( '.area-w-cinema-select' ).bind( 'change', function( e ) {
    e.preventDefault();
    var id      = $( this ).val(),
        area    = null,
        $cinema = $($(this).data('target'));

    if ($cinema.size() === 0) {
      $cinema = $( this ).closest( '.state-area-selection' ).find( 'select[name=cinema]' );
    }

    $cinema.children().remove();
    $( '<option/>', { value : '', text : 'Selecciona tu cine' } ).appendTo( $cinema );
    $noopt = $( '<option/>', { value : '', text : 'Selecciona primero tu zona', disabled : true } ).appendTo( $cinema );

    $cinema.val( '' );

    if ( !id )
      return;

    $.each( areas, function( i, _area ) {
      if ( id == _area.id ) {
        area = _area;
        return false;
      }
    } );

    if ( area ) {
      $.each( area.cinemas, function( i, cinema ) {
        $( '<option/>', { value : cinema.id, text : cinema.name } ).appendTo( $cinema );
      } );
      $noopt.remove();
//      if ( 2 === $cinema.children.length ) {
//        $cinema.children( ':eq(1)' ).attr( { selected : true } );
//      }
    }

    $cinema
      .trigger( 'change' )
      .trigger('cm.cinemas.updated', area);
  } );
}

$(document).bind('change_location', function(e) {
  $('#header-area-select').val('zona-' + e.cinema.area.id).trigger('change');
});

$(document).bind('user_init', function(e) {
  if ( isset('area_id', window) )
    return;
  
  if ( isset('selected_area_id', e.user) ) {
    $('#header-area-select').val('zona-' + e.user.selected_area_id).trigger('change');
  } else if ( isset('preferred_cinema', e.user) && isset('area', e.user.preferred_cinema) ) {
    $('#header-area-select').val('zona-' + e.user.preferred_cinema.area.id).trigger('change');
  }
});
