<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Promos - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Listado de Promos inactivos
              <div class="right">
                <a href="<?php echo $view['router']->generate( 'social_snack_promo_list' );?>">Ver activos</a>
              </div>
            </header>
            <div class="panel-body">
                  <div class="adv-table">
                    <!--<div class="clearfix">
                        <div class="btn-group">
                            <button class="btn green" id="editable-sample_new" onclick="window.location.href='promo_add'">
                                Nueva promo <i class="icon-plus"></i>
                            </button>
                        </div>
                    </div>-->
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Título</th>
                            <th>URL</th>
                            <th>Estado</th>
                            <th>Orden</th>
                            <th>Editar</th>
                            <th>Borrar</th>
                        </tr>
                        </thead>
                        <tbody>
<?php
foreach ($promos as $promo) {
?>
                        <tr class="gradeX">
                            <td><img src="<?php echo $view['fronthelper']->get_cms_url("promos/".$promo->getThumb(), '236x222'); ?>" /></td>
                            <td><?php echo $promo->getTitle(); ?></td>
                            <td><?php echo $promo->getUrl(); ?></td>
                            <td><?php echo $promo->getStatus(); ?></td>
                            <td><?php echo $promo->getSequence(); ?></td>
                            <td><a href="promo/<?php echo $promo->getId(); ?>">Editar</a></td>
                            <td><a href="promo_delete/<?php echo $promo->getId(); ?>"  onclick="return confirm('Estás seguro?')">Borrar</a></td>
                        </tr>
<?php
}
?>
                      </table>
                  </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#example').dataTable( {
            "aaSorting": [[ 4, "desc" ]]
        } );
    } );
</script>
<?php $view['slots']->stop(); ?>