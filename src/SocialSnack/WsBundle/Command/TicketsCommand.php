<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Ticket;

class TicketsCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:tickets' )
			->setDescription( 'Fetch tickets from the Web Service' )
      ->addOption( 'cinema_id', 'cid', InputOption::VALUE_OPTIONAL );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
		$time_start = microtime(true); 

		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$em           = $doctrine->getManager();
		$req          = $this->getContainer()->get( 'cinemex_ws' );
		$cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
		$ticket_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Ticket' );
		$states       = array();
		$areas        = array();
		
    if ( $cinema_id = $input->getOption( 'cinema_id' ) ) {
      $cinemas = $cinema_repo->findBy( array( 'id' => $cinema_id ) );
    } else {
      $cinemas = $cinema_repo->findAll();
    }
    
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
		foreach ( $cinemas as $cinema ) {
			$output->writeln( $cinema->getName() );
      $this->getContainer()->get('old_sound_rabbit_mq.update_tickets_producer')->publish( $cinema->getLegacyId() );
		}

		
		$output->writeln( 'Didn\'t fail, yay!' );
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		
		$output->writeln( 'otal Execution Time: '. $execution_time . ' Mins' );

		return;
	}
	
}