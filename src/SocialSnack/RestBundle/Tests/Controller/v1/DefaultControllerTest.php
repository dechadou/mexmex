<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;

class DefaultControllerTest extends WebTestCase {


  public function testRecoverPass() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
    ]);

    $email = LoadUserData::$users[0]->getEmail();

    $client = static::createClient();

    $client->enableProfiler();

    $client->request(
        'POST',
        '/rest/recoverPass',
        [
            'email' => $email,
        ]
    );

    $mailCollector = $client->getProfile()->getCollector('swiftmailer');

    $collectedMessages = $mailCollector->getMessages();
    $message = $collectedMessages[0];

    // Asserting email data
    $this->assertInstanceOf('Swift_Message', $message);

    // Since emails in dev environments are sent to a hardcoded testing address,
    // the actual recipient is stored in the "X-Swift-To" header.
    $x_to = $message->getHeaders()->get('X-Swift-To')->getNameAddresses();
    $this->assertEquals($email, key($x_to));
  }


  /**
   * @todo Test that the entry was actually persisted.
   */
  public function testRegisterGCM() {
    $client = static::createClient();
    $client->request(
        'POST',
        '/rest/registerGCM',
        [
            'uuid'  => 'foo',
            'gcmid' => 'bar',
            'type'  => 'phpunit'
        ]
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }


  protected function _testAppVersionsAction($device, $expected_version) {
    $url = sprintf('/rest/appVersions/%s', $device);
    $client = static::createClient();
    $client->request(
        'GET',
        $url
    );
    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $json = @json_decode($response->getContent());
    $this->assertInstanceOf('\stdClass', $json);
    $this->assertObjectHasAttribute('version', $json);
    $this->assertEquals($json->version, $expected_version);
  }

  public function testAppVersionsAction() {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppVersionsData',
    ]);

    $this->_testAppVersionsAction('ios', 'i2.0');
    $this->_testAppVersionsAction('android', 'a2.0');
  }

} 