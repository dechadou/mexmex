<?php

namespace SocialSnack\AdminBundle\Controller;

use Buzz\Exception\RequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SocialSnack\WsBundle\Command\MoviesEnqueueCommand;
use SocialSnack\WsBundle\Command\TasksConsumeCommand;
use SocialSnack\WsBundle\Command\CinemasCommand;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Request\Consulta;
use SocialSnack\WsBundle\Request\ConsultaAbstract;
use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/ws")
 */
class WebserviceController extends BaseController {

  
  protected function get_required_role() {
    return 'ROLE_PROCESS';
  }


  /**
   * @Route("/", name="social_snack_admin_ws_index")
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function defaultAction() {
    $this->check_roles();
    
    $state_repo  = $this->getDoctrine()->getRepository('SocialSnackWsBundle:State');
    $states      = $state_repo->findAllWithAreas();
    $cinema_repo = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas     = $cinema_repo->findAll(FALSE, FALSE);
    
    $output = $this->getRequest()->getSession()->getFlashBag()->get('ws_output');
    
    return $this->render('SocialSnackAdminBundle:Webservice:index.html.php', array(
        'states'  => $states,
        'cinemas' => $cinemas,
        'output'  => $output,
    ));
  }


  /**
   * @Route("/updateSessions", name="social_snack_admin_ws_update_sessions")
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function updateSessionsAction() {
    $this->check_roles();

    $em = $this->getDoctrine()->getManager();
    $cinema_id = $this->getRequest()->request->get('cinema_id');

    $task = new Task();
    $task
        ->setService('cinemex.session_updater')
        ->setData(serialize(array(
            'cinema_id' => $cinema_id,
            'propagate' => TRUE,
            'enqueue'   => FALSE,
            'source'    => 'manual',
        )))
        ->setPriority(1) // Highest priority
    ;
    $em->persist($task);
    $em->flush();

    $this->getRequest()->getSession()->getFlashBag()->set('ws_output', 'Tarea creada exitosamente. Se ejecutará en los próximos minutos.');

    return $this->redirect($this->generateUrl('social_snack_admin_ws_index'));
  }
  
  
  public function updateMoviesAction() {
    $this->check_roles();
    die('This method needs refactoring');
    $updater   = $this->get('cinemex.movie_updater');
    /* @var $updater \SocialSnack\WsBundle\Service\MovieUpdater */
    $cinema_id = $this->getRequest()->request->get('cinema_id');
    $updated   = $updater->execute(array(
        'cinema_id' => $cinema_id,
        'propagate' => TRUE,
        'enqueue'   => FALSE,
    ));
    
    if ($updated && $updater->getNewMovies()) {
      $input   = new ArrayInput(array());
      $output = new NullOutput();
      // Group.
      $command = new \SocialSnack\WsBundle\Command\MoviesGroupCommand();
      $command->setContainer($this->container);
      $command->run($input, $output);
      
      // Order.
      $command = new \SocialSnack\WsBundle\Command\MoviesOrderCommand();
      $command->setContainer($this->container);
      $command->run($input, $output);
    }
    
    $output = $updater->getConsoleLog();
    $this->getRequest()->getSession()->getFlashBag()->set('ws_output', $output);
    
    return $this->redirect($this->generateUrl('social_snack_admin_ws_index'));
  }
  
  
  public function updateCinemasAction() {
    $this->check_roles();
    
    $updater = $this->get('cinemex.cinema_updater');
    /* @var $updater \SocialSnack\WsBundle\Service\CinemaUpdater */
    $arg     = explode('/', $this->getRequest()->request->get('area_id'));
    $args    = array(
        'add_only'  => TRUE,
    );
    if ('area' === $arg[0]) {
      $args['area_id'] = $arg[1];
    } else {
      $args['state_id'] = $arg[1];
    }
    $updated = $updater->execute($args);
    
    // Recalculate distances.
    if ($updated && $updater->getNewCinemas()) {
      $em   = $this->getDoctrine()->getManager();
      $task = new Task();
      $task
          ->setService('cinemex.cinema_distance_updater')
          ->setData(serialize(['cinema_legacy_ids' => $updater->getNewCinemasIds()]))
      ;
      $em->persist($task);
      $em->flush();
    }
    
    $output = $updater->getConsoleLog();
    $this->getRequest()->getSession()->getFlashBag()->set('ws_output', $output);
    
    return $this->redirect($this->generateUrl('social_snack_admin_ws_index'));
  }
  
  
  /**
   * @todo Remove this action.
   */
  public function moviesenqueueAction() {
    $this->check_roles();
    
    $command = new MoviesEnqueueCommand();
    $command->setContainer($this->container);
    $input = new ArrayInput(array());
    $output = new NullOutput();
    $resultCode = $command->run($input, $output);

    $commandTask = new TasksConsumeCommand();
    $commandTask->setContainer($this->container);
    $resultCode = $commandTask->run($input, $output);

    return $this->render('SocialSnackAdminBundle:Default:moviesProcess.html.php');
  }

  
  /**
   * @todo Remove this action.
   */
  public function cinesenqueueAction() {
    $this->check_roles();
    
    $command = new CinemasCommand();
    $command->setContainer($this->container);
    $input = new ArrayInput(array());
    $output = new ConsoleOutput();
    $resultCode = $command->run($input, $output);
    $output->getStream();
    return $this->render('SocialSnackAdminBundle:Default:moviesProcess.html.php');
  }
    
  
  public function importMoviesAction() {
    $this->check_roles();
    
    $conn = $this->getDoctrine()->getConnection();
    $stmt = $conn->prepare('SELECT COUNT(*) FROM Cinema WHERE active = 1;');
    /* @var $stmt \Doctrine\DBAL\Statement */
    $stmt->execute();
    $res = $stmt->fetchColumn();
    
    return $this->render('SocialSnackAdminBundle:Webservice:importMovies.html.php', array('cinemas_count' => $res));
  }

  public function importMoviesIdsAction() {
    $this->check_roles();
    
    $req = $this->getRequest();
    $start = $req->request->get('page');
    if (!$start) {
      $start = 1;
    }
    $items_per_page = 5;
    $offset = ($start - 1) * $items_per_page;
    
//    if ($start > 5) {
//      return new JsonResponse(array('ids' => array()));
//    }
    
    $repo = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Cinema');
    $qb = $repo->createQueryBuilder('c');
    $qb->select('c.legacy_id');
    $qb->where('c.active = 1');
    $qb->setFirstResult($offset);
    $qb->setMaxResults($items_per_page);
    $qb->orderBy('c.id', 'ASC');
    $query = $qb->getQuery();
    $result = $query->getResult();
    
    $cinemas_ids = array_map(function($a) { return $a['legacy_id']; }, $result);
    
    if (!sizeof($cinemas_ids)) {
      return new JsonResponse(array('complete' => TRUE));
    }
    
    $chs = array();
    $master = curl_multi_init();
    $ids = array();

    foreach ($cinemas_ids as $cinema_id) {
      $req = $this->container->get('cinemex_ws');
      $req->init( 'Consulta' );
      $ch = $req->async_request(array(
          'TipoConsulta' => Consulta::TIPOCONSULTA_PELICULAS,
          'CodigoCine'   => $cinema_id,
          'CodigoEstado' => '',
          'CodigoZona'   => '',
      ));
      curl_multi_add_handle($master, $ch);
      $chs[] = $ch;
    }

    do {
      curl_multi_exec($master, $running);
    } while($running > 0);

    for ($i = 0; $i < sizeof($chs); $i++) {
      try {
        $res = curl_multi_getcontent( $chs[$i] );
        $res = $req->parse_response($res);
        foreach ($res as $movie) {
          $ids[] = (string)$movie->FILMCODE . '|' . (string)$movie->MOVIE;
        }
      } catch (\Exception $e) {
        curl_multi_remove_handle($master, $chs[$i]);
      }
    }
    
    curl_multi_close($master);
    
    $ids = array_unique($ids);
    $ids = array_values($ids);
    return new JsonResponse(array('ids' => $ids));
  }
  
  
  public function importMoviesInfoAction() {
    $this->check_roles();
    
    $req = $this->getRequest();
    $ids = $req->request->get('ids');
    $ids = array_unique($ids);
    
    $query = "SELECT legacy_id FROM Movie WHERE legacy_id IN (" . implode(',', $ids) . ");";
    $conn = $this->getDoctrine()->getConnection();
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $results = $stmt->fetchAll();
    
    $existing_ids = array_map(function($a) { return $a['legacy_id']; }, $results);
    
    $new_ids = array_diff($ids, $existing_ids);
    $movies = array();
    
    foreach ($new_ids as $legacy_id) {
      try {
        $req = $this->container->get('cinemex_ws');
        $req->init( 'ConsultaInfoPelicula' );
        $movie_info = $req->request( array(
            'Opcion'         => 12,
            'CodigoPelicula' => $legacy_id,
        ) );
      } catch ( \Exception $e ) {
        continue;
      }
      
      $movies[] = array(
          'id' => (string)$movie_info->FILMCODE,
          'key' => (string)$movie_info->MOVIE,
          'name' => (string)$movie_info->MOVIENAME,
          'c' => (string)$movie_info->TRAILER && (string)$movie_info->SINOPSIS,
      );
    }
    
    return new JsonResponse(array('movies' => $movies));
  }
  
  
  public function importMoviesProcessAction() {
    $this->check_roles();
    
    set_time_limit(5 * 60);
    
    $req = $this->getRequest();
    $legacy_id = $req->request->get('id');
    $movie_key = $req->request->get('key');
    $em = $this->getDoctrine()->getManager();
    
    try {
      $req = $this->container->get('cinemex_ws');
      $req->init( 'ConsultaInfoPelicula' );
      $movie_info = $req->request( array(
          'Opcion'         => 12,
          'CodigoPelicula' => $legacy_id,
      ) );
    } catch ( \Exception $e ) {
      return new JsonResponse(array('success' => FALSE), 500);
    }

    $movie = new Movie();

    list( $title, $attrs ) = WsHelper::parse_attrs_and_clean_title( $movie_info->MOVIENAME );

    $movie->setLegacyId( (int)$movie_info->FILMCODE );
    $movie->setLegacyIdBis( (string)$movie_key );
    $movie->setLegacyName( (string)$movie_info->MOVIENAME );
    $movie->setInfo( json_encode( $movie_info ) );
    $movie->setPremiere( (bool)(string)$movie_info->ESTRENO );
    if($movie_info->ESTRENO){
      $movie->setFixedPosition(500);
    }
    $movie->setGroupId( (int)$movie_info->CLAVEAGRUPACION );
    $movie->setActive( 0 );
    $movie->setName( ucwords( strtolower( $title ) ) );
    $movie->setShortName( ucwords( strtolower( $movie_info->MOVIESHORTNAME ) ) );
    $movie->setPosterUrl( md5( gethostname() . time() . (string)$movie_info->FILMCODE ) . '.jpg' );
    $movie->setAttributes( json_encode( (object)$attrs ) );
    $this->get('ss.ws.utils')->process_posters( $movie, $movie_info, FALSE );

    $em->persist($movie);
    $em->flush();
    
    return new JsonResponse(array('succes' => TRUE));
  }
  
  
  public function groupAndOrderAction(Request $request) {
    $this->check_roles();
    
    $input   = new ArrayInput(array());
    $output = new NullOutput();
    // Group.
    $command = new \SocialSnack\WsBundle\Command\MoviesGroupCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);

    // Order.
    $command = new \SocialSnack\WsBundle\Command\MoviesOrderCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);
    
    $redirect = $request->request->get('redirect');
    if (!$redirect) {
      return new JsonResponse(array('success' => TRUE));
    } else {
      return $this->redirect($redirect);
    }
  }


  public function sessionDebuggerAction(Request $request) {
    $this->check_roles();

    $session_id = '';
    $result     = NULL;

    if ($request->query->has('session_id')) {
      $session_id    = $request->query->get('session_id');
      $debug_handler = $this->get('admin.debug.handler');
      $result        = $debug_handler->debugSession($session_id);
    }

    return $this->render(
        'SocialSnackAdminBundle:Webservice:sessionDebugger.html.php',
        [
            'result'     => $result,
            'session_id' => $session_id,
        ]
    );
  }


  public function pingWsServersAction(Request $request) {
    $this->check_roles();

    $ws = $this->container->getParameter('cinemex_ws_host');

    /** @var \Memcached */
    $memcached = $this->get('memcached');

    /** @var \Buzz\Browser $browser */
    $browser = $this->get('gremo_buzz');
    $browser->getClient()->setTimeout(15);

    $result = [];
    $result2 = [];

    if ('POST' === $request->getMethod()) {
      foreach ($ws as $ip) {
        try {
          $res    = $browser->get('https://' . $ip . ConsultaAbstract::PATH);
          $status = $res->getStatusCode();
          $msg    = 'OK';
        } catch (RequestException $e) {
          $status = NULL;
          $msg    = $e->getMessage();
        }

        $fails = $memcached->get('ws_fails_node_' . $ip);
        if ($fails >= 30) {
          $health = 'danger';
        } elseif ($fails > 5) {
          $health = 'warning';
        } else {
          $health = 'stable';
        }


        $result[] = [
            'ip'     => $ip,
            'status' => $status,
            'msg'    => $msg,
            'health' => $health,
        ];
      }
    }

    return $this->render('SocialSnackAdminBundle:Webservice:pingWsServers.html.php', [
        'result' => $result,
    ]);
  }



  public function cinemasUpdatesLogAction(Request $request) {
    $this->check_roles();

    $cinemas = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Cinema')->findAll();
    $entries = $this->getDoctrine()->getRepository('SocialSnackWsBundle:CinemaUpdateLog')->findAllAssoc();

    return $this->render('SocialSnackAdminBundle:Webservice:cinemasUpdatesLog.html.php', [
        'cinemas' => $cinemas,
        'entries' => $entries
    ]);
  }


  /**
   * @Route("/status/", name="socialsnack_admin_ws_statusPanel")
   * @Method({"GET"})
   *
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function statusPanelAction(Request $request) {
    return $this->render('SocialSnackAdminBundle:Webservice:statusPanel.html.php');
  }


  /**
   * @Route("/status/data", name="socialsnack_admin_ws_statusData")
   * @Method({"GET"})
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function statusDataAction(Request $request) {
    /** @var \Memcached $mem */
    $mem    = $this->get('memcached');
    $keys   = $mem->getAllKeys();
    $result = [];

    foreach ($keys as $key) {
      if (preg_match('/^ws_fails_/', $key)) {
        $val = $mem->get($key);
        $result[$key] = $val;
      }
    }

    ksort($result);

    return new JsonResponse($result);
  }


}
