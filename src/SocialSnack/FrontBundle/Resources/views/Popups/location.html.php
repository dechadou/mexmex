<span class="location-popup-header"></span>
<span class="location-popup-arrow"></span>

<div id="location-error" class="msg msg-error"></div>

<form id="location-form">
  <p class="popup-title">Escoge tu ciudad y cine de preferencia</p>

  <div class="state-area-selection clearfix">
    <div class="half">
      <select name="area" class="location-select icon-darr area-w-cinema-select">
        <option value="">Selecciona tu ciudad</option>
        <?php foreach ( $states as $_state ) { ?>
        <optgroup label="<?php echo $_state->getName(); ?>">
          <?php foreach ( $_state->getAreas() as $_area ) { ?>
          <option value="<?php echo $_area->getId(); ?>"><?php echo $_area->getName(); ?></option>
          <?php } ?>
        </optgroup>
        <?php } ?>
      </select>
    </div>
    <div class="half">
      <select name="cinema" class="location-select icon-darr" rel="set_cinema">
        <option value="">Selecciona tu cine</option>
        <option value="" disabled="disabled">Selecciona primero tu zona</option>
      </select>
    </div>
  </div>

  <p class="or-sep"><span class="or">O escribe el nombre de tu cine favorito</span></p>
	<input type="text" name="location" class="location-input" id="location-input" placeholder="Ingresa el nombre del cine" autocomplete="off" />

	<p class="popup-small">
		Usaremos tu ubicación para ofrecerte las mejores películas en los Cinemex más cercanos.
	</p>
	<p class="popup-small api-enabled">
    <a href="#" rel="ask-location" data-api="1" class="btn btn-small btn-icon icon icon-marker">Encontrar mi ubicación automáticamente</a>
	</p>
</form><!-- #location-form -->

<div id="location-api">
  <p class="popup-title">Ayúdanos a encontrar las películas y horarios ideales para ti.</p>

  <span href="#" class="popup-loading-txt">Obteniendo tu ubicación</span>

	<p class="popup-small">
    Es posible que tu navegador solicite permiso para acceder a tu ubicación.
    Haz click en "Permitir" para localizar tu Cinemex más cercano.
	</p>
	<p class="popup-small">
    <a href="#" rel="ask-location" class="btn btn-small btn-icon icon icon-marker">O puedes ingresar ubicación manualmente</a>
	</p>
</div>

<div id="location-results">
  <p class="popup-title">Ayúdanos a encontrar las películas y horarios ideales para ti.</p>

	<span class="popup-subtitle">Estos son los cines que encontramos cerca de ti, selecciona tu preferido:</span>

	<ul></ul>

	<div id="location-alts">
		<a href="#" rel="ask-location" class="btn btn-icon icon icon-reload">Buscar otro cine</a>
    <div class="api-enabled">
      <a href="#" rel="ask-location" data-api="1" class="btn btn-icon icon icon-marker">Establecer mi ubicación</a>
    </div>
	</div>
</div><!-- #location-results -->

<a href="#" class="popup-close icon-small icon-close no-txt">Cerrar</a>
