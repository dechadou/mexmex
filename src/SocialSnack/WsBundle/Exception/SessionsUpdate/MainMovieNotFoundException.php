<?php

namespace SocialSnack\WsBundle\Exception\SessionsUpdate;

/**
 * Class MovieNotFoundException
 * @package SocialSnack\WsBundle\Exception\SessionsUpdate
 * @author Guido Kritz
 */
class MainMovieNotFoundException extends \Exception {

} 