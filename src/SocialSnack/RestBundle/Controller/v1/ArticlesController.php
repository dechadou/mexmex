<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticlesController extends BaseController {

	/**
	 * @Route("/", name="rest_v1_articles_default")
	 * @Method({"GET"})
	 */
	public function defaultAction() {
    return $this->forward('SocialSnackRestBundle:Articles:default');
	}


	/**
	 * @Route("/search", name="rest_v1_articles_search")
	 * @Method({"GET"})
	 */
	public function searchAction(Request $request) {
    return $this->forward('SocialSnackRestBundle:Articles:search', ['request' => $request]);
	}


	/**
	 * Return detailed info for article(s).
	 *
	 * @Route("/{id}", name="rest_v1_articles_single", requirements={"id"="[0-9]+"})
	 * @Method({"GET"})
	 */
	public function detailsAction($id, Request $request) {
      return $this->forward('SocialSnackRestBundle:Articles:details', ['id' => $id, 'request' => $request]);
	}

}