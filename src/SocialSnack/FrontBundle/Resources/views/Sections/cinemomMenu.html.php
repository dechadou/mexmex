<?php
$menu = array(
    array(
        'label' => 'CineMá',
        'route' => 'cinemom',
    ),
    array(
        'label' => 'Cines Sedes',
        'route' => 'cinemom_cinemas',
    ),
    array(
        'label' => 'Preguntas Frecuentes',
        'route' => 'cinemom_faq',
    ),
    array(
        'label' => 'Términos y Condiciones',
        'route' => 'cinemom_terms',
    ),
//    array(
//        'label' => 'Cartelera',
//        'route' => 'cinemom_movies',
//    ),
);
?>
<div class="section-header section-cinemom">
  <img src="<?php echo $view['assets']->getUrl( 'assets/img/header-cinemom.jpg' ); ?>" alt="CineMá" />
  <div class="section-nav">
    <ul>
      <?php foreach ( $menu as $item ) { ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'] ); ?>" class="<?php if ( $route == $item['route'] ) echo 'selected'; ?>"><?php echo $item['label']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>