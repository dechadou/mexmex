<div class="section-content landing-content landing-content-bg">
  <h1 class="landing-title"><?php echo $page->getName(); ?></h1>

  <div class="landing-grid-wrap">
    <?php
    $limit  = 4;
    $offset = 0;

    while ($row = array_slice($movies, $offset, $limit)) :
      $offset += $limit;
      ?>
      <div class="landing-grid">
        <?php foreach ($row as $match) : ?>
          <div  class="item">
            <a href="<?php echo $view['fronthelper']->get_movie_link($match); ?>">
              <img src="<?php echo $view['fronthelper']->get_poster_url($match->getPosterUrl(), '182x272'); ?>">

              <span class="btn btn-default btn-icon icon icon-ticket">Comprar boletos</span>
            </a>
          </div>
        <?php endforeach; ?>
      </div>

      <div class="clearfix"></div>
    <?php endwhile; ?>
  </div>
</div>