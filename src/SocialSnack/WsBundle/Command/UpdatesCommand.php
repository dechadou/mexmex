<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use SocialSnack\WsBundle\Command\MoviesGroupCommand;
use SocialSnack\WsBundle\Command\MoviesOrderCommand;
use SocialSnack\WsBundle\Entity\Task;
use SocialSnack\WsBundle\Entity\WsUpdate;

class UpdatesCommand extends ContainerAwareCommand {

  protected $time_limit = 300;
  
	protected function configure() {
		$this
			->setName( 'ws:updates' )
			->setDescription( 'Get updates from the Web Service' );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		
    $doctrine = $this->getContainer()->get( 'doctrine' );
		$em       = $doctrine->getManager();
		$req      = $this->getContainer()->get( 'cinemex_ws' );
    $args     = array();
    $conn     = $doctrine->getConnection();
    $query    = "SELECT `code` FROM WsUpdate ORDER BY `date_added` DESC LIMIT 1";
		$stmt     = $conn->prepare($query);
    $stmt->execute();
    $code     = $stmt->fetchColumn();
    
    if ( $code ) {
      $args['Codigo'] = $code;
    }
    
    try {
      $req->init( 'ConsultaActualizaciones' );
      $res = $req->request($args);
    } catch ( \SocialSnack\WsBundle\Exception\RequestMsgException $e ) {
      return FALSE;
    }
    
    // Store the new Update info.
    $code = $res->CodigoMax;
    
    $cinema_ids = array();
    if (isset($res->CINES)) {
      foreach ($res->CINES->CINE as $_cinema) {
        $cinema_ids[] = (string)$_cinema->Clave;
      }
    }
    
    $update = new WsUpdate();
    $update->setCode($code);
    $update->setData(json_encode($cinema_ids));
    $em->persist($update);
    $em->flush();

		set_time_limit($this->time_limit);
    
    // Process the updates.
    $output->writeln( 'Process tasks...' );
    
    /* @var $router \Symfony\Component\Routing\Router */
    $router = $this->getContainer()->get('router');
    
    /* @var $purger \SocialSnack\FrontBundle\Service\CachePurger */
    $purger = $this->getContainer()->get('ss.cache_purger');
    
    /* @var $front_utils \SocialSnack\FrontBundle\Service\Utils */
    $front_utils = $this->getContainer()->get('ss.front.utils');
    
    /* @var $service \SocialSnack\WsBundle\Service\SessionUpdater */
    $service = $this->getContainer()->get('cinemex.session_updater');
    $service->setConsoleVerbose();
    
    // Query all the cinemas together to save some performance.
    $cinema_repo = $this->getContainer()->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    $cinemas = $cinema_repo->findManyByLegacyIdBy($cinema_ids);
    
    // Now process each cinema.
    foreach ($cinemas as $cinema) {
      /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */

      $task = new Task();
      $task
          ->setService('cinemex.session_updater')
          ->setData(serialize(array(
              'cinema_id' => $cinema->getId(),
              'propagate' => TRUE,
              'enqueue'   => FALSE,
              'source'    => 'ws',
          )))
          ->setPriority(3)
      ;
      $em->persist($task);
    }

    $task = new Task();
    $task
        ->setService('cinemex.groupandorder')
        ->setPriority(3)
    ;
    $em->persist($task);

    $em->flush();

    $output->writeln( 'Done!' );
		return;
	}
	
}