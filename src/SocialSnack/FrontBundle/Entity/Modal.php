<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Modal
 *
 * @ORM\Table(name="Modal")
 * @ORM\Entity(repositoryClass="ModalRepository")
 */
class Modal
{

	const STATUS_PUBLISH = 'publish';
	const STATUS_DRAFT   = 'draft';

	const TYPE_IMAGE = 'image';
	const TYPE_SWF = 'swf';
	const TYPE_VIDEO = 'video';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string")
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="file", type="string", length=255, nullable=true)
	 */
	private $file;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="swf_width", type="string", nullable=true)
	 */
	private $width=null;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="swf_height", type="string", nullable=true)
	 */
	private $height=null;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clicktag", type="string", nullable=true)
	 */
	private $clicktag=null;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", nullable=true)
	 */
	private $url=null;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="section", type="array")
	 */
	private $section;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="status", type="string", length=40)
	 */
	private $status;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_created", type="datetime")
	 */
	private $dateCreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_updated", type="datetime", nullable=true)
	 */
	private $dateUpdated=null;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="delay", type="integer")
	 */
	private $delay = 10;


  /**
   * @var integer
   *
   * @ORM\Column(name="only_once", type="integer")
   */
  private $only_once = 0;

  /**
   * @var string
   *
   * @ORM\Column(name="position", type="string")
   */
  private $position = "center";

  /**
   * @var ArrayCollection
   *
   * @ORM\OneToMany(targetEntity="ModalTarget", mappedBy="modal")
   */
  protected $targets;

  /**
   * @var array
   *
   * @ORM\Column(name="dias", type="array")
   */
  private $dias;


	public function __construct() {
		$this->delay = 10;
		$this->file = '';
		$this->section = [];
		$this->type = self::TYPE_IMAGE;
		$this->position = 'center';
		$this->setDateCreated(new \DateTime('now'));
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Modal
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Set width
	 *
	 * @param string $width
	 * @return Modal
	 */
	public function setWidth($width)
	{
		$this->width = $width;

		return $this;
	}

	/**
	 * Set height
	 *
	 * @param string $height
	 * @return Modal
	 */
	public function setHeight($height)
	{
		$this->height = $height;

		return $this;
	}

	/**
	 * Get width
	 *
	 * @return string
	 */
	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * Get height
	 *
	 * @return string
	 */
	public function getHeight()
	{
		return $this->height;
	}

	/**
	 * Set url
	 *
	 * @param string $url
	 * @return Modal
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * Get url
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Modal
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set section
	 *
	 * @param array $section
	 * @return Modal
	 */
	public function setSection($section)
	{
		$this->section = $section;

		return $this;
	}

	/**
	 * Get section
	 *
	 * @return array
	 */
	public function getSection()
	{
		return $this->section;
	}

	/**
	 * Set file
	 *
	 * @param string $file
	 * @return Modal
	 */
	public function setFile($file)
	{
		$this->file = $file;

		return $this;
	}

	/**
	 * Get file
	 *
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}


	/**
	 * Set dateCreated
	 *
	 * @param \DateTime $dateCreated
	 * @return Article
	 */
	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

	/**
	 * Get dateCreated
	 *
	 * @return \DateTime
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * Set dateUpdated
	 *
	 * @param \DateTime $dateUpdated
	 * @return Article
	 */
	public function setDateUpdated($dateUpdated)
	{
		$this->dateUpdated = $dateUpdated;

		return $this;
	}

	/**
	 * Get dateUpdated
	 *
	 * @return \DateTime
	 */
	public function getDateUpdated()
	{
		return $this->dateUpdated;
	}

	/**
	 * Set status
	 *
	 * @param string $status
	 * @return Article
	 */
	public function setStatus($status)
	{
		if ($status == 'publish') $status = 1;
		if ($status == 'draft') $status = 0;
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set delay
	 *
	 * @param integer $delay
	 * @return Modal
	 */
	public function setDelay($delay)
	{
		$this->delay = $delay;

		return $this;
	}

	/**
	 * Get delay
	 *
	 * @param integer $delay
	 * @return integer
	 */
	public function getDelay()
	{
		return $this->delay;
	}

	/**
	 * Get clicktag
	 *
	 * @return string
	 */
	public function getClickTag()
	{
		return $this->clicktag;
	}

	/**
	 * Set clicktag
	 *
	 * @param string $clicktag
	 * @return Modal
	 */
	public function setClickTag($clicktag)
	{
		$this->clicktag = $clicktag;
		return $this;
	}


  /**
   * Get only_once
   *
   * @return integer
   */
  public function getOnlyOnce()
  {
    return $this->only_once;
  }

  /**
   * Set only_once
   *
   * @param integer $only_once
   * @return Modal
   */
  public function setOnlyOnce($only_once)
  {
    $this->only_once = $only_once;
    return $this;
  }

  /**
   * Get position
   *
   * @return string
   */
  public function getPosition()
  {
    return $this->position;
  }

  /**
   * Set position
   *
   * @param string $position
   * @return Modal
   */
  public function setPosition($position)
  {
    $this->position = $position;
    return $this;
  }

  /**
   * Get dias
   *
   * @return json_array
   */
  public function getDias()
  {
    //$dias = unserialize($this->dias);
    return $this->dias;
  }

  /**
   * Set dias
   *
   * @param json_array $dias
   * @return Modal
   */
  public function setDias($dias)
  {
    $this->dias = $dias;
    return $this;
  }

  /**
   * get targets
   *
   * @param string $targets
   * @return Modal
   */
  public function getTargets(){
    return $this->targets;
  }

}