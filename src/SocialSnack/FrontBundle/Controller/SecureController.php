<?php

namespace SocialSnack\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SecureController extends Controller {
  
  /**
   * @Route("/postMessage/", name="post_message_iframe")
   * @Template(engine="php")
   */
  public function postMessageAction() {
    $response = $this->render('SocialSnackFrontBundle:Secure:postMessage.html.php');
    $response->setPublic();
    $response->setMaxAge(7 * 24 * 60 * 60);
    $response->setSharedMaxAge(7 * 24 * 60 * 60);
    return $response;
  }
  
}