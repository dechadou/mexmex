<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

abstract class ArcoAbstractType extends AbstractType
{
  
  protected $ws;

  
  public function __construct($ws = FALSE) {
    $this->ws = $ws;
  }
  
  
  public function getWsChoices( $request_type, $choices = array() ) {
    $req = $this->ws;
    $req->init('Arco\MuestraCatalogos');
    $res = $req->request(array(
        'catalogo' => $request_type
    ));

    foreach ($res as $r) {
      $choices[(string)$r->CVE] = (string)$r->DESDESC;
    }

    return $choices;
  }

  
}