<?php

namespace SocialSnack\WsBundle\Request;

use SocialSnack\WsBundle\Exception\RequestMsgException;
use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;

class ConsultaDetallada extends ConsultaAbstract {

	const			TIPOCONSULTA_PELICULAS          = 1;
	const     TIPOCONSULTA_CINES              = 2;
	
	protected $method       = 'ConsultaDetallada';
	
	protected $default_args = array(
			'CodigoCine'        => 0,
			'CodigoEstado'      => 0,
			'CodigoZona'        => 0,
	);
	
	
	/**
	 * Sadly the WS response doesn't follow the XML specifications and returns
	 * an invalid document. Some ugly modifications should be applied to the
	 * string before decoding into an XML object.
	 * 
	 * @param  string $xml
	 * @return string
	 */
	protected function additional_format( $xml ) {
		$xml = preg_replace( '/\<SINOPSIS\>(.*?)\<\/SINOPSIS\>/s', '<SINOPSIS><![CDATA[$1]]></SINOPSIS>', $xml );
		$xml = preg_replace( '/\<DIRECCION\>(.*?)\<\/DIRECCION\>/s', '<DIRECCION><![CDATA[$1]]></DIRECCION>', $xml );
		return $xml;
	}
	
	
	/**
	 * Parse the request and return only the relevant nodes.
	 *
   * @throws \Exception
   * @throws RequestMsgException
   * @throws UnexpectedWsResponseException
	 * @param \SimpleXMLElement $xml
	 * @return \SimpleXMLElement
	 */
	public function parse_res( $xml ) {
		$node = '';
		switch ( $this->args[ 'TipoConsulta' ] ) {
			case self::TIPOCONSULTA_CINES:
				$node = 'CINE';
				break;
			case self::TIPOCONSULTA_PELICULAS:
				$node = 'PELICULA';
				break;
		}
		
		if ( !$node ) {
			throw new \Exception( 'Invalid parameter TipoConsulta.' );
    }

		if ( !isset( $xml->{$node} ) ) {
			if ( $xml->MENSAJE ) {
				throw new RequestMsgException( $xml->MENSAJE );
      } else {
        throw new UnexpectedWsResponseException( 'Invalid response 1:' . json_encode( $xml ) );
      }
		}

		return $xml->{$node};
	}
	
}