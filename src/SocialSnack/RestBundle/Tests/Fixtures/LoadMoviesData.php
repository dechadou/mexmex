<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\WsBundle\Entity\Movie;

class LoadMoviesData extends AbstractFixture implements OrderedFixtureInterface {

  static $movies;

  public function load(ObjectManager $manager) {
    self::$movies = [];

    $movie1 = new Movie();
    $movie1
        ->setName('Movie 1')
        ->setLegacyName('MOVIE 1 DIG ENG')
        ->setLegacyId(1001)
        ->setActive(TRUE)
        ->setAttributes(json_encode(['lang_en' => TRUE]))
        ->setFeatured(TRUE)
        ->setFixedPosition(500)
        ->setGroupId('111')
        ->setInfo(json_encode(['DIRECTOR' => 'Tarantino']))
        ->setPosition(50)
        ->setPosterUrl('poster.jpg')
        ->setPremiere(FALSE)
        ->setScore(4)
        ->setShortName('MOVIE 1')
        ->setType('single')
    ;
    $this->addReference('Movie-1', $movie1);
    self::$movies[] = $movie1;
    $manager->persist($movie1);

    $manager->flush();
  }

  public function getOrder() {
    return 4;
  }

}