<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Detrás de la magia');
$view['slots']->set('body_class', 'sponsored-magia');
?>

<?php $view['slots']->start('body'); ?>
<div id="landing-magia">
    <div class="magia-header">
        <img src="<?php echo $view['assets']->getUrl('assets/img/landings/detrasdelamagia/magia-logo.png'); ?>">
        <h1>Detras de la mágia</h1>
    </div>

    <div id="magia-main" class="magia-main"></div>

    <div class="magia-sidebar">
        <h2>Checa más entrevistas</h2>

        <ul id="magia-list" class="magia-sidebar-list"></ul>
    </div>

    <div class="clearfix"></div>
</div>
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('js_after_vendor'); ?>
<script id="tpl-magia-list-item" type="text/template">
<li>
    <a href="https://youtu.be/{{id}}" data-permalink="{{permalink}}" data-video="{{video}}">
        <img src="{{image}}">
        <span class="title">{{title}}</span>
        <span class="author">de {{author}}</a>
    </a>
</li>
</script>

<script id="tpl-magia-player" type="text/template">
<div class="magia-main-player">
    <iframe id="magia-player" src="//youtube.com/embed/{{id}}"></iframe>
</div>

<div class="magia-main-comments">
    <h2>Comentarios</h2>

    <div class="fb-comments" data-href="<?php echo $view['router']->generate('landing/detrasdelamagia', [], TRUE); ?>?video={{id}}" data-width="100%" data-numposts="10" data-colorscheme="light"></div>
</div>
</script>

<script>
;(function($){
    function parse_item(template, data) {
        return template
            .replace(/{{id}}/g,      data.snippet.resourceId.videoId)
            .replace(/{{author}}/g, 'Cinemex')
            .replace(/{{image}}/g,  data.snippet.thumbnails.default.url)
            .replace(/{{title}}/g,  data.snippet.title);
    }

    function set_playlist(list, template, items) {
        $.each(items, function(){
            var item = parse_item(template, this);

            item = $(item).data(this);

            list.append(item);
        });
    }

    function set_player(container, template, item) {
        var player = parse_item(template, item);

        container.html(player);
    }

    // Templates
    var tpl_player    = $('#tpl-magia-player').html(),
        tpl_list_item = $('#tpl-magia-list-item').html();

    // Sections
    var content = $('#magia-main'),
        list    = $('#magia-list');

    // $.getJSON('https://gdata.youtube.com/feeds/api/playlists/PLZyDyrVVpOZQ70ZVP7DWnIZh2jxsyfVXE?v=2&alt=json&orderby=published', function(response){

    function get_videos(page) {
        var query = {
            part: 'snippet',
            playlistId: 'PLZyDyrVVpOZQ70ZVP7DWnIZh2jxsyfVXE',
            key: 'AIzaSyDzVyYzwsZ6Vn9X8hyTOuM6kboFUoly2rU',
            maxResults: 50
        }

        if (page) {
          query.pageToken = page;
        }

        $.getJSON('https://www.googleapis.com/youtube/v3/playlistItems', query, function(response){
            if (response.items === 0) {
                return;
            }

            set_playlist(list, tpl_list_item, response.items);
            set_player(content, tpl_player, response.items[0]);

            if (response.nextPageToken) {
              get_videos(response.nextPageToken);
            }
        });
    }

    get_videos();

    list.on('click', 'a', function(ev){
        ev.preventDefault();

        set_player(content, tpl_player, $(this).parent().data());

        FB.XFBML.parse(document.body);
    });
})(jQuery);
</script>
<?php $view['slots']->stop(); ?>
