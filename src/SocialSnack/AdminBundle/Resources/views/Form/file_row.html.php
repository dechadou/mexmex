<?php if (isset($image_preview_path)) { ?>
<div class="form-group <?php if (isset($attr['row_class'])) echo $attr['row_class']; ?>">
  <div class="form-control-media">
    <?php echo $view['form']->label($form); ?>

    <div class="col-sm-10">
      <?php echo $view['form']->errors($form); ?>
      <?php echo $view['form']->widget($form); ?>
      <?php if (isset($attr['help'])) { ?>
        <span class="help-block"><?php echo $attr['help']; ?></span>
      <?php } ?>

      <?php if ($image_preview_path) { ?>
      <figure>
        <img src="<?php echo $view['fronthelper']->get_cms_url($image_preview_path, $image_preview_size); ?>" />
      </figure>
      <?php } ?>
    </div>
  </div>
</div>
<?php } else { ?>
  <?php echo $view['form']->row($form); ?>
<?php } ?>