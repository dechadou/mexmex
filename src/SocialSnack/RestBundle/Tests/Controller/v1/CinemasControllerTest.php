<?php

namespace SocialSnack\RestBundle\Tests\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData;


class CinemasControllerTest extends WebTestCase {

  public function setUp() {
    parent::setUp();

    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadStatesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadAreasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadCinemasData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadMoviesData',
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadSessionsData',
    ]);
  }


  protected function getCollectionModel() {
    return [
        [
            'type' => 'array',
            'each' => $this->getEntityModel(),
        ]
    ];
  }


  protected function getEntityModel() {
    return [
        'id' => 'integer',
        'name' => 'string',
        'lat' => 'float',
        'lng' => 'float',
        'platinum' => 'boolean',
        'area' => [
            'model' => [
                'id' => 'integer',
                'name' => 'string',
            ],
        ],
        'state' => [
            'model' => [
                'id' => 'integer',
                'name' => 'string',
            ],
        ],
        'info' => [
            'model' => [
                'address' => 'string',
                'phone' => 'string',
                'neighborhood' => 'string',
            ],
        ],
        'user_fav' => 'integer',
    ];
  }


  /**
   * @group functional
   */
  public function testDefaultAll() {
    $client = static::createClient();
    $client->request(
        'GET',
        '/rest/cinemas/',
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   */
  public function testDefaultState() {
    $state_id = LoadStatesData::$states[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/cinemas/area/%d', $state_id + 1000),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $model[0]['each']['state']['model']['id'] = [
        'type' => 'integer',
        'equals' => $state_id,
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   */
  public function testDefaultArea() {
    $area_id = LoadAreasData::$areas[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/cinemas/area/%d', $area_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getCollectionModel();
    $model[0]['each']['area']['model']['id'] = [
        'type' => 'integer',
        'equals' => $area_id,
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   */
  public function testSingle() {
    $cinema_id = LoadCinemasData::$cinemas[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/cinemas/%d', $cinema_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = $this->getEntityModel();
    $model['id'] = [
        'type' => 'integer',
        'equals' => $cinema_id,
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   */
  public function testMovies() {
    $cinema_id = LoadCinemasData::$cinemas[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/cinemas/%d/movies', $cinema_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = [
        [
            'type' => 'array',
            'each' => [
                'id'         => 'integer',
                'name'       => 'string',
                'score'      => 'integer',
                'cover'      => 'string',
                'type'       => 'array',
                'info'       => [
                    'model' => [
                        'sinopsis'       => 'string',
                        'original_title' => 'string',
                        'director'       => 'string',
                        'cast'           => 'string',
                        'country'        => 'string',
                        'genre'          => 'array',
                        'rating'         => 'string',
                        'year'           => 'string',
                        'duration'       => 'string',
                        'trailer'        => 'string',
                    ],
                ],
                'attributes' => 'array',
                'sessions'   => [
                    'type' => 'array',
                    'each' => [
                        'id'             => 'integer',
                        'date'           => 'integer',
                        'tz_offset'      => 'integer',
                        'cinema_id'      => 'integer',
                        'premium'        => 'boolean',
                        'extreme'        => 'boolean',
                        'seatallocation' => 'boolean',
                        'screen_number'  => 'integer',
                    ]
                ],
                'url'        => 'string',
                'featured'   => 'boolean',
                'ribbons'    => 'array',
                'label'      => 'string',
            ],
        ],
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }

  /**
   * @group functional
   */
  public function testMoviesV1_1() {
    $cinema_id = LoadCinemasData::$cinemas[0]->getId();

    $client = static::createClient();
    $client->request(
        'GET',
        sprintf('/rest/cinemas/%d/movies?v=1.1', $cinema_id),
        [],
        [],
        ['HTTP_X-Api-Consumer-Key' => 'abc']
    );
    $model = [
        [
            'type' => 'array',
            'each' => [
                'id'         => 'integer',
                'name'       => 'string',
                'score'      => 'integer',
                'cover'      => 'string',
                'type'       => 'array',
                'info'       => [
                    'model' => [
                        'sinopsis'       => 'string',
                        'original_title' => 'string',
                        'director'       => 'string',
                        'cast'           => 'string',
                        'country'        => 'string',
                        'genre'          => 'array',
                        'rating'         => 'string',
                        'year'           => 'string',
                        'duration'       => 'string',
                        'trailer'        => 'string',
                    ],
                ],
                'attributes' => 'array',
                'url'        => 'string',
                'featured'   => 'boolean',
                'ribbons'    => 'array',
                'label'      => 'string',
                'versions'   => [
                    'type' => 'array',
                    'each' => [
                        'id'       => 'integer',
                        'label'    => 'string',
                        'type'     => 'array',
                        'sessions' => [
                            'type' => 'array',
                            'each' => [
                                'id'             => 'integer',
                                'date'           => 'integer',
                                'tz_offset'      => 'integer',
                                'cinema_id'      => 'integer',
                                'premium'        => 'boolean',
                                'extreme'        => 'boolean',
                                'seatallocation' => 'boolean',
                                'screen_number'  => 'integer',
                            ]
                        ],
                    ],
                ],
            ],
        ],
    ];

    $this->assertJsonResponse($client->getResponse(), $model);
  }

}