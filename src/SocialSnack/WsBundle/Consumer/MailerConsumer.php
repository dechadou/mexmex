<?php

namespace SocialSnack\WsBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use SocialSnack\WsBundle\Entity\Cinema;
use SocialSnack\WsBundle\Entity\Movie;
use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Request\Consulta;

class MailerConsumer implements ConsumerInterface {
	
	protected $container;
	
	protected $mailer;
  
  
	public function __construct( $container, $mailer ) {
		$this->container  = $container;
		$this->mailer     = $mailer;
	}
	
	/**
	 * @param \PhpAmqpLib\Message\AMQPMessage $msg
	 * @return boolean
	 */
	public function execute( AMQPMessage $msg ) {
    return $this->mailer->execute( json_decode( $msg ) );
  }
}