<?php

namespace SocialSnack\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class CinemasController extends BaseController {


  /**
   * $_GET['q'] Optional.
   *
   * @Route("/", name="rest_cinemas_default")
   * @Route("/area/{area_id}")
   * @Route("/state/{state_id}")
   * @Method({"GET"})
   *
   * @param null $area_id
   * @param null $state_id
   * @return JsonResponse
   */
	public function defaultAction($area_id = NULL, $state_id = NULL) {
    $cinema_repo     = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Cinema');
    $result          = array();
    $user_id         = $this->getRequest()->get('user_id');
    $user_faves      = array();
    $attributes_repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Attribute');
    $attributes      = $attributes_repo->findAll();
    $response        = new JsonResponse();
    $context_handler = $this->get('rest.context.handler');

    // Ideally this request should be private, but we need to make it cacheable
    // in order to reduce server load :S
    $response->setPublic();

    // Deprecated since V2.
    if ($user_id && $context_handler->getApiVersion() < 2) {
      // If an User ID was provided, let's get his favourite cinemas and
      // calculate the ETag of the request based on that, so the cached response
      // is valid as long as hist favourite cinemas hasn't changed.

      $user_faves = $this->get_fave_cinemas_ids($user_id);
      $etag       = (!is_null($area_id) ? $area_id . '_' : '') . md5(implode('|', $user_faves));

      $response->setEtag($etag);
      if ( $response->isNotModified($this->getRequest()) ) {
        // If ETag matches return 304 code.
        return $response;
      }
    }

    if ( !is_null($area_id) ) {
      // Filter by area.
      $cinemas = $cinema_repo->findBy(array('area' => $area_id, 'active' => 1));
    } elseif ( !is_null($state_id) ) {
      // Filter by state.
      $cinemas = $cinema_repo->findBy(array('state' => $state_id, 'active' => 1));
    } else {
      // Return all cinemas.
      $cinemas = $cinema_repo->findAll();
    }
    
		if (!$cinemas) {
      return new JsonResponse(array());
    }

		foreach ($cinemas as $cinema) {
			$cinema_info = $cinema->toArray();
      //TODO: get attributes as entity, not as an array.
      foreach($attributes as $attr){
        if ($cinema->getAttributes() != NULL){
          if (in_array($attr->getId(),$cinema->getAttributes())){
            $cinema_info['attributes'][] = $attr->getValue();
          }
        }
      }

			$cinema_info['user_fav'] = $user_faves
					? ( ( in_array( $cinema->getId(), $user_faves ) ) ? 1 : 0 )
					: 0;
			$result[] = $cinema_info;
		}

    $response->setData($result);

    if ( !$user_id ) {
      // If no User ID was provided, then the response is completely static and
      // it can be cached with no problems.
      $response->setSharedMaxAge(60 * 60);
      $response->setMaxAge(6 * 60 * 60);
    }

		return $response;
	}


	/**
	 * @Route("/{id}", name="rest_cinemas_single")
	 * @Method({"GET"})
	 */
	public function singleAction($id) {
    $cinema_repo     = $this->getDoctrine()->getRepository('SocialSnackWsBundle:Cinema');
    $user_id         = $this->getRequest()->get('user_id');
    $user_faves      = array();
    $attributes_repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:Attribute');
    $attributes      = $attributes_repo->findAll();
    $response        = new JsonResponse();
    $context_handler = $this->get('rest.context.handler');

    // Ideally this request should be private, but we need to make it cacheable
    // in order to reduce server load :S
    $response->setPublic();

    // Deprecated since V2.
    if ($user_id && $context_handler->getApiVersion() < 2) {
      // If an User ID was provided, let's get his favourite cinemas and
      // calculate the ETag of the request based on that, so the cached response
      // is valid as long as hist favourite cinemas hasn't changed.

      $user_faves = $this->get_fave_cinemas_ids($user_id);
      $etag       = md5(implode('|', $user_faves));

      $response->setEtag($etag);
      if ( $response->isNotModified($this->getRequest()) ) {
        // If ETag matches return 304 code.
        return $response;
      }
    }


		$cinema = $cinema_repo->find($id);
		if (!$cinema) {
      return new JsonResponse(array(), 404);
    }

		$cinema_info = $cinema->toArray();
    //TODO: get attributes as entity, not as an array.
    foreach($attributes as $attr){
      if ($cinema->getAttributes() != NULL){
        if (in_array($attr->getId(),$cinema->getAttributes())){
          $cinema_info['attributes'][] = $attr->getValue();
        }
      }
    }

		$cinema_info['user_fav'] = $user_faves
				? ( ( in_array( $cinema->getId(), $user_faves ) ) ? 1 : 0 )
				: 0;

    $response->setData($cinema_info);

    if ( !$user_id ) {
      // If no User ID was provided, then the response is completely static and
      // it can be cached with no problems.
      $response->setSharedMaxAge(60 * 60);
      $response->setMaxAge(6 * 60 * 60);
    }

		return $response;
	}


  /**
   * $_GET['date'] Optional.
   *
   * @Route("/{cinema_id}/movies", name="rest_cinemas_movies")
   * @Method({"GET"})
   * @param integer $cinema_id
   * @param Request $request
   * @return JsonResponse
   */
	public function moviesAction($cinema_id, Request $request) {
    $date = $request->query->get('date');
    $handler = $this->get('rest.cinema.handler');

    if ($request->query->has('v') && $request->query->get('v') === '1.1') {
      $result = $handler->getMoviesV1_1($cinema_id, $date);
    } else {
      $result = $handler->getMovies($cinema_id, $date);
    }

    $response = new JsonResponse($result);
    $response->setPublic();
    $response->setMaxAge(15 * 60);
    $response->setSharedMaxAge(45 * 60);

    return $response;
	}


}