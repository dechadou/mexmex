<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class CinemaHandler {

  protected $doctrine;

  protected $movieHandler;

  public function __construct(Registry $doctrine, MovieHandler $movieHandler) {
    $this->doctrine = $doctrine;
    $this->movieHandler = $movieHandler;
  }

  public function getMovies($cinema_id, $date = NULL) {
    /* @var $movie_repo \SocialSnack\WsBundle\Entity\MovieRepository */
    $movie_repo   = $this->doctrine->getRepository( 'SocialSnackWsBundle:Movie' );
    /* @var $session_repo \SocialSnack\WsBundle\Entity\SessionRepository */
    $session_repo = $this->doctrine->getRepository( 'SocialSnackWsBundle:Session' );
    $result       = array();
    $date_from    = $date ? new \DateTime($date) : NULL;
    $date_to      = $date ? $date_from           : NULL;

    $movies = $movie_repo->findMoviesInCinema($cinema_id, $date_from, $date_to, TRUE);

    foreach ($movies as $movie) {
      $sessions = $session_repo->findForMovieInCinema($movie->getId(), $cinema_id);
      $_movie   = $this->movieHandler->serializeOne($movie, TRUE, $sessions, FALSE);
      $_movie['label'] = FrontHelper::get_movie_attr_str($movie->getAttr());

      $result[] = $_movie;
    }

    return $result;
  }

  public function getMoviesV1_1($cinema_id, $date = NULL) {
    /* @var $movie_repo \SocialSnack\WsBundle\Entity\MovieRepository */
    $movie_repo   = $this->doctrine->getRepository( 'SocialSnackWsBundle:Movie' );
    /* @var $session_repo \SocialSnack\WsBundle\Entity\SessionRepository */
    $session_repo = $this->doctrine->getRepository( 'SocialSnackWsBundle:Session' );
    $result       = array();
    $date_from    = $date ? new \DateTime($date) : NULL;
    $date_to      = $date ? $date_from           : NULL;

    $movies = $movie_repo->findMoviesInCinema($cinema_id, $date_from, $date_to);

    foreach ($movies as $movie) {
      $_movie = $this->movieHandler->serializeOne($movie, TRUE, FALSE, FALSE);
      $_movie['label'] = FrontHelper::get_movie_attr_str($movie->getAttr());
      $_movie['versions'] = $movie_repo->findVersionsInCinema($movie->getGroupId(), $cinema_id, array('id', 'attributes'));

      foreach ($_movie['versions'] as &$version) {
        $version['label'] = FrontHelper::get_movie_attr_str(json_decode($version['attributes']));
        if (!$version['label']) {
          $version['label'] = 'Tradicional';
        }
        $version['type'] = array_keys((array)json_decode($version['attributes']));
        unset($version['attributes']);

        $version['sessions'] = [];
        $sessions = $session_repo->findForMovieInCinema($version['id'], $cinema_id);
        foreach ($sessions as $session) {
          $_session = $session->toArray();
          unset($_session['tickets']);
          $version['sessions'][] = $_session;
        }
      }
      $result[] = $_movie;
    }

    return $result;
  }

}