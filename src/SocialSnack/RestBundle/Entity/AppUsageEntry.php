<?php

namespace SocialSnack\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUsageEntry
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AppUsageEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \SocialSnack\RestBundle\Entity\App
     *
     * @ORM\ManyToOne(targetEntity="\SocialSnack\RestBundle\Entity\App")
     */
    private $app;

    /**
     * @var integer
     *
     * @ORM\Column(name="hits", type="integer")
     */
    private $hits;

    /**
     * @var integer
     *
     * @ORM\Column(name="misses", type="integer")
     */
    private $misses;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    public function __construct() {
        $this->hits = 0;
        $this->misses = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     * @return AppUsageEntry
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return integer 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set misses
     *
     * @param integer $misses
     * @return AppUsageEntry
     */
    public function setMisses($misses)
    {
        $this->misses = $misses;
    
        return $this;
    }

    /**
     * Get misses
     *
     * @return integer 
     */
    public function getMisses()
    {
        return $this->misses;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return AppUsageEntry
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set app
     *
     * @param \SocialSnack\RestBundle\Entity\App $app
     * @return AppUsageEntry
     */
    public function setApp(\SocialSnack\RestBundle\Entity\App $app = null)
    {
        $this->app = $app;
    
        return $this;
    }

    /**
     * Get app
     *
     * @return \SocialSnack\RestBundle\Entity\App 
     */
    public function getApp()
    {
        return $this->app;
    }
}