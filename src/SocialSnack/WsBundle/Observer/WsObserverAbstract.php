<?php

namespace SocialSnack\WsBundle\Observer;

use SocialSnack\WsBundle\Request\ConsultaAbstract;

/**
 * Class WsObserverAbstract
 *
 * Observer class to handle WS statistics.
 *
 * @package SocialSnack\WsBundle\Observer
 * @author Guido Kritz
 */
abstract class WsObserverAbstract {

  protected $supportedMethods = [];


  /**
   * Observes the ConsultaAbstract::request() method.
   *
   * @param ConsultaAbstract       $ws_request
   * @param \Buzz\Message\Response $response
   * @param \Exception             $e
   */
  abstract protected function request(ConsultaAbstract $ws_request, \Buzz\Message\Response $response, \Exception $e);


  /**
   * Handles a notification from the observer's subject.
   *
   * @param ConsultaAbstract $request
   * @param string           $method
   * @return bool
   */
  public function update(ConsultaAbstract $request, $method) {
    if (!$this->supportsMethod($method)) {
      return FALSE;
    }

    call_user_func_array([$this, $method], array_merge([$request], array_slice(func_get_args(), 2)));
  }


  /**
   * Removes sensitive information for safe logging.
   *
   * @param array $args Input arguments.
   * @return array Sanitized arguments.
   */
  protected function sanitizeRequestArgs(array $args) {
    // Do not log credit card numbers!! Just keep the last four digits.
    if (isset($args['TDC'])) {
      $args['TDC'] = str_pad('', strlen($args['TDC']) - 4, '-') . substr($args['TDC'], -4);
    }

    return $args;
  }


  /**
   * Tells whether the notified method is supported by this observer or not.
   *
   * @param string $method Notified method.
   * @return bool
   */
  protected function supportsMethod($method) {
    return in_array($method, $this->supportedMethods);
  }

}