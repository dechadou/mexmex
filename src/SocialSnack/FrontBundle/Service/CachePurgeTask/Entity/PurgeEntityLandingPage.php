<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class PurgeEntityLandingPage extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->frontUtils->get_permalink($this->entity),
            'env'  => 'front'
        ],
        [
            'path' => $this->frontUtils->get_permalink($this->entity->getLanding()),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\LandingRepository::_findWithPages:slug:' . FrontHelper::sanitize_for_url($this->entity->getName()),
                'SocialSnack\FrontBundle\Entity\LandingRepository::_findWithPages:id:' . $this->entity->getId(),
            ],
            'env' => 'front'
        ]
    ];
  }

}