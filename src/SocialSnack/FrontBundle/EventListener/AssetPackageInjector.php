<?php

namespace SocialSnack\FrontBundle\EventListener;

use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\Templating\Asset\UrlPackage;

class AssetPackageInjector {

  /**
   * Inject custom Asset package to Kernel assets helper
   * @param \Symfony\Component\HttpKernel\Event\KernelEvent $event
   */
  public function onKernelRequest(KernelEvent $event) {
    $container = $event->getDispatcher()->getContainer();

    /* @var $assetsHelper \Symfony\Component\Templating\Helper\CoreAssetsHelper */
    $assetsHelper = $container->get('templating.helper.assets');
    $static = new UrlPackage($container->getParameter('static_abs_url') );
    $assetsHelper->addPackage('Static', $static);
    $assetsHelper->addPackage('MoviePosters', new UrlPackage($container->getParameter('movie_posters_url')));
    $assetsHelper->addPackage('QR', new UrlPackage($container->getParameter('qr_codes_url')));
    $assetsHelper->addPackage('CMS', new UrlPackage($container->getParameter('cms_upload_url')));
    $assetsHelper->setDefaultPackage($static);
  }

}
