<?php
$view->extend('SocialSnackFrontBundle:Privacy:base.html.php');
$view['slots']->set( 'title', 'Aviso de privacidad integro solicitantes y candidatos - Cinemex' );
$view['slots']->start('privacy_body');
?>
<h1 class="content-box-title">Aviso de privacidad integro solicitantes y candidatos</h1>

<div class="entry-body align-justify">
	<p>Cinemex Desarrollos, S.A. de C.V. (en adelante “Cinemex”), con domicilio en Avenida Javier Barros Sierra No. 540, Torre 1, PH1, Colonia Santa Fe, Delegación Álvaro Obregón, C.P. 01210, México, D.F., te comunica lo siguiente:</p>
	<p>Los datos personales, financieros, patrimoniales y sensibles, (en adelante y conjuntamente los “Datos Personales”), que les solicitamos son tratados con la finalidad  primaria de llevar a cabo el proceso de Reclutamiento y Selección</p>

	<h3>I.	DATOS Y SU FINALIDAD:</h3>

	<p>Los Datos Personales proporcionados a “Cinemex”, serán resguardados y administrados en términos de lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad (en adelante y conjuntamente “la Legislación”), en tal sentido, nos comprometemos a salvaguardar tal información bajo los principios de lealtad y responsabilidad. La información obtenida se encuentra protegida por medidas de seguridad físicas, tecnológicas y administrativas, apegadas a normas, que buscan salvaguardar los datos en términos de lo dispuesto en “la Legislación”.</p>

	<p>Los Datos Personales que se recaban, serán conservados por un periodo de  6 meses en medios físicos o electrónicos (tomando en consideración aspectos administrativos, contables, fiscales y jurídicos). En caso de no ser contratados serán descartados a efecto de evitar un tratamiento indebido de los mismos, con fundamento en el artículo 11, segundo párrafo de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares.</p>
	<p> “Cinemex” puede requerir, almacenar y hacer uso de los datos personales y sensibles que nos proporcionas al solicitar una de nuestras vacantes, ya sea por medio de bolsas de trabajo, correo electrónico, contacto, entrega física, o por medio de la solicitud de empleo publicada en nuestra página:</p>
	<p> www.cinemex.com </p>

	<p>Tus datos serán utilizados con las siguientes finalidades:</p>
  <ul>
    <li>Conocer y evaluar tu trayectoria, habilidades, experiencia laboral, y aptitudes, para que, en caso de cumplir con el perfil de la vacante que solicitas, te contactemos y tengamos una primera entrevista de trabajo.</li>
    <li>Realizar una o varias entrevistas de trabajo, cuestionarios, exámenes técnicos y psicométricos, para que podamos conocer y evaluar si tu perfil laboral corresponde al de la vacante que solicitas.</li>
    <li>Contactar a las empresas y/o personas físicas con las que previamente has desempeñado diversas labores en el área profesional, con la finalidad de conocer tu perfil laboral y desempeño.</li>
  </ul>

	<p>Las solicitudes de trabajo y/o currícula que nos entregues, únicamente constituye(n) una solicitud de empleo y, por lo tanto, no representa garantía alguna de que la misma será aceptada por “Cinemex” y no genera ningún derecho laboral a tu favor.</p>

	<h3>II.	LÍMITE AL USO O DIVULGACIÓN DE LOS DATOS: </h3>

	<p>Los datos proporcionados a “Cinemex” serán resguardados y administrados en términos de lo dispuesto en la “Ley” y, en tal sentido, nos comprometemos a salvaguardar tal información bajo los principios de lealtad y responsabilidad. </p>
	<p>La información obtenida se encuentra protegida por medio de procedimientos físicos, tecnológicos y administrativos apegados a normas que salvaguardan los datos en términos de lo dispuesto en la “Ley” durante el tiempo que dure el proceso de selección y reclutamiento, mismo que no excederá el plazo de seis meses.</p>
	<p>La gestión de las Solicitudes de Empleo de Cinemex.com obtenidas por medios propios o a través de un tercero según determine Cinemex  será avalada con Convenios de Confidencialidad.</p>
	<p>Al llenar o entregar una solicitud de empleo otorgas tu consentimiento para que tal información sea compartida con empresas de igual prestigio y relacionadas con nuestro giro, con las que integramos un grupo de intercambio que se reúne periódicamente; específicamente con las área de Reclutamiento y Selección. El objetivo es apoyar a las personas que actualmente se encuentran en búsqueda de empleo y cuyo perfil no se adecua a ninguna vacante en “Cinemex”; es decir, se comparte la información con las otras empresas con la expectativa de que éstas consideren a los aspirantes a trabajar en “Cinemex” dentro de sus procesos de selección vigentes. </p>

	<h3>III.	MEDIOS PARA EJERCER LOS DERECHOS “ARCO”: </h3>

	<p>Podrá ejercer sus derechos de acceso, rectificación, cancelación y oposición contenidos en “la Legislación”, únicamente mediante el llenado del formato correspondiente que podrás obtener de la página electrónica:</p>
	<p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>
	<p>Una vez presentada su solicitud en el formato prestablecido, “Cinemex” podrá solicitarle en un periodo no mayor a 5 días hábiles, la información y/o documentación necesaria para su seguimiento, así como para la acreditación de su identidad, de acuerdo a los términos que marca la Legislación. Por lo que contará con 10 días hábiles posteriores a su recepción, para atender este requerimiento. De lo contrario su solicitud se tendrá por no presentada.</p>

	<p>Asimismo, en un plazo posterior de 20 días hábiles “Cinemex” emitirá una resolución, la cual le será notificada por los medios de contacto que haya establecido en su solicitud. Una vez emitida la resolución y en caso de que la misma sea procedente (parcial o totalmente), “Cinemex” contará con 15 días hábiles para adoptar dicha resolución. </p>

	<p>Los términos y plazos indicados en los párrafos anteriores, podrán ser ampliados una sola vez en caso de ser necesario y se le deberá notificar a través de los medios de contacto que haya establecido. </p>

	<p>La revocación y el ejercicio de los Derechos ARCO serán gratuitos, debiendo usted cubrir únicamente los gastos justificados de envío, o el costo de reproducción en copias u otros formatos establecidos en su solicitud.</p>

	<p>Asimismo, le informamos que le asiste su derecho para acudir ante el Instituto Federal de Acceso a la Información y Protección de Datos en caso de considerar que su derecho a la protección de Datos Personales ha sido vulnerado. Para mayor información visite www.ifai.org.mx</p>

	<h3>IV. TRANSFERENCIA DE DATOS:</h3>

	<p>“Cinemex”, se abstendrá de vender, arrendar o alquilar tus datos personales y sensibles a un tercero, pero por razones de contratación podrán ser compartidos con: </p>

  <ul>
    <li>Empresas que se encuentren en búsqueda de candidatos con tus características y aptitudes, durante el periodo de reclutamiento y selección, mismo que no excederá de seis meses de la recepción de tu currícula y/o solicitud de empleo.</li>
    <li>Empresas que practiquen y evalúen exámenes psicométricos y otras empresas de su gremio. </li>
    <li>Empresas que ayuden en la gestión de Solicitudes de Empleo. </li>
  </ul>

	<p>Estas instituciones se encuentran obligadas contractualmente a mantener la confidencialidad de la información proporcionada por Cinemex, y a no utilizarla de manera diversa a la establecida en el presente Aviso.</p>
	<p>En caso de que no seas seleccionado para el equipo de trabajo de “Cinemex”, una vez concluido el plazo de seis meses, los datos personales y sensibles que nos has proporcionado, serán destruidos, a efecto de evitar un tratamiento indebido de los mismos. Se entiende que consientes la transferencia de tus datos, salvo que manifiestes expresamente tu oposición, mediante el llenado del formato correspondiente que podrás obtener de la página electrónica: </p>
	<p><a href="<?php echo $view['router']->generate('arco_form'); ?>"><?php echo $view['router']->generate('arco_form', array(), TRUE); ?></a></p>
	<p>Y deberás adjuntar una identificación oficial para acreditar tu identidad. </p>

	<h3>IV.	CAMBIOS AL AVISO DE PRIVACIDAD Y VULNERACIÓN DE SEGURIDAD:</h3>

	<p>“Cinemex” tiene el derecho de efectuar cualquier cambio o modificación al Aviso de Privacidad. Tales modificaciones podrán ser consultadas en </p>
	<p><a href="http://cinemex.com/privacidadcandidatos">http://cinemex.com/privacidadcandidatos</a>.</p>

	<p>En caso de que ocurra una vulneración de seguridad en cualquier fase del tratamiento de sus Datos Personales, “Cinemex” a través de la figura del responsable lo hará del conocimiento general, a través de la siguiente página de internet:</p>
	<p><a href="http://cinemex.com/privacidadcandidatos">http://cinemex.com/privacidadcandidatos</a></p>
  <p>para que tome las medidas correspondientes a fin de resguardar sus derechos.</p>
  <p>Fecha de última actualización: 5/Enero/2015</p>
</div>
<?php $view['slots']->stop(); ?>