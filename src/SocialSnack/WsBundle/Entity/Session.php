<?php

namespace SocialSnack\WsBundle\Entity;

use SocialSnack\WsBundle\Service\Helper as WsHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(indexes={
 *   @ORM\Index(name="sess_active_idx",    columns={"active"}),
 *   @ORM\Index(name="sess_date_idx",      columns={"date"}),
 *   @ORM\Index(name="sess_date_time_idx", columns={"date_time"}),
 *   @ORM\Index(name="sess_legacy_idx",    columns={"legacy_id"}),
 *   @ORM\Index(name="sess_group_idx",     columns={"group_id"})
 * })
 * @ORM\Entity(repositoryClass="SessionRepository")
 */
class Session
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="legacy_id", type="integer")
     */
    private $legacy_id;

    /**
     * @var boolean
     * @todo Remove this column and use $types instead.
     *
     * @ORM\Column(name="platinum", type="boolean")
     */
    private $platinum;

    /**
     * @var boolean
     * @todo Remove this column and use $types instead.
     *
     * @ORM\Column(name="premium", type="boolean")
     */
    private $premium;

    /**
     * @var boolean
     * @todo Remove this column and use $types instead.
     *
     * @ORM\Column(name="extreme", type="boolean")
     */
    private $extreme;

    /**
     * @var array
     *
     * @ORM\Column(name="types", type="json_array", length=255)
     */
    private $types;
    
    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \Time
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;
		
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_time", type="datetime")
     */
    private $date_time;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="tz", type="string", length=64)
     */
    private $tz;
    
		/**
		 * @ORM\ManyToOne(targetEntity="Cinema", inversedBy="sessions")
		 * @ORM\JoinColumn(name="cinema_id", referencedColumnName="id")
		 */
		private $cinema;
		
		/**
		 * @var integer
		 * 
		 * @ORM\Column(name="legacy_cinema_id", type="integer")
		 */
		private $legacy_cinema_id;
		
		/**
		 * @ORM\ManyToOne(targetEntity="Movie", inversedBy="sessions")
		 * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
		 */
		private $movie;
		
		/**
		 * @ORM\Column(name="info", type="text")
		 */
		private $info;
		
		/**
		 * @var boolean
		 * 
		 * @ORM\Column(name="active", type="boolean")
		 */
		private $active = 1;

		/**
		 * @var boolean
		 *
		 * @ORM\Column(name="updating", type="boolean")
		 */
		private $updating = 1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="group_id", type="string", length=20)
     */
    private $group_id;

		private $decoded_info = NULL;
		
		private $tickets = NULL;


    public function __construct() {
        $this->types = array();
        $this->extreme = FALSE;
        $this->premium = FALSE;
        $this->updating = FALSE;
    }
    
    
    public function __clone() {
        $this->decoded_info = NULL;
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legacy_id
     *
     * @param integer $legacyId
     * @return Session
     */
    public function setLegacyId($legacyId)
    {
        $this->legacy_id = $legacyId;
    
        return $this;
    }

    /**
     * Get legacy_id
     *
     * @return integer 
     */
    public function getLegacyId()
    {
        return $this->legacy_id;
    }

    /**
     * Set platinum
     *
     * @param boolean $platinum
     * @return Session
     */
    public function setPlatinum($platinum)
    {
        $this->platinum = $platinum;
    
        return $this;
    }

    /**
     * Get platinum
     *
     * @return boolean 
     */
    public function getPlatinum()
    {
        return $this->platinum;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Session
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $cinema
     * @return Session
     */
    public function setCinema(\SocialSnack\WsBundle\Entity\Cinema $cinema = null)
    {
        $this->cinema = $cinema;
    
        return $this;
    }

    /**
     * Get cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema 
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * Set movie
     *
     * @param \SocialSnack\WsBundle\Entity\Movie $movie
     * @return Session
     */
    public function setMovie(\SocialSnack\WsBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;
    
        return $this;
    }

    /**
     * Get movie
     *
     * @return \SocialSnack\WsBundle\Entity\Movie 
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Session
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Session
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }
		
		
		/**
		 * Get data from the WS movie object.
		 * 
		 * @param string $key
		 * @return mixed
		 */
		public function getData( $key ) {
			if ( is_null( $this->decoded_info ) ) {
				$this->decoded_info = json_decode( $this->getInfo() );
			}
      
      if ( !isset($this->decoded_info->{ $key }) )
        return NULL;
      
			return $this->decoded_info->{ $key };
		}

    /**
     * Set legacy_cinema_id
     *
     * @param integer $legacyCinemaId
     * @return Session
     */
    public function setLegacyCinemaId($legacyCinemaId)
    {
        $this->legacy_cinema_id = $legacyCinemaId;
    
        return $this;
    }

    /**
     * Get legacy_cinema_id
     *
     * @return integer 
     */
    public function getLegacyCinemaId()
    {
        return $this->legacy_cinema_id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return Session
     */
    public function setTime($time)
    {
        $this->time = $time;
    
        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }
		
		
		public function getTickets() {
			return $this->tickets;
		}
		
		
		public function setTickets( $tickets ) {
			$this->tickets = $tickets;
		}
		
		
		public function toArray() {
      $date_wtz = new \DateTime($this->getDateTime()->format('Y-m-d H:i:s'), new \DateTimeZone($this->getTz()));
			$result = array(
          'id'             => $this->getId(),
          'date'           => $date_wtz->getTimestamp(),
          'tz_offset'      => $date_wtz->getOffset(),
          'cinema_id'      => $this->getCinema()->getId(),
          'premium'        => $this->getPremium(),
          'extreme'        => $this->getExtreme(),
          'seatallocation' => $this->getData( 'SEATALLOCATIONON' ) != 'N',
          'tickets'        => array(),
          'screen_number'  => WsHelper::get_session_screen_number($this),
      );

			if ( $this->tickets ) {
				foreach ( $this->tickets as $ticket ) {
					$result[ 'tickets' ][] = array(
							'id'        => $ticket->getData( 'TICKETTYPECODE' ),
							'name'      => $ticket->getDescription(),
							'price'     => $ticket->getPrice(),
					);
				}
			}
			
			return $result;
		}

    /**
     * Set group_id
     *
     * @param integer $groupId
     * @return Session
     */
    public function setGroupId($groupId)
    {
        $this->group_id = $groupId;
    
        return $this;
    }

    /**
     * Get group_id
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set premium
     *
     * @param boolean $premium
     * @return Session
     */
    public function setPremium($premium)
    {
        $this->setType('premium', $premium);
        $this->premium = $premium;
    
        return $this;
    }

    /**
     * Get premium
     *
     * @return boolean 
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * Set date_time
     *
     * @param \DateTime $dateTime
     * @return Session
     */
    public function setDateTime($dateTime)
    {
        $this->date_time = $dateTime;
        $this->date = $dateTime;
        $this->time = $dateTime;

        return $this;
    }

    /**
     * Get date_time
     *
     * @return \DateTime 
     */
    public function getDateTime()
    {
        return $this->date_time;
    }

    /**
     * Set extreme
     *
     * @param boolean $extreme
     * @return Session
     */
    public function setExtreme($extreme)
    {
        $this->setType('extreme', $extreme);
        $this->extreme = $extreme;
    
        return $this;
    }

    /**
     * Get extreme
     *
     * @return boolean 
     */
    public function getExtreme()
    {
        return $this->extreme;
    }

    /**
     * Set tz
     *
     * @param string $tz
     * @return Session
     */
    public function setTz($tz)
    {
        $this->tz = $tz;
    
        return $this;
    }

    /**
     * Get tz
     *
     * @return string 
     */
    public function getTz()
    {
        return $this->tz;
    }

    /**
     * Set type
     *
     * @param string $types
     * @return Session
     */
    public function setTypes($types)
    {
        $this->types = $types;
    
        return $this;
    }

    /**
     * Get types
     *
     * @return array
     */
    public function getTypes()
    {
        // @todo Find a way to prevent empty elements from being persisted instead of calling this function all the time.
        return array_filter($this->types);
    }
    
    
    /**
     * Check if the session matches certain session type.
     * 
     * @param string $type Type identifier.
     * @return boolean
     */
    public function isType($type) {
      return in_array($type, $this->types);
    }
    
    
    /**
     * Set/unset session type.
     * 
     * @param string  $type
     * @param boolean $set
     * @return boolean
     */
    public function setType($type, $set = TRUE) {
      if ($set) { // Set
        if (!$this->isType($type)) {
          $this->types[] = $type;
        }
      } else { // Unset
        if ($this->isType($type)) {
          $this->types = array_diff($this->types, array($type));
        }
      }
      
      return $set;
    }
    
    
    /**
     * Check if this is a "CineMá" session.
     * 
     * @return boolean
     */
    public function isCinemom() {
      return $this->isType('cinemom');
    }
    
    
    /**
     * Set "CineMá" session type.
     * 
     * @param boolean $set
     * @return Session
     */
    public function setCinemom($set = TRUE)
    {
        $this->setType('cinemom', $set);

        return $this;
    }
    

    /**
     * Set updating
     *
     * @param boolean $updating
     * @return Session
     */
    public function setUpdating($updating)
    {
        $this->updating = $updating;
    
        return $this;
    }

    /**
     * Get updating
     *
     * @return boolean 
     */
    public function getUpdating()
    {
        return $this->updating;
    }
}