#!/usr/local/php5/bin/php -n
<?
$db_host = '10.1.1.216';
$db_user = 'cinemexuser';
$db_pass = 'P19nmm9Ven';
$db_name = 'cinemexdb';

$opts = array('mail::','date::');
$options = getopt(NULL, $opts);

$date_from = isset($options['date']) ? $options['date'] : @date('Y-m-d', time() - 24*60*60);
$date_to   = @date('Y-m-d', strtotime($date_from) + 24*60*60);

ob_start();
echo "Día $date_from\r\n";
echo "---\r\n";
mysql_connect($db_host, $db_user, $db_pass) or die('Error connecting DB');
mysql_select_db($db_name) or die('Error selecting DB');
$query = sprintf('SELECT COUNT(*) as trans,SUM(tickets_count) as tickets,SUM(amount) as valor FROM cinemexdb.Transaction WHERE purchase_date >= \'%s 00:00:00\' AND purchase_date < \'%s 00:00:00\'', $date_from, $date_to);
if (!$result = mysql_query($query))
  die(mysql_error());
echo "Total:\r\n";
while($row = mysql_fetch_assoc($result)){
  echo sprintf("%s transacciones, %s boletos, $%s\r\n", number_format($row['trans']), number_format($row['tickets']), number_format(substr($row['valor'],0,-2)));
}

$query = sprintf('SELECT c.name,COUNT(*) as trans,SUM(tickets_count) as tickets,SUM(amount) as valor FROM cinemexdb.Transaction t INNER JOIN cinemexdb.Session s ON t.session_id = s.id INNER JOIN cinemexdb.Cinema c ON s.cinema_id = c.id WHERE purchase_date >= \'%s 00:00:00\' AND purchase_date < \'%s 00:00:00\' GROUP BY c.id ORDER BY trans DESC LIMIT 5', $date_from, $date_to);
if (!$result = mysql_query($query))
  die(mysql_error());
echo "---\r\n";
echo "Por cine:\r\n";
while($row = mysql_fetch_assoc($result)){
  echo sprintf("%s: %s transacciones, %s boletos, $%s\r\n", $row['name'], number_format($row['trans']), number_format($row['tickets']), number_format(substr($row['valor'],0,-2)));
}

$query = sprintf('SELECT m.legacy_name,COUNT(*) as trans,SUM(tickets_count) as tickets,SUM(amount) as valor FROM cinemexdb.Transaction t INNER JOIN cinemexdb.Session s ON t.session_id = s.id INNER JOIN cinemexdb.Movie m ON s.movie_id = m.id WHERE purchase_date >= \'%s 00:00:00\' AND purchase_date < \'%s 00:00:00\' GROUP BY m.id ORDER BY trans DESC LIMIT 5', $date_from, $date_to);
if (!$result = mysql_query($query))
  die(mysql_error());
echo "---\r\n";
echo "Por película:\r\n";
while($row = mysql_fetch_assoc($result)){
  echo sprintf("%s: %s transacciones, %s boletos, $%s\r\n", $row['legacy_name'],  number_format($row['trans']), number_format($row['tickets']), number_format(substr($row['valor'],0,-2)));
}

$query = sprintf('SELECT COUNT(*) as cnt FROM cinemex_user WHERE signup_date < \'%s\'', $date_to);
if (!$result = mysql_query($query))
  die(mysql_error());
echo "\r\n";
if($row = mysql_fetch_assoc($result)){
  echo sprintf("Total usuarios registrados: %d\r\n",$row['cnt']);
}

$query = sprintf('SELECT COUNT(*) as cnt FROM cinemex_user WHERE iecode IS NOT NULL AND signup_date < \'%s\'', $date_to);
if (!$result = mysql_query($query))
  die(mysql_error());
if($row = mysql_fetch_assoc($result)){
  echo sprintf("Total usuarios registrados (invitado especial): %d\r\n",$row['cnt']);
}

if (isset($options['mail'])) {
  $mails = explode(',', $options['mail']);
  $content = ob_get_clean();
  $headers = "From: Cinemex <crons@cinemex.com>\r\n";
  foreach($mails as $mail) {
    echo "Mail a $mail\r\n";
    mail($mail,'Transacciones Cinemex '.$date_from,$content,$headers);
  }
} else {
  ob_flush();
}

