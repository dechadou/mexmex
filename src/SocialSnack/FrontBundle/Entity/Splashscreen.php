<?php

namespace SocialSnack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Modal
 *
 * @ORM\Table(name="Splashscreen")
 * @ORM\Entity(repositoryClass="SplashscreenRepository")
 */
class Splashscreen {

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255)
   */
  private $name;

  /**
   * @var string
   *
   * @ORM\Column(name="image_v", type="string", length=255)
   */
  private $image_v;

  /**
   * @var string
   *
   * @ORM\Column(name="image_h", type="string", length=255)
   */
  private $image_h;

  /**
   * @var string
   *
   * @ORM\Column(name="deep_link", type="text")
   */
  private $deep_link;

  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean")
   */
  private $active;


  public function __construct() {
    $this->active = TRUE;
  }


  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }


  /**
   * Set name
   *
   * @param string $name
   * @return Splashscreen
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }


  /**
   * Get name
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }


  /**
   * Set deep_link
   *
   * @param string $deepLink
   * @return Splashscreen
   */
  public function setDeepLink($deepLink) {
    $this->deep_link = $deepLink;

    return $this;
  }


  /**
   * Get deep_link
   *
   * @return string
   */
  public function getDeepLink() {
    return $this->deep_link;
  }


    /**
     * Set active
     *
     * @param boolean $active
     * @return Splashscreen
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set image_v
     *
     * @param string $imageV
     * @return Splashscreen
     */
    public function setImageV($imageV)
    {
        $this->image_v = $imageV;
    
        return $this;
    }

    /**
     * Get image_v
     *
     * @return string 
     */
    public function getImageV()
    {
        return $this->image_v;
    }

    /**
     * Set image_h
     *
     * @param string $imageH
     * @return Splashscreen
     */
    public function setImageH($imageH)
    {
        $this->image_h = $imageH;
    
        return $this;
    }

    /**
     * Get image_h
     *
     * @return string 
     */
    public function getImageH()
    {
        return $this->image_h;
    }
}