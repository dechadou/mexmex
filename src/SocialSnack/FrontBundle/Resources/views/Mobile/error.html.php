<?php $view->extend('SocialSnackFrontBundle:Mobile:base.html.php'); ?>
<?php $view['slots']->start('body'); ?>
<div class="error-page-msg">
  <div class="big-msg">
    <h1 class="big-msg-title"><?php echo $title; ?></h1>
    <p class="big-msg-msg">
      <?php echo $message; ?>
      <?php if (isset($error_id)) { ?>
        <br/><span style="font-size: 11px; font-weight: 400;">Error ID: <?php echo $error_id; ?></span>
      <?php } ?>
    </p>
    <?php if ( isset($text) ) { ?>
      <p><?php echo $text; ?></p>
    <?php } ?>
    <a href="<?php echo $view['router']->generate('mobile_index'); ?>" class="btn btn-default btn-icon icon icon-larr">Volver</a>
  </div>
</div>
<?php $view['slots']->stop(); ?>
