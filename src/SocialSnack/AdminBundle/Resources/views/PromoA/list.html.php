<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Listado de Promos A - Cinemex' );
$view['slots']->start('body')
?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Listado de Promos A <?php if (FALSE === $active) echo 'inactivas'; ?>
              <div class="right">
                <a href="<?php echo $view['router']->generate( 'social_snack_promoa_list' . (FALSE === $active ? '' : '_inactive') );?>">Ver <?php echo (FALSE === $active) ? 'activas' : 'inactivas'; ?></a>
              </div>
            </header>
            <div class="panel-body">
                  <div class="adv-table">
                    <div class="clearfix">
                        <div class="btn-group">
                            <button class="btn green" id="editable-sample_new" onclick="window.location.href='<?php echo $view['router']->generate('social_snack_promoa_add'); ?>'">
                                Nueva promo A <i class="icon-plus"></i>
                            </button>
                        </div>
                    </div>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Título</th>
                            <th>URL</th>
                            <th>Activa</th>
                            <th>Orden</th>
                            <th>Editar</th>
                            <th>Borrar</th>
                        </tr>
                        </thead>
                        <tbody>
<?php
foreach ($promosa as $promo) {
?>
                        <tr class="gradeX">
                            <td><img src="<?php echo $view['fronthelper']->get_cms_url($promo->getImage(), '1020x129'); ?>" width="300" /></td>
                            <td><?php echo $promo->getTitle(); ?></td>
                            <td><?php echo $promo->getUrl(); ?></td>
                            <td><?php echo $promo->getStatus() ? 'Sí' : 'No'; ?></td>
                            <td><?php echo $promo->getOrder(); ?></td>
                            <td><a href="<?php echo $view['router']->generate('social_snack_promoa', ['promoa_id' => $promo->getId()]); ?>">Editar</a></td>
                            <td><a href="<?php echo $view['router']->generate('social_snack_promoa_delete', ['promoa_id' => $promo->getId()]); ?>"  onclick="return confirm('Estás seguro?')">Borrar</a></td>
                        </tr>
<?php
}
?>
                      </table>
                  </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#example').dataTable( {
            "aaSorting": [[ 4, "desc" ]]
        } );
    } );
</script>
<?php $view['slots']->stop(); ?>