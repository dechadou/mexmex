<?php

namespace SocialSnack\WsBundle\Tests\Request;

use SocialSnack\WsBundle\Request\ConsultaIEC;

/**
 * Class ConsultaIECTest
 * @package SocialSnack\WsBundle\Tests\Request
 * @author Guido Kritz
 */
class ConsultaIECTest extends WebTestCase {

  protected function get_ws_method() {
    return 'ConsultaIEC';
  }

  protected function getWsUrl($method) {
    return 'https://ws.cinemex.test/wsInvitadoEspecial/wsInvitadoEspecial.asmx/' . $method;
  }

  public function testConsultaInfoSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;ConsultaIEC&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;NUMIEC&gt;3086810421529002&lt;/NUMIEC&gt;&lt;NUMINVITADO&gt;9011446&lt;/NUMINVITADO&gt;&lt;FECHAINSCRIPCION&gt;14/08/2014&lt;/FECHAINSCRIPCION&gt;&lt;PNOMBREIEC&gt;ARTURO&lt;/PNOMBREIEC&gt;&lt;SNOMBREIEC&gt;&lt;/SNOMBREIEC&gt;&lt;APATERNOIEC&gt;RODRIGUEZ&lt;/APATERNOIEC&gt;&lt;AMATERNOIEC&gt;MEZA&lt;/AMATERNOIEC&gt;&lt;TELEFONOCASA&gt;2222110413&lt;/TELEFONOCASA&gt;&lt;CELULAR&gt;2224108209&lt;/CELULAR&gt;&lt;EMAIL&gt;arturo.rm@betterhouse.com.mx&lt;/EMAIL&gt;&lt;FECHANACIMIENTO&gt;26/05/1981&lt;/FECHANACIMIENTO&gt;&lt;NIVEL&gt;BÁSICA&lt;/NIVEL&gt;&lt;VIGENCIANIVEL&gt;31/12/2014&lt;/VIGENCIANIVEL&gt;&lt;VISITASORO&gt;0&lt;/VISITASORO&gt;&lt;VISITASBLACK&gt;0&lt;/VISITASBLACK&gt;&lt;SALDO&gt;-1.00&lt;/SALDO&gt;&lt;FECHASALDOVALIDO&gt;28/12/2014&lt;/FECHASALDOVALIDO&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>'
    );

    $tipo_consulta = 1;
    $this->assertEquals($tipo_consulta, ConsultaIEC::TIPOCONSULTA_INFO);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta,
        'ClaveIEC'     => '1234567890123456',
    ));

    $this->assertInstanceOf('\SimpleXMLElement', $res);
  }

  public function testConsultaInfoInvalidCode() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;LMSERROR&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;ERROR&gt;&lt;ERRORCODIGO&gt;-50100&lt;/ERRORCODIGO&gt;&lt;ERRORDESC&gt;Número de Invitado Especial No Existe.&lt;/ERRORDESC&gt;&lt;/ERROR&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>'
    );

    $tipo_consulta = 1;
    $this->assertEquals($tipo_consulta, ConsultaIEC::TIPOCONSULTA_INFO);

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\InvalidIECodeException');

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta,
        'ClaveIEC'     => '0000000000000000',
    ));
  }

  public function testConsultaInfoUnexpectedResponse() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;LMSERROR&lt;/XMLFORMAT&gt;&lt;ERROR&gt;&lt;FechaHora&gt;20141229124222&lt;/FechaHora&gt;&lt;ERRORCODIGO&gt;-10300&lt;/ERRORCODIGO&gt;&lt;ERRORDESC&gt;Ocurrió un error al consultar los datos del invitado&lt;/ERRORDESC&gt;&lt;/ERROR&gt;&lt;/LMSXML&gt;</string>'
    );

    $tipo_consulta = 1;
    $this->assertEquals($tipo_consulta, ConsultaIEC::TIPOCONSULTA_INFO);

    $this->setExpectedException('\SocialSnack\WsBundle\Exception\UnexpectedWsResponseException');

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta,
        'ClaveIEC'     => '',
    ));
  }

  public function testConsultaTransactionsSuccess() {
    $this->setWsResponseContent(
        '<?xml version="1.0" encoding="utf-8"?> <string xmlns="http://tempuri.org/">&lt;?xml version= "1.0" encoding="utf-8" ?&gt;&lt;LMSXML&gt;&lt;XMLFORMAT&gt;MovimientosIEC&lt;/XMLFORMAT&gt;&lt;RESPUESTA&gt;&lt;TRANSACCION&gt;&lt;NUMIEC&gt;3086810421529002&lt;/NUMIEC&gt;&lt;FECHA&gt;05/11/2014 06:29:17 p. m.&lt;/FECHA&gt;&lt;COMPLEJO&gt;CINEMEX TENAYUCA&lt;/COMPLEJO&gt;&lt;ORIGEN&gt;PUNTO DE VENTA (DULCERIA)&lt;/ORIGEN&gt;&lt;TIPOMOV&gt;REDENCIONES&lt;/TIPOMOV&gt;&lt;PUNTOS&gt;276.00&lt;/PUNTOS&gt;&lt;/TRANSACCION&gt;&lt;/RESPUESTA&gt;&lt;/LMSXML&gt;</string>'
    );

    $tipo_consulta = 2;
    $this->assertEquals($tipo_consulta, ConsultaIEC::TIPOCONSULTA_TRANSACTIONS);

    $res = self::$ws->request(array(
        'TipoConsulta' => $tipo_consulta,
        'ClaveIEC'     => '1234567890123456',
    ));

    $this->assertInternalType('array', $res);
    foreach ($res as $item) {
      $this->assertInstanceOf('\SimpleXMLElement', $item);
      $this->assertObjectHasAttribute('NUMIEC',    $item);
      $this->assertObjectHasAttribute('FECHA',     $item);
      $this->assertObjectHasAttribute('COMPLEJO',  $item);
      $this->assertObjectHasAttribute('ORIGEN',    $item);
      $this->assertObjectHasAttribute('TIPOMOV',   $item);
      $this->assertObjectHasAttribute('PUNTOS',    $item);
      break; // Test only the first element.
    }
  }

} 