set :stages,        %w(production production2 production3 staging staging2 staging-all production-all)
set :default_stage, "staging"
set :stage_dir,     "app/config"
require 'capistrano/ext/multistage'

ssh_options[:port] = 60
ssh_options[:forward_agent] = true

set :user,        "cinemexssh"
set :use_sudo,    false
set :application, "Cinemex"
set :deploy_to,   "/var/www/html/cinemex.com"
set :app_path,    "app"

set :repository,       "git@bitbucket.org:socialsnack/cinemex.git"
set :scm,              :git
set :git_shallow_clone, 1

set :model_manager, "doctrine"

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
# logger.level = Logger::MAX_LEVEL

set :shared_files,      ["app/config/parameters.yml", web_path + "/hoststatus.html", web_path + "/sitemap.xml"]
set :shared_children,   [app_path + "/logs", app_path + "/spool", web_path + "/uploads", web_path + "/movie_posters", web_path + "/dynamic", app_path + '/lock', app_path + '/reports']
set :writable_dirs,     [app_path + "/logs", app_path + "/cache"]
set :webserver_user,    "apache"
set :permission_method, :acl
#set :use_set_permissions, true
set :use_composer, true
#set :update_vendors, true
set :copy_vendors, true

#set :deploy_via, :remote_cache

set :parameters_dir, "app/config"
set :parameters_file, false

task :upload_parameters do
  origin_file = parameters_dir + "/" + parameters_file
  if origin_file && File.exists?(origin_file)
    ext = File.extname(parameters_file)
    relative_path = "app/config/parameters" + ext

    if shared_files && shared_files.include?(relative_path)
      destination_file = shared_path + "/" + relative_path
    else
      destination_file = latest_release + "/" + relative_path
    end
    try_sudo "mkdir -p #{File.dirname(destination_file)}"

    top.upload(origin_file, destination_file)
  end
end

namespace :deploy do
  task :apache_graceful do
    capifony_pretty_print "--> Apache graceful restart"
    run "sudo /etc/init.d/httpd graceful"
    capifony_puts_ok
  end

  task :dump_assetic_locally do
    capifony_pretty_print "--> Clear Symonfy cache locally"
    run_locally "php app/console cache:clear --env=prod"
    capifony_puts_ok

    capifony_pretty_print "--> Dump assets with Assetic locally"
    run_locally "php app/console assetic:dump --env=prod"
    capifony_puts_ok
  end

  task :rsync_local_assets_to_server, :roles => :web do
    finder_options = {:except => { :no_release => true }}
    find_servers(finder_options).each {|s| run_locally "rsync -az --delete --rsh='ssh -p #{ssh_port(s)}' #{local_web_path}/js/ #{rsync_host(s)}:#{release_path}/web/js/" }
  end

  def local_web_path
    File.expand_path("web")
  end

  def rsync_host(server)
    :user ? "#{user}@#{server.host}" : server.host
  end

  def ssh_port(server)
    server.port || ssh_options[:port] || 22
  end

  task :syncassets do
    capifony_pretty_print "--> Sync assets with static servers"
    run "/home/#{user}/syncassets.sh", {:once => true}
    capifony_puts_ok
  end

  task :check_schema do
    capifony_pretty_print "--> Checking schema changes"
    sql = capture("php #{release_path}/app/console doctrine:schema:update --dump-sql", {:once => true})
    capifony_puts_ok
    if (sql =~ /Nothing to update/) == nil
      puts sql
      puts "Schema update needed. Run now? (y/n)"
      run_schema_update = STDIN.gets[0..0] rescue nil
      if run_schema_update == 'y' or run_schema_update == 'Y'
        capifony_pretty_print "--> Updating schema"
        run "php #{release_path}/app/console doctrine:schema:update --force", {:once => true}
      else
        capifony_pretty_print "--> Schema update skipped by user"
      end
      capifony_puts_ok
    end
  end

end

task :purgestatic do
  puts capture("/home/#{user}/clearcache-s.php static.cinemex.com /.+")
end

task :purgefront do
  puts capture("/home/#{user}/clearcache-f.php cinemex.com /")
end


#before "symfony:cache:warmup", "symfony:doctrine:schema:update"
after 'deploy:setup', 'upload_parameters'
#after 'deploy', 'deploy:cleanup'
before "deploy:update_code", "deploy:dump_assetic_locally"
after "deploy:update_code", "deploy:rsync_local_assets_to_server"
before "deploy:create_symlink", "deploy:check_schema"
