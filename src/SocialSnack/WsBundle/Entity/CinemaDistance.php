<?php

namespace SocialSnack\WsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CinemaDistance
 *
 * @ORM\Table(indexes={@ORM\Index(name="from_cinema_id", columns={"from_cinema_id"})})
 * @ORM\Entity
 */
class CinemaDistance {
    
	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Cinema")
	 * @ORM\JoinColumn(name="from_cinema_id", referencedColumnName="id", nullable=false) 
	 */
	private $from_cinema;
    
	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="\SocialSnack\WsBundle\Entity\Cinema")
	 * @ORM\JoinColumn(name="to_cinema_id", referencedColumnName="id", nullable=false) 
	 */
	private $to_cinema;
	
	/**
	 * @var float
	 * 
	 * @ORM\Column(name="distance", type="float")
	 */
	private $distance;
	

    /**
     * Set distance
     *
     * @param float $distance
     * @return CinemaDistance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    
        return $this;
    }

    /**
     * Get distance
     *
     * @return float 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set from_cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $fromCinema
     * @return CinemaDistance
     */
    public function setFromCinema(\SocialSnack\WsBundle\Entity\Cinema $fromCinema = null)
    {
        $this->from_cinema = $fromCinema;
    
        return $this;
    }

    /**
     * Get from_cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema 
     */
    public function getFromCinema()
    {
        return $this->from_cinema;
    }

    /**
     * Set to_cinema
     *
     * @param \SocialSnack\WsBundle\Entity\Cinema $toCinema
     * @return CinemaDistance
     */
    public function setToCinema(\SocialSnack\WsBundle\Entity\Cinema $toCinema = null)
    {
        $this->to_cinema = $toCinema;
    
        return $this;
    }

    /**
     * Get to_cinema
     *
     * @return \SocialSnack\WsBundle\Entity\Cinema 
     */
    public function getToCinema()
    {
        return $this->to_cinema;
    }
}