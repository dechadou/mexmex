<?php

namespace SocialSnack\WsBundle\Request;

use Buzz\Browser;
use SocialSnack\WsBundle\Observer\WsObserverAbstract;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Request {

  const PATH_DEFAULT = '/wsCinemexWeb/wsCinemexWeb.asmx';

	protected $initialized = FALSE;

	/**
	 * @var \SocialSnack\WsBundle\Request\ConsultaAbstract
	 */
	protected $query = NULL;
	protected $path = '';
	protected $container;
  protected $browser;
  protected $memcached;
	protected $observers = [];

  
	public function __construct(ContainerInterface $container, Browser $browser, \Memcached $memcached) {
		$this->container = $container;
    $this->browser = $browser;
    $this->memcached = $memcached;
	}
	
	
	/**
	 * Initialize the request.
	 * 
	 * @param string $method The Web Service method to call.
	 * @param array  $args
   * @return $this
	 */
	public function init($method, array $args = []) {
		$hosts  = $this->filterHosts($this->container->getParameter('cinemex_ws_host'));
		$client = $this->container->getParameter('cinemex_ws_client');
		$key    = $this->container->getParameter('cinemex_ws_key');

		if ( !is_null( $this->query ) ) {
			unset( $this->query );
			$this->query = NULL;
		}
		
		$this->query = $this->buildClient($method, $hosts, $client, $key, $this->browser);
		$this->query->setMemcached($this->memcached);

		$this->initialized = TRUE;

		foreach ($this->observers as $observer) {
			$this->query->addObserver($observer);
		}

		if (isset($args['retries'])) {
			$this->query->setRetries($args['retries']);
		}

		return $this;
	}


	/**
	 * Avoid using WS hosts with high failure ratio.
	 *
	 * @todo How I'm supposed to know if the faulty host is normal again if I'm not using it? :S
	 *
	 * @param array $hosts
	 * @return array
	 */
	protected function filterHosts(array $hosts) {
		$threshold = 30;
		$healthy   = [];

		foreach ($hosts as $host) {
			$key   = 'ws_fails_node_' . $host;
			$fails = $this->memcached->get($key);
			if ($fails < $threshold) {
				$healthy[] = $host;
			}
		}

		if (sizeof($healthy)) {
			return $healthy;
		} else {
			return $hosts;
		}
	}


	/**
	 * @param string  $method
	 * @param array   $hosts
	 * @param string  $client
	 * @param string  $key
	 * @param Browser $browser
	 * @return ConsultaAbstract
	 */
	protected function buildClient($method, array $hosts, $client, $key, Browser $browser) {
		$class = "\\SocialSnack\\WsBundle\\Request\\$method";
		$query = new $class($hosts, $client, $key, $browser);
		$query->pickHost();

		return $query;
	}
	
	
	/**
	 * Set the request arguments and return the request cURL Handler.
	 * 
	 * @param  array $args Arguments in $key => $value format.
	 * @return type cURL Handler.
	 */
	public function async_request( $args = array() ) {
		$this->query->set_args( $args );
		
		return $this->query->request(FALSE);
	}
	
	/**
	 * Set the request arguments and perform the request.
	 * 
	 * @param  array $args Arguments in $key => $value format.
	 * @return \SimpleXMLElement Request response.
	 */
	public function request( $args = array() ) {
		$this->query->set_args( $args );

		return $this->query->request();
	}
  
  
  public function parse_response($res) {
    return $this->query->parse_response($res);
  }


	public function addObserver(WsObserverAbstract $observer) {
		if (array_search($observer, $this->observers) !== FALSE) {
			return;
		}

		$this->observers[] = $observer;

		if ($this->initialized) {
			$this->query->addObserver($observer);
		}
	}


	public function addObservers(array $observers) {
		foreach ($observers as $observer) {
			$this->addObserver($observer);
		}
	}
		

	public function setTimeout($timeout) {
		$this->browser->getClient()->setTimeout($timeout);
	}

}
