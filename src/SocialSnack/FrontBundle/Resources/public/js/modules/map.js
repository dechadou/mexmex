( function() {
	var	$map	=	$( '#gmap' );
	if ( !$map.length )
		return;

	var	map_markers	=	[],
			map,
			state_names = [],
			states			=	{},
			info_window = null;


	/**
	 * Place the markers in the map.
	 *
	 * @param {array} places
	 * @param {Map} map
	 * @param {boolean} reset Remove the existing markers before inserting the
	 *                        new ones. Default = true.
	 */
	function set_markers( places, map, reset ) {
		// Reset by default.
		if ( 'undefined' == typeof reset )
			reset = true;

		if ( reset ) {
			for ( var i in map_markers ) {
				map_markers[i].setMap( null );
				delete map_markers[i];
			}

			map_markers = [];
		}

		$.each( places, function( i, place ) {
			var	$retailer	=	$( '#cinema-item-' + place.id ),
			    marker		=	new google.maps.Marker( {
						position	:	new google.maps.LatLng( place.lat, place.lng ),
						map				:	map,
            icon      : marker_img
			} );

			google.maps.event.addListener( marker, 'click', function() {
				$retailer.trigger( 'click' );
        $retailer.parent().prev().trigger( 'click' );
			} );
			place.lat = parseFloat( place.lat );
			place.lng = parseFloat( place.lng );

			map_markers.push( marker );
			$retailer.data( { marker : marker } );
		} );
	}


	/**
	 * Find the center by an average of the displayed markers coordinates.
	 *
	 * @param {Map} map
   * @param {boolean} no_zoom
	 */
	function center_markers_avg( map, no_zoom ) {
		var	minLat	=	null,
				maxLat	=	null,
				minLng	=	null,
				maxLng	=	null;

		for ( var i in map_markers ) {
			var	pos	=	map_markers[i].getPosition(),
					lat	=	pos.lat(),
					lng	=	pos.lng();

			if ( null === minLat || lat < minLat ) {
				minLat = lat;
			}
			if ( null === maxLat || lat > maxLat ) {
				maxLat = lat;
			}
			if ( null === minLng || lng < minLng ) {
				minLng = lng;
			}
			if ( null === maxLng || lng > maxLng ) {
				maxLng = lng;
			}
		}

		var	centerLat	=	maxLat + ( ( minLat - maxLat ) / 2 ),
				centerLng	=	maxLng + ( ( minLng - maxLng ) / 2 );

    // Center the map.
		map.setCenter( new google.maps.LatLng( centerLat, centerLng ) );

    if ( no_zoom )
      return;

    if ( minLat === maxLat && minLng === maxLng )
      return;

    // Calculate the best zoom to fit all the markers in the view.
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(new google.maps.LatLng(minLat,minLng));
    bounds.extend(new google.maps.LatLng(maxLat,maxLng));
    map.fitBounds(bounds);
	}


	/**
	 * Callback function when Gmaps SDK is loaded.
	 */
	window.gmaps_loaded = function() {
    var force_zoom = $( '#gmap' ).data( 'zoom' );
		var mapOptions = {
					zoom       : force_zoom || 9,
					mapTypeId  : google.maps.MapTypeId.ROADMAP
				},
		    map = new google.maps.Map( document.getElementById( "gmap" ), mapOptions );

		// Place the markers.
		set_markers( places, map );
		// Center the map.
		center_markers_avg( map, !!force_zoom );

		var $cinemas        = $( '.cinema-item' );

		$cinemas.bind( 'click', function( e ) {
			e.preventDefault();
			var	pos	= $( this ).data( 'pos' ).split( ',' );

			map.panTo( new google.maps.LatLng( pos[0], pos[1] ) );
			map.setZoom( 14 );

			$( this ).siblings().find( '.selected' ).removeClass( 'selected' );
			$( this ).find( 'a' ).addClass( 'selected' );

			if ( info_window ) {
				info_window.close();
				info_window = null;
			}

			info_window = new google.maps.InfoWindow( {
					content   : '<div class="map-info">'
						+ '<h1>' + $( this ).text() + '</h1>'
						+ '<p>' + $( this ).data( 'address' ) + '</p>'
						+ '<p style="text-align: center;"><a href="' + $( this ).find( 'a' ).attr( 'href' ) + '" class="btn btn-default btn-icon icon icon-marker">Ver Cartelera</a></p>'
						+ '</div>'
			} );
			info_window.open( map, $( this ).data( 'marker' ) );
		} ).css( { cursor : 'pointer' } );


		window.map = map;
	};

	LazyLoad.js( '//maps.googleapis.com/maps/api/js?key=' + config.gmaps_api_key + '&sensor=false&callback=gmaps_loaded' );
} )();
