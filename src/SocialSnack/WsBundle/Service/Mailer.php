<?php
namespace SocialSnack\WsBundle\Service;

class Mailer {

	protected $container;
	protected $doctrine;


	public function __construct( $container, $doctrine ) {
		$this->container = $container;
    $this->doctrine  = $doctrine;
	}


  public function enqueue( $msg ) {
    $this->execute((object)$msg);
  }


  public function execute( $msg ) {
//    try {
      $from = $this->container->getParameter( 'email_trans_from' );

      switch ( $msg->type ) {

        // Welcome emails.
        case 'welcome':
          $repo = $this->doctrine->getRepository( 'SocialSnackFrontBundle:User' );
          $user = $repo->find( $msg->user_id );
          $subject = 'Bienvenid@ a Cinemex';

          $body_plain = $this->container->get('templating')->render(
              'SocialSnackFrontBundle:Emails:welcome.txt.php',
              array(
                  'user' => $user,
                  'subject' => $subject
              )
          );
          $body_html  = $this->container->get('templating')->render(
              'SocialSnackFrontBundle:Emails:welcome.html.php',
              array(
                  'user' => $user,
                  'subject' => $subject
              )
          );

          $message = \Swift_Message::newInstance()
              ->setSubject( $subject )
              ->setFrom( array( $from['email'] => $from['name'] ) )
              ->setTo( array( $user->getEmail() => $user->getFullName() ) )
              ->setBody( $body_plain )
              ->addPart( $body_html, 'text/html' );
          break;

        // Tickets purchase confirmation email.
        case 'purchase':
          $repo    = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Transaction' );
          $trans   = $repo->find( $msg->trans_id );
          $subject = 'Tu compra en Cinemex';
          $data    = array(
              'subject'     => $subject,
              'transaction' => $trans,
              'session'     => $trans->getSession(),
          );

          $body_plain = $this->container->get('templating')->render(
              'SocialSnackFrontBundle:Emails:purchase.txt.php',
              $data
          );
          $body_html  = $this->container->get('templating')->render(
              'SocialSnackFrontBundle:Emails:purchase.html.php',
              $data
          );

          $recipients = isset( $msg->recipients )
              ? $msg->recipients
              : array( $trans->getUser()->getEmail() => $trans->getUser()->getFullName() );

          $message = \Swift_Message::newInstance()
              ->setSubject( $subject )
              ->setFrom( array( $from['email'] => $from['name'] ) )
              ->setTo( $recipients )
              ->setBody( $body_plain )
              ->addPart( $body_html, 'text/html' );
          break;


        // Password recovery email.
        case 'reset-pass':
          $message = $this->resetPassEmail($msg, $from);
          break;

        // Contact form email.
        case 'contact':
          $message = $this->contactEmail($msg, $from);
          break;

        // ARCO form email.
        case 'arco':
          $message = $this->arcoEmail($msg, $from);
          break;
        case 'corpsales':
          $message = $this->corpSalesEmail($msg, $from);
          break;
        case 'jobapplication':
          $message = $this->jobApplicationEmail($msg->data, $from);
          break;
      }

      if ( !$message )
        return FALSE;

      // Send the email.
      $sent = $this->container->get('mailer')->send( $message );

      if ( !$sent )
        return FALSE;

//		} catch ( \Exception $e ) {
//			\Doctrine\Common\Util\Debug::dump( $e->getMessage() );
//			return FALSE;
//		}
		return TRUE;
	}


  /**
   *
   * @param type $msg
   * @param type $from
   */
  protected function resetPassEmail($msg, $from) {
    $repo    = $this->doctrine->getRepository( 'SocialSnackFrontBundle:ResetCode' );
    $code    = $repo->find( $msg->code_id );
    $subject = 'Recuperar contraseña de Cinemex';
    $data    = array(
        'subject' => $subject,
        'code'    => $code,
        'user'    => $code->getUser(),
        'link'    => $this->container->get('router')->generate('user_reset_pass', array('code' => $code->getCode()), TRUE),
    );

    $body_plain = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:resetPass.txt.php',
        $data
    );
    $body_html  = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:resetPass.html.php',
        $data
    );

    $recipients = isset( $msg->recipients )
        ? $msg->recipients
        : array( $code->getUser()->getEmail() => $code->getUser()->getFullName() );

    $message = \Swift_Message::newInstance()
        ->setSubject( $subject )
        ->setFrom( array( $from['email'] => $from['name'] ) )
        ->setTo( $recipients )
        ->setBody( $body_plain )
        ->addPart( $body_html, 'text/html' );

    return $message;
  }


  /**
   *
   * @param type $json
   * @param type $from
   */
  protected function contactEmail($json, $from) {
    $contact_repo = $this->doctrine->getRepository( 'SocialSnackFrontBundle:Contact' );
    $cinema_repo  = $this->doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
    $contact      = $contact_repo->find( $json->contact_id );
    $subject      = 'Buzón Cinemex via cinemex.com';
    $data         = array(
        'subject' => $subject,
        'contact' => $contact,
        'cinema'  => $contact->getCinema() ? $cinema_repo->find($contact->getCinema()) : NULL,
    );

    $body_plain = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:contact.txt.php',
        $data
    );
    $body_html  = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:contact.html.php',
        $data
    );


    $message = \Swift_Message::newInstance()
        ->setSubject( $subject )
        ->setFrom( array( $from['email'] => $from['name'] ) )
        ->setReplyTo( array( $contact->getEmail() => $contact->getName() ) )
        ->setBody( $body_plain )
        ->addPart( $body_html, 'text/html' );

    $recipients = (array) $this->container->getParameter( 'email_contact_to' );

    foreach ($recipients as $recipient) {
      $message->addTo($recipient);
    }

    return $message;
  }


  /**
   *
   * @param type $json
   * @param type $from
   */
  protected function arcoEmail($json, $from) {
    $arco_repo  = $this->doctrine->getRepository( 'SocialSnackFrontBundle:ArcoEntry' );
    $entry      = $arco_repo->find( $json->entry_id );
    $form_data  = $entry->getData();
    $req_type   = explode(' ', $form_data->CveDerechoAccDat);
    $subject    =
        $form_data->owner->PrimerNombre . ' ' .
        $form_data->owner->SegundoNombre . ' ' .
        $form_data->owner->ApellidoPaterno . ' ' .
        $form_data->owner->ApellidoMaterno . ', ' .
        $req_type[0] . ', ' .
        ucwords(strtolower($form_data->Tipo_RelacionCMX));

    $subject = preg_replace('/\s+/', ' ', $subject);

    $data         = array(
        'subject' => $subject,
        'entry'   => $entry,
        'data'    => $form_data,
    );

    $body_plain = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:arco.txt.php',
        $data
    );
    $body_html  = $this->container->get('templating')->render(
        'SocialSnackFrontBundle:Emails:arco.html.php',
        $data
    );

    $recipients = $this->container->getParameter( 'email_arco_to' );

    $message = \Swift_Message::newInstance()
        ->setSubject( $subject )
        ->setFrom( array( $from['email'] => $from['name'] ) )
        ->setTo( $recipients )
        ->setBody( $body_plain )
        ->addPart( $body_html, 'text/html' );

    return $message;
  }

  protected function corpSalesEmail($data, $from) {
    $body_plain = $this->container
      ->get('templating')
      ->render('SocialSnackFrontBundle:Emails:corpsales.txt.php', [
        'subject' => 'Ventas corporativas',
        'data' => $data->data
      ]);

    $body_html = $this->container
      ->get('templating')
      ->render('SocialSnackFrontBundle:Emails:corpsales.html.php', [
        'subject' => 'Ventas corporativas',
        'data' => $data->data
      ]);

    return \Swift_Message::newInstance()
      ->setSubject('Ventas corporativas')
      ->setCc('pedrop@cinemex.net')
      ->setFrom([$data->data['email'] => $data->data['name']])
      ->setTo($data->recipient)
      ->setBody($body_plain)
      ->addPart($body_html, 'text/html');
  }

  protected function jobApplicationEmail($data, $from) {
    $cinema_repo   = $this->doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $cinema_email  = NULL;
    $manager_email = [];

    foreach (['cinema', 'family_cinema'] as $key) {
      if ($data['data'][$key] === '' OR !isset($data['data'][$key])) {
        continue;
      }

      if ($data['data'][$key] === -1) {
        $data['data'][$key] = 'Línea Cinemex (Call Center)';

        if ($key === 'cinema') {
          $cinema_email = 'lineacinemex@cinemex.net';
        }
      } else {
        $cinema = $cinema_repo->find($data['data'][$key]);
        $data['data'][$key] = $cinema->getName();

        if ($key === 'cinema') {
          $cinema_email  = $cinema->getData('EMAIL');

          $manager = $cinema->getData('manager');

          if (!empty($manager) AND isset($manager['email'])) {
            $manager_email[] = $manager['email'];
          }
        }
      }
    }

    if ($data['data']['area_type'] === 'cafes') {
      $cinema_email = 'cafecentralgourmet@cinemex.net';
    }

    if ($cinema_email === NULL) {
      return NULL;
    }

    $data['data']['general_state'] = $data['data']['general_state']->getName();

    $data['subject'] = 'Forma de solicitud de empleo';

    // Age calculation
    $now       = new \DateTime();
    $birthdate = $data['data']['general_birthdate'];

    $data['data']['general_age'] = $now->diff($birthdate)
      ->y;

    // Create emails templates
    $body_html = $this->container
      ->get('templating')
      ->render('SocialSnackFrontBundle:Emails:jobapplication.html.php', $data);

    $body_plain = $this->container
      ->get('templating')
      ->render('SocialSnackFrontBundle:Emails:jobapplication.txt.php', $data);

    $message = \Swift_Message::newInstance()
      ->setSubject($data['subject'])
      ->setFrom(array('no-reply@cinemex.com' => 'Cinemex'))
      ->setTo($cinema_email)
      ->setCc(['empleo@cinemex.net', 'amadav@cinemex.net'])
      ->setBody($body_plain)
      ->addPart($body_html, 'text/html');

    if (!empty($manager_email)) {
      $message->setCc($manager_email);
    }

    return $message;
  }
}
