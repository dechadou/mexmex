<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Viajes de Esperanza - Cinemex');
$view['slots']->set('body_class', 'sponsored-viajes-de-esperanza');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/landings/viajes/header.jpg'); ?>">
</div>

<div class="section-nav">
  <ul>
    <li><a href="<?php echo $view['router']->generate('viajes_index'); ?>">Viajes de Esperanza</a></li>
    <li><a href="<?php echo $view['router']->generate('viajes_terms'); ?>">Terminos y condiciones</a></li>
  </ul>
</div>

<div class="section-content">
  <img src="<?php echo $view['assets']->getUrl('assets/img/landings/viajes/content.jpg'); ?>">
</div>
<?php $view['slots']->stop(); ?>
