<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('modal_params', array('state_id'=>$state->getId()));
$view['slots']->set( 'title', 'Cines - Cinemex' );
$view['slots']->start( 'body' );
$places = array();
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('cinemas_archive'); ?>">Cines</a></li>
</ul>

<div class="clearfix row">
	
	<div class="side-col col">
		<div class="sidebar-heading">
			<span class="icon icon-star pad-left sidebar-title"><?php echo $state->getName(); ?></span>
      <div class="sidebar-selector">
        <a href="#" class="btn btn-default-alt switch-cinema btn-icon icon icon-rarr btn-icon-right" rel="ask_location">Selecciona otro estado</a>
        <select id="cinemas-select-city">
          <?php foreach ( $states as $_state ) { ?>
          <option value="<?php echo $_state->getId(); ?>" <?php if ( $_state->getId() == $state->getId() ) echo 'selected="selected"'; ?>><?php echo $_state->getName(); ?></option>
          <?php } ?>
        </select>
      </div>
		</div>
    
		<ul class="collapsable-list">
			<?php foreach ( $state->getAreas() as $area ) { ?>
			<li>
				<a href="#" class="sidebar-filter collapsable-anchor"><?php echo $area->getName(); ?></a>
				<ul class="collapsable-sublist">
					<?php foreach ( $area->getCinemas() AS $cinema ) { ?>
					<?php $places[] = array( 'lat' => $cinema->getLat(), 'lng' => $cinema->getLng(), 'id' => $cinema->getId() ); ?>
					<li id="cinema-item-<?php echo $cinema->getId(); ?>" class="cinema-item" data-pos="<?php echo $cinema->getLat() . ',' . $cinema->getLng(); ?>" data-address="<?php if (is_string($cinema->getData( 'DIRECCION' ))) { $value = $cinema->getData( 'DIRECCION' ).' '.$cinema->getData( 'COLONIA' ); echo htmlentities($value); } ?>"><a href="<?php echo $view['router']->generate( 'cinema_single', array( 'cinema_id' => $cinema->getId(), 'slug' => FrontHelper::sanitize_for_url( $cinema->getName() ) ) ); ?>"><?php echo $cinema->getName(); ?></a></li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</div>
  <script>
    var fallback = <?php echo $fallback ? 'true' : 'false'; ?>;
  </script>
	
	
	<div id="" class="main-col col">
		<div id="gmap"></div>
	</div><!-- #cinema-details -->
</div>

<script>
	var places = <?php echo json_encode( $places ); ?>;
  var marker_img = '<?php echo $view['assets']->getUrl('assets/img/gmaps-marker.png'); ?>';
</script>
<?php $view['slots']->stop(); ?>


<?php $view['slots']->start('esi_featured_promos'); ?>
<?php
echo $view['actions']->render(
    $view['router']->generate('esi_important_promos', array('target' => 'maps')),
    array('strategy' => 'esi')
);
?>
<?php $view['slots']->stop(); ?>