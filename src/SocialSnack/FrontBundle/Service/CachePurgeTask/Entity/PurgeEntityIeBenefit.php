<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityIeBenefit extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('ie_benefits'),
            'env'  => 'front'
        ],
        [
            'path' => $this->frontUtils->get_cms_url($this->entity->getImage()),
            'env'  => 'static'
        ],
    ];
  }

  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\IeBenefitRepository::findActive:1'
            ],
            'env' => 'front'
        ]
    ];
  }

}