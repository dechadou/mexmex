<?php
/* @var $entry \SocialSnack\FrontBundle\Entity\ArcoEntry */
/* @var $data array */
?>
<p>
  <strong>Fecha de solicitud:</strong>
  <?php echo $entry->getDateSubmitted()->format('d-m-Y'); ?>
  <br/>
  <strong>Folio:</strong>
  <?php echo $entry->getRef(); ?>
</p>

<?php
$owner = array(
  'Primer Nombre' => $data->owner->PrimerNombre,
  'Segundo Nombre' => $data->owner->SegundoNombre,
  'Apellido Paterno' => $data->owner->ApellidoPaterno,
  'Apellido Materno' => $data->owner->ApellidoMaterno,
  'Fecha de nacimiento' => $data->owner->FechaNac,
  'Domicilio' => $data->owner->Domicilio,
  'Código Postal' => $data->owner->CP,
  'Estado' => $data->owner->Estado,
  'Identificación' => $data->owner->Identificacion,
  'Email' => $data->owner->Email,
  'Teléfono' => $data->owner->Telefono,
  'Horario de contacto' => $data->owner->Horario,
  'Anexo que acredite la identidad del titular' => $data->owner->attach_id->url,
);
$output = array();
foreach ( $owner as $label => $value ) {
  if ( $value ) {
    $output[] = sprintf('<strong>%s:</strong> %s', $label, $value);
  }
}
?>
<?php if ( sizeof($output) ) { ?>
<p>
  <strong>Datos Del Titular</strong><br/>
  <?php echo implode('<br/>', $output); ?>
</p>
<?php } ?>

<?php
$rep = array(
  'Primer Nombre' => $data->rep->first_name,
  'Segundo Nombre' => $data->rep->middle_name,
  'Apellido Paterno' => $data->rep->last_name,
  'Apellido Materno' => $data->rep->mlast_name,
  'Email' => $data->rep->email,
  'Identificación oficial' => $data->rep_attach->attach_id ? $data->rep_attach->attach_id->url : NULL,
  'Poder donde se acredite la representación del titular' => $data->rep_attach->attach_power ? $data->rep_attach->attach_power->url : NULL,
);
$output = array();
foreach ( $rep as $label => $value ) {
  if ( $value ) {
    $output[] = sprintf('<strong>%s:</strong> %s', $label, $value);
  }
}
?>
<?php if ( sizeof($output) ) { ?>
<p>
  <strong>Representante Del Titular</strong><br/>
  <?php echo implode('<br/>', $output); ?>
</p>
<?php } ?>


<p>
  <strong>Relación con Cinemex:</strong> <?php echo $data->Tipo_RelacionCMX; ?>
  <?php
  if ( $value = $data->NumEmpleado ) {
    echo '<br/><strong>Número de empleado:</strong> ' . $value;
  }
  if ( $value = $data->NumIE ) {
    echo '<br/><strong>Número de Invitado Especial:</strong> ' . $value;
  }
  if ( $value = $data->rel_provider ) {
    echo '<br/><strong>Compañía:</strong> ' . $value;
  }
  if ( $value = $data->SolicitaTipo ) {
    echo '<br/><strong>Tipo de puesto:</strong> ' . $value;
  }
  if ( $value = $data->SolicitaComplejo ) {
    echo '<br/><strong>Complejo:</strong> ' . $value;
  }
  if ( $value = $data->RFC ) {
    echo '<br/><strong>RFC:</strong> ' . $value;
  }
  if ( $value = $data->RelacionOtro ) {
    echo '<br/>' . $value;
  }
  ?>
</p>

<p>
  <strong>Solicitud:</strong>
  <?php echo $data->CveDerechoAccDat; ?>
  <?php
  if ( $value = $data->Comentario ) {
    echo '<br/><strong>Comentarios:</strong> ' . $value;
  }
  if ( $value = $data->DatoModificar ) {
    echo '<br/><strong>Dato a modificar:</strong> ' . $value;
  }
  if ( $value = $data->DatoActual ) {
    echo '<br/><strong>Dato actual:</strong> ' . $value;
  }
  if ( $value = $data->DatoNuevo ) {
    echo '<br/><strong>Actualizar por:</strong> ' . $value;
  }
  if ( $data->attach_doc && $value = $data->attach_doc->url ) {
    echo '<br/><strong>Documentación adjunta:</strong> ' . $value;
  }
  if ( $data->attach_doc2 && $value = $data->attach_doc2->url ) {
    echo '<br/><strong>Documentación adicional adjunta:</strong> ' . $value;
  }
  ?>
</p>