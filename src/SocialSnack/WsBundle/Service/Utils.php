<?php

namespace SocialSnack\WsBundle\Service;

use SocialSnack\WsBundle\Entity\Session;
use SocialSnack\WsBundle\Entity\State;
use SocialSnack\WsBundle\Entity\StateArea;
use SocialSnack\WsBundle\Entity\Task;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class Utils {

  protected $container;


  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }


	/**
	 * Find the largest poster and queue for download and processing.
	 *
	 * @todo Bound the image to the movie entity.
	 *
	 * @param \SocialSnack\WsBundle\Entity\Movie $movie   Movie entity.
	 * @param \stdClass                          $_movie  Movie object from the WS.
   * @param boolean                            $enqueue Enqueue the image download or process right now.
	 * @return boolean
	 */
	public function process_posters($movie, $_movie, $enqueue = TRUE) {
		$poster_url = '';
		$max_size   = 0;

		for ( $i = 1 ; $i <= 4 ; $i++ ) {
			$size = 0;

			if ( empty($_movie->{ 'POSTER' . $i }) )
				continue;

      if ( $_movie->{ 'POSTER' . $i } instanceof \stdClass )
        continue;

      $_movie->{ 'POSTER' . $i } = (string)$_movie->{ 'POSTER' . $i };

			$match = preg_match( '/.+\-([0-9]+)x([0-9]+)\.(.+)/', $_movie->{ 'POSTER' . $i }, $res );
			if ( $match ) {
				$size = $res[1] * $res[2];
			}
			if ( $size > $max_size || !$poster_url ) {
				$poster_url = (string)$_movie->{ 'POSTER' . $i };
				$max_size   = $size;
			}
		}

		if ( !$poster_url )
			return FALSE;

    $task_args = array(
        'legacy_id' => (int)$movie->getLegacyId(),
        'url'       => $poster_url,
        'type'      => 'movie_poster',
        'filename'  => $movie->getPosterUrl(),
    );

    if ($enqueue) {
      // Enqueue posters for download.
      $em = $this->container->get('doctrine')->getManager();
      $task = new Task();
      $task->setService('cinemex.image_downloader');
      $task->setData(serialize($task_args));
      $em->persist($task);
    } else {
      $downloader = $this->container->get('cinemex.image_downloader');
      $downloader->execute($task_args);
    }

		return TRUE;
	}


  public function group_sessions_by_cinema_n_movie($sessions) {
    $cinema_repo = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema');
    $data        = array();
    $cinema_ids  = array_map( function($a) { return $a->getCinema()->getId(); }, $sessions );
    $cinema_ids  = array_unique( $cinema_ids );
    if ($cinema_ids) {
      $cinemas   = $cinema_repo->findManyBy($cinema_ids);
    }

    foreach ($cinemas as $cinema) {
      $sessions = $this->group_sessions_by_movie($sessions, $cinema);
      if ( !sizeof($sessions) )
        continue;

      $data[] = array(
          'cinema'   => $cinema,
          'sessions' => $sessions,
      );
    }

    return $data;
  }


  public function group_sessions_by_movie($sessions, $cinema = NULL) {
    $movie_repo = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');

    // Although this results are not directly used, by quering this entities now
    // they are already available for Doctrine to use them when they are needed.
    $movie_ids  = array_map( function($a) { return $a->getMovie()->getId(); }, $sessions );
    $movie_ids  = array_unique( $movie_ids );
    if ($movie_ids) {
      $movies   = $movie_repo->findManyBy($movie_ids);
    }
    $parent_ids = array_map( function($a) { return $a->getParent() ? $a->getParent()->getId() : FALSE; }, $movies );
    $parent_ids = array_unique( $parent_ids );
    $parent_ids = array_values(array_filter( $parent_ids ));
    if ($parent_ids) {
      $parents  = $movie_repo->findManyBy($parent_ids);
    }

		/* @var $_sess \SocialSnack\WsBundle\Entity\Session */
		foreach ( $sessions as $_sess ) {
      if ( !is_null($cinema) && $_sess->getCinema()->getId() !== $cinema->getId() ) {
        continue;
      }

			$_movie_id = $_sess->getMovie()->getId();
      $_movie    = $_sess->getMovie();
			$_group_id = $_movie->getGroupId();
      $cinemom   = $_sess->isCinemom();

      if ( $cinemom ) {
        $_movie_id .= '-cinemom';
      }

      if ( !isset( $groups[$_group_id] ) ) {
        $groups[$_group_id] = array(
            'parent'   => $_movie->getParent() ? $_movie->getParent() : $_movie,
            'sessions' => array(),
        );
      }

      if ( !isset( $groups[$_group_id]['sessions'][$_movie_id] ) ) {
        $groups[$_group_id]['sessions'][$_movie_id] = array(
            'movie'    => $_movie,
            'sessions' => array(),
            'cinemom'  => $cinemom,
        );
      }
      $groups[$_group_id]['sessions'][$_movie_id]['sessions'][] = $_sess;
    }

    return $groups;
  }


  /**
   * Find all active sessions using a price card.
   *
   * @param \SocialSnack\WsBundle\Entity\Ticket $ticket
   * @return array
   */
  public function find_sessions_from_ticket(\SocialSnack\WsBundle\Entity\Ticket $ticket) {
    $qb = $this->container->get('doctrine')->getManager()->createQueryBuilder();
    /* @var $qb \Doctrine\ORM\QueryBuilder */
    $qb->addSelect('s')
        ->from('SocialSnackWsBundle:Session', 's')
        ->andWhere('s.cinema = :cinema_id')
        ->andWhere($qb->expr()->like('s.info', ':pricecode'))
        ->andWhere('s.active = 1')
        ->setParameter('cinema_id', $ticket->getCinema()->getId())
        ->setParameter('pricecode', '%"PRICECODE":"' . $ticket->getLegacyId() . '"%');

    $query = $qb->getQuery();
    return $query->getResult();
  }


  public function cinemomCinemasQuery() {
    $type = 'cinemom';
    $qb = $this->container->get('doctrine')->getManager()->createQueryBuilder();
    /* @var $qb \Doctrine\ORM\QueryBuilder */
    $qb->addSelect('s')
        ->addSelect('c')
        ->addSelect('m')
        ->from('SocialSnackWsBundle:Session', 's')
        ->innerJoin('s.cinema', 'c')
        ->innerJoin('s.movie', 'm')
        ->andWhere($qb->expr()->like('s.types', ':type'))
        ->andWhere('s.active = 1')
        ->addGroupBy('s.cinema')
        ->addOrderBy('c.name', 'ASC')
        ->addOrderBy('s.date_time', 'ASC')
        ->setParameter('type', '%"' . $type . '"%');

    $query = $qb->getQuery();
    $query->useResultCache(TRUE, 30 * 60, 'cinemomCinemasQuery');
    return $query->getResult();
  }


  public function getStatesWithMovieAttr($attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:State')->createQueryBuilder('st');
    $likeors = array();

    foreach ($attributes as $attr) {
      $likeors[] = sprintf("attributes LIKE '%%\"%s\":true%%'", $attr);
    }

    $conn = $this->container->get('doctrine.orm.entity_manager')->getConnection();

    $query = "SELECT group_id FROM Movie WHERE (" . implode(' OR ', $likeors) . ") AND active = 1 group by group_id;";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $group_ids = array_map(function($a) { return '\'' . $a['group_id'] . '\''; }, $stmt->fetchAll());

    $query = "SELECT c.state_id FROM Cinema c INNER JOIN Session s ON s.cinema_id = c.id WHERE s.group_id IN (" . implode(',', $group_ids) . ") AND s.active = 1 GROUP BY c.state_id";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $state_ids = array_map(function($a) { return $a['state_id']; }, $stmt->fetchAll());

    $qb
        ->addSelect('st')
        ->andWhere('st.active = 1')
        ->andWhere($qb->expr()->in('st.id', $state_ids))
    ;

    $qb->addGroupBy('st')
        ->addOrderBy('st.order', 'DESC')
        ->addOrderBy('st.name', 'ASC');

    $query  = $qb->getQuery();
    $query->useResultCache(TRUE, 60 * 60, __METHOD__ . ':' . implode('|', $attributes));
    return $query->getResult();
  }


  public function getAreasInStateWithMovieAttr($state_id, $attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:StateArea')->createQueryBuilder('a');
    $likestr = '%%"%s":true%%';
    $likeors = array();

    $qb->addSelect('a')
        ->innerJoin('a.cinemas', 'c')
        ->innerJoin('SocialSnackWsBundle:Session', 's', 'WITH', 'c.id = s.cinema')
        ->innerJoin('SocialSnackWsBundle:Movie', 'm', 'WITH', 'm.group_id = s.group_id')
        ->andWhere('a.state = :state_id')
        ->andWhere('s.active = 1')
        ->andWhere('a.active = 1')
        ->setParameter('state_id', $state_id);

    foreach ($attributes as $i => $attr) {
      $likeors[] = $qb->expr()->like('m.attributes', ':m_attributes_' . $i);
      $qb->setParameter('m_attributes_' . $i, sprintf($likestr, $attr));
    }
    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Orx($likeors));

    $qb->addGroupBy('a')
        ->addOrderBy('a.name', 'ASC');

    $query  = $qb->getQuery();
    $query->useResultCache(TRUE, 60 * 60);
    return $query->getResult();
  }


  public function getCinemasInStateWithMovieAttr($state_id, $attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $likestr = '%%"%s":true%%';
    $likeors = array();

    $qb->select('c')
        ->innerJoin('c.sessions', 's')
        ->innerJoin('SocialSnackWsBundle:Movie', 'm', 'WITH', 'm.group_id = s.group_id')
        ->andWhere('c.state = :state_id')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->setParameter('state_id', $state_id);

    foreach ($attributes as $i => $attr) {
      $likeors[] = $qb->expr()->like('m.attributes', ':m_attributes_' . $i);
      $qb->setParameter('m_attributes_' . $i, sprintf($likestr, $attr));
    }
    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Orx($likeors));

    $qb->addGroupBy('c.id')
        ->addOrderBy('c.name', 'ASC');

    $query  = $qb->getQuery();
    $query->useResultCache(TRUE, 60 * 60);
    return $query->getResult();
  }


  public function getStatesWithSessionAttr($attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:State')->createQueryBuilder('st');
    $likestr = '%%"%s"%%';
    $likeors = array();

    $qb->addSelect('st')
        ->innerJoin('SocialSnackWsBundle:Cinema', 'c', 'WITH', 'c.state = st')
        ->innerJoin('SocialSnackWsBundle:Session', 's', 'WITH', 'c.id = s.cinema')
        ->andWhere('s.active = 1')
        ->andWhere('st.active = 1');

    foreach ($attributes as $i => $attr) {
      $likeors[] = $qb->expr()->like('s.types', ':s_types_' . $i);
      $qb->setParameter('s_types_' . $i, sprintf($likestr, $attr));
    }
    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Orx($likeors));

    $qb->addGroupBy('st')
        ->addOrderBy('st.order', 'DESC')
        ->addOrderBy('st.name', 'ASC');

    $query  = $qb->getQuery();
    $query->useResultCache(TRUE, 60 * 60);
    return $query->getResult();
  }


  public function getAreasInStateWithSessionAttr($state_id, $attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:StateArea')->createQueryBuilder('a');
    $likestr = '%%"%s"%%';
    $likeors = array();

    $qb->addSelect('a')
        ->innerJoin('a.cinemas', 'c')
        ->innerJoin('c.sessions', 's')
        ->andWhere('a.state = :state_id')
        ->andWhere('s.active = 1')
        ->andWhere('a.active = 1')
        ->setParameter('state_id', $state_id);

    foreach ($attributes as $i => $attr) {
      $likeors[] = $qb->expr()->like('s.types', ':s_types_' . $i);
      $qb->setParameter('s_types_' . $i, sprintf($likestr, $attr));
    }
    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Orx($likeors));

    $qb->addGroupBy('a')
        ->addOrderBy('a.name', 'ASC');

    $query  = $qb->getQuery();
    return $query->getResult();
  }


  public function getCinemasInStateWithSessionAttr($state_id, $attributes) {
    $qb = $this->container->get('doctrine')->getRepository('SocialSnackWsBundle:Cinema')->createQueryBuilder('c');
    $likestr = '%%"%s"%%';
    $likeors = array();

    $qb->select('c')
        ->innerJoin('c.sessions', 's')
        ->andWhere('c.state = :state_id')
        ->andWhere('s.active = 1')
        ->andWhere('c.active = 1')
        ->setParameter('state_id', $state_id);

    foreach ($attributes as $i => $attr) {
      $likeors[] = $qb->expr()->like('s.types', ':s_types_' . $i);
      $qb->setParameter('s_types_' . $i, sprintf($likestr, $attr));
    }
    $qb->andWhere(new \Doctrine\ORM\Query\Expr\Orx($likeors));

    $qb->addGroupBy('c.id')
        ->addOrderBy('c.name', 'ASC');

    $query  = $qb->getQuery();
    return $query->getResult();
  }

  public function getCinemasMapData($route, $current_state = NULL, array $cinema_ids = NULL) {
    $state_id = $current_state;

    $found = TRUE;
    $tree  = array();

    // Get the current state
    $state   = $this->container->get('doctrine')
      ->getRepository('SocialSnackWsBundle:State')
      ->find($state_id);

    // Gets the states for this cinemas
    $qb = $this->container->get('doctrine')
      ->getRepository('SocialSnackWsBundle:Cinema')
      ->createQueryBuilder('c');

    $qb->select('s')
      ->innerJoin('SocialSnackWsBundle:State', 's', 'WITH', 's.id = c.state')
      ->andWhere('c.active = 1')
      ->addGroupBy('s')
      ->addOrderBy('s.order', 'DESC')
      ->addOrderBy('s.name',  'ASC');

    if ($cinema_ids !== NULL) {
      $qb->andWhere('c.legacy_id IN (:ids)')
        ->setParameter('ids', $cinema_ids);
    }

    $query  = $qb->getQuery();
    $states = $query->getResult();

    // Gets cinemas in the current state that matches the cinemas list
    $qb = $this->container->get('doctrine')
      ->getRepository('SocialSnackWsBundle:Cinema')
      ->createQueryBuilder('c');

    $qb->addSelect('c')
      ->innerJoin('SocialSnackWsBundle:State', 's', 'WITH', 's.id = c.state')
      ->andWhere('c.active = 1')
      ->andWhere('c.state = :state_id')
      ->addGroupBy('c.id')
      ->addOrderBy('c.name', 'ASC')
      ->setParameter('state_id', $state_id);

    if ($cinema_ids !== NULL) {
      $qb->andWhere('c.legacy_id IN (:ids)')
        ->setParameter('ids', $cinema_ids);
    }

    $query   = $qb->getQuery();
    $cinemas = $query->getResult();

    if (count($cinemas) === 0) {
      $found = FALSE;
    }

    foreach ($cinemas as $cinema) {
      $area = $cinema->getArea();

      if (!isset($tree[$area->getId()])) {
        $tree[$area->getId()] = array(
            'area'    => $area,
            'cinemas' => array(),
        );
      }

      $tree[$area->getId()]['cinemas'][] = $cinema;
    }

    usort($tree, function($a, $b){
      return ($a['area']->getName() > $b['area']->getName());
    });

    return array(
      'state'        => $state,
      'states'       => $states,
      'tree'         => $tree,
      'found'        => $found,
      'route'        => $route
    );
  }


  public function getMinSessionTime($area = NULL, $date = NULL) {
    $qb = $this->container->get('doctrine')->getManager()->createQueryBuilder();
    $qb->select('MIN(s.time)')->from('SocialSnackWsBundle:Session', 's')
        ->andWhere('s.active = 1');

    if (!is_null($area)) {
      $qb->innerJoin('s.cinema', 'c')
          ->andWhere('c.active = 1');

      if ($area instanceof State) {
        $qb->andWhere('c.state = :area_id');
      } elseif ($area instanceof StateArea) {
        $qb->andWhere('c.area = :area_id');
      }

      $qb->setParameter('area_id', $area->getId());
    }

    if ($date) {
      $qb->andWhere('s.date = :date')
          ->setParameter('date', $date->format('Y-m-d'));
    }

    $query = $qb->getQuery();
    $query->execute();
    $res = $query->getResult();

    if ($res) {
      return $res[0][1];
    } else {
      return FALSE;
    }
  }

}
