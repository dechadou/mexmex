<div class="form-group">
  <label class="col-sm-2 col-sm-2 control-label required"><?php echo $label; ?></label>
  <div class="col-sm-10">
    <?php echo $view['form']->errors($form) ?>
    <?php echo $view['form']->widget($form) ?>
  </div>
</div>