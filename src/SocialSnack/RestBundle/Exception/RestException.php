<?php

namespace SocialSnack\RestBundle\Exception;

/**
 * Class RestException
 * @package SocialSnack\RestBundle\Exception
 * @author Guido Kritz
 */
class RestException extends \Exception {

  protected $errorcode;

  protected $statuscode;

  protected $data = [];

  /**
   * @param string $errorcode
   * @param string $message
   * @param int $statuscode
   * @param int $code
   * @param \Exception $previous
   */
  public function __construct($errorcode = '', $message = '', $statuscode = 400, $code = 0, $previous = NULL) {
    $this->errorcode = $errorcode;
    $this->statuscode = $statuscode;
    parent::__construct($message, $code, $previous);
  }

  /**
   * @return string
   */
  public function getErrorCode() {
    return $this->errorcode;
  }

  /**
   * @return int
   */
  public function getStatusCode() {
    return $this->statuscode;
  }

  public function setData(array $data) {
    $this->data = $data;

    return $this;
  }

  public function getData() {
    return $this->data;
  }

} 