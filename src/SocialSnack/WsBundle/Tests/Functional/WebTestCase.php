<?php

namespace SocialSnack\WsBundle\Tests\Functional;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;

class WebTestCase extends LiipWebTestCase {

  protected function loadFixtures(array $classNames, $omName = null, $registryName = 'doctrine', $purgeMode = null) {
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
    $result = parent::loadFixtures($classNames, $omName, $registryName, $purgeMode);
    $this->getContainer()->get('doctrine')->getManager()->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=1'));
    return $result;
  }

}