<?php

namespace SocialSnack\AdminBundle\Tests\Fixtures\Controller;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\IeBenefit;

class IeBenefitsData implements FixtureInterface {

  static $benefits;

  public function load(ObjectManager $manager) {
    self::$benefits = [];

    $faker = \Faker\Factory::create();

    $benefit = new IeBenefit();
    $benefit
        ->setName($faker->title())
        ->setDescription($faker->text())
        ->setImage($faker->imageUrl())
        ->setCategory(IeBenefit::CATEGORY_BASIC)
        ->setActive(TRUE)
    ;
    self::$benefits[] = $benefit;
    $manager->persist($benefit);

    $manager->flush();
  }

}