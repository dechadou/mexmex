<?php

namespace SocialSnack\RestBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SocialSnack\FrontBundle\Entity\Splashscreen;

class LoadSplashesData implements FixtureInterface {

  static $splashes;

  public function load(ObjectManager $manager) {
    self::$splashes = [];

    $splash = new Splashscreen();
    $splash
        ->setFile('file.jpg')
        ->setDeeplink('cinemex://test/123')
    ;
    self::$splashes[] = $splash;
    $manager->persist($splash);

    $manager->flush();
  }

}