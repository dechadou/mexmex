<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<div class="bottom-box col-sm-2-5 col-md-2-6 col" id="footer-tweets">
  <span class="bottom-title">
    <span class="discicon icon-twitter icon-red"></span>
    <a href="https://twitter.com/cinemex" target="_blank">Twitter</a>
    <div class="bottom-box-top">
      <a href="#" class="tweets-scroll laquo">&laquo;</a>
      <a href="#" class="tweets-scroll raquo">&raquo;</a>
    </div>
  </span>

  <div class="tweets-list">
    <?php foreach ( array_slice( $tweets, 0, 2 ) as $item ) { ?>
    <div class="tweet-item">
      <a href="//twitter.com/<?php echo $item->getScreenName(); ?>" target="_blank"><img src="<?php echo $item->getThumb(); ?>" alt="" width="48" height="48" /></a>
      <div class="tweet-balloon">
        <a href="//twitter.com/<?php echo $item->getScreenName(); ?>" target="_blank" class="tweet-author">
          <span class="tweet-username"><?php echo $item->getUserName(); ?></span>
          @<?php echo $item->getScreenName(); ?>
        </a>
        <p>
          <?php echo FrontHelper::linkify_twitter_status( $item->getText() ); ?>
        </p>
      </div>
    </div>
    <?php } ?>
  </div>
  <script>
    var footer_tweets = <?php echo json_encode( $_tweets ); ?>;
  </script>
</div>

<div class="bottom-box col-sm-hidden col-md-1-6 col">
  <span class="bottom-title">
    <span class="discicon icon-instagram icon-red"></span>
    <a href="https://instagram.com/cinemex" target="_blank">Instagram</a>
  </span>
  <div>
    <?php foreach ( $instagram as $item ) { ?>
    <a href="<?php echo $item->getLink(); ?>" target="_blank" class="instargam-thumb"><img src="<?php echo preg_replace( '/http\:\/\//', '//', $item->getThumb() ); ?>" alt="" width="67" height="67" /></a>
    <?php } ?>
  </div>
</div>
