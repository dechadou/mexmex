<?php
$view->extend('SocialSnackFrontBundle:About:base.html.php');

$view['slots']->set('body_class', 'site-section');
?>

<?php $view['slots']->start('section_body'); ?>
<div class="content-box">
  <h1 class="section-title">Acerca de nosotros</h1>

  <div class="grid-row">
    <div class="grid-col-7">
      <div class="slider-container" data-slider>
        <div class="slider-slider">
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-01.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-02.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-03.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-04.jpg'); ?>">
          </div>
          <div class="slide" style="width:536px;height:302px;">
            <img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-05.jpg'); ?>">
          </div>
        </div><!-- slider-slider -->

        <div class="slider-dots grid-row">
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-01.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-02.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-03.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-04.jpg'); ?>"></a>
          <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/nosotros-05.jpg'); ?>"></a>
        </div>
      </div>
    </div>

    <div class="grid-col-5">
      <p>Cinemex, la sexta cadena de cines más grande del mundo, ha llevado La Magia del Cine a millones de personas alrededor de México desde su fundación en agosto de 1995, fecha en la cual, abrió sus puertas  al público con el complejo Cinemex Altavista.</p>
      <p>El concepto de Cinemex llegó a revolucionar la industria del cine en nuestro país al instalar salas múltiplex y tipo estadio para poder satisfacer las necesidades únicas del mercado.</p>
      <p>En Cinemex, nuestros clientes son nuestros invitados y nuestra misión es ser los mejores en divertir a la gente. Con la idea de poderle llegar a más personas nos hemos expandido a múltiples ciudades dentro de México a lo largo del tiempo,  teniendo un crecimiento del 35% anual en los últimos 6 años. Hoy en día contamos con más de 271 complejos y 2,361 pantallas alrededor del país y esperando crecer aún más.</p>
      <p>Para que nuestros invitados disfruten al máximo La Magia del Cine ofrecemos innovadores conceptos como Platino Cinemex y Premium Cinemex, cines de vanguardia equipados con lujosos asientos y servicio de alimentos a la sala; también el concepto CinemeXtremo, la cual posiciona a Cinemex como  cadena líder en México en número de salas con la última tecnología en imagen y sonido Dolby ATMOS.</p>
      <p>Al igual contamos con Cinemex 3D, la experiencia X4D, Casa de Arte y Contenido Alternativo, siendo este último un espacio donde los invitados podrán disfrutar de partidos de la NFL, UEFA Champions League, temporadas de Ópera, Ballet Bolshoi, entre otros. Adicional, ofrecemos funciones CineMá, donde los invitados podrán asistir acompañados de sus bebés a una sala acondicionada especialmente para ellos.</p>
      <p>Hoy en día, podemos orgullosamente comunicar que somos una cadena de cines 100% digital donde la calidad de la imagen y sonido crea una experiencia inolvidable para nuestros invitados, cumpliendo nuestra misión de ser los mejores en divertir a la gente.</p>
      <p>El año pasado fuimos reconocidos como el <strong>“Exhibidor Internacional del Año”</strong> durante la Convención Anual de ShowEast 2013,  premio que reconoce nuestro liderazgo, innovación y crecimiento constante en los últimos años.</p>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
