<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Comunicados - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('announcements_page'); ?>">Comunicados</a></li>
</ul>

<div class="clearfix row">
  <div class="narrow-side-col col">
    <?php echo $this->render( 'SocialSnackFrontBundle:Sections:pagesMenu.html.php', array( 'route' => $route ) ); ?>
  </div>
  <div class="wide-main-col col content-box">
    <article class="entry-full">
      <h1 class="content-box-title">Comunicados</h1>
      
      <div class="entry-body">
        <p>
          Grupo Cinemex SA de CV y Sus Integradas, en cumplimiento de las obligaciones contenidas en el segundo párrafo
          de la <strong>fracción IV y el inciso c) de la fracción V del artículo 70 de la Ley del Impuesto sobre la
          Renta</strong>, se manifiesta que el monto del impuesto diferido por Grupo Cinemex S.A. de C.V., como empresa
          integradora y sus subsidiarias como empresas integradas, es de 1,301,103.00 (Un millón trescientos un mil
          ciento tres pesos M.N).
          <br/>
          <strong>Nota:</strong> se informa que esta es la página de internet oficial de todas y cada una de las
          sociedades que tributan en el capítulo VI de la ley del Impuesto Sobre la Renta llamado <strong>"Del Régimen
          Opcional Para Grupos de Sociedades"</strong></p>
      </div>
        
    </article>
  </div>
</div>
<?php $view['slots']->stop(); ?>