<?php

namespace SocialSnack\RestBundle\Exception;

/**
 * Class NotFoundRestException
 * @package SocialSnack\RestBundle\Exception
 * @author Guido Kritz
 */
class NotFoundRestException extends RestException {

  /**
   * @param string $errorcode
   * @param string $message
   * @param int $statuscode
   * @param int $code
   * @param \Exception $previous
   */
  public function __construct($errorcode = 'not-found', $message = 'Not Found', $statuscode = 404, $code = 0, $previous = NULL) {
    parent::__construct($errorcode, $message, $statuscode, $code, $previous);
  }

} 