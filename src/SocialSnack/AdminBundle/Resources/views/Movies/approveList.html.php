<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

/** @var \SocialSnack\WsBundle\Entity\Movie $movie */

$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de películas a aprobar - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de películas a aprobar
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <form method="post">
            <table  class="display table table-bordered table-striped" id="example">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Poster</th>
                  <th>Nombre</th>
                  <th>Tipo</th>
                  <th>Atributos</th>
                  <th>Editar</th>
                  <th><input type="checkbox" id="check-all" /></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($movies as $movie) { ?>
                  <tr class="gradeX">
                    <td><?php echo $movie->getId(); ?></td>
                    <td><img src="<?php echo $view['fronthelper']->get_poster_url($movie->getPosterUrl(), '200x295'); ?>" alt="<?php echo $movie->getName(); ?>" width="154" height="230" /></td>
                    <td>
                      <?php echo $movie->getName(); ?>
                      <br/>
                      <em><?php echo $movie->getLegacyName(); ?></em>
                    </td>
                    <td><?php echo 'grouper' === $movie->getType() ? 'Agrupador' : ($movie->getParent() ? 'Versión' : 'Sin grupo'); ?></td>
                    <td><?php echo FrontHelper::get_movie_attr_str($movie->getAttr()); ?></td>
                    <td><a href="<?php echo $view['router']->generate('social_snack_admin_movie', array('movie_id' => $movie->getId())); ?>">Editar</a></td>
                    <td><?php echo $view['form']->widget($form['aprobar_' . $movie->getId()]); ?></td>
                  </tr>
                <?php } ?>
            </table>
            <table>
              <tr>
                <td colspan="4" align="right">
                  <button type="submit" class="btn btn-primary">Aprobar seleccionados</button>
                </td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[0, "desc"]]
    });
    
    $('#check-all').on('change', function(e) {
      e.stopPropagation();
      $('td input:checkbox').prop('checked', ($(this).prop('checked')));
    });
  });
</script>
<?php $view['slots']->stop(); ?>