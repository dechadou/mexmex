<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Platino - Instalaciones - Cinemex' );
$view['slots']->start('body')
?>

<?php
echo $view->render('SocialSnackFrontBundle:Sections:platinumMenu.html.php', [
  'route' => $route
]);
?>

<div class="section-content">
  <div class="content-box">
    <h1 class="section-title">Platino</h1>

    <div class="grid-row">
      <div class="grid-col-7">
        <div class="slider-container" data-slider>
          <div class="slider-slider">
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-04.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-00.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-01.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-02.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-03.jpg'); ?>">
            </div>
            <div class="slide" style="width:536px;height:302px;">
              <img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-05.jpg'); ?>">
            </div>
          </div><!-- slider-slider -->

          <div class="slider-dots grid-row">
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-04.jpg'); ?>"></a>
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-00.jpg'); ?>"></a>
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-01.jpg'); ?>"></a>
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-02.jpg'); ?>"></a>
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-03.jpg'); ?>"></a>
            <a class="grid-col-2" href="#"><img src="<?php echo $view['assets']->getUrl('assets/img/sections/platino-05.jpg'); ?>"></a>
          </div>
        </div>
      </div>

      <div class="grid-col-5">
        <p class="lead">Disfruta la Magia del Cine con la máxima comodidad en nuestro exclusivo concepto Platino Cinemex®.</p>
        <p>Platino Cinemex® cuenta con instalaciones de primer nivel, con una taquilla y lobby exclusivos que te permitirán vivir una experiencia de manera única.</p>
        <p>Nuestras salas se encuentran equipadas con cómodos reposets de piel reclinables para que disfrutes tu película favorita con la máxima comodidad, desde donde podrás solicitar a nuestro staff lo que se te antoje de nuestra selecta carta de alimentos gourmet y bar, todos listos para llevarlos hasta tu butaca.</p>
        <p>En Platino Cinemex®, tenemos todo lo necesario para que tu experiencia sea la mejor y disfrutes al máximo de la Magia del Cine.</p>
      </div>
    </div>
  </div>
</div>
<?php $view['slots']->stop(); ?>
