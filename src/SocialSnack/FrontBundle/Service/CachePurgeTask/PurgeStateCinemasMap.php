<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

use Symfony\Component\Routing\Router;

class PurgeStateCinemasMap implements UrlTaskInterface, DoctrineTaskInterface {

  protected $router;

  protected $stateId;

  public function __construct(Router $router) {
    $this->router   = $router;
  }


  public function setStateId($stateId) {
    $this->stateId = $stateId;
  }


  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('cinemas_archive', ['id' => $this->stateId]),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                sprintf('SocialSnack\WsBundle\Entity\StateRepository::findWithAreasAndCinemas:%d_', $this->stateId)
            ],
            'env' => 'front'
        ]
    ];
  }

}