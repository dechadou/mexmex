<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set( 'title', 'Webservice' );
$view['slots']->start('body');
?>
<section class="panel">
  <header class="panel-heading">
    Debug funciones
  </header>
  <div class="panel-body">
    <form action="" method="get">
      <div class="row">
        <div class="col-sm-2">
          <input type="text" name="session_id" class="form-control" value="<?php echo htmlentities($session_id); ?>" />
        </div>
        <div class="col-sm-2">
          <button type="submit" class="btn btn-primary">Consultar</button>
        </div>
      </div>
    </form>
  </div>
</section>

<?php if (isset($result)) { ?>
  <div id="debug">
    <section class="panel">
      <header class="panel-heading">
        Resultado
      </header>

      <?php $table_data = $result->getDataForTable(); ?>
      <table class="table table-striped">
        <thead>
          <tr>
            <?php foreach ($table_data['thead'] as $th) { ?>
              <th><?php echo $th; ?></th>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($table_data['tbody'] as $tr) { ?>
          <tr>
            <?php if ($tr['th']) { ?>
              <th><?php echo $tr['th']; ?></th>
            <?php } ?>
            <?php foreach ($tr['td'] as $td) { ?>
              <td><?php echo $td; ?></td>
            <?php } ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>

    </section>

    <?php if ($analysis = $result->getAnalysis()) { ?>
      <section class="panel">
        <header class="panel-heading">
          Observaciones
        </header>
        <div class="panel-body"><?php echo implode('<br/>', $analysis); ?></div>
      </section>
    <?php } ?>

    <?php if ($raw = $result->getRawData()) { ?>
      <section class="panel">
        <header class="panel-heading">
          Debug
        </header>
        <div class="panel-body"><pre><?php var_dump($raw); ?></pre></div>
      </section>
    <?php } ?>
  </div>
<?php } ?>
<?php $view['slots']->stop();