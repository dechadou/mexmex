<div class="bottom-box">
  <span class="bottom-title icon-shout">
    <span class="discicon icon-shout icon-red"></span>
    Promociones destacadas
    <div class="bottom-box-top">
      <a href="<?php echo $view['router']->generate('promos_archive'); ?>">Ver todas &rarr;</a>
    </div>
  </span>
  <div class="clearfix row box-row" id="featured-promos-holder"></div>
</div>
<script>
  var featured_promos = {
    1 : [],
    2 : []
  };
  
  <?php
  foreach ($promos as $promo) {
    $html = '';
    
    if ($promo->getImage()) {
      $html = sprintf(
          '<a href="%s" class="lightbox-img"><img src="%s" alt="%s" /></a>',
          $view['fronthelper']->get_cms_url($promo->getImage(), '700x573'),
          $view['fronthelper']->get_cms_url($promo->getThumb(), '490x116'),
          $promo->getTitle()
      );
    } elseif ($promo->getUrl()) {
      $html = sprintf(
          '<a href="%s" target="%s" class="promo_events" data-promo-type="destacada" data-promo-ref="%s"><img src="%s" alt="%s" /></a>',
          $promo->getUrl(),
          $promo->getLinkTarget(),
          implode(',', $target),
          $view['fronthelper']->get_cms_url($promo->getThumb(), '490x116'),
          $promo->getTitle()
      );
    } else {
      $html = sprintf(
          '<img src="%s" alt="%s" />',
          $view['fronthelper']->get_cms_url($promo->getThumb(), '490x116'),
          $promo->getTitle()
      );
    }
  ?>
  featured_promos[<?php echo $promo->getSequence(); ?>].push('<?php echo $html; ?>');
  <?php
  }
  ?>
</script>