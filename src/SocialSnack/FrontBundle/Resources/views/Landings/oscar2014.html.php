<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Oscar 2014 - Cinemex');
$view['slots']->set('body_class', 'sponsored-oscar2014');

$view['slots']->start('body');
?>

<style>
.section-oscar2014 iframe {
    display:block;
    height:5200px;
    width:100%;
}
</style>

<div class="section-oscar2014">
    <iframe src="//promocinemex.com/landing/oscar" onload="scroll(0, 0);"></iframe>
</div>

<?php $view['slots']->stop(); ?>
