<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask\Entity;

use SocialSnack\FrontBundle\Service\CachePurgeTask\DoctrineTaskInterface;
use SocialSnack\FrontBundle\Service\CachePurgeTask\UrlTaskInterface;

class PurgeEntityModal extends AbstractEntityTask implements UrlTaskInterface, DoctrineTaskInterface {

  public function getUrlResources() {
    return [
        [
            'path' => $this->router->generate('modal'),
            'env'  => 'front'
        ],
    ];
  }


  public function getDoctrineResources() {
    return [
        [
            'ids' => [
                'SocialSnack\FrontBundle\Entity\Modal::getList',
                'SocialSnack\FrontBundle\Entity\Modal::findActive'
            ],
            'env' => 'front'
        ]
    ];
  }

}