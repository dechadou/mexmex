<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Restablecer contraseña - Cinemex' );
$view['slots']->start('body')
?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['router']->generate('user_forgot_pass'); ?>">Recuperar Contraseña</a></li>
</ul>

<div class="clearfix row">
	
	<div class="narrow-side-col col">
    <?php
    $menu = array(
        array(
          'label' => 'Iniciar sesión',
          'route' => 'user_login',
          'class' => 'icon-user',
        ),
        array(
          'label' => 'Registro',
          'route' => 'user_register',
          'class' => 'icon-user',
        ),
    );
    ?>
    <ul class="panel-menu">
      <?php
      foreach ( $menu as $item ) {
        if ( !is_array( $item['route'] ) ) {
          $item['route'] = array( $item['route'] );
        }
        $current = in_array( 'user_login', $item['route'] );
      ?>
      <li><a href="<?php echo $view['router']->generate( $item['route'][0] ); ?>" class="btn <?php echo $current ? 'btn-default' : 'btn-light'; ?> btn-icon icon <?php echo $item['class']; ?>"><?php echo $item['label']; ?></a></li>
      <?php
      }
      ?>
    </ul>
  </div>

  <div class="wide-main-col col">
    <?php echo $view->render('SocialSnackFrontBundle:Partials:resetPassForm.html.php', ['code' => $code, 'success' => $success, 'flash_msgs' => $flash_msgs]); ?>
  </div>
</div>
<?php $view['slots']->stop(); ?>