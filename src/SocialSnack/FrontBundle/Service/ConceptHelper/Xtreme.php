<?php

namespace SocialSnack\FrontBundle\Service\ConceptHelper;

use SocialSnack\FrontBundle\Service\ConceptHelperAbstract;

/**
 * Class Xtreme
 * @package SocialSnack\FrontBundle\Service\ConceptHelper
 * @author Guido Kritz
 */
class Xtreme extends ConceptHelperAbstract {

  protected function getConceptName() {
    return 'CX';
  }

} 