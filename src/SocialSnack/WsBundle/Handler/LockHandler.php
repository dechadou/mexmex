<?php

namespace SocialSnack\WsBundle\Handler;

use Monolog\Logger;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\WsBundle\Exception\ResourceLockedException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class LockHandler
 * @package SocialSnack\WsBundle\Handler
 * @author Guido Kritz
 */
class LockHandler {

  protected $path;

  protected $fs;

  protected $logger;


  public function __construct($path, Logger $logger) {
    $this->path   = $path;
    $this->fs     = new Filesystem();
    $this->logger = $logger;
  }


  /**
   * @param string $lock_id
   * @return string
   */
  protected function getLockFilename($lock_id) {
    $safe_id = FrontHelper::sanitize_for_url($lock_id);
    $path = $this->path;
    return $path . $safe_id . '.wslock';
  }


  /**
   * @param string $lock_id
   * @param string $processes
   * @return bool
   */
  public function isLocked($lock_id, $processes = NULL) {
    $fs = $this->fs;
    $path = $this->getLockFilename($lock_id);
    $exists = $fs->exists($path);

    if ($exists) {
      if (!is_null($processes)) {
        $still_running = $this->isStillRunning($lock_id, $processes);
        if (!$still_running) {
          $this->logger->critical(sprintf(
              'Resource %s locked but owner process not found.',
              $lock_id
          ));

          $fs->remove($path);

          return FALSE; // No longer locked.
        }
      }

      $this->checkLockTime($lock_id);
    }

    return $exists;
  }


  protected function isStillRunning($lock_id, $processes) {
    $path = $this->getLockFilename($lock_id);

    $data = json_decode(file_get_contents($path));
    $pid = $data->pid;
    $processes = explode("\n", $processes);

    $pattern = '/^[0-9]+\s+' . $pid . '\s/';
    foreach ($processes as $process) {
      if (preg_match($pattern, $process)) {
        return TRUE;
      }
    }

    return FALSE;
  }


  /**
   * @param string $path
   * @return int
   */
  public function getFileModificationTime($path) {
    return filemtime($path);
  }


  /**
   * @param string $lock_id
   * @return bool TRUE if the time limit is not exceeded.
   */
  public function checkLockTime($lock_id) {
    $path       = $this->getLockFilename($lock_id);
    $filemtime  = $this->getFileModificationTime($path);
    $lock_time  = time() - $filemtime;
    $alert_time = $this->getAlertTime($lock_id);

    if ($lock_time > $alert_time) {
      $this->logger->critical(sprintf(
          'Resource %s locked for %d minutes.',
          $lock_id,
          $lock_time/60
      ));

      return FALSE;
    }

    return TRUE;
  }


  protected function getAlertTime($lock_id) {
    $default = 60 * 60;
    $path    = $this->getLockFilename($lock_id);
    $data    = json_decode(file_get_contents($path));

    return isset($data->alert_time)
        ? $data->alert_time
        : $default;
  }


  /**
   * @param string     $lock_id
   * @param bool|array $check
   * @param array      $options
   * @return int
   * @throws ResourceLockedException
   */
  public function setLock($lock_id, $check = TRUE, array $options = []) {
    if (is_array($check)) {
      $processes = $check['processes'];
      $check     = $check['check'];
    } else {
      $processes = NULL;
    }

    if ($check) {
      $locked = $this->isLocked($lock_id, $processes);
      if ($locked) {
        throw new ResourceLockedException();
      }
    }

    return $this->writeLock($lock_id, $options);
  }


  /**
   * @param string $lock_id
   */
  public function releaseLock($lock_id) {
    $fs = $this->fs;
    $path = $this->getLockFilename($lock_id);
    $fs->remove($path);
  }


  protected function writeLock($lock_id, array $options = []) {
    $fs = $this->fs;
    $path = $this->getLockFilename($lock_id);

    $fs->touch($path);

    $data = array_merge(
        [
            'pid' => getmypid(),
        ],
        $options
    );

    return file_put_contents($path, json_encode($data));
  }

}