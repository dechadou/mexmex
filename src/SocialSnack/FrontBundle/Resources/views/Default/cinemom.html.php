<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['cinemom']);
$view['slots']->set('title', 'CineMá - Cinemex');
$view['slots']->start('body')
?>
        <?php
        echo $view->render(
            'SocialSnackFrontBundle:Sections:cinemomMenu.html.php',
            array( 'route' => $route )
        );
        ?>

				<div class="cinemom-content">
          <p class="align-center">
            <h1 id="cinemom-logo">CineMá</h1>
          </p>
          <p><strong>CineMá</strong> es un concepto único creado por Cinemex, donde queremos consentir a las mamás y papás que han dejado de asistir al cine al convertirse en padres. Por esta razón te invitamos a que regreses a Cinemex acompañado de tu  bebé.</p>
          <p>En CineMá podrás asistir con tu bebé (no mayor a 2 años), a disfrutar los estrenos del momento los días jueves y domingos en cines participantes. La sala se adecuará para tu mayor comodidad de la siguiente manera:</p>
          <ul class="highlight">
           <li>El volumen se reduce al 50%.
           <li>El aire acondicionado se mantiene bajo.
           <li>La luz no se apaga por completo.
           <li>Contamos con servicio de dulcería a la sala.
           <li>Las proyecciones están programadas para la primera función de los jueves y domingos.
          </ul>

          <p class="bigger">¡No te pierdas la oportunidad de ir al cine con tu bebé y disfrutar la Magia del Cine!</p>

				</div>
<?php $view['slots']->stop(); ?>