<?php

namespace SocialSnack\WsBundle\Service;


abstract class WorkerAbstract {
  
  /**
   * @var string
   */
  protected $error;
  
  /**
   * @var boolean
   */
  protected $console_verbose = FALSE;
  
  /**
   * @var array
   */
  protected $console_log = array();
  
  /**
   * @var int
   */
  protected $time_start = 0;
  
  /**
   * @var int
   */
  protected $execution_time = 0;
  
  
  abstract public function execute($data);

  
  /**
   * Tasks queue is not actually implemented.
   * This method is only intended to maintain compatibility with older versions
   * of the code and should be deprecated.
   * 
   * @param type $msg
   */
  public function enqueue($msg) {
    $this->execute($msg);
  }
  
  
  /**
   * Set error description.
   * 
   * @param string $error
   */
  protected function setError($error) {
    $this->error = $error;
  }
  
  
  /**
   * Return the last issued error.
   * 
   * @return string
   */
  public function getError() {
    return $this->error;
  }
  
  
  /**
   * 
   * @param boolean $set
   */
  public function setConsoleVerbose($set = TRUE) {
    $this->console_verbose = $set;
  }
  
  /**
   * 
   * @return boolean
   */
  public function getConsoleVerbose() {
    return $this->console_verbose;
  }
  
  /**
   * 
   * @return array
   */
  public function getConsoleLog() {
    return $this->console_log;
  }
  
  
  /**
   * Print console output.
   * 
   * @param mixed $msg
   * @return boolean
   */
  protected function consoleInfo($msg) {
    if ( is_array($msg) && isset($msg[0]) && is_string($msg[0]) ) {
      $result = TRUE;
      foreach ( $msg as $m ) {
        $result = $result && $this->consoleInfo($m);
      }
      return $result;
    }
    
    $this->console_log[] = $msg;
    
    if ( !$this->console_verbose ) {
      return FALSE;
    }
    
    if ( is_string($msg) ) {
      echo $msg . "\r\n";
    } else {
      var_dump($msg);
    }
    
    return TRUE;
  }
  
  
  protected function start_timer() {
		$this->time_start = microtime(true); 
  }
  
  
  protected function stop_timer() {
		$time_end = microtime(true);
		$this->execution_time = ($time_end - $this->time_start)/60;
  }
  
}