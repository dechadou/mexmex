<?php

namespace SocialSnack\FrontBundle\Controller;

use SocialSnack\WsBundle\Exception\InvalidIECodeException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use SocialSnack\FrontBundle\Entity\User;

class UserController extends BaseController {


	/**
	 * @Route("/", name="user_profile")
	 * @Method({"GET"})}
	 * @Template(engine="php")
	 */
	public function profileAction() {
		$flash_msgs = $this->get_flash_bag();

		return array(
				'user'       => $this->getUser(),
				'route'      => $this->getRequest()->attributes->get('_route'),
				'flash_msgs' => $flash_msgs,
		);
	}


	/**
	 * @Route("/perfil/editar", name="user_profile_edit")
	 * @Method({"GET"})
	 * @Template(engine="php")
	 */
	public function profileEditAction() {
		$flash_msgs = $this->get_flash_bag();
		$user = $this->getUser();
		$form = $this->createForm( new \SocialSnack\FrontBundle\Form\Type\ProfileType(), $user );

		return array(
				'user'       => $this->getUser(),
				'route'      => $this->getRequest()->attributes->get('_route'),
				'flash_msgs' => $flash_msgs,
				'form'       => $form->createView(),
		);
	}


	/**
	 * @Route("/perfil/editar")
	 * @Method({"POST"})
	 */
	public function profileEditPostAction() {
		/** @todo Everything :D */
		$request     = $this->getRequest();
    $response    = new RedirectResponse( $this->generateUrl('user_profile') );
		$app_session = $request->getSession();

		$user = $this->getUser();
    $form = $this->createForm( new \SocialSnack\FrontBundle\Form\Type\ProfileType(), $user );
    $form->handleRequest( $request );

    if ( $user->getPlainPassword() ) {
      // If a new password was supplied then store the hash for that password.
      $factory = $this->get('security.encoder_factory');
      $encoder = $factory->getEncoder($user);
      $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
      $user->setPassword($password);
    }

    if ( $form->isValid() ) {
      $move_file = ( $user->getFile() instanceof \Symfony\Component\HttpFoundation\File\UploadedFile );

      $phone = $form->get('info_phone_mobile')->getData();
      $carrier = $form->get('info_phone_carrier')->getData();

      if (!empty($phone) AND !empty($carrier)) {
        $user->addInfo('mobile_number', $phone);
        $user->addInfo('mobile_carrier', $carrier);
      }

      $em = $this->getDoctrine()->getManager();
      $user->setUpdateDate();
      $em->persist( $user );

      if ( $move_file ) {
        $this->container->get('ss.front.utils')->upload_avatar($user);
      }

      $em->flush();

      // Tell the client-side app to refresh user info cache.
      $response->headers->setCookie( new \Symfony\Component\HttpFoundation\Cookie( 'fetch_cache', '1', 0, '/', null, false, false ) );

      $app_session->getFlashBag()->add( 'notice', 'Los cambios fueron guardados.' );
      return $response;
    }

    $has_errors = FALSE;
    foreach ( $form->all() as $child ) {
      foreach ( $child->getErrors() as $error) {
        $app_session->getFlashBag()->add( 'error', $error->getMessage() );
        $has_errors = TRUE;
      }
    }

    if ( !$has_errors ) {
      $app_session->getFlashBag()->add( 'error', 'Ocurrió un error. Por favor, verifica los datos ingresados.' );
    }

    $response->setTargetUrl( $this->generateUrl('user_profile_edit') );
    return $response;
	}


	/**
	 * @Route("/mis-compras", name="user_purchases")
	 * @Method({"GET"})
	 * @Template(engine="php")
	 */
	public function purchasesAction() {
    $trans_repo = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:Transaction' );
    $apps_repo = $this->getDoctrine()->getRepository( 'SocialSnackRestBundle:App' );
    $user = $this->getUser();
    /** @todo Find with movies in same query */
    $apps = $apps_repo->findAll();
    $transactions = $trans_repo->findBy( array( 'user' => $user ), array( 'purchase_date' => 'DESC' ) );
		return array(
				'user'         => $this->getUser(),
				'route'        => $this->getRequest()->attributes->get('_route'),
        'transactions' => $transactions,
        'apps'         => $apps
		);
	}


	/**
	 * @Route("/invitado-especial", name="user_ie")
	 * @Method({"GET"})
	 * @Template(engine="php")
	 */
  public function ieAction(Request $request) {
    $user         = $this->getUser();
    $info         = NULL;
    $transactions = NULL;
    $valid        = TRUE;

    if ( $iecode = $user->getIecode() ) {
      try {
        $req = $this->get('cinemex_ws');
        $req->init('ConsultaIEC');
        $info         = $req->request(array(
            'TipoConsulta' => \SocialSnack\WsBundle\Request\ConsultaIEC::TIPOCONSULTA_INFO,
            'ClaveIEC'     => $iecode,
        ));
        $transactions = $req->request(array(
            'TipoConsulta' => \SocialSnack\WsBundle\Request\ConsultaIEC::TIPOCONSULTA_TRANSACTIONS,
            'ClaveIEC'     => $iecode,
        ));
      } catch (InvalidIECodeException $e) {
        $valid = FALSE;
        $request->getSession()
            ->getFlashBag()
            ->add('notice', sprintf(
                'El número de Invitado Especial <strong>%d</strong> no es válido. Por favor, ingresa un nuevo código.',
                $user->getIecode()
            ));
      }
    }

		return array(
        'user'         => $this->getUser(),
        'route'        => $this->getRequest()->attributes->get('_route'),
        'info'         => $info,
        'transactions' => $transactions,
        'flash_msgs'   => $this->get_flash_bag(),
        'valid'        => $valid,
		);
	}


	/**
	 * @Route("/peliculas-vistas", name="user_movies_seen")
	 * @Method({"GET"})
	 * @Template(engine="php")
	 */
	public function moviesSeenAction() {
		return array(
				'user'  => $this->getUser(),
				'route' => $this->getRequest()->attributes->get('_route'),
		);
	}


	/**
	 * @Route("/quieres-ver", name="user_wanna_see")
	 * @Method({"GET"})
	 * @Template(engine="php")
	 */
	public function wannaSeeAction() {
		return array(
				'user'  => $this->getUser(),
				'route' => $this->getRequest()->attributes->get('_route'),
		);
	}

}
