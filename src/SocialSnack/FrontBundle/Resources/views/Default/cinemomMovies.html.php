<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('body_class', ['cinemom']);
$view['slots']->set('title', 'CineMá - Cinemex');
$view['slots']->start('body')
?>
<?php
echo $view->render(
    'SocialSnackFrontBundle:Sections:cinemomMenu.html.php',
    array( 'route' => $route )
);
?>

<div>

<?php
foreach ( $data as &$_cinema ) {
  $cinema   = $_cinema['cinema'];
  $sessions = $_cinema['sessions'];
?>
  <?php foreach ($sessions as &$_movie) : ?>
  <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="billboard-movie-poster">
            <img src="<?php echo $view['assets']->getUrl( $_movie['parent']->getPosterUrl(), 'MoviePosters' ); ?>" alt="<?php echo $_movie['parent']->getName(); ?>" width="60" height="89"/>
          </a>
          <a href="<?php echo $view['fronthelper']->get_movie_link( $_movie['parent'] ); ?>" class="mycinema-item-title"><?php echo $_movie['parent']->getName(); ?></a>
  <div class="billboard-li" data-movie-id="<?php echo $_movie['parent']->getId(); ?>">
    <a href="<?php echo $view['fronthelper']->get_cinema_link( $cinema ); ?>" class="mycinema-item-title"><?php echo $cinema->getName(); ?></a>
    <?php
    echo $this->render('SocialSnackFrontBundle:Partials:sidebar/movieSessions.html.php', array(
        'items'    => $_movie['sessions'],
        'platinum' => $cinema->getPlatinum(),
        'checkout_ref' => 'cinema',
    ));
    ?>
  </div>
  <?php endforeach; ?>
<?php } ?>
</div>
<?php $view['slots']->stop(); ?>