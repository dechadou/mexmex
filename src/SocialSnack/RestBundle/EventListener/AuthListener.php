<?php

namespace SocialSnack\RestBundle\EventListener;

use Acme\DemoBundle\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class AuthListener {

	public function onKernelController(FilterControllerEvent $event) {
		$controllers = $event->getController();

		/*
		 * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
		 * If it is a class, it comes in array format
		 */
		if (!is_array($controllers)) {
			return;
		}

		/* @var $controller \Symfony\Bundle\FrameworkBundle\Controller\Controller */
		$controller = $controllers[0];
		
//		if ($controller[0] instanceof TokenAuthenticatedController) {
//			$token = $event->getRequest()->query->get('token');
//			if (!in_array($token, $this->tokens)) {
//				throw new AccessDeniedHttpException('This action needs a valid token!');
//			}
//		}
	}

}