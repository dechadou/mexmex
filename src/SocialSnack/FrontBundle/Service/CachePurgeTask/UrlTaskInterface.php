<?php

namespace SocialSnack\FrontBundle\Service\CachePurgeTask;

interface UrlTaskInterface extends CachePurgeTaskInterface {

  public function getUrlResources();

}