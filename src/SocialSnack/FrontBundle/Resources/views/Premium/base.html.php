<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set('title', 'Premium - Cinemex');
?>

<?php $view['slots']->start('body'); ?>
<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/sections/premium-header.jpg'); ?>">
</div>

<div class="section-nav">
  <ul>
    <li><a href="<?php echo $view['router']->generate('premium_billboard'); ?>" <?php if ($current_page === 'billboard') : ?>class="selected"<?php endif; ?>>Cartelera</a></li>
    <li><a href="<?php echo $view['router']->generate('premium_cinemas'); ?>" <?php if ($current_page === 'cinemas') : ?>class="selected"<?php endif; ?>>Cines</a></li>
    <li><a href="<?php echo $view['router']->generate('premium_index'); ?>" <?php if ($current_page === 'index') : ?>class="selected"<?php endif; ?>>Premium</a></li>
  </ul>
</div>

<div class="section-content">
<?php $view['slots']->output('section_body'); ?>
</div>
<?php $view['slots']->stop(); ?>
