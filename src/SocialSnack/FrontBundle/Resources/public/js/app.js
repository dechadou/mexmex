LazyLoad.js( '//connect.facebook.net/es_ES/all.js#xfbml=1&appId=' + config.fb.app_id );
LazyLoad.js( '//platform.twitter.com/widgets.js' );
LazyLoad.js( '//apis.google.com/js/plusone.js' );

var user = {
  initialized : false,
	loaded      : false,
	registered  : false
};


$( '#billboard-filters select' ).customSelect( { customClass : 'billboard-options-select', addClasses : 'custom-select icon-small pad-left icon-grey' } );
$( '.sidebar-box select, #header-area select' ).customSelect( { customClass : 'std-form-select' } );
$( '.std-form select' ).customSelect( { customClass : 'std-form-select' } );
$( '.location-select' ).customSelect( { customClass : 'location-custom-select', addClasses : 'icon pad-right icon-dark' } );

$(document).ready(function() {
    $('.lightbox-img').magnificPopup({type:'image'});

    $('.lightbox-gal').magnificPopup({
        type:'image',
        gallery:{
            enabled:true,
            tPrev: 'Anterior', // Alt text on left arrow
            tNext: 'Siguiente', // Alt text on right arrow
            tCounter: '%curr% de %total%' // Markup for "1 of 7" counterIncrement
        }
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
});

setup_area_w_cinema();
init_user(function() {
  // Only ask location if it is required to display some data in current page.
  var _ask_location = $('.require-location').length > 0;

  // Hash #no-location in URL disables automatic ask location on load.
  if (document.location.hash.match(/\bno-location\b/)) {
    _ask_location = false;
  } else if (isset('no_location') && no_location === true) {
    _ask_location = false;
  }

  if (_ask_location) {
    if ( !isset('area_id', window) && !isset('preferred_cinema', user) && !getCookie( 'cinemex_location_ask' ) ) {
      if ( isset('selected_area_id', user) ) {
        ask_location(false, {
          area_id : user.selected_area_id,
          close_bt : false
        });
      } else {
        ask_location(true, {
          close_bt : false
        });
      }
    } else {
      billboard_tip();
    }
  }
  parse_dynamic_containers();
});


/////////////
// Begin User
/////////////

/**
 * Udate properties in User object.
 *
 * @param {object} args
 * @param {function} callback
 * @returns {undefined}
 */
function update_user( args, callback ) {
	for ( var key in args ) {
		user[ key ] = args[ key ];
	}
	persist_user( callback );
}


/**
 * Persist user info both client and server-side.
 *
 * @todo Store server-side.
 *
 * @param {function} callback
 * @returns {undefined}
 */
function persist_user( callback ) {
	$.localStorage.setItem('user', JSON.stringify( user ), function() {
    _persist_user( callback );
  });
}
function _persist_user( callback ) {
	user.loaded = true;

  if ( callback ) {
    callback();
  }
}


/**
 * Load user info.
 *
 * @param {function} cb Optiona. Callback function.
 * @returns {undefined}
 */
function init_user( cb ) {
	// Load user from local app cache if available.
  $.localStorage.getItem( 'user', function(_user) { _init_user(_user, cb) } );
}

function _init_user(_user, cb) {
	if ( _user ) {
		_user = $.parseJSON( _user );

		// Merge loaded user info with current user info.
		for ( var key in _user ) {
			if ( undefined !== user[key] && undefined === _user[key] )
				continue;
			user[key] = _user[key];
		}
		user.loaded = true;
	}

	if ( user.session_id && user.session_id !== $.cookie( 'PHPSESSID' ) ) {
		empty_user();
	}

  if ( getCookie( 'fetch_cache' ) ) {
    load_user_header_box();
  }

  // Suggest change the default area.
  if ( !isset('area_id', window) && (suggest_area_id = getCookie('suggest_area')) ) {
    var suggested_area = get_area_by_id(suggest_area_id);
    if ( suggested_area ) {
      $.display_announce({
        content : '¿Deseas establecer <strong>' + suggested_area.name + '</strong> como tu zona predeterminada?',
        actions : [
          {
            'class' : 'btn-icon icon icon-tick',
            caption : 'Aceptar',
            events  : [
              {
                event : 'click',
                callback : function(e) {
                  e.preventDefault();
                  update_user({
                    preferred_cinema : null,
                    selected_area_id : suggest_area_id
                  }, function() {
                    ask_location(false, { area_id : suggest_area_id });
                  });
                  $.close_announce($(this).closest('.top-announce'));
                }
              }
            ]
          }
        ],
        hide_on_scroll : true
      });
    }
    unsetCookie('suggest_area');
  }

	if ( user.template ) {
		parse_template( user.template );
	}

  $.each( [ 'area', 'cinema' ], function( i, k ) {
    if ( user['billboard_' + k] ) {
      var $select = $( '#billboard-filters select[name=' + k + ']' ),
          $child  = $select.find( 'option' ),
          val     = user['billboard_' + k],
          $opt    = $child.filter( '[value=' + val + ']' )
      ;

//      $child.filter( ':selected' ).removeAttr( 'selected' );
//      $opt.attr( { selected : true } );

      $select.val( val );

      $select.next( 'span' ).children( 'span' ).text( $opt.text() );

      $select.trigger( 'change' );

    }
  } );

  if ( !user.registered && ( $popup = $( '#pre-checkout-popup' ) ).length && !getCookie('no_pre_checkout_popup') ) {
    $popup.popup();
    $( 'a[rel="popup-close"]' ).bind( 'click', function( e ) {
      e.preventDefault();
      setCookie('no_pre_checkout_popup', 1, 3);
    } );
  }
//	if ( user.registered ) {
//		$( '#login-popup' ).remove();
//	}

  user.initialized = true;

  $.event.trigger( {
    type : 'user_init',
    user : user
  } );

  if ( cb )
    cb();

	return user;

	// @todo If not attempt to load from session.
}


function empty_user() {
	for ( var key in user ) {
//		if ( 'preferred_cinema' == key )
//			continue;

		delete user[key];
	}
}


( function() {
	var $form   = $( '#login-form' ),
	    $bt     = $form.find( 'button[type=submit]' ),
      working = false;

	$form.bind( 'submit', function( e ) {
		e.preventDefault();

    if ( working )
      return;

		if ( !$( '#login-user' ).run_validators() ) {
      $( '#login-user' ).ctl_msg( 'Por favor, ingresa tu email.' );
      return;
    }

		if ( !$( '#login-pass' ).run_validators() ) {
      $( '#login-pass' ).ctl_msg( 'Por favor, verifica tu contraseña.' );
      return;
    }

		$bt.addClass( 'btn-loading' );
    working = true;

		$.ajax( {
			url      : $form.attr( 'action' ),
			type     : 'post',
			data     : $form.serialize(),
			dataType : 'json',
			success  : login_success,
			error    : function() { login_error(); }
		} );
	} );

	function login_success( res ) {
    if ( !res.success )
      return login_error();

		parse_login_res(res, function() {
      if ( window['redirect_after_login'] ) {
        document.location = url_prefix;
        return;
      }
      $bt.removeClass( 'btn-loading' );
      working = false;
      $.close_popups();
    });

	}

	function login_error() {
		$bt.removeClass( 'btn-loading' );
    working = false;
		$bt.ctl_msg( 'Email o contraseña no válidos.', 228 );
	}

} )();


function load_user_header_box() {
  $.ajax( {
    url      : rest_url + 'me/loginData',
    type     : 'get',
    dataType : 'json',
    success  : function(res) { parse_login_res(res); }
  } );
}


function parse_login_res( res, cb ) {
  res.data.session_id = $.cookie( 'PHPSESSID' );

  parse_template( res.data.template );

  update_user(res.data, function() {
    unsetCookie( 'fetch_cache' );

    $.event.trigger( {
      type : 'user_login'
    } );

    if ( cb )
      cb();
  });

}


function login_with_cb( cb ) {
  var namespace = '_' + new Date().getTime();
  $( document ).bind( 'user_login.' + namespace, function() {
    $( document ).unbind( 'user_login.' + namespace );
    cb();
  } );
  $( '#login-popup' ).popup();
}

///////////
// End User
///////////


function parse_template( template ) {
	$.each( template, function( id, dom ) {
		$( '#' + id ).not( '.no-cache' ).html( dom );
	} );

  if (isset('template', user) &&  / class="discicon icon-dark icon-settings" title="Mi Perfil">Mi perfil<\/a>/.test(user.template['header-user'])) {
    load_user_header_box();
  }
}

function parse_dynamic_containers() {
  $( '.dynamic-container' ).each( function() {
    var $t     = $( this ),
        events = $t.data( 'listeners' ) ? $t.data( 'listeners' ).split( ' ' ) : [];

    if ( events.length ) {
      $.each( events, function( i, event ) {
        $(document).on( event, $.proxy( dynamic_container_event, $t ) );
      } );
    }
    dynamic_load( this );
  } );
}

function dynamic_container_event() {
	dynamic_load( this );
}


function dynamic_load( container, args ) {
	if ( !container )
		return false;

	var $t          = $( container ),
	    url         = $t.data( 'route' ),
	    _source     = $t.data( 'source' ).split( ' ' ),
			source_type = _source.shift(),
			source_str  = _source.join( ' ' );
	    data        = window;

	switch ( source_type ) {
		case 'fn':
			data = window[source_str]();
			break;

		case 'var':
			$.each ( source_str.split( '.' ), function( i, part ) {
				data = data[part];
				if ( 'undefined' === typeof data )
					return false;
			} );
			break;

    default:
      data = {};
      break;
	};

	if ( !args )
		args = {};


	if ( 'undefined' === typeof data )
		return false;

	matches = url.match( /\{(.+?)\}/g );
	if ( matches ) {
		$.each( matches, function( i, match ) {
			url = url.replace( match, data[match.substr(1,match.length-2)] );
		} );
	}

	if ( !url )
		return false;

	if ( args.url_append ) {
		url += args.url_append;
	}

	$t.blockui( 'loading' );
	$t.load( url_prefix + url, function(response, status, xhr) {
    if ( 'error' === status ) {
      $t.unblockui();
      return;
    }
		if ( ( cb = $t.data( 'loadcb') ) && ( 'function' === typeof window[cb] ) ) {
			window[cb]( $t );
		}
	} );
}


$( 'select[rel="set_cinema"]' ).bind( 'change', function( e ) {
  e.preventDefault();
  if ( id = $( this ).val() ) {
    set_cinema( get_cinema_by_id( id ) );
  }
} );


var sidebar_filter = '';
if ( filter = document.location.hash.match(/\bfilter=(.+)\b/) ) {
  sidebar_filter = filter[1];
}

function populate_sidebar_filters( $context, $filter ) {
  var types = {
    trad     : 'Mostrar salas tradicionales',
    platinum : 'Mostrar salas Platino',
    premium  : 'Mostrar salas Premium',
    cx       : 'Mostrar salas CinemeXtremo',
    v3d      : 'Mostrar 3D',
    v4d      : 'Mostrar X4D'
  };
  var show = -1;
  $.each(types, function(k, v) {
    if ($context.find('.mycinema-sessions-group[data-'+k+'=1]').length) {
      $('<option/>', { value : k, text : v }).appendTo($filter);
      show++;
    }
  });
  if ( show < 1 ) {
    $filter.remove();
    $filter.next('.sidebar-custom-select').remove();
  }
}

// Use this var to track how many times the user changes the cinema per pageview.
var change_cinema_count = 0;

function mycinema_sidebar_loaded( $context ) {
  $context.undelegate('.sidebar');

  var $filter = $context.find('[name=type]');
  $filter.bind('change', function() {
    sidebar_filter = $filter.val();
    filter_sidebar_movies( sidebar_filter );
  });

  populate_sidebar_filters($context, $filter);

  if (sidebar_filter && $filter.children('[value="' + sidebar_filter + '"]').length) {
    $filter.val(sidebar_filter).trigger('change');
  }

  $context.find( '[rel="show-sidebar-selects"]' ).bind( 'click', function( e ) {
		e.preventDefault();
    $( '#sidebar-selects' ).slideToggle();
  } );

	$context.find( '[rel="sidebar-state-change"]' ).bind( 'change', function( e ) {
		e.preventDefault();
    dynamic_load( $( this ).closest( '.dynamic-container' ), { url_append : '/area-' + $( this ).val() } );
	} );

  $context.find( 'select' ).customSelect( { customClass : 'sidebar-custom-select', addClasses : 'icon pad-right icon-dark' } );

  $context.find( 'select[name=date]' ).bind( 'change', function( e ) {
    e.preventDefault();
    $.event.trigger( {
      type   : 'change_location',
      cinema : user.preferred_cinema
    } );
  } );

  $context.find( 'select[name=cinema]' ).bind( 'change', function( e ) {
    e.preventDefault();
    var cinema = false,
        cinema_id = $( this ).val();

    if ( !cinema_id )
      return false;

    $.each( cinemas, function( i, _cinema ) {
      if ( _cinema.id == cinema_id ) {
        cinema = _cinema;
        return false; //break
      }
    } );

    if ( !cinema )
      return false;

    set_cinema( cinema );
  } );

  var $list = $context.find( '.mycinema-list:last' ),
      max_h;

  if ( $list.length ) {
    max_h = $( '.main-col' ).height() - ( $( '.main-col .movies-grid' ).length ? 58 : 0 ) - $list.position().top;

    $list.css( {
      'max-height' : max_h < 400 ? 400 : max_h
    } );
  }

  $context.delegate( '[rel=ask_location]', 'click.sidebar', function( e ) {
    e.preventDefault();
    ask_location();
    change_cinema_count++;
//    _gaq.push(['_setCustomVar', 1, 'Change cinema', change_cinema_count, 3]);
    ga('set', 'metric1', change_cinema_count);
//    _gaq.push(['_trackEvent', 'Page interaction', 'Change cinema']);
    ga('send', 'event', 'Page interaction', 'Change cinema');
  } );

  $context.delegate( '.mycinema-more', 'click.sidebar', function( e ) {
    e.preventDefault();
    $( this ).hide().siblings( '.hidden' ).hide().removeClass( 'hidden' ).fadeIn();
  } );

  $context.find( '.mycinema-sessions a' ).tooltip();

//  if ( !getCookie( 'cinemex_tour_shown' ) && !user.registered ) {
//    start_tour( true );
//  }

  billboard_tip();

  if ( (typeof is_home !== 'undefined') && is_home && movies && (typeof this_area_movies !== 'undefined') && this_area_movies ) {
    billboard_filter_by_ids( this_area_movies );
  }
}


function get_cinemasidebar_args() {

  var $side = $( '#sidebar-mycinema' ).closest( '.dynamic-container' ),
      args  = [];

  var id = user.preferred_cinema ? user.preferred_cinema.id : false;

  if ( !id )
    return undefined;

  var date = $side.find( '[name="date"]' ).val();
  if ( date ) {
    args.push( 'date-' + date );
  }

  return {
    id   : id,
    args : args.join( '/' )
  };
}


function get_movieside_args() {
  if ( !user.preferred_cinema )
    return false;

  var $side = $( '#location-sidebar' ).closest( '.dynamic-container' ),
      args  = [];

  if ( val = $side.find( '[rel="sidebar-state-change"]' ).val() ) {
    args.push( 'area-' + val );
  } else if ( user.billboard_area && ( val = user.billboard_area ) ) {
    args.push( 'area-' + val );
  }

  return {
    id   : user.preferred_cinema.id,
    args : args.join( '/' )
  };
}


function filter_sidebar_movies( filter ) {
  var $lists  = $( '.mycinema-list' ),
      $groups = $lists.find( '.mycinema-sessions-group' );
  if ( !$lists.length )
    return;

  $lists.find('.sidebar-nosessions').remove();

  $( '.mycinema-more' ).hide();
  $( '.mycinema-li.hidden' ).removeClass('hidden');
  $lists.find('.first-child').removeClass('first-child');

  if ( !filter ) {
    $groups
      .addClass('filtered').show();
  } else {
    $groups
      .removeClass('filtered').hide()
      .filter('[data-' + filter + ']')
        .show().addClass('filtered');
  }

  $lists.each(function() {
    $( this ).find('.filtered:first').closest('.mycinema-li').addClass('first-child');
  });
  $lists.find( '.mycinema-li' ).each(function() {
    var $t = $( this );
    if ( $t.find('.filtered').length ) {
      $t.show();
    } else {
      $t.hide();
    }
  });

  $lists.each(function() {
    var $list = $( this );
    if ( !$list.find('.filtered').length ) {
      var msg = 'No se encontraron funciones';
      if ( $list.attr('rel') == 'main-list' ) {
        msg += ' en este cine';
      }
      msg += ' que coincidan con las opciones seleccionadas.'
      $list.append('<div class="sidebar-nosessions">' + msg + '</div>');
    }
  });
}


/////////////
// BEGIN Tabs
/////////////

$( '.tabs-links' ).delegate( 'a', 'click', function( e ) {
	e.preventDefault();
	var $t         = $( this ),
	    i          = $t.parent().index(),
			$links     = $t.closest( '.tabs-links' ).children(),
			$container = $t.closest( '.tabs' ),
			$content   = $container.find( '.tab-content' ).eq( i );

	$links.find( '.selected' ).removeClass( 'selected' );
	$links.find( '.discicon' ).removeClass( 'icon-red' ).addClass( 'icon-light' );
	$t.addClass( 'selected' );
	$t.find( '.discicon' ).removeClass( 'icon-light' ).addClass( 'icon-red');
	$content.addClass( 'selected' ).siblings().removeClass( 'selected' );
} );

$( 'a[rel="tab-select"]' ).bind( 'click', function( e ) {
	e.preventDefault();
	var $tab = $( '#' + $( this ).data( 'tab' ) ),
	    i    = $tab.index(),
			$link = $tab.closest( '.tabs' ).find( '.tabs-links a' ).eq( i );

	$link.trigger( 'click' );
	$.scroll_to( $tab, -80 );
} );

/////////////
// END Tabs
/////////////


$( '.collapsable-list .collapsable-anchor' ).bind( 'click', function( e ) {
	e.preventDefault();
	$( this ).parent().siblings().find( '.selected' ).removeClass( 'selected' ).parent().find( '.collapsable-sublist' ).slideUp();
	$( this ).toggleClass( 'selected' ).parent().find( '.collapsable-sublist' ).slideToggle();
} );



$( '.movies-grid' ).delegate( '.watch-trailer-bt', 'click', function( e ) {
	e.preventDefault();

	var $t    = $( this ),
	    $p    = $t.closest( '.movies-grid-item' ),
			$grid = $p.closest( '.movies-grid' ),
			$s    = $p.siblings(),
      cols  = $grid.data( 'movies-per-row' ) || 4,
      col_n = ( ( $p.index() % cols ) + 1 )
	;

	$p.toggleClass( 'trailer-expanded' );
	$s.find( '.ingrid-trailer' ).remove();
	if ( $p.hasClass( 'trailer-expanded' ) ) {
		$p.stop().animate( { marginBottom : 650 } );
		$s.filter( '.trailer-expanded' ).removeClass( 'trailer-expanded' ).stop().css( { marginBottom : 0 } );
		var $container = $( '<div/>', { 'class' : 'ingrid-trailer movie-details-tabs' } ),
		    $title     = $( '<p/>',   { 'class' : 'ingrid-title', text : 'Estás viendo el trailer de:' } ),
				$btns      = $( '<div/>', { 'class' : 'ingrid-btns' } )
		;

		$( '<span/>',   { 'class' : 'ingrid-movie-title', text : $p.find( '.movies-grid-title' ).text() } ).appendTo( $title );

    if ($t.closest('.movies-grid').hasClass('upcoming-movies') === false) {
		  $( '<a/>',      { href : $p.find( '.movies-grid-title' ).attr( 'href' ), 'class' : 'btn btn-default btn-icon icon icon-ticket', text : 'Comprar boletos' } ).appendTo( $btns );
    }

		$( '<a/>',      { href : '#', 'class' : 'btn btn-icon btn-notxt icon icon-close', rel : 'ingrid-trailer-close', text : 'Cerrar' } ).appendTo( $btns );
		$title.appendTo( $container );
		$btns.appendTo( $container );
		$( '<iframe/>', { width : '100%', height : 450, src : '//www.youtube.com/embed/' + $t.data( 'youtube-id' ) + '?rel=0', frameborder : 0, allowfullscreen : true } ).appendTo( $container );
		$container.css( { left : ( -100 * ( $p.index() % cols ) ) + '%' } ).hide();
		$p.append( $container );
		$container.slideDown();
    $container.addClass( 'col-' + col_n + ' cols-' + cols );
    $.scroll_to( $container, -50 );
	} else {
		$p.stop().animate( { marginBottom : 0 } );
		$p.find( '.ingrid-trailer' ).slideUp( { complete : function() { $( this ).remove(); } } );
	}
} );

$( '.movies-grid' ).delegate( '[rel="ingrid-trailer-close"]', 'click', function( e ) {
	e.preventDefault();

	var $t    = $( this ).closest( '.ingrid-trailer' ),
	    $p    = $t.closest( '.movies-grid-item' )
	;

	$p.removeClass( 'trailer-expanded' ).stop().animate( { marginBottom : 0 } );
//	$t.remove();
	$t.slideUp( { complete : function() { $( this ).remove(); } } );
} );



//////////////////////////
// BEGIN billboard filters
//////////////////////////

( function() {
  if ( 'undefined' === typeof movies )
    return;

  var filters         = '',
      _movies         = movies,
      page            = 1,
      movies_per_page = 32,
      $load_more_bt   = $( '[rel="grid-load-more"]' )
  ;

  function filter_by_ids( ids ) {
    var $bill = $('[rel="main-billboard"]'),
        $grid = $bill.find('.movies-grid'),
        $bt   = $bill.find('[rel="grid-load-more"]');

    $grid.children().remove();
    movies_per_page = $grid.data( 'movies-per-page' );
    _movies = movies.filter( function(m){ return $.inArray(m.id, ids) > -1; } );
    page = 1;
    append_movies( get_movies_page(), $grid, $bt );
  }
  window.billboard_filter_by_ids = filter_by_ids;

  function append_movies( movies, $grid, $bt ) {
    var args = {};
    var cols;

    if (cols = $grid.data('movies-per-row')) {
      args.colClass = 'col-sm-1-' + (cols - 1) + ' col-md-1-' + cols;
    }

    $movies = render_movie_grid(movies, args);
    $movies.hide();
    $grid.append( $movies );
    $movies.fadeIn();
    show_hide_paginator( $bt );
  }

  function get_movies_page() {
    var start = ( page - 1 ) * movies_per_page;

    return _movies.slice( start, start + movies_per_page );
  }

  function load_next_page( $bt, $grid ) {
    page++;
    movies_per_page = $grid.data( 'movies-per-page' );
    append_movies( get_movies_page(), $grid, $bt );
    show_hide_paginator( $bt );
  }

  function show_hide_paginator( $bt ) {
    var start = page * movies_per_page;
    if ( _movies.length <= start ) {
      $bt.hide();
    } else {
      $bt.show();
    }
  }

  $load_more_bt.bind( 'click', function( e ) {
    e.preventDefault();
    load_next_page( $( this ), $( this ).parent().parent().find( '.movies-grid:first' ) );
  } );

} )();


$( '#cinemas-select-city' ).bind( 'change', function( e ) {
  e.preventDefault();

  var val = $(this).val();
  var url = document.location + '';

  if (!val) {
    return;
  }

  var pattern = $(this).data('pattern') || 'cines/{state_id}';
  var replacement = pattern.replace('{state_id}', val);
  var re = new RegExp('\\b' + pattern.replace('{state_id}', '?(\\d+)?') + '\\b');

  url = url.replace(re, replacement);
  $('.side-col').blockui( 'loading' );
  document.location = url;
} );
if ( $( '#cinemas-select-city' ).length && fallback ) {
  $( document ).bind( 'user_init', function() {
    var area_id  = false,
        state_id = false;
        id       = false;

    if ( user.preferred_cinema && ( id = user.preferred_cinema.id ) ) {
      cinema = get_cinema_by_id( id );
      area_id = cinema.area.id;
    }

    if ( area_id ) {
      $.each( areas, function( i, area ) {
        if ( area.id == area_id ) {
          state_id = area.state_id;
          return false;
        }
      } );
    }

    if ( state_id ) {
      $( '#cinemas-select-city' ).val( state_id );
      $( '#cinemas-select-city' ).trigger( 'change' );
    }
  } );
}


function get_img_size(url, size) {
  var parts = url.split('.'),
      ext   = parts.pop();

  return parts.join('.') + '-' + size + '.' + ext;
}

function render_movie_grid(movies, args) {
  if (!args) {
    args = {};
  }
  if (!'colClass' in args) {
    args.colClass = 'col-md-1-4';
  }

  console.log('args', args);

  var $wrapper = $( '<div/>' );
  $.each( movies, function( i, movie ) {
    var $el     = $( '<div/>', { 'class' : 'col col-xs-1-3 movies-grid-item ' + args.colClass } ),
        $img_a  = $( '<a/>',   { href : movie.url, 'class' : 'movies-grid-cover' } ),
        $img    = $( '<img/>', { src : get_img_size(movie.cover, '154x230'), alt : ''/*, width : 154, height : 230*/ } ),
        $title  = $( '<a/>',   { href : movie.url, 'class' : 'movies-grid-title', text : movie.name } ),
        $buy_bt = $( '<a/>',   { href : movie.url, 'class' : 'movie-poster-bt buy-tickets-bt', html : 'Consulta<br/>horarios' } ),
        $vid_bt = $( '<a/>',   { href : '#', 'class' : 'movie-poster-bt watch-trailer-bt', html : 'Ver<br/>trailer', data : { 'youtube-id' : movie.youtube_id } } )
    ;

    $img_a.append( $img );
    $el.append( $img_a );
    $el.append( $title );

    var show_premiere = true;
    var show_presale  = true;
    if (isset('type', movie)) {
      $.each( movie.type, function( h, type ) {
        var $bt;
        switch ( type ) {
          case 'classics':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-classics', text : 'Cl\u00e1sicos' } );
            show_presale = false;
            show_premiere = false;
            break;
          case 'v3d':
          case '3D':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-3d', text : '3D' } );
            break;
          case 'v4d':
          case '4D':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-x4d', text : '4D' } );
            break;
          case 'presale':
            if (show_presale) {
              $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-pre', text : 'Pre-venta' } );
              show_premiere = false;
            }
            break;
          case 'preview':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-preview', text : 'Pre-estreno' } );
            show_premiere = false;
            break;
          case 'rerelease':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-rerelease', text : 'Re-estreno' } );
            show_premiere = false;
            break;
          case 'tour':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-tour-de-cine', text : 'Tour de cine' } );
            show_premiere = false;
            break;
          case 'fest':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-festivales', text : 'Festivales' } );
            show_premiere = false;
            break;
          case 'exclusive':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-exclusive', text : 'Exclusiva' } );
            show_premiere = false;
            break;
          case 'premiere':
            if ( show_premiere ) {
              $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-new', text : 'Estreno' } );
            }
            break;
          case 'oscar_nominee':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-oscars', text : 'Nominada al Oscar' } );
            break;
          case 'oscar_winner':
            $bt = $( '<span/>', { 'class' : 'movie-poster-ribbon ribbon-winner', text : 'Ganadora del Oscar' } );
            break;
        }
        if ( $bt ) {
          $el.append( $bt );
        }
      } );
    }

    $el.append( $buy_bt );

    if (movie.youtube_id != false) {
        $el.append( $vid_bt );
    }

    $the_oscars_movies = [];

    if (jQuery.inArray(movie.id, $the_oscars_movies) !== -1) {
        var $bt = $('<span/>', {
            'class': 'movie-poster-ribbon ribbon-winner',
            'text':  'Ganadora Oscar'
        });

        $el.append($bt);
    }

    $wrapper.append( $el );
  } );

  return $wrapper.children();
}




////////////////////////////////////////////////////////////////////////////////
// BEGIN Banners
////////////////////////////////////////////////////////////////////////////////

$('.promo_events').on('click', function(e) {
  var $t = $(this);
  var options = {
    'nonInteraction' : 1
  };

  if (e.which === 1 && $t.attr('target') !== '_blank' && !e.shiftKey && !e.ctrlKey && !e.metaKey) {
    e.preventDefault();
    options.hitCallback = function() {
      document.location = $t.attr('href');
    };
  }

  ga('send', 'event', 'Promo ' + $t.data('promo-type') + ' "' + $t.attr('alt') + '"', 'Click', $t.data('promo-ref'), 1, options);
} );

$( '#main-slider' ).slider( {
  onslide : function( $old, $current ) {
    var $a;
    if ( $old.length && ( $a = $old.find( '.expanded' ) ).length ) {
      $a.trigger( 'click' );
      $current.closest( '.slider-slider' ).css( { height : 130 } );
    }
  }
} );

////////////////////////////////////////////////////////////////////////////////
// END Banners
////////////////////////////////////////////////////////////////////////////////


function get_moviescore_args() {
  var args = [];

  if ( user.initialized && user.registered && user.info.id ) {
    args.push( 'user-' + user.info.id );
  }

  return {
    args : args.join( '/' )
  };
}


( function() {
  var working = false;

  $( '#movie-score-set' )
    .delegate( '[rel=set-score]', 'mouseenter', function( e ) {
      $( this ).addClass( 'hover' )
        .prevAll().addClass( 'hover' )
        .nextAll().addClass( 'nohover' );
    } )
    .delegate( '[rel=set-score]', 'mouseleave', function( e ) {
      $( this ).removeClass( 'hover' )
        .prevAll().removeClass( 'hover' )
        .nextAll().removeClass( 'nohover' );
    } );

  $( '#movie-score-set' ).delegate( '[rel=set-score]', 'click', function( e ) {
    e.preventDefault();

    if ( working )
      return;

    var $t = $( this ),
        $num = $t.closest( '.movie-details-score' ).find( '.score-number' );

    if ( !user.registered ) {
      login_with_cb( function() {
        $t.trigger( 'click' );
      } );
      return;
    }

    $t.addClass( 'whole' );
    $t.prevAll().addClass( 'whole' );
    $t.nextAll().removeClass();

    working = true;
    $num.addClass( 'working' );

    var val = $t.data( 'score' ),
        id  = $t.data( 'id' )
    ;
    $.ajax({
      url      : rest_url + 'movies/' + id + '/vote',
      type     : 'post',
      dataType : 'json',
      data     : { value : val },
      success  : function( res ) {
        working = false;
        $num.removeClass( 'working' );
      },
      error    : function() {
        working = false;
        $num.removeClass( 'working' );
      }
    });
  } );
} )();


////////////////////////////////////////////////////////////////////////////////
// BEGIN Promos archive
////////////////////////////////////////////////////////////////////////////////

$( '#promos-archive' ).delegate( '.promo', 'click', function( e ) {
  e.preventDefault();
  var $t     = $( this ).parent(),
      $promo = $t.find( '.ingrid-promo' ),
      $open  = $t.siblings( '.promo-expanded' )
  ;

  $t.toggleClass( 'promo-expanded' );

  if ( $t.hasClass( 'promo-expanded' ) ) {
    $open.find( '.ingrid-promo' ).hide();
    $open.animate( { paddingBottom : 0 } );
    $open.removeClass( 'promo-expanded' );
    $promo.addClass( 'col-' + ( ( $t.index() % 5 ) + 1 ) );
    $promo.css( { opacity : 0 } ).removeClass( 'hidden' );
    var h = Math.max(412, $promo.outerHeight()) + 30;
    $promo.hide().css( { opacity : 1 } ).slideDown();
    $t.animate( { paddingBottom : h } );
  } else {
    $promo.hide();
    $t.animate( { paddingBottom : 0 } );
  }
} );



////////////////////////////////////////////////////////////////////////////////
// BEGIN Footer tweets
////////////////////////////////////////////////////////////////////////////////

( function() {
  var page  = 1,
      $list = $( '#footer-tweets .tweets-list' );

  function build_tweet( tweet ) {
    var author_url = '//twitter.com/' + tweet.screen_name,
        $tweet     = $( '<div/>', { 'class' : 'tweet-item' } ),
        $img       = $( '<img/>', { src : tweet.thumb, width : 48, height : 48 } ),
        $balloon   = $( '<div/>', { 'class' : 'tweet-balloon' } ),
        $author    = $( '<a/>',   { 'class' : 'tweet-author', href : author_url, target : '_blank', text : ' @' + tweet.screen_name } )
    ;

    $img.appendTo( $tweet );
    $img.wrap( $( '<a/>', { href : author_url, target : '_blank' } ) );
    $author.prepend( $( '<span/>', { 'class' : 'tweet-username', text : tweet.user_name } ) );
    $author.appendTo( $balloon );
    $balloon.append( $( '<p/>', { html : tweet.html } ) );
    $balloon.appendTo( $tweet );

    return $tweet;
  }

  function next_page( v ) {
    page += v;

    if ( page <= 0 ) {
      page = Math.floor( footer_tweets.length / 2 );
    }

    var start = ( page - 1 ) * 2;

    if ( start >= footer_tweets.length ) {
      page  = 1;
      start = 0;
    }
    $list.children().remove();

    $.each( footer_tweets.slice( start, start + 2 ), function( i, tweet ) {
      var $tweet = build_tweet( tweet );
      $tweet.hide();
      $list.append( $tweet );
      $tweet.fadeIn();
    } );
  }

  $( '#footer-tweets .tweets-scroll' ).bind( 'click', function( e ) {
    e.preventDefault();
    next_page( $( this ).hasClass( 'laquo' ) ? -1 : 1 );
  } );
} )();


////////////////////////////////////////////////////////////////////////////////
// BEGIN User pass recovery
////////////////////////////////////////////////////////////////////////////////

/**
 * Pass recovery form submit function.
 *
 * @param {type} $form
 * @param {type} $ui
 * @param {type} $bt
 * @returns {undefined}
 */
function pass_recovery_submit( $form, $ui, $bt ) {
  $.ajax({
    url      : rest_url + 'recoverPass',
    type     : 'post',
    dataType : 'json',
    data     : $form.serialize(),
    success  : function(res) {
      unset_working();
      $form.replaceWith( $( '<div/>', { text : 'En unos instantes recibirás un email con las instrucciones para recuperar tu contraseña.', 'class' : 'msg' } ) );
    },
    error    : function(res) {
      unset_working();

      var msg = 'Ocurrió un error. Por favor, inténtalo nuevamente.';

      if ( res.responseJSON && ( errorcode = res.responseJSON.errorcode ) ) {
        switch ( errorcode ) {
          case 'no-user-found':
            msg = 'No existe ningún usuario con el email ingresado.';
            break;
        }
      }
      $.msg_popup( msg, 'msg-error', true );
    }
  });

  function unset_working() {
    $ui.unblockui();
    $bt.removeClass('btn-loading');
  }
}


/**
 * Pass reset form submit function.
 *
 * @param {type} $form
 * @param {type} $ui
 * @param {type} $bt
 * @returns {undefined}
 */
function pass_reset_submit( $form, $ui, $bt ) {
  $.ajax({
    url      : $form.attr('action'),
    type     : 'post',
    dataType : 'json',
    data     : $form.serialize(),
    success  : function(res) {
      unset_working();
      $form.replaceWith( $( '<div/>', { text : '¡Felicidades! Ya puedes ingresar con tu nueva contraseña.', 'class' : 'msg' } ) );
    },
    error    : function(res) {
      unset_working();

      var msg = 'Ocurrió un error. Por favor, inténtalo nuevamente.';

      if ( res.responseJSON && ( errorcode = res.responseJSON.errorcode ) ) {
        switch ( errorcode ) {
          case 'invalid-data':
            msg = 'Los datos ingresados no son válidos.';
            break;
        }
      }
      $.msg_popup( msg, 'msg-error', true );
    }
  });

  function unset_working() {
    $form.data('working', false);
    $ui.unblockui();
    $bt.removeClass('btn-loading');
  }
}


/**
 * Contact form submit function.
 *
 * @param {type} $form
 * @param {type} $ui
 * @param {type} $bt
 * @returns {undefined}
 */
function contact_submit( $form, $ui, $bt ) {
  $.ajax({
    url      : $form.attr('action'),
    type     : 'post',
    dataType : 'json',
    data     : $form.serialize(),
    success  : function(res) {
      unset_working();
      var $msg = $( '<div/>', { text : '\u00a1Gracias! Ya hemos recibido tus comentarios y consultas, te responderemos a la brevedad.', 'class' : 'msg' } );
      $form.replaceWith( $msg );
      $.scroll_to( $ui, -20 );
    },
    error    : function(res) {
      unset_working();

      var msg = 'Ocurrió un error. Por favor, verifica los datos ingresados e inténtalo nuevamente.';

      $.msg_popup( msg, 'msg-error', true );
    }
  });

  function unset_working() {
    $form.data('working', false);
    $ui.unblockui();
    $bt.removeClass('btn-loading');
  }
}


////////////////////////////////////////////////////////////////////////////////
// BEGIN Tooltip
////////////////////////////////////////////////////////////////////////////////

$.fn.tooltip = function() {
  var id = 'tooltip-aSdmF45xX';

  $( this ).each( function() {
    $( this ).data( { tooltip : $( this ).attr( 'title' ) } ).removeAttr( 'title' );
  } );

  /** @todo This mouse events shouldn't be binded on touch devices. */
  $( this )
    .bind( 'mouseover', function() {
      $( '#' + id ).remove();
      var $t  = $( this ),
          $tt = $( '<div/>', { 'class' : 'tooltip', id : id, text : $t.data( 'tooltip' ) } ),
          $css = $t.data('tooltip-class');
      $tt.addClass($css);
      $tt.css( { opacity : 0 } ).appendTo( 'body' );
      $tt.css( {
        left : $t.offset().left - ( $tt.outerWidth() - $t.outerWidth() ) / 2,
        top  : $t.offset().top - $tt.outerHeight() - 20,
        position : 'absolute'
      } );
      $tt.animate( { opacity : 1, top : '+=10' } );
    } )
    .bind( 'mouseleave', function() {
      $( '#' + id ).fadeOut();
    } );
};


////////////////////////////////////////////////////////////////////////////////
// BEGIN closest cinemas
////////////////////////////////////////////////////////////////////////////////

+(function($){
    $('body')
        .on('click', '.mycinema-item-address', function(){
            $(this).toggleClass('open');
        });
})(jQuery);


////////////////////////////////////////////////////////////////////////////////
// BEGIN Invitado Especial levels
////////////////////////////////////////////////////////////////////////////////

+(function($){
    var tabs = $('.ie-levels-tabs .tab'),
        contents = $('.ie-levels-block');

    tabs
        .on('click', function(ev){
            ev.preventDefault();

            var el     = $(this),
                target = $(el.data('target'));

            if (el.hasClass('active')) {
                return;
            }

            tabs.removeClass('active');
            contents
                .removeClass('active')
                .hide();

            el.addClass('active');
            target
                .addClass('active')
                .fadeIn();
        })
        .first()
        .trigger('click');

    $('.ie-promos-grid').each(function(){
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled:true
            }
        });
    });
})(jQuery);


////////////////////////////////////////////////////////////////////////////////
// Landings
////////////////////////////////////////////////////////////////////////////////

;(function($){
    $('[data-landing-trigger]').on('click', function(ev){
        ev.preventDefault();

        var target = $(this).data('landing-target');

        if (target === undefined) {
          target = $(this).attr('href');
        }

        $('[data-landing-target], .landing-target').removeClass('active');
        $(target).addClass('active');
    });
})(jQuery);


////////////////////////////////////////////////////////////////////////////////
// Map toggler
////////////////////////////////////////////////////////////////////////////////

(function($){

  $('.cinema-single-map-trigger').on('click', function(ev){
    ev.preventDefault();
    $('.cinema-single-map-target').toggleClass('shown');
    $(this).toggleClass('opened');
    $('.cinema-single-map-target').slideDown();
    var a    = $(this).find('a'),
        text = a.text();
    a.text(text.toLowerCase() === 'ver mapa' ? 'Ocultar' : 'Ver mapa');
    google.maps.event.trigger(window.map,'resize');
    window.map.setZoom(10);

  });
})(jQuery);



////////////////////////////////////////////////////////////////////////////////
// Header area selector
////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready( function( $ ) {
  $( '#header-area' ).bind( 'submit', function( e ) {
    e.preventDefault();
    var $select = $('#header-area-select');

    if (!$select.val()) {
      return;
    }

    document.location = $select.find(':selected').data('url');
  } );
} );


////////////////////////////////////////////////////////////////////////////////
// Header user menu
////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready( function( $ ) {
  $( '#header-user' ).delegate( '#header-user-bt > a', 'click', function( e ) {
    e.preventDefault();
    e.stopPropagation();
    var $t = $(this),
        o = $t.hasClass('open');

    if ( o ) {
      close_menu($t);
    } else {
      $('#header-user-menu').slideDown();
      $t.addClass('open');
      $(document).bind('click.headerusermenu', function() { close_menu($t); });
    }
  } );

  function close_menu($t) {
    $('#header-user-menu').slideUp();
    $t.removeClass('open');
    $(window).unbind('click.headerusermenu');
  }
} );




////////////////////////////////////////////////////////////////////////////////
// iPad
////////////////////////////////////////////////////////////////////////////////

$('#hamburger-bt').unbind('click').click(function(e){
  e.preventDefault();
  $(this).toggleClass('open');
  $('#top-menu').animate({marginLeft:$(this).hasClass('open')?0:-200});
});


jQuery(document).ready(function($){
  $('[data-slider]').each(function(){
    var $slider = $(this),
        $slides = $slider.find('.slider-slider'),
        $args   = $slider.data();

    $slides.height($slides.data('height'));
    $slider.slider($args);
  });
});



////////////////////////////////////////////////////////////////////////////////
// Featured promos
////////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function($){
  // Display a random banner on each place.
  if (!isset('featured_promos')) {
    return;
  }
  var $holder = $('#featured-promos-holder');
  var items = [];
  $.each(featured_promos, function(i, pos) {
    var html = pos[Math.floor(Math.random()*pos.length)];
    items.push('<div class="col-sm-1-2 col">' + html + '</div>');
  });
  $holder.append(items);
  $holder.find('.lightbox-img').magnificPopup({type:'image'});
});


////////////////////////////////////////////////////////////////////////////////
// Billboard tab text
////////////////////////////////////////////////////////////////////////////////

+(function($){
  var tabs = $('.file-tabs'),
      tab  = $('.tab-next-week'),
      day  = (new Date()).getDay(),
      text = 'Próxima semana';

  if (tab.size() !== 0) {
    if (day >= 1 && day <= 3) {
      text = 'Estrenos de esta semana';
    }

    tab.text(text);
  }

  tabs.removeClass('hidden');
})(jQuery);


////////////////////////////////////////////////////////////////////////////////
// BEGIN Lightbox helper
////////////////////////////////////////////////////////////////////////////////

function show_lightbox(images, options){
  var image, rand, defaults;

  if (images.length === 0) {
    return;
  }

  defaults = {
    mainClass: 'mfp-fade',
    tLoading: 'Cargando...',
    tClose: 'Cerrar',
    delay: 7000
  };

  options = $.extend({}, defaults, options);

  var args = options;

  args.callbacks = {};
  args.callbacks.imageLoadComplete = function(){
    var self = this;

    setTimeout(function(){
      self.close();
    }, options.delay);
  }

  rand  = Math.floor(Math.random() * ((images.length - 1) - 0 + 1)) + 0;
  image = images[rand];

  args.items = {
    src: image,
    type: 'image'
  };

  $.magnificPopup.open(args, 0);
}

jQuery(document).ready(function($) {
  if (Is.Home() && Is.iPad() && !getCookie('skip_store_landing')) {
    setCookie('skip_store_landing', '1', 2);
    document.location = 'http://m.cinemex.com/appios?redirect_url=' + encodeURIComponent('http://cinemex.com/');
  }
});
