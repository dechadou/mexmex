<?php
namespace SocialSnack\WsBundle\Command;

use SocialSnack\FrontBundle\Service\Utils;

class AvatarsRegenCommand extends AbstractThumbRegenCommand {

  protected function configure() {
    $this
        ->setName('cinemex:avatars:regenerate')
        ->setDescription('Regenerate avatars thumbnails')
    ;
  }


  protected function getFiles() {
    $res = $this->getContainer()
        ->get('doctrine')
        ->getRepository('SocialSnackFrontBundle:User')
        ->createQueryBuilder('u')
        ->select('u.avatar')
        ->where('u.avatar != :default')
        ->setParameter('default', 'default-avatar.png')
        ->getQuery()
        ->getArrayResult();
    ;

    $files = array_map(function($row) { return $row['avatar']; }, $res);

    // Workaround to skip FB avatars (not hosted by us).
    $files = array_filter($files, function($f) { return substr($f, 0, 2) !== '//'; });

    // Make sure no file is processed more than once.
    $files = array_unique($files);

    return $files;
  }


  protected function getAvailableSizes() {
    return Utils::$avatar_sizes;
  }


  protected function process($files, $sizes) {
    /** @var Utils $utils */
    $utils = $this->getContainer()->get('ss.front.utils');

    $servers = $this->getContainer()->getParameter('static_fss_enabled');

    foreach ($files as $filename) {
      $this->output->write(sprintf('File %s... ', $filename));

      $original = 'http:' . $utils->get_avatar_url($filename, NULL);

      try {
        $source = @file_get_contents($original);

        if (!$source) {
          throw new \Exception('download error');
        }

        $utils->resize_n_upload_to_cdn(
            'uploads/avatars/' . $filename,
            $source,
            $servers,
            $sizes,
            TRUE
        );

        $result = '<fg=green>saved</>';
      } catch (\Exception $e) {
        $result = sprintf('<fg=red>%s</>', $e->getMessage());
      }

      $this->output->writeln($result);
    }
  }

}
