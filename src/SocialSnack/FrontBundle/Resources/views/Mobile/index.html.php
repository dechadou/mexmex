<?php $view->extend('SocialSnackFrontBundle:Mobile:skeleton.html.php'); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="alternate" href="cinemex://com.cinemex/home" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start('base'); ?>
<div id="location-popup" class="location-popup">
  <?php
  echo $view['actions']->render(
      $view['router']->generate('esi_location_popup', array()),
      array('strategy' => 'esi')
  );
  ?>
</div><!-- #location-popup -->
<form id="location-form-helper" action="<?php echo $view['router']->generate('mobile_set_cinema'); ?>" method="post">
  <input type="hidden" name="cinema" value="" id="location-helper-id" />
</form>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('js_after'); ?>
<script>
jQuery( document ).ready( function( $ ) {
  ask_location(true);
} );

var $btn_row = $('<div id="btn-row" class="hidden"><a href="#" class="btn btn-default btn-icon icon icon-rarr btn-icon-right">Seleccionar</a></div>');
$('.state-area-selection').after($btn_row);
$btn_row.delegate( 'a', 'click', function( e ) {
  e.preventDefault();
  $('#location-form-helper').trigger('submit');
} );
$('#location-form-helper').bind('submit', function() {
  if ( !$('#location-helper-id').val() )
    return false;
  
  $('body').blockui();
});
$( '[name=cinema]' ).bind( 'change', function( e ) {
  e.preventDefault();
  $('#location-helper-id').val($(this).val());
  $('#btn-row').removeClass('hidden')[$(this).val() ? 'show' : 'hide']();
} );
</script>
<?php $view['slots']->stop(); ?>
