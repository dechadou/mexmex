<?php

namespace SocialSnack\AdminBundle\Controller;

use SocialSnack\AdminBundle\Form\Type\IeBenefitType;
use SocialSnack\FrontBundle\Entity\IeBenefit;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SocialSnack\AdminBundle\Form\Type\PromoIEType;
use SocialSnack\FrontBundle\Entity\PromoIE;

class PromoIEController extends BaseController {
  
  protected function get_required_role() {
    return 'ROLE_CONTENT';
  }
  
  
  /**
   * @Route("/promosie", name="social_snack_promoie_list", defaults={"active"=true})
   * @Route("/promosie_inactive", name="social_snack_promoie_list_inactive", defaults={"active"=false})
   * @Template(engine="php")
   */
  public function listAction($active) {
    $this->check_roles();

    $repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:PromoIE');
    $promos = $repo->findActive($active);
    
    return array(
        'promos' => $promos,
        'active' => $active,
    );
  }
  
  
  /**
   * @Route("/promosie/add", name="social_snack_promoie_add", defaults={"promo_id": null})
   * @Route("/promosie/{promo_id}", name="social_snack_promoie_edit")
   * @Template(engine="php")
   */
  public function editAction(Request $request, $promo_id = NULL) {
    $this->check_roles();

    $session = $request->getSession();
    $repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:PromoIE');
    $purge = NULL;
    
    if (is_null($promo_id)) {
      $promo = new PromoIE();
    } else {
      $promo = $repo->find($promo_id);
    }
    
    $form = $this->createForm(new PromoIEType(), $promo);
    $form->handleRequest($request);
    
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($promo);
      $em->flush();
      
      $base_path = 'promosie' . DIRECTORY_SEPARATOR;

      if ($file = $form['file_thumb']->getData()) {
        $filename = $promo->getId() . "-thumb.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path, 300, 393);
        $promo->setThumb($base_path . $filename);
      }
      
      if ($file = $form['file_image']->getData()) {
        $filename = $promo->getId() . "-image.jpg";
        $this->container->get('ss.front.utils')->upload_cms_content($filename, file_get_contents($file->getPathName()), $base_path);
        $promo->setImage($base_path . $filename);
      }

      $em->flush();
      
      $purger = $this->get('ss.cache_purger');
      $purge = $purger->purge_cache(array('entity' => $promo));
      
      $session->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados.');
      $session->getFlashBag()->set('purge', $purge);
      return $this->redirect($this->generateUrl('social_snack_promoie_edit', array('promo_id' => $promo->getId())));
    }
    
    return array(
        'promo' => $promo,
        'form'  => $form->createView(),
        'purge' => $session->getFlashBag()->get('purge'),
        'submit_msg' => $session->getFlashBag()->get('submit_msg'),
    );
  }

  /**
   * @Route("/promosie/{promo_id}/delete", name="social_snack_promoie_delete")
   * @ParamConverter("promo", options={"id" = "promo_id"})
   * @Template(engine="php")
   */
  public function deleteAction(PromoIE $promo, Request $request) {
    $this->check_roles();

    $em = $this->getDoctrine()->getManager();
    $em->remove($promo);
    $em->flush();

    return $this->redirect($this->generateUrl('social_snack_promoie_list'));
  }


  /**
   * @Route("/ieBenefits", name="social_snack_promoie_benefits_list", defaults={"active"=true})
   * @Route("/ieBenefits/inactive", name="social_snack_promoie_benefits_list_inactive", defaults={"active"=false})
   */
  public function benefitsListAction($active) {
    $this->check_roles();

    $repo = $this->getDoctrine()->getRepository('SocialSnackFrontBundle:IeBenefit');

    return $this->render('SocialSnackAdminBundle:PromoIE:benefitsList.html.php', [
        'benefits' => $repo->findActiveNoCache($active),
        'active'   => $active,
    ]);
  }


  /**
   * @Route("/ieBenefits/add", name="social_snack_promoie_benefits_add", defaults={"benefit"=null})
   * @Route("/ieBenefits/{id}", name="social_snack_promoie_benefits_edit")
   */
  public function benefitsEditAction(IeBenefit $benefit = NULL, Request $request) {
    $this->check_roles();

    if (!$benefit) {
      $benefit = new IeBenefit();
    }

    $form = $this->createForm(new IeBenefitType(), $benefit);
    $form->add('submit', 'submit', ['label' => 'Guardar']);
    $form->handleRequest($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($benefit);

      $base_path = 'iebenefits' . DIRECTORY_SEPARATOR;
      if ($file = $form['imageFile']->getData()) {
        $filename = md5(microtime() . uniqid()) . '.jpg';
        $this->container->get('ss.front.utils')->upload_cms_content(
            $filename,
            file_get_contents($file->getPathName()),
            $base_path,
            120,
            120
        );
        $benefit->setImage($base_path . $filename);
      }

      $em->flush();

      $purger = $this->get('ss.cache_purger');
      $purge  = $purger->purge_cache([
          'entity' => $benefit,
      ]);

      $request->getSession()->getFlashBag()->set('submit_msg', 'Los cambios fueron guardados exitosamente.');
      $request->getSession()->getFlashBag()->set('purge', $purge);

      return $this->redirect($this->generateUrl('social_snack_promoie_benefits_edit', [
          'id' => $benefit->getId(),
      ]));
    }

    return $this->render('SocialSnackAdminBundle:PromoIE:benefitsEdit.html.php', [
        'benefit'    => $benefit,
        'form'       => $form->createView(),
        'submit_msg' => $request->getSession()->getFlashBag()->get('submit_msg'),
        'purge'      => $request->getSession()->getFlashBag()->get('purge'),
    ]);
  }


  /**
   * @Route("/ieBenefits/{id}/delete", name="social_snack_promoie_benefits_delete")
   *
   * @param IeBenefit $benefit
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function benefitsDeleteAction(IeBenefit $benefit) {
    $this->check_roles();

    $em = $this->getDoctrine()->getManager();
    $em->remove($benefit);
    $em->flush();

    return $this->redirect($this->generateUrl('social_snack_promoie_benefits_list'));
  }

}