<?php

namespace SocialSnack\RestBundle\Handler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
use SocialSnack\FrontBundle\Service\Utils;
use SocialSnack\WsBundle\Entity\MovieComing;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

class MovieComingHandler {

  /** @var Registry */
  protected $doctrine;

  protected $front_utils;

  protected $router;

  static $default_limit = 60;

  static $max_limit = 50;


  public function __construct(Registry $doctrine, Utils $front_utils, Router $router) {
    $this->doctrine = $doctrine;
    $this->front_utils = $front_utils;
    $this->router = $router;
  }

  public function getAll($offset = NULL, $limit = NULL) {
    $repo = $this->doctrine->getRepository('SocialSnackWsBundle:MovieComing');

    // Query movies and format some fields.
    $_movies = $repo->findAllFuture($offset, $limit);
    $movies = array();
    foreach ($_movies as $movie) {
      $movies[] = $this->serializeOne($movie);
    }

    return $movies;
  }


  public function getAllPagination(Request $request, $page, $limit) {
    $repo = $this->doctrine->getRepository('SocialSnackWsBundle:MovieComing');

    $total = $repo->countAllFuture();
    $pages = ceil($total/$limit);
    $pagination = array();

    // Build pagination links.
    $url = $this->router->generate($request->attributes->get('_route'), array(), TRUE);
    $query = $request->query;
    $query->remove('next');
    $query->remove('prev');
    if ($page > 1) {
      $query_args = $query->all();
      $query_args['page'] = $page - 1;
      $pagination['prev'] = $url . '?' . http_build_query($query_args);;
    }
    if ($page < $pages) {
      $query_args = $query->all();
      $query_args['page'] = $page + 1;
      $pagination['next'] = $url . '?' . http_build_query($query_args);;
    }

    return $pagination;
  }


  public function getOffsetLimit($page, $limit) {
    if (!$limit || $limit > self::$max_limit) {
      $limit = self::$default_limit;
    }
    if (!$page || $page < 1) {
      $page = 1;
    }
    $offset = ($page - 1) * $limit;

    return array($page, $offset, $limit);
  }


  public function serializeOne(MovieComing $movie) {
    $_movie                  = $movie->toArray();
    $_movie['poster_small']  = $this->front_utils->get_poster_url($_movie['cover'], 'small');
    $_movie['poster_medium'] = $this->front_utils->get_poster_url($_movie['cover'], 'medium');
    $_movie['poster_big']    = $this->front_utils->get_poster_url($_movie['cover'], 'big');
    $_movie['cover']         = $this->front_utils->get_poster_url($_movie['cover'], '200x295');

    if ($movie->getAttr()) {
      $_movie['label'] = FrontHelper::get_movie_attr_str($movie->getAttr());
    } else {
      $_movie['label'] = '';
    }

    return $_movie;
  }
}
