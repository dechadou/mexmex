<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Condiciónes y preguntas frecuentes - Cinemex' );
$view['slots']->start('body');
?>
<div class="content-box">
  <h1 class="content-box-title">Tarjeta Cinefan</h1>

  <div class="entry-body align-justify body-cinefan">
      <h2>Preguntas Frecuentes</h2>

      <h3>I. SECCION: CONDICIONES GENERALES.</h3>

      <p>La tarjeta “Cinefan” se puede comprar directamente en taquilla en cualquier Cinemex de la República Mexicana por un precio de $50.00 (cincuenta pesos 00/100 M.N.), excepto en las taquillas Platino Cinemex del Distrito Federal, Área Metropolitana y Cuernavaca.</p>
      <p>Con la tarjeta “Cinefan” podrás:</p>

      <ul>
        <li>Comprar en la taquilla de Cinemex, 20 boletos a precio especial  http://cinemex.com para cualquier película y función en salas tradicionales, 3D, Premium, Platino, Cinemextremo y Cinemex X4D Motion de lunes a domingo.</li>
        <li>Comprar 20 combos Cinefan por un costo extra de $145.00 (ciento cuarenta y cinco pesos 00/100 M.N.) que incluye: 1 palomitas saladas grandes (267 gr), 2 refrescos grandes (946 ml c/u), 1 Kit Kat (45 gr) y 1 Crunch Chocolate (40 gr) en caso de que se haya agotado el KIT KAT (45g) o Crunch Chocolate (40 gr) se dará otro dulce del mismo precio o mayor.</li>
      </ul>

      <p>La compra de los boletos no aplica en Preventas, eventos especiales, otras promociones, salas Palco Cinemex, salas Platino de las Ciudad de México, Área Metropolitana y Cuernavaca. El precio de los boletos varía según el cine en el que se utilice la tarjeta y se encuentran sujetos a cambio sin previo aviso.</p>
      <p>Al vencer tu tarjeta Cinefan o agotarse tus boletos a precio preferencial, canjéala por una nueva tarjeta y obtén $10.00 (diez pesos 00/100 M.N.) de descuento sobre el precio de la nueva tarjeta a adquirir (descuento no acumulable ni canjeable por efectivo o productos).</p>

      <h3>II. SECCION: PREGUNTAS FRECUENTES.</h3>

      <h4>A. COMO PUEDO OBTENER LA TARJETA CINEFAN</h4>

      <dl>
        <dt>¿Dónde puedo adquirir la tarjeta “Cinefan”?</dt>
          <dd>
            <p>La Tarjeta “Cinefan” solo se puede obtener directamente en la taquilla de cualquier Cinemex de la República Mexicana.</p>
            <p>* Excepto en las taquillas Platino ubicadas en la Ciudad de México, Área Metropolitana y Cuernavaca.</p>
          </dd>

        <dt>¿Cuál es el precio de la tarjeta “Cinefan”?</dt>
          <dd>La tarjeta “Cinefan” tiene un costo de $50.00 (cincuenta pesos 00/100 M.N.) (Costo que podrá variar sin previo aviso).</dd>

        <dt>¿Cuántos boletos a precio especial puedo comprar con la tarjeta “Cinefan”?</dt>
          <dd>
            <p>Con la tarjeta “Cinefan” solo podrás comprar 20 boletos a un precio especial para cualquiera de las siguientes salas tradicional, sala 3D, sala Premium, Platino (excepto salas Platino ubicadas en la Ciudad de México, Área Metropolitana y Cuernavaca), Cinemextremo y Cinemex X4D Motion, como tu elijas. No aplica en salas Palco Cinemex.</p>
            <p>Por cada boleto que compres se te entregará un cupón que será válido para comprar un combo Cinefan por $145.00 (ciento cuarenta y cinco pesos 00/100 M.N.) Adicionales.</p>
          </dd>

        <dt>¿Puedo comprar la tarjeta “Cinefan” con mis puntos de Invitado Especial?</dt>
          <dd>No, no se puede adquirir la tarjeta “Cinefan” con tus puntos de Invitado Especial de ningún nivel (Básico, Oro y Premium).</dd>

        <dt>Si compro un certificado de regalo, ¿puedo comprar  una tarjeta “Cinefan”?</dt>
          <dd>Si, con los certificados de regalo podrás comprar la tarjeta “Cinefan”</dd>

        <dt>¿Cuántas tarjetas “Cinefan” podrían llegar a tener?</dt>
          <dd>No hay límite de compra de tarjetas “Cinefan” así que puedes tener las que quieras.</dd>

        <dt>En caso de que llegará a perder mi tarjeta “Cinefan”, ¿me podrían dar una reposición?</dt>
          <dd>No, desgraciadamente si pierdes tu tarjeta “Cinefan” no podemos reponértela, por lo que te recomendamos que la guardes en un lugar seguro para evitar que se pierdan tus beneficios.</dd>

        <dt>¿Mi tarjeta “Cinefan” es transferible? Es decir la puedo regalar</dt>
          <dd>La tarjeta “Cinefan” la puedes utilizar tú o alguien más, es transferible, no necesitas ninguna clave para usarla. Por lo que si la llegas a perder, cualquier persona que la encuentre puede utilizarla, por lo tanto te recomendamos cuidarla lo mejor posible.</dd>
      </dl>

      <h4>B. COMO USAR MIS BOLETOS Y MIS COMBOS DE LA TARJETA CINEFAN</h4>

      <dl>
        <dt>¿Los boletos los puedo utilizar en cualquier cine de la república?</dt>
          <dd>Los boletos los puedes utilizar en cualquier cine Cinemex de la república Mexicana excepto en las salas Platino Cinemex del Distrito Federal, Área Metropolitana y Cuernavaca. No aplica en salas Palco Cinemex.</dd>

        <dt>Cuál es el precio que tendrá cada boleto de cine</dt>
          <dd>Para conocer el precio especial que tendrá el boleto ingresa a Cinemex.com.</dd>

        <dt>¿En qué sala son válidos los boletos de la tarjeta Cinefan?</dt>
          <dd>Los boletos son válidos en sala Tradicional, 3D, Premium, Platino, Cinemextremo y Cinemex X4D Motion de cualquier Cinemex de la república Mexicana excepto en las salas Platino Cinemex del Distrito Federal, Área Metropolitana y Cuernavaca. No aplica en salas Palco Cinemex.</dd>

        <dt>¿Qué necesito hacer para hacer uso de mis boletos?</dt>
          <dd>Únicamente presenta en taquilla tu tarjeta Cinefan para que compres tus boletos a precio preferencial.</dd>

        <dt>¿Cada que utilice mi tarjeta “Cinefan” me van a ir descontando los boletos que puedo comprar a precio preferencial?</dt>
          <dd>Así es, cada que compres un boleto utilizando la tarjeta “Cinefan”, se te irán descontando el número de boletos que puedes comprar a precio preferencia hasta agotar los 20 boletos.</dd>

        <dt>¿Puedo hacer válido los 20 boletos a precio especial al mismo tiempo?</dt>
          <dd>Sí, puedes comprar los 20 boletos a precio especial al mismo tiempo en una sola visita.</dd>

        <dt>¿Sobre qué precio de boleto se calcula el precio preferencial?</dt>
          <dd>Dado que la promoción no aplica con otras promociones, el precio preferencial será sobre el precio de taquilla de adulto del domingo del cine en el que quieras hacer uso de tu tarjeta Cinefan. Consulta en Cinemex.com la lista de precios de acuerdo al cine de tu preferencia.</dd>

        <dt>¿Además de los 20 boletos a precio preferencial que más incluye mi tarjeta Cinefan?</dt>
          <dd>Incluye 20 combos Cinefan, con un costo adicional de $145 (ciento cuarenta y cinco pesos 00/100 M.N) cada uno.</dd>

        <dt>¿Qué incluyen los combos Cinefan?</dt>
          <dd>El combo Cinefan incluye: 1 palomitas saladas grandes (267 gr), 2 refrescos grandes (946 ml c/u), 1 Kit Kat (45 gr) y 1 Crunch Chocolate (40 gr) en caso de que se haya agotado el KIT KAT y/o Crunch Chocolate (40 gr) (45g) se dará otro dulce del mismo precio o mayor.</dd>

        <dt>¿Puedo crecer mi combo Cinefan?</dt>
          <dd>No, no se podrá crecer tu combo Cinefan.</dd>

        <dt>¿Puedo hacer refill del combo Cinefan?</dt>
          <dd>No, no se puede hacer refill del refresco y tampoco de las palomitas.</dd>

        <dt>¿Cómo puedo hacer uso de mis combos? </dt>
          <dd>Cuando compres tus boletos con tu tarjeta Cinefan, en taquilla te entregarán un cupón el cual podrás canjear en la dulcería de Cinemex por tu combo Cinefan a precio especial.</dd>

        <dt>¿Puedo usar mi combo cuando yo quiera? </dt>
          <dd>No, El Combo Cinefan se deberá usar el mismo día de su emisión.</dd>

        <dt>¿Qué pasa con mis combos cuando canjeo más de un boleto de la tarjeta Cinefan?</dt>
          <dd>Se emitirá un cupón con el número y monto correspondiente al uso de tus boletos de la tarjeta Cinefan, es decir, si canjeas 2 boletos de tu tarjeta Cinefan se emitirá un cupón  valido por la compra de 2 combos Cinefan con un monto a pagar de $290 (dos ciento noventa pesos 00/100 M.N.).</dd>

        <dt>¿Tengo que comprar el total de combos Cinefan que se mencionan en el cupón?</dt>
          <dd>No, por ejemplo: Si tu cupón menciona 2 combos Cinefan podrás adquirir solo 1 combo Cinefan por $145 (ciento cuarenta y cinco pesos 00/100 M.N.) Sin embargo estarías perdiendo la opción de comprar el segundo  combo.</dd>

        <dt>¿Cómo puedo obtener el descuento de $10 pesos en mi tarjeta Cinefan?</dt>
          <dd>Al vencer tu tarjeta Cinefan o agotarse tus boletos presente tu tarjeta Cinefan en taquilla para comprar otra tarjeta Cinefan y obtén $10 Diez pesos 00/100 M.N.) de descuento en el costo de la nueva tarjeta que compres; el descuento no es acumulable ni canjeable por efectivo o producto.</dd>
      </dl>

      <h3>C. PRECIO ESPECIAL DE BOLETOS CINEFAN</h3>

      <p>Para conocer el precio especial de los boletos con tu tarjeta Cinefan ingresa a Cinemex.com.</p>

      <h2>Terminos y Condiciones</h2>

      <p>La tarjeta “Cinefan”, es un producto exclusivo de Cinemex, la cual puede adquirir en la taquilla de cualquier Cinemex  de la República Mexicana, con excepción de los Cinemex Platino ubicados en el Distrito Federal, Área Metropolitana y Cuernavaca, a un costo de $50 (cincuenta pesos 00/100 M.N.) por tarjeta.</p>
      <p>La tarjeta Cinefan estará en venta del 8 de enero al 31 de mayo de 2016.Con una <strong>vigencia en el mismo periodo es decir, del 8 de enero al 31 de mayo de 2016</strong> (ocho de enero al treinta y uno de mayo de dos mil diez y seis).</p>

      <p>Los invitados que adquieran la tarjeta Cinefan tendrán los siguientes beneficios:</p>
      <ul>
        <li>20 boletos a precio preferencial</li>
        <li>20 combos Cinefan por un costo extra de $145 (ciento cuarenta y cinco pesos 00/100 M.N.) cada combo.</li>
      </ul>

      <p>Estos beneficios solo podrán ser utilizados, en cualquier Cinemex del 8 de enero al 31 de mayo del 2016 con las siguientes restricciones:</p>

      <p>Boletos:</p>

      <ul>
        <li>Compra de 1 a 20 boletos a precio preferencial para cualquier película y función.</li>
        <li>El uso de la tarjeta es válida de lunes a domingo en cualquier sala Cinemex (tradicional, 3D, Premium, CinemeXtremo, Cinemex X4D Motion y Platino, (excepto salas Platino ubicadas en el Distrito Federal, Área Metropolitana y Cuernavaca) No aplica en Palco Cinemex.</li>
        <li>Canjeable en taquilla y sujeto a disponibilidad de la sala.</li>
        <li>Los boletos a precio preferencial no aplican en Preventa, otras promociones, eventos especiales.</li>
        <li>El descuento aplicado a los boletos se toman sobre el precio más alto del domingo del cine correspondiente y de acuerdo al formato de sala seleccionado.</li>
      </ul>

      <p>Combos Cinefan:</p>
      <ul>
        <li>Compra de hasta 20 Combos Cinefan por un precio de  $145 (ciento cuarenta y cinco pesos 00/100 M.N) cada uno.</li>
        <li>Cada combo incluye: 1 palomitas saladas grandes (267 gr), 2 refrescos grandes (946 ml c/u), 1 Kit Kat (45 gr) y 1 Crunch Chocolate (40 gr) en caso de que se haya agotado el KIT KAT (45g) o Crunch Chocolate (40 gr) se dará otro dulce del mismo precio o mayor.</li>
        <li>Para poder obtener el combo se deberá adquirir un boleto de la tarjeta Cinefan, en ese momento se emitirá un cupón válido por el combo de la promoción, para canjear en la dulcería del cine para el mismo día de la emisión del cupón.</li>
        <li>Por cada boleto que adquieras con la tarjeta Cinefan, se emitirán los cupones para los Combos Cinefan correspondientes  y estos serán válidos para el mismo día de su emisión, pagando la cantidad correspondiente por el número de cupones/combos Cinefan que se quieran adquirir.</li>
        <li>No aplica con otras promociones, ni otros combos u otros descuentos. No se podrá hacer refill del refresco ni las palomitas del combo Cinefan.</li>
      </ul>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="4">PRECIO</th></tr>
          <tr><th>CENTRO</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>

        <tbody>
          <tr><td>CENTRO C TELMEX</td><td>33</td><td>43</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CUAUHTEMOC</td><td>48</td><td>59</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CUAUHTEMOC PREMIUM</td><td>62</td><td>77</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GALERIAS</td><td>40</td><td>50</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GALERIAS PREMIUM</td><td>53</td><td>66</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>INSURGENTES PREMIUM</td><td>68</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PALACIO CHINO</td><td>35</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PARQUE DELTA</td><td>58</td><td>71</td><td>58</td><td>60</td></tr>
          <tr><td>PATRIOTISMO</td><td>53</td><td>66</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PATRIOTISMO PREMIUM</td><td>68</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>REAL</td><td>38</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>REFORMA 222</td><td>49</td><td>60</td><td>49</td><td>42</td></tr>
          <tr><td>REFORMA 222 PREMIUM</td><td>63</td><td>74</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>REFORMA PREMIUM</td><td>51</td><td>0</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN ANTONIO</td><td>47</td><td>57</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>WTC</td><td>53</td><td>66</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="4">PRECIO</th></tr>
          <tr><th>NOR-ORIENTE</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>AMERICAS II</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ARAGÓN</td><td>45</td><td>55</td><td>45</td><td>35</td></tr>
          <tr><td>CD AZTECA</td><td>28</td><td>35</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ECATEPEC</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MACROPLAZA TECAMAC</td><td>37</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MEXIQUENSE</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>OJO DE AGUA</td><td>38</td><td>46</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PALOMAS</td><td>27</td><td>34</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>NORTE</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>

        <tbody>
          <tr><td>CLAVERIA</td><td>40</td><td>48</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>COACALCO</td><td>27</td><td>32</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>IZCALLI</td><td>41</td><td>53</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA JOYA</td><td>24</td><td>28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA VIA</td><td>24</td><td>28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAS FLORES COACALCO</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LOMAS VERDES PREMIUM</td><td>54</td><td>68</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MISTERIOS</td><td>41</td><td>51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MUNDO E</td><td>55</td><td>68</td><td>54</td><td>44</td><td>77</td><td>95</td></tr>
          <tr><td>MUNDO E PREMIUM</td><td>70</td><td>87</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PARQUE LINDAVISTA</td><td>49</td><td>60</td><td>49</td><td>43</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PERINORTE</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA CUAUTITLAN</td><td>18</td><td>22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA JARDINES TULTITLÁN</td><td>24</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA TLALNE</td><td>55</td><td>68</td><td>63</td><td>78</td><td>77</td><td>95</td></tr>
          <tr><td>PUERTA TEXCOCO</td><td>28</td><td>36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>EL ROSARIO</td><td>32</td><td>39</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN MATEO</td><td>41</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN MATEO PREMIUM</td><td>52</td><td>65</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TENAYUCA</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TEXCOCO</td><td>33</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TICOMAN</td><td>36</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TICOMAN PREMIUM</td><td>46</td><td>57</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TULTITLAN</td><td>24</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>VALLE DORADO</td><td>44</td><td>54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>VALLE DORADO PREMIUM</td><td>56</td><td>70</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ZONA ESMERALDA</td><td>49</td><td>63</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ZUMPANGO</td><td>25</td><td>39</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>ORIENTE</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CHALCO</td><td>28</td><td>33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CHIMALHUACAN</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CORTIJO</td><td>21</td><td>27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>IXTAPALUCA</td><td>21</td><td>27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>IZTAPALAPA</td><td>32</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA VIGA</td><td>35</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LOS REYES</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PASEO CHALCO</td><td>29</td><td>35</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA ORIENTE</td><td>38</td><td>47</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TEZONTLE</td><td>44</td><td>52</td><td>43</td><td>33</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TLAHUAC</td><td>42</td><td>52</td><td>42</td><td>47</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ZARAGOZA</td><td>26</td><td>33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>PONIENTE</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>ANTARA</td><td>58</td><td>74</td><td>56</td><td>58</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CERVANTES SAAVEDRA PREMIUM</td><td>67</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PABELLON POLANCO</td><td>54</td><td>69</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>BOSQUES PREMIUM</td><td>67</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAS GRANJAS</td><td>21</td><td>30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>INTERLOMAS PREMIUM</td><td>60</td><td>70</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LEGARIA</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN ESTEBAN</td><td>35</td><td>43</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SANTA FE</td><td>58</td><td>72</td><td>63</td><td>52</td><td>77</td><td>95</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>DF Y AREA METROPOLITANA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>SUR</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>ALTAVISTA</td><td>55</td><td>75</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CNA</td><td>38</td><td>47</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CNA PREMIUM</td><td>52</td><td>64</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>COAPA</td><td>52</td><td>64</td><td>51</td><td>42</td><td>77</td><td>95</td></tr>
          <tr><td>CUICUILCO</td><td>58</td><td>74</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GRAN SUR</td><td>52</td><td>65</td><td>52</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LORETO</td><td>56</td><td>73</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LORETO PREMIUM</td><td>70</td><td>87</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PERICOAPA</td><td>45</td><td>58</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PRADO COAPA</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TLALPAN</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>UNIVERSIDAD</td><td>52</td><td>67</td><td>52</td><td>42</td><td>77</td><td>95</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>MONTERREY Y AREA METROPOLITANA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>APODACA</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ESCOBEDO</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAS PLAZAS OUTLET</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN NICOLAS</td><td>36</td><td>43</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN NICOLAS PREMIUM</td><td>45</td><td>55</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CONTRY</td><td>28</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LINDAVISTA</td><td>44</td><td>54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LINDAVISTA PREMIUM</td><td>56</td><td>70</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SUN MALL GUADALUPE</td><td>30</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SUN MALL JUÁREZ</td><td>45</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CENTRIKA</td><td>44</td><td>54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CUMBRES</td><td>33</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA SILLA</td><td>35</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LEONES</td><td>41</td><td>51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LINCOLN</td><td>30</td><td>36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PASEO TEC PREMIUM</td><td>39</td><td>51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA REAL MONTERREY</td><td>37</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PUEBLO SERENA PREMIUM</td><td>53</td><td>68</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PUEBLO SERENA PLATINO</td><td>84</td><td>110</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>REVOLUCION</td><td>35</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>FIESTA ANAHUAC</td><td>47</td><td>59</td><td>47</td><td>40</td><td>77</td><td>95</td></tr>
          <tr><td>LAS AMERICAS</td><td>40</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SANTO DOMINGO</td><td>25</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>HUMBERTO LOBO</td><td>54</td><td>67</td><td>54</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>HUMBERTO LOBO PREMIUM</td><td>68</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PASEO SAN PEDRO PLATINO</td><td>102</td><td>126</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN AGUSTIN</td><td>54</td><td>67</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN AGUSTIN PREMIUM</td><td>68</td><td>84</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SANTA CATARINA</td><td>45</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>AGUASCALIENTES</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>AGUASCALIENTES</td><td>45</td><td>54</td></tr>
          <tr><td>EXPOPLAZA AGUASCALIENTES</td><td>37</td><td>45</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>BAJA CALIFORNIA</th><th colspan="2">PRECIO</th></tr>
          <tr><th>MEXICALI</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>MEXICALI</td><td>39</td><td>47</td></tr>
          <tr><td>MUNDO DIVERTIDO MEXICALI</td><td>37</td><td>45</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>BAJA CALIFORNIA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>TIJUANA</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>LOMA BONITA</td><td>31</td><td>37</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MACROPLAZA TIJUANA</td><td>35</td><td>44</td><td>&nbsp;</td><td>29</td><td>70</td><td>88</td></tr>
          <tr><td>MUNDO DIVERTIDO TIJUANA</td><td>30</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>OASIS</td><td>27</td><td>33</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PAVILION ZONA RIO</td><td>34</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA MINARETE TIJUANA</td><td>34</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TIJUANA PACIFICO</td><td>32</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>BAJA CALIFORNIA SUR</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CABO SAN LUCAS</td><td>34</td><td>42</td></tr>
          <tr><td>CABO SAN LUCAS</td><td>67</td><td>87</td></tr>
          <tr><td>LA PAZ</td><td>28</td><td>35</td></tr>
          <tr><td>LA PAZ PLATINO</td><td>56</td><td>82</td></tr>
          <tr><td>CAMPECHE</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CD. DEL CARMEN PLATINO</td><td>60</td><td>77</td></tr>
          <tr><td>PALMIRA</td><td>42</td><td>55</td></tr>
          <tr><td>CHIAPAS</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TAPACHULA</td><td>32</td><td>49</td></tr>
          <tr><td>GALERIAS TUXTLA</td><td>43</td><td>54</td></tr>
          <tr><td>CHIHUAHUA</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GALERIAS TEC</td><td>45</td><td>56</td></tr>
          <tr><td>EL CAMINO</td><td>44</td><td>54</td></tr>
          <tr><td>SAN LORENZO</td><td>35</td><td>43</td></tr>
          <tr><td>SANDERS</td><td>31</td><td>38</td></tr>
          <tr><td>FUENTES MARES</td><td>30</td><td>37</td></tr>
          <tr><td>PLAZA HOLLYWOOD</td><td>44</td><td>56</td></tr>
          <tr><td>PLAZA HOLLYWOOD PLATINO</td><td>72</td><td>92</td></tr>
          <tr><td>JUVENTUD</td><td>31</td><td>38</td></tr>
          <tr><td>MIRADOR</td><td>35</td><td>42</td></tr>
          <tr><td>DELICIAS</td><td>31</td><td>38</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>COAHUILA</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>GALERIAS SALTILLO</td><td>46</td><td>57</td></tr>
          <tr><td>GALERIAS SALTILLO PLATINO</td><td>70</td><td>91</td></tr>
          <tr><td>INTERMALL</td><td>42</td><td>54</td></tr>
          <tr><td>INTERMALL PLATINO</td><td>68</td><td>0</td></tr>
          <tr><td>MIRASIERRA</td><td>32</td><td>40</td></tr>
          <tr><td>MONCLOVA</td><td>34</td><td>40</td></tr>
          <tr><td>SALTILLO</td><td>34</td><td>42</td></tr>
          <tr><td>TORREON</td><td>33</td><td>42</td></tr>
          <tr><td>COLIMA</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>COLIMA</td><td>38</td><td>46</td></tr>
          <tr><td>DURANGO</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>DURANGO</td><td>35</td><td>44</td></tr>
          <tr><td>DURANGO PLATINO</td><td>66</td><td>&nbsp;</td></tr>
          <tr><td>GOMEZ PALACIO</td><td>33</td><td>42</td></tr>
          <tr><td>HAMBURGO</td><td>33</td><td>42</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>ESTADO DE MEXICO</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>ATLACOMULCO</td><td>38</td><td>47</td></tr>
          <tr><td>LERMA</td><td>38</td><td>47</td></tr>
          <tr><td>LERMA PREMIUM</td><td>49</td><td>61</td></tr>
          <tr><td>LA PILITA</td><td>26</td><td>33</td></tr>
          <tr><td>METEPEC</td><td>45</td><td>58</td></tr>
          <tr><td>METEPEC PREMIUM</td><td>58</td><td>72</td></tr>
          <tr><td>SANTIN</td><td>29</td><td>43</td></tr>
          <tr><td>TOLUCA</td><td>35</td><td>44</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>GUANAJUATO</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>GUANAJUATO</td><td>42</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>IRAPUATO</td><td>33</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN ROQUE</td><td>25</td><td>30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MALECON</td><td>26</td><td>30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA MAYOR</td><td>52</td><td>65</td><td>52</td><td>38</td><td>77</td><td>95</td></tr>
          <tr><td>PLAZA MAYOR PLATINO</td><td>83</td><td>101</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA STADIUM</td><td>40</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN MIGUEL DE ALLENDE</td><td>28</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>VIA ALTA SALAMANCA</td><td>39</td><td>51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>VIA ALTA SALAMANCA PREMIUM</td><td>54</td><td>71</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>GUERRERO</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>LAS PALMAS ACAPULCO</td><td>31</td><td>38</td></tr>
          <tr><td>OCEANIC ACAPULCO</td><td>21</td><td>26</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>HIDALGO</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>GRAN PATIO PACHUCA</td><td>33</td><td>41</td></tr>
          <tr><td>PACHUCA</td><td>31</td><td>38</td></tr>
          <tr><td>PLAZA Q</td><td>33</td><td>40</td></tr>
          <tr><td>TULANCINGO</td><td>31</td><td>38</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>JALISCO</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CD GUZMAN</td><td>42</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CORDILLERAS PREMIUM</td><td>58</td><td>72</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GUADALAJARA</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA GOURMETERIA</td><td>49</td><td>63</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAS PLAZAS</td><td>38</td><td>46</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA MILENIUM</td><td>37</td><td>47</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAO PAULO</td><td>107</td><td>133</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TLAQUEPAQUE</td><td>28</td><td>35</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ZAPOPAN ACUEDUCTO</td><td>40</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td><td>77</td><td>95</td></tr>
          <tr><td>ZAPOPAN ACUEDUCTO PREMIUM</td><td>54</td><td>66</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>OCOTLAN</td><td>42</td><td>53</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MACROPLAZA VALLARTA</td><td>33</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA CARACOL VALLARTA</td><td>33</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GALERIAS PUERTO VALLARTA</td><td>45</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN GASPAR</td><td>24</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>MICHOACAN</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>LA PIEDAD</td><td>33</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAZARO CARDENAS</td><td>25</td><td>32</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA ESTADIO MORELIA</td><td>44</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA ESTADIO MORELIA (MACROPANTALLA)</td><td>47</td><td>60</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ALTOZANO</td><td>37</td><td>50</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>ALTOZANO PLATINO</td><td>82</td><td>102</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PASEO ZAMORA</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MORELOS</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CUAUTLA</td><td>38</td><td>47</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>DIANA</td><td>44</td><td>54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>DIANA PREMIUM</td><td>58</td><td>72</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>JACARANDAS</td><td>35</td><td>43</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA CUERNAVACA</td><td>56</td><td>70</td><td>56</td><td>52</td><td>77</td><td>95</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>NAYARIT</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>FORUM TEPIC</td><td>49</td><td>61</td></tr>
          <tr><td>FORUM TEPIC PLATINO</td><td>74</td><td>0</td></tr>
          <tr><td>TEPIC</td><td>35</td><td>43</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>OAXACA</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>MACROPLAZA OAXACA</td><td>40</td><td>49</td></tr>
          <tr><td>OAXACA</td><td>35</td><td>45</td></tr>
          <tr><td>PLAZA BELLA OAXACA</td><td>34</td><td>42</td></tr>
          <tr><td>SALINA CRUZ</td><td>40</td><td>49</td></tr>
          <tr><td>TUXTEPEC</td><td>35</td><td>42</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>PUEBLA</th><th colspan="4">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>DEL PARQUE</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>HERMANOS SERDAN</td><td>25</td><td>32</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MILENIUM</td><td>42</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA DORADA PUEBLA</td><td>37</td><td>46</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PUEBLA</td><td>45</td><td>56</td><td>45</td><td>38</td></tr>
          <tr><td>PUEBLA CENTRO</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SONATA PLATINO</td><td>98</td><td>115</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>TRIANGULO PUEBLA</td><td>52</td><td>68</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>QUERETARO</th><th colspan="4">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>BOULEVARES </td><td>46</td><td>59</td><td>45</td><td>38</td></tr>
          <tr><td>BOULEVARES PREMIUM </td><td>60</td><td>77</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CANDILES</td><td>44</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CENTRO SUR</td><td>41</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CONSTITUYENTES</td><td>40</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>SAN JUAN DEL RIO</td><td>38</td><td>46</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>QUINTANA ROO</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CANCÚN</td><td>43</td><td>53</td><td>48</td><td>59</td></tr>
          <tr><td>CUMBRES LA ROCA PREMIUM</td><td>49</td><td>67</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LA ISLA CANCÚN</td><td>51</td><td>63</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LOS HÉROES CANCÚN</td><td>32</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MULTIPLAZA CANCÚN</td><td>32</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CHETUMAL</td><td>31</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAYA DEL CARMEN</td><td>35</td><td>42</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>SAN LUIS POTOSI</th><th>PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CD VALLES</td><td>31</td><td>38</td></tr>
          <tr><td>CITADELLA</td><td>40</td><td>50</td></tr>
          <tr><td>CITADELLA PLATINO</td><td>70</td><td>90</td></tr>
          <tr><td>EL PASEO </td><td>31</td><td>37</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>SINALOA</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>FORUM CULIACAN</td><td>47</td><td>59</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>FORUM CULIACAN PLATINO</td><td>68</td><td>0</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PLAZA CULIACAN</td><td>37</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GUAMUCHIL</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GUASAVE</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LOS MOCHIS</td><td>42</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>PASEO LOS MOCHIS</td><td>49</td><td>61</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>EL TOREO</td><td>24</td><td>29</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GALERIAS MAZATLAN</td><td>30</td><td>38</td><td>&nbsp;</td><td>&nbsp;</td><td>70</td><td>88</td></tr>
          <tr><td>GALERIAS MAZATLAN PLATINO</td><td>59</td><td>76</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MAZATLAN</td><td>24</td><td>29</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>SONORA</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>PLAZA GOYA</td><td>37</td><td>45</td></tr>
          <tr><td>GUAYMAS</td><td>35</td><td>45</td></tr>
          <tr><td>CANANEA</td><td>36</td><td>43</td></tr>
          <tr><td>ENCINAS</td><td>30</td><td>38</td></tr>
          <tr><td>METRO CENTRO HERMOSILLO PREMIUM</td><td>42</td><td>55</td></tr>
          <tr><td>MIRADOR HERMOSILLO</td><td>30</td><td>38</td></tr>
          <tr><td>NACOZARI</td><td>33</td><td>40</td></tr>
          <tr><td>NAVOJOA</td><td>35</td><td>45</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>TABASCO</th><th colspan="6">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th><th>Cinefan X4D - 2D</th><th>Cinefan X4D - 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>ALTABRISA</td><td>39</td><td>49</td><td>&nbsp;</td><td>34</td><td>77</td><td>95</td></tr>
          <tr><td>ALTABRISA PLATINO</td><td>77</td><td>91</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CARDENAS</td><td>29</td><td>36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>VILLAHERMOSA</td><td>28</td><td>34</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>TAMAULIPAS</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CD MANTE</td><td>33</td><td>40</td></tr>
          <tr><td>CD. VICTORIA</td><td>33</td><td>42</td></tr>
          <tr><td>MATAMOROS</td><td>31</td><td>37</td></tr>
          <tr><td>DOS LAREDOS</td><td>33</td><td>40</td></tr>
          <tr><td>PLAZA REAL NUEVO LAREDO</td><td>31</td><td>39</td></tr>
          <tr><td>PLAZA REAL REYNOSA</td><td>44</td><td>54</td></tr>
          <tr><td>PLAZA REAL REYNOSA PREMIUM</td><td>58</td><td>0</td></tr>
          <tr><td>ZONA DORADA</td><td>28</td><td>36</td></tr>
          <tr><td>PLAZA BELLA RIO BRAVO</td><td>24</td><td>31</td></tr>
          <tr><td>TAMPICO</td><td>37</td><td>45</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>VERACRUZ</th><th colspan="4">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th><th>Cinefan CX 2D</th><th>Cinefan CX 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>ANDAMAR</td><td>51</td><td>60</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>BOCA DEL RIO</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>EL PALMAR</td><td>28</td><td>35</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>FORUM COATZACOALCOS</td><td>41</td><td>51</td><td>41</td><td>32</td></tr>
          <tr><td>FORUM COATZACOALCOS PLATINO</td><td>79</td><td>0</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>CORDOBA</td><td>28</td><td>34</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>MINATITLAN</td><td>33</td><td>40</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>GRAN PATIO POZA RICA</td><td>47</td><td>59</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>POZA RICA</td><td>37</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LAS PALMAS VERACRUZ</td><td>35</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>LOS PINOS</td><td>29</td><td>36</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>NUEVO VERACRUZ </td><td>45</td><td>55</td><td>&nbsp;</td><td>&nbsp;</td></tr>
          <tr><td>NUEVO VERACRUZ PLATINO</td><td>65</td><td>87</td><td>&nbsp;</td><td></td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>YUCATAN</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>CANEK</td><td>45</td><td>54</td></tr>
          <tr><td>CITY CENTER MÉRIDA PREMIUM</td><td>49</td><td>67</td></tr>
          <tr><td>GALERIAS MERIDA</td><td>48</td><td>59</td></tr>
          <tr><td>GALERIAS MERIDA PLATINO</td><td>72</td><td>0</td></tr>
          <tr><td>GRAN PLAZA MERIDA</td><td>44</td><td>54</td></tr>
          <tr><td>MACROPLAZA MERIDA</td><td>37</td><td>45</td></tr>
          <tr><td>PENINSULA MONTEJO</td><td>72</td><td>94</td></tr>
          <tr><td>REX</td><td>25</td><td>32</td></tr>
          <tr><td>VALLADOLID</td><td>35</td><td>44</td></tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr><th>ZACATECAS</th><th colspan="2">PRECIO</th></tr>
          <tr><th>&nbsp;</th><th>Cinefan 2D</th><th>Cinefan 3D</th></tr>
        </thead>
        <tbody>
          <tr><td>FRESNILLO</td><td>27</td><td>33</td></tr>
          <tr><td>ZACATECAS</td><td>31</td><td>38</td></tr>
          <tr><td>ZACATECAS PREMIUM</td><td>41</td><td>50</td></tr>
        </tbody>
      </table>
  </div>
</div>
<?php $view['slots']->stop(); ?>
