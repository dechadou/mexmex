<?php

namespace SocialSnack\WsBundle\Request;

class TerminaCompra extends Compra {

	protected $method = 'TerminaCompra';

	protected $default_args = array(
			'ClaveExterna'   => 0,
			'CodigoCine'     => 0,
			'Session'        => 0,
			'CadenaBoletos'  => '',
      'TDC'            => 0,
			'Vigencia'       => 0,
			'DV'             => 0,
			'CadenaPlatino'  => '',
			'IE'             => '',
			'NombreCliente'  => 0,
			'TranTemp'       => '',
	);
	
}
