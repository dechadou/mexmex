<?php

namespace SocialSnack\RestBundle\Handler;

use SocialSnack\WsBundle\Exception\UnexpectedWsResponseException;
use SocialSnack\WsBundle\Request\Request as WsRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IEHandler
 * @package SocialSnack\RestBundle\Handler
 * @auhor Guido Kritz
 */
class IEHandler {

  protected $container;

  protected $ws;

  public function __construct(ContainerInterface $container, WsRequest $ws) {
    $this->container = $container;
    $this->ws = $ws;
  }


  public function verifyIECode($iecode) {
    $this->getIEInfo($iecode);
    return TRUE;
  }


  public function getIEInfo($iecode) {
    $iecode = str_replace(' ', '', $iecode);

    $this->ws->init('ConsultaIEC');
    return $this->ws->request(array(
        'TipoConsulta' => \SocialSnack\WsBundle\Request\ConsultaIEC::TIPOCONSULTA_INFO,
        'ClaveIEC'     => $iecode,
    ));
  }


  public function getIETransactions($iecode) {
    $this->ws->init('ConsultaIEC');
    return $this->ws->request(array(
        'TipoConsulta' => \SocialSnack\WsBundle\Request\ConsultaIEC::TIPOCONSULTA_TRANSACTIONS,
        'ClaveIEC'     => $iecode,
    ));
  }

} 