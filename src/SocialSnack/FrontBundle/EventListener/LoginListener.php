<?php
namespace SocialSnack\FrontBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use SocialSnack\FrontBundle\Entity\User as User;

class LoginListener
{
    /**
     * Catches the login of a user and does something with it
     *
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $event
     * @return void
     */
    public function onSecurityInteractiveLogin( InteractiveLoginEvent $event )
    {
        $token = $event->getAuthenticationToken();
        if ( $token && $token->getUser() instanceof User && $event->getRequest()->isXmlHttpRequest() ) {
					
            $result = array('success' => false);
            return new \Symfony\Component\HttpFoundation\Response(json_encode($result));
        }
    }
}