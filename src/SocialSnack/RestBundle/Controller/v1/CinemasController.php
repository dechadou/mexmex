<?php

namespace SocialSnack\RestBundle\Controller\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class CinemasController extends BaseController {


  /**
   * $_GET['q'] Optional.
   *
   * @Route("/", name="rest_v1_cinemas_default")
   * @Route("/area/{area_id}")
   * @Method({"GET"})
   */
  public function defaultAction($area_id = NULL) {
    /** @var \SocialSnack\RestBundle\Handler\ContextHandler $handler */
    $context_handler = $this->get('rest.context.handler');

    list($area_id, $state_id) = $context_handler->normalizeAreaIds($area_id);

    return $this->forward('SocialSnackRestBundle:Cinemas:default', ['area_id' => $area_id, 'state_id' => $state_id]);
  }


  /**
   * @Route("/{id}", name="rest_v1_cinemas_single")
   * @Method({"GET"})
   */
  public function singleAction($id) {
    return $this->forward('SocialSnackRestBundle:Cinemas:single', ['id' => $id]);
  }


  /**
   * $_GET['date'] Optional.
   *
   * @Route("/{cinema_id}/movies", name="rest_v1_cinemas_movies")
   * @Method({"GET"})
   * @param integer $cinema_id
   * @param Request $request
   * @return JsonResponse
   */
  public function moviesAction($cinema_id, Request $request) {
    return $this->forward('SocialSnackRestBundle:Cinemas:movies', ['cinema_id' => $cinema_id, 'request' => $request]);
  }


}