<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use Buzz\Message\Request;

class SymfonyHttpCachePurger extends CachePurgerUrlAbstract {

  protected function purge($path, $host, $server) {
    $req = new Request('PURGEX', $path, $server);
    $req->addHeader(sprintf('Host: %s', $host));
    $req->addHeader(sprintf('X-Host: %s', $host));

    try {
      $res = $this->browser->send($req);
      $output = $res->getContent();
    } catch (\Exception $e) {
      $output = $e->getMessage();
    }

    return sprintf('%s: Symfony HTTP Cache %s: %s', $server, $path, $output);
  }

}