<div id="main-slider">
  <div class="slider-slider">
    <?php foreach ( $promosa as $promo ) { ?>
    <div class="slide">
      <a href="<?php echo $promo->getUrl(); ?>" target="<?php echo $promo->getLinkTarget(); ?>" data-collapsed="<?php echo $view['fronthelper']->get_cms_url($promo->getImage(), '1020x129'); ?>" class="promo_events" data-promo-type="A" data-promo-ref="<?php echo $promo->getOrder(); ?>" alt="<?php echo $promo->getTitle(); ?>">
        <img src="<?php echo $view['fronthelper']->get_cms_url($promo->getImage(), '1020x129'); ?>" alt="<?php echo $promo->getTitle(); ?>" />
      </a>
    </div>
    <?php } ?>
  </div>
  <div class="slider-dots">
  </div>
</div><!-- #main-slider -->
