<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Viajes de Esperanza - Cinemex');
$view['slots']->set('body_class', 'sponsored-viajes-de-esperanza');
?>

<?php $view['slots']->start('body'); ?>

<div class="section-header">
  <img src="<?php echo $view['assets']->getUrl('assets/img/landings/viajes/header.jpg'); ?>">
</div>

<div class="section-nav">
  <ul>
    <li><a href="<?php echo $view['router']->generate('viajes_index'); ?>">Viajes de Esperanza</a></li>
    <li><a href="<?php echo $view['router']->generate('viajes_terms'); ?>">Terminos y condiciones</a></li>
  </ul>
</div>

<div class="section-content">
  <div class="content-box">
    <h1 class="section-title">Términos y condiciones</h1>

    <p>Para participar deberás ser seguidores de Cinemex en Twitter (twitter.com/Cinemex), y estar pendiente de las preguntas que se publicaran en Twitter de Cinemex, a partir del 7 de noviembre del 2014 y hasta el 20 de noviembre del 2014. Una última pregunta será publicada el viernes 21 de noviembre a las 9am, en cuanto sea publicada deberás de mandar tus respuestas al correo promociones@cinemex.net junto con tu boleto de la película en Cinemex del 6 al 9 de noviembre de 2014, nombre completo y número de celular. Si eres el primer participante en mandar las respuestas correctas y datos, podrás ganar un viaje doble a la Rivera Maya.</p>
    <p>El ganador se dará a conocer el martes 25 de noviembre del 2014 en Twitter de Cinemex. Es importante que envíes una dirección válida de correo electrónico y un número de celular, ya que a través de estos te contactaremos. En caso de ser ganador es indispensable contar con el boleto de la película “Viajes de Esperanza” en Cinemex, con fecha del 6 al 9 de noviembre de 2014.</p>
    <p>Podrán participar en esta promoción todas las personas físicas mayores de 18 (dieciocho) años de sexo masculino o femenino que cumplan cabalmente con las condiciones establecidas en estos términos y condiciones. La participación en esta promoción es única, en caso de que se detecten dos (2) o más registros con datos iguales de cada uno de los participantes  o similares serán descalificados. Los responsables de la promoción se reservan el derecho de eliminar a todo aquel invitado que lleve a cabo cualquier comportamiento irregular o ilegal en esta promoción.</p>
    <p>No podrán participar en ésta promoción, empleados de Cinemex o de sus subsidiarias o afiliadas, de Viajes de Esperanza, Coralillo Films o personal perteneciente a compañía alguna involucrada en esta promoción ni cónyuges, parientes en línea recta en cualquier grado o parientes en línea colateral hasta el segundo grado de las personas antes mencionadas.</p>
    <p>En caso de que el premio no sea reclamado por los ganadores, en un plazo de 30 días hábiles siguientes a la publicación que lo acreditó como tal, dicho premio pasará al siguiente participante que resulte ganador de acuerdo a los términos antes mencionados.</p>
    <p>La promoción es vigente del 6 de noviembre de 2014 y hasta el 21 de noviembre de 2014 y válida en la República Mexicana. La inscripción a esta promoción es gratuita.</p>
    <p>No podrán ser acreedores al premio quienes hayan ganado en alguna otra promoción realizada por Cinemex en los últimos 6 meses.</p>
    <p>Los participantes que contesten la trivia se considerarán que conocen y aceptan incondicionalmente los términos y condiciones de la misma.</p>
    <p>De la misma manera, al participar en la promoción, todos los participantes autorizan a los responsables a publicar sus nombres a los efectos señalados en estas reglas, para publicaciones de carácter informativo y o publicitario, sin compensación alguna bajo ningún concepto, por lo que los participantes al participar en la promoción otorgan a las organizadoras, responsables, sus subsidiarias y filiales el finiquito más amplio que en derecho corresponda, manifestando que no se reservan acción alguna en su contra.</p>
    <p>La entrega del premio se encuentra condicionada al cumplimiento previo de todas y cada una de las leyes federales, estatales y locales, regulaciones y reglamentos que sean aplicables. El no cumplimiento a cualquiera de los puntos previstos en  a las bases y condiciones, o cualquier falsedad en el registro o la conducta errónea y/o dolosa del ganador, provocará  la descalificación inmediata del participante y, consecuentemente todos los privilegios en su carácter de posible ganador serán terminados de manera inmediata.</p>
    <p>Cinemex Desarrollos, S.A. de C.V., (mencionado como “Cinemex”), con domicilio en Avenida Javier Barros Sierra, Número 540, Torre 1, PH1, Col. Santa Fe, C.P. 01210, Álvaro Obregón, México, D.F., te comunica lo siguiente:</p>
    <p>Los Datos Personales que te son solicitados, son con la finalidad primaria de poderte identificar como participante y en caso de ser ganador,  poder realizar la entrega del incentivo.</p>
    <p>Asimismo, podremos tratar tus Datos Personales con las siguientes finalidades secundarias:</p>

    <ul>
      <li>Proporcionarte información de eventos, boletines informativos y/o cartelera “Cinemex” por medios electrónicos, auditivos y/o mensajes de texto;</li>
      <li>Participación en trivias, y en su caso, entrega de premios y publicación de ganadores;</li>
      <li>Informarte y ofrecerte promociones y publicidad de “Cinemex”, de patrocinadores y socios comerciales;</li>
      <li>Llenado de encuestas; y</li>
      <li>Contactarte en fechas especiales.</li>
    </ul>

    <p>Al participar, estás aceptando los términos y condiciones de la promoción, así como, el  Aviso de Privacidad.</p>
    <p>Podrá conocer el Aviso de Privacidad Integro en la siguiente página de Internet: www.cinemex.com/privacidadpromociones</p>
    <p>Los organizadores y/o responsables no otorgan algún tipo de garantía respecto del Premio materia de la presente promoción. Del mismo modo los organizadores se deslindan de cualquier responsabilidad que pudiera derivarse de defectos, imperfecciones o vicios en el Premio, debiéndose en su caso hacerse efectiva la garantía que proporciona el fabricante del mismo en los términos que en ella se establezcan.</p>
    <p>Los participantes que resulten ganadores tendrán un plazo improrrogable de 30 días siguientes, a partir del primer día de la publicación, para reclamar el premio; en caso de no hacerlo dentro de ese tiempo se entenderá que renuncia expresa e irrevocablemente a cualquier reclamación, acción o derecho.</p>
    <p>Los ganadores no pueden  variar, cambiar o sustituir, agregar ningún elemento al premio (en ninguna sección).</p>
    <p>Al participar en esta promoción los participantes ganadores, renuncian a cualquier reclamación o acción legal que pudieran ejercer y liberan de cualquier responsabilidad a la Compañía Organizadora o sus empresas filiales o subsidiarias y/o a cualquiera de los patrocinadores del premio, por situaciones resultantes del uso y disfrute del mismo.</p>
    <p>Todas las promociones de los Organizadores y/o responsables podrán ser suspendidas, canceladas o modificadas total o parcialmente, en cualquier momento haciendo la publicación en la página web o en el medio donde se publique la presente promoción. Los Organizadores son el órgano inapelable que interpretará todas las cuestiones que se susciten con relación a esta promoción y las bases y condiciones del concurso promocional.</p>


    <h2>PREMIO</h2>

    <p>Viajes para dos personas a la Rivera Maya.</p>

    <ul>
      <li>Boleto de avión redondo Cd. México - Cancún -   Cancún - Cd. México</li>
      <li>Hotel para dos personas durante 3 días y dos noches.</li>
      <li>Alimentos incluidos  (desayuno, comida, cena).</li>
      <li>Bebidas ilimitadas  (no incluye bebidas alcohólicas).</li>
      <li>En caso de que el ganador resida fuera de la ciudad de México deberá de trasladarse al D.F para poder llevar a cabo el viaje.</li>
      <li>El viaje tendrá vigencia hasta el mayo de 2015 y no podrá ser realizado en temporadas alta, días festivos ni puentes vacacionales.</li>
      <li>No incluye transportaciones terrestres Aeropuerto - Hotel, hotel aeropuerto, gastos personales, llamadas telefónicas, contrataciones de pay per view, masajes, servicios de estéticas o cualquier gasto adicional al paquete.</li>
    </ul>
  </div>
</div>
<?php $view['slots']->stop(); ?>
