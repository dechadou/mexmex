<?php

namespace SocialSnack\AdminBundle\Tests\Controller;
use SocialSnack\AdminBundle\Entity\AdminRole;
use SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class ReportControllerTest
 * @package SocialSnack\AdminBundle\Tests\Controller
 * @author Guido Kritz
 */
class ReportControllerTest extends WebTestCase {


  public function testTransactionsByDateAction() {
    $date = new \DateTime('yesterday');
    $client = $this->getLoggedInClient(['ROLE_REPORTS']);
    $client->request(
        'POST',
        '/missisippi/report/transactionsByDate',
        ['date' => $date->format('Y-m-d')]
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertEquals('text/csv; charset=UTF-8', $client->getResponse()->headers->get('Content-type'));
  }


  public function testTransactionsByDateActionForbidden() {
    $date = new \DateTime('yesterday');
    $client = $this->getLoggedInClient();
    $client->request(
        'POST',
        '/missisippi/report/transactionsByDate',
        ['date' => $date->format('Y-m-d')]
    );
    $this->assertEquals(403, $client->getResponse()->getStatusCode());
  }


  public function testTicketsByCinemaByAppAction() {
    $client = $this->getLoggedInClient(['ROLE_REPORTS']);
    $client->request(
        'POST',
        '/missisippi/report/ticketsByCinemaByApp',
        ['year' => 2014, 'month' => '']
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertEquals('text/csv; charset=UTF-8', $client->getResponse()->headers->get('Content-type'));
  }
} 