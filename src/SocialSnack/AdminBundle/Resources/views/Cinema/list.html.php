<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de cines - Cinemex');
$view['slots']->start('body')
?>

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de cines
      </header>
      <div class="panel-body">
        <div class="adv-table">
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>ID</th>
                <th>Legacy ID</th>
                <th>Nombre</th>
                <th>Estado</th>
                <th>Area</th>
                <th>Platino</th>
                <th>Activo</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($cinemas as $cinema) { ?>
                <tr class="gradeX">
                  <td><?php echo $cinema->getId(); ?></td>
                  <td><?php echo $cinema->getLegacyId(); ?></td>
                  <td><?php echo $cinema->getName(); ?></td>                            
                  <td><?php echo $cinema->getState()->getName(); ?></td>
                  <td><?php echo $cinema->getArea()->getName(); ?></td>
                  <td><?php echo ($cinema->getPlatinum()) ? "Si" : "No"; ?></td>   
                  <td><?php echo ($cinema->getActive()) ? "Si" : "No"; ?></td>   
                  <td><a href="cinema/<?php echo $cinema->getId(); ?>" class="edit">Editar</a></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').dataTable({
      "aaSorting": [[4, "desc"]]
    });
  });
</script>
<?php $view['slots']->stop(); ?>