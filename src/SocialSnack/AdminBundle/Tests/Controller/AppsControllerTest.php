<?php

namespace SocialSnack\AdminBundle\Tests\Controller;

/**
 * Class AppsControllerTest
 * @package SocialSnack\AdminBundle\Tests\Controller
 * @author Guido Kritz
 */
class AppsControllerTest extends WebTestCase {


  public function testAppVersionsAction() {
    $client = $this->getLoggedInClient(['ROLE_APPS']);
    $client->request(
        'GET',
        '/missisippi/appversions'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testAppVersionsAddAction() {
    $client = $this->getLoggedInClient(['ROLE_APPS']);
    $client->request(
        'GET',
        '/missisippi/appversions/add'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testPushSendAction() {
    $client = $this->getLoggedInClient(['ROLE_APPS']);
    $client->request(
        'GET',
        '/missisippi/apps/push/send'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testPushBulkAction() {
    $client = $this->getLoggedInClient(['ROLE_APPS']);
    $client->request(
        'GET',
        '/missisippi/apps/push/bulk'
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }


  public function testPushBulkProcessAction() {
    $client = $this->getLoggedInClient(['ROLE_APPS']);
    $client->request(
        'POST',
        '/missisippi/apps/push/bulk/process',
        [
            'form' => [
                'device'  => ['android', 'iphone'],
                'title'   => 'Lorem',
                'message' => 'Ipsum',
            ],
        ]
    );
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-type'));
  }


}