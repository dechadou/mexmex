<?php

namespace SocialSnack\WsBundle\Tests\Request;

/**
 * Class ConsultaActualizacionesTest
 * @package SocialSnack\WsBundle\Tests\Request
 * @author Guido Kritz
 */
class ConsultaActualizacionesTest extends WebTestCase {

  protected function get_ws_method() {
    return 'ConsultaActualizaciones';
  }


  public function testRequestSuccess() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?><CMXXML>  <RESPUESTA>    <CodigoMax>163422</CodigoMax>    <CINES>      <CINE>        <Clave>1074</Clave>        <Code>PCA</Code>      </CINE>      <CINE>        <Clave>1173</Clave>        <Code>MCU</Code>      </CINE>      <CINE>        <Clave>1275</Clave>        <Code>ISL</Code>      </CINE>      <CINE>        <Clave>1285</Clave>        <Code>LRO</Code>      </CINE>    </CINES>  </RESPUESTA></CMXXML>');

    $res = self::$ws->request();

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    $this->assertObjectHasAttribute('CodigoMax', $res);
    $this->assertObjectHasAttribute('CINES', $res);
  }


  public function testRequestNoCinemas() {
    $this->setWsResponseContent('<?xml version="1.0" encoding="utf-8"?> <CMXXML>   <RESPUESTA>     <CodigoMax>163418</CodigoMax>   </RESPUESTA> </CMXXML>');

    $res = self::$ws->request();

    $this->assertInstanceOf('\SimpleXMLElement', $res);
    $this->assertObjectHasAttribute('CodigoMax', $res);
  }

}