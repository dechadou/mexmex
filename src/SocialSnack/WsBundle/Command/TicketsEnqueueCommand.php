<?php
namespace SocialSnack\WsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SocialSnack\WsBundle\Entity\Task;

class TicketsEnqueueCommand extends ContainerAwareCommand {
	
	protected function configure() {
		$this
			->setName( 'ws:tickets:enqueue' )
			->setDescription( 'Enqueue to fetch tickets from the Web Service' )
      ->addOption( 'cinema_id', 'cid', InputOption::VALUE_OPTIONAL );
	}
	
	protected function execute( InputInterface $input, OutputInterface $output ) {
		set_time_limit( 0 );
		
    $updater      = $this->getContainer()->get( 'cinemex.ticket_updater' );
		$doctrine     = $this->getContainer()->get( 'doctrine' );
		$em           = $doctrine->getManager();
		$cinema_repo  = $doctrine->getRepository( 'SocialSnackWsBundle:Cinema' );
    $tasks        = array();
    
    if ( $cinema_id = $input->getOption( 'cinema_id' ) ) {
      $cinemas = $cinema_repo->findBy( array( 'id' => $cinema_id ) );
    } else {
      $cinemas = $cinema_repo->findAll();
    }
    
    
		/* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */
		/* @var $task   \SocialSnack\WsBundle\Entity\Task   */
		foreach ( $cinemas as $cinema ) {
			$output->writeln( sprintf( 'Cinema %s', $cinema->getName() ) );
      $task = new Task();
      $task->setService('cinemex.ticket_updater');
      $task->setData(serialize(array('cinema_legacy_id' => $cinema->getLegacyId(), 'cinema_id' => $cinema->getId())));
      $em->persist($task);
		}

    $em->flush();
    
		$output->writeln( 'Didn\'t fail, yay!' );
		return;
		
	}
	
}