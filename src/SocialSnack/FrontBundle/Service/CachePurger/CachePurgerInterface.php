<?php

namespace SocialSnack\FrontBundle\Service\CachePurger;

use SocialSnack\FrontBundle\Service\CachePurgeTask\CachePurgeTaskInterface;

interface CachePurgerInterface {

  public function canHandle(CachePurgeTaskInterface $task);

}