<?php
$view->extend('SocialSnackFrontBundle::base.html.php');

$view['slots']->set('title', 'Clásicos - Cinemex');
$view['slots']->set('body_class', 'sponsored-classics');
?>

<?php $view['slots']->start('body'); ?>
<style>
.section-classics .section-nav {
  margin-bottom:1px;
}

.classics-content {
  background:#b5242a url(<?php echo $view['assets']->getUrl('assets/img/landings/clasicos/bg.png'); ?>) no-repeat 50% 0;
  background-size:cover;
  padding:70px 0;
}

.classics-content .grid-col-4 {
  width:auto;
}

.classics-content .movie-title {
  font-size:20px;
  line-height:1.1;
  margin-bottom:10px;
}

.classics-content .movie-poster {
  background:#b5983a;
  background:-webkit-linear-gradient(top,  #b5983a 0%,#faf1ab 30%,#b5983a 60%);
  background:-o-linear-gradient(top,  #b5983a 0%,#faf1ab 30%,#b5983a 60%);
  background:linear-gradient(to bottom,  #b5983a 0%,#faf1ab 30%,#b5983a 60%);
  filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#b5983a', endColorstr='#b5983a',GradientType=0);

  border:1px solid #9d8411;
  box-shadow:0 5px 10px rgba(0, 0, 0, .5);
  margin-bottom:15px;
  padding:10px;
}

.classics-content .movie-poster img {
  border:1px solid #9d8411;
  display:block;
}

.classics-content .movie-card:hover {
  text-decoration:none;
}

.classics-content .movie-card {
  color:white;
  display:block;
  margin-bottom:40px;
  text-align:center;
  width:204px;
}

.classics-index .classics-content,
.classics-cinemas .classics-content {
  background:none;
  padding:0;
}

.classics-cinemas .section-nav {
  margin-bottom:15px;
}
</style>

<div class="section-classics classics-<?php echo $current_page; ?>">
  <div class="section-header">
    <img src="<?php echo $view['assets']->getUrl('assets/img/landings/clasicos/header.jpg'); ?>" alt="Regresan los clásicos">
  </div>

  <div class="section-nav">
    <ul>
      <li><a href="<?php echo $view['router']->generate('classics_index'); ?>" <?php if ($current_page === 'index') : ?>class="selected"<?php endif; ?>>Clásicos</a></li>
      <li><a href="<?php echo $view['router']->generate('classics_movies'); ?>" <?php if ($current_page === 'movies') : ?>class="selected"<?php endif; ?>>Películas</a></li>
      <li><a href="<?php echo $view['router']->generate('classics_cinemas'); ?>" <?php if ($current_page === 'cinemas') : ?>class="selected"<?php endif; ?>>Sedes</a></li>
    </ul>
  </div>

  <div class="section-content classics-content"><?php $view['slots']->output('landingbody'); ?></div>
</div>
<?php $view['slots']->stop(); ?>
