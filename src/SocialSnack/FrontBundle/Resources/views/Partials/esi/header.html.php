<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<header id="site-header">
  <div id="site-top">
  </div><!-- #site-top -->

  <div class="wrapper" id="header-logo-area">
    <h1 id="site-logo"><a href="<?php echo $view['router']->generate('home'); ?>">Cinemex</a></h1>

    <form method="get" action="<?php echo $view['router']->generate('billboard'); ?>" id="header-area" class="col-xs-hidden">
      <select id="header-area-select">
        <option value="">Selecciona tu ubicación</option>
        <?php foreach ( $states as $_state ) { ?>
        <optgroup label="<?php echo $_state->getName(); ?>">
          <?php if ( sizeof($_state->getAreas()) > 1 ) { ?>
          <option value="estado-<?php echo $_state->getId(); ?>" data-display-text="<?php echo $_state->getName(); ?>" data-url="<?php echo $view['router']->generate('billboard', array('area_type' => 'estado', 'area_id' => $_state->getId(), 'slug' => FrontHelper::sanitize_for_url($_state->getName()))); ?>">Buscar todo <?php echo $_state->getName(); ?></option>
          <?php } ?>
          <?php foreach ( $_state->getAreas() as $_area ) { ?>
          <option value="zona-<?php echo $_area->getId(); ?>" data-url="<?php echo $view['router']->generate('billboard', array('area_type' => 'zona', 'area_id' => $_area->getId(), 'slug' => FrontHelper::sanitize_for_url($_area->getName()))); ?>"><?php echo $_area->getName(); ?></option>
          <?php } ?>
        </optgroup>
        <?php } ?>
      </select>

      <button type="submit" class="btn btn-default">Ver toda la cartelera</button>
    </form><!-- #header-area -->

    <div id="header-user" <?php if ( isset( $no_cache_header_user ) ) echo 'class="no-cache"'; ?>>
      <div id="header-user-login">
        <a href="<?php echo $view['router']->generate( 'user_login' ); ?>" class="btn btn-icon icon icon-user" rel="popup-trigger" data-popup="#login-popup">Ingresar</a>
        <a href="<?php echo $view['router']->generate( 'user_register' ); ?>" class="btn btn-icon icon icon-edit">Registrarme</a>
      </div>
      <a class="no-desktop" id="hamburger-bt"> <em class="icon icon-white icon-settings no-txt"></em>Menú</a>
    </div>
  </div>


    <div id="top-menu">
      <ul class="wrapper">
        <li><a href="<?php echo $view['router']->generate( 'home' );            ?>">Inicio</a></li>
        <li><a href="<?php echo $view['router']->generate( 'cinemas_archive' ); ?>">Cines</a></li>
        <li><a href="<?php echo $view['router']->generate( 'platinum' );        ?>">Platino</a></li>
        <li><a href="<?php echo $view['router']->generate( 'premium_index' );         ?>">Premium</a></li>
        <li><a href="<?php echo $view['router']->generate( 'x4d_index' );             ?>">X4D</a></li>
        <li><a href="<?php echo $view['router']->generate( 'xtremo_index' );             ?>">CinemeXtremo</a></li>
        <li><a href="<?php echo $view['router']->generate( '3d_index' );              ?>">3D</a></li>
        <li><a href="<?php echo $view['router']->generate( 'art' );             ?>">Casa de Arte</a></li>
        <li><a href="<?php echo $view['router']->generate( 'ea_index' );             ?>">Espacio alternativo</a></li>
        <li><a href="<?php echo $view['router']->generate( 'cinemom' );         ?>">CineMá</a></li>
        <li><a href="<?php echo $view['router']->generate( 'ie_program' );      ?>">Invitado Especial</a></li>
        <li><a href="<?php echo $view['router']->generate( 'promos_archive' );  ?>">Promos</a></li>
        <li><a href="<?php echo $view['router']->generate( 'contact' );         ?>">Buzón Cinemex</a></li>
        <li><a href="<?php echo $view['router']->generate( 'about_index' );         ?>">Nosotros</a></li>
      </ul><!-- #top-menu -->
    </div>


</header><!-- #site-header -->
