<?php
$view->extend('SocialSnackFrontBundle::base.html.php');
$view['slots']->set( 'title', 'Platino - Cinemex' );
$view['slots']->start('body')
?>

<?php
echo $view->render(
    'SocialSnackFrontBundle:Sections:platinumMenu.html.php',
    array( 'route' => $route )
);
?>

<div id="billboard-movies">
  <?php
  $movies_per_page = 18;
  $movies_cols     = 6;
  echo $view->render(
      'SocialSnackFrontBundle:Partials:bodyMovies.html.php',
      array(
          'movies'          => array_slice( $movies, 0, $movies_per_page ),
          '_movies'         => $_movies,
          'movies_per_page' => $movies_per_page,
          'movies_cols'     => $movies_cols,
      )
  );
  ?>
</div><!-- #billboard-movies -->

<?php $view['slots']->stop(); ?>