server "108.168.135.11", :app, :web, :primary => true
server "108.168.135.39", :app, :web, :primary => true
set :clear_controllers, false
set :parameters_file, "parameters_staging.yml"
#set :use_set_permissions, true
set :branch, "composer-parameters"
