<?php
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:Mobile:skeleton.html.php');
$view['slots']->start('base');
?>
<div id="overflow-ctl">
  <header id="site-header">
    <div id="site-top">
      <h1 id="site-logo"><a href="<?php echo $view['router']->generate( 'mobile_index' ); ?>">Cinemex</a></h1>
    </div><!-- #site-top -->
  </header><!-- #site-header -->

  <div class="wrapper" id="main-body">

    <?php $view['slots']->output('body'); ?>

  </div><!-- #main-body -->

</div><!-- #overflow-ctl -->

<div id="popups">
  <div id="popups-center">

    <?php $view['slots']->output( 'popups' ); ?>

  </div><!-- #popups-center -->
</div><!-- #popups -->

<?php $view['slots']->stop(); ?>