<?php

namespace SocialSnack\AdminBundle\Tests\Functional\Controller;

use SocialSnack\AdminBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadAppData;
use SocialSnack\RestBundle\Tests\Fixtures\LoadUserData;

class ClientsControllerTest extends WebTestCase {

  protected $authHandlerMock;

  public function setUp() {
    $this->authHandlerMock = $this->getServiceMockBuilder('rest.auth.handler')->getMock();
  }

  protected function _testGetTokenAction($adminRoles, $method, $params) {
    $client = $this->getLoggedInClient(
        $adminRoles,
        [
            '\SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            '\SocialSnack\RestBundle\Tests\Fixtures\LoadAppData',
            '\SocialSnack\RestBundle\Tests\Fixtures\LoadUserData',
        ]
    );

    $client->getContainer()->set(
        'rest.auth.handler',
        $this->authHandlerMock
    );

    if (is_callable($params)) {
      $params = $params();
    }

    $route = sprintf('/missisippi/clients/%d/getToken', LoadAppData::$apps[0]->getId());
    $client->request(
        $method,
        $route,
        $params
    );

    return $client->getResponse();
  }

  public function testGetTokenAction_Forbidden() {
    $response = $this->_testGetTokenAction([], 'GET', []);

    $this->assertSame(403, $response->getStatusCode());
  }

  public function testGetTokenAction_GET() {
    $response = $this->_testGetTokenAction(['ROLE_APPS'], 'GET', []);

    $this->assertTrue($response->isSuccessful());
  }

  public function testGetTokenAction_POST_Success() {
    $this->authHandlerMock;
    $this->authHandlerMock->method('buildToken')->willReturn('TEST TOKEN');

    $response = $this->_testGetTokenAction(['ROLE_APPS'], 'POST', function() {
      return [
          'form' => [
              'user_id'    => LoadUserData::$users[0]->getId(),
              'passphrase' => 'foobar',
          ]
      ];
    });

    $this->assertContains('TEST TOKEN', $response->getContent());
  }

  public function testGetTokenAction_POST_BadPassphrase() {
    $response = $this->_testGetTokenAction(['ROLE_APPS'], 'POST', function() {
      return [
          'form' => [
              'user_id'    => LoadUserData::$users[0]->getId(),
              'passphrase' => 'badpass',
          ]
      ];
    });

    $this->assertContains('BAD PASSPHRASE', $response->getContent());
  }

  public function testGetTokenAction_POST_InvalidUser() {
    $response = $this->_testGetTokenAction(['ROLE_APPS'], 'POST', function() {
      return [
          'form' => [
              'user_id'    => LoadUserData::$users[0]->getId() * 1000,
              'passphrase' => 'foobar',
          ]
      ];
    });

    $this->assertContains('INVALID USER', $response->getContent());
  }

}