<?php

namespace SocialSnack\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

use SocialSnack\WsBundle\Service\Helper as WsHelper;
use SocialSnack\FrontBundle\Service\Helper as FrontHelper;

class PartialsController extends BaseController {

  /**
   * _sidebar_check_disabled function.
   *
   * @access protected
   * @param mixed $cinema
   * @param mixed $single
   * @return void
   */
  protected function _sidebar_check_disabled($cinema, $single) {
      $disabled_cinemas = $this->container->getParameter('disabled_cinemas');

      if ( in_array($cinema->getId(), $disabled_cinemas) ) {
          $response = $this->forward('SocialSnackFrontBundle:Partials:sidebarDisabledCinema', array( 'cinema' => $cinema, 'single' => $single ));
          $response->send();

          die();
      }
  }

  /**
   * Find the earliest session and set the response cache expire header with the
   * time of that session.
   *
   * @todo This function needs some testing and refactoring.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   * @param array $sessions
   */
  protected function _expire_by_sessions( &$response, $sessions ) {
      $response->setSharedMaxAge( 10 * 60 );
      $response->setMaxAge( 10 * 60 );
      $response->setPublic();
      return;

      $expire        = NULL;
      $first_sessions = array();

      foreach ( $sessions as $session ) {
          $first_session = NULL;

          if ( !$session )
              continue;

          try {
              $first_session = reset( $session );

              if ( isset( $first_session['sessions'] ) ) {
                  $first_session  = reset( $first_session['sessions'] );
                  $first_sessions[] = $first_session['sessions'][0];
              } else {
                  foreach ( $session as $item ) {
                      $first_session  = reset( $item['movies'] );
                      $first_sessions[] = $first_session['sessions'][0];
                  }
              }
          } catch ( \Exception $e ) {}
      }

      foreach ( $first_sessions as $first_session ) {
          if ( $first_session instanceof \SocialSnack\WsBundle\Entity\Session ) {
              /* @var $d \DateTime */
              /* @var $t \DateTime */

              $d = $first_session->getDate();
              $t = $first_session->getTime();
              $d->setTime( $t->format('H'), $t->format('i'), $t->format('s') );

              if ( is_null( $expire ) || $d < $expire ) { // Keep the lowest time.
                  $expire = $d;
              }
          }
      }

      if ( $expire ) {
          $now  = new \DateTime('now');
          $nows = (int)$now->format('U');
          $exps = (int)$expire->format('U');
          $diff = ( $exps - $nows ) / 60;

          if ( $diff > 30 ) {
              $response->setExpires( $expire );
          } else {
              $response->setSharedMaxAge( 30 * 60 );
              $response->setMaxAge( 30 * 60 );
          }

          $response->setPublic();
      }
  }

  /*
  public function esiCsrfAction($token, $name) {
      $response = $this->render( 'SocialSnackFrontBundle:Partials:esi/csrf.html.php', array(
          'token' => $token,
          'name'  => $name,
      ) );

      $response->setPrivate();
      return $response;
  }
  //*/

  /**
   * @Route("/esi/jsStuff", name="esi_js_stuff")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/jsStuff.html.php")
   * @Cache(public=true, maxage="604800", smaxage="604800")
   */
  public function esiJsStuffAction() {
    return $this->_get_js_stuff();
  }

  /**
   * @Route("/esi/socialFooter", name="esi_social_footer")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/socialFooter.html.php")
   * @Cache(public=true, maxage="3600", smaxage="3600")
   */
  public function esiSocialFooterAction() {
    $doctrine   = $this->getDoctrine();
    $insta_repo = $doctrine->getRepository( 'SocialSnackWsBundle:Instagram' );
    $tweet_repo = $doctrine->getRepository( 'SocialSnackWsBundle:Twitter' );
    $instagram  = $insta_repo->findBy( array( 'active' => 1 ), array( 'id' => 'DESC' ), 8 );
    $tweets     = $tweet_repo->findBy( array( 'active' => 1 ), array( 'id' => 'DESC' ), 10 );
    $_tweets    = array();

    foreach ( $tweets as $tweet ) {
        $_tweet = $tweet->toArray();
        $_tweet['html'] = FrontHelper::linkify_twitter_status( htmlentities($tweet->getText()) );
        unset( $_tweet['text'] );
        $_tweets[] = $_tweet;
    }

    return array(
        'instagram' => $instagram,
        'tweets'    => $tweets,
        '_tweets'   => $_tweets,
    );
  }

  /**
   * @Route("/esi/newsFooter", name="esi_news_footer")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/newsFooter.html.php")
   * @Cache(public=true, maxage="3600", smaxage="3600")
   */
  public function esiNewsFooterAction() {
    $doctrine = $this->getDoctrine();
    $repo     = $doctrine->getRepository( 'SocialSnackFrontBundle:Article' );
    $articles = $repo->findBy(
        array('status' => \SocialSnack\FrontBundle\Entity\Article::STATUS_PUBLISH),
        array('dateCreated' => 'DESC'),
        1
    );

    return array('articles' => $articles);
  }


  /**
   * @Route("/esi/locationPopup", name="esi_location_popup")
   * @Template(engine="php", template="SocialSnackFrontBundle:Popups:location.html.php")
   * @Cache(public=true, maxage="604800", smaxage="604800")
   */
  public function esiLocationPopupAction() {
    $doctrine   = $this->getDoctrine();
    $state_repo = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
    $states     = $state_repo->findAllWithAreas();

    return array('states' => $states);
  }

  /**
   * @Route("/sidebarCinema/{cinema_id}/{arguments}",
   *   requirements={"arguments" = ".*"},
   *   defaults={"arguments" = ""},
   *   name="sidebar_cinema"
   * )
   * @Template(engine="php")
   */
  public function sidebarCinemaAction( $cinema_id, $arguments ) {
    /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */

    // Parse URL arguments.
    $args   = FrontHelper::parse_url_arguments( $arguments );
    $single = isset( $args[ 'single' ] );

    // DB stuff.
    $session_repo    = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Session' );
    $cinema_repo     = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
    $movie_repo      = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
    $cinema          = $cinema_repo->find( $cinema_id );

    if (!$cinema) {
      return $this->createNotFoundException();
    }

    /* @var $session_repo \SocialSnack\WsBundle\Entity\SessionRepository */
    /* @var $cinema_repo  \SocialSnack\WsBundle\Entity\CinemaRepository  */
    /* @var $movie_repo   \SocialSnack\WsBundle\Entity\MovieRepository   */

    // List dates with sessions.
    $available_dates = $session_repo->findAvailableDatesByCinema( $cinema_id );

    // Get sessions based on date.
    $date = FrontHelper::get_billboard_date($args, $cinema, $available_dates);
    $sessions = $movie_repo->findAllSessionsInCinemaByDate( $cinema->getId(), $date );

    // PLATINO
    $cinema_platinum = NULL;
    $cinema_standard = NULL;

    if ($cinema->getPlatinum() === FALSE) {
      $cinema_platinum = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => TRUE,
          'active'   => TRUE,
      ));
    } else {
      $cinema_standard = $cinema_repo->findOneBy(array(
          'lat'      => $cinema->getLat(),
          'lng'      => $cinema->getLng(),
          'platinum' => FALSE,
          'active'   => TRUE,
      ));
    }

    $response = $this->render(
        'SocialSnackFrontBundle:Partials:sidebarCinema.html.php',
        array(
            'date'            => $date,
            'cinema'          => $cinema,
            'cinema_platinum' => $cinema_platinum,
            'cinema_standard' => $cinema_standard,
            'sessions'        => $sessions,
            'single'          => $single,
            'available_dates' => $available_dates,
        )
    );
    $response->setSharedMaxAge( 10 * 60 );
    $response->setMaxAge( 10 * 60 );
    return $response;
  }


  /**
   * sidebarDisabledCinemaAction function.
   *
   * @access public
   * @param mixed $cinema
   * @param mixed $single
   * @return void
   */
  public function sidebarDisabledCinemaAction($cinema, $single) {
    $response = new Response();
    $response->setPublic();
    $response->setSharedMaxAge( 10 * 60 );
    $response->setMaxAge( 10 * 60 );
    $response->setContent( $this->renderView('SocialSnackFrontBundle:Partials:sidebarDisabledCinema.html.php', array( 'cinema' => $cinema, 'single' => $single )) );

    return $response;
  }


  /**
   * @Route("/sidebarX4d/{arguments}",
   *   requirements={"arguments" = ".*"},
   *   defaults={"arguments" = ""}
   * )
   * @Template(engine="php")
   */
  public function sidebarX4dAction( $arguments ) {
    /* @var $cinema \SocialSnack\WsBundle\Entity\Cinema */

    // Parse URL arguments.
    $args = FrontHelper::parse_url_arguments( $arguments );

    // DB stuff.
    $state_repo   = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:State' );
    $session_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Session' );
    $cinema_repo  = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Cinema' );
    $movie_repo   = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );

    $today = new \DateTime( 'today' );

    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

    $qb->addSelect( 's.date' )
        ->from( 'SocialSnackWsBundle:Session', 's' )
        ->innerJoin( 'SocialSnackWsBundle:Movie', 'm' )
        ->where( $qb->expr()->like( 'm.attributes', ':attributes' ) )
        ->setParameter( 'attributes', '%"v4d":true%' )
        ->groupBy( 's.date' )
        ->orderBy( 's.date', 'ASC' );

    WsHelper::dql_apply_dates( $qb, $today, 0 );
    $query = $qb->getQuery();
    $available_dates = $query->execute();

    // Get sessions based on date.
    if (isset($args['date'])) {
        $date = new \DateTime( $args['date'] );

        if ( $date == $today ) {
            $date    = new \DateTime( 'now' );
        }
    } else {
        $date = $available_dates[0]['date'];
    }

    $sessions = $cinema_repo->findByAttrWithMovieSessionsForDate( array( 'v4d' => TRUE ), $date );

    // Render the view.
    return array(
        'date'            => $date,
        'sessions'        => $sessions,
        'available_dates' => $available_dates
    );
  }


  /**
   * @Route("/bodyMovies/{arguments}",
   *   requirements={"arguments" = ".*"},
   *   defaults={"arguments" = ""}
   * )
   * @Template(engine="php")
   */
  public function bodyMoviesAction( $arguments ) {
    // Parse URL arguments.
    $args = FrontHelper::parse_url_arguments( $arguments );

    if (!isset($args['area'])) {
        $args['area'] = 1;
    }

    if (!isset($args['date'])) {
        $args['date'] = date('Y-m-d');
    } else {
        $args['date'] = WsHelper::format_ymd( $args['date'] );
    }

    $movie_repo  = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );

    if ( !isset( $args['cinema'] ) ) {
        $movies = $movie_repo->findAllMoviesByAreaByDate( $args['area'], $args['date'] );
    } else {
        $movies = $movie_repo->findAllMoviesByCinemaByDate( $args['area'], $args['date'] );
    }

    // Render the view.
    return array(
        'movies' => $movies
    );
  }


  /**
   * @Route("/sidebarMovie/{movie_id}/{cinema_id}/{arguments}",
   *   name="partials_sidebar_movie",
   *   defaults={
   *     "arguments" = ""
   *   },
   *   requirements={
   *     "movie_id"  = "[0-9]+",
   *     "cinema_id" = "[0-9]+",
   *     "arguments" = ".*"}
   * )
   * @Template(engine="php")
   */
  public function sidebarMovieAction( $movie_id, $cinema_id, $arguments ) {
    // Parse URL arguments.
    $args = FrontHelper::parse_url_arguments( $arguments );

    $movie_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Movie' );
    $cinema_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:Cinema' );
    $distance_repo = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:CinemaDistance' );
    $state_repo    = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:State' );
    $area_repo     = $this->get( 'doctrine' )->getRepository( 'SocialSnackWsBundle:StateArea' );

    $today  = new \DateTime( 'today' );
    $cinema = $cinema_repo->find( $cinema_id );
    $movie  = $movie_repo->find( $movie_id );

    if (!$cinema || !$movie) {
      return new Response('', 404);
    }

    // Find closest cinemas.
    /* @var $cc \SocialSnack\WsBundle\Entity\CinemaDistance */
    $closer = array();

    foreach ($cinema->getClosestCinemas() as $cc) {
        $closer[$cc->getToCinema()->getId()] = $cc->getDistance();
    }

    asort($closer);

    $closer_ids = array_keys($closer);

    // Find dates with sessions to display.
    if ( $closer_ids ) {
        $available_dates = $movie_repo->findAvailableDatesInCinema( $movie, array_merge( array( $cinema_id ), $closer_ids ) );
    } else {
        $available_dates = $movie_repo->findAvailableDatesInCinema( $movie, $cinema_id );
    }

    // Get sessions based on date.
    $date = FrontHelper::get_billboard_date($args, $cinema, $available_dates);

    $sessions = $movie_repo->findMovieSessionsInCinemaByDate( $movie, $cinema->getId(), $date );

    if ( $closer_ids ) {
        $closer_cinemas = $cinema_repo->findWithMovieSessionsForDate( $closer_ids, $movie, $date );
        $closer_cinemas = FrontHelper::order_cinemas_by_distance( $closer_cinemas, $cinema->getClosestCinemas() );
    } else {
        $closer_cinemas = NULL;
    }

    /** @todo Replace everything with a single query with all the cinemas, then group PHP side */

    $response = new Response();

    if ( $sessions || $closer_cinemas ) {
        $this->_expire_by_sessions( $response, array( $sessions, $closer_cinemas ) );
    }

    // Render the view.
    $response->setContent($this->renderView('SocialSnackFrontBundle:Partials:sidebarMovie.html.php', array(
        'date'            => $date,
        'movie'           => $movie,
        'cinema'          => $cinema,
        'sessions'        => $sessions,
        'closer_cinemas'  => $closer_cinemas,
        //'other_cinemas'   => $other_cinemas,
        //'states'          => $states,
        //'area'            => $area,
        'available_dates' => $available_dates,
        'area'            => $cinema->getArea()
    )));

    return $response;
  }

  /**
   * @Route("/movieScore/{movie_id}",
   *   requirements={
   *     "movie_id"  = "[0-9]+",
   *   }
   * )
   * @Template(engine="php")
   */
  public function movieScoreAction( $movie_id ) {
      $movie_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
      $vote_repo  = $this->getDoctrine()->getRepository( 'SocialSnackFrontBundle:MovieVote' );
      $movie      = $movie_repo->find( $movie_id );
      $user       = $this->getUser();

      if ( $user ) {
          $vote = $vote_repo->findOneBy( array( 'user' => $user, 'movie' => $movie ) );
      } else {
          $vote = false;
      }

      return array(
          'movie'       => $movie,
          'user_score'  => $vote ? $vote->getScore() : 0,
      );
  }

  /**
   * @Route("/esi/moviesInArea/{area_id}", name="esi_movies_in_area")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/moviesInArea.html.php")
   * @Cache(public=true, maxage="3600", smaxage="3600")
   */
  public function esiMoviesInAreaAction($area_id) {
    $movie_repo = $this->getDoctrine()->getRepository( 'SocialSnackWsBundle:Movie' );
    $movies     = $movie_repo->findMovieIdsInArea($area_id);
    $movies_ids = array_map( function($a) { return $a['id']; }, $movies );

    return array('movies_ids' => $movies_ids);
  }


  /**
   * @Route("/esi/header", name="esi_header")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/header.html.php")
   * @Cache(public=true, maxage="604800", smaxage="604800")
   */
  public function esiHeaderAction() {
    $doctrine      = $this->get( 'doctrine' );
    $state_repo    = $doctrine->getRepository( 'SocialSnackWsBundle:State' );
    $states        = $state_repo->findAllWithAreas();

    return array('states' => $states);
  }

  /**
   * @Route("/modal", name="modal")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:modal.html.php")
   * @Cache(public=true, maxage="86400", smaxage="86400")
   */
  public function modalAction(Request $request) {
    $modal_params = $request->query->get('modal_params');
    $modals_repo   = $this->get( 'doctrine' )->getRepository( 'SocialSnackFrontBundle:Modal');
    $modals		   = $modals_repo->findActive($modal_params);
    return array(
        'modals'=>$modals,
        'modal_params'=>$modal_params
    );
  }


  /**
   * @Route("/billboardPage")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:billboard/block.html.php")
   * @Cache(public=true, maxage="900", smaxage="1800")
   */
  public function billboardPageAction() {
    $request = $this->getRequest();
    $doctrine        = $this->getDoctrine();
    $movie_repo      = $doctrine->getRepository('SocialSnackWsBundle:Movie');
    $state_repo      = $doctrine->getRepository('SocialSnackWsBundle:State');
    $cinema_repo     = $doctrine->getRepository('SocialSnackWsBundle:Cinema');
    $state_id        = $request->query->get('state_id');
    $area_id         = $request->query->get('area_id');
    $cinema_id       = $request->query->get('cinema_id');
    $state           = $state_repo->find($state_id);
    $is_state        = !$area_id;
    $data            = array();
    $_date           = $request->query->get('date');
    $date            = FrontHelper::get_billboard_date(array('date' => $_date), $state);
    $items_per_page  = 10;
    $page            = $request->query->get('page');
    $offset          = $page * $items_per_page;

    if ( $is_state ) {
      $args = array('state_id' => $state_id);
    } else {
      $args = array('area_id' => $area_id);
    }

    $cinemas = $cinema_repo->findHavingSessionsForDate($date, $args, $offset, $items_per_page);

    foreach ( $cinemas as &$cinema ) {
      $sessions = $movie_repo->findAllSessionsInCinemaByDate( $cinema->getId(), $date );
      if ( !sizeof($sessions) )
        continue;

      $data[] = array(
          'cinema'   => $cinema,
          'sessions' => $sessions,
      );
    }

    return array('data' => $data);
  }


  /**
   * @Route("/billboardCinema")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:billboard/block.html.php")
   * @Cache(public=true, maxage="900", smaxage="1800")
   */
  public function billboardCinemaAction() {
    $request = $this->getRequest();
    $doctrine        = $this->getDoctrine();
    $movie_repo      = $doctrine->getRepository('SocialSnackWsBundle:Movie');
    $state_repo      = $doctrine->getRepository('SocialSnackWsBundle:State');
    $state_id        = $request->query->get('state_id');
    $cinema_id       = $request->query->get('cinema_id');
    $state           = $state_repo->find($state_id);
    $data            = array();
    $_date           = $request->query->get('date');
    $date            = FrontHelper::get_billboard_date(array('date' => $_date), $state);

    $qb = $doctrine->getManager()->createQueryBuilder();
    /** @var $qb \Doctrine\ORM\QueryBuilder */
    $qb->select('c')
        ->from('SocialSnackWsBundle:Cinema', 'c')
        ->andWhere('c = :cinema')
        ->setParameter('cinema', $cinema_id);

    $query = $qb->getQuery();
    $cinemas = $query->getResult();

    foreach ( $cinemas as &$cinema ) {
      $sessions = $movie_repo->findAllSessionsInCinemaByDate( $cinema->getId(), $date );
      if ( !sizeof($sessions) )
        continue;

      $data[] = array(
          'cinema'   => $cinema,
          'sessions' => $sessions,
      );
    }

    return array('data' => $data);
  }


  /**
   * @Route("/esi/promosImportant", name="esi_important_promos")
   * @Template(engine="php", template="SocialSnackFrontBundle:Partials:esi/promosImportant.html.php")
   * @Cache(public=true, maxage="86400", smaxage="86400")
   */
  public function importantPromosAction(Request $request) {
    $promoimp_repo = $this->get( 'doctrine' )->getRepository( 'SocialSnackFrontBundle:PromoImportant' );

    $_target = $request->query->get('target') ?: '';
    $target = explode(',', $_target);
    $promos = $promoimp_repo->findByTarget($target);

    return array('promos' => $promos, 'target' => $target);
  }

  /**
   * This method returns an json with the list of cinemas in the especified state
   * with the especified attributes.
   *
   * @Route("/getBillboard")
   *
   * @todo TODO: implement request params, like attrs and states
   */
  public function getBillboard(Request $request) {
    $response = new JsonResponse();
    $movies   = [];
    $status   = NULL;

    $attrs  = $request->query->get('attrs');
    $states = $request->query->get('state');

    $x4d_cinemas = [1032, 1029, 1197, 1118, 1276, 1025, 1095, 1230];

    // Find the states of the x4d cinemas
    $qb = $this->getDoctrine()->getRepository('SocialSnackWsBundle:State')
      ->createQueryBuilder('s');

    $qb->select('s')
      ->join('SocialSnackWsBundle:Cinema', 'c', 'WITH', 's.id=c.state')
      ->andWhere($qb->expr()->in('c.legacy_id', $x4d_cinemas));

    $query      = $qb->getQuery();
    $results    = $query->getResult();
    $x4d_states = array_map(function($c){ return $c->getId(); }, $results);

    if (in_array($states, $x4d_states)) :
      $status = 'success';

      $movie_repo = $this->get('doctrine')->getRepository('SocialSnackWsBundle:Movie');
      $results    = $movie_repo->findByAttr(['v4d' => TRUE]);

      foreach ($results as $movie) {
        $movie = $movie->toArray(FALSE, FALSE, FALSE);
        $slug  = FrontHelper::sanitize_for_url($movie['name']);

        $movie['cover'] = $this->container->get('templating.helper.assets')->getUrl($movie['cover'], 'MoviePosters');
        $movie['url']   = $this->generateUrl('movie_single', array('movie_id' => $movie['id'], 'slug' => $slug));

        $movies[] = $movie;
      }
    else :
      $status = 'unavailable';
    endif;

    $response->setData([
      'status' => $status,
      'items'  => $movies
    ]);

    $response->setMaxAge(86400);
    $response->setSharedMaxAge(86400);

    return $response;
  }

}
