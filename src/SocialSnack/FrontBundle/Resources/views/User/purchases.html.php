<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$view->extend('SocialSnackFrontBundle:User:base.html.php');
$view['slots']->set( 'title', 'Mis compras - Cinemex' );
$view['slots']->start( 'user_body' );
?>
<div class="content-box">
	<h1 class="content-box-title">Mis compras</h1>
  <?php if ( !sizeof( $transactions ) ) { ?>
  Aún no has realizado ninguna compra.
  <?php } ?>
  
  <?php foreach ( $transactions as $t ) { ?>
  <div class="my-purchases">
    <img src="<?php echo $view['fronthelper']->get_poster_url( $t->getMovie()->getPosterUrl(), '67x99' ); ?>" alt="<?php echo $t->getMovie()->getName(); ?>" width="64" heigh="94"/>
    <dl class="movie-details-info">
      <dt>Título</dt>
      <dd><?php echo $t->getMovie()->getName(); ?></dd>
      <?php $type = FrontHelper::get_movie_attr_str($t->getMovie()->getAttr(), ', '); ?>
      <?php if ( $type ) { ?>
      <dt>Versión</dt>
      <dd><?php echo $type; ?></dd>
      <?php } ?>
      <dt>Día</dt>
      <dd><?php echo $t->getSession()->getDate()->format( 'd/m' ); ?></dd>
      <dt>Horario</dt>
      <dd><?php echo $t->getSession()->getTime()->format( 'h:i A' ); ?></dd>
      <dt>Cine</dt>
      <dd><?php echo $t->getCinema()->getName(); ?></dd>
    </dl>
    <dl class="movie-details-info">
      <dt>Boletos comprados</dt>
      <dd>
        <?php $tickets = array(); ?>
        <?php foreach ( $t->getTickets() as $ticket ) { ?>
        <?php   $tickets[] = $ticket->qty . ' ' . $ticket->name; ?>
        <?php } ?>
        <?php echo implode( ',', $tickets ); ?>
      </dd>
      <dt>Código de compra</dt>
      <dd><?php echo $t->getCode(); ?></dd>
    </dl>
    <?php if (!empty($t->getAppId())){ ?>
    <dl class="movie-details-info">
      <dt>Origen de Compra</dt>
      <dd>
        <?php
        foreach($apps as $_app){
          if($_app->getAppId() == $t->getAppId()){
            echo $_app->getName();
            break;
          }
        }
        ?>
      </dd>
    </dl>
    <?php } ?>
    <a href="<?php echo $view['router']->generate( 'booking_print', array( 'id' => $t->getId(), 'hash' => $t->getHash() ) ); ?>#print" class="btn btn-icon icon icon-print">Imprimir</a>
  </div>
  <?php } ?>
</div>
<?php
$view['slots']->stop();
?>