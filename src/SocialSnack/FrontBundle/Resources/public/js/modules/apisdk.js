var CMX = {
  app_id : 'XXQha7vz4kdvoMSdixhN',
  base_url : url_prefix + 'rest/v2'
};

CMX.init = function(args) {
  if ('app_id' in args) {
    this.app_id = args.app_id;
  }
  return this;
};

CMX._get_full_path = function(path) {
  if (path.substr(0,1) != '/') {
    path = '/' + path;
  }
  return this.base_url + path;
};

CMX.api = function(path, method, params, callback, error) {
  if (!params) {
    params = {};
  }
  if ('string' === typeof params) {
    params += '&app_id=' + this.app_id;
  } else {
    params.app_id = this.app_id;
  }
  
  var call = {
    url      : this._get_full_path(path),
    type     : method ? method : 'GET',
    dataType : 'json',
    data     : params
  };
  
  if (callback) {
    call.success = callback;
  }
  if (error) {
    call.error = error;
  }
  
  return $.ajax(call);
};

CMX.get = function(path, params, callback, error) {
  return this.api(path, 'GET', params, callback, error);
}

CMX.post = function(path, params, callback, error) {
  return this.api(path, 'POST', params, callback, error);
}