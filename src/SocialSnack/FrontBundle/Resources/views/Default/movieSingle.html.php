<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;

$poster_url = $view['fronthelper']->get_movie_poster_url($movie, '182x272');
?>
<?php $view->extend('SocialSnackFrontBundle::base.html.php'); ?>
<?php $view['slots']->set( 'body_class', ['movie-single', 'movie-' . $movie->getGroupId()] ); ?>
<?php $view['slots']->set( 'title', $movie->getName() . ' - Cinemex' ); ?>

<?php $view['slots']->start('head_meta_tags'); ?>
<link rel="canonical" href="<?php echo $view['fronthelper']->get_permalink($movie, TRUE); ?>" />
<link rel="alternate" href="cinemex://com.cinemex/movie/<?php echo $movie->getId(); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'og_tags' ); ?>
<meta property="og:image" content="https:<?php echo $poster_url; ?>" />
<meta property="og:type"  content="video.movie" />
<meta property="og:title" content="<?php echo $movie->getName(); ?>" />
<meta property="og:description" content="<?php echo substr(htmlspecialchars(strip_tags(is_string($movie->getData('SINOPSIS')) ? $movie->getData('SINOPSIS') : '')), 0, 155); ?>" />
<?php $view['slots']->stop(); ?>

<?php $view['slots']->start( 'body' ); ?>
<ul id="breadcrumb">
  <li><a href="<?php echo $view['router']->generate('home'); ?>">Inicio</a></li>
  <li><a href="<?php echo $view['fronthelper']->get_movie_link( $movie ); ?>"><?php echo $movie->getName(); ?></a></li>
</ul>

<div class="clearfix row">

	<div class="col col-sm-3-5 col-md-2-3">
		<div id="movie-details" itemscope itemtype="http://schema.org/Movie">
			<div class="movie-details-main content-box">
				<h1 class="movie-details-title content-box-title" itemprop="name"><?php echo $movie->getName(); ?></h1>
				<?php
				$movie_info = array(
						array(
								'label' => 'Título original',
								'key'   => 'NOMBREORIGINAL',
						),
						array(
								'label' => 'País',
								'key'   => 'PAISORIGEN',
						),
						array(
								'label' => 'Año',
								'key'   => 'ANIO',
						),
						array(
								'label' => 'Clasificación',
								'key'   => 'RATING',
						),
						array(
								'label'  => 'Duración',
								'key'    => 'DURACION',
								'format' => '%s minutos',
						),
						array(
								'label' => 'Género',
								'key'   => 'GENERO',
						),
						array(
								'label' => 'Sitio oficial',
								'key'   => 'WEBSITE',
						),
				);
				if ( is_string( $movie->getData( 'DIRECTOR' ) ) ) {
					$start_tag = '<span itemprop="director" itemscope itemtype="http://schema.org/Person"><span itemprop="name">';
					$close_tag = '</span></span>';
					array_splice( $movie_info, 1, 0, array( array(
								'label' => 'Director',
								'value' =>
										$start_tag
										. implode(
												$close_tag . ', ' . $start_tag,
												explode( ',' , $movie->getData( 'DIRECTOR' ) )
										)
										. $close_tag,
					) ) );
				}
				if ( is_string( $movie->getData( 'ACTORES' ) ) ) {
					$start_tag = '<span itemprop="actor" itemscope itemtype="http://schema.org/Person"><span itemprop="name">';
					$close_tag = '</span></span>';
					array_splice( $movie_info, 1, 0, array( array(
								'label' => 'Actores',
								'value' =>
										$start_tag
										. implode(
												$close_tag . ', ' . $start_tag,
												explode( ',' , $movie->getData( 'ACTORES' ) )
										)
										. $close_tag,
					) ) );
				}
				?>
				<dl class="movie-details-info">
					<?php foreach ( $movie_info as $item ) { ?>
					<?php if ( ( ( isset( $item['value'] ) && ( $value = $item['value'] ) ) || ( $value = $movie->getData( $item['key'] ) ) ) && is_string( $value ) ) { ?>
					<dt><?php echo $item['label']; ?></dt>
					<dd><?php echo isset( $item['format'] ) ? sprintf( $item['format'], $value ) : $value; ?></dd>
					<?php } ?>
					<?php } ?>
				</dl>

				<div class="movie-details-btns">
					<?php if ( $trailer_url = $movie->getData( 'TRAILER' ) ) { ?>
					<a href="#" class="btn btn-grey btn-icon icon icon-play" rel="tab-select" data-tab="movie-trailer-tab">Ver Tráiler</a>
					<?php } ?>

					<?php $score = $movie->getScore(); ?>
					<div id="movie-score-set" class="movie-details-score dynamic-container require-location" data-route="partials/movieScore/<?php echo $movie->getId(); ?>" data-source="" data-listeners="">
						<span class="btn btn-sec btn-no-shadow score-number"><?php echo $score ? $score : 'N/A'; ?></span>
					</div>
				</div>

				<img src="<?php echo $poster_url; ?>" alt="<?php echo $movie->getName(); ?>" width="182" height="272" class="movie-details-cover" itemprop="image" />
			</div>

			<div class="movie-details-tabs tabs">
				<ul class="movie-details-tabs-links tabs-links">
					<li><a href="#" class="selected"><span class="discicon icon-red icon-star"></span> Trailer</a></li>
					<li><a href="#"><span  class="discicon icon-light icon-screen"></span> Sinopsis</a></li>
					<li><a href="#"><span  class="discicon icon-light icon-comments"></span> Comentarios</a></li>
				</ul>

				<div class="tabs-content">
					<div class="movie-details-tab tab-content selected" id="movie-trailer-tab">
						<?php if ( $youtube_id = FrontHelper::get_youtube_id_from_url( $movie->getData( 'TRAILER' ) ) ) { ?>
						<iframe width="100%" height="450" src="//www.youtube.com/embed/<?php echo $youtube_id; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
						<?php } else { ?>
						No hay tráiler disponible.
						<?php } ?>
					</div>

					<div class="movie-details-tab tab-content">
						<p><?php echo is_string( $movie->getData( 'SINOPSIS' ) ) ? $movie->getData( 'SINOPSIS' ) : ''; ?></p>
					</div>

					<div class="movie-details-tab tab-content">
						<div class="fb-comments" data-href="<?php echo $view['fronthelper']->get_movie_link_abs($movie); ?>" data-colorscheme="light" data-numposts="5" data-width="599"></div>
					</div>
				</div>
			</div>
		</div><!-- #movie-details -->
	</div>

	<div class="dynamic-container col col-sm-2-5 col-md-1-3" data-route="partials/sidebarMovie/<?php echo $movie->getId(); ?>/{id}/{args}" data-source="fn get_cinemasidebar_args" data-listeners="user_login change_location" data-loadcb="mycinema_sidebar_loaded">

    <div id="location-sidebar" class="popup location-popup">
      <?php echo $this->render( 'SocialSnackFrontBundle:Popups:locationSide.html.php'); ?>
    </div><!-- #location-sidebar -->

	</div>

</div>
<?php
$view['slots']->stop();
?>


<?php $view['slots']->start('esi_featured_promos'); ?>
<?php
echo $view['actions']->render(
    $view['router']->generate('esi_important_promos', array('target' => 'movies')),
    array('strategy' => 'esi')
);
?>
<?php $view['slots']->stop(); ?>
