<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de Beneficios IE - Cinemex');
$view['slots']->start('body')
?>

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Listado de Beneficios IE <?php if (FALSE === $active) echo 'inactivos'; ?>
          <div class="right">
            <a href="<?php echo $view['router']->generate( 'social_snack_promoie_benefits_list' . (FALSE === $active ? '' : '_inactive') );?>">Ver <?php echo (FALSE === $active) ? 'activos' : 'inactivos'; ?></a>
          </div>
        </header>
        <div class="panel-body">
          <div class="adv-table">
            <div class="clearfix">
              <div class="btn-group">
                <button class="btn green" id="editable-sample_new" onclick="window.location.href = '<?php echo $view['router']->generate('social_snack_promoie_benefits_add'); ?>'">
                  Nuevo beneficio IE <i class="icon-plus"></i>
                </button>
              </div>
            </div>
            <table  class="display table table-bordered table-striped" id="example">
              <thead>
              <tr>
                <th>Imagen</th>
                <th>Nombre</th>
                <th>Activo</th>
                <th>Editar</th>
                <th>Borrar</th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($benefits as $benefit) {
                ?>
                <tr class="gradeX">
                  <td><img src="<?php echo $view['fronthelper']->get_cms_url($benefit->getImage(), '100x100'); ?>" width="100" /></td>
                  <td><?php echo $benefit->getName(); ?></td>
                  <td><?php echo $benefit->getActive() ? 'Sí' : 'No'; ?></td>
                  <td><a href="<?php echo $view['router']->generate('social_snack_promoie_benefits_edit', array('id' => $benefit->getId())); ?>">Editar</a></td>
                  <td><a href="<?php echo $view['router']->generate('social_snack_promoie_benefits_delete', array('id' => $benefit->getId())); ?>" onclick="return confirm('Estás seguro?')">Borrar</a></td>
                </tr>
              <?php
              }
              ?>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#example').dataTable();
    });
  </script>
<?php $view['slots']->stop(); ?>