<?php

namespace SocialSnack\FrontBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class ConceptHelperFactory
 * @package SocialSnack\FrontBundle\Service
 * @author Guido Kritz
 */
class ConceptHelperFactory {

  static $doctrine;

  public function __construct(Registry $doctrine) {
    self::$doctrine = $doctrine;
  }


  /**
   * @param string $concept
   * @return ConceptHelperAbstract
   * @throws \Exception
   */
  public static function createHelper($concept) {
    // Since a PHP class name cannot start with a number...
    if ('3D' === $concept) {
      $concept = 'V3D';
    }

    // Just to avoid confusions...
    if ('CX' === $concept) {
      $concept = 'Xtreme';
    }

    $class  = "\\SocialSnack\\FrontBundle\\Service\\ConceptHelper\\$concept";

    if (!class_exists($class)) {
      throw new \Exception(sprintf('Class "%s" is not defined.', $class));
    }

    $helper = new $class(self::$doctrine);

    return $helper;
  }

} 