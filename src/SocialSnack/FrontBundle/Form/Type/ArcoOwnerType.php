<?php

namespace SocialSnack\FrontBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

use SocialSnack\WsBundle\Request\Arco\MuestraCatalogos;

class ArcoOwnerType extends ArcoAbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $max_error = 'Máximo permitido: %d caracteres';
    $max_fname = 20;
    $max_lname = 30;
    $max_zip   = 10;
    $max_phone = 20;
    $max_hours = 50;
    
    $builder->add('PrimerNombre', null, array(
        'label' => 'Primer nombre',
        'constraints' => array(
            new NotBlank(),
            new Length(array(
                'max' => $max_fname,
                'maxMessage' => sprintf($max_error, $max_fname),
            )),
        ),
    ));
    $builder->add('SegundoNombre', null, array(
        'label' => 'Segundo nombre',
        'required' => FALSE,
        'constraints' => array(
            new Length(array(
                'max' => $max_fname,
                'maxMessage' => sprintf($max_error, $max_fname),
            )),
        ),
    ));
    $builder->add('ApellidoPaterno', null, array(
        'label' => 'Apellido paterno',
        'constraints' => array(
            new NotBlank(),
            new Length(array(
                'max' => $max_lname,
                'maxMessage' => sprintf($max_error, $max_lname),
            )),
        ),
    ));
    $builder->add('ApellidoMaterno', null, array(
        'label' => 'Apellido materno',
        'constraints' => array(
            new NotBlank(),
            new Length(array(
                'max' => $max_lname,
                'maxMessage' => sprintf($max_error, $max_lname),
            )),
        ),
    ));

    $current_year = (int)date('Y');
    $years = range($current_year, $current_year - 100);
    $builder->add('FechaNac', 'birthday', array(
        'label' => 'Fecha de nacimiento (día/mes/año)',
        'years' => $years,
        'constraints' => array(
            new NotBlank(),
        ),
        'format' => 'dd/MM/yyyy'
    ));

    $builder->add('Domicilio', null, array(
        'label' => 'Domicilio',
        'constraints' => array(
            new NotBlank(),
        ),
    ));
    $builder->add('CP', null, array(
        'label' => 'Código Postal',
        'constraints' => array(
            new NotBlank(),
            new Length(array(
                'max' => $max_zip,
                'maxMessage' => sprintf($max_error, $max_zip),
            )),
            new Type(array(
                'type' => 'numeric',
                'message' => 'El valor debe ser numérico'
            ))
        ),
    ));
    $builder->add('Estado', 'choice', array(
        'label' => 'Estado',
        'choices' => $this->getWsChoices(MuestraCatalogos::CATALOGO_ESTADOS, array( '' => 'Seleccione un estado' )),
        'constraints' => array(
            new NotBlank()
        ),
    ));
    $builder->add('Identificacion', 'choice', array(
        'label' => 'Identificación',
        'choices' => $this->getWsChoices(MuestraCatalogos::CATALOGO_IDENTIFICACION, array( '' => 'Seleccione una opción' )),
        'constraints' => array(
            new NotBlank()
        ),
    ));
    $builder->add('Email', 'email', array(
        'label' => 'Email',
        'constraints' => array(
            new NotBlank(),
            new Email(),
        ),
    ));
    $builder->add('Telefono', null, array(
        'label' => 'Teléfono',
        'required' => FALSE,
        'constraints' => array(
            new Length(array(
                'max' => $max_phone,
                'maxMessage' => sprintf($max_error, $max_phone),
            )),
            new Type(array(
                'type' => 'numeric',
                'message' => 'El valor debe ser numérico'
            ))
        ),
    ));
    $builder->add('Horario', null, array(
        'label' => 'Horario de contacto',
        'constraints' => array(
            new NotBlank(),
            new Length(array(
                'max' => $max_hours,
                'maxMessage' => sprintf($max_error, $max_hours),
            )),
        ),
    ));
    $builder->add('attach_id', 'file', array(
        'label' => 'Anexo que acredite la identidad del titular',
        'attr' => array('class' => 'whole'),
        'constraints' => array(
            new File(array(
                'maxSize' => '6144k',
                'maxSizeMessage' => 'El tamaño del archivo supera el límite permitido.',
                'uploadErrorMessage' => 'No se pudo subir el archivo.'
            ))
        )
    ));
  }


  public function getName() {
    return 'arco_owner';
  }

}