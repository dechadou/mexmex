<?php

namespace SocialSnack\AdminBundle\Form\Type;

use SocialSnack\AdminBundle\Validator\Constraints\CinemexDeepLink;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SplashscreenType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options = []) {
    $builder
        ->add('name', 'text', array(
            'label' => 'Nombre',
        ))
        ->add('deep_link', 'text', array(
            'label' => 'Deep link',
            'constraints' => [
                new CinemexDeepLink(),
            ],
            'required' => FALSE,
            'attr' => [
                'help' => 'Home: cinemex://com.cinemex/home<br/>
Película: cinemex://com.cinemex/movie/movie_id<br/>
Próximo estreno: cinemex://com.cinemex/movieComing/id<br/>
Novedad: cinemex://com.cinemex/article/article_id<br/>
Promos: cinemex://com.cinemex/promociones<br/>
Cine: cinemex://com.cinemex/cinema/cine_id<br/>'
            ]
        ))
        ->add('image_v', 'file', array(
            'label' => 'Imagen vertical',
            'mapped' => FALSE,
            'required' => $builder->getData()->getId() ? FALSE : TRUE,
            'image_preview_path' => 'image_v',
        ))
        ->add('image_h', 'file', array(
            'label' => 'Imagen horizontal',
            'mapped' => FALSE,
            'required' => $builder->getData()->getId() ? FALSE : TRUE,
            'image_preview_path' => 'image_h',
        ))
        ->add('active', 'checkbox', array(
            'label' => 'Activo',
            'required' => FALSE,
        ))
    ;
  }

  public function getName() {
    return '';
  }

}