<?php
$view->extend('SocialSnackAdminBundle::base.html.php');
$view['slots']->set('title', 'Listado de transacciones - Cinemex');
$view['slots']->start('body')
?>
<style>
  #example_filter{display:none}
</style>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        Listado de transacciones
      </header>

      <div class="panel-body">
        <div class="col-lg-8 right">
          <select class="left input-sm" name="search_term" style="width:30%">
            <option value="0">Booking</option>
            <option value="1">TDC</option>
          </select>
          <input type="text" id="search_box" placeholder="search" class="form-control input-sm right" style="width:67%">
        </div>
        <div class="adv-table">
          <table  class="display table table-bordered table-striped" id="example">
            <thead>
              <tr>
                <th>Fecha de compra</th>
                <th>Booking</th>
                <th>Función</th>
                <th>Película</th>
                <th>Cine</th>
                <th>TDC</th>
                <th>Ver</th>
                <th>IP</th>
                <th>Bloquear IP</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
     $('#example').dataTable( {
      "order": [[ 0, "desc" ]],
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo $view['router']->generate('social_snack_admin_transactions_list_ajax'); ?>",
      "aoColumns": [
          null,
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false }
      ]
    });
    var table_api = $("#example").dataTable().api();

    $('#search_box').keyup(function(e){
      if(e.keyCode == 13) {
        table_api
            .column($('select[name="search_term"]').val())
            .search($(this).val()).draw();
      }
    })

    // Grab the datatables input box and alter how it is bound to events
    /*$(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input", function(e) { // Bind our desired behavior
          // If the length is 3 or more characters, or the user pressed ENTER, search
          if(this.value.length >= 3 || e.keyCode == 13) {
            // Call the API search function
            table_api.search(this.value).draw();
          }
          // Ensure we clear the search if they backspace far enough
          if(this.value == "") {
            table_api.search("").draw();
          }
          return;
        });
    */
    $('#example').delegate('.block-ip', 'click', function(e) {
      e.preventDefault();
      var $t = $(this);
      var data = $t.data('post');
      
      if ($t.data('working')) {
        return;
      }
      
      if (!confirm('Si bloqueas la IP ' + data.ip + ' no se podrán realizar más transacciones desde ese origen. ¿Deseas continuar?')) {
        return;
      }
      
      $t.data({ working : true }).text('Procesando...');
      
      $.ajax({
        url : $t.attr('href'),
        type : 'post',
        data : data,
        dataType : 'json',
        success : function(res) {
          $t.replaceWith('Listo');
        },
        error : function() {
          alert('Ocurrió un error.');
          $t.data({ working : false }).text('Bloquear IP');
        }
      });
    });
  });
</script>
<?php $view['slots']->stop(); ?>