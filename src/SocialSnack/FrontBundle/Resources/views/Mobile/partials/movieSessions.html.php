<?php
use \SocialSnack\FrontBundle\Service\Helper as FrontHelper;
?>
<?php foreach ( $items as $item ) { ?>
<div class="mycinema-sessions-group" <?php echo FrontHelper::get_movie_sessions_block_data($item, $platinum); ?>>
  <div class="mycinema-variant">
    <?php foreach (FrontHelper::get_movie_attrs($item['movie']->getAttr(), TRUE, TRUE) as $type => $label) {; ?>
      <span class="movie-info-attr movie-attr-<?php echo $type; ?>"><?php echo $label; ?></span>
    <?php } ?>
  </div>
  <div class="posrel">
    <div class="posrel sessions-scroller">
      <div class="mycinema-sessions icon icon-dark icon-clock pad-left">
        <?php /* @var $session \SocialSnack\WsBundle\Entity\Session */ ?>
        <?php foreach ( $item['sessions'] as $session ) { ?>
        <a href="<?php echo $view['router']->generate( 'mobile_checkout', array( 'session_id' => $session->getId() ) ); ?>" rel="noindex,nofollow" title="Comprar boletos"><?php echo $session->getTime()->format( 'h:i A' ); ?></a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>