<?php

namespace SocialSnack\AdminBundle\Tests\Functional\Controller;

use SocialSnack\AdminBundle\Tests\Controller\WebTestCase;
use SocialSnack\AdminBundle\Tests\Fixtures\LoadTrackingCodeData;

class MoviesControllerTest extends WebTestCase {

  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeListAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/movies/trackingCodes/'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeListAction_Inactive() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/movies/trackingCodes/inactive'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeAddAction() {
    $client = $this->getLoggedInClient(['ROLE_CONTENT']);
    $client->request(
        'GET',
        '/missisippi/movies/trackingCodes/add'
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeEditAction() {
    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadTrackingCodeData'
        ]
    );
    $client->request(
        'GET',
        sprintf('/missisippi/movies/trackingCodes/%d', LoadTrackingCodeData::$codes[0]->getId())
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeDeleteAction() {
    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadTrackingCodeData'
        ]
    );
    $client->request(
        'GET',
        sprintf('/missisippi/movies/trackingCodes/%d/delete', LoadTrackingCodeData::$codes[0]->getId())
    );

    $this->assertSame(302, $client->getResponse()->getStatusCode());
    $this->assertSame('/missisippi/movies/trackingCodes/', $client->getResponse()->headers->get('location'));
  }


  /**
   * @group functional
   * @group smoke
   */
  public function testTrackingCodeAssignAction() {
    $client = $this->getLoggedInClient(
        ['ROLE_CONTENT'],
        [
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadAdminData',
            'SocialSnack\AdminBundle\Tests\Fixtures\LoadTrackingCodeData'
        ]
    );
    $client->request(
        'GET',
        sprintf('/missisippi/movies/trackingCodes/%d/assign', LoadTrackingCodeData::$codes[0]->getId())
    );

    $this->assertTrue($client->getResponse()->isSuccessful());
  }

}