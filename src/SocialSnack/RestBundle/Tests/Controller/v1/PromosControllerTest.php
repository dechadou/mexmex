<?php

namespace SocialSnack\RestBundle\Controller\v1;

use SocialSnack\RestBundle\Tests\Controller\WebTestCase;
use SocialSnack\RestBundle\Tests\Fixtures\LoadPromosData;

class PromosControllerTest extends WebTestCase {

  protected function getPromoCollectionModel() {
    return [
        'promo' => [
            'type' => 'array',
            'each' => $this->getPromoModel(),
        ]
    ];
  }


  protected function getPromoModel() {
    return [
        'id'       => 'integer',
        'title'    => 'string',
        'url'      => 'string',
        'image'    => 'string',
        'thumb'    => 'string',
    ];
  }


  protected function getPromoACollectionModel() {
    return [
        'promoA' => [
            'type' => 'array',
            'each' => $this->getPromoAModel(),
        ]
    ];
  }


  protected function getPromoAModel() {
    return [
        'id'       => 'integer',
        'title'    => 'string',
        'url'      => 'string',
        'image'    => 'string',
    ];
  }


  protected function getPromoIECollectionModel() {
    return [
        'promoIE' => [
            'type' => 'array',
            'each' => $this->getPromoIEModel(),
        ]
    ];
  }


  protected function getPromoIEModel() {
    return [
        'id'         => 'integer',
        'title'      => 'string',
        'thumb'      => 'string',
        'image'      => 'string',
        'categories' => 'array',
    ];
  }


  protected function getPromoImportantCollectionModel() {
    return [
        'promoImportant' => [
            'type' => 'array',
            'each' => $this->getPromoImportantModel(),
        ]
    ];
  }


  protected function getPromoImportantModel() {
    return [
        'id'       => 'integer',
        'title'    => 'string',
        'url'      => 'string',
        'image'    => 'string',
        'thumb'    => 'string',
    ];
  }


  protected function _test($url, $model) {
    $this->loadFixtures([
        '\SocialSnack\RestBundle\Tests\Fixtures\LoadPromosData',
    ]);

    $client = self::createClient();
    $client->request(
        'GET',
        is_string($url) ? $url : $url()
    );

    $this->assertJsonResponse($client->getResponse(), $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testDefault() {
    $model = [
        [
            'model' => array_merge(
                $this->getPromoCollectionModel(),
                $this->getPromoACollectionModel(),
                $this->getPromoIECollectionModel(),
                $this->getPromoImportantCollectionModel()
            ),
        ]
    ];

    $this->_test('/rest/promos/', $model);
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTypePromo() {
    $this->_test(
        '/rest/promos/promo',
        $this->getPromoCollectionModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTypePromoA() {
    $this->_test(
        '/rest/promos/promoA',
        $this->getPromoACollectionModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTypePromoIE() {
    $this->_test(
        '/rest/promos/promoIE',
        $this->getPromoIECollectionModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTypePromoImportant() {
    $this->_test(
        '/rest/promos/promoImportant',
        $this->getPromoImportantCollectionModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTargetPromo() {
    $this->_test(
        function() { return sprintf('/rest/promos/promo/%d', LoadPromosData::$promos[0]->getId()); },
        $this->getPromoModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTargetPromoA() {
    $this->_test(
        function() { return sprintf('/rest/promos/promoA/%d', LoadPromosData::$promosA[0]->getId()); },
        $this->getPromoAModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTargetPromoIE() {
    $this->_test(
        function() { return sprintf('/rest/promos/promoIE/%d', LoadPromosData::$promosIE[0]->getId()); },
        $this->getPromoIEModel()
    );
  }


  /**
   * @group functional
   * @group api_v1
   */
  public function testTargetPromoImportant() {
    $this->_test(
        function() { return sprintf('/rest/promos/promoImportant/%d', LoadPromosData::$promosImportant[0]->getId()); },
        $this->getPromoImportantModel()
    );
  }

}