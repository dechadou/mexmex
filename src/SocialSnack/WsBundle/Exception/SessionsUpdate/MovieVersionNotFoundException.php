<?php

namespace SocialSnack\WsBundle\Exception\SessionsUpdate;

use SocialSnack\WsBundle\Entity\Movie;

class MovieVersionNotFoundException extends \Exception {
  
  protected $main_movie = NULL;
  
  public function __construct(Movie $main_movie, $message = '', $code = 0, $previous = NULL) {
    $this->main_movie = $main_movie;
    
    parent::__construct($message, $code, $previous);
  }
  
  /**
   * @return Movie
   */
  public function getMainMovie() {
    return $this->main_movie;
  }
  
}
